__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import Syren, Wych, Debutante, Bloodbride


class DELeader(Leader):
    def __init__(self, *args, **kwargs):
        super(DELeader, self).__init__(*args, **kwargs)
        UnitType(self, Syren)


class DETroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(DETroopers, self).__init__(*args, **kwargs)
        UnitType(self, Wych)


class DENewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(DENewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Debutante)


class DESpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(DESpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Bloodbride)


class DEKillTeam(SWAGRoster):
    army_name = 'Dark Eldar Wych Cult Kill Team'
    army_id = 'de_swag_v1'

    def __init__(self):
        super(DEKillTeam, self).__init__(
            DELeader, DETroopers, DESpecialists,
            DENewRecruits)
