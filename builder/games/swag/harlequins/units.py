__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from .armory import *


class Master(Unit):
    type_name = 'Troupe Master'
    type_id = 'h_master_v1'

    def __init__(self, parent):
        super(Master, self).__init__(parent, points=300, gear=[
            Gear('Concealed blade'), Gear('Haqlequin mask'),
            Gear('Flip belt'), Gear('Holo suit')])
        H2H(self, master=True)
        Pistols(self, master=True)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Player(Unit):
    type_name = 'Player'
    type_id = 'h_player_v1'

    def __init__(self, parent):
        super(Player, self).__init__(parent, points=150, gear=[
            Gear('Concealed blade'), Gear('Haqlequin mask'),
            Gear('Flip belt'), Gear('Holo suit')])
        H2H(self)
        Pistols(self)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Mime(Unit):
    type_name = 'Mime'
    type_id = 'h_mime_v1'

    def __init__(self, parent):
        super(Mime, self).__init__(parent, points=125, gear=[
            Gear('Concealed blade'), Gear('Haqlequin mask'),
            Gear('Flip belt'), Gear('Holo suit')])
        H2H(self)
        Pistols(self)
        Grenades(self)
        Misc(self)


class Virtuoso(Unit):
    type_name = 'Virtuoso'
    type_id = 'h_virtuoso_v1'

    def __init__(self, parent):
        super(Virtuoso, self).__init__(parent, points=175, gear=[
            Gear('Concealed blade'), Gear('Haqlequin mask'),
            Gear('Flip belt'), Gear('Holo suit')])
        H2H(self, virtuoso=True)
        Pistols(self, master=True)
        Grenades(self)
        Misc(self)
        Characteristics(self)
        Skills(self)
