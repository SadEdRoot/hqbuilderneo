__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.swag.unit import Unit
from builder.games.swag.options import Characteristics, Skills
from .armory import *


class Appointed(Unit):
    type_name = 'Appointed Immortal'
    type_id = 'necr_appointed_v1'

    def __init__(self, parent):
        super(Appointed, self).__init__(parent, points=200, gear=[
            Gear('Combat blade'), Gear('Immortal exosceleton')
        ])
        Basic(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Immortal(Unit):
    type_name = 'Immortal'
    type_id = 'necr_immortal_v1'

    def __init__(self, parent):
        super(Immortal, self).__init__(parent, points=110, gear=[
            Gear('Combat blade'), Gear('Immortal exosceleton')
        ])
        Basic(self)
        Misc(self)
        Characteristics(self)
        Skills(self)


class Warrior(Unit):
    type_name = 'Warrior'
    type_id = 'necr_warrior_v1'

    def __init__(self, parent):
        super(Warrior, self).__init__(parent, points=80, gear=[
            Gear('Combat blade'), Gear('Warrior exosceleton')
        ])
        Basic(self, warrior=True)
        Misc(self)


class Deathmark(Unit):
    type_name = 'Deathmark'
    type_id = 'necr_deathmark_v1'

    def __init__(self, parent):
        super(Deathmark, self).__init__(parent, points=120, gear=[
            Gear('Combat blade'), Gear('Immortal exosceleton')
        ])
        Special(self)
        Misc(self)
        Characteristics(self)
        Skills(self)
