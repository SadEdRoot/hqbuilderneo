__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import Superior, Sister, Novitiate, Gunner


class ASLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(ASLeader, self).__init__(*args, **kwargs)
        UnitType(self, Superior)


class ASTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(ASTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Sister)


class ASNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(ASNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Novitiate)


class ASSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(ASSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Gunner)


class SororitasKillTeam(SWAGRoster):
    army_name = 'Adepta Sororitas Kill Team'
    army_id = 'sororitas_swag_v1'

    def __init__(self):
        super(SororitasKillTeam, self).__init__(
            ASLeader, ASTroopers, ASSpecialists, ASNewRecruits)
