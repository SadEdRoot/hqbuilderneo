__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import Alpha, Warrior, Spawn, Gunbeast


class TLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(TLeader, self).__init__(*args, **kwargs)
        UnitType(self, Alpha)


class TTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(TTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Warrior)


class TNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(TNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Spawn)


class TSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(TSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Gunbeast)


class TKillTeam(SWAGRoster):
    army_name = 'Tyranid Warrior Kill Team'
    army_id = 'tyranid_swag_v1'

    def __init__(self):
        super(TKillTeam, self).__init__(
            TLeader, TTroopers, TSpecialists,
            TNewRecruits, max_limit=5)
