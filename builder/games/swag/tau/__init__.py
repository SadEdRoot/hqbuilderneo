__author__ = 'Ivan Truskov'

from builder.core2 import UnitType
from builder.games.swag.sections import Section
from builder.games.swag.roster import Leader, Troopers, Specialists,\
    NewRecruits, SWAGRoster
from .units import Shasui, Pathfinder, Cadet, Specialist,\
    Recon, Grav, Pulse


class TauLeader(Leader):
    def __init__(self, *args, **kwargs):
        super(TauLeader, self).__init__(*args, **kwargs)
        UnitType(self, Shasui)


class TauTroopers(Troopers):
    def __init__(self, *args, **kwargs):
        super(TauTroopers, self).__init__(*args, **kwargs)
        UnitType(self, Pathfinder)


class TauNewRecruits(NewRecruits):
    def __init__(self, *args, **kwargs):
        super(TauNewRecruits, self).__init__(*args, **kwargs)
        UnitType(self, Cadet)


class TauSpecialists(Specialists):
    def __init__(self, *args, **kwargs):
        super(TauSpecialists, self).__init__(*args, **kwargs)
        UnitType(self, Specialist)


class Drones(Section):
    def __init__(self, parent):
        super(Drones, self).__init__(parent, 'dr', 'Drones',
                                          None, 3)
        UnitType(self, Recon)
        UnitType(self, Grav)
        UnitType(self, Pulse)


class TauKillTeam(SWAGRoster):
    army_name = 'Tau Pathfinder Kill Team'
    army_id = 'tau_swag_v1'

    def __init__(self):
        super(TauKillTeam, self).__init__(
            TauLeader, TauTroopers, TauSpecialists, TauNewRecruits)
        tr = self.sections[1]
        nr = self.sections[-1]
        dr = Drones(self)
        self._sections.value.append(dr)
        nr.peer_sections.append(dr)
        tr.peer_sections.append(dr)
