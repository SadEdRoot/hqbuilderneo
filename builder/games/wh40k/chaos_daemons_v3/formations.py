__author__ = 'Ivan Truskov'

from builder.games.wh40k.roster import Formation, UnitType

from .troops import Letters, Horrors, Daemonettes, Bearers, Nurglings, BlueHorrors, BrimstoneHorrors
from .elites import Beasts, Crushers, Fiends, Flamers
from .fast import Hounds, Drones, Seekers, Hellflayer, Screamers
from .heavy import SoulGrinder, SkullCannon, BurningChariot, SeekerCavalcade
from .hq import Fateweaver, LordOfChange, DaemonPrince,\
    Skulltaker, HKhorne, Changeling, BlueScribes, HTzeentch, ExaltedFlamer,\
    Epidemius, HNurgle, Masque, HSlaanesh


class Murderhorde(Formation):
    army_name = 'Murderhorde'
    army_id = 'mhorde_v3'

    def __init__(self):
        super(Murderhorde, self).__init__()
        heralds = [
            UnitType(self, HKhorne),
            UnitType(self, Skulltaker)
        ]
        self.add_type_restriction(heralds, 1, 1)
        footmen = [
            UnitType(self, Letters),
            UnitType(self, Hounds),
            UnitType(self, Crushers)
        ]
        self.add_type_restriction(footmen, 8, 8)


class Gorethunder(Formation):
    army_name = 'Gorethunder battery'
    army_id = 'gorethunder_v3'

    def __init__(self):
        super(Gorethunder, self).__init__()
        self.herald = UnitType(self, HKhorne, min_limit=1, max_limit=1)
        UnitType(self, SkullCannon, min_limit=3, max_limit=3)

    def check_rules(self):
        super(Gorethunder, self).check_rules()
        for unit in self.herald.units:
            if not unit.ride.throne.value:
                self.error('The Herald of Khorne must take Blood Throne of Khorne as a dedicated Transport')
                break


class Warpflame(Formation):
    army_name = 'Warpflame Host'
    army_id = 'wflame_v3'

    def __init__(self):
        super(Warpflame, self).__init__()
        heralds = [
            UnitType(self, HTzeentch),
            UnitType(self, Changeling)
        ]
        self.add_type_restriction(heralds, 1, 1)
        footmen = [
            UnitType(self, Horrors),
            UnitType(self, Flamers),
            UnitType(self, ExaltedFlamer)
        ]
        self.add_type_restriction(footmen, 9, 9)


class Skyhost(Formation):
    army_name = 'Burning Skyhost'
    army_id = 'burning_host_v3'

    def __init__(self):
        super(Skyhost, self).__init__()
        self.herald = UnitType(self, HTzeentch)
        scribes = UnitType(self, BlueScribes)
        self.add_type_restriction([self.herald, scribes], 1, 1)
        footmen = [
            UnitType(self, Screamers),
            UnitType(self, BurningChariot)
        ]
        self.add_type_restriction(footmen, 9, 9)

    def check_rules(self):
        super(Skyhost, self).check_rules()
        for unit in self.herald.units:
            if not unit.ride.any:
                self.error('The Herald of Tzeentch must take Burning Chariot of Tzeentch or a Disc of Tzeentch')
                break


class Tallyband(Formation):
    army_name = 'Tallyband'
    army_id = 'tallyband_v3'

    def __init__(self):
        super(Tallyband, self).__init__()
        heralds = [
            UnitType(self, HNurgle),
            UnitType(self, Epidemius)
        ]
        self.add_type_restriction(heralds, 1, 1)
        footmen = [
            UnitType(self, Bearers),
            UnitType(self, Nurglings)
        ]
        self.add_type_restriction(footmen, 7, 7)


class Rotswarm(Formation):
    army_name = 'Rotswarm'
    army_id = 'rotswarm_v3'

    def __init__(self):
        super(Rotswarm, self).__init__()
        UnitType(self, HNurgle, min_limit=1, max_limit=1)
        footmen = [
            UnitType(self, Beasts),
            UnitType(self, Drones)
        ]
        self.add_type_restriction(footmen, 7, 7)


class Flayertroupe(Formation):
    army_name = 'Flayertroupe'
    army_id = 'flayertroupe_v3'

    def __init__(self):
        super(Flayertroupe, self).__init__()
        heralds = [
            UnitType(self, HSlaanesh),
            UnitType(self, Masque)
        ]
        self.add_type_restriction(heralds, 1, 1)
        footmen = [
            UnitType(self, Daemonettes),
            UnitType(self, Fiends)
        ]
        self.add_type_restriction(footmen, 6, 6)


class GrandCalvalcade(Formation):
    army_name = 'Grand Cavalcade'
    army_id = 'gcavalcade_v3'

    def __init__(self):
        super(GrandCalvalcade, self).__init__()
        self.herald = UnitType(self, HSlaanesh, min_limit=1, max_limit=1)
        footmen = [
            UnitType(self, Seekers),
            UnitType(self, Hellflayer),
            UnitType(self, SeekerCavalcade)
        ]
        self.add_type_restriction(footmen, 6, 6)

    def check_rules(self):
        super(GrandCalvalcade, self).check_rules()
        for unit in self.herald.units:
            if not unit.ride.any:
                self.error('The Herald of Slaanesh must take Steed of Slaanesh or a Seeker Chariot or an Exalted Seeker Chariot')
                break


class Tetrad(Formation):
    army_name = 'Infernal Tetrad'
    army_id = 'tetrad_v3'

    def __init__(self):
        super(Tetrad, self).__init__()
        self.princes = UnitType(self, DaemonPrince, min_limit=4, max_limit=4)

    def check_rules(self):
        super(Tetrad, self).check_rules()
        gods = []
        for prince in self.princes.units:
            gods.append(prince.get_dedication())
        gods = set(gods)
        if len(gods) != 4:
            self.error('Each Daemon Prince must be upgraded to be a Daemon of a different Chaos God')


class Forgehost(Formation):
    army_name = 'Forgehost'
    army_id = 'forgehost_v3'

    def __init__(self):
        super(Forgehost, self).__init__()
        UnitType(self, SoulGrinder, min_limit=3, max_limit=3)


class LorestealerHost(Formation):
    army_name = 'Lorestealer Host'
    army_id = 'lorestealer_host_v5'

    def __init__(self):
        super(LorestealerHost, self).__init__()
        UnitType(self, BlueScribes, min_limit=1, max_limit=1)
        UnitType(self, BlueHorrors, min_limit=3, max_limit=9)


class BrimstoneConflagration(Formation):
    army_name = 'Brimstone Conflagration'
    army_id = 'brimstone_conflagration_v5'

    def __init__(self):
        super(BrimstoneConflagration, self).__init__()
        UnitType(self, ExaltedFlamer, min_limit=1, max_limit=1)
        UnitType(self, BrimstoneHorrors, min_limit=3, max_limit=9)


class OmniscientOracles(Formation):
    army_name = 'Omniscient Oracles'
    army_id = 'omniscient_oracles_v5'

    def __init__(self):
        super(OmniscientOracles, self).__init__()
        UnitType(self, Fateweaver, min_limit=1, max_limit=1)
        UnitType(self, LordOfChange, min_limit=1, max_limit=3)


class HeraldsAnarchic(Formation):
    army_name = 'Heralds Anarchic'
    army_id = 'heralds_anarchic_v5'

    def __init__(self):
        super(HeraldsAnarchic, self).__init__()
        UnitType(self, HTzeentch, min_limit=3, max_limit=9)
