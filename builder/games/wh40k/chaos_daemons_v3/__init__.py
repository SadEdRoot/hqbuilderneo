__author__ = 'Ivan Truskov'
__summary__ = ['Chaos Daemons codex (2013)', 'Curse of the Wulfen', 'Wrath of Magnus']

from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Detachment, Formation
from builder.games.wh40k.fortifications import Fort
from builder.games.wh40k.escalation.chaos import LordsOfWar as DaemonLordsOfWar
from builder.games.wh40k.imperial_armour.volume13 import DaemonHq, DaemonElites,\
    DaemonFast, DaemonHeavy, DaemonLords, cd_unit_types
from builder.games.wh40k.imperial_armour.dataslates import ChaosKnights

from .troops import Letters, Horrors, Daemonettes, Bearers, Nurglings, BlueHorrors, BrimstoneHorrors
from .elites import Beasts, Crushers, Fiends, Flamers
from .fast import Hounds, Fury, Drones, Seekers, Hellflayer, Screamers
from .heavy import SoulGrinder, SkullCannon, BurningChariot, SeekerCavalcade
from .hq import FuryThirster, RageThirster, WrathThirster, Scarbrand,\
    Fateweaver, Kugath, LordOfChange, Unclean, Keeper, DaemonPrince, Karanak,\
    Skulltaker, HKhorne, Changeling, BlueScribes, HTzeentch, ExaltedFlamer,\
    Epidemius, HNurgle, Masque, HSlaanesh, Belakor
from .formations import *


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.kugath = UnitType(self, Kugath)
        self.unclean = UnitType(self, Unclean)
        self.fateweaver = UnitType(self, Fateweaver)
        self.lordofchange = UnitType(self, LordOfChange)
        self.keeper = UnitType(self, Keeper)
        self.scarbrand = UnitType(self, Scarbrand)
        self.bloodthirsters = [
            UnitType(self, FuryThirster),
            UnitType(self, RageThirster),
            UnitType(self, WrathThirster)
        ]
        self.epidemius = UnitType(self, Epidemius, slot=0.25)
        self.hnurgle = UnitType(self, HNurgle, slot=0.25)
        self.bluescribes = UnitType(self, BlueScribes)
        self.changeling = UnitType(self, Changeling, slot=0.25)
        self.htzeentch = UnitType(self, HTzeentch, slot=0.25)
        UnitType(self, ExaltedFlamer)
        self.masque = UnitType(self, Masque)
        self.hslaanesh = UnitType(self, HSlaanesh, slot=0.25)
        self.karanak = UnitType(self, Karanak, slot=0.25)
        self.skulltaker = UnitType(self, Skulltaker, slot=0.25)
        self.hkhorne = UnitType(self, HKhorne, slot=0.25)
        self.prince = UnitType(self, DaemonPrince)
        UnitType(self, Belakor)

    def check_limits(self):
        return float(self.min) <= sum(float(t.count) for t in self.types) and \
            sum(float(t.count) * float(t.slot) for t in self.types) <= float(self.max)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, Letters)
        UnitType(self, Daemonettes)
        UnitType(self, Bearers)
        UnitType(self, Horrors)
        UnitType(self, BlueHorrors)
        UnitType(self, BrimstoneHorrors)
        UnitType(self, Nurglings)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, Hounds)
        UnitType(self, Seekers)
        UnitType(self, Drones)
        UnitType(self, Screamers)
        UnitType(self, Fury)
        UnitType(self, Hellflayer)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        UnitType(self, Fiends)
        UnitType(self, Flamers)
        UnitType(self, Crushers)
        UnitType(self, Beasts)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, SoulGrinder)
        UnitType(self, SkullCannon)
        UnitType(self, BurningChariot)
        UnitType(self, SeekerCavalcade)
        self.prince = UnitType(self, DaemonPrince)


class HQ(DaemonHq, BaseHQ):
    pass


class Elites(DaemonElites, BaseElites):
    pass


class FastAttack(DaemonFast, BaseFastAttack):
    pass


class HeavySupport(DaemonHeavy, BaseHeavySupport):
    pass


class LordsOfWar(ChaosKnights, DaemonLords, DaemonLordsOfWar):
    pass


class ChaosDaemonsV3Base(Wh40kBase):
    army_id = 'chaos_daemons_v3'
    army_name = 'Chaos Daemons'

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)

        super(ChaosDaemonsV3Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )

    def check_rules(self):
        super(ChaosDaemonsV3Base, self).check_rules()
        if sum(u.count for u in [self.hq.skulltaker, self.hq.epidemius, self.hq.changeling, self.hq.karanak,
                                 self.hq.hkhorne, self.hq.hnurgle, self.hq.hslaanesh, self.hq.htzeentch]) > 4:
            self.error("More then 4 Heralds cannot be taken in the primary detachment")

        godList = {
            'kh': 'Khorne',
            'tz': 'Tzeentch',
            'ng': 'Nurgle',
            'sl': 'Slaanesh'
        }
        aglist = []

        if self.hq.scarbrand.count +\
           sum(ut.count for ut in self.hq.bloodthirsters) > 0:
            aglist.append('kh')
        if self.hq.fateweaver.count + self.hq.lordofchange.count > 0:
            aglist.append('tz')
        if self.hq.kugath.count + self.hq.unclean.count > 0:
            aglist.append('ng')
        if self.hq.keeper.count > 0:
            aglist.append('sl')
        self.heavy.prince.active = len(aglist) > 0
        aglist = set(aglist)
        if len(aglist) == 0:
            if self.heavy.prince.count:
                self.error("Daemon princes can only be taken for Heavy Support if Greater Daemon of the corresponding "
                           "Chaos god is in HQ.")
        else:
            for gid in aglist:
                if any(prince.get_dedication() == gid for prince in self.hq.prince.units):
                    self.error("Daemon princes of " + godList[gid] + " cannot be taken as HQ.")
            for gid in [item for item in list(godList.keys()) if item not in aglist]:
                if any(prince.get_dedication() == gid for prince in self.heavy.prince.units):
                    self.error("Daemon princes of " + godList[gid] + " cannot be taken as Heavy Support.")


class ChaosDaemonsV3CAD(ChaosDaemonsV3Base, CombinedArmsDetachment):
    army_id = 'chaos_daemons_v3_cad'
    army_name = 'Chaos Daemons (Combined arms detachment)'


class ChaosDaemonsV3AD(ChaosDaemonsV3Base, AlliedDetachment):
    army_id = 'chaos_daemons_v2_ad'
    army_name = 'Chaos Daemons (Allied detachment)'

class ChaosDaemonsV3PA(ChaosDaemonsV3Base, PlanetstrikeAttacker):
    army_id = 'chaos_daemons_v3_pa'
    army_name = 'Chaos Daemons (Planetstrike attacker detachment)'

class ChaosDaemonsV3PD(ChaosDaemonsV3Base, PlanetstrikeDefender):
    army_id = 'chaos_daemons_v3_pd'
    army_name = 'Chaos Daemons (Planetstrike defender detachment)'

class ChaosDaemonsV3SA(ChaosDaemonsV3Base, SiegeAttacker):
    army_id = 'chaos_daemons_v3_sa'
    army_name = 'Chaos Daemons (Siege War attacker detachment)'

class ChaosDaemonsV3SD(ChaosDaemonsV3Base, SiegeDefender):
    army_id = 'chaos_daemons_v3_sd'
    army_name = 'Chaos Daemons (Siege War defender detachment)'

class ChaosDaemonsV3KillTeam(Wh40kKillTeam):
    army_id = 'chaos_daemons_v3_kt'
    army_name = 'Chaos Daemons'

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(ChaosDaemonsV3KillTeam.KTElites, self).__init__(parent)
            UnitType(self, Fiends)
            UnitType(self, Flamers)
            UnitType(self, Crushers)

    def __init__(self):
        super(ChaosDaemonsV3KillTeam, self).__init__(Troops(self), self.KTElites(self), BaseFastAttack(self))


class Incursion(Wh40k7ed):
    army_id = 'incursion_v3'
    army_name = 'Daemonic Incursion Detachment'

    class Core(Detachment):
        def __init__(self, parent):
            super(Incursion.Core, self).__init__(parent, 'core', 'Core',
                                                [Murderhorde, Warpflame,
                                                 Tallyband, Flayertroupe], 1, None)

    class DaemonLord(Formation):
        army_name = 'Daemon Lord'
        army_id = 'incursion_lord_v3'

        def __init__(self):
            super(Incursion.DaemonLord, self).__init__()
            lords = [
                UnitType(self, Kugath),
                UnitType(self, Unclean),
                UnitType(self, Fateweaver),
                UnitType(self, LordOfChange),
                UnitType(self, Keeper),
                UnitType(self, Scarbrand),
                UnitType(self, FuryThirster),
                UnitType(self, RageThirster),
                UnitType(self, WrathThirster),
                UnitType(self, Belakor),
                UnitType(self, DaemonPrince)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(Incursion.Command, self).__init__(parent, 'command', 'Command',
                                                   [Tetrad, Incursion.DaemonLord],
                                                    0, 3)

    class Flock(Formation):
        army_name = 'Daemon Flock'
        army_id = 'incursion_furies_v3'

        def __init__(self):
            super(Incursion.Flock, self).__init__()
            UnitType(self, Fury, min_limit=1, max_limit=1)

    class Hunter(Formation):
        army_name = 'The Hunter of Khorne'
        army_id = 'incursion_hunter_v3'

        def __init__(self):
            super(Incursion.Hunter, self).__init__()
            UnitType(self, Karanak, min_limit=1, max_limit=1)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(Incursion.Auxilary, self).__init__(parent, 'auxilary', 'Auxilary',
                                                     [
                                                         Gorethunder,
                                                         Skyhost, Rotswarm,
                                                         GrandCalvalcade,
                                                         Forgehost,
                                                         Incursion.Flock,
                                                         Incursion.Hunter
                                                     ], 1, None)

    def __init__(self):
        core = self.Core(self)
        command = self.Command(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class Pandemoniad(Wh40k7ed):
    army_id = 'Pandemoniad_v5'
    army_name = 'Pandemoniad of Tzeentch'

    class Core(Detachment):
        def __init__(self, parent):
            super(Pandemoniad.Core, self).__init__(parent, 'core', 'Core',
                                                [Warpflame, LorestealerHost], 1, None)

    class PandemoniumLord(Formation):
        army_name = 'Lord of Pandemonium'
        army_id = 'pandemonium_lord_v5'

        def __init__(self):
            super(Pandemoniad.PandemoniumLord, self).__init__()
            lords = [
                UnitType(self, Fateweaver),
                UnitType(self, LordOfChange),
                UnitType(self, DaemonPrince),
                UnitType(self, HTzeentch)
            ]
            self.add_type_restriction(lords, 1, 1)

    class Command(Detachment):
        def __init__(self, parent):
            super(Pandemoniad.Command, self).__init__(parent, 'command', 'Command',
                                                   [Pandemoniad.PandemoniumLord, OmniscientOracles],
                                                    0, 3)

    class Agents(Formation):
        army_name = 'Agents Of Tzeench'
        army_id = 'agents_of_tzeentcn_v5'

        def __init__(self):
            super(Pandemoniad.Agents, self).__init__()
            agents = [
                UnitType(self, Changeling),
                UnitType(self, BlueScribes),
            ]
            self.add_type_restriction(agents, 1, 1)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(Pandemoniad.Auxilary, self).__init__(parent, 'auxilary', 'Auxilary',
                                                     [
                                                         Pandemoniad.Agents,
                                                         Forgehost,
                                                         Skyhost,
                                                         BrimstoneConflagration,
                                                         HeraldsAnarchic
                                                     ], 1, None)

    def __init__(self):
        core = self.Core(self)
        command = self.Command(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


faction = 'Chaos Daemons'


class ChaosDaemonsV3(Wh40kImperial, Wh40k7ed):
    army_id = 'chaos_daemons_v3'
    army_name = 'Chaos Daemons'
    faction = faction

    def __init__(self):
        super(ChaosDaemonsV3, self).__init__(
            [ChaosDaemonsV3CAD, Incursion, Pandemoniad, Murderhorde, Gorethunder, Warpflame,
             Skyhost, Tallyband, Rotswarm, Flayertroupe, GrandCalvalcade,
             Tetrad, Forgehost, LorestealerHost, BrimstoneConflagration, OmniscientOracles, HeraldsAnarchic])

class ChaosDaemonsV3Missions(Wh40kImperial, Wh40k7edMissions):
    army_id = 'chaos_daemons_v3'
    army_name = 'Chaos Daemons'
    faction = faction

    def __init__(self):
        super(ChaosDaemonsV3Missions, self).__init__(
            [ChaosDaemonsV3CAD, ChaosDaemonsV3PA, ChaosDaemonsV3PD, ChaosDaemonsV3SA, ChaosDaemonsV3SD, Incursion, Pandemoniad, Murderhorde, Gorethunder, Warpflame,
             Skyhost, Tallyband, Rotswarm, Flayertroupe, GrandCalvalcade,
             Tetrad, Forgehost, LorestealerHost, BrimstoneConflagration, OmniscientOracles, HeraldsAnarchic])


detachments = [
    ChaosDaemonsV3CAD,
    ChaosDaemonsV3AD,
    ChaosDaemonsV3PA,
    ChaosDaemonsV3PD,
    ChaosDaemonsV3SA,
    ChaosDaemonsV3SD,
    Incursion,
    Pandemoniad,
    Murderhorde,
    Gorethunder,
    Warpflame,
    Skyhost,
    Tallyband,
    Rotswarm,
    Flayertroupe,
    GrandCalvalcade,
    Tetrad,
    Forgehost,
    LorestealerHost,
    BrimstoneConflagration,
    OmniscientOracles,
    HeraldsAnarchic
]


unit_types = [
    Letters, Horrors, Daemonettes, Bearers,
    Nurglings, BlueHorrors, BrimstoneHorrors,
    Beasts, Crushers, Fiends, Flamers,
    Hounds, Fury, Drones, Seekers, Hellflayer, Screamers,
    SoulGrinder, SkullCannon, BurningChariot, SeekerCavalcade,
    FuryThirster, RageThirster, WrathThirster, Scarbrand,
    Fateweaver, Kugath, LordOfChange, Unclean, Keeper, DaemonPrince, Karanak,
    Skulltaker, HKhorne, Changeling, BlueScribes, HTzeentch, ExaltedFlamer,
    Epidemius, HNurgle, Masque, HSlaanesh, Belakor
] + cd_unit_types
