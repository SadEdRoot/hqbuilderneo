__author__ = 'Ivan Truskov'

from builder.core2 import Gear, Count, OptionalSubUnit,\
    UnitDescription, SubUnit, OptionsList, OneOf
from builder.games.wh40k.roster import Unit


class BaseDaemon(Unit):
    class LeaderType(Unit):
        def __init__(self, parent, leader_name, base_gear, base_points):
            super(BaseDaemon.LeaderType, self).__init__(parent, name=leader_name,
                                                    gear=base_gear, points=5 + base_points)
            self.les = Count(self, "Lesser Rewards", 0, 2, 10)
            self.grt = Count(self, "Greater Rewards", 0, 1, 20)

        def check_rules(self):
            super(BaseDaemon.LeaderType, self).check_rules()
            Count.norm_points(20, [self.les, self.grt])

    class Leader(OptionalSubUnit):
        def __init__(self, parent, leader_type, leader_name, base_gear, base_points):
            super(BaseDaemon.Leader, self).__init__(parent, 'Leader')
            SubUnit(self, leader_type(None, leader_name, base_gear, base_points))

    class Options(OptionsList):
        def __init__(self, parent, banner_name):
            super(BaseDaemon.Options, self).__init__(parent, 'Options')
            self.icon = self.variant('Chaos Icon', 10)
            self.banner = self.variant(banner_name, 10)
            self.instrument = self.variant('Instrument of Chaos', 10)

        def check_rules(self):
            super(BaseDaemon.Options, self).check_rules()
            self.banner.active = self.banner.used = self.icon.value

    def __init__(self, parent, base_name,
                 leader_name, base_gear, base_points,
                 banner_name, min_count=10,
                 max_count=20, icon_available=True):
        self.min_count = min_count
        self.max_count = max_count
        self.icon_available = icon_available
        super(BaseDaemon, self).__init__(parent)
        self.warriors = Count(self, base_name, min_count, max_count, base_points, True,
                          gear=UnitDescription(base_name, base_points, options=base_gear))
        self.leader = self.Leader(self, self.LeaderType, leader_name,
                                  base_gear, base_points)
        if self.icon_available:
            self.opt = self.Options(self, banner_name)

    def check_rules(self):
        ldr = self.leader.count
        self.warriors.min, self.warriors.max = (self.min_count - ldr, self.max_count - ldr)

    def get_count(self):
        return self.warriors.cur + self.leader.count

    def build_description(self):
        res = UnitDescription(self.type_name, count=self.get_count(), points=self.points)
        res.add(self.leader.description)

        model_desc = self.warriors.gear
        generic = self.warriors.cur
        if self.icon_available:
            if self.opt.banner.value and self.opt.banner.used:
                res.add(model_desc.clone().add(self.opt.banner.gear)
                        .add_points(self.opt.banner.points)
                        .add_points(self.opt.icon.points))
                generic -= 1
            else:
                if self.opt.icon.value:
                    res.add(model_desc.clone().add(self.opt.icon.gear)
                            .add_points(self.opt.icon.points))
                    generic -= 1
            if self.opt.instrument.value:
                res.add(model_desc.clone().add(self.opt.instrument.gear)
                        .add_points(self.opt.instrument.points))
                generic -= 1
        res.add(model_desc.clone().set_count(generic))
        return res


class Letters(BaseDaemon):
    type_name = "Bloodletters of Khorne"
    type_id = 'bloodletters_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Bloodletters-of-Khorne')

    def __init__(self, parent):
        super(Letters, self).__init__(
            parent,
            base_name='Bloodletter',
            leader_name='Bloodreaper',
            base_gear=[Gear('Hellblade')],
            base_points=10,
            banner_name='Banner of blood'
        )


class Horrors(BaseDaemon):
    type_name = "Pink Horrors of Tzeentch"
    type_id = 'horrors_v4'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Pink-Horrors-of-Tzeentch')

    def __init__(self, parent):
        super(Horrors, self).__init__(
            parent,
            base_name='Pink Horror',
            leader_name='Iridescent Horror',
            base_gear=[],
            base_points=9,
            banner_name='Blasted standard',
            icon_available=False
        )
        self.opt = self.Options(self)
        self.icons = self.Icons(self)

    class Icons(OneOf):
        def __init__(self, parent):
            super(Horrors.Icons, self).__init__(parent, 'Icons Options')
            self.icon = self.variant('Chaos Icon', 0)
            self.arcane_icon = self.variant("Arcane Icon", 10)
            self.blasted = self.variant("Blasted standard", 15)
            self.icon_of_fire = self.variant("Icon of fire", 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Horrors.Options, self).__init__(parent, 'Options')
            self.icon = self.variant('Chaos Icon', 10)
            self.instrument = self.variant('Instrument of Chaos', 10)

    def check_rules(self):
        super(Horrors, self).check_rules()
        self.icons.visible = self.icons.used = self.opt.icon.value

    def build_description(self):
        res = UnitDescription(self.type_name, count=self.get_count(), points=self.points)
        res.add(self.leader.description)

        model_desc = self.warriors.gear
        generic = self.warriors.cur
        if self.opt.icon.value:
            res.add(model_desc.clone().add(self.icons.description)
                    .add_points(self.icons.points)
                    .add_points(self.opt.icon.points))
            generic -= 1
        if self.opt.instrument.value:
            res.add(model_desc.clone().add(self.opt.instrument.gear)
                    .add_points(self.opt.instrument.points))
            generic -= 1
        res.add(model_desc.clone().set_count(generic))
        return res

    def count_charges(self):
        if 11 <= self.get_count() <= 15:
            return 2
        elif 16 <= self.get_count() <= 20:
            return 3
        else:
            return 1

    def build_statistics(self):
        return self.note_charges(super(Horrors, self).build_statistics())


class BlueHorrors(Unit):
    type_name = "Blue Horrors"
    type_id = 'blue_horrors_v5'
    # wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Pink-Horrors-of-Tzeentch')

    def __init__(self, parent):
        self.min_count = 10
        self.max_count = 20
        super(BlueHorrors, self).__init__(parent)
        self.warriors = Count(self, "Blue Horrors", self.min_count, self.max_count, points=5, per_model=True,
                          gear=UnitDescription("Blue Horrors", 5, options=[]))

    def count_charges(self):
        return (9 + self.warriors.cur) / 10

    def get_count(self):
        return self.warriors.cur

    def build_statistics(self):
        return self.note_charges(super(BlueHorrors, self).build_statistics())


class BrimstoneHorrors(Unit):
    type_name = "Brimstone Horrors"
    type_id = 'Brimstone_horrors_v5'
    # wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Pink-Horrors-of-Tzeentch')

    def __init__(self, parent):
        self.min_count = 10
        self.max_count = 20
        super(BrimstoneHorrors, self).__init__(parent)
        self.warriors = Count(self, "Brimstone Horrors", self.min_count, self.max_count, points=3, per_model=True,
                          gear=UnitDescription("Brimstone Horrors", 3, options=[]))

    def count_charges(self):
        return 1

    def get_count(self):
        return self.warriors.cur

    def build_statistics(self):
        return self.note_charges(super(BrimstoneHorrors, self).build_statistics())


class Bearers(BaseDaemon):
    type_name = "Plaguebearers of Nurgle"
    type_id = 'bearers_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Plaguebearers-of-Nurgle')

    def __init__(self, parent):
        super(Bearers, self).__init__(
            parent,
            base_name='Plaguebearer',
            leader_name='Plagueridden',
            base_gear=[Gear('Plaguesword')],
            base_points=9,
            banner_name='Plague banner'
        )


class Daemonettes(BaseDaemon):
    type_name = "Daemonettes of Slaanesh"
    type_id = 'daemonettes_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Daemonettes-of-Slaanesh')

    def __init__(self, parent):
        super(Daemonettes, self).__init__(
            parent,
            base_name='Daemonette',
            leader_name='Alluress',
            base_gear=[],
            base_points=9,
            banner_name='Rapturous standard'
        )


class Nurglings(Unit):
    type_name = "Nurglings"
    type_id = 'nurglings_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-daemons', unit_name='Nurglings')

    def __init__(self, parent):
        super(Nurglings, self).__init__(parent)
        self.models = Count(self, 'Nurglings', 3, 9, 15, True,
                            gear=UnitDescription('Nurglings', 15))

    def get_count(self):
        return self.models.cur
