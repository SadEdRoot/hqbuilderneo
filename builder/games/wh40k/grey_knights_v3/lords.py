from builder.core2 import *
from builder.games.wh40k.roster import Unit
__author__ = 'Ivan Truskov'


class Draigo(Unit):
    type_id = 'draigo_v3'
    type_name = 'Kaldor Draigo'

    wikilink = Unit.template_link.format(codex_name='grey-knights', unit_name='Kaldor-Draigo')

    def __init__(self, parent):
        super(Draigo, self).__init__(parent=parent, points=245, unique=True, static=True, gear=[
            Gear('Terminator armour'),
            Gear('Storm bolter'),
            Gear('The Titansword'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Psyk-out grenades'),
            Gear('Iron Halo'),
            Gear('Storm shield')
        ])

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Draigo, self).build_statistics())
