from builder.core2 import UnitList, ListSubUnit, OneOf, Gear
from .armory import Vehicle, CultVehicleEquipment, DemolitionCharges
from builder.games.wh40k.unit import Unit


class Squadron(Unit):
    unit_class = None
    unit_min = 1
    unit_max = 3

    def __init__(self, parent):
        super(Squadron, self).__init__(parent)
        self.tanks = UnitList(self, self.unit_class, self.unit_min, self.unit_max)

    def get_count(self):
        return self.tanks.count


class ScoutSentinel(ListSubUnit):
    type_name = 'Scout Sentinel'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Scout-Sentinel-Squadron')

    def __init__(self, parent):
        super(ScoutSentinel, self).__init__(parent=parent, points=35)
        self.Weapon(self)
        Vehicle(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScoutSentinel.Weapon, self).__init__(parent=parent, name='Weapon')
            self.multilaser = self.variant('Multi-laser', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.autocannon = self.variant('Autocannon', 5)
            self.missilelauncher = self.variant('Missile launcher', 5)
            self.lascannon = self.variant('Lascannon', 10)


class ArmouredSentinel(ListSubUnit):
    type_name = 'Armoured Sentinel'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Armoured-Sentinel-Squadron')

    def __init__(self, parent):
        super(ArmouredSentinel, self).__init__(parent=parent, points=40)
        self.Weapon(self)
        Vehicle(self)

    class Weapon(ScoutSentinel.Weapon):
        def __init__(self, parent):
            super(ArmouredSentinel.Weapon, self).__init__(parent=parent)
            self.plasmacannon = self.variant('Plasma cannon', 10)


class Chimera(Unit):
    type_name = 'Chimera'
    type_id = 'gs_chimera_v1'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Chimera')

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Heavy bolter', 0)
            self.variant('Heavy flamer', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Multi-laser', 0)
            self.variant('Heavy bolter', 0)
            self.variant('Heavy flamer', 0)

    def __init__(self, parent):
        super(Chimera, self) .__init__(parent=parent, points=65, gear=[Gear('Searchlight'), Gear('Smoke launchers')])
        self.Weapon1(self)
        self.Weapon2(self)
        CultVehicleEquipment(self, name='Cult vehicle equipment')


class GoliathTruck(Unit):
    type_name = 'Goliath Truck'
    type_id = 'gs_goliath_truck_v1'
    # wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Chimera')

    def __init__(self, parent):
        super(GoliathTruck, self) .__init__(parent=parent, points=50, gear=[
            Gear('Heavy stubber'), Gear('Twin-linked autocannon')])

        DemolitionCharges(self, name='Cult vehicle equipment')


class ScoutSentinelSquad(Squadron):
    type_name = 'Scout Sentinel Squadron'
    type_id = 'gs_scout_sentinel_squad_v1'
    unit_class = ScoutSentinel


class ArmouredSentinelSquad(Squadron):
    type_name = 'Armoured Sentinel Squadron'
    type_id = 'gs_armoured_sentinel_squad_v1'
    unit_class = ArmouredSentinel
