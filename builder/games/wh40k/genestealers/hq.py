__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf
from builder.games.wh40k.roster import Unit
from .armory import *


class Ghosar(Unit):
    type_name = 'Patriarch Ghosar, The Ghastly Truth'
    type_id = 'gs_ghosar_v1'

    def __init__(self, parent):
        super(Ghosar, self).__init__(parent, 'Patriarch Ghosar', 115,
                                     gear=[Gear("Patriarch's claws"),
                                           Gear('Genestealer familiar')],
                                     static=True, unique=True)

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Ghosar, self).build_statistics())


class Orthan(Unit):
    type_name = 'Magus Orthan Trysst, Prophet of the great Patriarch'
    type_id = 'gs_magus_orthan_v1'

    def __init__(self, parent):
        super(Orthan, self).__init__(parent, 'Magus Orthan Trysst', 65,
                                     gear=[Gear('Autopistol'), Gear('Force stave'),
                                           Gear('Genestealer familiar')],
                                     static=True, unique=True)

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Orthan, self).build_statistics())


class Vorgan(Unit):
    type_name = 'Primus Vorgan Trysst, Right hand of the Patriarch'
    type_id = 'gs_primus_vorgan_v1'

    def __init__(self, parent):
        super(Vorgan, self).__init__(parent, 'Primus Vorgan Trysst', 75,
                                     gear=[Gear('Needle pistol'), Gear('Bone sword'),
                                           Gear('Blasting charges')],
                                     static=True, unique=True)


class Patriarch(Unit):
    type_name = 'Patriarch'
    type_id = 'gs_patriarch_v1'

    class Mastery(OneOf):
        def __init__(self, parent):
            super(Patriarch.Mastery, self).__init__(parent, 'Mastery level')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            else:
                return 2

    def __init__(self, parent):
        super(Patriarch, self).__init__(parent, self.type_name, 90,
                                         gear=[
                                             Gear('Patriarch\'s claws')
                                         ])
        self.psy = self.Mastery(self)
        self.familliars = Familliars(self, 'Familliars')

    def count_charges(self):
        return self.psy.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Patriarch, self).build_statistics())


class Magus(Unit):
    type_name = 'Magus'
    type_id = 'gs_magus_v1'

    class Mastery(OneOf):
        def __init__(self, parent):
            super(Magus.Mastery, self).__init__(parent, 'Mastery level')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            else:
                return 2

    class MeleeWeapon(SacredRelic, ForseStave):
        pass

    class RangedWeapon(SacredRelic, AutoPistol):
        pass

    def __init__(self, parent):
        super(Magus, self).__init__(parent, self.type_name, 40)
        self.psy = self.Mastery(self)
        self.familliars = Familliars(self, 'Familliars')
        self.mle = self.MeleeWeapon(self, name='Weapon', magus=True)
        self.rng = self.RangedWeapon(self, name='Weapon', magus=True)

    def get_unique_gear(self):
        return self.mle.get_unique() + self.rng.get_unique()

    def count_charges(self):
        return self.psy.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Magus, self).build_statistics())

    def check_rules(self):
        super(Magus, self).check_rules()
        relic_count = len(self.mle.get_unique() + self.rng.get_unique())
        if relic_count > 1:
            self.error('Only one weapon may be exchanged to Sacred Relic')


class Primus(Unit):
    type_name = 'Primus'
    type_id = 'gs_primus_v1'

    class Pistol(SacredRelic, NeedlePistol):
        pass

    class Sword(SacredRelic, Bonesword):
        pass

    class Claw(SacredRelic, RendingClaws):
        pass

    class Charger(SacredRelic, BlastingCharges):
        pass

    class Injector(SacredRelic, ToxinIngector):
        pass

    def __init__(self, parent):
        super(Primus, self).__init__(parent, self.type_name, 75)
        self.pistol = self.Pistol(self, name='Pistol')
        self.sword = self.Sword(self, name='Sword', primus=True)
        self.claw = self.Claw(self, name='Claw')
        self.charger = self.Charger(self, name='Charger')
        self.injector = self.Injector(self, name='Injector')

    def get_unique_gear(self):
        return self.pistol.get_unique() + self.sword.get_unique() + self.claw.get_unique() +\
               self.charger.get_unique() + self.injector.get_unique()

    def check_rules(self):
        super(Primus, self).check_rules()
        relic_count = len(self.get_unique_gear())
        if relic_count > 1:
            self.error('Only one weapon may be exchanged to Sacred Relic')


class Iconward(Unit):
    type_name = 'Acolyte Iconward'
    type_id = 'gs_iconward_v1'

    class Pistol(SacredRelic, AutoPistol):
        pass

    class Claw(SacredRelic, RendingClaws):
        pass

    class Charger(SacredRelic, BlastingCharges):
        pass

    class Banner(SacredRelic, SacredCultBanner):
        pass

    def __init__(self, parent):
        super(Iconward, self).__init__(parent, self.type_name, 65)
        self.pistol = self.Pistol(self, name='Pistol')
        self.claw = self.Claw(self, name='Claw')
        self.charger = self.Charger(self, name='Charger')
        self.banner = self.Banner(self, name='Banner', iconward=True)

    def get_unique_gear(self):
        return self.pistol.get_unique() + self.claw.get_unique() +\
               self.charger.get_unique() + self.banner.get_unique()

    def check_rules(self):
        super(Iconward, self).check_rules()
        relic_count = len(self.get_unique_gear())
        if relic_count > 1:
            self.error('Only one weapon may be exchanged to Sacred Relic')
