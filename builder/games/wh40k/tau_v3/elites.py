__author__ = 'Denis Romanov'
__maintainter__ = 'Ivan Truskov'

from collections import defaultdict
from builder.core2 import ListSubUnit, OneOf, OptionsList, Gear,\
    UnitList, Count, UnitDescription
from builder.games.wh40k.roster import Unit
from .armory import *


class StealthTeam(Unit):
    type_name = 'XV25 Stealth Battlesuits'
    type_id = 'stealthteam_v3'

    class Suit(ListSubUnit):
        type_name = 'Stealth Shas\'ui'
        type_id = 'shasui_v1'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(StealthTeam.Suit.Weapon, self).__init__(parent=parent, name='Weapon')
                self.burstcannon = self.variant('Burst cannon', 0)
                self.fusionblaster = self.variant('Fusion blaster', 5)

        class ShasVre(OptionsList):
            def __init__(self, parent):
                super(StealthTeam.Suit.ShasVre, self).__init__(parent=parent, name='Leader', limit=None)
                self.shasvre = self.variant('Stealth Shas\'vre', 10, gear=[])

        class Options(OptionsList):
            def __init__(self, parent):
                super(StealthTeam.Suit.Options, self).__init__(parent=parent, name='Options')
                self.homingbeacon = self.variant('Homing beacon', 10)
                self.marketlightandtargetlock = self.variant('Market light and target lock', 5, gear=[
                    Gear('Market light'), Gear('Target lock')
                ])

        def __init__(self, parent):
            super(StealthTeam.Suit, self).__init__(
                parent=parent, points=30,
                gear=[Gear('Stealth battlesuit')]
            )
            self.weapon = self.Weapon(self)
            self.support = SupportSystem(self, slots=1)
            self.shasvre = self.ShasVre(self)
            self.opt = self.Options(self)
            self.drones = Drones(self)

        def build_description(self):
            desc = super(StealthTeam.Suit, self).build_description()
            if self.shasvre.shasvre.value:
                desc.name = self.shasvre.shasvre.title
            return desc

        def check_rules(self):
            # if self.get_count() > 1:
            #     self.shasvre.shasvre.value = False
            for o in [self.opt, self.drones]:
                o.visible = o.used = self.shasvre.shasvre.value
            self.drones.check_rules()

        @ListSubUnit.count_gear
        def count_shasvre(self):
            return self.shasvre.shasvre.value

        @ListSubUnit.count_gear
        def count_blasters(self):
            return self.weapon.cur == self.weapon.fusionblaster

    def __init__(self, parent):
        super(StealthTeam, self).__init__(parent=parent)
        self.team = UnitList(self, self.Suit, min_limit=3, max_limit=6)
        self.opt = Ritual(self)

    def check_rules(self):
        shasvre = sum(u.count_shasvre() for u in self.team.units)
        if shasvre > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(shasvre))

        blaster = sum(m.count_blasters() for m in self.team.units)
        blaster_lim = int(self.get_count() / 3)
        if blaster > blaster_lim:
            self.error('Only one blaster may be taken per 3 models in suites in unit.')

    def get_count(self):
        return self.team.count

    def build_statistics(self):
        res = super(StealthTeam, self).build_statistics()
        res['Models'] += sum(u.drones.count for u in self.team.units if u.drones.used)
        return res


class TeamDarktide(Unit):
    type_name = 'Stealth Team Darktide'
    type_id = 'darktide_v3'

    def __init__(self, parent):
        super(TeamDarktide, self).__init__(parent, points=125, static=True, unique=True, gear=[
            UnitDescription("Shas'vre Ryaon", options=[
                Gear('Stealth battlesuit'), Gear('Fusion blaster')]),
            UnitDescription("Stealth Shas'ui", count=2, options=[
                Gear('Stealth battlesuit'), Gear('Burst cannon')]),
            Gear('Marker Drone')
        ])

    def build_statistics(self):
        return defaultdict(int, {'Models': 0})


class CrisisTeam(Unit):
    type_name = 'XV8 Crisis Battlesuits'
    type_id = 'crisisbattlesuitteam_v3'

    class CrisisSuit(ListSubUnit):
        type_name = 'Crisis Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent):
            super(CrisisTeam.CrisisSuit, self).__init__(
                parent=parent, points=22, name='Shas\'ui',
                gear=[Gear('Crisis battlesuit')]
            )
            self.weapon = Weapon(self, slots=3)
            self.support = SupportSystem(self, slots=3)
            self.shasvre = self.ShasVre(self)
            self.drones = Drones(self)
            self.signature = SignatureSystem(self)

        class ShasVre(OptionsList):
            def __init__(self, parent):
                super(CrisisTeam.CrisisSuit.ShasVre, self).__init__(parent=parent, name='Leader', limit=None)

                self.shasvre = self.variant('Crisis Shas\'vre', 10, gear=[])

        def check_rules(self):
            # if self.get_count() > 1:
            #     self.shasvre.shasvre.value = False
            self.drones.check_rules()
            self.signature.visible = self.signature.used = self.shasvre.shasvre.value
            free_slots = 3 - self.weapon.count_slots() - self.signature.count_slots() - self.support.count_slots()
            for s in [self.weapon, self.signature, self.support]:
                s.set_free_slots(free_slots)

        @ListSubUnit.count_gear
        def count_shasvre(self):
            return self.shasvre.shasvre.value

        @ListSubUnit.count_unique
        def get_unique_gear(self):
            return self.signature.get_unique_gear()

        def build_description(self):
            desc = super(CrisisTeam.CrisisSuit, self).build_description()
            if self.shasvre.shasvre.value:
                desc.name = self.shasvre.shasvre.title
            return desc

    def __init__(self, parent):
        super(CrisisTeam, self).__init__(parent=parent)
        self.team = UnitList(self, self.CrisisSuit, min_limit=1, max_limit=9)
        self.opt = Ritual(self)

    def get_count(self):
        return self.team.count

    def check_rules(self):
        shasvre = sum(u.count_shasvre() for u in self.team.units)
        if shasvre > 1:
            self.error('Only one Shas\'ui can be upgraded to Shas\'vre (upgraded: {0}).'.format(shasvre))

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.team.units), [])

    def build_statistics(self):
        res = super(CrisisTeam, self).build_statistics()
        res['Models'] += sum(u.drones.count for u in self.team.units)
        return res


class Bodyguards(Unit):
    type_name = 'XV8 Crisis Bodyguards'
    type_id = 'bodyguardteam_v3'

    class CrisisSuit(ListSubUnit):
        def __init__(self, parent):
            super(Bodyguards.CrisisSuit, self).__init__(
                parent=parent, points=32, name='Crisis Bodyguard',
                gear=[Gear('Crisis battlesuit')]
            )
            self.weapon = Weapon(self, slots=3)
            self.support = SupportSystem(self, slots=3)
            self.signature = SignatureSystem(self, slots=None)
            self.drones = Drones(self)

        def check_rules(self):
            self.drones.check_rules()
            free_slots = 3 - self.weapon.count_slots() - self.support.count_slots()
            for s in [self.weapon, self.support]:
                s.set_free_slots(free_slots)

        @ListSubUnit.count_unique
        def get_unique_gear(self):
            return self.signature.get_unique_gear()

    def __init__(self, parent):
        super(Bodyguards, self).__init__(parent=parent)
        self.team = UnitList(self, self.CrisisSuit, min_limit=1, max_limit=9)
        self.opt = self.Options(self, ritual_points=2)

    class Options(Ritual):
        def __init__(self, parent, ritual_points):
            super(Bodyguards.Options, self).__init__(parent, ritual_points=ritual_points)
            self.farsight = self.variant('Farsight\'s Bodyguard Team')
            self.farsight.visible = False

    def check_rules(self):
        self.team.update_range(1, 7 if self.opt.farsight.value else 9)

    def get_count(self):
        return self.team.count

    def is_farsight_guard(self):
        return self.opt.farsight.value

    def get_unique_gear(self):
        return sum((u.get_unique_gear() for u in self.team.units), [])

    def build_statistics(self):
        res = super(Bodyguards, self).build_statistics()
        res['Models'] += sum(u.drones.count for u in self.team.units)
        return res


class Ghostkeels(Unit):
    type_name = 'XV95 Ghostkeel battlesuits'
    type_id = 'ghostkeel_v3'

    class Suit(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Ghostkeels.Suit.Weapon1, self).__init__(parent, 'Weapon')
                self.variant('Fusion collider', 0)
                self.variant('Cyclic ion raker', 0)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Ghostkeels.Suit.Weapon2, self).__init__(parent, '')
                self.variant('Twin-linked flamer', 0)
                self.variant('Twin-linked burst cannon', 5)
                self.variant('Twin-linked fusion blaster', 10)

        def __init__(self, parent):
            super(Ghostkeels.Suit, self).__init__(parent, 'Ghostkeel Shas\'vre', 130, gear=[
                Gear('Ghostkeel battlesuit'), Gear('Ghostkeel electrowarfare suite'),
                Gear('Holophoton countermeasures'), Gear('MV5 Stealth Drone', count=2)
            ])
            self.Weapon1(self)
            self.Weapon2(self)
            SupportSystem(self, 2, ghostkeel=True)

    def __init__(self, parent):
        super(Ghostkeels, self).__init__(parent)
        self.models = UnitList(self, self.Suit, 1, 3)
        self.ritual = Ritual(self)

    def get_count(self):
        return self.models.count

    def build_statistics(self):
        res = super(Ghostkeels, self).build_statistics()
        res['Models'] += self.models.count * 2
        return res


class Riptides(Unit):
    type_name = 'XV104 Riptide battlesuits'
    type_id = 'xv104riptide_v3'

    class Riptide(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Riptides.Riptide.Weapon1, self).__init__(parent=parent, name='Weapon')
                self.heavyburstcannon = self.variant('Heavy burst cannon', 0)
                self.ionaccelerator = self.variant('Ion accelerator', 5)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Riptides.Riptide.Weapon2, self).__init__(parent=parent, name='')
                self.twinlinkedsmartmissilesystem = self.variant('Twin-linked smart missile system', 0)
                self.twinlinkedplasmarifle = self.variant('Twin-linked plasma rifle', 0)
                self.twinlinkedfusionblaster = self.variant('Twin-linked fusion blaster', 0)

        def __init__(self, parent):
            super(Riptides.Riptide, self).__init__(
                name='Riptide Shas\'vre',
                parent=parent, points=180,
                gear=[Gear('Riptide battlesuit'), Gear('Nova reactor'),
                      Gear('Riptide shield generator')]
            )
            self.Weapon1(self)
            self.Weapon2(self)
            self.supp = SupportSystem(self, slots=2, riptide=True)
            self.shieldedmissiledrone = Count(self, 'Shielded Missile Drone', min_limit=0, max_limit=2, points=25)

        @ListSubUnit.count_unique
        def check_array(self):
            if self.supp.earthcastepilotarray.visible and\
               self.supp.earthcastepilotarray.value:
                return [Gear('Earth Caste Pilot Array')]
            else:
                return []

    def __init__(self, parent):
        super(Riptides, self).__init__(parent)
        self.models = UnitList(self, self.Riptide, 1, 3)
        Ritual(self)

    def get_count(self):
        return self.models.count

    def build_statistics(self):
        res = super(Riptides, self).build_statistics()
        res['Models'] += sum(u.shieldedmissiledrone.cur for u in self.models.units)
        return res

    def get_unique_gear(self):
        return sum((u.check_array() for u in self.models.units), [])
