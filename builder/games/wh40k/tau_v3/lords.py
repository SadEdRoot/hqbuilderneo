__author__ = 'Ivan Truskov'

from .armory import *

from builder.core2 import Gear, OneOf, ListSubUnit, UnitList, Unit


class Stormsurges(Unit):
    type_name = 'KV128 Stormsurges'
    type_id = 'stormsurges_v3'

    class Stormsurge(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Stormsurges.Stormsurge.Weapon1, self).__init__(parent, 'Weapon')
                self.variant('Pulse blastcannon', 0)
                self.variant('Pulse driver cannon', 15)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Stormsurges.Stormsurge.Weapon2, self).__init__(parent, '')
                self.variant('Twin-linked flamer', 0)
                self.variant('Twin-linked burst cannon', 5)
                self.variant('Twin-linked airbursting fragmentation projector', 5)

        def __init__(self, parent):
            super(Stormsurges.Stormsurge, self).__init__(
                parent, 'KV128 Stormsurge', 360, gear=[
                    Gear('Twin-linked smart missile system'),
                    Gear('Cluster rocket system'),
                    Gear('Destroyer missile', count=4)
                ])
            self.Weapon1(self)
            self.Weapon2(self)
            SupportSystem(self, 3, stormsurge=True)

    def __init__(self, parent):
        super(Stormsurges, self).__init__(parent)
        self.models = UnitList(self, self.Stormsurge, 1, 3)

    def get_count(self):
        return self.models.count
