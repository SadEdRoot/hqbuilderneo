__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.section import LordsOfWarSection


class Baneblade(Unit):
    type_id = 'baneblade_v1'
    type_name = 'Baneblade'

    class Options(OptionsList):
        def __init__(self, parent):
            super(Baneblade.Options, self).__init__(parent, name='Options')
            self.variant('Hunter-killer missile', 10)
            self.sb = self.variant('Storm bolter', 5)
            self.hs = self.variant('Heavy stubber', 5)

        def check_rules(self):
            super(Baneblade.Options, self).check_rules()
            self.process_limit([self.sb, self.hs], 1)

    class Command(OptionsList):
        def __init__(self, parent):
            super(Baneblade.Command, self).__init__(parent, name='')
            self.variant('Commissariat Crew', 45)
            self.variant('Command Tank', 200)

    class Count(Count):
        @property
        def description(self):
            return [Gear('Lascannon', count=self.cur), Gear('Twin-linked heavy bolter', count=self.cur)]

    def __init__(self, parent):
        super(Baneblade, self).__init__(parent=parent, points=525, gear=[
            Gear('Autocannon'),
            Gear('Baneblade cannon'),
            Gear('Demolisher cannon'),
            Gear('Twin-linked heavy bolter'),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
        ])

        self.Count(self, 'Side sponsons', 0, 2, 50)
        self.Options(self)
        self.Command(self)


class SponsonsUnit(Unit):

    class BolterCount(Count):
        @property
        def description(self):
            return [Gear('Lascannon', count=self.cur), Gear('Twin-linked heavy bolter', count=self.cur)]

    class FlamerCount(Count):
        @property
        def description(self):
            return [Gear('Lascannon', count=self.cur), Gear('Twin-linked heavy flamer', count=self.cur)]

    def __init__(self, *args, **kwargs):
        super(SponsonsUnit, self).__init__(*args, **kwargs)
        self.hb = self.BolterCount(self, 'Sponsons with heavy bolter', 0, 2, 50)
        self.hf = self.FlamerCount(self, 'Sponsons with heavy flamer', 0, 2, 50)

    def check_rules(self):
        super(SponsonsUnit, self).check_rules()
        Count.norm_counts(0, 2, [self.hb, self.hf])


class Banehammer(SponsonsUnit):
    type_id = 'banehammer_v1'
    type_name = 'Banehammer'

    def __init__(self, parent):
        super(Banehammer, self).__init__(parent=parent, points=410, gear=[
            Gear('Tremor cannon'),
            Gear('Twin-linked heavy bolter'),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
        ])
        Baneblade.Options(self)


class Banesword(SponsonsUnit):
    type_id = 'banesword_v1'
    type_name = 'Banesword'

    def __init__(self, parent):
        super(Banesword, self).__init__(parent=parent, points=430, gear=[
            Gear('Quake cannon'),
            Gear('Twin-linked heavy bolter'),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
        ])
        Baneblade.Options(self)
        Baneblade.Command(self)


class Doomhammer(SponsonsUnit):
    type_id = 'doomhammer_v1'
    type_name = 'Doomhammer'

    def __init__(self, parent):
        super(Doomhammer, self).__init__(parent=parent, points=420, gear=[
            Gear('Magma cannon'),
            Gear('Twin-linked heavy bolter'),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
        ])
        Baneblade.Options(self)


class Hellhammer(SponsonsUnit):
    type_id = 'hellhammer_v1'
    type_name = 'Hellhammer'

    def __init__(self, parent):
        super(Hellhammer, self).__init__(parent=parent, points=540, gear=[
            Gear('Hellhammer cannon'),
            Gear('Autocannon'),
            Gear('Demolisher cannon'),
            Gear('Twin-linked heavy bolter'),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
        ])
        Baneblade.Options(self)


class Shadowsword(Unit):
    type_id = 'shadowsword_v1'
    type_name = 'Shadowsword'

    class AddWeapon(OptionsList):
        def __init__(self, parent):
            super(Shadowsword.AddWeapon, self).__init__(parent, name='')
            self.hb = self.variant('Twin-linked heavy bolter', 10)
            self.hf = self.variant('Heavy flamer', 10)

        def check_rules(self):
            super(Shadowsword.AddWeapon, self).check_rules()
            self.process_limit([self.hb, self.hf], 1)

    class Targeter(OptionsList):
        def __init__(self, parent):
            super(Shadowsword.Targeter, self).__init__(parent, name='')
            self.up = self.variant('Targeters', 0, gear=[])

    class BolterCount(Count):
        @property
        def description(self):
            return [Gear('Targeter' if self.parent.tg.up.value else 'Lascannon', count=self.cur),
                    Gear('Twin-linked heavy bolter', count=self.cur)]

    class FlamerCount(Count):
        @property
        def description(self):
            return [Gear('Targeter' if self.parent.tg.up.value else 'Lascannon', count=self.cur),
                    Gear('Twin-linked heavy flamer', count=self.cur)]

    def check_rules(self):
        super(Shadowsword, self).check_rules()
        Count.norm_counts(0, 2, [self.hb, self.hf])

    def __init__(self, parent):
        super(Shadowsword, self).__init__(parent=parent, points=455, gear=[
            Gear('Volcano cannon'),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
        ])
        self.hb = self.BolterCount(self, 'Sponsons with heavy bolter', 0, 2, 50)
        self.hf = self.FlamerCount(self, 'Sponsons with heavy flamer', 0, 2, 50)

        Baneblade.Options(self)
        self.AddWeapon(self)
        self.tg = self.Targeter(self)
        Baneblade.Command(self)


class Stormlord(SponsonsUnit):
    type_id = 'Stormlord_v1'
    type_name = 'Stormlord'

    def __init__(self, parent):
        super(Stormlord, self).__init__(parent=parent, points=480, gear=[
            Gear('Vulcan mega-bolter'),
            Gear('Heavy stubber', count=2),
            Gear('Twin-linked heavy bolter'),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
        ])
        Baneblade.Options(self)
        Shadowsword.AddWeapon(self)


class Stormsword(SponsonsUnit):
    type_id = 'stormsword_v1'
    type_name = 'Stormsword'

    class Command(OptionsList):
        def __init__(self, parent):
            super(Stormsword.Command, self).__init__(parent, name='')
            self.variant('Command Tank', 200)

    def __init__(self, parent):
        super(Stormsword, self).__init__(parent=parent, points=485, gear=[
            Gear('Stormsword siege cannon'),
            Gear('Twin-linked heavy bolter'),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
        ])
        Baneblade.Options(self)
        self.Command(self)


class Fortress(Unit):
    type_id = 'fortress_v1'
    type_name = 'Fortress of Arrogance'

    def __init__(self, parent):
        super(Fortress, self).__init__(parent=parent, points=940, gear=[
            Gear('Autocannon'),
            Gear('Baneblade cannon'),
            Gear('Demolisher cannon'),
            Gear('Hunter-killer missile'),
            Gear('Lascannon', count=2),
            Gear('Storm bolter'),
            Gear('Twin-linked heavy bolter', count=3),
            Gear('Searchlight'),
            Gear('Smoke launchers'),
            UnitDescription('Commissar Yarrick', options=[
                Gear('Carapace armour'), Gear('Storm bolter'), Gear('Battle claw'), Gear('Close combat weapon'),
                Gear('Bolt pistol'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Force field'), Gear('Bale eye')
            ])
        ])

    def get_unique(self):
        return 'Commissar Yarrick'


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent=parent)
        UnitType(self, Baneblade)
        UnitType(self, Banehammer)
        UnitType(self, Banesword)
        UnitType(self, Doomhammer)
        UnitType(self, Hellhammer)
        UnitType(self, Shadowsword)
        UnitType(self, Stormlord)
        UnitType(self, Stormsword)
        UnitType(self, Fortress)


unit_types = [
    Baneblade,
    Banehammer,
    Banesword,
    Doomhammer,
    Hellhammer,
    Shadowsword,
    Stormlord,
    Stormsword,
    Fortress
]
