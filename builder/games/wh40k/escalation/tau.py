__author__ = 'Denis Romanow'

from builder.core2 import *
from builder.games.wh40k.roster import LordsOfWarSection


class TigerShark(Unit):
    type_id = 'tiger_shark_v1'
    type_name = 'Tiger Shark'

    def __init__(self, parent):
        super(TigerShark, self) .__init__(parent=parent, points=520, gear=[
            Gear('Twin-linked ion cannon'),
            Gear('Twin-linked missile pod'),
            Gear('Burst cannon', count=2),
        ])
        from builder.games.wh40k.obsolete.tau_v2.armory import Vehicle
        self.Weapon(self)
        Vehicle(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TigerShark.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Gun drones', 0, gear=[Gear('Gun drones', count=14), Gear('Drone Rack')])
            self.variant('Seeker missiles', 0, gear=Gear('Seeker missile', count=6))


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent=parent)
        UnitType(self, TigerShark)
