__author__ = 'Denis Romanow'

from builder.core2 import *
from .heavy import Squadron
from .armory import Vehicle
from builder.games.wh40k.roster import Unit


class ScoutSentinel(ListSubUnit):
    type_name = 'Scout Sentinel'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Scout-Sentinel-Squadron')

    def __init__(self, parent):
        super(ScoutSentinel, self).__init__(parent=parent, points=35)
        self.Weapon(self)
        Vehicle(self, sentinel=True)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScoutSentinel.Weapon, self).__init__(parent=parent, name='Weapon')
            self.multilaser = self.variant('Multi-laser', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.autocannon = self.variant('Autocannon', 5)
            self.missilelauncher = self.variant('Missile launcher', 5)
            self.lascannon = self.variant('Lascannon', 10)


class ArmouredSentinel(ListSubUnit):
    type_name = 'Armoured Sentinel'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Armoured-Sentinel-Squadron')

    def __init__(self, parent):
        super(ArmouredSentinel, self).__init__(parent=parent, points=40)
        self.Weapon(self)
        Vehicle(self, sentinel=True)

    class Weapon(ScoutSentinel.Weapon):
        def __init__(self, parent):
            super(ArmouredSentinel.Weapon, self).__init__(parent=parent)
            self.plasmacannon = self.variant('Plasma cannon', 10)


class Hellhound(ListSubUnit):
    type_name = 'Hellhound'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Hellhound-Squadron')

    def __init__(self, parent):
        super(Hellhound, self).__init__(parent=parent)
        self.type = self.Type(self)
        self.Weapon(self)
        Vehicle(self)

    class Type(OneOf):
        def __init__(self, parent):
            super(Hellhound.Type, self).__init__(parent=parent, name='Type')
            self.hellhound = self.variant('Hellhound', 125, gear=[Gear('Inferno cannon')])
            self.devildog = self.variant('Devil dog', 135, gear=[Gear('Melta cannon')])
            self.banewolf = self.variant('Banewolf', 130, gear=[Gear('Chem cannon')])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Hellhound.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.multymelta = self.variant('Multy melta', 10)

    def build_description(self):
        desc = super(Hellhound, self).build_description()
        desc.name = self.type.cur.title
        return desc


class Valkyrie(ListSubUnit):
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Valkyrie-Squadron')

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Multi-laser', 0)
            self.variant('Lascannon', 10)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Valkyrie.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Hellstrike missiles', 0, gear=[Gear('Hellstrike missile', count=2)])
            self.variant('Multiple rocket pods', 10, gear=[Gear('Multiple rocket pod', count=2)])

    class Weapon3(OptionsList):
        def __init__(self, parent):
            super(Valkyrie.Weapon3, self).__init__(parent=parent, name='Sponsons')
            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])

    def __init__(self, parent):
        super(Valkyrie, self).__init__(parent=parent, points=125, name="Valkyrie", gear=[Gear('Extra armour'),
                                                                                         Gear('Searchlight')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)


class Vendetta(ListSubUnit):
    type_name = 'Vendetta'
    type_id = 'vendetta_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Vendetta-Squadron')

    def __init__(self, parent):
        super(Vendetta, self).__init__(parent=parent, points=170, gear=[Gear('Searchlight'), Gear('Extra armour'),
                                                                        Gear('Twin-linked lascannon')])
        self.Weapon(self)
        Valkyrie.Weapon3(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Vendetta.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Two twin-linked lascannons', 0, gear=[Gear('Twin-linked lascannon', count=2)])
            self.variant('Two hellfury missiles', 0, gear=[Gear('Hellfury missile', count=2)])


class ScoutSentinelSquad(Squadron):
    type_name = 'Scout Sentinel Squadron'
    type_id = 'scout_sentinel_squad_v1'
    unit_class = ScoutSentinel


class ArmouredSentinelSquad(Squadron):
    type_name = 'Armoured Sentinel Squadron'
    type_id = 'armoured_sentinel_squad_v1'
    unit_class = ArmouredSentinel


class HellhoundSquadron(Squadron):
    type_name = 'Hellhound Squadron'
    type_id = 'hellhound_squadron_v1'
    unit_class = Hellhound


class Valkyries(Squadron):
    type_name = 'Valkyrie Squadron'

    type_id = 'valkyrie_squadron_v1'
    unit_class = Valkyrie
    unit_max = 4


class VendettaSquadron(Squadron):
    type_name = 'Vendetta Squadron'
    type_id = 'vendetta_gunship_squadron_v1'
    unit_class = Vendetta
    unit_max = 4


class Riders(Unit):
    type_name = 'Rough Rider Squad'
    type_id = 'roughridersquad_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Rough-Riders')

    class Sergeant(Unit):
        type_name = 'Rough Rider Sergeant'

        def __init__(self, parent):
            super(Riders.Sergeant, self).__init__(parent=parent, points=55 - 4 * 11, gear=[
                Gear('Flak armour'),
                Gear('Hunting lance'),
                Gear('Frag grenades'),
                Gear('Krak grenades')
            ])
            self.Weapon(self, name='Weapon', ccw=True)
            self.Weapon(self, name='', pistol=True)

        class Weapon(OneOf):
            def __init__(self, parent, name, pistol=False, ccw=False):
                super(Riders.Sergeant.Weapon, self).__init__(parent=parent, name=name)
                self.closecombatweapon = ccw and self.variant('Close combat weapon', 0)
                self.laspistol = pistol and self.variant('Laspistol', 0)
                self.variant('Power weapon', 15)
                self.variant('Plasma pistol', 15)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Riders.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.variant('Melta bombs', 5)

    model = UnitDescription('Rough Rider', points=11, options=[
        Gear('Flak armour'),
        Gear('Frag grenades'),
        Gear('Krak grenades'),
        Gear('Close combat weapon'),
        Gear('Laspistol'),
    ])

    class Rider(Count):
        @property
        def description(self):
            return Riders.model.clone().add(Gear('Hunting lance')).set_count(self.cur -
                                                                             sum(o.cur for o in self.parent.spec))

    def __init__(self, parent):
        super(Riders, self).__init__(parent=parent)
        SubUnit(self, self.Sergeant(None))
        self.models = self.Rider(self, 'Rough Rider', 4, 9, 11)
        self.spec = [
            Count(self, name=g['name'], min_limit=0, max_limit=2, points=g['points'],
                  gear=Riders.model.clone().add(Gear(g['name'])).add_points(g['points']))
            for g in [
                dict(name='Flamer', points=5),
                dict(name='Grenade launcher', points=5),
                dict(name='Meltagun', points=10),
                dict(name='Plasma gun', points=15),
            ]
        ]

    def check_rules(self):
        super(Riders, self).check_rules()
        Count.norm_counts(0, 2, self.spec)
