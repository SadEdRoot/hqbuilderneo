__author__ = 'Denis Romanow'
from collections import defaultdict

from builder.games.wh40k.imperial_armour.volume1.troops import SabreBattery
from builder.core2 import *
from .armory import Transport, SpecWeapon, HeavyWeapon, Melee, Ranged
from builder.games.wh40k.roster import Unit


class BaseVeteran(Unit):

    base_unit = None

    def build_description(self):
        desc = super(BaseVeteran, self).build_description()
        if self.base_unit.vet_opt.kg.value:
            desc.add(Gear('Krak grenades'))
        if self.base_unit.vet_opt.gr.value:
            desc.add(Gear('Carapace armour'))
        else:
            desc.add(Gear('Flak armour'))
        return desc


class Veteran(BaseVeteran, ListSubUnit):
    type_name = 'Veteran'

    class BaseWeapon(OneOf):
        def __init__(self, parent, name):
            super(Veteran.BaseWeapon, self).__init__(parent, name=name)
            self.variant('Lasgun', 0)
            self.variant('Shotgun', 0)
            self.hf = self.variant('Heavy flamer', 10)

    class Weapon(SpecWeapon, BaseWeapon):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Veteran.Options, self).__init__(parent, name='Options')
            self.vox = self.variant('Vox-caster', 5)

    class Charge(OptionsList):
        def __init__(self, parent):
            super(Veteran.Charge, self).__init__(parent, name='')
            self.dem = self.variant('Demolition charge')

    def __init__(self, parent):
        super(Veteran, self).__init__(parent, gear=[Gear('Frag grenades')])
        self.wep = self.Weapon(self, name='Weapon')
        self.opt = self.Options(self)
        self.ch = self.Charge(self)
        self.base_unit = self.root_unit

    def check_rules(self):
        super(Veteran, self).check_rules()
        self.ch.visible = self.ch.used = self.base_unit.vet_opt.dem.value
        for o in [self.wep.hf] + self.wep.spec:
            o.active = not self.opt.any
        self.opt.visible = self.opt.used = self.wep.cur not in [self.wep.hf] + self.wep.spec

    @ListSubUnit.count_gear
    def count_vox(self):
        return self.opt.vox.value

    @ListSubUnit.count_gear
    def count_flame(self):
        return self.wep.hf == self.wep.cur

    @ListSubUnit.count_gear
    def count_dem(self):
        return self.ch.dem.value

    @ListSubUnit.count_gear
    def count_spec(self):
        return self.wep.cur in self.wep.spec


class VeteranWeaponTeam(BaseVeteran):
    type_name = 'Veteran Weapon Team'

    def __init__(self, parent, root_unit):
        super(VeteranWeaponTeam, self).__init__(parent, gear=[Gear('Frag grenades'), Gear('Lasgun')])
        self.base_unit = root_unit
        self.wep = HeavyWeapon(self, name='Weapon')


class VeteranCommander(BaseVeteran):
    type_name = 'Veteran Sergeant'

    def __init__(self, parent, root_unit):
        super(VeteranCommander, self).__init__(parent, gear=[Gear('Frag grenades')])
        self.base_unit = root_unit
        Melee(self, 'Weapon')
        Ranged(self, '')


class VeteranSquad(Unit):
    type_name = 'Veterans'
    type_id = 'veteran_squad_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Veterans')

    def __init__(self, parent):
        super(VeteranSquad, self).__init__(parent, points=60)
        self.commander = SubUnit(self, VeteranCommander(self, root_unit=self))
        self.chars = self.Commanders(self)
        self.veterans = UnitList(self, Veteran, 1, 9, start_value=9)
        self.vet_opt = self.VeteranOptions(self)
        self.members = self.Members(self)
        self.transport = Transport(self)

    class Commanders(OptionsList):
        def __init__(self, parent):
            super(VeteranSquad.Commanders, self).__init__(parent, name='')
            self.harker = self.variant(
                'Sergeant Harker', 55, gear=[UnitDescription(
                    'Sergeant Harker', points=55, options=[
                        Gear('Flak armour'),
                        Gear('Close combat weapon'),
                        Gear('Frag grenades'),
                        Gear('Krak grenades'),
                        Gear('Payback'),
                    ]
                )])

        def check_rules(self):
            super(VeteranSquad.Commanders, self).check_rules()
            self.harker.visible = self.harker.used = not self.roster.is_cadia()
            self.harker.value = self.harker.value and not self.roster.is_cadia()
            self.parent.commander.visible = self.parent.commander.used = not self.any

    class Members(OptionalSubUnit):
        def __init__(self, parent):
            super(VeteranSquad.Members, self).__init__(parent, name='')
            self.heavy = SubUnit(self, VeteranWeaponTeam(None, parent))

    class VeteranOptions(OptionsList):
        def __init__(self, parent):
            super(VeteranSquad.VeteranOptions, self).__init__(parent, name='Veteran options')
            self.kg = self.variant('Krak grenades', 10, gear=[])
            self.gr = self.variant('Grenadiers', 15, gear=[])
            self.sent = self.variant('Forward Sentries', 10, gear=[Gear('Camo gear'), Gear('Snare mines')])
            self.dem = self.variant('Demolitions', 30, gear=[Gear('Melta bombs')])

        def check_rules(self):
            super(VeteranSquad.VeteranOptions, self).check_rules()

    def check_rules(self):
        super(VeteranSquad, self).check_rules()
        if sum(u.count_vox() for u in self.veterans.units) > 1:
            self.error('Only one veteran may have vox-caster')
        if sum(u.count_dem() for u in self.veterans.units) != 1 and self.vet_opt.dem.value:
            self.error('One veteran must take demolition charge')
        flame = sum(u.count_flame() for u in self.veterans.units)
        if flame > 1:
            self.error('Only one veteran may have heavy flamer')
        spec_limit = 3 - (1 if flame else 0)
        if sum(u.count_spec() for u in self.veterans.units) > spec_limit:
            self.error('Only {} veterans may take special weapon'.format(spec_limit))
        if self.chars.any and self.vet_opt.gr.value:
            self.error('If a squad includes Sergeant Harker it may not take the Grenadiers doctrine')
        if self.members.heavy.used and self.veterans.count != 7:
            self.error('Veterans Squad must include 1 Veteran Weapon Team and {} Veterans'.format(7))
        elif not self.members.heavy.used and self.veterans.count != 9:
            self.error('Veterans Squad must include {} Veterans'.format(9))

    def get_count(self):
        return 1 + self.veterans.count + self.members.count

    def get_unique_gear(self):
        return self.chars.description

    def build_statistics(self):
        return self.note_transport(super(VeteranSquad, self).build_statistics())


class PlatoonCommander(Unit):
    type_name = 'Platoon Commander'

    class SpecIssues(OptionsList):
        def __init__(self, parent):
            super(PlatoonCommander.SpecIssues, self).__init__(parent, name='Options')
            self.variant('Melta bombs', 5)

    def __init__(self, parent):
        super(PlatoonCommander, self).__init__(parent, gear=[Gear('Flak armour'), Gear('Frag grenades')])
        Melee(self, 'Weapon')
        Ranged(self, '')
        self.SpecIssues(self)


class Guardsman(ListSubUnit):
    type_name = 'Guardsman'

    class BaseWeapon(OneOf):
        def __init__(self, parent, name):
            super(Guardsman.BaseWeapon, self).__init__(parent, name=name)
            self.variant('Lasgun', 0)
            self.variant('Laspistol and close combat weapon', 0, gear=[Gear('Laspistol'), Gear('Close combat weapon')])
            self.hf = self.variant('Heavy flamer', 10)

    class Weapon(SpecWeapon, BaseWeapon):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Guardsman.Options, self).__init__(parent, name='Options', limit=1)
            self.vox = self.variant('Vox-caster', 5)
            self.banner = self.variant('Platoon standard', 10)
            self.medic = self.variant('Medi-pack', 15)

    def __init__(self, parent):
        super(Guardsman, self).__init__(parent, gear=[Gear('Flak armour'), Gear('Frag grenades')])
        self.wep = self.Weapon(self, name='Weapon')
        self.opt = self.Options(self)
        self.base_unit = self.root_unit

    def check_rules(self):
        super(Guardsman, self).check_rules()
        for o in [self.wep.hf] + self.wep.spec:
            o.active = not self.opt.any
        self.opt.visible = self.opt.used = self.wep.cur not in [self.wep.hf] + self.wep.spec

    @ListSubUnit.count_gear
    def count_vox(self):
        return self.opt.vox.value

    @ListSubUnit.count_gear
    def count_flame(self):
        return self.wep.hf == self.wep.cur

    @ListSubUnit.count_gear
    def count_banner(self):
        return self.opt.banner.value

    @ListSubUnit.count_gear
    def count_medic(self):
        return self.opt.medic.value


class HeavyWeaponTeam(Unit):
    type_name = 'Heavy Weapon Team'

    def __init__(self, parent, root_unit):
        super(HeavyWeaponTeam, self).__init__(parent, gear=[Gear('Frag grenades'), Gear('Flak armour'), Gear('Lasgun')])
        self.base_unit = root_unit
        self.wep = HeavyWeapon(self, name='Weapon')


class PlatoonCommandSquad(Unit):
    type_name = 'Platoon Command Squad'
    type_id = 'platoon_command_squad_v1'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Platoon-Command-Squad')

    def __init__(self, parent):
        super(PlatoonCommandSquad, self).__init__(parent, points=30)
        self.commander = SubUnit(self, PlatoonCommander(self))
        self.veterans = UnitList(self, Guardsman, 2, 4, start_value=4)
        self.members = self.Members(self)
        self.vet_opt = self.Options(self, name='Options')
        self.transport = Transport(self)

    class Members(OptionalSubUnit):
        def __init__(self, parent):
            super(PlatoonCommandSquad.Members, self).__init__(parent, name='', limit=1)
            self.heavy = SubUnit(self, HeavyWeaponTeam(None, parent))

    class Options(OptionsList):
        def __init__(self, parent, name):
            super(PlatoonCommandSquad.Options, self).__init__(parent, name=name)
            self.variant('Krak grenades', 1, per_model=True)

        @property
        def points(self):
            return super(PlatoonCommandSquad.Options, self).points * self.parent.count_members()

    def check_rules(self):
        super(PlatoonCommandSquad, self).check_rules()
        if sum(u.count_banner() for u in self.veterans.units) > 1:
            self.error('Only one guardsman may have platoon standard')
        if sum(u.count_medic() for u in self.veterans.units) > 1:
            self.error('Only one guardsman may have medi-pack')
        if sum(u.count_vox() for u in self.veterans.units) > 1:
            self.error('Only one guardsman may have vox-caster')
        if sum(u.count_flame() for u in self.veterans.units) > 1:
            self.error('Only one guardsman may have heavy flamer')
        if self.members.heavy.used and self.veterans.count != 2:
            self.error('Platoon Command Squad must include one Heavy Weapon Team and two Guardsman')
        elif not self.members.heavy.used and self.veterans.count != 4:
            self.error('Platoon Command Squad must include four Guardsman')

    def count_members(self):
        return 1 + self.veterans.count + self.members.count


class InfantrySquad(ListSubUnit):
    type_name = 'Infantry Squad'
    model = UnitDescription('Guardsman', options=[Gear('Frag grenades'), Gear('Flak armour')])
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Infantry-Squad')

    class Sergeant(Unit):
        type_name = 'Sergeant'

        class Melee(OneOf):
            def __init__(self, parent, name):
                super(InfantrySquad.Sergeant.Melee, self).__init__(parent, name=name)
                self.variant('Close combat weapon', 0)
                self.variant('Power weapon', 15)

        def __init__(self, parent):
            super(InfantrySquad.Sergeant, self).__init__(parent, gear=[Gear('Flak armour'), Gear('Frag grenades')])
            self.Melee(self, 'Weapon')
            Ranged(self, '')
            PlatoonCommander.SpecIssues(self)

    class SpecWeapon(OptionsList):
        def __init__(self, parent, name):
            super(InfantrySquad.SpecWeapon, self).__init__(parent, name=name)
            self.spec = [
                self.variant(
                    o['name'], o['points'],
                    gear=InfantrySquad.model.clone().add(Gear(o['name'])).add_points(o['points']))
                for o in SpecWeapon.weapon
            ]
            self.variant(
                'Vox-caster', 5,
                gear=InfantrySquad.model.clone().add(Gear('Vox-caster')).add_points(5).add(Gear('Lasgun'))
            )

        def check_rules(self):
            super(InfantrySquad.SpecWeapon, self).check_rules()
            self.process_limit(self.spec, 1)

        @property
        def description(self):
            desk = super(InfantrySquad.SpecWeapon, self).description
            count = 9 - self.count - 2 * self.parent.members.count
            return desk + [InfantrySquad.model.clone().add(Gear('Lasgun')).set_count(count)]

    def __init__(self, parent):
        super(InfantrySquad, self).__init__(parent, points=50, per_model=False)
        SubUnit(self, self.Sergeant(None))
        self.spec = self.SpecWeapon(self, name='Options')
        self.members = PlatoonCommandSquad.Members(self)
        self.vet_opt = PlatoonCommandSquad.Options(self, name='')
        self.transport = Transport(self)

    def count_members(self):
        return 9 if self.members.any else 10

    @ListSubUnit.count_gear
    def has_transport(self):
        return self.transport.any


class HeavySquad(ListSubUnit):
    type_name = 'Heavy Weapon Squad'
    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Heavy-Weapons-Squad')

    model = UnitDescription('Heavy Weapon Team', options=[Gear('Frag grenades'), Gear('Flak armour'), Gear('Lasgun')])

    class ML(Count):
        @property
        def description(self):
            return [
                HeavySquad.model.clone().add(Gear('Missile launcher'))
                .add_points(15).set_count(self.cur - self.parent.flakk.cur)
            ]

    def __init__(self, parent):
        super(HeavySquad, self).__init__(parent, points=45, per_model=False)
        self.heavy = [
            Count(self, 'Mortar', 0, 3, 5, gear=self.model.clone().add(Gear('Mortar')).add_points(5)),
            Count(self, 'Autocannon', 0, 3, 10, gear=self.model.clone().add(Gear('Autocannon')).add_points(10)),
            Count(self, 'Heavy bolter', 0, 3, 10, gear=self.model.clone().add(Gear('Heavy bolter')).add_points(10)),
        ]
        self.ml = self.ML(self, 'Missile launcher', 0, 3, 15)
        self.heavy.append(self.ml)
        self.flakk = Count(self, 'Flakk missiles', 0, 3, 10,
                           gear=self.model.clone().add(Gear('Missile launcher')).add_points(15)
                           .add(Gear('Flakk missiles')).add_points(10))
        self.heavy.append(Count(self, 'Lascannon', 0, 3, 20,
                                gear=self.model.clone().add(Gear('Lascannon')).add_points(20)))

        self.vet_opt = PlatoonCommandSquad.Options(self, name='')

    def check_rules(self):
        super(HeavySquad, self).check_rules()
        if sum(o.cur for o in self.heavy) != 3 and self.root_unit.opt.heavy.value:
            self.error('Each Heavy Weapons Team must take one item from the Heavy Weapons list')
        Count.norm_counts(0, 3, self.heavy)
        self.flakk.max = self.ml.cur

    def build_description(self):
        desc = super(HeavySquad, self).build_description()
        desc.add(self.model.clone().set_count(3 - sum(o.cur for o in self.heavy)))
        return desc

    @staticmethod
    def count_members():
        return 3


class SpecialSquad(ListSubUnit):
    type_name = 'Special Weapon Squad'

    model = UnitDescription('Guardsman', options=[Gear('Frag grenades'), Gear('Flak armour')])

    class ML(Count):
        @property
        def description(self):
            return [
                HeavySquad.model.clone().add(Gear('Missile launcher'))
                .add_points(15).set_count(self.cur - self.parent.flakk.cur)
            ]

    def __init__(self, parent):
        super(SpecialSquad, self).__init__(parent, points=30, per_model=False)
        self.spec = [
            Count(self, o['name'], 0, 3, o['points'],
                  gear=InfantrySquad.model.clone().add(Gear(o['name'])).add_points(o['points']))
            for o in SpecWeapon.weapon + [dict(name='Demolition charge', points=20)]
        ]
        self.vet_opt = PlatoonCommandSquad.Options(self, name='')

    def check_rules(self):
        super(SpecialSquad, self).check_rules()
        if sum(o.cur for o in self.spec) != 3 and self.root_unit.opt.spec.value:
            self.error('Three Guardsmen must replace their lasgun with one item from the Special Weapons list')
        Count.norm_counts(0, 3, self.spec)

    def build_description(self):
        desc = super(SpecialSquad, self).build_description()
        desc.add(self.model.clone().add(Gear('Lasgun')).set_count(6 - sum(o.cur for o in self.spec)))
        return desc

    @staticmethod
    def count_members():
        return 6


class ConscriptSquad(Unit):
    type_name = 'Conscripts'

    wikilink = Unit.template_link.format(codex_name='astra-militarum', unit_name='Conscripts')

    def __init__(self, parent):
        super(ConscriptSquad, self).__init__(parent)
        self.conscript = Count(
            self, 'Conscript', 20, 50, 3,
            gear=UnitDescription('Conscript', options=[Gear('Frag grenades'), Gear('Flak armour'), Gear('Lasgun')])
        )

    def count_members(self):
        return self.conscript.cur


class InfantryPlatoon(Unit):
    type_name = 'Infantry Platoon'
    type_id = 'infantry_platoon_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(InfantryPlatoon.Options, self).__init__(parent, name='Squads', used=False)
            self.heavy = self.variant('Heavy Weapon Team')
            self.sabre = self.variant('Sabre Weapons Battery')
            self.spec = self.variant('Special Weapon Team')
            self.cons = self.variant('Conscripts')

    def __init__(self, parent):
        super(InfantryPlatoon, self).__init__(parent)
        self.command = SubUnit(self, PlatoonCommandSquad(None))
        self.infantry = UnitList(self, InfantrySquad, 2, 5)
        self.opt = self.Options(self)
        self.heavy = UnitList(self, HeavySquad, 1, 5)
        self.sabre = UnitList(self, SabreBattery, 1, 5)
        self.spec = UnitList(self, SpecialSquad, 1, 3)
        self.cons = SubUnit(self, ConscriptSquad(None))

    def transport_count(self):
        return sum(u.has_transport() for u in self.infantry.units) +\
            self.command.unit.transport.any

    def check_rules(self):
        super(InfantryPlatoon, self).check_rules()
        try:
            sabre_flag = self.root.imperial_armour_on
        except Exception:
            sabre_flag = False
        self.opt.sabre.visible = self.opt.sabre.used = sabre_flag
        self.sabre.used = self.sabre.visible = sabre_flag and self.opt.sabre.value
        self.heavy.used = self.heavy.visible = self.opt.heavy.value
        self.spec.used = self.spec.visible = self.opt.spec.value
        self.cons.used = self.cons.visible = self.opt.cons.value
        if self.sabre.used and self.heavy.used:
            if self.sabre.count + self.heavy.count > 5:
                self.error('Number of Heavy Weapons Squads or Sabre Batteris should not be more then 5')

    def count_members(self):
        counts = self.command.unit.count_members()
        if self.opt.cons.value:
            counts += self.cons.unit.count_members()

        counts += sum((u.count_members() * u.count for u in self.infantry.units))
        if self.opt.heavy.value:
            counts += sum((u.count_members() * u.count for u in self.heavy.units))
        if self.opt.sabre.value:
            counts += sum((u.count_members() * u.count for u in self.sabre.units))
        if self.opt.spec.value:
            counts += sum((u.count_members() * u.count for u in self.spec.units))

        return counts

    def note_units(self, statistic):
        counts = self.command.unit.count
        if self.opt.cons.value:
            counts += 1

        counts += sum((u.count for u in self.infantry.units))
        if self.opt.heavy.value:
            counts += sum((u.count for u in self.heavy.units))
        if self.opt.spec.value:
            counts += sum((u.count for u in self.spec.units))

        if counts:
            statistic['Units'] = counts
        return statistic

    def note_transport(self, statistic):
        count = self.transport_count()
        if count:
            statistic['Models'] += count
            statistic['Units'] += count
        return statistic

    def build_statistics(self):
        return self.note_transport(self.note_units(defaultdict(int, {'Models': self.count_members()})))
