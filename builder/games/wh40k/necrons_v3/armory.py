from builder.core2 import OptionsList, OneOf

__author__ = 'Ivan Truskov'


class Ranged(OptionsList):
    def __init__(self, parent):
        super(Ranged, self).__init__(parent, 'Ranged weapon', limit=1)
        self.variant('Gauntlet of fire', 10)
        self.variant('Tachyon arrow', 25)


class Melee(OneOf):
    '''
    @note Staff of light is also included.
    For cryptecs it will simply be in their gear
    '''
    def __init__(self, parent, overlord=False):
        super(Melee, self).__init__(parent, 'Melee weapon')
        self.variant('Staff of light', 0)
        self.variant('Hyperphase sword', 0)
        self.variant('Voidblade', 0)
        self.variant('Warscythe', 20)
        if overlord and self.parent.roster.supplement == 'mephrit':
            self.edge = self.variant('Edge of Eternity', 20)
        else:
            self.edge = None

    def get_unique(self):
        if self.cur == self.edge:
            return self.description
        return []


class Technoarcana(OptionsList):
    def __init__(self, parent, cryptek=False):
        super(Technoarcana, self).__init__(parent, 'Technoarcana')
        self.variant('Mindshackle scarabs', 10)
        self.variant('Phylactery', 15)
        if not cryptek:
            self.variant('Resurrection orb', 25)
        self.variant('Phase shifter', 25)


class Artefacts(OptionsList):
    def __init__(self, parent, staff=None, cryptek=False):
        super(Artefacts, self).__init__(
            parent, 'Artefacts of the Aeons', limit=1)
        self.solar = self.variant('The Solar  Staff', 15)
        self.staff = staff
        self.variant('The Veil of Darkness', 25)
        self.variant('The Gauntlet of the Conflagator', 30)
        if not cryptek:
            self.reaper = self.variant('Voidreaper', 30)
        self.variant('The Nightmare Shroud', 35)
        if not cryptek:
            self.variant('The Orb of Eternity', 40)

    def check_rules(self):
        super(Artefacts, self).check_rules()
        if self.staff is not None:
            self.staff.used = self.staff.visible =\
                not(self.solar.value or self.reaper.value)

    def get_unique(self):
        return self.description


class Relics(OptionsList):
    def __init__(self, parent, cryptek=False):
        super(Relics, self).__init__(parent, 'Relics of the War in Heaven')
        if cryptek:
            self.variant('The God Shackle', 10)
        self.variant('Solar Thermasite', 25)
