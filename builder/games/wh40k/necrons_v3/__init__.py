__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'
__summary__ = ['Codex Necrons 7th Edition', 'Shield of Baal: Exterminatus']


from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kKillTeam,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, LordsOfWarSection, UnitType, Detachment,\
    FlyerSection
from builder.games.wh40k.fortifications import Fort
from .hq import Overlord, Lord, Cryptek, DestroyerLord, CommandBarge,\
    Illuminor, Orikan, Nemesor, Vargard, Anrakyr, Trazyn
from .elites import Lychguard, Deathmarks, FlayedOnes, Praetorians,\
    Stalkers, Nightbringer, Deciever
from .troops import Warriors, Immortals
from .fast import TombBlades, Destroyers, NightScythe, GhostArk, Wraiths,\
    Scarabs, NightScythes
from .heavy import HeavyDestroyers, Spyders, DoomScythe,\
    AnnihilationBarge, DoomsdayArk, TranscendentCtan, Monolith,\
    DoomScythes
from .lords import TesseractVault, Obelisk, Imotekh
from .formations import *


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.nemesor = UnitType(self, Nemesor)
        self.vargard = UnitType(self, Vargard)
        UnitType(self, Illuminor)
        UnitType(self, Orikan)
        self.anrakyr = UnitType(self, Anrakyr)
        self.trazin = UnitType(self, Trazyn)
        self.overlord = UnitType(self, Overlord)
        UnitType(self, DestroyerLord)
        UnitType(self, Lord)
        UnitType(self, Cryptek)
        UnitType(self, CommandBarge)


class HQ(BaseHQ):
    pass


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.deathmarks = UnitType(self, Deathmarks)
        self.lichguards = UnitType(self, Lychguard)
        self.praetorians = UnitType(self, Praetorians)
        self.flayedone = UnitType(self, FlayedOnes)
        self.stalker = UnitType(self, Stalkers)
        UnitType(self, Nightbringer)
        UnitType(self, Deciever)


class Elites(BaseElites):
    pass


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.warriors = UnitType(self, Warriors)
        self.immortals = UnitType(self, Immortals)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.wraiths = UnitType(self, Wraiths)
        self.scarabs = UnitType(self, Scarabs)
        self.tombblades = UnitType(self, TombBlades)
        self.destroyers = UnitType(self, Destroyers)
        night = UnitType(self, NightScythe)
        night.visible = False
        UnitType(self, NightScythes)
        UnitType(self, GhostArk)


class FastAttack(BaseFastAttack):
    pass


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.ark = UnitType(self, DoomsdayArk)
        self.barge = UnitType(self, AnnihilationBarge)
        self.monolith = UnitType(self, Monolith)
        doom = UnitType(self, DoomScythe)
        doom.visible = False
        self.scythe = UnitType(self, DoomScythes)
        self.spyders = UnitType(self, Spyders)
        UnitType(self, HeavyDestroyers)


class HeavySupport(BaseHeavySupport):
    pass


class LordsOfWar(LordsOfWarSection):
    def __init__(self, parent, vault=True):
        super(LordsOfWar, self).__init__(parent)
        if vault:
            UnitType(self, TesseractVault)
        UnitType(self, Obelisk)
        UnitType(self, Imotekh)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [NightScythes, DoomScythes])


class NecronV3Base(Wh40kBase):
    army_id = 'necron_v3_base'
    army_name = 'Necrons'

    @property
    def supplement(self):
        return None

    def __init__(self):
        super(NecronV3Base, self).__init__(
            hq=HQ(parent=self), elites=Elites(parent=self), troops=Troops(parent=self), fast=FastAttack(parent=self), heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(self)
        )


class NecronV3CAD(NecronV3Base, CombinedArmsDetachment):
    army_id = 'necron_c3_cad'
    army_name = 'Necrons (Combined arms detachment)'


class NecronV3AD(NecronV3Base, AlliedDetachment):
    army_id = 'necron_v2_ad'
    army_name = 'Necron (Allied detachment)'


class NecronV3PA(NecronV3Base, PlanetstrikeAttacker):
    army_id = 'necron_v3_pa'
    army_name = 'Necrons (Planetstrike attacker detachment)'


class NecronV3PD(NecronV3Base, PlanetstrikeDefender):
    army_id = 'necron_v2_pd'
    army_name = 'Necron (Planetstrike defender detachment)'


class NecronV3SA(NecronV3Base, SiegeAttacker):
    army_id = 'necron_v3_sa'
    army_name = 'Necrons (Siege War attacker detachment)'


class NecronV3SD(NecronV3Base, SiegeDefender):
    army_id = 'necron_v2_sd'
    army_name = 'Necron (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'necron_v2_asd'
    army_name = 'Necron (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class MephritCohort(Wh40kBase):
    army_id = 'necron_v3_mephrit_cohort'
    army_name = 'Mephrit Dynasty Cohort'

    @property
    def supplement(self):
        return 'mephrit'

    def __init__(self):
        troops = Troops(parent=self)
        troops.min, troops.max = (3, 8)
        super(MephritCohort, self).__init__(
            sections=[
                HQ(parent=self), Elites(parent=self), troops,
                FastAttack(parent=self), HeavySupport(parent=self),
                Fort(parent=self),
                LordsOfWar(self, vault=False)]
        )


class NecronV3KillTeam(Wh40kKillTeam):
    army_id = 'necron_v3_kt'
    army_name = 'Necrons'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(NecronV3KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [Warriors, Immortals],
                                                    lambda u: u.transport.subopt)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(NecronV3KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [Deathmarks, Lychguard, Praetorians],
                                                    lambda u: u.transport.subopt)
            UnitType(self, FlayedOnes)

    class KTFast(FastSection):
        def __init__(self, parent):
            super(NecronV3KillTeam.KTFast, self).__init__(parent)
            self.wraiths = UnitType(self, Wraiths)
            self.scarabs = UnitType(self, Scarabs)
            self.tombblades = UnitType(self, TombBlades)
            self.destroyers = UnitType(self, Destroyers)

    def __init__(self):
        super(NecronV3KillTeam, self).__init__(self.KTTroops(self),
                                               self.KTElites(self),
                                               self.KTFast(self))


faction = 'Necrons'


class Decurion(Wh40k7ed):
    army_id = 'necron_decurion_v3'
    army_name = 'Necron Decurion Detachment'

    @property
    def supplement(self):
        return None

    class Core(Detachment):
        def __init__(self, parent):
            super(Decurion.Core, self).__init__(parent, 'core', 'Core',
                                                [ReclamationLegion], 1, None)

    class Command(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(Decurion.Command, self).__init__(parent, 'command', 'Command',
                                                   [RoyalCourt], 0, 1)

        def check_limits(self):
            self.max = max([1, len(self.core.units)])
            return super(Decurion.Command, self).check_limits()

    class StarGod(BaseFormation):
        army_name = 'Star-God'
        army_id = 'necron_stargod_v3'

        def __init__(self):
            super(Decurion.StarGod, self).__init__()
            nightbringer = UnitType(self, Nightbringer)
            deciever = UnitType(self, Deciever)
            trans = UnitType(self, TranscendentCtan)
            vault = UnitType(self, TesseractVault)
            self.add_type_restriction([nightbringer, deciever, trans, vault],
                                      1, 1)

    class Flayeds(BaseFormation):
        army_name = 'Flayed Ones'
        army_id = 'necron_flayedones_v3'

        def __init__(self):
            super(Decurion.Flayeds, self).__init__()
            UnitType(self, FlayedOnes, min_limit=1, max_limit=1)

    class Marks(BaseFormation):
        army_name = 'Deathmarks'
        army_id = 'necron_deathmarks_v3'

        def __init__(self):
            super(Decurion.Marks, self).__init__()
            UnitType(self, Deathmarks, min_limit=1, max_limit=1)

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(Decurion.Auxilary, self).__init__(parent, 'auxilary', 'Auxilary',
                                                    [JudicatorBatallion,
                                                     DestroyerCult,
                                                     CanoptekHarvest,
                                                     Decurion.StarGod,
                                                     Decurion.Flayeds,
                                                     Decurion.Marks,
                                                     AnnihilationNexus,
                                                     LivingTomb,
                                                     DeathbringerFlight], 1, 10)

        def check_limits(self):
            self.min = max([1, len(self.core.units)])
            self.max = max([1, len(self.core.units)]) * 10
            return super(Decurion.Auxilary, self).check_limits()

    def __init__(self):
        core = self.Core(self)
        command = self.Command(self, core)
        aux = self.Auxilary(self, core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class NecronV3(Wh40kImperial, Wh40k7ed):
    army_id = 'necron_v3'
    army_name = 'Necrons'
    # development = True
    faction = faction

    def __init__(self):
        super(NecronV3, self).__init__([
            NecronV3CAD,
            MephritCohort,
            FlierDetachment,
            Decurion,
            ReclamationLegion,
            JudicatorBatallion,
            DestroyerCult,
            DeathbringerFlight,
            LivingTomb,
            AnnihilationNexus,
            CanoptekHarvest,
            RoyalCourt,
            Conclave,
            RoyalDecurion,
            StrategicDecurion,
            Guardians,
            MephritResurgence,
            RetributionPhalanx,
            OppressorFlight
        ])


class NecronV3Missions(Wh40kImperial, Wh40k7edMissions):
    army_id = 'necron_v3_mis'
    army_name = 'Necrons'
    # development = True
    faction = faction

    def __init__(self):
        super(NecronV3Missions, self).__init__([
            NecronV3CAD,
            MephritCohort,
            NecronV3PA,
            NecronV3PD,
            NecronV3SA,
            NecronV3SD,
            FlierDetachment,
            Decurion,
            ReclamationLegion,
            JudicatorBatallion,
            DestroyerCult,
            DeathbringerFlight,
            LivingTomb,
            AnnihilationNexus,
            CanoptekHarvest,
            RoyalCourt,
            Conclave,
            RoyalDecurion,
            StrategicDecurion,
            Guardians,
            MephritResurgence,
            RetributionPhalanx,
            OppressorFlight
        ])


detachments = [
    NecronV3CAD,
    NecronV3AD,
    MephritCohort,
    NecronV3PA,
    NecronV3PD,
    NecronV3SA,
    NecronV3SD,
    FlierDetachment,
    Decurion,
    ReclamationLegion,
    JudicatorBatallion,
    DestroyerCult,
    DeathbringerFlight,
    LivingTomb,
    AnnihilationNexus,
    CanoptekHarvest,
    RoyalCourt,
    Conclave,
    RoyalDecurion,
    StrategicDecurion,
    Guardians,
    MephritResurgence,
    RetributionPhalanx,
    OppressorFlight
]


unit_types = [
    Overlord, Lord, Cryptek, DestroyerLord, CommandBarge,
    Illuminor, Orikan, Nemesor, Vargard, Anrakyr, Trazyn, Lychguard,
    Deathmarks, FlayedOnes, Praetorians, Stalkers, Nightbringer,
    Deciever, Warriors, Immortals, TombBlades, Destroyers,
    NightScythe, GhostArk, Wraiths, Scarabs, NightScythes,
    HeavyDestroyers, Spyders, DoomScythe, AnnihilationBarge,
    DoomsdayArk, TranscendentCtan, Monolith, DoomScythes
]
