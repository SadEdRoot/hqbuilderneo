__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.wh40k.roster import Unit


class TesseractVault(Unit):
    type_name = 'Tesseract Vault'
    type_id = 'vault_v3'

    def __init__(self, parent):
        super(TesseractVault, self).__init__(parent, self.type_name, 550, gear=[
            Gear('Tesla sphere', count=4),
            Gear('Powers of the C\'tan')],
            static=True)


class Obelisk(Unit):
    type_name = 'Obelisk'
    type_id = 'obelisk_v3'

    def __init__(self, parent):
        super(Obelisk, self).__init__(parent, self.type_name, 300, gear=[
            Gear('Tesla sphere', count=4)],
            static=True)


class Imotekh(Unit):
    type_name = 'Imotekh the Stormlord, Phaeron of the Sautekh dynasty'
    type_id = 'imotekhthestormlord_v3'

    def __init__(self, parent):
        super(Imotekh, self).__init__(parent=parent, name='Imotekh the Stormlord',
                                      points=190, unique=True, gear=[
                                          Gear('Gauntlet of Fire'), Gear('Phase Shifter'),
                                          Gear('Staff of the Destroyer')
                                      ], static=True)
