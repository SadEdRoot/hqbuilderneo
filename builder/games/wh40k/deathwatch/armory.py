__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf
from builder.games.wh40k.adepta_sororitas_v4.armory import CadianRelicsHoly, CadianWeaponHoly,\
    CadianWeaponArcana


class BaseWeapon(OneOf):
    def __init__(self, parent, name, **kwargs):
        self.armour = kwargs.pop('armour', None)
        self.pwr_weapon = []
        self.tda_weapon = []
        self.relic_options = []

        super(BaseWeapon, self).__init__(parent, name, **kwargs)

    def check_rules(self):
        if self.armour:
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.pwr_weapon
            else:
                visible = self.pwr_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def get_unique(self):
        if self.used and self.cur in self.relic_options:
            return self.description
        return []


class BoltPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(BoltPistol, self).__init__(*args, **kwargs)
        self.boltpistol = self.variant('Bolt pistol', 0)
        self.pwr_weapon += [self.boltpistol]


class Boltgun(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Boltgun, self).__init__(*args, **kwargs)
        self.boltgun = self.variant('Boltgun', 0)
        self.pwr_weapon += [self.boltgun]


class Ranged(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Ranged, self).__init__(*args, **kwargs)

        stormbolter = self.variant('Storm bolter', 5)
        combimelta = self.variant('Combi-melta', 10)
        combiflamer = self.variant('Combi-flamer', 10)
        combiplasma = self.variant('Combi-plasma', 10)
        hflamer = self.variant('Hand flamer', 10)
        gravpistol = self.variant('Grav-pistol', 15)
        infernopistol = self.variant('Inferno pistol', 15)
        plasmapistol = self.variant('Plasma pistol', 15)

        self.pwr_weapon += [stormbolter,
                            combiflamer,
                            combimelta,
                            combiplasma,
                            hflamer,
                            gravpistol,
                            infernopistol,
                            plasmapistol]


class Special(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Special, self).__init__(*args, **kwargs)
        shotgun = self.variant('Deathwatch shotgun')
        self.stalker = self.variant('Stalker pattern boltgun', 5)
        flamer = self.variant('Flamer', 5)
        meltagun = self.variant('Meltagun', 10)
        gravgun = self.variant('Grav-gun', 15)
        plasmagun = self.variant('Plasma gun', 15)
        self.spec = [shotgun, self.stalker, flamer,
                     meltagun, gravgun, plasmagun]

    def is_spec(self):
        return self.used and self.cur in self.spec

    def is_stalker(self):
        return self.used and (self.cur == self.stalker)


class PlainMelee(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PlainMelee, self).__init__(*args, **kwargs)
        melee = self.variant('Close combat weapon', 0)
        self.pwr_weapon += [melee]


class Melee(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Melee, self).__init__(*args, **kwargs)
        sword = self.variant('Chainsword')
        claw = self.variant('Lightning claw', 15)
        power = self.variant('Power weapon', 15)
        fist = self.variant('Power fist', 25)
        self.hammer = self.variant('Thunder hammer', 30)
        self.pwr_weapon += [sword, claw, power, fist, self.hammer]

    def is_hammer(self):
        return self.cur == self.hammer


class Heavy(BaseWeapon):

    class Ammo(OptionsList):
        def __init__(self, parent):
            super(Heavy.Ammo, self).__init__(parent=parent, name='', limit=None)
            self.hellfire = self.variant('Hellfire shells', 5)
            self.flakk = self.variant('Flakk missiles', 10)

        def switch(self, toflakk):
            self.hellfire.used = self.hellfire.visible = not toflakk
            self.flakk.used = self.flakk.visible = toflakk

    def __init__(self, parent, *args, **kwargs):
        super(Heavy, self).__init__(parent, *args, **kwargs)
        self.heavybolter = self.variant('Heavy bolter', 10)
        self.heavyflamer = self.variant('Heavy flamer', 10)
        self.missilelauncher = self.variant('Missile launcher', 15)
        self.infernus = self.variant('Infernus heavy bolter', 20)
        self.frag = self.variant('Deathwatch frag cannon', 25)
        self.heavy = [self.heavybolter, self.heavyflamer, self.missilelauncher,
                      self.infernus, self.frag]
        self.ammo = self.Ammo(parent=parent)
        self.hidden = False

    def is_heavy(self):
        return self.cur in self.heavy

    def check_rules(self):
        super(Heavy, self).check_rules()
        self.ammo.visible = self.ammo.used = self.visible and not self.hidden and\
                             self.cur in [self.heavybolter, self.missilelauncher, self.infernus]
        for vrt in self.heavy:
            vrt.used = vrt.visible = not self.hidden
        if self.ammo.visible:
            self.ammo.switch(self.cur == self.missilelauncher)

    def set_visible(self, flag):
        self.hidden = not flag
        self.check_rules()

    def has_furor(self):
        return self.cur in [self.infernus, self.frag]


class SpecialWargear(OptionsList):
    def __init__(self, parent):
        super(SpecialWargear, self).__init__(parent, 'Special issue wargear')
        self.auspex = self.variant('Auspex', 5)
        self.shield = self.variant('Combat shield', 5)
        self.meltabombs = self.variant('Melta bombs', 5)
        self.teleporthomer = self.variant('Deathwatch teleport homer', 10)
        self.digitalweapons = self.variant('Digital weapons', 10)


class RelicVigilant(OptionsList):
    def __init__(self, parent, watchmaster=False):
        super(RelicVigilant, self).__init__(parent, 'Relics of the Vigilant', limit=1)
        self.variant('The Tome of Ectoclades', 10)
        self.variant('Banebolts of Eryxia', 10)
        self.key = watchmaster and self.variant('The Osseus Key', 15)
        self.variant('Dominius Aegis', 20)
        self.variant('The Beacon Angelis', 30)

    def has_key(self):
        return self.key and self.key.value

    def get_unique(self):
        if self.used:
            return self.description
        return []


class HolyRelicVigilant(CadianRelicsHoly, RelicVigilant):
    pass


class RelicWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(RelicWeapon, self).__init__(*args, **kwargs)
        self.thief = self.variant('The Thief of Secrets', 20)
        self.relic_options += [self.thief]


class HolyWeapon(CadianWeaponHoly, RelicWeapon):
    def __init__(self, *args, **kwargs):
        super(HolyWeapon, self).__init__(*args, **kwargs)
        self.relic_options += [self.worthy_blade]


class ArcanaWeapon(CadianWeaponArcana, BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(ArcanaWeapon, self).__init__(*args, **kwargs)
        self.relic_options += [self.qann]


class Vehicle(OptionsList):
    def __init__(self, parent, blade=True):
        super(Vehicle, self).__init__(parent, 'Options')
        if blade:
            self.dblade = self.Variant(self, 'Dozer blade', 5)
        self.sbgun = self.Variant(self, 'Storm bolter', 5)
        self.exarm = self.Variant(self, 'Extra armour', 10)
        self.hkm = self.Variant(self, 'Hunter-killer missile', 10)
