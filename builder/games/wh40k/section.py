from builder.core2 import section
from .unit import Unit
from builder.core2 import UnitType, SubRoster

__author__ = 'Ivan Truskov'


class Section(section.Section):

    def __init__(self, *args, **kwargs):
        self.ia_units = []
        super(Section, self).__init__(*args, **kwargs)

    def append_unit_type(self, unit_type):
        super(Section, self).append_unit_type(unit_type)
        if hasattr(unit_type.unit_class, 'imperial_armour')\
           and unit_type.unit_class.imperial_armour:
            self.ia_units.append(unit_type)


class Formation(section.Formation):
    wikilink = ''

    def __init__(self, *args, **kwargs):
        self.ia_units = []
        super(Formation, self).__init__(*args, **kwargs)

    def append_unit_type(self, unit_type):
        super(Formation, self).append_unit_type(unit_type)
        if hasattr(unit_type.unit_class, 'imperial_armour')\
           and unit_type.unit_class.imperial_armour:
            self.ia_units.append(unit_type)


class CompositeFormation(Formation):

    def _build_unique_map(self, names_getter):
        unique_map = {}
        for unit in self.units:
            if hasattr(unit, 'get_unique_map'):
                submap = unit.get_unique_map(names_getter)
                for (k, v) in submap.items():
                    if k in list(unique_map.keys()):
                        unique_map[k] += v
                    else:
                        unique_map[k] = v
            else:
                un = names_getter(unit)
                if un is None:
                    continue
                if not isinstance(un, (list, type)):
                    un = [un]
                    for name in un:
                        if name in list(unique_map.keys()):
                            unique_map[name] += 1
                        else:
                            unique_map[name] = 1
        return unique_map


class HQSection(Section):
    def __init__(self, parent, min_limit=1, max_limit=2):
        super(HQSection, self).__init__(parent, 'hq', 'HQ', min_limit, max_limit)


class ElitesSection(Section):
    def __init__(self, parent, min_limit=0, max_limit=3):
        super(ElitesSection, self).__init__(parent, 'el', 'Elites', min_limit, max_limit)


class TroopsSection(Section):
    def __init__(self, parent, min_limit=2, max_limit=6):
        super(TroopsSection, self).__init__(parent, 'tr', 'Troops', min_limit, max_limit)


class FastSection(Section):
    def __init__(self, parent, min_limit=0, max_limit=3):
        super(FastSection, self).__init__(parent, 'fa', 'Fast attack', min_limit, max_limit)


class HeavySection(Section):
    def __init__(self, parent, min_limit=0, max_limit=3):
        super(HeavySection, self).__init__(parent, 'hs', 'Heavy support', min_limit, max_limit)


class LordsOfWarSection(Section):
    def __init__(self, parent, min_limit=0, max_limit=1):
        super(LordsOfWarSection, self).__init__(parent, 'low', 'Lords of War', min_limit, max_limit)


class SuperHeavySection(Section):
    def __init__(self, parent, min_limit=0, max_limit=None):
        super(SuperHeavySection, self).__init__(parent, 'super', 'Super-heavy', min_limit, max_limit)


class FlyerSection(Section):
    def __init__(self, parent, flier_classes, min_limit=1, max_limit=3):
        super(FlyerSection, self).__init__(parent, 'flyer', 'Flyer Wing', min_limit, max_limit)
        for _class in flier_classes:
            self.add_flier_class(_class)

    def add_flier_class(self, flier_class):
        class Wing(flier_class):
            unit_min = 2
        UnitType(self, Wing)


class Detachment(Section):

    @staticmethod
    def build_detach(instance, detach_class, detach_faction=None, *args, **kwargs):
        class DetachUnit(Unit):
            type_id = detach_class.army_id
            type_name = detach_class.army_name
            faction = detach_faction
            wikilink = getattr(detach_class, 'wikilink', '')

            imperial_armour = hasattr(detach_class, 'imperial_armour') and\
                detach_class.imperial_armour

            def __init__(self, parent):
                super(DetachUnit, self).__init__(parent)
                detach = detach_class()
                detach.parent = self
                self.sub_roster = SubRoster(self, detach)

            def dump(self):
                data = super(DetachUnit, self).dump()
                data['detach_unit'] = True
                return data

            def get_unique_map(self, names_getter):
                return self.sub_roster.roster._build_unique_map(names_getter)

            def build_statistics(self):
                return self.sub_roster.roster.build_statistics()

        return UnitType(instance, DetachUnit, *args, **kwargs)

    def __init__(self, parent, typename, caption, roster_types,
                 min_limit=0, max_limit=1, grouped_roster_types=None,
                 obsolete_types=None,
                 faction=None):

        super(Detachment, self).__init__(parent, typename, caption,
                                         min_limit=min_limit, max_limit=max_limit)
        list(map(lambda _type: Detachment.build_detach(self, _type, faction), roster_types))
        if grouped_roster_types:
            try:
                for group in grouped_roster_types:
                    list(map(lambda _type: Detachment.build_detach(self, _type, detach_faction=group, group=group), grouped_roster_types[group]))
            except Exception:
                pass
        if obsolete_types:
            list(map(lambda _type: Detachment.build_detach(self, _type, None, visible=False), obsolete_types))
