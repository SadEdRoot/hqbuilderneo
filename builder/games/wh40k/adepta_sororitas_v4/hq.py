from builder.core2 import Gear, OptionsList, UnitDescription
from .armory import Boltgun, BoltPistol, WeaponRelic,\
    Melee, Ranged, Eviscerator, Shotgun,\
    BaseRelic, InfernoPistol, Laspistol, CCW, CadianRelicsHoly,\
    CadianWeaponHoly, CadianWeaponArcana
from builder.games.wh40k.roster import Unit


class ASWeaponRelic(CadianWeaponHoly, WeaponRelic):
    def __init__(self, *args, **kwargs):
        super(ASWeaponRelic, self).__init__(*args, **kwargs)
        self.relics += [self.worthy_blade]


class MarsWeaponRelic(CadianWeaponArcana, WeaponRelic):
    def __init__(self, *args, **kwargs):
        super(MarsWeaponRelic, self).__init__(*args, **kwargs)
        self.relics += [self.qann]


class ASRelic(CadianRelicsHoly, BaseRelic):
    pass


class Veridyan(Unit):
    type_name = 'Canoness Veridyan'
    type_id = 'veridyan_v1'
    # wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Uriah-Jacobus')

    def __init__(self, parent):
        super(Veridyan, self).__init__(parent, 'Canoness Veridyan', 85, unique=True, static=True, gear=[
            Gear('Bolt pistol'), Gear('Power sword'), Gear('Frag grenades'), Gear('Krak grenades')
        ])


class Jacobus(Unit):
    type_name = 'Uriah Jacobus, Protector of the Faith'
    type_id = 'jaccobus_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Uriah-Jacobus')

    def __init__(self, parent):
        super(Jacobus, self).__init__(parent, 'Uriah Jacobus', 100, unique=True, static=True, gear=[
            Gear('Bolt pistol'), Gear('The Redeemer'),
            Gear('Chainsword'), Gear('Frag grenades'), Gear('Krak grenades'),
            Gear('Banner of Sanctity'), Gear('Rosarius')
        ])


class Canoness(Unit):
    type_name = 'Canoness'
    type_id = 'canoness_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Canoness')

    class Weapon1(MarsWeaponRelic, Eviscerator, Melee,
                  Ranged, InfernoPistol, Boltgun, BoltPistol):
        pass

    class Weapon2(ASWeaponRelic, InfernoPistol, Eviscerator, Melee):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Canoness.Options, self).__init__(parent, 'Options')
            self.variant('Melta bombs', 5)
            self.variant('Rosarius', 15)

    def __init__(self, parent):
        super(Canoness, self).__init__(parent, points=65, gear=[
            Gear('Frag grenades'), Gear('Krak grenades')])

        self.wep1 = self.Weapon1(self, 'Ranged weapon')
        self.wep2 = self.Weapon2(self, 'Melee weapon')
        self.wep2.melee = self.wep2.melee[1:]
        self.opt = self.Options(self)
        self.relics = ASRelic(self)

    def check_rules(self):
        super(Canoness, self).check_rules()
        for wr1, wr2 in zip(self.wep1.relics, self.wep2.relics):
            wr1.active = not(self.relics.any or self.wep2.cur == wr2)
            wr2.active = not(self.relics.any or self.wep1.cur == wr1)
        self.wep1.allow_melee(not self.wep2.is_melee())
        self.wep1.allow_ranged(not self.wep2.is_ranged())
        self.wep2.allow_melee(not self.wep1.is_melee())
        self.wep2.allow_ranged(not self.wep1.is_ranged())

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        for wep in [self.wep1, self.wep2]:
            if wep.cur in wep.relics:
                return wep.description
        if self.relics.any:
            return self.relics.description
        return []


class Priest(Unit):
    type_name = 'Ministorum Priest'
    type_id = 'priest_v4'
    wikilink = Unit.template_link.format(codex_name='adepta-sororitas', unit_name='Ministorum-Priest')

    class Weapon1(CadianWeaponArcana, Eviscerator, Shotgun, Melee, Ranged, Laspistol):
        def __init__(self, parent):
            super(Priest.Weapon1, self).__init__(parent)
            self.variant('Autogun', 0)
            self.variant('Bolt pistol', 1)
            self.variant('Boltgun', 1)
            self.variant('Plasma gun', 15)

    class Weapon2(CadianWeaponHoly, Eviscerator, Shotgun, Melee, Ranged, CCW):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Priest.Options, self).__init__(parent, 'Options')
            self.variant('Melta bombs', 5)

    def __init__(self, parent):
        super(Priest, self).__init__(parent, points=25,
                                     gear=[Gear('Flak armour'), Gear('Frag grenades'),
                                           Gear('Krak grenades'), Gear('Rosarius')])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.opt = self.Options(self)
        self.relics = ASRelic(self, priest=True)

    def get_unique_gear(self):
        wep_rel = []
        if self.wep2.cur == self.wep2.worthy_blade:
            wep_rel += self.wep2.description
        if self.wep1.cur == self.wep1.qann:
            wep_rel += self.wep1.description
        return self.relics.description + wep_rel

    def check_rules(self):
        super(Priest, self).check_rules()
        self.wep1.allow_melee(not self.wep2.is_melee())
        self.wep1.allow_ranged(not self.wep2.is_ranged())
        self.wep2.allow_melee(not self.wep1.is_melee())
        self.wep2.allow_ranged(not self.wep1.is_ranged())

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')


class Celestine(Unit):
    type_name = 'Celestine, The Living Saint'
    type_id = 'celestine_v4'

    def __init__(self, parent):
        super(Celestine, self).__init__(parent, 'Celestine', 200, [
            Gear('The Argent Blade'), Gear('The Armour of Saint Katherine'), Gear('Frag grenades'), Gear('Krak grenades'),
            UnitDescription('Germinae Superia', count=2, options=[Gear('Bolt pistol'), Gear('Power Sword'),
                                                                  Gear('Frag grenades'), Gear('Krak grenades')])],
                                        unique=200)

    def build_statistics(self):
        return {'Models': 3, 'Units': 1}
