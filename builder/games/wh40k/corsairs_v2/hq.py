__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf, OptionsList
from builder.games.wh40k.harlequins.armory import CommonEnigmas
from builder.games.wh40k.dark_eldar_v2.armory import Artefacts
from builder.games.wh40k.eldar_v3.armory import Relics
from builder.games.wh40k.unit import Unit


class Prince(Unit):
    type_name = 'Corsair Prince'
    type_id = 'prince_v2'
    imperial_armour = True

    class JetbikeWeapons(OneOf):
        def __init__(self, parent):
            super(Prince.JetbikeWeapons, self).__init__(parent, 'Jetbike weapon')
            self.variant('Twin-linked lasblaster')
            self.variant('Twin-linked shuriken catapult')
            self.variant('Twin-linked splinter rifles')

    class Movement(OptionsList):
        def __init__(self, parent):
            super(Prince.Movement, self).__init__(parent, 'Movement options', limit=1)
            self.bike = self.variant('Upgrade to Corsair Prince Cloud Dancer', 15, gear=[])
            self.variant('Corsair jet pack', 5)

    class Weapons(OptionsList):
        def __init__(self, parent):
            super(Prince.Weapons, self).__init__(parent, 'Weapons')
            self.variant('Haywire grenades', 15)
            self.variant('Tanglefield grenades', 10)
            self.variant('Power weapon', 15)
            self.variant('Venom blade', 10)
            self.variant('Blast pistol', 20)
            self.variant('Dissonance pistol', 10)
            self.variant('Fusion pistol', 20)
            self.variant('Void sabre', 20)
            self.variant('Balelight', 15)

    class Power(OptionsList):
        def __init__(self, parent):
            super(Prince.Power, self).__init__(parent, 'Wild Psyker')
            self.variant('Mastery level 1', 20)

    class Armour(OptionsList):
        def __init__(self, parent):
            super(Prince.Armour, self).__init__(parent, 'Armour', limit=1)
            self.variant('Voidplate harness', 15)
            self.variant('Ghostplate armour', 10)

    class Field(OptionsList):
        def __init__(self, parent):
            super(Prince.Field, self).__init__(parent, 'Protective field', limit=1)
            self.variant('Shimmershield', 15)
            self.variant('Force shield', 10)
            self.variant('Shadowshield', 25)

    class Obscession(OptionsList):
        def __init__(self, parent):
            super(Prince.Obscession, self).__init__(parent, 'Obscession', limit=1)
            self.drugs = self.variant('Seeker of Forbidden Pleasures', gear=[Gear('Corsair Combat Drugs')])
            self.key = self.variant('Traveller of Forgotten Paths', gear=[Gear('Multiphase Key Generator')])
            self.junk = self.variant('Collector of Ancient Treasures', gear=[])
            self.variant('Reaper of the Outer Dark', gear=[])
            self.powers = self.variant('Wielder of Profane Powers')
            self.fnp = self.variant('Survivor if Endless Darkness')

    def is_druglord(self):
        return self.trait.drugs.value

    def is_keybearer(self):
        return self.trait.key.value

    def is_junk_collector(self):
        return self.trait.junk.value

    def is_ancient(self):
        return self.trait.fnp.value

    def __init__(self, parent):
        super(Prince, self).__init__(parent, 'Corsair Prince', 75, [
            Gear('Brace of pistols'), Gear('Shadowwave grenades'),
            Gear('Plasma grenades'), Gear('Heavy mesh armour')])
        self.move = self.Movement(self)
        self.bike_weapon = self.JetbikeWeapons(self)
        self.power = self.Power(self)
        self.wep = self.Weapons(self)
        self.Armour(self)
        self.Field(self)
        self.trait = self.Obscession(self)
        self.artefacts = [
            CommonEnigmas(self),
            Artefacts(self),
            Relics(self, seer=False)
        ]

    def check_rules(self):
        super(Prince, self).check_rules()
        self.bike_weapon.used = self.bike_weapon.visible = self.move.bike.value
        art_sum = 0
        for art in self.artefacts:
            art_sum += art.any * self.trait.junk.value
            art.used = art.visible = self.trait.junk.value
        if art_sum > 1:
            self.error('Corsair Prince may carry no more then one artifact')

    def build_description(self):
        res = super(Prince, self).build_description()
        if self.move.bike.value:
            res.name = 'Corsair prince Cloud Dancer'
        return res

    def count_charges(self):
        return self.trait.powers.value + self.power.any

    def build_statistics(self):
        return self.note_charges(super(Prince, self).build_statistics())


class CharacterTraits(OptionsList):
    def __init__(self, parent):
        super(CharacterTraits, self).__init__(parent, 'Obscession')
        self.drug = self.variant('Corsair Combat Drugs', 15)
        self.key = self.variant('Multiphase Key Generator', 25)
        self.weapon = self.variant('Master-crafted weapon', 10)
        self.fnp = self.variant('Feel No Pain', 10)

    def check_rules(self):
        super(CharacterTraits, self).check_rules()
        self.drug.used = self.drug.visible = self.roster.prince.is_druglord()
        self.key.used = self.key.visible = self.roster.prince.is_keybearer()
        self.weapon.used = self.weapon.visible = self.roster.prince.is_junk_collector()
        self.fnp.used = self.fnp.visible = self.roster.prince.is_ancient()
        self.used = self.visible = sum(opt.visible for opt in [
            self.drug, self.key, self.weapon, self.fnp])


class Dreamer(Unit):
    type_name = 'Corsair Void Dreamer'
    type_id = 'dreamer_v2'
    imperial_armour = True

    class Power(OneOf):
        def __init__(self, parent):
            super(Dreamer.Power, self).__init__(parent, 'Wild Psyker')
            self.variant('Mastery Level 1')
            self.ml2 = self.variant('Mastery Level 2', 20)
            self.ml3 = self.variant('Mastery Level 3', 40)

    def __init__(self, parent):
        super(Dreamer, self).__init__(parent, points=50, gear=[
            Gear('Brace of pistols'), Gear('Witch staff'), Gear('Heavy mesh armour'),
            Gear('Shadowwave grenades'), Gear('Plasma grenades')
        ])
        self.move = Prince.Movement(self)
        self.move.bike.visible = self.move.bike.used = False
        Prince.Weapons(self)
        Prince.Armour(self)
        Prince.Field(self)
        self.power = self.Power(self)
        CharacterTraits(self)

    def count_charges(self):
        if self.power.cur == self.power.ml2:
            return 2
        elif self.power.cur == self.power.ml3:
            return 3
        return 1

    def build_statistics(self):
        return self.note_charges(super(Dreamer, self).build_statistics())


class Baron(Unit):
    type_name = 'Corsair Baron'
    type_id = 'baron_v2'
    imperial_armour = True

    class Movement(OptionsList):
        def __init__(self, parent):
            super(Baron.Movement, self).__init__(parent, 'Movement options', limit=1)
            self.bike = self.variant('Upgrade to Cloud Dancer Baron', 15, gear=[])
            self.variant('Corsair jet pack', 5)

    class Power(OptionsList):
        def __init__(self, parent):
            super(Baron.Power, self).__init__(parent, 'Wild Psyker')
            self.variant('Mastery level 1', 15)

    def __init__(self, parent):
        super(Baron, self).__init__(parent, points=30, gear=[
            Gear('Brace of pistols'), Gear('Close combat weapon'), Gear('Heavy mesh armour'),
            Gear('Shadowwave grenades'), Gear('Plasma grenades')
        ])
        self.move = self.Movement(self)
        self.bike_weapon = Prince.JetbikeWeapons(self)
        Prince.Weapons(self)
        Prince.Armour(self)
        Prince.Field(self)
        self.power = self.Power(self)
        CharacterTraits(self)

    def check_rules(self):
        super(Baron, self).check_rules()
        self.bike_weapon.used = self.bike_weapon.visible = self.move.bike.value

    def build_description(self):
        res = super(Baron, self).build_description()
        if self.move.bike.value:
            res.name = 'Cloud Dancer Baron'
        return res

    def count_charges(self):
        return self.power.any

    def build_statistics(self):
        return self.note_charges(super(Baron, self).build_statistics())
