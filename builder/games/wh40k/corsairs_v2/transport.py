__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OneOf, OptionsList
from builder.games.wh40k.roster import Unit


class Falcon(Unit):
    type_name = 'Corsair Falcon'
    type_id = 'corsair_falcon_v2'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Falcon.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Twin-linked lasblasters')
            self.variant('Twin-linked splinter rifles')
            self.variant('Twin-linked shuriuken catapults')
            self.variant('Splinter cannon', 10)
            self.variant('Shuriken Cannon', 20)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Falcon.Weapon2, self).__init__(parent, '')
            self.variant('Shuriken cannon')
            self.variant('Splinter cannon')
            self.variant('Scatter laser')
            self.variant('Starcannon', 5)
            self.variant('Bright lance', 5)
            self.variant('Dark lance', 5)
            self.variant('Eldar Missile Launcher', 15)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Falcon.Options, self).__init__(parent, 'Options')
            self.variant('Corsair void burners', 5)
            self.variant('Corsair kinetic shroud', 15)
            self.variant('Star Engines', 15)

    def __init__(self, parent):
        super(Falcon, self).__init__(parent, 'Falcon', 130, [Gear('Pulse laser')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.Options(self)


class Venom(Unit):
    type_name = 'Corsair Venom'
    type_id = 'venom_v2'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Venom.Weapon, self).__init__(parent, '')
            self.variant('Shuriken cannon')
            self.variant('Splinter cannon')
            self.variant('Scatter laser')

    class Options(Falcon.Options):
        def __init__(self, parent):
            super(Venom.Options, self).__init__(parent)
            self.variant('Chain snares', 5)

    def __init__(self, parent):
        super(Venom, self).__init__(parent, 'Venom', 50)
        Falcon.Weapon1(self)
        self.Weapon(self)
        self.Options(self)
