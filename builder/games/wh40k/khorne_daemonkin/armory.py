__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from builder.core2 import OneOf, OptionsList, Gear, ListSubUnit
from builder.games.wh40k.roster import Unit


class BaseWeapon(OneOf):
    def __init__(self, parent, name, **kwargs):
        self.armour = kwargs.pop('armour', None)
        self.pwr_weapon = []
        self.tda_weapon = []
        self.artefact_weapon = []

        super(BaseWeapon, self).__init__(parent, name, **kwargs)

    def check_rules(self):
        if self.armour:
            if self.armour.is_tda():
                visible = self.tda_weapon
                invisible = self.pwr_weapon
            else:
                visible = self.pwr_weapon
                invisible = self.tda_weapon

            for wep in visible:
                wep.visible = True
            for wep in invisible:
                wep.visible = False

    def get_unique(self):
        if self.used and self.cur in self.artefact_weapon:
            return self.description
        return []


class BoltPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(BoltPistol, self).__init__(*args, **kwargs)
        self.boltpistol = self.variant('Bolt pistol', 0)
        self.pwr_weapon += [self.boltpistol]


class PlasmaPistol(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PlasmaPistol, self).__init__(*args, **kwargs)
        self.plaspistol = self.variant('Plasma pistol', 15)
        self.pwr_weapon += [self.plaspistol]


class Hellblade(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Hellblade, self).__init__(*args, **kwargs)
        self.hellblade = self.variant('Hellblade', 0)


class PowerMaul(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(PowerMaul, self).__init__(*args, **kwargs)
        self.powermaul = self.variant('Power maul', 0)
        self.pwr_weapon += [self.powermaul]


class Boltgun(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Boltgun, self).__init__(*args, **kwargs)
        self.boltgun = self.variant('Boltgun', 0)
        self.pwr_weapon += [self.boltgun]


class Ccw(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Ccw, self).__init__(*args, **kwargs)
        self.chainsword = self.variant('Close combat weapon', 0)
        self.pwr_weapon += [self.chainsword]


class Ranged(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Ranged, self).__init__(*args, **kwargs)
        self.unique_ranged = [
            self.variant('Combi-bolter', 3),
            self.variant('Combi-melta', 10),
            self.variant('Combi-flamer', 10),
            self.variant('Combi-plasma', 10),
            self.variant('Plasma pistol', 15),
        ]
        self.pwr_weapon += self.unique_ranged

    def has_unique_ranged(self):
        return self.cur in self.unique_ranged

    def set_unique_ranged(self, val):
        for o in self.unique_ranged:
            o.active = val


class Melee(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Melee, self).__init__(*args, **kwargs)
        self.pwr_weapon += [
            self.variant('Chainaxe', 8),
            self.variant('Lightning claw', 15),
            self.variant('Power weapon', 15),
            self.variant('Power fist', 25),
            self.variant('Axe of Khorne', 30)
        ]


class Heavy(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Heavy, self).__init__(*args, **kwargs)
        self.heavy = [
            self.variant('Heavy bolter', 10),
            self.variant('Aurocannon', 10),
            self.variant('Missile Launcher', 15),
            self.variant('Lascannon', 20),
        ]

    def has_heavy(self):
        return self.cur in self.heavy


class Special(BaseWeapon):
    def __init__(self, *args, **kwargs):
        super(Special, self).__init__(*args, **kwargs)
        self.special = [
            self.variant('Flamer', 5),
            self.variant('Meltagun', 10),
            self.variant('Plasma gun', 15)
        ]

    def has_special(self):
        return self.cur in self.special

    def activate_spec(self, val):
        for o in self.special:
            o.active = val


class CommonOptions(OptionsList):
    def __init__(self, parent, name='Options'):
        super(CommonOptions, self).__init__(parent, name=name)
        self.variant('Melta bombs', 5)


class SpecialIssue(CommonOptions):
    def __init__(self, parent, armour=None, gift=None, ride=True):
        self.armour = armour
        self.gift = gift
        self.ride = ride
        super(SpecialIssue, self).__init__(parent)
        if ride:
            self.jumppack = self.variant('Jump pack', 15)
            self.bike = self.variant('Chaos bike', 20)
            self.ride_opt = [self.jumppack, self.bike]
        self.sigilofcorruption = self.variant('Sigil of corruption', 25)

    def check_rules(self):
        super(SpecialIssue, self).check_rules()

        if self.ride:
            flag = not self.armour or (self.armour and not self.armour.is_tda())
            flag = flag and (not self.gift or (self.gift and not self.gift.juggernaut.value))
            for o in self.ride_opt:
                o.visible = o.used = flag


class Loci(OptionsList):
    def __init__(self, parent):
        super(Loci, self).__init__(parent, 'Loci of Khorne', limit=1)
        self.variant('Lesser Locus of Abjuration', 10)
        self.variant('Greater Locus of Fury', 20)
        self.variant('Exalted Locus of Wrath', 25)


class Gifts(OptionsList):
    def __init__(self, parent, armour=None, lord=False, herald=False):
        super(Gifts, self).__init__(parent, name='Gifts of Khorne')
        self.armour = armour

        self.ichorblood = self.variant('Ichor blood', 5)
        if lord:
            self.auraofdarkglory = self.variant('Aura of dark glory', 15)
        self.collar = self.variant('Collar of Khorne', 15)
        self.combatfamiliar = self.variant('Combat familiar', 15)
        if lord or herald:
            self.juggernaut = self.variant('Juggernaut of Khorne', 45)


class Vehicle(OptionsList):
    def __init__(self, parent, tank=True):
        super(Vehicle, self).__init__(parent, name='Options')
        self.variant('Combi-bolter', 5)
        self.comby = [
            self.variant('Combi-melta', 10),
            self.variant('Combi-flamer', 10),
            self.variant('Combi-plasma', 10),
        ]
        self.variant('Extra armour', 10)
        self.variant('Havok launcher', 12)
        if tank:
            self.variant('Daemonic possession', 15)


class ArtefactGear(OptionsList):
    def __init__(self, parent, armour=None, lord=False, prince=False):
        super(ArtefactGear, self).__init__(parent, name='Artefact of Slaughter')
        self.armour = armour
        self.variant('The Brazen Rune', 15)
        if lord:
            self.variant('The Scull-helm of Khorne', 15)
        if prince or lord:
            self.bloodforged = self.variant('The Blood-forged Armour', 50)

    def check_rules(self):
        super(ArtefactGear, self).check_rules()
        if self.armour is not None:
            self.bloodforged.used = self.bloodforged.visible = not self.armour.is_tda()

    def get_unique(self):
        if self.used:
            return self.description
        return []


class ArtefactWeapon(BaseWeapon):
    def __init__(self, *args, **kwargs):
        lord = kwargs.pop('lord', False)
        prince = kwargs.pop('prince', False)
        super(ArtefactWeapon, self).__init__(*args, **kwargs)
        if lord or prince:
            self.variant('Goredrinker', 30)
        self.variant('The Blade of Emdless Bloodshed', 35)
        if lord or prince:
            self.variant('Kor\'lath, the Axe of Ruin', 60)

    @staticmethod
    def process_artefacts(weapon1, weapon2):
        for r1, r2 in zip(weapon1.artefact_weapon, weapon2.artefact_weapon):
            r2.active = not weapon1.cur == r1
            r1.active = not weapon2.cur == r2


class Armour(OneOf):
    power_armour_set = [Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')]

    def __init__(self, parent, tda=0):
        super(Armour, self).__init__(parent, 'Armour')
        self.pwr = self.variant('Power armour', 0, gear=self.power_armour_set)
        self.tda = tda and self.variant('Terminator armour', tda)

    def is_tda(self):
        return self.cur == self.tda


class Icon(OptionsList):
    def __init__(self, parent, wrath=0):
        super(Icon, self).__init__(parent, name='Chaos Icon', limit=1, order=1000)
        self.wrath = self.variant('Icon of wrath', wrath)


class IconBearer(Unit):

    def __init__(self, parent, *args, **kwargs):
        wrath = kwargs.pop('wrath', 0)
        super(IconBearer, self).__init__(parent, *args, **kwargs)
        self.icon = Icon(self, wrath=wrath)

    def count_icon(self):
        return 1 if self.icon.any else 0


class IconBearerGroup(IconBearer, ListSubUnit):
    def count_icon(self):
        return super(IconBearerGroup, self).count_icon() * self.get_count()


class IconicUnit(Unit):

    def __init__(self, *args, **kwargs):
        super(IconicUnit, self).__init__(*args, **kwargs)
        self.champion = None
        self.models = None

    def check_rules(self):
        super(IconicUnit, self).check_rules()
        count = 0
        for u in [self.champion.unit] + self.models.units:
            count += u.count_icon()
        if count > 1:
            self.error("{0} can't take more then 1 icon (taken: {1}).".format(self.name, count))

    def get_count(self):
        return self.models.count + 1
