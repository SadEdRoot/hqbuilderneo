__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UnitDescription, Count, OneOf, OptionsList,\
    ListSubUnit, SubUnit, UnitList
from builder.games.wh40k.roster import Unit
from .armory import IconBearer, IconBearerGroup, IconicUnit
from .fast import RhinoTransport
from .heavy import LandRaiderTransport


class Possessed(IconBearer):
    type_name = "Possessed"
    type_id = "possessed_v1"

    model_points = 30
    model_gear = [Gear('Close combat weapon'), Gear('Power armour')]

    class Champion(IconBearer):
        def __init__(self, parent):
            super(Possessed.Champion, self).__init__(
                parent, points=150 - Possessed.model_points * 4, name='Possessed Champion',
                gear=Possessed.model_gear, wrath=20
            )

    class Count(Count):
        @property
        def description(self):
            model = UnitDescription('Possessed', points=Possessed.model_points, options=Possessed.model_gear)
            if self.parent.count_icon():
                return [model.clone().set_count(self.cur - 1),
                        model.add(self.parent.icon.description).add_points(self.parent.icon.points)]
            return [model.set_count(self.cur)]

    def __init__(self, parent):
        super(Possessed, self).__init__(parent, wrath=20, gear=[Gear('Mark of Khorne')])

        self.champion = SubUnit(self, self.Champion(None))
        self.models = self.Count(self, 'Possessed', 4, 19, self.model_points)

        self.transport = RhinoTransport(self)

    def get_count(self):
        return self.models.cur + 1

    def check_rules(self):
        super(Possessed, self).check_rules()
        count = 0
        for u in [self.champion.unit, self]:
            count += u.count_icon()
        if count > 1:
            self.error("{0} can't take more then 1 icon (taken: {1}).".format(self.name, count))

    def build_description(self):
        description = UnitDescription(name=self.name, points=self.points, count=self.count)
        for o in [self.champion, self.models, self.transport]:
            description.add(o.description)
        return description

    def build_statistics(self):
        return self.note_transport(super(Possessed, self).build_statistics())


class Terminators(IconicUnit):
    type_name = 'Chaos Terminators'
    type_id = 'chaos_terminators_v1'

    model_points = 34

    class TerminatorMelee(OneOf):
        def __init__(self, parent):
            super(Terminators.TerminatorMelee, self).__init__(parent, 'Melee weapon')
            self.variant('Power weapon', 0)
            self.claw = self.variant('Lightning claw', 3)
            self.variant('Power fist', 7)
            self.variant('Chainfist', 12)

    class TerminatorRanged(OneOf):
        def __init__(self, parent, melee, heavy=False):
            self.twin = melee
            super(Terminators.TerminatorRanged, self).__init__(parent, 'Ranged weapon')
            self.variant('Combi-bolter', 0)
            self.variant('Combi-flamer', 5)
            self.variant('Combi-melta', 5)
            self.variant('Combi-plasma', 5)
            self.claw = self.variant('Lightning claw', 4)
            self.heavy = []
            if heavy:
                self.heavy += [
                    self.variant('Heavy flamer', 10),
                    self.variant('Reaper autocannon', 25)
                ]

        def check_rules(self):
            super(Terminators.TerminatorRanged, self).check_rules()
            self.claw.active = self.twin.cur == self.twin.claw

        def is_heavy(self):
            return self.cur in self.heavy

    class Champion(IconBearer):

        def __init__(self, parent):
            super(Terminators.Champion, self).__init__(
                parent, points=104 - Terminators.model_points * 2, name='Terminator Champion',
                gear=[Gear('Terminator armour')], wrath=25)

            self.wep1 = Terminators.TerminatorMelee(self)
            self.wep2 = Terminators.TerminatorRanged(self, self.wep1, False)

    class Terminator(IconBearerGroup):
        def __init__(self, parent):
            super(Terminators.Terminator, self).__init__(
                parent, name='Chaos Terminator', points=Terminators.model_points, gear=[Gear('Terminator armour')],
                wrath=25)
            self.wep1 = Terminators.TerminatorMelee(self)
            self.wep2 = Terminators.TerminatorRanged(self, self.wep1, True)

        @ListSubUnit.count_gear
        def count_heavy(self):
            return self.wep2.is_heavy()

    def __init__(self, parent):
        super(Terminators, self).__init__(parent, gear=[Gear('Mark of Khorne')])
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Terminator, min_limit=2, max_limit=9)

        self.transport = LandRaiderTransport(self)

    def check_rules(self):
        super(Terminators, self).check_rules()
        spec_lim = int(self.get_count() / 5)
        taken = sum((u.count_heavy() for u in self.models.units))
        if taken > spec_lim:
            self.error("Chaos Terminators can't have more then 1 heavy weapon for 5 terminators (taken {}).".
                       format(taken))

    def get_count(self):
        return self.models.count + 1

    def build_statistics(self):
        return self.note_transport(super(Terminators, self).build_statistics())


class Bloodcrushers(Unit):
    type_id = 'bloodcrushers_v1'
    type_name = 'Bloodcrushers'
    model_points = 47
    model = UnitDescription('Bloodcrusher', points=model_points,
                            options=[Gear('Hellblade')])

    class Options(OptionsList):
        def __init__(self, parent):
            super(Bloodcrushers.Options, self).__init__(parent, 'Options')
            self.reaper = self.variant('Bloodhunter', 5)
            self.instrument = self.variant('Instrument of Chaos', 10)
            self.banner = self.variant('Banner of Blood', 20)

    def __init__(self, parent):
        super(Bloodcrushers, self).__init__(parent)
        self.models = Count(self, 'Bloodcrushers', 3, 9, self.model_points, per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur

    def build_description(self):
        desc = UnitDescription(self.name, self.points)
        diff = sum([o.value for o in self.opt.options])
        desc.add(self.model.clone().set_count(self.models.cur - diff))
        if self.opt.reaper.value:
            desc.add(UnitDescription(name='Bloodhunter', points=50, options=[Gear('Hellblade')]))
        if self.opt.banner.value:
            desc.add(self.model.clone().add(Gear('Banner of Blood')).add_points(self.opt.banner.points))
        if self.opt.instrument.value:
            desc.add(self.model.clone().add(Gear('Instrument of Chaos')).add_points(self.opt.instrument.points))
        return desc
