__author__ = 'Ivan Truskov'

from builder.core2 import Gear, Count, ListSubUnit, UnitList,\
    OneOf, OptionsList, UpgradeUnit, UnitDescription, SubUnit
from .fast import WolfTransport, DropPod, Stormwolf, Rhino, Razorback
from .heavy import LandRaider, LandRaiderRedeemer, LandRaiderCrusader
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedDreadnought, IATransportedUnit
from builder.games.wh40k.roster import Wh40kKillTeam
from .armory import BoltPistol, Chainsword,\
    MeleePlain, PowerWeapon, PlasmaPistol, Heavy, Armour,\
    TerminatorMelee, TerminatorRanged, Ranged, Melee, MeltaBombs,\
    Boltgun, Special, DredWeapon
from builder.games.wh40k.roster import Unit


class Servitors(Unit):
    type_id = 'servitors_v4'
    type_name = 'Servitors'

    class Servitor(ListSubUnit):

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Servitors.Servitor.Weapon, self).__init__(parent, 'Weapon')
                self.arm = self.variant('Servo-arm', 0)
                self.variant('Heavy bolter', 10)
                self.variant('Multi-melta', 10)
                self.variant('Plasma cannon', 20)

        @ListSubUnit.count_gear
        def has_weapon(self):
            return self.wep.cur != self.wep.arm

        def __init__(self, parent):
            super(Servitors.Servitor, self).__init__(parent, 'Servitor', 10)
            self.wep = self.Weapon(self)

    def __init__(self, parent):
        super(Servitors, self).__init__(parent, self.type_name)
        self.servos = UnitList(self, self.Servitor, 1, 5)

    def check_rules(self):
        super(Servitors, self).check_rules()
        wep_cnt = sum([u.has_weapon() for u in self.servos.units])
        if wep_cnt > 2:
            self.error("No more then 2 Servitors can take ranged weapons")

    def get_count(self):
        return self.servos.count


class WolfScouts(Unit):
    type_name = 'Wolf Scouts'
    type_id = 'scouts_v4'

    class Scout(ListSubUnit):
        model_gear = [Gear('Scout armour'),
                      Gear('Frag grenades'),
                      Gear('Krak grenades')]

        class ScoutWeapon(OneOf):
            def __init__(self, *args, **kwargs):
                super(WolfScouts.Scout.ScoutWeapon, self)\
                    .__init__(*args, **kwargs)
                self.variant('Space marine shotgun', 0)
                self.variant('Close combat weapon', 0)
                self.variant('Sniper rifle', 1)

        class Bolter(Heavy, Special, PowerWeapon, PlasmaPistol, ScoutWeapon,
                     Boltgun):
            pass

        class Pistol(Heavy, MeleePlain, BoltPistol):
            plain_name = 'Close combat weapon'

        def __init__(self, parent):
            super(WolfScouts.Scout, self).__init__(parent, 'Wolf Scout',
                                                   14, gear=self.model_gear)
            self.wep1 = self.Bolter(self, name='Weapon', scout=True)
            self.wep2 = self.Pistol(self, name='')

        def build_points(self):
            res = super(WolfScouts.Scout, self).build_points()
            if len(self.take_cloak()) > 0:
                res = res + 2
            return res

        def take_cloak(self):

            return [Gear('Camo cloak')] if self.root_unit.cloak.any else []

        def check_rules(self):
            if (self.wep1.is_heavy() or self.wep1.is_spec())\
               and self.wep2.is_heavy():
                self.error('Only one Special or heavy weapon may be carried')
            super(WolfScouts.Scout, self).check_rules()

        def build_description(self):
            desc = super(WolfScouts.Scout, self).build_description()
            desc.add(self.take_cloak())
            return desc

        @ListSubUnit.count_gear
        def has_plasma_power(self):
            return self.wep1.cur == self.wep1.pwr\
                or self.wep1.cur == self.wep1.ppist

        @ListSubUnit.count_gear
        def has_heavy_special(self):
            return (self.wep1.is_heavy() or self.wep1.is_spec())\
                or self.wep2.is_heavy()

    class WolfGuard(UpgradeUnit, Scout):

        class PistolWeapon(Ranged, Melee, BoltPistol):
            pass

        def make_upgraded_options(self):
            super(WolfScouts.WolfGuard, self).make_upgraded_options()
            rng = self.PistolWeapon(self, name="Weapon")
            opt = MeltaBombs(self)
            self.upgraded_options += [rng, opt]

        def __init__(self, parent):
            super(WolfScouts.WolfGuard, self).__init__(
                parent, upgraded_name='Wolf Guard Pack Leader',
                upgrade_cost=10, upgraded_gear=[
                    Gear('Power armour'), Gear('Boltgun'),
                    Gear('Frag grenades'), Gear('Kreak grenades')
                ])

        def take_cloak(self):
            return [] if self.up.any else\
                super(WolfScouts.WolfGuard, self).take_cloak()

        @ListSubUnit.count_gear
        def is_guard(self):
            return self.up.any

    class Camouflage(OptionsList):
        def __init__(self, parent):
            super(WolfScouts.Camouflage, self).__init__(parent, 'Camouflage')
            self.variant('Camo cloaks', 2, per_model=True, gear=[])

        @property
        def points(self):
            return 0

    def __init__(self, parent):
        super(WolfScouts, self).__init__(parent, self.type_name)
        self.marines = UnitList(self, self.WolfGuard, 5, 10)
        self.cloak = self.Camouflage(self)

    def get_count(self):
        return self.marines.count

    def check_rules(self):
        super(WolfScouts, self).check_rules()

        guards = sum(u.is_guard() for u in self.marines.units)
        if guards > 1:
            self.error("Only one model can be upgraded to Wolf Guard Pack Leader")

        if sum(u.has_plasma_power() for u in self.marines.units) > 2:
            self.error('Only two Wolf Scouts may take plasma pistol or power weapon')
        if sum(u.has_heavy_special() for u in self.marines.units) > 1:
            self.error('Only one Wolf Scout can take heavy or special weapon')


class LoneWolf(Unit):
    type_name = 'Lone Wolf'
    type_id = 'lone_wolf_v4'

    class WeaponMelee(TerminatorMelee, Ranged, Melee, Chainsword):
        pass

    class WeaponRanged(TerminatorRanged, Ranged, Melee, BoltPistol):
        pass

    def __init__(self, parent):
        super(LoneWolf, self).__init__(parent, self.type_name, 20)

        ar = Armour(self, tda=(not type(self.parent.roster) is Wh40kKillTeam) and 15)
        self.WeaponMelee(self, name="Weapon", armour=ar)
        self.WeaponRanged(self, name="Weapon", armour=ar)
        MeltaBombs(self)

        Count(self, 'Fenrisian Wolf', 0, 2, 8, per_model=True)


class Dreadnought(IATransportedDreadnought):
    type_name = 'Dreadnought'
    type_id = 'dreadnought_v4'

    class BuildIn(OneOf):
        def __init__(self, parent, mount):
            super(Dreadnought.BuildIn, self).__init__(parent=parent, name='Built-in weapon')
            self.mount = mount
            self.variant('Storm Bolter', 0)
            self.variant('Heavy Flamer', 10)

        def check_rules(self):
            super(Dreadnought.BuildIn, self).check_rules()
            self.used = self.visible = self.mount.allow_builtin()

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.extraarmour = self.variant('Extra Armour', 10)
            self.smokelaunchers = self.variant('Smoke launchers', 25)
            self.venerable = self.variant('Upgrade to Venerable Dreadnough', 25, gear=[])

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon1, self).__init__(parent, 'Weapon')
            self.pfist = self.variant('Power fist', 0)
            self.wclaw = self.variant('Great Wolf claw', 5)
            self.missilelauncher = self.variant('Missile Launcher ', 10)
            self.acan = self.variant('Twin-linked autocannon', 15)
            self.faxe = self.variant('Fenrisian great axe and blizzard shield', 25,
                                     gear=[Gear('Fenrisian great axe'), Gear('Blizzard shield')])

        def allow_builtin(self):
            return self.cur in [self.pfist, self.wclaw]

        def check_rules(self):
            self.faxe.used = self.faxe.visible = self.parent.is_venerable()

    def __init__(self, parent):
        super(Dreadnought, self).__init__(
            parent=parent, points=95, walker=True,
            gear=[Gear('Searchlight')]
        )
        self.wep1 = self.Weapon1(self)
        self.buildin = self.BuildIn(self, self.wep1)
        self.wep2 = DredWeapon(self)
        self.opt = self.Options(self)
        self.transport = WolfTransport(self, [DropPod])

    def is_venerable(self):
        return self.opt.venerable.value

    def build_description(self):
        desc = super(Dreadnought, self).build_description()
        if self.is_venerable():
            desc.name = 'Venerable Dreadnought'
        return desc

    def check_rules(self):
        self.wep2.used = self.wep2.visible = not self.wep1.cur == self.wep1.faxe


class Murderfang(Unit):
    type_id = 'murderfang_v4'
    type_name = 'Murderfang the Curseborn'

    def __init__(self, parent):
        super(Murderfang, self).__init__(parent, 'Murderfang', 135, gear=[
            Gear('Searchlight'), Gear('The Murderclaws')
        ], unique=True)
        self.transport = WolfTransport(self, [DropPod])

    def build_statistics(self):
        return self.note_transport(super(Murderfang, self).build_statistics())


class WolfGuards(IATransportedUnit):
    type_name = "Wolf Guard"
    type_id = "wolf_guards_v4"
    transport_allowed = True

    class WolfGuardLeader(Unit):
        type_name = 'Wolf Guard Pack Leader'

        class Weapon1(Melee, Ranged, Boltgun, BoltPistol):
            pass

        class Weapon2(Melee, Ranged, Boltgun, Chainsword):
            pass

        def __init__(self, parent):
            super(WolfGuards.WolfGuardLeader, self).__init__(parent, self.type_name, 18, gear=[
                Gear('Power armour'), Gear('Frag grenades'), Gear('Krak grenades')
            ])
            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.opt = MeltaBombs(self)

        def get_unit_options(self):
            return self.parent.parent.ride

        def build_description(self):
            desc = super(WolfGuards.WolfGuardLeader, self).build_description()
            desc.add(self.get_unit_options().description)
            return desc

        def build_points(self):
            pts = super(WolfGuards.WolfGuardLeader, self).build_points()
            ride = self.get_unit_options()
            # points in parent entity are redefined
            pts += sum(o.points for o in ride.options if o.value and o.used and o.points is not None)
            return pts

    class Marine(WolfGuardLeader, ListSubUnit):
        type_name = "Wolf Guard"

        # redefinition
        def get_unit_options(self):
            return self.root_unit.ride

    class Ride(OptionsList):
        def __init__(self, parent):
            super(WolfGuards.Ride, self).__init__(parent, 'Movement options', limit=1)
            self.pack = self.variant('Jump pack', 3, per_model=True)
            self.bike = self.variant('Space Marine bike', 7, per_model=True)

    def __init__(self, parent):
        super(WolfGuards, self).__init__(parent, name="Wolf Guard")
        self.ldr = SubUnit(self, self.WolfGuardLeader(parent=None))
        self.models = UnitList(self, self.Marine, 4, 9)
        self.ride = self.Ride(self)
        if self.transport_allowed:
            self.transport = WolfTransport(self, [Rhino, Razorback, DropPod,
                                                  Stormwolf, LandRaider,
                                                  LandRaiderCrusader, LandRaiderRedeemer])

    def get_count(self):
        return self.models.count + 1

    def build_points(self):
        return self.ldr.points + self.models.points + (self.transport.points if self.transport else 0)

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points,
                               count=self.count)
        desc.add(self.ldr.description)
        desc.add(self.models.description)
        if self.transport_allowed:
            desc.add(self.transport.description)
        return desc

    def has_bike(self):
        return self.ride.bike.value

    def has_jump(self):
        return self.ride.pack.value


class WolfGuardTerminators(IATransportedUnit):
    type_name = 'Wolf Guard Terminators'
    type_id = 'wolf_guard_term_v4'
    transport_allowed = True

    class Leader(Unit):
        type_name = 'Wolf Guard Terminator Leader'

        class WeaponRanged(TerminatorRanged):
            def __init__(self, parent):
                super(WolfGuardTerminators.Leader.WeaponRanged, self).__init__(
                    parent, 'Ranged weapon')
                self.heavyflamer = self.variant('Heavy flamer', 10)
                self.assaultcannon = self.variant('Assault cannon', 20)
                self.tda_weapon += [self.heavyflamer, self.assaultcannon]

        class WeaponMelee(TerminatorMelee):
            pass

        class Missiles(OptionsList):
            def __init__(self, parent):
                super(WolfGuardTerminators.Leader.Missiles, self).__init__(parent, 'Extra weapon')
                self.variant('Cyclone missile launcher', 25)

        def __init__(self, parent):
            super(WolfGuardTerminators.Leader, self).__init__(
                parent, self.type_name, 33, [Gear('Terminator armour')])
            self.rng = self.WeaponRanged(self)
            self.mle = self.WeaponMelee(self, name='Melee weapon')
            self.cyclone = self.Missiles(self)

        def has_restricted(self):
            return self.rng.cur in [self.rng.heavyflamer, self.rng.assaultcannon]\
                or self.cyclone.any

        def check_rules(self):
            if self.rng.cur in [self.rng.heavyflamer, self.rng.assaultcannon]\
               and self.cyclone.any:
                self.error('Cannot take heavy weapon together with missile launcher')

        def build_points(self):
            res = super(WolfGuardTerminators.Leader, self).build_points()
            if self.rng.cur == self.rng.tda_wc\
               and self.mle.cur == self.mle.tda_wc2:
                res = res - 10
            if self.rng.cur == self.rng.tda_ham\
               and self.mle.cur == self.mle.tda_ss:
                res = res - 10
            return res

        def has_claws(self):
            return self.rng.cur == self.rng.tda_wc and self.mle.cur == self.mle.tda_wc2

    class Marine(Leader, ListSubUnit):
        type_name = 'Wolf Guard Terminator'

        @ListSubUnit.count_gear
        def count_restricted(self):
            return self.has_restricted()

    def __init__(self, parent):
        super(WolfGuardTerminators, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Leader(parent=None))
        self.marines = UnitList(self, self.Marine, 2, 9)
        if self.transport_allowed:
            self.transport = WolfTransport(self, [
                LandRaider, LandRaiderCrusader, LandRaiderRedeemer])

    def get_count(self):
        return 1 + self.marines.count

    def check_rules(self):
        super(WolfGuardTerminators, self).check_rules()
        limit = self.get_count() / 5
        taken = sum(u.count_restricted() for u in self.marines.units)\
            + (1 if self.ldr.unit.has_restricted() else 0)
        if taken > limit:
            self.error('Only one heavy weapon or missile launcher allowed per 5 terminators; taken: {} (in {})'.format(taken, self.count))

    def all_claws(self):
        # import pdb; pdb.set_trace()
        return self.ldr.unit.has_claws() and all(u.has_claws() for u in self.marines.units)


class Arjac(Unit):
    type_name = 'Arjac Rockfist, Grimnar\'s champion'
    type_id = 'arjac_v4'

    def __init__(self, parent):
        super(Arjac, self).__init__(parent, 'Arjac Rockfist', 115, gear=[
            Gear('Terminator armour'), Gear('Anvil Shield'), Gear('Foehammer')
        ], static=True, unique=True)


class Wulfen(IATransportedUnit):
    type_name = "Wulfen"
    type_id = "wulfen_v4"

    class Marine(ListSubUnit):

        class Launchers(OptionsList):
            def __init__(self, parent):
                super(Wulfen.Marine.Launchers, self).__init__(parent, "Launcher")
                self.variant("Stormfrag auto-launcher", 2)

        class Melee(OptionsList):
            def __init__(self, parent):
                super(Wulfen.Marine.Melee, self).__init__(parent, 'Melee weapon')
                self.variant('Great frost axe', 8)
                self.variant('Two frost claws', 12, gear=[Gear('Frost claws', count=2)])
                self.variant(
                    'Thunder hammer and storm shield', 20, gear=[Gear('Thunder hammer'), Gear('Storm shield')])

        def __init__(self, parent):
            super(Wulfen.Marine, self).__init__(
                parent=parent, name='Wulfen', points=30,
                gear=[
                    Gear('Close combat weapon')
                ]
            )
            self.wep1 = self.Melee(self)
            self.Launchers(self)

    class PackLeader(UpgradeUnit, Marine):
        def __init__(self, parent):
            super(Wulfen.PackLeader, self).__init__(
                parent, upgraded_name='Wulfen Pack Leader',
                upgraded_gear=[
                    Gear('Frost claws', count=2)
                ],
                upgrade_cost=20)

        def make_upgraded_options(self):
            super(Wulfen.PackLeader, self).make_upgraded_options()
            opt = self.Launchers(self)
            self.upgraded_options += [opt]

        @ListSubUnit.count_gear
        def is_leader(self):
            return self.up.any

    def __init__(self, parent):
        super(Wulfen, self).__init__(parent)
        self.marines = UnitList(self, self.PackLeader, 5, 10)
        self.transport = WolfTransport(self, transport_types=[
            Stormwolf
        ])

    def check_rules(self):
        super(Wulfen, self).check_rules()

        leaders = sum(u.is_leader() for u in self.marines.units)
        if leaders > 1:
            self.error("Only one model can be upgraded to Wolfen Pack Leader")

    def get_count(self):
        return self.marines.count


class FierceEyeWolfGuard(Unit):
    type_name = "Fierce-Eye's Wolf Guard"
    type_id = 'eye_wolf_guard_v1'

    def __init__(self, parent):
        common = [Gear('Terminator armour')]
        super(FierceEyeWolfGuard, self).__init__(parent, points=230, static=True, gear=[
            UnitDescription('Wolf Guard Terminator', options=[Gear('Thunder hammer'), Gear('Storm shield')] + common),
            UnitDescription('Wolf Guard Terminator', options=[Gear('Heavy flamer'), Gear('Power fist')] + common),
            UnitDescription('Wolf Guard Terminator', options=[Gear('Storm bolter'), Gear('Power fist')] + common),
            UnitDescription('Wolf Guard Terminator', options=[Gear('Storm bolter'), Gear('Chainfist')] + common),
            UnitDescription('Beoric Winterfang', options=[Gear('Storm bolter'), Gear('Frost sword')] + common)
        ])

    def get_count(self):
        return 5

    def get_unique(self):
        return 'Beoric Winterfang'


from .hq import WolfMaster
from .armory import Options


class IronPriestOld(WolfMaster):
    type_id = 'iron_priest_v4_old'
    type_name = 'Iron Priest'

    class Bolter(BoltPistol, Boltgun):
        pass

    def __init__(self, parent):
        super(IronPriestOld, self).__init__(parent, self.type_name, 55,
                                            gear=[
                                                Gear('Runic Armour'),
                                                Gear('Frag Grenades'),
                                                Gear('Krak Grenades'),
                                                Gear('Servo-arm'),
                                                Gear('Thunder hammer')
                                            ])
        self.wep1 = self.Bolter(self, name='Weapon')
        self.opt = Options(self, iron_priest=True, wolf_mount=50)
        self.wolves = Count(self, 'Cyberwolf', 0, 4, 15, per_model=True)
