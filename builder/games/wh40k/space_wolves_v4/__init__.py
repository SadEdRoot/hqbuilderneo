__author__ = 'Denis Romanov'

from builder.games.wh40k.roster import Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Wh40kBase, Wh40kKillTeam,\
    CompositeFormation, PrimaryDetachment,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, LordsOfWarSection, UnitType, Detachment,\
    FlyerSection
from builder.games.wh40k.fortifications import Fort

from builder.games.wh40k.escalation.space_marines\
    import LordsOfWar as MarinesLordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour import volume2
from builder.games.wh40k.imperial_armour.volume11\
    import space_wolves as volume11
from builder.games.wh40k.imperial_armour.dataslates.space_marines import Xiphon, Leviathan
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from builder.games.wh40k.imperial_armour.aeronautika.space_marines import HyperiosBattery
from builder.games.wh40k.dataslates.command_tanks import SpaceMarineCommandTanks
from .hq import WolfLord, Ragnar, Harald, Canis, RunePriest, WolfPriest,\
    Njal, Ulrik, BattleLeader, Bjorn, IronPriest, Krom, KromDragongaze
from .elites import Servitors, WolfScouts, Dreadnought,\
    Murderfang, WolfGuards, WolfGuardTerminators, Arjac, LoneWolf, Wulfen,\
    FierceEyeWolfGuard, IronPriestOld
from .troops import BloodClaws, Lucas, GreyHunters, IronaxeGreyHunters,\
    RedfistBloodClaws
from .fast import DropPod, Rhino, Razorback, Stormwolf, Swiftclaws,\
    Skyclaws, WolfPack, ThunderwolfCavalry, LandSpeederSquadron, Stormwolves
from .heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer,\
    Stormfang, Whirlwinds, Vindicators, Predators, LongFangs, Stormfangs,\
    DeimosVindicator
from .lords import Logan
from .formations import GreatCompany, Firehowlers, Ironwolves,\
    Drakeslayers, Deathwolves, Blackmanes, ChampionsWulfen,\
    Greatpack, WulfenMurderpack, Spear, Wyrdstorm, Ancients,\
    Heralds, Wolfkin, Daggerfist, Deathpack, Stormforce, Brethen,\
    Voidclaws, Council, Shieldbrothers, Thunderstrike, Champions,\
    IceStorm, FierceEyeFinest, IronclawStrikeForce, WulfenFormation


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        UnitType(self, WolfLord)
        UnitType(self, Ragnar)
        UnitType(self, Krom)
        UnitType(self, KromDragongaze)
        UnitType(self, Harald)
        UnitType(self, Canis)
        self.rune = UnitType(self, RunePriest)
        UnitType(self, Njal)
        UnitType(self, WolfPriest)
        UnitType(self, Ulrik)
        UnitType(self, BattleLeader)
        UnitType(self, Bjorn)
        self.priest = UnitType(self, IronPriest)
        UnitType(self, SpaceMarineCommandTanks)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.priest = UnitType(self, IronPriestOld)
        self.servs = UnitType(self, Servitors)
        UnitType(self, WolfScouts)
        self.lonewolves = UnitType(self, LoneWolf, slot=0)
        UnitType(self, Dreadnought)
        UnitType(self, Murderfang)
        self.guards = UnitType(self, WolfGuards)
        self.terminators = UnitType(self, WolfGuardTerminators)
        self.champion = UnitType(self, Arjac)
        self.wulfen = UnitType(self, Wulfen)
        self.guards2 = UnitType(self, FierceEyeWolfGuard)

    def check_limits(self):
        self.servs.slot = 1 if (self.parent.hq.priest.count == 0 and
                                self.priest.count == 0) else 0
        self.champion.slot = 1 if (self.guards.count + self.terminators.count + self.guards2.count == 0) else 0
        return super(BaseElites, self).check_limits()


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.bcl = UnitType(self, BloodClaws)
        self.lucas = UnitType(self, Lucas)
        self.hunters = UnitType(self, GreyHunters)
        UnitType(self, IronaxeGreyHunters)
        self.bcl2 = UnitType(self, RedfistBloodClaws)

    def check_limits(self):
        self.lucas.slot = 1 if (self.bcl.count == 0 and self.bcl2.count == 0) else 0
        return super(Troops, self).check_limits()


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, DropPod)
        UnitType(self, Rhino)
        UnitType(self, Razorback)
        plane = UnitType(self, Stormwolf)
        plane.visible = False
        UnitType(self, Stormwolves)
        UnitType(self, Swiftclaws)
        UnitType(self, Skyclaws)
        UnitType(self, WolfPack)
        UnitType(self, ThunderwolfCavalry)
        UnitType(self, LandSpeederSquadron)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, LandRaider)
        UnitType(self, LandRaiderCrusader)
        UnitType(self, LandRaiderRedeemer)
        plane = UnitType(self, Stormfang)
        plane.visible
        UnitType(self, Stormfangs)
        UnitType(self, Vindicators)
        UnitType(self, Whirlwinds)
        UnitType(self, Predators)
        UnitType(self, LongFangs)


class BaseLords(LordsOfWarSection):
    def __init__(self, parent):
        super(BaseLords, self).__init__(parent)
        UnitType(self, Logan)


class HQ(volume2.SpaceMarineHQ, volume11.IA11HQSection, BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class FastAttack(volume2.SpaceMarineFast, BaseFastAttack):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, HyperiosBattery)
        UnitType(self, Xiphon)


class Elites(volume2.SpaceWolvesElites, BaseElites):
    pass


class HeavySupport(volume2.SpaceMarineHeavySupport, BaseHeavySupport):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        UnitType(self, DeimosVindicator)
        self.relics.append(UnitType(self, Leviathan))


class Lords(CerastusSection, volume2.SpaceMarineLordsOfWar, Titans,
            MarinesLordsOfWar, BaseLords):
    def __init__(self, parent):
        super(Lords, self).__init__(parent)
        from builder.games.wh40k.space_marines_v3 import Guilliman
        UnitType(self, Guilliman)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Stormwolves, Stormfangs])


class SpaceWolvesV4Base(volume2.SpaceMarinesRelicRoster):
    # army_id = 'space_wolves_v4'
    # army_name = 'Space Wolves'

    def is_champion(self):
        return False

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(SpaceWolvesV4Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            lords=Lords(self), fort=Fort(parent=self)
        )

    def check_rules(self):
        super(SpaceWolvesV4Base, self).check_rules()
        banners = sum(u.has_banner() for u in self.troops.hunters.units)
        if banners > 1:
            self.error("Only one Wolf Standard may be taken per Detachment")
        lone_limit = sum(o.count for o in self.troops.types + [
            self.elites.guards, self.elites.terminators, self.elites.guards2
        ])
        if self.elites.lonewolves.count > lone_limit:
            self.error('Number of Lone Wolves must be lower then number of troops and Wolf Guard units')

    def has_keeper(self):
        return self.hq.rune.count > 0


class GreatWolfCompany(SpaceWolvesV4Base,
                       volume2.SpaceMarinesRelicRoster):
    army_id = 'space_wolves_v4_great_company'
    army_name = 'Company of the Great Wolf Detachment'

    def is_champion(self):
        return True

    def __init__(self):
        self.hq = HQ(parent=self)
        self.hq.cypher.visible = False
        self.hq.min, self.hq.max = (1, 4)
        self.elites = Elites(parent=self)
        self.elites.min, self.elites.max = (2, 6)
        self.troops = Troops(parent=self)
        self.troops.min, self.troops.max = (0, 3)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        # note: we skip SpaceWolvesV4Base constructor here
        super(SpaceWolvesV4Base, self).__init__(
            sections=[self.hq, self.elites, self.troops, self.fast,
                      self.heavy, Fort(parent=self), BaseLords(self)]
        )


class WolvesUnleashed(SpaceWolvesV4Base,
                       volume2.SpaceMarinesRelicRoster):
    army_id = 'space_wolves_v4_wolves_unleashed'
    army_name = 'The Wolves Unleashed Detachment'

    def is_champion(self):
        return False

    def __init__(self):
        self.hq = HQ(parent=self)
        self.hq.cypher.visible = False
        self.hq.min, self.hq.max = (2, 6)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        # note: we skip SpaceWolvesV4Base constructor here
        super(SpaceWolvesV4Base, self).__init__(
            sections=[self.hq, self.elites, self.troops, self.fast,
                      self.heavy, Fort(parent=self), BaseLords(self)]
        )


class Lord(WulfenFormation):
    army_id = 'space_wolves_v4_lord'
    army_name = 'Lord of the Fang'

    def __init__(self):
        super(Lord, self).__init__()
        lorg = [
            UnitType(self, Logan),
            UnitType(self, WolfPriest),
            UnitType(self, Ulrik),
            UnitType(self, Njal),
            UnitType(self, Bjorn),
            UnitType(self, Arjac),
            UnitType(self, Canis),
        ]
        self.add_type_restriction(lorg, 1, 1)


class Fangs(WulfenFormation):
    army_id = 'space_wolves_v4_fangs'
    army_name = 'Fangs of the Tempest'

    def __init__(self):
        super(Fangs, self).__init__()
        lorg = [
            UnitType(self, Stormfang),
            UnitType(self, Stormwolf)
        ]
        self.add_type_restriction(lorg, 1, 1)


class Curseborn(WulfenFormation):
    army_id = 'space_wolves_v4_wulfen_curseborn'
    army_name = 'The Curseborn'

    def __init__(self):
        super(Curseborn, self).__init__()
        UnitType(self, Murderfang, min_limit=1, max_limit=1)


class LegendaryGreatpack(WulfenFormation, CompositeFormation):
    army_id = 'space_wolves_v4_legendary_greatpack'
    army_name = 'Legendary Greatpack'

    def __init__(self):
        super(LegendaryGreatpack, self).__init__()
        self.add_type_restriction([
            Detachment.build_detach(self, Firehowlers),
            Detachment.build_detach(self, Ironwolves),
            Detachment.build_detach(self, Drakeslayers),
            Detachment.build_detach(self, Deathwolves),
            Detachment.build_detach(self, Blackmanes),
            Detachment.build_detach(self, ChampionsWulfen),
        ], 1, 1)


class StrikeForce(Wh40k7ed):
    army_id = 'space_wolves_v4_strike_force'
    army_name = 'Wolf Claw Strike Force'

    class Core(Detachment):
        def __init__(self, parent):
            super(StrikeForce.Core, self).__init__(parent, 'core', 'Core',
                                                 [Greatpack, LegendaryGreatpack], 1, None)

    class Command(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(StrikeForce.Command, self).__init__(parent, 'command', 'Command',
                                                    [Lord, Heralds, Wyrdstorm], 0, 5)

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(StrikeForce.Auxilary, self).__init__(parent, 'auxilary', 'Auxilary',
                                                    [Spear,
                                                     Fangs,
                                                     Ancients,
                                                     Curseborn,
                                                     WulfenMurderpack,
                                                     Wolfkin], 1, None)

    def __init__(self):
        core = self.Core(self)
        command = self.Command(self, core)
        aux = self.Auxilary(self, core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


class SpaceWolvesV4CAD(SpaceWolvesV4Base, CombinedArmsDetachment):
    army_id = 'space_wolves_v4_cad'
    army_name = 'Space Wolves (Combined arms detachment)'


class SpaceWolvesV4AD(SpaceWolvesV4Base, AlliedDetachment):
    army_id = 'space_wolves_v4_ad'
    army_name = 'Space Wolves (Allied detachment)'


class SpaceWolvesV4PA(SpaceWolvesV4Base, PlanetstrikeAttacker):
    army_id = 'space_wolves_v4_pa'
    army_name = 'Space Wolves (Planetstrike attacker detachment)'


class SpaceWolvesV4PD(SpaceWolvesV4Base, PlanetstrikeDefender):
    army_id = 'space_wolves_v4_pd'
    army_name = 'Space Wolves (Planetstrike defender detachment)'


class SpaceWolvesV4SA(SpaceWolvesV4Base, SiegeAttacker):
    army_id = 'space_wolves_v4_sa'
    army_name = 'Space Wolves (Siege War attacker detachment)'


class SpaceWolvesV4SD(SpaceWolvesV4Base, SiegeDefender):
    army_id = 'space_wolves_v4_sd'
    army_name = 'Space Wolves (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'space_wolves_v4_asd'
    army_name = 'Space Wolves (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class SpaceWolvesV4KillTeam(Wh40kKillTeam):
    army_id = 'space_wolves_v4_kt'
    army_name = 'Space Wolves'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(SpaceWolvesV4KillTeam.KTTroops, self).__init__(parent)
            proc = Wh40kKillTeam.process_transported_units(self, [
                BloodClaws, GreyHunters
            ], lambda u: u.transport.transports[2:])
            self.claws = proc[0]
            self.lucas = UnitType(self, Lucas)
            UnitType(self, IronaxeGreyHunters)
            self.claws2 = UnitType(self, RedfistBloodClaws)

            def check_limits(self):
                self.lucas.slot = 1 if (self.claws.count == 0 and self.claws2.count == 0) else 0
                return super(Troops, self).check_limits()

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(SpaceWolvesV4KillTeam.KTElites, self).__init__(parent)
            proc = Wh40kKillTeam.process_transported_units(self, [
                WolfGuards
            ], lambda u: u.transport.transports[2:])
            self.guards = proc[0]
            self.servs = UnitType(self, Servitors)
            UnitType(self, WolfScouts)
            self.lonewolves = UnitType(self, LoneWolf, slot=0)

            self.wulfen = UnitType(self, Wulfen)
            self.guards2 = UnitType(self, FierceEyeWolfGuard)

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(SpaceWolvesV4KillTeam.KTFastAttack, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [
                Rhino, Razorback, LandSpeederSquadron
            ])
            UnitType(self, Swiftclaws)
            UnitType(self, Skyclaws)
            UnitType(self, WolfPack)
            UnitType(self, ThunderwolfCavalry)

    def __init__(self):
        self.troops = self.KTTroops(self)
        self.elites = self.KTElites(self)
        super(SpaceWolvesV4KillTeam, self).__init__(
            self.troops, self.elites, self.KTFastAttack(self)
        )

    def check_rules(self):
        super(SpaceWolvesV4KillTeam, self).check_rules()
        lone_limit = sum(o.count for o in self.troops.types + [
            self.elites.guards, self.elites.guards2
        ])
        if self.elites.lonewolves.count > lone_limit:
            self.error('Number of Lone Wolves must be lower then number of troops and Wolf Guard units')


faction = 'Space wolves'


class SpaceWolvesV4(Wh40k7ed, Wh40kImperial):
    army_id = 'space_wolves_v4'
    army_name = 'Space Wolves'
    faction = faction

    def __init__(self):
        super(SpaceWolvesV4, self).__init__([
            SpaceWolvesV4CAD, StrikeForce, GreatWolfCompany, WolvesUnleashed, FlierDetachment,
            GreatCompany, Firehowlers, Ironwolves, Drakeslayers, Deathwolves, Blackmanes,
            ChampionsWulfen, Greatpack, WulfenMurderpack, Spear, Wyrdstorm, Ancients, Heralds, Wolfkin,
            Daggerfist, Deathpack, IceStorm, FierceEyeFinest,
            Stormforce, Brethen, Voidclaws, Council,
            Shieldbrothers, Thunderstrike, Champions, IronclawStrikeForce
        ], [])


class SpaceWolvesV4Missions(Wh40k7edMissions, Wh40kImperial):
    army_id = 'space_wolves_v4_mis'
    army_name = 'Space Wolves'
    faction = faction

    def __init__(self):
        super(SpaceWolvesV4Missions, self).__init__([
            SpaceWolvesV4CAD, SpaceWolvesV4PA, SpaceWolvesV4PD, SpaceWolvesV4SA,
            SpaceWolvesV4SD, StrikeForce, GreatWolfCompany, WolvesUnleashed, FlierDetachment,
            GreatCompany, Firehowlers, Ironwolves, Drakeslayers, Deathwolves, Blackmanes,
            ChampionsWulfen, Greatpack, WulfenMurderpack, Spear, Wyrdstorm, Ancients, Heralds, Wolfkin,
            Daggerfist, Deathpack, IceStorm, FierceEyeFinest,
            Stormforce, Brethen, Voidclaws, Council,
            Shieldbrothers, Thunderstrike, Champions, IronclawStrikeForce
        ], [])


detachments = [
    SpaceWolvesV4CAD,
    SpaceWolvesV4AD,
    SpaceWolvesV4PA,
    SpaceWolvesV4PD,
    SpaceWolvesV4SA,
    SpaceWolvesV4SD,
    StrikeForce,
    GreatWolfCompany,
    WolvesUnleashed,
    FlierDetachment,
    GreatCompany,
    Firehowlers,
    Ironwolves,
    Drakeslayers,
    Deathwolves,
    Blackmanes,
    ChampionsWulfen,
    Greatpack,
    WulfenMurderpack,
    Spear,
    Wyrdstorm,
    Ancients,
    Heralds,
    Wolfkin,
    Daggerfist,
    Deathpack,
    Stormforce,
    Brethen,
    Voidclaws,
    Council,
    Shieldbrothers,
    Thunderstrike,
    Champions,
    IceStorm,
    FierceEyeFinest,
    IronclawStrikeForce
]


unit_types = [
    WolfLord, Ragnar, Harald, Canis, RunePriest, WolfPriest,
    Njal, Ulrik, BattleLeader, Bjorn, IronPriest, Krom, KromDragongaze,
    Servitors, WolfScouts, Dreadnought,
    Murderfang, WolfGuards, WolfGuardTerminators, Arjac, LoneWolf, Wulfen,
    FierceEyeWolfGuard,
    BloodClaws, Lucas, GreyHunters, IronaxeGreyHunters,
    RedfistBloodClaws,
    DropPod, Rhino, Razorback, Stormwolf, Swiftclaws,
    Skyclaws, WolfPack, ThunderwolfCavalry, LandSpeederSquadron, Stormwolves,
    LandRaider, LandRaiderCrusader, LandRaiderRedeemer,
    Whirlwinds, Vindicators, Predators, LongFangs, Stormfangs,
    Logan
]
