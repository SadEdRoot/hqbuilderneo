__author__ = 'Denis Romanov'

from builder.core2 import Gear, OptionsList
from builder.games.wh40k.roster import Unit

ia_id = ' (IA vol.11)'


class Irillyth(Unit):
    clear_name = 'Irillyth, Shade of Twilight'
    type_name = clear_name + ia_id
    type_id = 'irillythshadeoftwilight_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Irillyth, self).__init__(parent=parent, points=180, unique=True, name=self.clear_name, gear=[
            Gear('Spear of Starlight'),
            Gear('Shadow Spectre jet pack'),
            Gear('Spectre Holo-fields'),
            Gear('Haywire grenade'),
            Gear('Plasma grenades')
        ])


class BelAnnath(Unit):
    clear_name = 'Bel-Annath'
    type_name = clear_name + ia_id
    type_id = 'belannath_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(BelAnnath, self).__init__(parent=parent, points=150, unique=True, name=self.clear_name, gear=[
            Gear('Ghosthelm'), Gear('Fusion Pistol'), Gear('Rune Armour'),
            Gear('The Sundered Spear')
        ])


class Wraithseer(Unit):
    clear_name = 'Eldar Wraithseer'
    type_name = clear_name + ia_id
    type_id = 'wraithseer_v2'
    imperial_armour = True

    def __init__(self, parent):
        super(Wraithseer, self).__init__(parent=parent, name=self.clear_name, points=185,
                                         gear=[Gear('Ghostspear'), Gear('Wraithshield')])
        self.Weapon(self)

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Wraithseer.Weapon, self).__init__(parent=parent, name='Weapon', limit=1)
            self.variant('Bright Lance', 20)
            self.variant('Scatter Laser', 15)
            self.variant('Eldar Missile Launcher', 25)
            self.variant('Star Cannon', 15)
            self.variant('Shuriken cannon', 10)
            self.variant('D-Cannon', 60)
            self.variant('Wraithcannon', 45)
