from builder.core2 import OneOf, Gear, Count
from builder.games.wh40k.eldar_v3.armory import VehicleEquipment
from builder.games.wh40k.roster import Unit

__author__ = 'Denis Romanov'

ia_id = ' (IA vol.11)'


class Scorpion(Unit):
    clear_name = 'Scorpion'
    type_name = 'Eldar Scorpion' + ia_id
    type_id = 'ia11_scorpion_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Scorpion, self).__init__(parent=parent, points=650, gear=[Gear('Twin-linked Pulsar')],
                                       name=self.clear_name)
        self.Weapon(self)
        VehicleEquipment(self, squadron=False)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Scorpion.Weapon, self).__init__(parent=parent, name='Weapon')

            self.shurikencannon = self.variant('Shuriken Cannon', 0)
            self.scatterlaser = self.variant('Scatter Laser', 0)
            self.starcannon = self.variant('Starcannon', 5)
            self.brightlance = self.variant('Bright Lance', 5)


class Cobra(Unit):
    clear_name = 'Cobra'
    type_name = 'Eldar Cobra' + ia_id
    type_id = 'ia11_cobra_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Cobra, self).__init__(parent=parent, points=675, gear=[Gear('D-Cannon')], name=self.clear_name)
        self.Weapon(self)
        VehicleEquipment(self, squadron=False)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Cobra.Weapon, self).__init__(parent=parent, name='Weapon')

            self.shurikencannon = self.variant('Shuriken Cannon', 0)
            self.scatterlaser = self.variant('Scatter Laser', 0)
            #self.eldarmissilelauncher = self.variant('Eldar Missile Launcher', 15)
            self.starcannon = self.variant('Starcannon', 5)
            self.brightlance = self.variant('Bright Lance', 5)


class VampireRaider(Unit):
    clear_name = 'Vampire'
    type_name = 'Eldar Vampire Raider' + ia_id
    type_id = 'ia11_vampire_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(VampireRaider, self).__init__(parent=parent, points=730,
                                            gear=[Gear('Scatter Laser'), Gear('Improved holo-fields')],
                                            name=self.clear_name)
        self.Weapons(self)

    class Weapons(OneOf):
        def __init__(self, parent):
            super(VampireRaider.Weapons, self).__init__(parent=parent, name='Weapons')
            self.variant('Twin-linked Pulse Lasers', 0, gear=[Gear('Twin-linked Pulse Laser', count=2)])
            self.variant('Twin-linked Phoenix Missile Launchers', 0,
                         gear=[Gear('Twin-linked Phoenix Missile Launcher', count=2)])
            self.pulsar = self.variant('Twin-linked Pulsar', 0)


class SkathachWraithknight(Unit):
    clear_name = 'Eldar Skathach Wraithknight'
    type_name = clear_name + ia_id
    type_id = 'skathach_wraithknight_v2'
    imperial_Armour = True

    class ArmWeapon(OneOf):
        def __init__(self, parent):
            super(SkathachWraithknight.ArmWeapon, self).__init__(parent=parent, name='Arm weapons')
            self.variant('Two deathshroud cannons', 0, gear=[Gear('Deathshroud cannon', count=2)])
            self.variant('Two inferno lances', 0, gear=[Gear('Inferno lance', count=2)])
            self.variant('Deathshroud cannon and scattershield', 0,
                         gear=[Gear('Deathshroud cannon'), Gear('Scattershield')])
            self.variant('Inferno lance and scattershield', 0,
                         gear=[Gear('Inferno lance'), Gear('Scattershield')])

    def __init__(self, parent):
        super(SkathachWraithknight, self).__init__(parent=parent, name=SkathachWraithknight.clear_name,
                                                   points=315, gear=[Gear('Webway shunt generator')])
        self.wep1 = self.ArmWeapon(self)
        self.scatter = Count(self, 'Scatter laser', 0, 2, points=15)
        self.shuriken = Count(self, 'Shuriken cannon', 0, 2, points=15)
        self.star = Count(self, 'Starcannon', 0, 2, points=15)

    def check_rules(self):
        super(SkathachWraithknight, self).check_rules()
        Count.norm_counts(0, 2, [self.scatter, self.shuriken, self.star])


class Revenant(Unit):
    clear_name = 'Revenant Titan'
    type_name = clear_name + ia_id
    type_id = 'ia11_revenanttitan_v2'
    imperial_armour = True

    def __init__(self, parent):
        super(Revenant, self).__init__(parent=parent, points=900, gear=[
            Gear('Cloudburst Missile Launcher')],
                                       name=self.clear_name)
        self.Weapons(self)
        self.Weapons(self, '')

    class Weapons(OneOf):
        def __init__(self, parent, name='Weapon'):
            super(Revenant.Weapons, self).__init__(parent=parent, name=name)
            self.variant('Pulsar', 0)
            self.variant('Sonic Lance', 0)


class Phantom(Unit):
    clear_name = 'Phantom Titan'
    type_name = clear_name + ia_id
    type_id = 'ia11_phantomtitan_v2'
    imperial_armour = True

    def __init__(self, parent):
        super(Phantom, self).__init__(parent=parent, points=2500, name=self.clear_name,
                                      gear=[Gear('Phantom cloudburst Missile Launcher'),
                                            Gear('Phantom missile launcher')])
        self.Weapon1(self)
        self.wep1 = self.Weapon2(self, name='Arm mounted')
        self.wep2 = self.Weapon2(self, name='')
        self.builtin = self.Weapon1(self, 'Built-in', True)

    class Weapon1(OneOf):
        def __init__(self, parent, name='Caparace Mounted Weapon', twin_link=False):
            super(Phantom.Weapon1, self).__init__(parent=parent, name=name)
            self.variant(('Twin-linked ' if twin_link else '') + 'Starcannon', 0)
            self.variant('Pulse Laser', 15)

    class Weapon2(OneOf):
        def __init__(self, parent, name):
            super(Phantom.Weapon2, self).__init__(parent=parent, name=name)
            self.variant('Phantom Pulsar', 0)
            self.variant('Phantom D-Bombard', 0)
            self.ccw = self.variant('Phantom glaive')

    def check_rules(self):
        super(Phantom, self).check_rules()
        self.wep1.ccw.used = self.wep1.ccw.visible = self.wep2.cur != self.wep2.ccw
        self.wep2.ccw.used = self.wep2.ccw.visible = self.wep1.cur != self.wep1.ccw
        self.builtin.used = self.builtin.visible = self.wep1.cur == self.wep1.ccw or\
                                                   self.wep2.cur == self.wep2.ccw
