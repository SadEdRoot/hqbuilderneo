__author__ = 'Ivan Truskov'

from builder.games.wh40k.section import HeavySection, LordsOfWarSection,\
    FastSection, ElitesSection, UnitType
from .fast import Chimera, HellhoundSquadron, CentaurSquadron, SalamanderSquadron,\
    TaurosSquadron, DropSquadron, ScoutSentinelSquadron,\
    ArmouredSentinelSquadron
from .elites import SalamanderCommand, Atlas
from .heavy import LemanRussSquadron, DestroyerSquadron, ThundererSquadron,\
    OrdnanceBattery, HydraBattery, ManticoreBattery, HydraPlatform,\
    ManticorePlatform, EarthshakerPlatform, CarriageBattery, FieldArtilleryBattery,\
    SentryBattery, Rapiers, Hades, CyclopsSquad, SentinelSquadron
from .lords import Baneblade, Shadowsword, Stormblade, Stormsword,\
    Macharius, MachariusVanquisher, MachariusVulcan, MachariusOmega,\
    Crassus, Praetor, Dominus, Gorgon, Malcador, MalcadorAnnihilator,\
    MalcadorDefender, MalcadorInfernus, Valdor, Minotaur


class IGFast(FastSection):
    def __init__(self, parent):
        super(IGFast, self).__init__(parent)
        UnitType(self, Chimera)
        UnitType(self, HellhoundSquadron)
        UnitType(self, CentaurSquadron)
        UnitType(self, SalamanderSquadron)
        UnitType(self, TaurosSquadron)
        UnitType(self, DropSquadron)
        UnitType(self, ScoutSentinelSquadron)
        UnitType(self, ArmouredSentinelSquadron)


class IGElites(ElitesSection):
    def __init__(self, parent):
        super(IGElites, self).__init__(parent)
        UnitType(self, SalamanderCommand)
        UnitType(self, Atlas)


class IGHeavy(HeavySection):
    def __init__(self, parent):
        super(IGHeavy, self).__init__(parent)
        UnitType(self, LemanRussSquadron)
        UnitType(self, DestroyerSquadron)
        UnitType(self, ThundererSquadron)
        UnitType(self, OrdnanceBattery)
        UnitType(self, HydraBattery)
        UnitType(self, ManticoreBattery)
        UnitType(self, HydraPlatform)
        UnitType(self, ManticorePlatform)
        UnitType(self, EarthshakerPlatform)
        UnitType(self, CarriageBattery)
        UnitType(self, FieldArtilleryBattery)
        UnitType(self, SentryBattery)
        UnitType(self, Rapiers)
        UnitType(self, Hades)
        UnitType(self, CyclopsSquad)
        UnitType(self, SentinelSquadron)


class IGLords(LordsOfWarSection):
    def __init__(self, parent):
        super(IGLords, self).__init__(parent)
        UnitType(self, Baneblade)
        UnitType(self, Shadowsword)
        UnitType(self, Stormblade)
        UnitType(self, Stormsword)
        UnitType(self, Macharius)
        UnitType(self, MachariusVanquisher)
        UnitType(self, MachariusVulcan)
        UnitType(self, MachariusOmega)
        UnitType(self, Crassus)
        UnitType(self, Praetor)
        UnitType(self, Dominus)
        UnitType(self, Gorgon)
        UnitType(self, Malcador)
        UnitType(self, MalcadorAnnihilator)
        UnitType(self, MalcadorDefender)
        UnitType(self, MalcadorInfernus)
        UnitType(self, Valdor)
        UnitType(self, Minotaur)


unit_types = [
    Chimera, HellhoundSquadron, CentaurSquadron,
    SalamanderSquadron, TaurosSquadron, DropSquadron,
    ScoutSentinelSquadron, ArmouredSentinelSquadron,
    SalamanderCommand, Atlas, LemanRussSquadron, DestroyerSquadron,
    ThundererSquadron, OrdnanceBattery, HydraBattery,
    ManticoreBattery, HydraPlatform, ManticorePlatform,
    EarthshakerPlatform, CarriageBattery, FieldArtilleryBattery,
    SentryBattery, Rapiers, Hades, CyclopsSquad, SentinelSquadron,
    Baneblade, Shadowsword, Stormblade, Stormsword, Macharius,
    MachariusVanquisher, MachariusVulcan, MachariusOmega, Crassus,
    Praetor, Dominus, Gorgon, Malcador, MalcadorAnnihilator,
    MalcadorDefender, MalcadorInfernus, Valdor, Minotaur
]
