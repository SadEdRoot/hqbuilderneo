__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, Count,\
    SubUnit, UnitDescription, Gear, OneOf
from builder.games.wh40k.skitarii.armory import Melee,\
    SpecialIssue, Ranged, CadiaRelics, HolyWeaponRelic,\
    ArcanaWeaponRelic, MechanicusOption
from builder.games.wh40k.roster import Unit


class Lance(OneOf):
    def __init__(self, parent, *args):
        super(Lance, self).__init__(parent, *args)
        self.lance = self.variant('Arc lance', 0)


class CommonOptions(OptionsList, MechanicusOption):
    def __init__(self, parent):
        super(CommonOptions, self).__init__(parent, 'Options', limit=1)
        self.variant('Enchanced data-tether', 5 * self.freeflag)
        self.variant('Omnispex', 10 * self.freeflag)


class Hoplites(Unit):
    type_name = 'Secutarii Hoplites'
    type_id = 'hoplites_v1'
    imperial_armour = True

    class Alpha(Unit):
        class Weapon(ArcanaWeaponRelic, Ranged, Lance):
            pass

        class Weapon2(HolyWeaponRelic, Melee):
            pass

        def __init__(self, parent):
            super(Hoplites.Alpha, self).__init__(
                parent, 'Hoplite Alpha', 23, [Gear('Secutarii war plate'),
                                              Gear('Mag-inverter shield'),
                                              Gear('Kyropatris field generator')])
            self.rng = self.Weapon(self)
            self.mle = self.Weapon2(self)
            SpecialIssue(self)
            CommonOptions(self)
            self.relics = CadiaRelics(self)

        def get_unique_gear(self):
            return self.relics.description + self.rng.get_unique()\
                + self.mle.get_unique()

        def check_rules(self):
            super(Hoplites.Alpha, self).check_rules()

            if len(self.get_unique_gear()) > 1:
                self.error('Model cannot carry more then one relic')

    def __init__(self, parent):
        super(Hoplites, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Alpha(parent=self))
        self.wars = Count(self, self.type_name, 9, 19, 13, True,
                          gear=UnitDescription('Secutarii Hoplite', 13, options=[
                              Gear('Secutarii war plate'),
                              Gear('Arc lance'),
                              Gear('Mag-inverter shield'),
                              Gear('Kyropatris field generator')
                          ]))

    def get_unique_gear(self):
        return self.ldr.unit.get_unique_gear()

    def get_count(self):
        return self.wars.cur + 1  # leader always present


class Caster(OneOf):
    def __init__(self, parent, *args):
        super(Caster, self).__init__(parent, *args)
        self.caster = self.variant('Galvanic-caster', 0)


class Peltasts(Unit):
    type_name = 'Secutarii Peltasts'
    type_id = 'peltasts_v1'
    imperial_armour = True

    class Alpha(Unit):
        class Weapon(ArcanaWeaponRelic, Ranged, Caster):
            pass

        class Weapon2(HolyWeaponRelic, Melee):
            pass

        def __init__(self, parent):
            super(Peltasts.Alpha, self).__init__(
                parent, 'Peltast Alpha', 22, [Gear('Secutarii war plate'),
                                              Gear('Kyropatris field generator')])
            self.rng = self.Weapon(self)
            self.mle = self.Weapon2(self)
            SpecialIssue(self)
            CommonOptions(self)
            self.relics = CadiaRelics(self)

        def get_unique_gear(self):
            return self.relics.description + self.rng.get_unique()\
                + self.mle.get_unique()

        def check_rules(self):
            super(Peltasts.Alpha, self).check_rules()

            if len(self.get_unique_gear()) > 1:
                self.error('Model cannot carry more then one relic')

    def __init__(self, parent):
        super(Peltasts, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Alpha(parent=self))
        self.wars = Count(self, self.type_name, 9, 19, 12, True,
                          gear=UnitDescription('Secutarii Peltast ', 12, options=[
                              Gear('Secutarii war plate'),
                              Gear('Galvanic-caster'),
                              Gear('Kyropatris field generator')
                          ]))

    def get_unique_gear(self):
        return self.ldr.unit.get_unique_gear()

    def get_count(self):
        return self.wars.cur + 1  # leader always present
