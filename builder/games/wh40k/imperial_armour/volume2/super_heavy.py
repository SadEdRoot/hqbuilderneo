__author__ = 'dante'


from builder.core2 import *
from .options import SpaceMarinesBaseVehicle


class Fellblade(SpaceMarinesBaseVehicle):
    type_id = 'fellblade_v1'
    type_name = 'Fellblade Super-heavy Tank (IA vol.2)'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Fellblade.Options, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 10)
            self.variant('Armoured Ceramite', 25)
            self.weapon = [
                self.variant('Heavy bolter', 15),
                self.variant('Heavy flamer', 15),
                self.variant('Multi-melta', 20),
            ]

        def check_rules(self):
            self.process_limit(self.weapon, 1)

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Fellblade.Sponsons, self).__init__(parent, 'Weapon')
            self.variant('Quad lascannons', 0, gear=Gear('Quad lascannon', count=2))
            self.variant('Laser destroyers', 0, gear=Gear('Laser destroyer', count=2))

    class Hull(OneOf):
        def __init__(self, parent):
            super(Fellblade.Hull, self).__init__(parent, '')
            self.variant('Twin-linked heavy bolter', 0)
            self.variant('Twin-linked heavy flamer', 0)

    def __init__(self, parent):
        super(Fellblade, self) .__init__(parent=parent, name='Fellblade', points=540, gear=[
            Gear('Fellblade accelerator cannon'),
            Gear('Demolisher cannon'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], super_heavy=True)
        self.Sponsons(self)
        self.Hull(self)
        self.Options(self)


class Typhon(SpaceMarinesBaseVehicle):
    type_id = 'typhon_v1'
    type_name = 'Typhon Heavy Siege Tank (IA vol.2)'
    imperial_armour = True

    class Options(OptionsList):
        def __init__(self, parent):
            super(Typhon.Options, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 10)
            self.variant('Armoured Ceramite', 20)
            self.weapon = [
                self.variant('Heavy bolter', 15),
                self.variant('Heavy flamer', 15),
                self.variant('Multi-melta', 20),
            ]

        def check_rules(self):
            self.process_limit(self.weapon, 1)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Typhon.Sponsons, self).__init__(parent, 'Weapon', limit=1)
            self.variant('Heavy bolters', 20, gear=Gear('Heavy bolter', count=2))
            self.variant('Lascannons', 40, gear=Gear('Lascannon', count=2))

    def __init__(self, parent):
        super(Typhon, self) .__init__(parent=parent, name='Typhon', points=350, gear=[
            Gear('Dreadhammer siege cannon'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], super_heavy=True)
        self.Sponsons(self)
        self.Options(self)


class Cerberus(SpaceMarinesBaseVehicle):
    type_id = 'cerberus_v1'
    type_name = 'Cerberus Heavy Tank Destroyer (IA vol.2)'
    imperial_armour = True

    def __init__(self, parent):
        super(Cerberus, self) .__init__(parent=parent, name='Cerberus', points=355, gear=[
            Gear('Twin-linked neutron laser battery'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], super_heavy=True)
        Typhon.Sponsons(self)
        Typhon.Options(self)


class IA2ThunderhawkGunship(SpaceMarinesBaseVehicle):
    type_id = 'ia_thunderhawk_gunship_v1'
    type_name = 'Thunderhawk Gunship (IA vol.2)'
    imperial_armour = True

    def __init__(self, parent):
        super(IA2ThunderhawkGunship, self) .__init__(parent=parent, name='Thunderhawk Gunship', points=685, gear=[
            Gear('Twin-linked heavy bolter', count=4),
            Gear('Lascannon', count=2),
            Gear('Armoured Ceramite'),
        ], super_heavy=True, thunder_hawk=True, transport=True)
        self.Weapon(self)
        self.Bombs(self)
        self.Options(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(IA2ThunderhawkGunship.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Thunderhawk cannon', 0)
            self.variant('Turbo-laser destructor', 90)

    class Bombs(OneOf):
        def __init__(self, parent):
            super(IA2ThunderhawkGunship.Bombs, self).__init__(parent=parent, name='')
            self.variant('Hellstrike missiles', 0, gear=Gear('Hellstrike missile', count=6))
            self.variant('Thunderhawk cluster bombs', 60, gear=Gear('Thunderhawk cluster bomb', count=6))

    class Options(OptionsList):
        def __init__(self, parent):
            super(IA2ThunderhawkGunship.Options, self).__init__(parent=parent, name='Options')
            self.variant('Flare/chaff launcher', 10)
            self.variant('Armoured cockpit', 15)
            self.variant('Illum flares', 5)
            self.variant('Distinctive paint scheme or markings', 10)


class IA2ThunderhawkTransport(SpaceMarinesBaseVehicle):
    type_id = 'ia_thunderhawk_transport_v1'
    type_name = 'Thunderhawk Transport (IA vol.2)'
    imperial_armour = True

    def __init__(self, parent):
        super(IA2ThunderhawkTransport, self) .__init__(parent=parent, name='Thunderhawk Transport', points=400, gear=[
            Gear('Twin-linked heavy bolter', count=4),
            Gear('Armoured Ceramite'),
        ], transport=True, thunder_hawk=True, super_heavy=True)
        self.Options(self)
        Count(self, 'Hellstrike missile', 0, 6, 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(IA2ThunderhawkTransport.Options, self).__init__(parent=parent, name='Options')
            self.variant('Flare/chaff launcher', 10)
            self.variant('Armoured cockpit', 15)
            self.variant('Illum flares', 5)
            self.variant('Distinctive paint scheme or markings', 10)
