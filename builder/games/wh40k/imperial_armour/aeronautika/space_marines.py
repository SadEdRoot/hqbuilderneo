__author__ = 'Ivan Truskov'

from builder.core2 import Gear, UnitDescription,\
    Count
from builder.games.wh40k.roster import Unit


class HyperiosBattery(Unit):
    type_name = 'Hyperios Air defence battery'
    type_id = 'hyperios_battery_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(HyperiosBattery, self).__init__(parent)
        self.missiles = Count(self, 'Hyperios platform with missile launcher', 1, 4, 35, True,
                              gear=UnitDescription('Hyperios Platform', 35, options=[
                                  Gear('Twin-linked Hyperios missile launcher')
                              ]))
        self.command = Count(self, 'Hyperios platform with command platfotm', 0, 1, 45, True,
                             gear=UnitDescription('Hyperios platform', 45, options=[
                                 Gear('Hyperios Command platform')
                             ]))

    def check_rules(self):
        Count.norm_counts(1, 4, [self.missiles, self.command])

    def get_count(self):
        return self.missiles.cur + self.command.cur
