__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.wh40k.roster import Unit


class HectorRex(Unit):
    type_name = 'Inquisitor-Lord Hector Rex (IA vol.5-7)'
    type_id = 'rex_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(HectorRex, self).__init__(parent, 'Hector Rex', 175,
                                        gear=[
                                            Gear('Artificer armour'),
                                            Gear('Bolt pistol'),
                                            Gear('Storm sield'),
                                            Gear('Arias'),
                                            Gear('Frag grenades'),
                                            Gear('Krak grenades'),
                                            Gear('Psyk-out grenades'),
                                            Gear('Psychic hood')
                                        ], static=True, unique=True)

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(HectorRex, self).build_statistics())


class Necrosius(Unit):
    type_name = 'Necrosius (IA vol.5-7)'
    type_id = 'necrosius_v1'

    def __init__(self, parent):
        super(Necrosius, self).__init__(parent, 'Necrosius', 160,
                                        gear=[
                                            Gear('Power armour'),
                                            Gear('Force sword'),
                                            Gear('Bolt pistol'),
                                            Gear('Storm sield'),
                                            Gear('Frag grenades'),
                                            Gear('Krak grenades'),
                                            Gear('Blight grenades')
                                        ], static=True, unique=True)

    def count_charges(self):
        return 2

    def build_statistics(self):
        return self.note_charges(super(Necrosius, self).build_statistics())


class Zhufor(Unit):
    type_name = 'Zhufor the Impaler, Lord of the Skulltakers (IA vol.5-7)'
    type_id = 'zhufor_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Zhufor, self).__init__(parent, 'Zhufor', 165,
                                        gear=[
                                            Gear('Terminator armour'),
                                            Gear('Skulltaker chainaxe'),
                                            Gear('Claw of Demnos')
                                        ], static=True, unique=True)


class Arkos(Unit):
    type_name = 'Arkos the Faithless, Scion of Alpharius (IA vol.5-7)'
    type_id = 'arkos_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(Arkos, self).__init__(parent, 'Arkos', 130,
                                        gear=[
                                            Gear('Power armour'),
                                            Gear('Combi-melta'),
                                            Gear('Dark Blade'),
                                            Gear('Frag grenades'),
                                            Gear('Krak grenades'),
                                            Gear('Aura of Dark Glory')
                                        ], static=True, unique=True)


class Mamon(Unit):
    type_name = 'Mamon Incarnate, Daemon Prince of Nurgle'
    type_id = 'mamon_v1'

    def __init__(self, parent):
        super(Mamon, self).__init__(parent, name='Mamon', points=220, gear=[Gear('Contagion Spray')],
                                    static=True, unique=True)


class Uraka(Unit):
    type_name = 'Warbound Uraka, Daemon Prince of Khorne'
    type_id = 'uraka_v1'

    def __init__(self, parent):
        super(Uraka, self).__init__(parent, name='Warbound Uraka', points=200, gear=[
            Gear('Executioner\'s Axe')
        ], static=True, unique=True)
