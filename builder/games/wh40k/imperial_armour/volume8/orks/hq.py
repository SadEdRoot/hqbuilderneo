__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.wh40k.roster import Unit


class Zhadsnark(Unit):
    type_name = "Zhadsnark 'Da Rippa' (IA vol.8)"
    type_id = 'zhardsnark_v2'
    imperial_armour = True

    def __init__(self, parent):
        super(Zhadsnark, self).__init__(parent, 'Zhadsnark', 150, gear=[
            Gear('\'Da Beast\''),
            Gear('\'Da Rippa\''),
            Gear('Slugga'),
            Gear('Stikkbombz')
        ], static=True, unique=True)


class Buzzgob(Unit):
    type_name = 'Mek Boss Buzzgob (IA vol.8)'
    type_id = 'buzzgob_v2'
    imperial_armour = True

    def __init__(self, parent):
        super(Buzzgob, self).__init__(parent, 'Buzzgob', 100, gear=[
            Gear('Slugga'),
            Gear('Mek arms'),
            Gear('Big choppa'),
            Gear('\'Eavy Armour'),
            Gear('Bosspole'),
            Gear('Nitunckle & Lunk')], static=True, unique=True)
