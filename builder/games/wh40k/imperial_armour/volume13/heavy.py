__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear, UnitList,\
    ListSubUnit, UnitDescription
from .options import CommonOption, ChaosSpaceMarinesBaseVehicle
from builder.games.wh40k.roster import Unit


class InfernalPredator(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Infernal Relic Predator (IA vol.13)'
    type_id = 'inferno_predator_v1'
    imperial_armour = True

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(InfernalPredator.Sponsons, self).__init__(parent, 'Sponsons')
            self.flame = self.variant('Heavy flamers', 20,
                                      gear=[Gear('Heavy flamer', count=2)])
            self.bolt = self.variant('Heavy bolters', 20,
                                     gear=[Gear('Heavy bolter', count=2)])
            self.mal = self.variant('Malefic ammunition', 30, active=False)
            self.las = self.variant('Lascannons', 40,
                                    gear=[Gear('Lascannon', count=2)])

        def check_rules(self):
            super(InfernalPredator.Sponsons, self).check_rules()
            self.mal.active = self.bolt.value
            OptionsList.process_limit([self.flame, self.bolt, self.las], 1)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(InfernalPredator.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Flamestorm cannon')
            self.variant('Magna-melta cannon', 25)
            self.variant('Autocannon', 0, gear=[Gear('Autocannon'), Gear('Inferno bolts')])
            self.variant('Heavy conversion beamer', 45)
            self.variant('Twin-linked lascannon', 10)
            self.variant('Plasma destroyer', 15)

    class Option(CommonOption):
        def __init__(self, parent):
            super(InfernalPredator.Option, self).__init__(parent, armour=10)
            self.variant('Pintle-mounted combi-bolter', 5)
            self.variant('Combi-flamer', 10)
            self.variant('Combi-melta', 10)
            self.variant('Combi-plasma', 10)

    def __init__(self, parent):
        super(InfernalPredator, self).__init__(parent, 'Chaos Infernal Relic Predator',
                                               points=90, gear=[
                                                   Gear('Searchlight'),
                                                   Gear('Smoke launcher')
                                               ], tank=True)
        self.Weapon(self)
        self.Sponsons(self)
        self.Option(self)

    def is_relic(self):
        return True


class ChaosSicaranTank(ChaosSpaceMarinesBaseVehicle):
    type_name = "Chaos Relic Sicaran Battle Tank (IA vol.13)"
    type_id = "chaos_sicaran_tank_v1"
    imperial_armour = True

    class Options(CommonOption):
        def __init__(self, parent):
            super(ChaosSicaranTank.Options, self).__init__(parent, ceramite=20, malefic=40)
            self.variant('Pintle-mounted Combi-flamer', 10)
            self.variant('Pintle-mounted Combi-melta', 10)
            self.variant('Pintle-mounted Combi-plasma', 10)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(ChaosSicaranTank.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.variant('Heavy bolters', 20, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Lascannons', 40, gear=[Gear('Lascannon', count=2)])

    def __init__(self, parent):
        super(ChaosSicaranTank, self).__init__(
            parent=parent, points=135, name="Relic Sicaran Tank", gear=[
                Gear('Twin-linked accelerator cannon'),
                Gear('Heavy bolter'),
                Gear('Smoke launchers'),
                Gear('Searchlight'),
                Gear('Extra armour'),
            ], tank=True)
        self.side = self.Sponsons(self)
        self.opt = self.Options(self)

    def is_relic(self):
        return True


class ChaosProteus(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Land Raider Proteus (IA vol.13)'
    type_id = 'proteus_v1'
    imperial_armour = True

    class Options(CommonOption):
        def __init__(self, parent):
            super(ChaosProteus.Options, self).__init__(parent, armour=10, ceramite=20, malefic=35)
            self.variant('Ark of Unnameable Horror', 50)

    class PintleWeapon(OptionsList):
        def __init__(self, parent):
            super(ChaosProteus.PintleWeapon, self).__init__(
                parent, 'Pintle-mounted weapon', limit=1)
            self.variant('Combi-flamer', 10)
            self.variant('Combi-melta', 10)
            self.variant('Combi-plasma', 10)
            self.variant('Heavy flamer', 15)
            self.variant('Heavy bolter', 15)
            self.variant('Milti-melta', 20)

    class HullWeapon(OptionsList):
        def __init__(self, parent):
            super(ChaosProteus.HullWeapon, self).__init__(
                parent, 'Hull-mounted weapon', limit=1)
            self.variant('Twin-linked heavy bolter', 20)
            self.variant('Twin-linked heavy flamer', 20)

    def __init__(self, parent):
        super(ChaosProteus, self).__init__(parent, 'Chaos Land Raider Proteus', points=200, gear=[
            Gear('Searchlight'), Gear('Smoke launcher'),
            Gear('Lascannon', count=2)
        ], tank=True, transport=True)
        self.Options(self)
        self.PintleWeapon(self)
        self.HullWeapon(self)


class ChaosAchilles(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Infernal Relic Achilles (IA vol.13)'
    type_id = 'achilles_v1'
    imperial_armour = True

    class Options(CommonOption):
        def __init__(self, parent):
            super(ChaosAchilles.Options, self).__init__(parent, malefic=15)

    class PintleWeapon(OptionsList):
        def __init__(self, parent):
            super(ChaosAchilles.PintleWeapon, self).__init__(
                parent, 'Pintle-mounted weapon', limit=1)
            self.variant('Combi-flamer', 10)
            self.variant('Combi-melta', 10)
            self.variant('Combi-plasma', 10)
            self.variant('Heavy flamer', 15)
            self.variant('Heavy bolter', 15)
            self.variant('Milti-melta', 20)

    def __init__(self, parent):
        super(ChaosAchilles, self).__init__(parent, 'Infernal Relic Achilles', points=330, gear=[
            Gear('Searchlight'), Gear('Smoke launcher'),
            Gear('Multi-melta', count=2), Gear('Quad mortar'),
            Gear('Exra armour'), Gear('Armoured ceramite')
        ], tank=True, transport=True)
        self.Options(self)
        self.PintleWeapon(self)

    def is_relic(self):
        return True


class FireRaptor(ChaosSpaceMarinesBaseVehicle):
    type_name = 'Chaos Fire Raptor Gunship (IA vol.13)'
    type_id = 'fire_raptor_gunship_v1'
    imperial_armour = True

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(FireRaptor.Weapon1, self).__init__(parent, 'Turret-mounted weapon')
            self.heavybolter = self.variant('Quad heavy bolter', 0,
                                                      gear=[Gear('Quad heavy bolter', count=2)])
            self.reaper = self.variant('Reaper autocannon batteries', 10,
                                       gear=[Gear('Reaper autocannon battery', 10)])

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(FireRaptor.Weapon2, self).__init__(parent, 'Missile weapons')
            self.hell = self.variant('Four hellstrike missiles', 0,
                                     gear=[Gear('Hellstrike missile', count=4)])
            self.inc = self.variant('Four Balefire incendiary missiles', 15,
                                    gear=[Gear('Balefire incendiary missile', count=4)])

    class Options(OptionsList):
        def __init__(self, parent, weapon):
            super(FireRaptor.Options, self).__init__(parent, 'Options')
            self.weapon = weapon
            self.variant('Searchlight', 1)
            self.variant('Dirge caster', 5)
            self.variant('Armoured Ceramite', 20)
            self.variant('Warpflame Gargoyles', 5)
            self.variant('Daemonic Posession', 15)
            self.mal = self.variant('Malefic Ammunition', 20)

        def check_rules(self):
            if self.weapon.cur == self.weapon.heavybolter:
                self.mal.visible = self.mal.used = True
                self.mal.points = 50
            else:
                if self.weapon.cur == self.weapon.reaper:
                    self.mal.visible = self.mal.used = True
                    self.mal.points = 35

    def __init__(self, parent):
        super(FireRaptor, self).__init__(
            parent=parent, name='Chaos Fire Raptor', points=200, gear=[
                Gear('Twin-linked avenger bolt cannon'),
                Gear('Armoured Ceramite')
            ])
        self.wep1 = self.Weapon1(self)
        self.Weapon2(self)
        self.opt = self.Options(self, self.wep1)


class Slaughterers(Unit):
    type_name = 'Blood Slaughterers of Khorne (IA vol.13)'
    type_id = 'slaughterers_v1'
    imperial_armour = True

    class Slaughterer(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(Slaughterers.Slaughterer.Weapon, self).__init__(
                    parent, 'Weapon')
                self.variant('Dreadnought close combat weapon')
                self.variant('Impaler', 5)

        def __init__(self, parent):
            super(Slaughterers.Slaughterer, self).__init__(
                parent, 'Blood Slaughterer', 130, [Gear('Deadnought close combat weapon')])
            self.Weapon(self)

    def __init__(self, parent):
        super(Slaughterers, self).__init__(parent, 'Blood Slaughterers of Khorne')
        self.models = UnitList(self, self.Slaughterer, 1, 3)

    def get_count(self):
        return self.models.count


class ChaosRapiers(Unit):
    type_name = 'Chaos Rapiers Weapons Battery (IA vol.13)'
    type_id = 'chaos_rapiers__v1'
    imperial_armour = True

    class Carrier(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(ChaosRapiers.Carrier.Weapon, self).__init__(
                    parent, 'Weapon')
                self.variant('Quad heavy bolter')
                self.variant('Laser destroyer', 15)
                self.variant('Ectoplasma cannon', 15)
                self.variant('Hades autocannon', 25)
                self.variant('Cyclotrathe pattern conversion beamer', 35)

        def __init__(self, parent):
            super(ChaosRapiers.Carrier, self).__init__(
                parent, 'Chaos Rapier carrier', 40, [
                    UnitDescription('Chaos Space Marine', count=2, options=[
                        Gear('Power armour'), Gear('Bolt pistol'),
                        Gear('Krak grenades'), Gear('Krak grenades')
                    ])])
            self.Weapon(self)

    def __init__(self, parent):
        super(ChaosRapiers, self).__init__(parent, 'Chaos Rapiers Weapons Battery')
        self.models = UnitList(self, self.Carrier, 1, 3)

    def get_count(self):
        return self.models.count

    def build_statistics(self):
        return {'Models': 3 * self.get_count(), 'Units': 1}


class PlagueHulk(Unit):
    type_name = 'Plague Hulk of Nurgle (IA vol.13)'
    type_id = 'plague_hulk_v1'
    imperial_armour = True

    class Weapon(OneOf):
        def __init__(self, parent):
            super(PlagueHulk.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Iron claw')
            self.variant('Warpsword', 25)

    def __init__(self, parent):
        super(PlagueHulk, self).__init__(parent, 'Plague Hulk', 150, [
            Gear('Rancid vomit'), Gear('Rot cannon')
        ])
        self.Weapon(self)


class GiantChaosSpawn(Unit):
    type_name = 'Giant Chaos Spawn (IA vol.13)'
    type_id = 'giant_chaos_spawn_v1'
    imperial_armour = True

    def __init__(self, parent):
        super(GiantChaosSpawn, self).__init__(parent, name='Giant Chaos Spawn', points=80, static=True)
