__author__ = 'Ivan Truskov'

from builder.core2.roster import Roster
from builder.games.wh40k.section import HeavySection, LordsOfWarSection,\
    FastSection, ElitesSection, UnitType, Formation, HQSection

from .hq import Mamon, Uraka
from .heavy import InfernalPredator, ChaosSicaranTank, ChaosProteus,\
    ChaosAchilles, FireRaptor, Slaughterers, ChaosRapiers, PlagueHulk,\
    GiantChaosSpawn
from .lords import Fellblade, Spartan, Typhon, GreaterScorpion,\
    ChaosReaverTitan, ChaosWarhoundTitan, ThunderhawkGunship,\
    Scabeiathrax, Anggrath, Zarakynel, Aetaosraukeres
from .fast import StormEagleGunship, HellTalon, HellBlade, Dreadclaw,\
    Kharybdis, BlightDrones
from .elites import FerrumDreadnought, SonicDreadnought, ChaosContemptor,\
    Decimator, SpinedBeast
from .options import ChaosSpaceMarinesLegaciesNode, ChaosSpaceMarinesBaseVehicle,\
    ChaosSpaceMarinesBaseSquadron
from .transport import IATransportedUnit


class MarinesFast(FastSection):
    def __init__(self, parent):
        super(MarinesFast, self).__init__(parent)
        UnitType(self, StormEagleGunship)
        UnitType(self, HellBlade)
        UnitType(self, HellTalon)
        UnitType(self, Dreadclaw)
        self.kharybdis = UnitType(self, Kharybdis)
        UnitType(self, BlightDrones)

    def count_relics(self):
        return self.kharybdis.count


class MarinesHeavy(HeavySection):
    def __init__(self, parent):
        super(MarinesHeavy, self).__init__(parent)
        pred = UnitType(self, InfernalPredator)
        sicaran = UnitType(self, ChaosSicaranTank)
        UnitType(self, ChaosProteus)
        achilles = UnitType(self, ChaosAchilles)
        UnitType(self, Spartan)
        UnitType(self, Slaughterers)
        UnitType(self, FireRaptor)
        UnitType(self, ChaosRapiers)
        UnitType(self, PlagueHulk)
        self.relics = [pred, sicaran, achilles]

    def count_relics(self):
        return sum(ut.count for ut in self.relics)


class MarinesElites(ElitesSection):
    def __init__(self, parent):
        super(MarinesElites, self).__init__(parent)
        self.ferrum = UnitType(self, FerrumDreadnought)
        UnitType(self, SonicDreadnought)
        UnitType(self, ChaosContemptor)
        UnitType(self, Decimator)
        UnitType(self, SpinedBeast)

    def count_relics(self):
        return self.ferrum.count


class MarinesLords(LordsOfWarSection):
    def __init__(self, parent):
        super(MarinesLords, self).__init__(parent)
        UnitType(self, Fellblade)
        UnitType(self, Typhon)
        UnitType(self, ThunderhawkGunship)
        UnitType(self, GreaterScorpion)
        UnitType(self, ChaosReaverTitan)
        UnitType(self, ChaosWarhoundTitan)


class ChaosSpaceMarinesLegaciesFormation(Formation, ChaosSpaceMarinesLegaciesNode):
    def count_ruin_legacies(self):
        leg_sum = 0
        for unit in self.units:
            if isinstance(unit, ChaosSpaceMarinesBaseVehicle) or\
               isinstance(unit, IATransportedUnit) or\
               isinstance(unit, ChaosSpaceMarinesBaseSquadron):
                leg_sum += unit.count_ruin_legacies()
            else:
                if hasattr(unit, 'sub_roster') and\
                   isinstance(unit.sub_roster.roster, ChaosSpaceMarinesLegaciesNode):
                    leg_sum += unit.sub_roster.roster.count_ruin_legacies()
        return leg_sum

    def check_rules(self):
        super(ChaosSpaceMarinesLegaciesFormation, self).check_rules()
        self.root.extra_checks['legacies_of_ruin'] = ChaosSpaceMarinesLegaciesNode.root_check


class ChaosSpaceMarinesLegaciesRoster(Roster, ChaosSpaceMarinesLegaciesNode):
    def count_ruin_legacies(self):
        leg_sum = 0
        for sec in self.sections:
            for unit in sec.units:
                if isinstance(unit, ChaosSpaceMarinesBaseVehicle) or\
                   isinstance(unit, IATransportedUnit) or\
                   isinstance(unit, ChaosSpaceMarinesBaseSquadron):
                    leg_sum += unit.count_ruin_legacies()
                else:
                    if hasattr(unit, 'sub_roster') and\
                       isinstance(unit.sub_roster.roster, ChaosSpaceMarinesLegaciesNode):
                        leg_sum += unit.sub_roster.roster.count_ruin_legacies()
        return leg_sum

    def check_rules(self):
        super(ChaosSpaceMarinesLegaciesRoster, self).check_rules()
        self.root.extra_checks['legacies_of_ruin'] = ChaosSpaceMarinesLegaciesNode.root_check


class DaemonHeavy(HeavySection):
    def __init__(self, parent):
        super(DaemonHeavy, self).__init__(parent)
        UnitType(self, Slaughterers)
        UnitType(self, PlagueHulk)
        UnitType(self, GiantChaosSpawn)


class DaemonElites(ElitesSection):
    def __init__(self, parent):
        super(DaemonElites, self).__init__(parent)
        UnitType(self, Decimator)
        UnitType(self, SpinedBeast)


class DaemonFast(FastSection):
    def __init__(self, parent):
        super(DaemonFast, self).__init__(parent)
        UnitType(self, BlightDrones)


class DaemonLords(LordsOfWarSection):
    def __init__(self, parent):
        super(DaemonLords, self).__init__(parent)
        UnitType(self, Scabeiathrax)
        UnitType(self, Anggrath)
        UnitType(self, Zarakynel)
        UnitType(self, Aetaosraukeres)


class DaemonHq(HQSection):
    def __init__(self, parent):
        super(DaemonHq, self).__init__(parent)
        UnitType(self, Mamon)
        UnitType(self, Uraka)


csm_unit_types = [
    StormEagleGunship, HellBlade, HellTalon, Dreadclaw,
    Kharybdis, BlightDrones, InfernalPredator, ChaosSicaranTank,
    ChaosProteus, ChaosAchilles, Spartan, Slaughterers, FireRaptor,
    ChaosRapiers, PlagueHulk, FerrumDreadnought, SonicDreadnought,
    ChaosContemptor, Decimator, SpinedBeast, Fellblade, Typhon,
    ThunderhawkGunship, GreaterScorpion, ChaosReaverTitan,
    ChaosWarhoundTitan
]


cd_unit_types = [
    Slaughterers, PlagueHulk, GiantChaosSpawn,
    Decimator, SpinedBeast, BlightDrones, Scabeiathrax, Anggrath,
    Zarakynel, Aetaosraukeres, Mamon, Uraka
]
