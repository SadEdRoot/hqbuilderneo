__author__ = 'Denis Romanov'

from builder.core2 import *
from .armory import *
from builder.games.wh40k.unit import Unit, Squadron


class Spawn(Unit):
    type_name = 'Chaos Spawns'
    type_id = 'chaosspawns_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chaos-Spawn')

    def __init__(self, parent):
        super(Spawn, self).__init__(parent=parent)
        self.models = Count(self, 'Chaos Spawn', min_limit=1, max_limit=5, points=30,
                            gear=UnitDescription('Chaos Spawn', points=30))
        self.marks = Marks(self, khorne=2, tzeentch=4, nurgle=6, slaanesh=3, per_model=True)

    def get_count(self):
        return self.models.cur

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle


class Heldrake(Unit):
    type_name = 'Heldrake'
    type_id = 'heldrake_v1'

    def __init__(self, parent):
        super(Heldrake, self).__init__(parent=parent, points=170, gear=[Gear('Daemonic posession')])
        self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Heldrake.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant('Hades autocannon', 0)
            self.variant('Baleflamer', 0)


class Heldrakes(Squadron):
    type_name = 'Heldrakes'
    type_id = 'heldrakes_v3'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Heldrake')
    unit_class = Heldrake
    unit_max = 4


class WarpTalons(Unit):
    type_name = "Warp Talons"
    type_id = "warp_talons_v1"
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Warp-Talons')

    class Count(Count):
        @property
        def description(self):
            return UnitDescription(
                name='Warp Talon Champion',
                points=WarpTalons.champ_points + super(WarpTalons.Count, self).points,
                options=WarpTalons.model_gear + [Gear('Gift of mutation', count=self.cur)]
            )

    model_points = 30
    model_gear = [Gear('Jump pack'), Gear('Lightning claw', count=2), Gear('Power armour')]
    champ_points = 160 - model_points * 4

    def __init__(self, parent):
        super(WarpTalons, self).__init__(parent, points=self.model_points)
        self.gift = self.Count(self, 'Champion\'s gift', 0, 2, 10)
        self.models = Count(self, 'Warp Talon', 4, 9, self.model_points,
                            gear=UnitDescription('Warp Talon', points=self.model_points, options=self.model_gear))

        self.marks = Marks(self, khorne=4, tzeentch=6, nurgle=4, slaanesh=3, per_model=True)
        self.veterans = Veterans(self, 3, per_model=True)

    def get_count(self):
        return self.models.cur + 1

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle


class Raptors(IconicUnit):
    type_name = 'Raptors'
    type_id = 'raptors_v1'

    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Raptors')

    model_points = 17
    model_gear = Armour.power_armour_set + [Gear('Jump pack')]

    class Raptor(IconBearerGroup):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Raptors.Raptor.Weapon1, self).__init__(parent=parent, name='Weapon')
                self.boltpistol = self.variant('Bolt pistol', 0)
                self.plasmapistol = self.variant('Plasma pistol', 15)

        class Weapon2(OptionsList):
            def __init__(self, parent):
                super(Raptors.Raptor.Weapon2, self).__init__(parent=parent, name='', limit=1)
                self.flamer = self.variant('Flamer', 5)
                self.meltagun = self.variant('Meltagun', 10)
                self.plasmagun = self.variant('Plasma gun', 15)

        def __init__(self, parent):
            super(Raptors.Raptor, self).__init__(
                parent, name='Raptor', points=Raptors.model_points,
                gear=Raptors.model_gear + [Gear('Close combat weapon')],
                wrath=15, flame=10, despair=10, excess=30, vengeance=20
            )
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)

        def check_rules(self):
            super(Raptors.Raptor, self).check_rules()
            self.wep1.plasmapistol.active = not self.wep2.any
            self.wep2.active = self.wep2.used = self.wep1.cur == self.wep1.boltpistol

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.wep2.any or self.wep1.cur != self.wep1.boltpistol

    class Champion(IconBearer):

        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Raptors.Champion.Options, self).__init__(parent, name='Options')
                self.variant('Melta bombs', 5)
                self.variant('Gift of mutation', 10)

        def __init__(self, parent):
            super(Raptors.Champion, self).__init__(
                parent, points=95 - Raptors.model_points * 4, name='Raptor Champion', gear=Raptors.model_gear,
                wrath=15, flame=10, despair=10, excess=30, vengeance=20
            )

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.Options(self)

        def check_rules(self):
            super(Raptors.Champion, self).check_rules()
            self.wep1.set_unique_ranged(not self.wep2.has_unique_ranged())
            self.wep2.set_unique_ranged(not self.wep1.has_unique_ranged())

    def __init__(self, parent):
        super(Raptors, self).__init__(parent)
        self.marks = Marks(self, khorne=2, tzeentch=2, nurgle=3, slaanesh=2, per_model=True)
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Raptor, min_limit=4, max_limit=14)
        self.veteran = Veterans(self, points=2, per_model=True)

    def check_rules(self):
        super(Raptors, self).check_rules()
        spec = sum(o.count_spec() for o in self.models.units)
        if spec > 2:
            self.error("Only 2 raptors can take special weapons (taken: {0}).".format(spec))


class Bikers(IconicUnit):
    type_name = 'Chaos Bikers'
    type_id = 'chaos_bikers_v1'
    wikilink = Unit.template_link.format(codex_name='chaos-space-marines', unit_name='Chaos-Bikers')

    model_points = 20
    model_gear = Armour.power_armour_set + [Gear('Chaos bike')]

    def __init__(self, parent):
        super(Bikers, self).__init__(parent)
        self.marks = Marks(self, khorne=2, tzeentch=3, nurgle=6, slaanesh=2, per_model=True)
        self.champion = SubUnit(self, self.Champion(None))
        self.models = UnitList(self, self.Biker, min_limit=2, max_limit=9)
        self.veteran = Veterans(self, points=1, per_model=True)

    def check_rules(self):
        super(Bikers, self).check_rules()
        spec = sum(o.count_spec() for o in self.models.units)
        if spec > 2:
            self.error("Only 2 chaos bikers can take special weapons (taken: {0}).".format(spec))

    class Biker(IconBearerGroup):
        class TlBoltgun(BaseWeapon):
            def __init__(self, *args, **kwargs):
                super(Bikers.Biker.TlBoltgun, self).__init__(*args, **kwargs)
                self.tl_boltgun = self.variant('Twin-linked boltgun', 0)

        class Weapon(BaseWeapon):
            def __init__(self, *args, **kwargs):
                super(Bikers.Biker.Weapon, self).__init__(*args, **kwargs)
                self.spec = [
                    self.variant('Flamer', 5),
                    self.variant('Meltagun', 10),
                    self.variant('Plasma gun', 15)
                ]

            def has_spec(self):
                return self.cur in self.spec

            def activate_spec(self, val):
                for o in self.spec:
                    o.active = val

        class Weapon1(Weapon, Ccw):
            pass

        class Weapon2(Weapon, TlBoltgun):
            pass

        def __init__(self, parent):
            super(Bikers.Biker, self).__init__(
                parent, name='Chaos Biker', points=Bikers.model_points,
                gear=Bikers.model_gear + [Gear('Bolt pistol')],
                wrath=20, flame=15, despair=10, excess=35, vengeance=25
            )
            self.wep1 = self.Weapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '')

        def check_rules(self):
            super(Bikers.Biker, self).check_rules()
            self.wep1.activate_spec(not self.wep2.has_spec())
            self.wep2.activate_spec(not self.wep1.has_spec())

        @ListSubUnit.count_gear
        def count_spec(self):
            return self.wep1.has_spec() or self.wep2.has_spec()

    class Champion(IconBearer):

        class Weapon1(Ranged, Melee, Ccw):
            pass

        class Weapon2(Ranged, Melee, BoltPistol):
            pass

        class Options(OptionsList):
            def __init__(self, parent):
                super(Bikers.Champion.Options, self).__init__(parent, name='Options')
                self.variant('Melta bombs', 5)
                self.variant('Gift of mutation', 10)

        def __init__(self, parent):
            super(Bikers.Champion, self).__init__(
                parent, points=70 - Bikers.model_points * 2, name='Chaos Biker Champion',
                gear=Bikers.model_gear + [Gear('Twin-linked boltgun')],
                wrath=20, flame=15, despair=10, excess=35, vengeance=25
            )

            self.wep1 = self.Weapon1(self, name='Weapon')
            self.wep2 = self.Weapon2(self, name='')
            self.Options(self)

        def check_rules(self):
            super(Bikers.Champion, self).check_rules()
            self.wep1.set_unique_ranged(not self.wep2.has_unique_ranged())
            self.wep2.set_unique_ranged(not self.wep1.has_unique_ranged())

    def not_nurgle(self):
        return self.marks.any and not self.marks.nurgle
