__author__ = 'dvarh'
from builder.core2 import Gear, OptionsList, UnitList,\
    ListSubUnit, Count, UnitDescription, OneOf, SubUnit, OptionalSubUnit
from builder.games.wh40k.roster import Unit
from .fast import Transport
from .troops import WarlockLeader
from .armory import VehicleEquipment, Ghostwalk


class DarkReapers(Unit):
    type_name = 'Dark Reapers'
    type_id = 'darkreapers_v3'
    default_min = 3
    default_max = 10
    model_points = 25
    model_gear = [Gear('Heavy Aspect armor'), Gear('Reaper launcher with starswarm missiles'), Gear('Reaper rangefinder')]

    class Missile(OptionsList):
        def __init__(self, parent):
            super(DarkReapers.Missile, self).__init__(parent, 'Missile')
            self.variant('Starshot missiles', 8)

    class Exarch(Unit):
        class Weapons(OneOf):
            def __init__(self, parent):
                super(DarkReapers.Exarch.Weapons, self).__init__(parent, 'Weapons')
                self.launcher = self.variant('Reaper launcher with starswarm missiles', 0)
                self.variant('Shuriken cannon', 0)
                self.variant('Eldar missile launcher', 20)
                self.variant('Tempest launcher', 20)

            def is_launcher(self):
                return self.cur == self.launcher

        def __init__(self, parent):
            super(DarkReapers.Exarch, self).__init__(parent, name='Dark Reaper Exarch', points=DarkReapers.model_points + 15,
                                               gear=[Gear('Heavy Aspect armor'), Gear('Reaper rangefinder')])
            self.wep = self.Weapons(self)
            self.missile = DarkReapers.Missile(self)

        def check_rules(self):
            super(DarkReapers.Exarch, self).check_rules()
            self.missile.visible = self.missile.used = self.wep.is_launcher()

    class ReaperExarch(OptionalSubUnit):
        def __init__(self, parent):
            super(DarkReapers.ReaperExarch, self).__init__(parent=parent, name='Exarch', limit=1)
            SubUnit(self, DarkReapers.Exarch(parent=None))

    def __init__(self, parent):
        super(DarkReapers, self).__init__(parent, self.type_name)
        self.models = Count(self, 'Dark Reaper', self.default_min, self.default_max, self.model_points, gear=UnitDescription('Dark Reaper', self.model_points, options=self.model_gear), per_model=True)
        self.exarch = self.ReaperExarch(self)

        self.missile = self.Missile(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.models.cur + self.exarch.count

    def check_rules(self):
        self.models.min = self.default_min - self.exarch.count
        self.models.max = self.default_max - self.exarch.count

    def build_points(self):
        return self.models.points + self.transport.points +\
            self.models.cur * self.missile.points + self.exarch.points

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription(
            name='Dark Reaper',
            options=DarkReapers.model_gear,
            count=self.models.cur,
            points=DarkReapers.model_points
        )
        desc.add(model)
        desc.add(self.exarch.description)
        desc.add(self.transport.description)
        return desc

    def build_statistics(self):
        return self.note_transport(super(DarkReapers, self).build_statistics())


class Battery(Unit):
    type_name = "Vaul\'s Wraith Support Battery"
    type_id = 'vaul_battery_v3'
    crew = UnitDescription(name='Guardian crew', count=2, options=[
        Gear('Mesh armour'),
        Gear('Shuriken catapult'),
        Gear('Plasma grenades'),
    ])

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Battery.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Shadow weaver', 0)
            self.variant('Vibro cannon', 0)
            self.variant('D-cannon', 25)

    def __init__(self, parent):
        super(Battery, self).__init__(parent=parent)
        self.models = Count(self, 'Support Weapon', 1, 3, 30, True)
        self.warlock = WarlockLeader(self)
        self.wep = self.Weapon(self)

    def build_points(self):
        return self.models.points + self.warlock.points +\
            self.models.cur * self.wep.points

    def get_count(self):
        return self.models.cur

    def build_description(self):
        desc = UnitDescription(self.name, self.points, self.get_count())
        model = UnitDescription('Support Weapon', 30 + self.wep.points,
                                self.models.cur, self.wep.description)
        desc.add(model)
        desc.add(self.crew.clone().set_count(self.models.cur * 2))
        desc.add(self.warlock.description)
        return desc

    def count_charges(self):
        return self.warlock.count

    def build_statistics(self):
        result = self.note_charges(super(Battery, self).build_statistics())
        result['Models'] = self.models.cur * 3 + self.warlock.count
        return result


class Falcons(Unit):
    type_name = "Falcons"
    type_id = "falcons_v3"

    class Falcon(ListSubUnit):
        type_name = "Falcon"
        type_id = "falcon_v3"

        class TopWeapon(OneOf):
            def __init__(self, parent):
                super(Falcons.Falcon.TopWeapon, self).__init__(parent, 'Top Weapon')
                self.variant('Shuriken cannon', 0)
                self.variant('Scatter laser', 0)
                self.variant('Starcannon', 5)
                self.variant('Bright lance', 5)
                self.variant('Eldar missile launcher', 15)

        class BottomWeapon(OneOf):
            def __init__(self, parent):
                super(Falcons.Falcon.BottomWeapon, self).__init__(parent, 'Bottom Weapon')
                self.variant('Twin-linked shuriken catapult', 0)
                self.variant('Shuriken cannon', 10)

        def __init__(self, parent):
            super(Falcons.Falcon, self).__init__(parent=parent, points=125, gear=[
                Gear('Pulse laser'),
            ], )
            self.top = self.TopWeapon(self)
            self.bottom = self.BottomWeapon(self)
            self.opt = VehicleEquipment(self)

        def build_description(self):
            desc = super(Falcons.Falcon, self).build_description()
            if self.root_unit.matrix.used and self.root_unit.matrix.any:
                desc.add(Gear('Ghostwalk matrix'))
                desc.add_points(self.root_unit.matrix.points)
            return desc

    def __init__(self, parent):
        super(Falcons, self).__init__(parent)
        self.models = UnitList(self, self.Falcon, 1, 3)
        self.matrix = Ghostwalk(parent=self)

    def check_rules(self):
        super(Falcons, self).check_rules()

    def build_points(self):
        return super(Falcons, self).build_points() + self.matrix.points * (self.models.count - 1) * self.matrix.used

    def get_count(self):
        return self.models.count


class FirePrisms(Unit):
    type_name = "Fire Prisms"
    type_id = "fireprisms_v3"

    class FirePrism(ListSubUnit):
        type_name = "Fire Prism"
        type_id = "fireprism_v3"

        class Weapon(OneOf):
            def __init__(self, parent):
                super(FirePrisms.FirePrism.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Twin-linked shuriken catapult', 0)
                self.variant('Shuriken cannon', 10)

        def __init__(self, parent):
            super(FirePrisms.FirePrism, self).__init__(parent=parent, points=125, gear=[
                Gear('Prism cannon'),
            ], )
            self.wep = self.Weapon(self)
            self.opt = VehicleEquipment(self)

        def build_description(self):
            desc = super(FirePrisms.FirePrism, self).build_description()
            if self.root_unit.matrix.used and self.root_unit.matrix.any:
                desc.add(Gear('Ghostwalk matrix'))
            return desc

    def build_points(self):
        return super(FirePrisms, self).build_points() + self.matrix.points * (self.models.count - 1) * self.matrix.used

    def __init__(self, parent):
        super(FirePrisms, self).__init__(parent)
        self.models = UnitList(self, self.FirePrism, 1, 3)
        self.matrix = Ghostwalk(parent=self)

    def get_count(self):
        return self.models.count


class NightSpinners(Unit):
    type_name = "Night Spinners"
    type_id = "nightspinners_v3"

    class NightSpinner(ListSubUnit):
        type_name = "Night Spinner"
        type_id = "nightspinners_v3"

        class Weapon(OneOf):
            def __init__(self, parent):
                super(NightSpinners.NightSpinner.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Twin-linked shuriken catapult', 0)
                self.variant('Shuriken cannon', 10)

        def __init__(self, parent):
            super(NightSpinners.NightSpinner, self).__init__(parent=parent, points=100, gear=[
                Gear('Doomweaver'),
            ], )
            self.wep = self.Weapon(self)
            self.opt = VehicleEquipment(self)

        def build_description(self):
            desc = super(NightSpinners.NightSpinner, self).build_description()
            if self.root_unit.matrix.used and self.root_unit.matrix.any:
                desc.add(Gear('Ghostwalk matrix'))
                desc.add_points(self.root_unit.matrix.points)
            return desc

    def __init__(self, parent):
        super(NightSpinners, self).__init__(parent)
        self.models = UnitList(self, self.NightSpinner, 1, 3)
        self.matrix = Ghostwalk(parent=self)

    def build_points(self):
        return super(NightSpinners, self).build_points() + self.matrix.points * (self.models.count - 1) * self.matrix.used

    def get_count(self):
        return self.models.count


class WarWalkers(Unit):
    type_name = "War Walkers"
    type_id = "warwalkers_v3"

    class WarWalker(ListSubUnit):
        type_name = "War Walker"
        type_id = "warwalker_v3"
        base_points = 60

        class Weapon(OneOf):
            def __init__(self, parent):
                super(WarWalkers.WarWalker.Weapon, self).__init__(parent, 'Weapon')
                self.variant('Shuriken cannon', 0)
                self.variant('Scatter laser', 0)
                self.variant('Starcannon', 5)
                self.variant('Bright lance', 5)
                self.variant('Eldar missile launcher', 15)

        def __init__(self, parent):
            super(WarWalkers.WarWalker, self).__init__(parent=parent, points=self.base_points, gear=[
                Gear('Power field'),
            ])
            self.left_wep = self.Weapon(self)
            self.right_wep = self.Weapon(self)
            self.opt = VehicleEquipment(self, warwalker=True)

        def build_description(self):
            desc = super(WarWalkers.WarWalker, self).build_description()
            if self.root_unit.matrix.used and self.root_unit.matrix.any:
                desc.add(Gear('Ghostwalk matrix'))
                desc.add_points(self.root_unit.matrix.points)
            return desc

    def __init__(self, parent):
        super(WarWalkers, self).__init__(parent)
        self.models = UnitList(self, self.WarWalker, 1, 3)
        self.matrix = Ghostwalk(parent=self)

    def build_points(self):
        return super(WarWalkers, self).build_points() + self.matrix.points * (self.models.count - 1) * self.matrix.used

    def get_count(self):
        return self.models.count


class Wraithlord(Unit):
    type_name = 'Wraithlord'
    type_id = 'wraithlord_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Wraithlord.Weapon, self).__init__(parent=parent, name='Weapons')
            self.variant('Shuriken catapult', 0)
            self.variant('Flamer', 0)

    class Glaive(OptionsList):
        def __init__(self, parent):
            super(Wraithlord.Glaive, self).__init__(parent, name='Glaive', limit=1)
            self.variant('Ghostglaive', 5)

    def __init__(self, parent):
        super(Wraithlord, self).__init__(parent=parent, points=120)
        self.wep1 = self.Weapon(self)
        self.wep2 = self.Weapon(self)
        self.wep3 = self.Glaive(self)
        self.shuriken = Count(self, 'Shuriken cannon', 0, 2, points=15)
        self.scatter = Count(self, 'Scatter laser', 0, 2, points=15)
        self.lance = Count(self, 'Bright lance', 0, 2, points=20)
        self.star = Count(self, 'Starcannon', 0, 2, points=20)
        self.launcher = Count(self, 'Eldar missile launcher', 0, 2, points=30)

    def check_rules(self):
        super(Wraithlord, self).check_rules()
        Count.norm_counts(0, 2, [self.shuriken, self.scatter, self.lance, self.star, self.launcher])
