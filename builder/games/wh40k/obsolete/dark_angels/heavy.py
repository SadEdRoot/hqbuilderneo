__author__ = 'Denis Romanov'
from builder.core.options import norm_counts
from builder.games.wh40k.obsolete.dark_angels.armory import ranged_pistol, melee
from builder.games.wh40k.obsolete.dark_angels.transport import *
from functools import reduce


class DeathwingVehicle(Unit):
    def __init__(self, deathwing = False):
        Unit.__init__(self)
        self.forced_dw = deathwing
        if not deathwing:
            self.dw = self.opt_options_list('Options', [["Deathwing Vehicle", 30]])

    def check_rules(self):
        self.set_points(self.build_points() + (30 if self.forced_dw else 0))
        self.build_description(add = ["Deathwing Vehicle" if self.forced_dw else None])


class LandRaider(DeathwingVehicle):
    name = "Land Raider"
    base_points = 250
    gear = ['Twin-linked heavy bolter','Twin-linked lascannon','Twin-linked lascannon','Smoke launchers','Searchlight']
    def __init__(self, deathwing = False):
        DeathwingVehicle.__init__(self, deathwing)
        self.opt = self.opt_options_list('', vehicle + [["Multi melta", 10, 'mmelta']])


class LRCrusader(LandRaider):
    name = "Land Raider Crusader"
    base_points = 250
    gear = ['Twin-linked assault cannon','Hurricane bolter','Hurricane bolter','Smoke launchers','Searchlight','Frag Assault Launcher']


class LRRedeemer(LandRaider):
    name = "Land Raider Redeemer"
    base_points = 240
    gear = ['Twin-linked assault cannon','Flamestorm cannon','Flamestorm cannon','Smoke launchers','Searchlight','Frag Assault Launcher']


class Predator(Unit):
    name = "Predator"
    base_points = 75
    gear = ['Smoke launchers','Searchlight']
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Turret', [
            ['Autocannon',0,'acan'],
            ['Twin-linked lascannon',25,'tllcan']
            ])
        self.side = self.opt_options_list('Side sponsons',[
            ['Sponsons with heavy bolters',20,'hbgun'],
            ['Sponsons with lascannons',40,'lcan']
            ],1)
        self.opt = self.opt_options_list('Options', vehicle)

class Whirlwind(Unit):
    name = "Whirlwind"
    base_points = 65
    gear = ['Whirlwind multiple missile launcher','Smoke launchers','Searchlight']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', vehicle)


class Vindicator(Unit):
    name = "Vindicator"
    base_points = 125
    gear = ['Demolisher cannon','Storm bolter','Smoke launchers','Searchlight']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', vehicle + [["Siege shield", 10, 'sshld']])

class LandSpeederVengeance(Unit):
    name = "Land Speeder Vengeance"
    base_points = 140
    gear = ['Plasma storm battery']
    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Heavy bolter',0],
            ['Assault cannon',20]
        ])


class Devastators(Unit):
    name = 'Devastator'
    gear = ['Power armor', 'Bolt pistol', 'Frag grenades', 'Krak grenades']

    class Sergeant(Unit):
        base_points = 70 - 14 * 4
        name = 'Space Marine Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Signum']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun', 0, 'bolt']] + melee)
            self.wep2 = self.opt_one_of('', ranged_pistol + melee)
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])
            self.vet = self.opt_options_list('', [["Veteran", 10, 'vet']])

        def check_rules(self):
            self.set_points(self.build_points())
            if self.vet.get('vet'):
                self.build_description(name='Veteran Sergeant', exclude=[self.vet.id])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Space Marine', 4, 9, 14)
        self.hb = self.opt_count('Heavy bolter', 0, 4, 10)
        self.mm = self.opt_count('Multi-melta', 0, 4, 10)
        self.ml = self.opt_count('Missile launcher', 0, 4, 15)
        self.flakk = self.opt_count('Flakk missiles', 0, 4, 10)
        self.pc = self.opt_count('Plasma cannon', 0, 4, 15)
        self.las = self.opt_count('Lascannon', 0, 4, 20)
        self.heavy = [self.hb, self.mm, self.ml, self.pc, self.las]
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        norm_counts(0, 4, self.heavy)
        self.flakk.update_range(0, self.ml.get())
        self.points.set(self.build_points(count=1))
        self.build_description(count=self.marines.get(), options=[self.sergeant, self.transport])
        for o in self.heavy:
            self.description.add(o.get_selected())
        self.description.add(self.flakk.get_selected())
        self.description.add('Boltgun', self.marines.get() - reduce(lambda val, o: val + o.get(), self.heavy, 0))


    def get_count(self):
        return self.marines.get() + 1

