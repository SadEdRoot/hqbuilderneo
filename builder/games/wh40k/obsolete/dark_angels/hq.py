__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.dark_angels.transport import Rhino, Razorback, DropPod
from builder.games.wh40k.obsolete.dark_angels.heavy import LandRaider, LRCrusader, LRRedeemer
from builder.core.options import norm_counts
from builder.games.wh40k.obsolete.dark_angels.armory import *
from functools import reduce


class Azrael(StaticUnit):
    name = 'Azrael'
    base_points = 215
    gear = ['Bolt pistol', 'Frag grenades', 'Krak grenades', 'Protector', "Lion's Wrath", 'Sword of Secrets',
            'Lion Helm']


class Ezekiel(StaticUnit):
    name = 'Ezekiel'
    base_points = 145
    gear = ['Master crafted bolt pistol', 'Frag grenades', 'Krak grenades', 'Artificer armour', 'Psychic hood',
            'Traitor\'s Bane', 'Book of Salvation']


class Asmodai(StaticUnit):
    name = 'Asmodai'
    base_points = 140
    gear = ['Frag grenades', 'Krak grenades', 'Power armour', 'Crozius arcanum', 'Rosarius', 'Blade of Reason']


class Belial(Unit):
    name = 'Belial'
    base_points = 190
    unique = True
    gear = ['Terminator armour', 'Iron halo', 'Teleport homer']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Storm bolter and Sword of Silence', 0],
            ['Thunder hammer and storm shield', 0],
            ['Pair of lightning claws', 0],
        ])


class Sammael(Unit):
    name = 'Sammael'
    base_points = 200
    unique = True
    gear = ['Power armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Teleport homer', 'Raven Sword',
            'Adamantine Mantle', 'Iron Halo']

    class Sableclaw(StaticUnit):
        name = 'Sableclaw'
        base_points = 0
        gear = ['Twin-linked assault cannon', 'Twin-linked heavy bolter', 'Night Halo']

    def __init__(self):
        Unit.__init__(self)
        self.mount = self.opt_one_of('Transport', [
            ['Corvex', 0, 'jb'],
            ['Sableclaw', 0],
        ])
        self.claw = self.opt_sub_unit(self.Sableclaw())

    def check_rules(self):
        self.set_points(self.base_points)
        claw = not self.mount.get_cur() == 'jb'
        self.claw.set_active(claw)
        self.build_description(exclude=[self.mount.id if claw else None])


class CompanyMaster(Unit):
    name = 'Company Master'
    base_points = 90

    def __init__(self):
        Unit.__init__(self)
        self.armr = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Artificer armour', 20, 'art'],
            ['Terminator armour', 40, 'tda']
        ])

        self.wep1 = self.opt_one_of('Weapon', melee + ranged + relic_weapon)
        self.wep2 = self.opt_one_of('', ranged_pistol + melee + relic_weapon)
        self.tda_wep1 = self.opt_one_of('Weapon', tda_weapon + relic_weapon)
        self.tda_wep2 = self.opt_one_of('', tda_power + relic_weapon)
        self.relic = self.opt_options_list('Relic', [['Perfidious Relic of the Unforgiven', 15, 'pru']] + relic_gear)
        self.opt = self.opt_options_list('Options', spec + [['Storm Shield', 15, 'ss']])
        self.ride = self.opt_options_list('', spec_jump)

    def check_rules(self):
        tda = self.has_tda()
        self.ride.set_visible(not tda)
        self.wep1.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.tda_wep1.set_visible(tda)
        self.tda_wep2.set_visible(tda)
        self.gear = ['Iron halo']
        if not tda:
            self.gear += ['Frag grenades', 'Krak grenades']
        Unit.check_rules(self)

    def has_tda(self):
        return self.armr.get_cur() == 'tda'


class Librarian(Unit):
    name = 'Librarian'
    base_points = 65
    gear = ['Force weapon', 'Psychic hood']

    def __init__(self):
        Unit.__init__(self)
        self.armr = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Terminator armour', 30, 'tda']
        ])

        self.wep = self.opt_one_of('Weapon', ranged_pistol + melee + relic_weapon)
        self.tda_wep = self.opt_one_of('Weapon', tda_weapon + relic_weapon)
        self.relic = self.opt_options_list('Relic', relic_gear)
        self.psy = self.opt_one_of('Psyker', [['Mastery Level 1', 0, 'lvl1'], ['Mastery Level 2', 35, 'lvl2']])
        self.opt = self.opt_options_list('Options', spec)
        self.ride = self.opt_options_list('', spec_jump + spec_bike, limit=1)

    def check_rules(self):
        tda = self.has_tda()
        self.ride.set_visible(not tda)
        self.wep.set_visible(not tda)
        self.tda_wep.set_visible(tda)
        Unit.check_rules(self)
        if not tda:
            self.description.add(['Frag grenades', 'Krak grenades'])

    def has_tda(self):
        return self.armr.get_cur() == 'tda'

    def has_bike(self):
        return self.ride.get('bike')


class InterrogatorChaplain(Unit):
    name = 'Interrogator-Chaplain'
    base_points = 110

    def __init__(self):
        Unit.__init__(self)
        self.armr = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Terminator armour', 30, 'tda']
        ])

        self.wep = self.opt_one_of('Weapon', ranged_pistol + melee + relic_weapon)
        self.tda_wep = self.opt_one_of('Weapon', tda_weapon + relic_weapon)
        self.relic = self.opt_options_list('Relic', relic_gear)
        self.opt = self.opt_options_list('Options', spec)
        self.ride = self.opt_options_list('', spec_jump + spec_bike, limit=1)

    def check_rules(self):
        tda = self.has_tda()
        self.ride.set_visible(not tda)
        self.wep.set_visible(not tda)
        self.tda_wep.set_visible(tda)
        self.gear = ['Rozarius', 'Crozius Arcanum']
        if not tda:
            self.gear += ['Frag grenades', 'Krak grenades']
        Unit.check_rules(self)

    def has_tda(self):
        return self.armr.get_cur() == 'tda'

    def has_bike(self):
        return self.ride.get('bike')


class Chaplain(Unit):
    name = 'Chaplain'
    base_points = 90
    gear = ['Power armour', 'Rozarius', 'Crozius Arcanum', 'Frag grenades', 'Krak grenades']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', ranged_pistol + melee + relic_weapon)
        self.opt = self.opt_options_list('Options', spec)
        self.ride = self.opt_options_list('', spec_jump + spec_bike, limit=1)

    def has_bike(self):
        return self.ride.get('bike')


class CommandSquad(Unit):
    name = 'Command Squad'

    class Veteran(Unit):
        name = 'Veteran'
        base_points = 20
        gear = ["Power Armour", 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            self.up = self.opt_options_list('', [
                ["Company champion", 15, 'ch'],
                ["Apothecary", 15, 'ap'],
            ], limit=1)
            weaponlist = [
                ['Bolter', 0, 'bgun'],
                ['Storm Bolter', 3, 'sbgun'],
                ['Flamer', 5, 'flame'],
                ['Meltagun', 10, 'mgun'],
                ['Combi-melta', 10, 'cmelta'],
                ['Combi-flamer', 10, 'cflame'],
                ['Combi-plasma', 10, 'cplasma'],
                ['Plasmagun', 15, 'psw'],
                ['Power weapon', 15, 'psw'],
                ['Lightning claw', 15, 'lclaw'],
                ['Power fist', 25, 'pfist'],
                ['Thunder hammer', 30, 'ham']
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Chainsword', 0, 'csw']] + weaponlist)
            self.wep2 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist'],
                                                   ['Plasma pistol', 15, 'ppist']] + weaponlist)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Storm shield', 15, 'ss']
            ])
            self.banner = self.opt_options_list('Standard', [
                ['Company Standard', 15, 'comstd'],
                ['Revered Standard', 25, 'revstd'],
                ['Dark Angels Chapter Banner', 45, 'cb'],
            ] + standard, limit=1)

        def get_unique_banners(self):
            ids = self.banner.get_all()
            if not ids or ids[0] == 'comstd' or ids[0] == 'revstd':
                return []
            return self.banner.get_selected()

        def check_rules(self):
            ch = self.up.get('ch')
            ap = self.up.get('ap')
            self.wep1.set_visible(not ch and not ap)
            self.wep2.set_visible(not ch and not ap)
            self.opt.set_visible(not ch and not ap)
            self.banner.set_visible(not ch and not ap)

            self.set_points(self.build_points())
            if ch:
                self.build_description(name='Company champion', exclude=[self.up.id],
                                       add=['Blade of Caliban', 'Combat Shield', 'Bolt pistol'])
            elif ap:
                self.build_description(name='Apothecary', exclude=[self.up.id], add=['Chainsword', 'Narthecium'])
            else:
                self.build_description()

    def __init__(self):
        Unit.__init__(self)
        self.hg = [self.opt_sub_unit(self.Veteran()) for _ in range(0, 5)]
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        ch = sum((1 for c in self.hg if c.get_unit().up.get('ch')))
        ap = sum((1 for c in self.hg if c.get_unit().up.get('ap')))
        flag = sum((1 for c in self.hg if c.get_unit().banner.get_all()))
        if ch > 1:
            self.error('Only one Veteran can be upgraded to Company champion (upgraded: {})'.format(ch))
        if ap > 1:
            self.error('Only one Veteran can be upgraded to Apothecary (upgraded: {})'.format(ap))
        if flag > 1:
            self.error('Only one Veteran can take a banner (taken: {})'.format(flag))
        self.points.set(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return 5

    def get_unique_gear(self):
        return sum((u.get_unit().get_unique_banners() for u in self.hg), [])


class CommandSquad_v2(Unit):
    name = 'Command Squad'

    class Veteran(ListSubUnit):
        name = 'Veteran'
        base_points = 20
        gear = ["Power Armour", 'Frag grenades', 'Krak grenades']

        def __init__(self):
            ListSubUnit.__init__(self)
            weaponlist = [
                ['Bolter', 0, 'bgun'],
                ['Storm Bolter', 3, 'sbgun'],
                ['Flamer', 5, 'flame'],
                ['Meltagun', 10, 'mgun'],
                ['Combi-melta', 10, 'cmelta'],
                ['Combi-flamer', 10, 'cflame'],
                ['Combi-plasma', 10, 'cplasma'],
                ['Plasmagun', 15, 'psw'],
                ['Power weapon', 15, 'psw'],
                ['Lightning claw', 15, 'lclaw'],
                ['Power fist', 25, 'pfist'],
                ['Thunder hammer', 30, 'ham']
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Chainsword', 0, 'csw']] + weaponlist)
            self.wep2 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist'],
                                                   ['Plasma pistol', 15, 'ppist']] + weaponlist)
            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Storm shield', 15, 'ss']
            ])
            self.banner = self.opt_options_list('Standard', [
                ['Company Standard', 15, 'comstd'],
                ['Revered Standard', 25, 'revstd'],
                ['Dark Angels Chapter Banner', 45, 'cb'],
            ] + standard, limit=1)

        def get_unique_banners(self):
            ids = self.banner.get_all()
            if not ids or ids[0] == 'comstd' or ids[0] == 'revstd':
                return []
            return self.banner.get_selected()

        def count_banners(self):
            return self.get_count() if self.banner.get_all() else 0

    def __init__(self):
        Unit.__init__(self)
        self.hg = self.opt_units_list(self.name, self.Veteran, 1, 5)
        self.hg.get_units()[0].count.set(5)
        self.up = self.opt_options_list('', [
            ["Company champion", self.Veteran.base_points + 15, 'ch'],
            ["Apothecary", self.Veteran.base_points + 15, 'ap'],
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        self.hg.update_range()
        flag = sum((c.count_banners() for c in self.hg.get_units()))
        if flag > 1:
            self.error('Only one Veteran can take a banner (taken: {})'.format(flag))
        if self.get_count() != 5:
            self.error('Command Squad must include 5 Veterans (include Apothecary and Company champion) '
                       '(taken: {})'.format(self.get_count()))
        self.points.set(self.build_points(count=1))
        self.build_description(count=1, exclude=[self.up.id])
        if self.up.get('ch'):
            self.description.add(ModelDescriptor(
                'Company champion',
                points=self.Veteran.base_points + 15,
                gear=["Power Armour", 'Frag grenades', 'Krak grenades', 'Blade of Caliban', 'Combat Shield',
                      'Bolt pistol'],
                count=1).build())
        if self.up.get('ap'):
            self.description.add(ModelDescriptor(
                'Apothecary',
                points=self.Veteran.base_points + 15,
                gear=["Power Armour", 'Frag grenades', 'Krak grenades', 'Chainsword', 'Narthecium'],
                count=1).build())

    def get_count(self):
        return self.hg.get_count() + len(self.up.get_all())

    def get_unique_gear(self):
        return sum((u.get_unique_banners() for u in self.hg.get_units()), [])


class DeathwingCommandSquad(Unit):
    name = 'Deathwing Command Squad'

    class Veteran(Unit):
        name = 'Deathwing Terminator'
        base_points = 220 / 5
        gear = ["Terminator Armour"]

        def __init__(self):
            Unit.__init__(self)
            self.up = self.opt_options_list('', [
                ["Deathwing Champion", 5, 'ch'],
                ["Deathwing Apothecary", 30, 'ap'],
            ], limit=1)

            self.wep1 = self.opt_one_of('Weapon', [['Power fist', 0], ['Chainfist', 5]])
            self.wep2 = self.opt_one_of('', [
                ['Storm bolter', 0, 'bolt'],
                ['Heavy flamer', 10, 'hf'],
                ['Plasma Cannon', 15, 'pc'],
                ['Assault Cannon', 20, 'ac'],
                ['Cyclone missile launcher', 25, 'cyc']
            ])
            self.spec = ['hf', 'pc', 'ac', 'cyc']
            self.both = self.opt_options_list('', [
                ['Pair of lightning claws', 0, 'lc'],
                ['Thunder hammer and storm shield', 5, 'thss'],
            ], limit=1)

            self.banner = self.opt_options_list('Standard', [
                ['Deathwing  Company Banner', 45, 'cb'],
                ['Revered Standard', 20, 'revstd'],
            ] + standard, limit=1)

            self.have_flag = False
            self.have_champ = False
            self.have_apoth = False
            self.have_spec = False

        def has_spec_wep(self):
            return self.wep2.get_cur() != 'bolt' and self.both.get_all() == []

        def check_rules(self):
            ch = self.up.get('ch')
            ap = self.up.get('ap')
            self.both.set_visible(not ch and not ap)
            self.wep1.set_visible(not ch and not ap and self.both.get_all() == [])
            self.wep2.set_visible(not ch and not ap and self.both.get_all() == [])
            self.banner.set_visible(not ch and not ap)

            self.up.set_active_options(['ch'], not self.have_champ or ch)
            self.up.set_active_options(['ap'], not self.have_apoth or ap)
            if not self.banner.get_all():
                self.banner.set_active_options(self.banner.get_all_ids(), not self.have_flag)
            if not self.has_spec_wep():
                self.wep2.set_active_options(self.spec, not self.have_spec)

            self.set_points(self.build_points())
            if ch:
                self.build_description(name='Deathwing Champion', exclude=[self.up.id], add=['Halberd of Caliban'])
            elif ap:
                self.build_description(name='Deathwing Apothecary', exclude=[self.up.id], add=['Power fist',
                                                                                               'Narthecium'])
            else:
                self.build_description(add=['Storm bolter' if self.wep2.get_cur() == 'cyc' else None])

    def __init__(self):
        Unit.__init__(self)
        self.hg = [self.opt_sub_unit(self.Veteran()) for _ in range(0, 5)]
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(True), LRCrusader(True), LRRedeemer(True)])

    def check_rules(self):
        ch = reduce(lambda val, c: val + (1 if c.get_unit().up.get('ch') else 0), self.hg, 0)
        ap = reduce(lambda val, c: val + (1 if c.get_unit().up.get('ap') else 0), self.hg, 0)
        flag = reduce(lambda val, c: val + (1 if c.get_unit().banner.get_all() else 0), self.hg, 0)
        spec = reduce(lambda val, c: val + (1 if c.get_unit().has_spec_wep() else 0), self.hg, 0)
        for hg in self.hg:
            hg.get_unit().have_champ = (ch > 0)
            hg.get_unit().have_flag = (flag > 0)
            hg.get_unit().have_apoth = (ap > 0)
            hg.get_unit().have_spec = (spec > 0)
            hg.get_unit().check_rules()
        self.points.set(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return 5


class DeathwingCommandSquad_v2(Unit):
    name = 'Deathwing Command Squad'

    class Veteran(ListSubUnit):
        name = 'Deathwing Terminator'
        base_points = 220 / 5
        gear = ["Terminator Armour"]

        def __init__(self):
            ListSubUnit.__init__(self)

            self.wep1 = self.opt_one_of('Weapon', [['Power fist', 0], ['Chainfist', 5]])
            self.wep2 = self.opt_one_of('', [
                ['Storm bolter', 0, 'bolt'],
                ['Heavy flamer', 10, 'hf'],
                ['Plasma Cannon', 15, 'pc'],
                ['Assault Cannon', 20, 'ac'],
            ])
            self.cyc = self.opt_options_list('', [
                ['Cyclone missile launcher', 25, 'cyc']
            ])
            self.spec = ['hf', 'pc', 'ac']
            self.both = self.opt_options_list('', [
                ['Pair of lightning claws', 0, 'lc'],
                ['Thunder hammer and storm shield', 5, 'thss'],
            ], limit=1)

            self.banner = self.opt_options_list('Standard', [
                ['Deathwing Company Banner', 45, 'cb'],
                ['Revered Standard', 20, 'revstd'],
            ] + standard, limit=1)

        def has_spec_wep(self):
            return self.get_count() if self.wep2.get_cur() in self.spec or self.cyc.get('cyc') else 0

        def get_unique_banners(self):
            ids = self.banner.get_all()
            if not ids or ids[0] == 'revstd':
                return []
            return self.banner.get_selected()

        def count_banners(self):
            return self.get_count() if self.banner.get_all() else 0

        def check_rules(self):
            has_pair = len(self.both.get_all()) > 0
            self.wep1.set_visible(not has_pair)
            self.wep2.set_visible(not has_pair)
            self.wep2.set_active_options(self.spec, not self.cyc.get('cyc'))
            self.cyc.set_visible(self.wep2.get_cur() not in self.spec)
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.hg = self.opt_units_list(self.name, self.Veteran, 1, 5)
        self.hg.get_units()[0].count.set(5)
        self.up = self.opt_options_list('', [
            ["Deathwing champion", self.Veteran.base_points + 5, 'ch'],
            ["Deathwing Apothecary", self.Veteran.base_points + 30, 'ap'],
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(True), LRCrusader(True), LRRedeemer(True)])

    def check_rules(self):
        self.hg.update_range()
        flag = sum((c.count_banners() for c in self.hg.get_units()))

        if flag > 1:
            self.error('Only one Deathwing Terminator can take a banner (taken: {})'.format(flag))

        heavy = sum((c.has_spec_wep() for c in self.hg.get_units()))
        if heavy > 1:
            self.error('Only one Deathwing Terminator can special weapon (taken: {})'.format(heavy))
        if self.get_count() != 5:
            self.error('Deathwing Command Squad must include 5 Deathwing Terminators (include Deathwing Apothecary and '
                       'Deathwing champion) (taken: {})'.format(self.get_count()))
        self.points.set(self.build_points(count=1))
        self.build_description(count=1, exclude=[self.up.id])
        if self.up.get('ch'):
            self.description.add(ModelDescriptor(
                'Deathwing champion',
                points=self.Veteran.base_points + 5,
                gear=["Terminator Armour", 'Halberd of Caliban'],
                count=1).build())
        if self.up.get('ap'):
            self.description.add(ModelDescriptor(
                'Deathwing Apothecary',
                points=self.Veteran.base_points + 30,
                gear=["Terminator Armour", 'Power fist', 'Narthecium'],
                count=1).build())

    def get_count(self):
        return self.hg.get_count() + len(self.up.get_all())

    def get_unique_gear(self):
        return sum((u.get_unique_banners() for u in self.hg.get_units()), [])


class RavenwingCommandSquad(Unit):
    name = 'Ravenwing Command Squad'

    class Veteran(Unit):
        name = 'Ravenwing Black Knight'
        base_points = 40
        gear = ["Power Armour", 'Frag grenades', 'Krak grenades', 'Teleport Homer']

        def __init__(self):
            Unit.__init__(self)
            self.up = self.opt_options_list('', [
                ["Ravenwing Champion", 5, 'ch'],
                ["Ravenwing Apothecary", 30, 'ap'],
            ], limit=1)

            self.wep1 = self.opt_one_of('Weapon', [['Plasma talon', 0], ['Ravenwing grenade launcher', 0]])
            self.banner = self.opt_options_list('Standard', [
                ['Ravenwing Company Banner', 15, 'cb'],
                ['Revered Standard', 25, 'revstd'],
            ] + standard, limit=1)

            self.have_flag = False
            self.have_champ = False
            self.have_apoth = False

        def check_rules(self):
            ch = self.up.get('ch')
            ap = self.up.get('ap')
            self.wep1.set_visible(not ch and not ap)
            self.banner.set_visible(not ch and not ap)

            self.up.set_active_options(['ch'], not self.have_champ or ch)
            self.up.set_active_options(['ap'], not self.have_apoth or ap)
            if not self.banner.get_all():
                self.banner.set_active_options(self.banner.get_all_ids(), not self.have_flag)

            self.set_points(self.build_points())
            if ch:
                self.build_description(name='Ravenwing Champion', exclude=[self.up.id], add=['Blade of Caliban',
                                                                                             'Bolt pistol',
                                                                                             'Plasma Talon'])
            elif ap:
                self.build_description(name='Ravenwing Apothecary', exclude=[self.up.id], add=['Corvus hammer',
                                                                                               'Narthecium',
                                                                                               'Plasma Talon'])
            else:
                self.build_description(add=['Corvus hammer', 'Bolt pistol'])

    def __init__(self):
        Unit.__init__(self)
        self.hg = [self.opt_sub_unit(self.Veteran()) for _ in range(0, 3)]

    def check_rules(self):
        ch = reduce(lambda val, c: val + (1 if c.get_unit().up.get('ch') else 0), self.hg, 0)
        ap = reduce(lambda val, c: val + (1 if c.get_unit().up.get('ap') else 0), self.hg, 0)
        flag = reduce(lambda val, c: val + (1 if c.get_unit().banner.get_all() else 0), self.hg, 0)
        for hg in self.hg:
            hg.get_unit().have_champ = (ch > 0)
            hg.get_unit().have_flag = (flag > 0)
            hg.get_unit().have_apoth = (ap > 0)
            hg.get_unit().check_rules()
        self.points.set(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return 3


class RavenwingCommandSquad_v2(Unit):
    name = 'Ravenwing Command Squad'

    class Veteran(ListSubUnit):
        name = 'Ravenwing Black Knight'
        base_points = 40
        gear = ["Power Armour", 'Frag grenades', 'Krak grenades', 'Teleport Homer']

        def __init__(self):
            ListSubUnit.__init__(self)

            self.wep1 = self.opt_one_of('Weapon', [['Plasma talon', 0], ['Ravenwing grenade launcher', 0]])
            self.banner = self.opt_options_list('Standard', [
                ['Ravenwing Company Banner', 15, 'cb'],
                ['Revered Standard', 25, 'revstd'],
            ] + standard, limit=1)

        def get_unique_banners(self):
            ids = self.banner.get_all()
            if not ids or ids[0] == 'revstd':
                return []
            return self.banner.get_selected()

        def count_banners(self):
            return self.get_count() if self.banner.get_all() else 0

    def __init__(self):
        Unit.__init__(self)
        self.hg = self.opt_units_list(self.name, self.Veteran, 3, 5)
        self.up = self.opt_options_list('', [
            ["Ravenwing Champion", self.Veteran.base_points + 5, 'ch'],
            ["Ravenwing Apothecary", self.Veteran.base_points + 30, 'ap'],
        ])

    def check_rules(self):
        self.hg.update_range()
        flag = sum((c.count_banners() for c in self.hg.get_units()))
        if flag > 1:
            self.error('Only one Ravenwing Black Knight can take a banner (taken: {})'.format(flag))
        self.points.set(self.build_points(count=1))
        self.build_description(count=1, exclude=[self.up.id])
        if self.up.get('ch'):
            self.description.add(ModelDescriptor(
                'Ravenwing Champion',
                points=self.Veteran.base_points + 5,
                gear=self.Veteran.gear + ['Blade of Caliban', 'Bolt pistol', 'Plasma Talon'],
                count=1).build())
        if self.up.get('ap'):
            self.description.add(ModelDescriptor(
                'Ravenwing Apothecary',
                points=self.Veteran.base_points + 30,
                gear=self.Veteran.gear + ['Corvus hammer', 'Narthecium', 'Plasma Talon'],
                count=1).build())

    def get_count(self):
        return self.hg.get_count() + len(self.up.get_all())

    def get_unique_gear(self):
        return sum((u.get_unique_banners() for u in self.hg.get_units()), [])


class Techmarine(Unit):
    name = 'Techmarine'
    base_points = 50
    gear = ['Frag grenades', 'Krak grenades', 'Artificar armour']

    def __init__(self):
        Unit.__init__(self)
        self.harness = self.opt_one_of('Special', [
            ['Servo-arm', 0],
            ['Servo-harness', 25],
        ])
        self.wep = self.opt_one_of('Weapon', ranged_pistol + melee)
        self.ride = self.opt_options_list('Options', spec)
        self.opt = self.opt_options_list('', spec_bike)


class Servitors(Unit):
    name = "Servitors"
    base_points = 10

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count(self.name, 1, 5, self.base_points)
        self.hw = [
            self.opt_count('Heavy bolter', 0, 2, 10),
            self.opt_count('Multi-melta', 0, 2, 20),
            self.opt_count('Plasma cannon', 0, 2, 20)
        ]

    def check_rules(self):
        norm_counts(0, min(2, self.count.get()), self.hw)
        self.set_points(self.build_points(count=1, base_points=0))
        self.build_description(exclude=[self.count.id])
        self.description.add('Servo-Arm', self.count.get() - reduce(lambda val, h: val + h.get(), self.hw, 0))
