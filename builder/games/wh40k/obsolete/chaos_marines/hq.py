﻿# -*- coding: utf-8 -*-

__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit
from builder.games.wh40k.obsolete.chaos_marines.armory import *
from functools import reduce


class Abaddon(StaticUnit):
    name = 'Abaddon the Despoiler'
    base_points = 265
    gear = ['Terminator armour', 'Drach\'nyen', 'Talon of Horus', 'Veterans of the Long War', 'Mark of Chaos Ascendant']


class Huron(StaticUnit):
    name = 'Huron Blackheart'
    base_points = 160
    gear = ['Power armour', 'Power axe', 'Frag grenades', 'Krak grenades', 'Sigil of corruption', 'The Tyrant\'s claw',
            'Veterans of the Long War']


class Kharn(StaticUnit):
    name = 'Khârn the Betrayer'
    base_points = 160
    gear = ['Power armour', 'Plasma pistol', 'Frag grenades', 'Krak grenades', 'Aura of dark glory', 'Gorechild',
            'Mark of Khorne', 'Veterans of the Long War']


class Ahriman(StaticUnit):
    name = 'Ahriman'
    base_points = 230
    gear = ['Power armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Aura of dark glory', 'Inferno Bolts',
            'The Black Staff of Ahriman', 'Mark of Tzeentch', 'Veterans of the Long War']


class Typhus(StaticUnit):
    name = 'Typhus'
    base_points = 230
    gear = ['Terminator armour', 'Blight grenades', 'Manreaper', 'Mark of Nurgle', 'Veterans of the Long War']


class Lucius(StaticUnit):
    name = 'Lucius the Eternal'
    base_points = 165
    gear = ['Power sword', 'Doom syren', 'Frag grenades', 'Krak grenades', 'Armour of Shrieking Souls',
            'Lash of Torment', 'Mark of Slaanesh', 'Veterans of the Long War']


class Fabius(StaticUnit):
    name = 'Fabius Bile'
    base_points = 165
    gear = ['Power armour', 'Bolt pistol', 'Frag grenades', 'Krak grenades', 'Xyclos Needler', 'Rod of Torment',
            'The Chirurgeon']


def get_unique_opt(o):
    if o.get_cur() in artefact_weapon_ids:
        return [o.get_selected()]
    return []


def exclude_gear(o1, o2, ids):
    o1.set_active_options(ids, not o2.get_cur() in ids)
    o2.set_active_options(ids, not o1.get_cur() in ids)


class LordV2(Unit):
    name = 'Chaos Lord'
    base_points = 65

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.arm = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Terminator armour', 40, 'tda']
        ])
        wep = artefact_weapon if not self.black_legion else []
        self.pwep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + melee + ranged + wep)
        self.pwep2 = self.opt_one_of('', [['Close combat weapon', 0, 'ccw']] + melee + ranged + wep)
        self.twep1 = self.opt_one_of('Weapon', [['Combi-bolter', 0, 'cbgun']] + tda_ranged + wep)
        self.twep2 = self.opt_one_of('', [['Power weapon', 0, 'pwep']] + tda_melee + wep)
        self.mark = self.opt_options_list('Marks', marks, 1)
        self.opt = self.opt_options_list('Options', special_gear)
        self.rewards = self.opt_options_list('', reward)
        self.artefacts = self.opt_options_list('', artefact_option if not self.black_legion else bl_artefacts)
        if not self.black_legion:
            self.vet = self.opt_options_list('', [['Veterans of the Long War', 5, 'lwvet']])
        self.ride = self.opt_options_list('', ride, 1)

    def check_rules(self):
        #check options that depend on armour
        tda = self.arm.get_cur() == 'tda'
        self.twep1.set_visible(tda)
        self.twep2.set_visible(tda)
        self.pwep1.set_visible(not tda)
        self.pwep2.set_visible(not tda)
        self.ride.set_visible(not tda)

        # make sure only 1 weapon from Artefact Weapons or Ranged is taken
        exclude_gear(self.pwep1, self.pwep2, ranged_ids)
        if not self.black_legion:
            exclude_gear(self.pwep1, self.pwep2, artefact_weapon_ids)
            exclude_gear(self.twep1, self.twep2, artefact_weapon_ids)

        #check options that depend on mark
        self.opt.set_visible_options(['blgr'], self.mark.get('ng'))
        if not self.black_legion:
            self.artefacts.set_visible_options(['msc'], self.mark.get('tz'))
            self.pwep1.set_active_options(['blind'], self.mark.get('kh'))
            self.pwep2.set_active_options(['blind'], self.mark.get('kh'))
            self.twep1.set_active_options(['blind'], self.mark.get('kh'))
            self.twep2.set_active_options(['blind'], self.mark.get('kh'))

        self.ride.set_visible_options(['disc'], self.mark.get('tz'))
        self.ride.set_visible_options(['pal'], self.mark.get('ng'))
        self.ride.set_visible_options(['steed'], self.mark.get('sl'))
        self.ride.set_visible_options(['jug'], self.mark.get('kh'))

        self.points.set(self.build_points() + (5 if self.black_legion else 0))
        self.build_description(gear=['Frag grenades', 'Krak grenades'] if not tda else [])
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_unique_gear(self):
        return sum([get_unique_opt(o) for o in [self.pwep1, self.pwep2, self.twep1, self.twep2]], []) + \
            self.artefacts.get_selected()


class SorcV2(Unit):
    name = 'Sorcerer'
    base_points = 60

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.arm = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Terminator armour', 25, 'tda']
        ])
        wep = artefact_weapon if not self.black_legion else []
        self.pwep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + melee + ranged + wep)
        self.pwep2 = self.opt_one_of('', [['Force weapon', 0, 'fw']] + melee + ranged + wep)
        self.twep1 = self.opt_one_of('Weapon', [['Combi-bolter', 0, 'cbgun']] + tda_ranged + wep)
        self.twep2 = self.opt_one_of('', [['Force weapon', 0, 'fw']] + tda_melee + wep)
        #psykers cannot into khorne
        self.mark = self.opt_options_list('Marks', marks[1:], 1)
        self.opt = self.opt_options_list('Options', special_gear)
        self.rewards = self.opt_options_list('', reward)
        self.artefacts = self.opt_options_list('', artefact_option if not self.black_legion else bl_psy_artefacts)
        if not self.black_legion:
            self.vet = self.opt_options_list('', [['Veterans of the Long War', 5, 'lwvet']])
        self.ride = self.opt_options_list('', ride, 1)
        self.lev = self.opt_options_list('Psyker', [
            ['Mastery Level 2', 25],
            ['Mastery Level 3', 50],
        ], limit=1)

    def check_rules(self):
        #check options that depend on armour
        tda = self.arm.get_cur() == 'tda'
        self.twep1.set_visible(tda)
        self.twep2.set_visible(tda)
        self.pwep1.set_visible(not tda)
        self.pwep2.set_visible(not tda)
        self.ride.set_visible(not tda)

        #check options that depend on mark
        self.opt.set_visible_options(['blgr'], self.mark.get('ng'))
        self.ride.set_visible_options(['pal'], self.mark.get('ng'))
        self.ride.set_visible_options(['steed'], self.mark.get('sl'))

        self.ride.set_visible_options(['disc'], self.mark.get('tz'))
        if not self.black_legion:
            self.artefacts.set_visible_options(['msc'], self.mark.get('tz'))

        # make sure only 1 weapon from Artefact Weapons or Ranged is taken
        exclude_gear(self.pwep1, self.pwep2, ranged_ids)
        if not self.black_legion:
            exclude_gear(self.pwep1, self.pwep2, artefact_weapon_ids)
            exclude_gear(self.twep1, self.twep2, artefact_weapon_ids)

        self.ride.set_visible_options(['jug'], False)
        if not self.black_legion:
            self.pwep1.set_active_options(['blind'], False)
            self.pwep2.set_active_options(['blind'], False)
            self.twep1.set_active_options(['blind'], False)
            self.twep2.set_active_options(['blind'], False)

        self.points.set(self.build_points() + (5 if self.black_legion else 0))
        self.build_description(gear=['Frag grenades', 'Krak grenades'] if not tda else [])
        if self.black_legion:
            self.description.add('Veterans of the Long War')
        if len(self.lev.get_all()) == 0:
            self.description.add('Mastery Level 1')

    def get_unique_gear(self):
        return sum([get_unique_opt(o) for o in [self.pwep1, self.pwep2, self.twep1, self.twep2]], []) + \
            self.artefacts.get_selected()


class DaemonPrinceV2(Unit):
    name = "Daemon Prince"
    base_points = 145
    gear = ['Veterans of the Long War']

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.god = self.opt_one_of('', [
            ['Daemon of Khorne', 15, 'kh'],
            ['Daemon of Tzeentch', 15, 'tz'],
            ['Daemon of Nurgle', 15, 'ng'],
            ['Daemon of Slaanesh', 10, 'sl']
        ])
        self.black_legion = black_legion
        wep = artefact_weapon if not self.black_legion else []
        self.wep = self.opt_one_of('Weapon', [['Close combat weapon', 0, 'ccw']] + wep)
        self.opt = self.opt_options_list('Options', [
            ['Wings', 40, 'wng'],
            ['Power armour', 20, 'pwr']
        ])
        self.rewards = self.opt_options_list('', reward)
        self.artefacts = self.opt_options_list('', artefact_option if not self.black_legion else bl_psy_artefacts)
        self.lev = self.opt_options_list('Psyker', [
            ['Mastery Level 1', 25],
            ['Mastery Level 2', 50],
            ['Mastery Level 3', 75],
        ], limit=1)

    def check_rules(self):
        self.lev.set_visible(not self.god.get_cur() == 'kh')
        if not self.black_legion:
            self.wep.set_active_options(['blind'], self.god.get_cur() == 'kh')
            self.artefacts.set_visible_options(['msc'], self.god.get_cur() == 'tz')
        else:
            self.artefacts.set_visible_options(['mem'], self.lev.is_any_selected())
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum([get_unique_opt(o) for o in [self.wep]], []) + self.artefacts.get_selected()


class WarpsmithV2(Unit):
    name = "Warpsmith"
    base_points = 110
    gear = ['Fleshmetal', 'Frag grenades', 'Krak grenades', 'Mechatendrils']

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        wep = artefact_weapon if not self.black_legion else []
        self.wep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + ranged + wep)
        self.wep2 = self.opt_one_of('', [['Power axe', 0, 'paxe']] + ranged + wep)
        self.mark = self.opt_options_list('Mark of Chaos', marks, 1)
        self.rewards = self.opt_options_list('Options', reward)
        self.artefacts = self.opt_options_list('', artefact_option if not self.black_legion else bl_artefacts)
        if not self.black_legion:
            self.vet = self.opt_options_list('', [['Veterans of the Long War', 5, 'lwvet']])

    def check_rules(self):
        exclude_gear(self.wep1, self.wep2, ranged_ids)

        if not self.black_legion:
            exclude_gear(self.wep1, self.wep2, artefact_weapon_ids)
            self.wep1.set_active_options(['blind'], self.mark.get('kh'))
            self.wep2.set_active_options(['blind'], self.mark.get('kh'))
            self.artefacts.set_visible_options(['msc'], self.mark.get('tz'))

        self.points.set(self.build_points() + (5 if self.black_legion else 0))
        self.build_description(add=['Veterans of the Long War'] if self.black_legion else [])

    def get_unique_gear(self):
        return sum([get_unique_opt(o) for o in [self.wep1, self.wep2]], []) + \
            self.artefacts.get_selected()


class ApostleV2(Unit):
    name = "Dark Apostle"
    base_points = 105
    gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Sigil of Corruption']

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        wep = artefact_weapon if not self.black_legion else []
        self.wep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + ranged + wep)
        self.wep2 = self.opt_one_of('Weapon', [['Power maul', 0, 'pml']] + ranged + wep)
        self.mark = self.opt_options_list('Mark of Chaos', marks, 1)
        self.rewards = self.opt_options_list('Options', reward)
        self.artefacts = self.opt_options_list('', artefact_option if not self.black_legion else bl_artefacts)
        if not self.black_legion:
            self.vet = self.opt_options_list('', [['Veterans of the Long War', 5, 'lwvet']])

    def check_rules(self):
        exclude_gear(self.wep1, self.wep2, ranged_ids)

        if not self.black_legion:
            exclude_gear(self.wep1, self.wep2, artefact_weapon_ids)

            self.wep1.set_active_options(['blind'], self.mark.get('kh'))
            self.wep2.set_active_options(['blind'], self.mark.get('kh'))
            self.artefacts.set_visible_options(['msc'], self.mark.get('tz'))

        self.points.set(self.build_points() + (5 if self.black_legion else 0))
        self.build_description(add=['Veterans of the Long War'] if self.black_legion else [])

    def get_unique_gear(self):
        return sum([get_unique_opt(o) for o in [self.wep1, self.wep2]], []) + \
            self.artefacts.get_selected()


#deprecated units

class Lord(Unit):
    name = 'Chaos Lord'
    base_points = 65

    def __init__(self):
        Unit.__init__(self)
        self.arm = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Terminator armour', 40, 'tda']
        ])
        self.pwep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + melee + ranged + artefact_weapon)
        self.pwep2 = self.opt_one_of('', [['Close combat weapon', 0, 'ccw']] + melee + ranged + artefact_weapon)
        self.twep1 = self.opt_one_of('Weapon', [['Combi-bolter', 0, 'cbgun']] + tda_ranged + artefact_weapon)
        self.twep2 = self.opt_one_of('', [['Power weapon', 0, 'pwep']] + tda_melee + artefact_weapon)
        self.mark = self.opt_options_list('Mark of Chaos', marks, 1)
        self.opt = self.opt_options_list('Options', special_gear + reward +
                                         artefact_option + [['Veterans of the Long War', 5, 'lwvet']])
        self.ride = self.opt_options_list('Movement options', ride, 1)

    def get_artifacts(self):
        result = []
        alist = [item[2] for item in artefact_weapon]
        result = reduce(lambda val, it: val + [it] if self.pwep1.get_cur() == it else val, alist, result)
        result = reduce(lambda val, it: val + [it] if self.pwep2.get_cur() == it else val, alist, result)
        result = reduce(lambda val, it: val + [it] if self.twep1.get_cur() == it else val, alist, result)
        result = reduce(lambda val, it: val + [it] if self.twep2.get_cur() == it else val, alist, result)
        alist = [item[2] for item in artefact_option]
        result = reduce(lambda val, it: val + [it] if self.opt.get(it) else val, alist, result)
        return result

    def check_rules(self):
        #check options that depend on armour
        tda = self.arm.get_cur() == 'tda'
        self.twep1.set_visible(tda)
        self.twep2.set_visible(tda)
        self.pwep1.set_visible(not tda)
        self.pwep2.set_visible(not tda)
        self.ride.set_visible(not tda)

        # make sure only 1 weapon from Artefact Weapons or Ranged is taken
        if not tda:
            self.gear = ['Frag grenades', 'Krak grenades']
            rlist = [item[2] for item in ranged]
            flag1 = reduce(lambda val, it: val or self.pwep1.get_cur() == it, rlist, False)
            flag2 = reduce(lambda val, it: val or self.pwep2.get_cur() == it, rlist, False)
            self.pwep1.set_active_options(rlist, not flag2)
            self.pwep2.set_active_options(rlist, not flag1)
            alist = [item[2] for item in artefact_weapon]
            flag1 = reduce(lambda val, it: val or self.pwep1.get_cur() == it, alist, False)
            flag2 = reduce(lambda val, it: val or self.pwep2.get_cur() == it, alist, False)
            self.pwep1.set_active_options(alist, not flag2)
            self.pwep2.set_active_options(alist, not flag1)
        else:
            self.gear = []
            alist = [item[2] for item in artefact_weapon]
            flag1 = reduce(lambda val, it: val or self.twep1.get_cur() == it, alist, False)
            flag2 = reduce(lambda val, it: val or self.twep2.get_cur() == it, alist, False)
            self.twep1.set_active_options(alist, not flag2)
            self.twep2.set_active_options(alist, not flag1)

        #check options that depend on mark
        self.opt.set_visible_options(['blgr'], self.mark.get('ng'))
        self.opt.set_visible_options(['msc'], self.mark.get('tz'))

        self.pwep1.set_active_options(['blind'], self.mark.get('kh'))
        self.pwep2.set_active_options(['blind'], self.mark.get('kh'))
        self.twep1.set_active_options(['blind'], self.mark.get('kh'))
        self.twep2.set_active_options(['blind'], self.mark.get('kh'))

        self.ride.set_visible_options(['disc'], self.mark.get('tz'))
        self.ride.set_visible_options(['pal'], self.mark.get('ng'))
        self.ride.set_visible_options(['steed'], self.mark.get('sl'))
        self.ride.set_visible_options(['jug'], self.mark.get('kh'))
        Unit.check_rules(self)


class Sorc(Unit):
    name = 'Sorcerer'
    base_points = 60

    def __init__(self):
        Unit.__init__(self)
        self.arm = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Terminator armour', 25, 'tda']
        ])
        self.pwep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + melee + ranged + artefact_weapon)
        self.pwep2 = self.opt_one_of('', [['Force weapon', 0, 'fw']] + melee + ranged + artefact_weapon)
        self.twep1 = self.opt_one_of('Weapon', [['Combi-bolter', 0, 'cbgun']] + tda_ranged + artefact_weapon)
        self.twep2 = self.opt_one_of('', [['Force weapon', 0, 'fw']] + tda_melee + artefact_weapon)
        self.mark = self.opt_options_list('Mark of Chaos', marks[1:], 1)
        #psykers cannot into khorne
        self.opt = self.opt_options_list('Options', special_gear + reward + artefact_option +
                                         [['Veterans of the Long War', 5, 'lwvet']])
        self.ride = self.opt_options_list('Movement options', ride, 1)
        self.lev = self.opt_options_list('Psyker', [
            ['Mastery Level 2', 25],
            ['Mastery Level 3', 50],
        ], limit=1)

    def get_artifacts(self):
        result = []
        alist = [item[2] for item in artefact_weapon]
        result = reduce(lambda val, it: val + [it] if self.pwep1.get_cur() == it else val, alist, result)
        result = reduce(lambda val, it: val + [it] if self.pwep2.get_cur() == it else val, alist, result)
        result = reduce(lambda val, it: val + [it] if self.twep1.get_cur() == it else val, alist, result)
        result = reduce(lambda val, it: val + [it] if self.twep2.get_cur() == it else val, alist, result)
        alist = [item[2] for item in artefact_option]
        result = reduce(lambda val, it: val + [it] if self.opt.get(it) else val, alist, result)
        return result

    def check_rules(self):
        #check options that depend on armour
        tda = self.arm.get_cur() == 'pwr'
        self.twep1.set_visible(not tda)
        self.twep2.set_visible(not tda)
        self.pwep1.set_visible(tda)
        self.pwep2.set_visible(tda)
        self.ride.set_visible(tda)
        #check options that depend on mark
        self.opt.set_visible_options(['blgr'], self.mark.get('ng'))
        self.ride.set_visible_options(['pal'], self.mark.get('ng'))
        self.ride.set_visible_options(['steed'], self.mark.get('sl'))

        self.ride.set_visible_options(['disc'], self.mark.get('tz'))
        self.opt.set_visible_options(['msc'], self.mark.get('tz'))
        # make sure only 1 weapon from Artefact Weapons or Ranged is taken
        if tda:
            self.gear = ['Frag grenades', 'Krak grenades']
            rlist = [item[2] for item in ranged]
            flag1 = reduce(lambda val, it: val or self.pwep1.get_cur() == it, rlist, False)
            flag2 = reduce(lambda val, it: val or self.pwep2.get_cur() == it, rlist, False)
            self.pwep1.set_active_options(rlist, not flag2)
            self.pwep2.set_active_options(rlist, not flag1)
            alist = [item[2] for item in artefact_weapon]
            flag1 = reduce(lambda val, it: val or self.pwep1.get_cur() == it, alist, False)
            flag2 = reduce(lambda val, it: val or self.pwep2.get_cur() == it, alist, False)
            self.pwep1.set_active_options(alist, not flag2)
            self.pwep2.set_active_options(alist, not flag1)
        else:
            self.gear = []
            alist = [item[2] for item in artefact_weapon]
            flag1 = reduce(lambda val, it: val or self.twep1.get_cur() == it, alist, False)
            flag2 = reduce(lambda val, it: val or self.twep2.get_cur() == it, alist, False)
            self.twep1.set_active_options(alist, not flag2)
            self.twep2.set_active_options(alist, not flag1)
        self.ride.set_visible_options(['jug'], False)
        self.pwep1.set_active_options(['blind'], False)
        self.pwep2.set_active_options(['blind'], False)
        self.twep1.set_active_options(['blind'], False)
        self.twep2.set_active_options(['blind'], False)
        Unit.check_rules(self)
        if len(self.lev.get_all()) == 0:
            self.description.add('Mastery Level 1')


class DaemonPrince(Unit):
    name = "Daemon Prince"
    base_points = 145
    gear = ['Veterans of the Long War']

    def __init__(self):
        Unit.__init__(self)
        self.god = self.opt_one_of('', [
            ['Daemon of Khorne', 15, 'kh'],
            ['Daemon of Tzeentch', 15, 'tz'],
            ['Daemon of Nurgle', 15, 'ng'],
            ['Daemon of Slaanesh', 10, 'sl']
        ])
        self.wep = self.opt_one_of('Weapon', [['Close combat weapon', 0, 'ccw']] + artefact_weapon)
        self.opt = self.opt_options_list('Options', [
            ['Wings', 40, 'wng'],
            ['Power armour', 20, 'pwr']
        ] + reward + artefact_option)
        self.lev = self.opt_count('Mastery Level', 0, 3, 25)

    def get_artifacts(self):
        result = []
        alist = [item[2] for item in artefact_weapon]
        result = reduce(lambda val, it: val + [it] if self.wep.get_cur() == it else val, alist, result)
        alist = [item[2] for item in artefact_option]
        result = reduce(lambda val, it: val + [it] if self.opt.get(it) else val, alist, result)
        return result

    def check_rules(self):
        self.wep.set_active_options(['blind'], self.god.get_cur() == 'kh')
        self.opt.set_visible_options(['msc'], self.god.get_cur() == 'tz')
        self.lev.set_active(not self.god.get_cur() == 'kh')
        Unit.check_rules(self)


class Apostle(Unit):
    name = "Dark Apostle"
    base_points = 105
    gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Sigil of Corruption']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + ranged + artefact_weapon)
        self.wep2 = self.opt_one_of('Weapon', [['Power maul', 0, 'pml']] + ranged + artefact_weapon)
        self.mark = self.opt_options_list('Mark of Chaos', marks, 1)
        self.opt = self.opt_options_list('Options', reward + artefact_option +
                                         [['Veterans of the Long War', 0, 'lwvet']])

    def get_artifacts(self):
        result = []
        alist = [item[2] for item in artefact_weapon]
        result = reduce(lambda val, it: val + [it] if self.wep1.get_cur() == it else val, alist, result)
        result = reduce(lambda val, it: val + [it] if self.wep2.get_cur() == it else val, alist, result)
        alist = [item[2] for item in artefact_option]
        result = reduce(lambda val, it: val + [it] if self.opt.get(it) else val, alist, result)
        return result

    def check_rules(self):
        rlist = [item[2] for item in ranged]
        flag1 = reduce(lambda val, it: val or self.wep1.get_cur() == it, rlist, False)
        flag2 = reduce(lambda val, it: val or self.wep2.get_cur() == it, rlist, False)
        self.wep1.set_active_options(rlist, not flag2)
        self.wep2.set_active_options(rlist, not flag1)
        alist = [item[2] for item in artefact_weapon]
        flag1 = reduce(lambda val, it: val or self.wep1.get_cur() == it, alist, False)
        flag2 = reduce(lambda val, it: val or self.wep2.get_cur() == it, alist, False)
        self.wep1.set_active_options(alist, not flag2)
        self.wep2.set_active_options(alist, not flag1)
        self.wep1.set_active_options(['blind'], self.mark.get('kh'))
        self.wep2.set_active_options(['blind'], self.mark.get('kh'))
        self.opt.set_visible_options(['msc'], self.mark.get('tz'))
        Unit.check_rules(self)


class Warpsmith(Unit):
    name = "Warpsmith"
    base_points = 110
    gear = ['Fleshmetal', 'Frag grenades', 'Krak grenades', 'Mechatendrils']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + ranged + artefact_weapon)
        self.wep2 = self.opt_one_of('', [['Power axe', 0, 'paxe']] + ranged + artefact_weapon)
        self.mark = self.opt_options_list('Mark of Chaos', marks, 1)
        self.opt = self.opt_options_list('Options', reward + artefact_option +
                                         [['Veterans of the Long War', 5, 'lwvet']])

    def get_artifacts(self):
        result = []
        alist = [item[2] for item in artefact_weapon]
        result = reduce(lambda val, it: val + [it] if self.wep1.get_cur() == it else val, alist, result)
        result = reduce(lambda val, it: val + [it] if self.wep2.get_cur() == it else val, alist, result)
        alist = [item[2] for item in artefact_option]
        result = reduce(lambda val, it: val + [it] if self.opt.get(it) else val, alist, result)
        return result

    def check_rules(self):
        rlist = [item[2] for item in ranged]
        flag1 = reduce(lambda val, it: val or self.wep1.get_cur() == it, rlist, False)
        flag2 = reduce(lambda val, it: val or self.wep2.get_cur() == it, rlist, False)
        self.wep1.set_active_options(rlist, not flag2)
        self.wep2.set_active_options(rlist, not flag1)
        alist = [item[2] for item in artefact_weapon]
        flag1 = reduce(lambda val, it: val or self.wep1.get_cur() == it, alist, False)
        flag2 = reduce(lambda val, it: val or self.wep2.get_cur() == it, alist, False)
        self.wep1.set_active_options(alist, not flag2)
        self.wep2.set_active_options(alist, not flag1)
        self.wep1.set_active_options(['blind'], self.mark.get('kh'))
        self.wep2.set_active_options(['blind'], self.mark.get('kh'))
        self.opt.set_visible_options(['msc'], self.mark.get('tz'))
        Unit.check_rules(self)
