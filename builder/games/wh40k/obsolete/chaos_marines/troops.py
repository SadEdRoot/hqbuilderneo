__author__ = 'Ivan Truskov'

from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.core.options import norm_counts
from builder.games.wh40k.obsolete.chaos_marines.armory import tank_upgrades, melee, ranged


class Rhino(Unit):
    name = "Chaos Rhino"
    base_points = 35
    gear = ['Combi-bolter', 'Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', tank_upgrades)

    def check_rules(self):
        self.opt.set_active_options(['cflame'], not self.opt.get('cmelta') and not self.opt.get('cplasma'))
        self.opt.set_active_options(['cplasma'], not self.opt.get('cmelta') and not self.opt.get('cflame'))
        self.opt.set_active_options(['cmelta'], not self.opt.get('cplasma') and not self.opt.get('cflame'))
        Unit.check_rules(self)


class VeteranOfLongWar():
    base_points = 0
    option = 'Veterans of the Long War'

    def __init__(self, price=1):
        self.gear = []
        self.veteran = False
        self.price = price

    def set_veteran(self, val):
        self.veteran = val
        if self.option in self.gear:
            self.gear.remove(self.option)
        if self.veteran:
            self.gear.append(self.option)
            self.base_points += self.price


class MarkedUnit():
    base_points = 0
    mark_names = {
        'kh': 'Mark of Khorne',
        'tz': 'Mark of Tzeentch',
        'ng': 'Mark of Nurgle',
        'sl': 'Mark of Slaanesh'
    }

    def __init__(self, kh, tz, ng, sl):
        self.gear = []
        self.mark = None
        self.mark_points = {'kh': kh, 'tz': tz, 'ng': ng, 'sl': sl}

    def set_mark(self, mark=None):
        for name_mark in self.mark_names.values():
            if name_mark in self.gear:
                self.gear.remove(name_mark)
        self.mark = mark
        self.update_gear()

    def get_mark(self):
        return self.mark

    def update_gear(self):
        if self.mark is None:
            return
        self.gear.append(self.mark_names[self.mark])
        self.base_points += self.mark_points[self.mark]


class IconBearer(Unit):
    def __init__(self, kh=0, tz=0, ng=0, sl=0, vengeance=0, wrath=0, flame=0, despair=0, excess=0):
        Unit.__init__(self)
        #MarkedUnit.__init__(self, kh, tz, ng, sl)
        #self.icon_list = [
        #    ['Icon of vengeance', vengeance, 'veng'],
        #    ['Icon of wrath', wrath, 'wrath'],
        #    ['Icon of flame', flame, 'flame'],
        #    ['Icon of despair', despair, 'despair'],
        #    ['Icon of excess', excess, 'excess'],
        #]
        #self.icon_ids = map(lambda v: v[2], self.icon_list)
        self.icons = self.opt_options_list('Chaos Icon', [
            ['Icon of vengeance', vengeance, 'veng'],
            ['Icon of wrath', wrath, 'wrath'],
            ['Icon of flame', flame, 'flame'],
            ['Icon of despair', despair, 'despair'],
            ['Icon of excess', excess, 'excess'],
        ], limit=1)

    @staticmethod
    def get_mark_id(mark):
        all_ids = mark.get_all()
        return all_ids[0] if len(all_ids) == 1 else None

    def set_icon(self, mark):
        mark_id = self.get_mark_id(mark)
    #def update_gear(self):
    #    #set allowed icon
        self.icons.set_visible_options(['wrath'], mark_id == 'kh')
        self.icons.set_visible_options(['flame'], mark_id == 'tz')
        self.icons.set_visible_options(['despair'], mark_id == 'ng')
        self.icons.set_visible_options(['excess'], mark_id == 'sl')
        self.icons.update_limit()
        IconBearer.check_rules(self)
        return self.get_count() if len(self.icons.get_all()) > 0 else 0
    #    MarkedUnit.update_gear(self)
    #
    #def has_icon(self):
    #    return self.get_count() if len(self.icons.get_all()) > 0 else 0
    #
    #def enable_icon(self, val):
    #    if len(self.icons.get_all()) > 0 and val is False:
    #        return
    #    self.icons.set_active_options(self.icon_ids, val)


class AspiringChampion(IconBearer):
    name = 'Aspiring Champion'

    def __init__(self):
        IconBearer.__init__(self, wrath=20, flame=15, despair=10, excess=30, vengeance=20)
        self.opt = self.opt_options_list('Options', [
            ['Melta bombs', 5, 'mbomb'],
            ['Gift of mutation', 10, 'gift']
        ])
        self.wep1 = self.opt_one_of('Weapon', [['Close combat weapon', 0, 'ccw']] + melee + ranged)
        self.wep2 = self.opt_one_of('', [['Bolt pistol', 0, 'bpist']] + melee + ranged)
        self.wep3 = self.opt_one_of('', [['Boltgun', 0, 'bgun']] + ranged)
        self.ranged_ids = [item[2] for item in ranged]
        self.gear = ['Power armour', 'Frag grenades', 'Krak grenades']
        self.base_points = 75 - 13 * 4

    def check_rules(self):
        #any weapon can be switched for ranged, but oly single ranged weapon can be taken
        ranged_flag1 = self.wep1.get_cur() in self.ranged_ids
        ranged_flag2 = self.wep2.get_cur() in self.ranged_ids
        ranged_flag3 = self.wep3.get_cur() in self.ranged_ids
        self.wep1.set_active_options(self.ranged_ids, not(ranged_flag2 or ranged_flag3))
        self.wep2.set_active_options(self.ranged_ids, not(ranged_flag1 or ranged_flag3))
        self.wep3.set_active_options(self.ranged_ids, not(ranged_flag2 or ranged_flag1))
        IconBearer.check_rules(self)


class ChaosSpaceMarines(Unit):
    name = 'Chaos Space Marines'

    class SpaceMarine(IconBearer):
        name = 'Chaos Space Marine'
        spec_list = ['flame', 'mgun', 'pgun']
        heavy_list = ['acan', 'mlaunch', 'lcannon']
        base_points = 13

        def __init__(self):
            IconBearer.__init__(self, wrath=20, flame=15, despair=10, excess=30, vengeance=25)
            self.count = self.opt_count(self.name, 1, 19, self.base_points)
            self.wep = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bgun'],
                ['Close combat weapon', 0, 'ccw'],
                ['Flamer', 5, 'flame'],
                ['Meltagun', 10, 'mgun'],
                ['Plasma gun', 15, 'pgun'],
                ['Autocannon', 10, 'acan'],
                ['Missile launcher', 15, 'mlaunch'],
                ['Lascannon', 20, 'lcannon']
            ])
            self.side = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bpist'],
                ['Plasma pistol', 15, 'ppist']
            ])
            self.opt = self.opt_options_list('', [
                ['Close combat weapon', 2, 'ccw']
            ])
            self.spec_allowed = True
            self.heavy_allowed = True
            self.gear = ['Power armour', 'Frag grenades', 'Krak grenades']

        def has_ppist(self):
            return self.get_count() if self.side.get_cur() == 'ppist' else 0

        def has_hvy1(self):
            return self.get_count() if self.wep.get_cur() in self.spec_list else 0

        def has_hvy2(self):
            return self.get_count() if self.wep.get_cur() in self.heavy_list else 0

        def enable_ppist(self, val):
            if self.side.get_cur() == 'ppist' and val is False:
                return
            self.wep.set_active_options(['ppist'], val)

        def enable_spec(self, val):
            if self.wep.get_cur() in self.spec_list and val is False:
                return
            self.spec_allowed = val
            self.wep.set_active_options(self.spec_list, val)

        def enable_heavy(self, val):
            if self.wep.get_cur() in self.heavy_list and val is False:
                return
            self.heavy_allowed = val
            self.wep.set_active_options(self.heavy_list, val)

        def check_rules(self):
            #check whether the option to take additional ccw for 2 points or switch boltgun for it for free is chosen
            self.wep.set_active_options(['ccw'], not self.opt.get('ccw'))
            self.opt.set_active(not self.wep.get_cur() == 'ccw')

            #if plasma pistol is taken, boltgun cannot be exchanged for heavy weapons
            self.side.set_active_options(['ppist'], not (self.has_hvy1() and self.has_hvy2()))
            self.wep.set_active_options(self.heavy_list, not self.side.get_cur() == 'ppist' and self.heavy_allowed)
            self.wep.set_active_options(self.spec_list, not self.side.get_cur() == 'ppist' and self.spec_allowed)
            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id])

    def __init__(self, black_legion=False):
        Unit.__init__(self)
        self.black_legion = black_legion
        self.ldr = self.opt_sub_unit(AspiringChampion())
        self.warriors = self.opt_units_list(self.SpaceMarine.name, self.SpaceMarine, 4, 19)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 2, 'tz'],
            ['Mark of Nurgle', 3, 'ng'],
            ['Mark of Slaanesh', 2, 'sl']
        ], limit=1)
        if not self.black_legion:
            self.opt = self.opt_options_list('Options', [['Veterans of the Long War', 1, 'lwvet']])
        self.transport = self.opt_optional_sub_unit('Transport', Rhino())

    def check_rules(self):
        self.warriors.update_range()
        icon = sum([u.set_icon(self.marks) for u in [self.ldr.get_unit()] + self.warriors.get_units()])
        if icon > 1:
            self.error("Chaos Space Marines can't take more then 1 icon (taken: {0}).".format(icon))

        plas = 0
        hvy1 = 0
        hvy2 = 0
        for unit in self.warriors.get_units():
            plas += unit.has_ppist()
            hvy1 += unit.has_hvy1()
            hvy2 += unit.has_hvy2()

        speclim = 2 if self.get_count() >= 10 else 1
        if plas > 1:
            self.error("Chaos Space Marines can't have more then 1 plasma pistol among chaos space marines.")
        if speclim < 2 and hvy2 > 0:
            self.error("Chaos Space Marines can't take autocannon, missile launcher or lascannon in units of "
                       "less then 10 models")
        elif hvy2 > 1:
            self.error("Chaos Space Marines can't have more then 1 autocannon, missile launcher or lascannon among "
                       "chaos space marines (taken: {0}).".format(hvy2))
        if plas + hvy1 + hvy2 > speclim:
            self.error("Chaos Space Marines can't take more then 1 special weapon in an unit of 9 or less models, or 2 "
                       "in units with 10 or more models (taken: {0}).".format(plas + hvy1 + hvy2))
        for unit in self.warriors.get_units():
            unit.enable_heavy(self.get_count() >= 10)

        self.points.set(self.build_points(count=1, options=[self.ldr, self.warriors, self.transport]) +
                        ((self.opt.points() if not self.black_legion else 1) + self.marks.points()) * self.get_count())
        self.build_description(count=1)
        if self.black_legion:
            self.description.add('Veterans of the Long War')

    def get_count(self):
        return self.warriors.get_count() + 1


class ChaosCultists(Unit):
    name = "Chaos Cultists"
    base_gear = ['Improvised armour', 'Close combat weapon']
    base_points = 4

    class Champ(Unit):
        name = 'Cultist Champion'
        base_points = 50 - 4 * 9

        def __init__(self):
            Unit.__init__(self)
            self.gear = ['Autopistol', 'Close combat weapon', 'Improvised armour']
            self.opt = self.opt_options_list('Weapon', [['Shotgun', 2, 'sgun']])

    def __init__(self):
        Unit.__init__(self)
        self.ldr = self.opt_sub_unit(self.Champ())
        self.count = self.opt_count('Chaos cultist', 9, 34, self.base_points)
        self.wep = self.opt_count('Autogun', 0, 9, 1)
        self.stubb = self.opt_count('Heavy stubber', 0, 1, 5)
        self.flame = self.opt_count('Flamer', 0, 1, 5)
        self.marks = self.opt_options_list('Marks', [
            ['Mark of Khorne', 2, 'kh'],
            ['Mark of Tzeentch', 1, 'tz'],
            ['Mark of Nurgle', 2, 'ng'],
            ['Mark of Slaanesh', 1, 'sl']
        ], limit=1)

    def get_mark_id(self):
        return self.marks.get_all()[0] if len(self.marks.get_all()) == 1 else None

    def check_rules(self):
        heavy_count = self.stubb.get() + self.flame.get()

        norm_counts(0, (self.count.get() + 1) / 10, [self.stubb, self.flame])
        self.wep.update_range(0, self.count.get() - heavy_count)
        self.set_points(self.count.points() + self.wep.points() + self.stubb.points() + self.flame.points() +
                        self.marks.points() * self.get_count() + self.ldr.points())
        self.build_description(options=[self.ldr, self.marks], count=1)

        cult = ModelDescriptor(
            name='Chaos Cultist',
            points=self.base_points,
            gear=self.base_gear
        )
        self.description.add(cult.clone().add_gear('Autopistol').build(self.count.get() - self.wep.get() - heavy_count))
        self.description.add(cult.clone().add_gear('Autogun', 1).build(self.wep.get()))
        self.description.add(cult.clone().add_gear('Heavy stubber', 5).build(self.stubb.get()))
        self.description.add(cult.clone().add_gear('Flamer', 5).build(self.flame.get()))

    def get_count(self):
        return self.count.get() + 1


class PlagueZombie(Unit):
    name = "Plague Zombies"
    base_points = 4
    champ_points = 50 - 9 * base_points
    gear = ['Improvised armour', 'Close combat weapon']

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Plague zombie', 9, 34, self.base_points)
        self.champ_desc = ModelDescriptor(name='Plague zombie champion', gear=self.gear, points=self.champ_points)
        self.zombie_desc = ModelDescriptor(name='Plague zombie', gear=self.gear, points=self.base_points)

    def get_count(self):
        return self.count.get() + 1

    def check_rules(self):
        self.set_points(self.count.points() + self.champ_points)
        self.description.reset()
        self.description.set_header(self.name, self.count.points() + self.champ_points)
        self.description.add(self.champ_desc.build(1))
        self.description.add(self.zombie_desc.build(self.count.get()))
