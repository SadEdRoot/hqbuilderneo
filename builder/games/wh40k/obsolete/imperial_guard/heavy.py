__author__ = 'Denis Romanow'

from builder.core.unit import Unit, OptionsList
from builder.games.wh40k.obsolete.imperial_guard.fast import VehicleSquad, Vehicle


class Lemans(VehicleSquad):
    name = 'Leman Russ Squadron'

    class Leman(Vehicle):
        name = 'Leman Russ'
        guns = dict(
            bt='Battle cannon',
            ext='Exterminator autocannon',
            vn='Vanquisher battle cannon',
            er='Eradicator nova cannon',
            dem='Demolisher siege cannon',
            pn='Punisher gatling cannon',
            exc='Executioner cannon'
        )
        sponsons = dict(
            hb='Heavy bolter',
            hf='Heavy flamer',
            mm='Multimelta',
            pc='Plasma cannon',
        )
        gear = ['Searchlight', 'Smoke launchers']

        def __init__(self):
            Vehicle.__init__(self)

            self.type = self.opt_one_of('Type', [
                ['Leman Russ Battle Tank', 150, 'bt'],
                ['Leman Russ Exterminator', 150, 'ext'],
                ['Leman Russ Vanquisher', 155, 'vn'],
                ['Leman Russ Eradicator', 160, 'er'],
                ['Leman Russ Demolisher', 165, 'dem'],
                ['Leman Russ Punisher', 180, 'pn'],
                ['Leman Russ Executioner', 190, 'exc'],
            ])

            self.wep1 = self.opt_one_of('Weapon', [
                ['Heavy bolter', 0],
                ['Heavy flamer', 0],
                ['Lascannon', 15],
            ])

            self.wep2 = self.opt_options_list('Sponsons', [
                ['Heavy bolters', 20, 'hb'],
                ['Heavy flamers', 20, 'hf'],
                ['Multimeltas', 30, 'mm'],
                ['Plasma cannons', 40, 'pc'],
            ], limit=1)

            self.wep3 = self.opt_options_list('Options', [
                ['Pintle-mounted storm bolter', 10],
                ['Pintle-mounted heavy stubber', 10],
            ], limit=1)

            self.opt = self.opt_options_list('', [
                ['Dozen blade', 10],
                ['Extra armor', 15],
                ['Hunter-killer missile', 10],
            ])

            self.pask = self.opt_options_list('', [
                ['Knight Commander Pask', 50, 'pask'],
            ])

        def check_rules(self):
            single_points = self.build_points(exclude=[self.count.id], count=1) + (self.add_opts.points() if
                                                                                   self.add_opts else 0)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id, self.type.id, self.wep2.id],
                                   gear=[self.guns[self.type.get_cur()]] + self.gear, name=self.type.get_selected())
            if self.add_opts:
                self.description.add(self.add_opts.get_selected())
            if len(self.wep2.get_all()):
                self.description.add(self.sponsons[self.wep2.get_all()[0]], 2)

        def get_pask(self):
            return self.get_count() if self.pask.get('pask') else 0

    def __init__(self):
        VehicleSquad.__init__(self)
        opts = OptionsList('opts', 'Options', [
            ['Camo netting', 20, 'cn'],
        ])
        self.units = self.opt_units_list('', self.Leman, 1, 3)
        self.append_opt(opts)
        self.opt = opts

    def check_rules(self):
        if sum((u.get_pask() for u in self.units.get_units())) > 1:
            self.error('Knight Commander Pask is not unique')
        VehicleSquad.check_rules(self)

    def get_unique(self):
        if sum((u.get_pask() for u in self.units.get_units())) > 0:
            return 'Knight Commander Pask'


class Ordnance(VehicleSquad):
    name = 'Ordnance Battery'

    class Ordnance(Vehicle):
        name = 'Ordnance'
        guns = dict(
            bs='Earthshaker cannon',
            md='Medusa siege cannon',
            cl='Colossus siege mortar',
            gr='Griffon heavy mortar',
        )
        gear = ['Searchlight', 'Smoke launchers']

        def __init__(self):
            Vehicle.__init__(self)

            self.type = self.opt_one_of('Type', [
                ['Basilisk', 125, 'bs'],
                ['Medusa', 135, 'md'],
                ['Colossus', 140, 'cl'],
                ['Griffon', 75, 'gr'],
            ])
            self.wep1 = self.opt_one_of('Weapon', [
                ['Heavy bolter', 0],
                ['Heavy flamer', 0],
            ])

            self.wep3 = self.opt_options_list('Options', [
                ['Pintle-mounted storm bolter', 10],
                ['Pintle-mounted heavy stubber', 10],
            ], limit=1)

            self.opt = self.opt_options_list('', [
                ['Enclosed crew compartment', 15],
                ['Dozen blade', 10],
                ['Extra armor', 15],
                ['Hunter-killer missile', 10],
            ])
            self.med = self.opt_options_list('', [
                ['Bastion-breacher shells', 5],
            ])

        def check_rules(self):
            self.med.set_visible(self.type.get_cur() == 'md')
            single_points = self.build_points(exclude=[self.count.id], count=1) + (self.add_opts.points() if
                                                                                   self.add_opts else 0)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id, self.type.id],
                                   gear=[self.guns[self.type.get_cur()]] + self.gear, name=self.type.get_selected())
            if self.add_opts:
                self.description.add(self.add_opts.get_selected())

    def __init__(self):
        VehicleSquad.__init__(self)
        opts = OptionsList('opts', 'Options', [
            ['Camo netting', 20, 'cn'],
        ])
        self.units = self.opt_units_list('', self.Ordnance, 1, 3)
        self.append_opt(opts)
        self.opt = opts


class Hydra(VehicleSquad):
    name = 'Hydra Flak Tank Battery'

    class Hydra(Vehicle):
        name = 'Hydra Flak Tank'
        gear = ['Twin-linked hydra autocannon', 'Twin-linked hydra autocannon', 'Searchlight', 'Smoke launchers',
                'Auto-targeting system']
        base_points = 75

        def __init__(self):
            Vehicle.__init__(self)

            self.wep1 = self.opt_one_of('Weapon', [
                ['Heavy bolter', 0],
                ['Heavy flamer', 0],
            ])

            self.wep3 = self.opt_options_list('Options', [
                ['Pintle-mounted storm bolter', 10],
                ['Pintle-mounted heavy stubber', 10],
            ], limit=1)

            self.opt = self.opt_options_list('', [
                ['Dozen blade', 10],
                ['Extra armor', 15],
                ['Hunter-killer missile', 10],
            ])

    def __init__(self):
        VehicleSquad.__init__(self)
        opts = OptionsList('opts', 'Options', [
            ['Camo netting', 20, 'cn'],
        ])
        self.units = self.opt_units_list('', self.Hydra, 1, 3)
        self.append_opt(opts)
        self.opt = opts


class Manticore(Unit):
    name = 'Manticore Rocket Launcher'
    base_points = 160
    gear = ['Storm eagle rockets', 'Searchlight', 'Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Heavy bolter', 0],
            ['Heavy flamer', 0],
        ])

        self.wep3 = self.opt_options_list('Options', [
            ['Pintle-mounted storm bolter', 10],
            ['Pintle-mounted heavy stubber', 10],
        ], limit=1)

        self.opt = self.opt_options_list('', [
            ['Dozen blade', 10],
            ['Hunter-killer missile', 10],
            ['Extra armor', 15],
            ['Camo netting', 30, 'cn'],
        ])


class Deathstrike(Unit):
    name = 'Deathstrike Rocket Launcher'
    base_points = 160
    gear = ['Deathstrike missile', 'Searchlight', 'Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Heavy bolter', 0],
            ['Heavy flamer', 0],
        ])

        self.wep3 = self.opt_options_list('Options', [
            ['Pintle-mounted storm bolter', 10],
            ['Pintle-mounted heavy stubber', 10],
        ], limit=1)

        self.opt = self.opt_options_list('', [
            ['Dozen blade', 10],
            ['Hunter-killer missile', 10],
            ['Extra armor', 15],
            ['Camo netting', 30, 'cn'],
        ])



    # class Retributors(Unit):
#     name = 'Retributor Squad'
#     class Retributor(ListSubUnit):
#         max = 9
#         name = 'Retributor'
#         base_points = 12
#         gear = ['Power armour','Frag grenades','Krak grenades','Bolt pistol']
#         def __init__(self):
#             Unit.__init__(self)
#             self.hlist = [
#                 ['Heavy bolter',5,'hbgun'],
#                 ['Multi-melta', 10, 'mmgun' ],
#                 ['Heavy flamer', 20, 'hflame' ]
#             ]
#
#             self.wep = self.opt_one_of('Weapon', [['Boltgun',0,'bgun']] + self.hlist)
#             self.flag = self.opt_options_list('Options',[['Simulacrum imperialis',20,'simp']])
#             self.have_flag = False
#             self.have_hvy = False
#         def has_hvy(self):
#             return self.wep.get_cur() != 'bgun'
#         def check_rules(self):
#             if self.wep.get_cur() == 'bgun':
#                 self.wep.set_active_options([w[2] for w in self.hlist], not self.have_hvy)
#             if not self.flag.get('simp'):
#                 self.flag.set_active_options(self.flag.get_all_ids(), not self.have_flag)
#             Unit.check_rules(self)
#     class Superior(Unit):
#         name = 'Retributor Superior'
#         gear = ['Power armour','Frag grenades','Krak grenades']
#         base_points = 65 - 12 * 4
#         def __init__(self):
#             Unit.__init__(self)
#             weaponlist = [
#                 ['Chainsword',0,'chsw'],
#                 ['Storm Bolter',3,'sbgun'],
#                 ['Power sword',10,'psw'],
#                 ['Combi-melta', 10, 'cmelta' ],
#                 ['Combi-flamer', 10, 'cflame' ],
#                 ['Combi-plasma', 10, 'cplasma' ],
#                 ['Condemnor boltgun',15,'cbgun'],
#                 ['Plasma pistol',15,'ppist']
#             ]
#             self.wep1 = self.opt_one_of('Weapon', [['Boltgun',0,'bgun']] + weaponlist)
#             self.wep2 = self.opt_one_of('Weapon',[['Bolt pistol',0,'bpist']] + weaponlist)
#             self.opt = self.opt_options_list('Options',[
#                 ['Melta bombs',5,'mbomb']
#             ])
#     def __init__(self):
#         Unit.__init__(self)
#         self.squad = self.opt_units_list(self.Retributor.name,self.Retributor,4,9)
#         self.sup = self.opt_sub_unit(self.Superior())
#         self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')
#     def check_rules(self):
#         self.squad.update_range()
#         flag = reduce(lambda val, c: val + (1 if c.flag.get('simp') else 0), self.squad.get_units(), 0)
#         if flag > 1:
#             self.error('Only one Simulacrum imperialis can be carried by squad of Dominions.')
#         hvy = reduce(lambda val, c: val + (1 if c.has_hvy() else 0), self.squad.get_units(), 0)
#         if hvy > 4:
#             self.error('No more then 4 Retributors can carry heavy weapons.')
#         for c in self.squad.get_units():
#             c.have_flag = (flag > 0)
#             self.have_hvy = hvy >= 4
#             c.check_rules()
#         self.points.set(self.build_points(count=1))
#         self.build_description(count=1)
#     def get_count(self):
#         return self.squad.get_count() + 1
#
# class Exorcist(Unit):
#     name = "Exorcist"
#     base_points = 135
#     gear = ['Exorcist launcher','Smoke launchers']
#     def __init__(self):
#         Unit.__init__(self)
#         self.opt = self.opt_options_list('Options',[
#                 ['Searchlight',1,'light'],
#                 ['Dozer blade', 5, 'dblade'],
#                 ['Storm bolter', 10, 'sbgun'],
#                 ['Hunter-killer missile', 10, 'hkm'],
#                 ['Extra armour', 15, 'exarm']
#         ])
#
# class PEngine(ListSubUnit):
#     max = 3
#     name = 'Penitent Engine'
#     base_points = 85
#     gear = ['Two Dreadnought close combat weapons (with built in heavy flamers)']
