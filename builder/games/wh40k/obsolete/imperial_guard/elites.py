__author__ = 'Denis Romanow'

from builder.core.unit import Unit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.imperial_guard.troops import Chimera


class OgrynSquad(Unit):
    name = 'Ogryn Squad'
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Ogryns', 2, 9, 40)
        self.transport = self.opt_optional_sub_unit('Transport', Chimera())
        self.base_gear = ['Flak armour', 'Ripper gun', 'Frag grenades']

    def get_count(self):
        return self.count.get() + 1

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Ogryn Bone \'ead', count=1, gear=self.base_gear, points=50).build())
        self.description.add(ModelDescriptor('Ogryn', count=self.count.get(), gear=self.base_gear, points=40).build())
        self.description.add(self.transport.get_selected())


class PsykerSquad(Unit):
    name = 'Psyker Battle Squad'
    base_points = 20

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Sanctioned Psykers', 4, 9, 10)
        self.transport = self.opt_optional_sub_unit('Transport', Chimera())
        self.base_gear = ['Flak armour', 'Laspistol', 'Closed combat weapon ']

    def get_count(self):
        return self.count.get() + 1

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Overseer', count=1, gear=self.base_gear, points=20).build())
        self.description.add(ModelDescriptor('Sanctioned Psyker', count=self.count.get(), gear=self.base_gear,
                                             points=10).build())
        self.description.add(self.transport.get_selected())


class RatlingSquad(Unit):
    name = 'Ratling Squad'

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Ratlings', 3, 10, 10)
        self.base_gear = ['Flak armour', 'Sniper rifle', 'Laspistol']

    def check_rules(self):
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor('Ratling', count=self.count.get(), gear=self.base_gear, points=10).build())


class GuardsmanMarbo(Unit):
    static = True
    unique = True
    base_points = 65
    name = 'Guardsman Marbo'
    gear = ['Flak armour', 'Ripper pistol', 'Envenomed blade', 'Frag grenades', 'Melta bombs', 'Demolition charge']


class StormTroopers(Unit):
    name = 'Storm Troopers Squad'

    class Commander(Unit):
        name = 'Storm Trooper Sergeant'
        base_points = 21
        gear = ['Carapace armour', 'Close combat weapon', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            wep = [
                ['Bolt pistol', 0],
                ['Boltgun', 0],
                ['Power weapon', 10],
                ['Plasma pistol', 10],
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Hot-shot laspistol', 0]] + wep)
            self.wep2 = self.opt_one_of('', [['Hot-shot lasgun', 0]] + wep)

    def __init__(self):
        Unit.__init__(self)
        self.commander = self.opt_sub_unit(self.Commander())
        self.count = self.opt_count('Storm Trooper', 4, 9, 16)
        self.spec = [
            self.opt_count('Flamer', 0, 2, 5),
            self.opt_count('Grenade launcher', 0, 2, 5),
            self.opt_count('Melta gun', 0, 2, 10),
            self.opt_count('Plasma gun', 0, 2, 15)
        ]
        self.transport = self.opt_optional_sub_unit('Transport', Chimera())
        self.base_gear = ['Carapace armour', 'Hot-shot laspistol', 'Close combat weapon', 'Frag grenades',
                          'Krak grenades']

    def get_count(self):
        return self.count.get() + 1

    def check_rules(self):
        norm_counts(0, 2, self.spec)
        self.set_points(self.build_points(count=1))
        self.build_description(options=[self.commander], count=1)
        st = ModelDescriptor(name='Storm Trooper', points=16, gear=self.base_gear)
        count = self.count.get()
        for o in self.spec:
            if o.get() > 0:
                self.description.add(st.clone().add_gear(o.name, count=1, points=o.points() / o.get()).build(o.get()))
                count -= o.get()
        self.description.add(st.clone().add_gear('Hot-shot lasgun').build(count))
        self.description.add(self.transport.get_selected())
