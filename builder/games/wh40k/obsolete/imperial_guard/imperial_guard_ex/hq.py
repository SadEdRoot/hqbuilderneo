__author__ = 'Ivan Truskov'
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.imperial_guard.troops import Chimera


class Valkyrie(Unit):
    name = 'Valkyrie'
    base_points = 100
    gear = ['Searchlight', 'Extra armour']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Multi-laser', 0],
            ['Lascannon', 15]
        ])
        self.wep2 = self.opt_one_of('', [
            ['Hellstrike missiles', 0, 'mis'],
            ['Multiple rocket pods', 30, 'rp']
        ])
        self.wep3 = self.opt_options_list('', [
            ['Pair of sponsons with heavy bolters', 10, 'hb']
        ])

    def check_rules(self):
        single_points = self.build_points(count=1)
        self.points.set(single_points * self.get_count())
        self.build_description(points=single_points, count=1, exclude=[self.wep2.id, self.wep3.id])
        if self.wep2.get_cur() == 'mis':
            self.description.add('Hellstrike missile', 2)
        elif self.wep2.get_cur() == 'rp':
            self.description.add('Multiple rocket pod', 2)
        if self.wep3.get('hb'):
            self.description.add('Heavy bolter', 2)


class GeneralOdon(Unit):
    name = 'General Myndoras Odon'
    base_points = 115
    unique = True

    def __init__(self):
        Unit.__init__(self)
        self.advisors = self.opt_options_list('Regimental advisors', [
            ['Astropath', 30, 'ap'],
            ['Master of ordnance', 30, 'moo'],
            ['Officer of the Fleer', 30, 'of'],
        ])
        self.bodyguards = self.opt_count('Bodyguards', 0, 2, 15)
        self.transport = self.opt_optional_sub_unit('Transport', [Chimera(), Valkyrie()])

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor('General Myndoras Odon',
                                             gear=['Flak armour', 'Laspistol', 'Frag grenades', 'Krak grenades',
                                                   'Refractor field', 'Powerfist', 'Close combat weapon']).build(1))
        guard = ModelDescriptor('Veteran Guardsman',
                                gear=['Flak armour', 'Frag grenades', 'Krak grenades', 'Close combat weapon'])
        self.description.add(guard.clone().add_gear('Meltagun').build(1))
        self.description.add(guard.add_gear('Lasgun').build(1))
        self.description.add(guard.clone().add_gear('Vox-caster').build(1))
        self.description.add(guard.clone().add_gear('Regimantal Banner').build(1))
        adv_gear = ['Frag grenades', 'Close combat weapon', 'Laspistol', 'Flak armour', 'Krak grenades']
        if self.advisors.get('ap'):
            self.description.add(ModelDescriptor('Astropath', points=30, gear=adv_gear).build(1))
        if self.advisors.get('moo'):
            self.description.add(ModelDescriptor('Master of ordnance', points=30, gear=adv_gear).build(1))
        if self.advisors.get('of'):
            self.description.add(ModelDescriptor('Officer of the Fleer', points=30, gear=adv_gear).build(1))
        if self.bodyguards.get() > 0:
            self.description.add(ModelDescriptor('Bodyguard', points=15, gear=adv_gear).build(self.bodyguards.get()))
        self.description.add(self.transport.get_selected())
