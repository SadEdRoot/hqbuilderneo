__author__ = 'Denis Romanov'

from builder.core2 import UnitType

from builder.games.wh40k.roster import Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, Fort, LordsOfWarSection,\
    Detachment, Wh40kImperial

from builder.games.wh40k.escalation.space_marines\
    import LordsOfWar as MarinesLordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour import volume2
from builder.games.wh40k.imperial_armour.volume11\
    import space_wolves as volume11
from builder.games.wh40k.imperial_armour.aeronautika.space_marines import HyperiosBattery
from builder.games.wh40k.dataslates.cypher import Cypher
from builder.games.wh40k.dataslates.command_tanks import SpaceMarineCommandTanks
from .hq import WolfLord, Ragnar, Harald, Canis, RunePriest, WolfPriest,\
    Njal, Ulrik, BattleLeader, Bjorn
from .elites import IronPriest, Servitors, WolfScouts, Dreadnought,\
    Murderfang, WolfGuards, WolfGuardTerminators, Arjac, LoneWolf
from .troops import BloodClaws, Lucas, GreyHunters
from .fast import DropPod, Rhino, Razorback, Stormwolf, Swiftclaws,\
    Skyclaws, WolfPack, ThunderwolfCavalry, LandSpeederSquadron
from .heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer,\
    Stormfang, Whirlwind, Vindicator, Predator, LongFangs
from .lords import Logan


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        UnitType(self, WolfLord)
        UnitType(self, Ragnar)
        UnitType(self, Harald)
        UnitType(self, Canis)
        self.rune = UnitType(self, RunePriest)
        UnitType(self, Njal)
        UnitType(self, WolfPriest)
        UnitType(self, Ulrik)
        UnitType(self, BattleLeader)
        UnitType(self, Bjorn)
        UnitType(self, SpaceMarineCommandTanks)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.priest = UnitType(self, IronPriest)
        self.servs = UnitType(self, Servitors)
        UnitType(self, WolfScouts)
        self.lonewolves = UnitType(self, LoneWolf, slot=0)
        UnitType(self, Dreadnought)
        UnitType(self, Murderfang)
        self.guards = UnitType(self, WolfGuards)
        self.terminators = UnitType(self, WolfGuardTerminators)
        self.champion = UnitType(self, Arjac)

    def check_limits(self):
        self.servs.slot = 1 if (self.priest.count == 0) else 0
        self.champion.slot = 1 if (self.guards.count + self.terminators.count == 0) else 0
        return super(BaseElites, self).check_limits()


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.bcl = UnitType(self, BloodClaws)
        self.lucas = UnitType(self, Lucas)
        self.hunters = UnitType(self, GreyHunters)

    def check_limits(self):
        self.lucas.slot = 1 if (self.bcl.count == 0) else 0
        return super(Troops, self).check_limits()


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, DropPod)
        UnitType(self, Rhino)
        UnitType(self, Razorback)
        UnitType(self, Stormwolf)
        UnitType(self, Swiftclaws)
        UnitType(self, Skyclaws)
        UnitType(self, WolfPack)
        UnitType(self, ThunderwolfCavalry)
        UnitType(self, LandSpeederSquadron)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, LandRaider)
        UnitType(self, LandRaiderCrusader)
        UnitType(self, LandRaiderRedeemer)
        UnitType(self, Stormfang)
        UnitType(self, Vindicator)
        UnitType(self, Whirlwind)
        UnitType(self, Predator)
        UnitType(self, LongFangs)


class BaseLords(LordsOfWarSection):
    def __init__(self, parent):
        super(BaseLords, self).__init__(parent)
        UnitType(self, Logan)


class HQ(volume2.SpaceMarineHQ, volume11.IA11HQSection, BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.cypher = UnitType(self, Cypher, slot=0)


class FastAttack(volume2.SpaceMarineFast, BaseFastAttack):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, HyperiosBattery)


class Elites(volume2.SpaceWolvesElites, BaseElites):
    pass


class HeavySupport(volume2.SpaceMarineHeavySupport, BaseHeavySupport):
    pass


class Lords(volume2.SpaceMarineLordsOfWar, Titans, MarinesLordsOfWar, BaseLords):
    pass


class SpaceWolvesV3Base(volume2.SpaceMarinesRelicRoster):
    # army_id = 'space_wolves_v3'
    # army_name = 'Space Wolves'

    def is_champion(self):
        return False

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(SpaceWolvesV3Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            lords=Lords(self), fort=Fort(parent=self)
        )

    def check_rules(self):
        super(SpaceWolvesV3Base, self).check_rules()
        banners = sum(u.has_banner() for u in self.troops.hunters.units)
        if banners > 1:
            self.error("Only one Wolf Standard may be taken per Detachment")
        lone_limit = sum(o.count for o in self.troops.types + [
            self.elites.guards, self.elites.terminators
        ])
        if self.elites.lonewolves.count > lone_limit:
            self.error('Number of Lone Wolves must be lower then number of troops and Wolf Guard units')

    def has_keeper(self):
        return self.hq.rune.count > 0


class GreatWolfCompany(SpaceWolvesV3Base,
                       volume2.SpaceMarinesRelicRoster):
    army_id = 'space_wolves_v3_great_company'
    army_name = 'Company of the Great Wolf Detachment'

    def is_champion(self):
        return True

    def __init__(self):
        self.hq = HQ(parent=self)
        self.hq.cypher.visible = False
        self.hq.min, self.hq.max = (1, 4)
        self.elites = Elites(parent=self)
        self.elites.min, self.elites.max = (2, 8)
        self.troops = Troops(parent=self)
        self.troops.min, self.troops.max = (0, 3)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        # note: we skip SpaceWolvesV3Base constructor here
        super(SpaceWolvesV3Base, self).__init__(
            sections=[self.hq, self.elites, self.troops, self.fast,
                      self.heavy, Fort(parent=self), BaseLords(self)]
        )


class ChampionFormation(volume2.SpaceMarinesLegaciesFormation):

    '''
    @note None of these formations have grey claws or lone wolves,
    so we can safely ignore checking for them
    '''

    def is_champion(self):
        return True


class Stormforce(ChampionFormation):
    army_id = 'space_wolves_v3_stormforce'
    army_name = 'Kingsguard Stormforce'

    class NoTransportGuard(WolfGuardTerminators):
        transport_allowed = False

    def __init__(self):
        super(Stormforce, self).__init__()
        self.logan = UnitType(self, Logan, min_limit=1, max_limit=1)
        self.terms = UnitType(self, self.NoTransportGuard, min_limit=1, max_limit=1)
        self.raiders = [
            UnitType(self, LandRaider),
            UnitType(self, LandRaiderRedeemer),
            UnitType(self, LandRaiderCrusader)
        ]
        self.add_type_restriction(self.raiders, 1, 1)
        UnitType(self, Stormfang, min_limit=1, max_limit=1)

    def check_rules(self):
        super(Stormforce, self).check_rules()
        for log in self.logan.units:
            if not log.ride.any:
                self.error("Logan Grimnar must be embarked on his pimp ride")


class Brethen(ChampionFormation):
    army_id = 'space_wolves_v3_brethen'
    army_name = 'Brethen of the Fell-Handed'

    def __init__(self):
        super(Brethen, self).__init__()
        UnitType(self, Bjorn, min_limit=1, max_limit=1)
        self.dreds = UnitType(self, Dreadnought, min_limit=2, max_limit=2)

    def check_rules(self):
        super(Brethen, self).check_rules()
        for dred in self.dreds.units:
            if not dred.opt.venerable.value:
                self.error("Dreadnought must be upgraded to Venerable")


class Voidclaws(ChampionFormation):
    army_id = 'space_wolves_v3_voidclaws'
    army_name = 'Wolf Guard Void Claws'

    def __init__(self):
        super(Voidclaws, self).__init__()
        self.terms = UnitType(self, Stormforce.NoTransportGuard, min_limit=1, max_limit=1)


    def check_rules(self):
        super(Voidclaws, self).check_rules()
        for term in self.terms.units:
            if not term.all_claws():
                self.error("All models in the Wolf Guard Terminators unit must be equipped with pair of claws")


class Council(ChampionFormation):
    army_id = 'space_wolves_v3_council'
    army_name = 'Grimnar\'s War Council'

    def __init__(self):
        super(Council, self).__init__()
        UnitType(self, Ulrik, min_limit=1, max_limit=1)
        UnitType(self, Njal, min_limit=1, max_limit=1)
        UnitType(self, RunePriest, min_limit=1, max_limit=1)
        UnitType(self, IronPriest, min_limit=1, max_limit=1)


class Shieldbrothers(ChampionFormation):
    army_id = 'space_wolves_v3_shieldbrothers'
    army_name = 'Arjac\'s Shieldbrothers'

    def __init__(self):
        super(Shieldbrothers, self).__init__()
        UnitType(self, Arjac, min_limit=1, max_limit=1)
        UnitType(self, Stormforce.NoTransportGuard, min_limit=1, max_limit=1)
        UnitType(self, LandRaiderCrusader, min_limit=1, max_limit=1)


class Thunderstrike(ChampionFormation):
    army_id = 'space_wolves_v3_thunderstrike'
    army_name = 'Wolf Guard Thunderstrike'

    class DropPodGuard(WolfGuards):
        transport_allowed = False

        def __init__(self, parent):
            super(Thunderstrike.DropPodGuard, self).__init__(parent)
            # drop pod cannot carry jet packers, correct?
            self.ride.used = self.ride.visible = False

    def __init__(self):
        super(Thunderstrike, self).__init__()
        UnitType(self, self.DropPodGuard, min_limit=1, max_limit=1)
        UnitType(self, DropPod, min_limit=1, max_limit=1)
        UnitType(self, Stormforce.NoTransportGuard, min_limit=1, max_limit=1)


class Champions(ChampionFormation):
    army_id = 'space_wolves_v3_champions'
    army_name = 'The Champions of Fenris'

    def __init__(self):
        super(Champions, self).__init__()
        Detachment.build_detach(self, Stormforce, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Brethen, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Voidclaws, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Council, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Shieldbrothers, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Thunderstrike, min_limit=1, max_limit=1)


class SpaceWolvesV3CAD(SpaceWolvesV3Base, CombinedArmsDetachment):
    army_id = 'space_wolves_v3_cad'
    army_name = 'Space Wolves (Combined arms detachment)'


class SpaceWolvesV3AD(SpaceWolvesV3Base, AlliedDetachment):
    army_id = 'space_wolves_v3_ad'
    army_name = 'Space Wolves (Allied detachment)'

faction = 'Space_wolves'


class SpaceWolvesV3(Wh40k7ed, Wh40kImperial):
    army_id = 'space_wolves_v3'
    army_name = 'Space Wolves'
    faction = faction

    def __init__(self):
        super(SpaceWolvesV3, self).__init__([
            SpaceWolvesV3CAD, GreatWolfCompany,
            Stormforce, Brethen, Voidclaws, Council,
            Shieldbrothers, Thunderstrike, Champions
        ], [])

detachments = [
    SpaceWolvesV3CAD,
    SpaceWolvesV3AD,
    GreatWolfCompany,
    Stormforce,
    Brethen,
    Voidclaws,
    Council,
    Shieldbrothers,
    Thunderstrike,
    Champions
]
