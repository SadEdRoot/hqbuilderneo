
__author__ = 'Ivan Truskov'
from builder.core.unit import Unit
from builder.core.options import norm_points
from builder.core.model_descriptor import ModelDescriptor
from builder.games.wh40k.obsolete.chaos_daemons.troops import CountUnit, BaseDaemon


class Hounds(CountUnit):
    name = "Flesh Hounds of Khorne"
    gear = ['Collar of Khorne']

    def __init__(self):
        CountUnit.__init__(self, 5, 20, 16)

    def check_rules(self):
        CountUnit.check_rules(self)
        self.description.add(ModelDescriptor('Flesh Hound of Khorne', self.get_count(), 16, self.gear).build())


class Screamers(CountUnit):
    name = "Screamers of Tzeentch"

    def __init__(self):
        CountUnit.__init__(self, 3, 9, 25)


class Drones(Unit):
    name = "Plague Drones of Nurgle"
    gear = ['Plaguesword']
    base_points = 42

    class Plaguebringer(Unit):
        name = "Plaguebringer"
        base_points = 42 + 5
        gear = ['Plaguesword']

        def __init__(self):
            Unit.__init__(self)
            self.les = self.opt_count("Lesser Rewards", 0, 2, 10)
            self.grt = self.opt_count("Greater Rewards", 0, 1, 20)

        def check_rules(self):
            norm_points(20, [self.les, self.grt])
            self.points.set(self.build_points(options=[self.les, self.grt, self.dh, self.sting]))
            self.build_description(options=[self.les, self.grt, self.dh, self.sting])

        def add_opt(self, dh, sting):
            self.dh = dh
            self.sting = sting

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Plague Drones', 3, 9, self.base_points)
        bringer = self.Plaguebringer()
        self.leader = self.opt_optional_sub_unit(self.Plaguebringer.name, bringer)
        self.opt = self.opt_options_list('Options', [
            ['Chaos Icon', 15, 'chic'],
            ['Plague banner', 25, 'ban'],
            ['Instrument of Chaos', 10, 'ich']
        ])
        self.dh = self.opt_options_list('', [['Death\'s heads', 5, 'dh']])
        self.sting = self.opt_options_list('', [['Rot proboscis', 5, 'rp'], ['Venom sting', 5, 'vs']], 1)
        bringer.add_opt(self.dh, self.sting)

    def check_rules(self):
        self.opt.set_active_options(['ban'], self.opt.get('chic'))
        ldr = self.leader.get_count()
        self.warriors.update_range(3 - ldr, 9 - ldr)
        self.set_points((self.base_points + self.dh.points() + self.sting.points()) * self.warriors.get() +
                        self.leader.points() + self.opt.points())
        self.build_description(options=[self.leader], gear=[])
        base_daemon = ModelDescriptor(name='Plague Drone', gear=self.gear, points=self.base_points)
        base_daemon.add_gear_opt(self.dh)
        base_daemon.add_gear_opt(self.sting)
        generic = self.warriors.get()
        if self.opt.get('chic'):
            if self.opt.get('ban'):
                self.description.add(base_daemon.clone().add_gear('Plague banner', points=40).build(1))
            else:
                self.description.add(base_daemon.clone().add_gear('Chaos Icon', points=15).build(1))
            generic -= 1
        if self.opt.get('ich'):
            self.description.add(base_daemon.clone().add_gear('Instrument of Chaos', points=10).build(1))
            generic -= 1
        self.description.add(base_daemon.build(generic))

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Fury(Unit):
    name = "Chaos Furies"
    base_points = 35
    god_list = {'kh':'Khorne', 'tz':'Tzeentch', 'ng':'Nurgle', 'sl':'Slaanesh'}

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Chaos Fury', 5, 20, 6)
        self.opt = self.opt_options_list('Upgrade to', [
            ['Daemons of Khorne', 2, 'kh'],
            ['Daemons of Tzeentch', 1, 'tz'],
            ['Daemons of nurgle', 2, 'ng'],
            ['Daemons of Slaanesh', 2, 'sl']
        ], 1)

    def check_rules(self):
        self.set_points(self.base_points + (self.count.get() - 5) * 6 + self.opt.points() * self.count.get())
        name = self.name
        if len(self.opt.get_all()):
            name += ' of ' + self.god_list[self.opt.get_all()[0]]
        self.build_description(exclude=[self.count.id, self.opt.id])
        self.description.add(ModelDescriptor(name, self.count.get(), 6 + self.opt.points()).build())

    def get_count(self):
        return self.count.get()


class Seekers(BaseDaemon):
    name = "Seekers of Slaanesh"

    def __init__(self):
        BaseDaemon.__init__(
            self,
            unit_name=self.name,
            base_name='Seeker of Slaanesh',
            leader_name='Heartseeker',
            base_gear=[],
            base_points=12,
            banner_name='Rapturous standard',
            min_count=5,
            max_count=20
        )


class Hellflayer(Unit):
    name = "Hellflayer of Slaanesh"
    gear = ["Exalted Alluress", "Hellflayer Chariot"]
    static = True
    base_points = 60
