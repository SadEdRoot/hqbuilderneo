__author__ = 'Ivan Truskov'
from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.core.options import norm_points


class CountUnit(Unit):
    name = None

    def __init__(self, min_count, max_count, base_points):
        self.base_points = base_points
        Unit.__init__(self)
        self.warriors = self.opt_count(self.name, min_count, max_count, self.base_points)

    def check_rules(self):
        self.set_points(self.warriors.points())
        self.build_description(exclude=[self.warriors.id], gear=[])

    def get_count(self):
        return self.warriors.get()


class BaseDaemon(Unit):
    class Leader(Unit):
        def __init__(self, leader_name, base_gear, base_points):
            self.name = leader_name
            self.base_points = base_points + 5
            self.gear = base_gear
            Unit.__init__(self)
            self.les = self.opt_count("Lesser Rewards", 0, 2, 10)
            self.grt = self.opt_count("Greater Rewards", 0, 1, 20)

        def check_rules(self):
            norm_points(20, [self.les, self.grt])
            Unit.check_rules(self)

    def __init__(self, unit_name, base_name, leader_name, base_gear, base_points, banner_name, min_count=10,
                 max_count=20, icon_available=True):
        self.name = unit_name
        self.base_gear = base_gear
        self.base_points = base_points
        self.model_name = base_name
        self.min_count = min_count
        self.max_count = max_count
        self.icon_available = icon_available
        self.banner_name = banner_name
        Unit.__init__(self)
        self.warriors = self.opt_count(base_name, min_count, max_count, self.base_points)
        self.leader = self.opt_optional_sub_unit(leader_name, self.Leader(leader_name, base_gear, base_points))
        if self.icon_available:
            self.opt = self.opt_options_list('Options', [
                ['Chaos Icon', 10, 'chic'],
                [banner_name, 10, 'ban'],
                ['Instrument of Chaos', 10, 'ich']
            ])

    def check_rules(self):
        if self.icon_available:
            self.opt.set_active_options(['ban'], self.opt.get('chic'))
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min_count - ldr, self.max_count - ldr)
        self.set_points(self.build_points(count=1, base_points=0))
        self.build_description(options=[self.leader])
        base_daemon = ModelDescriptor(name=self.model_name, gear=self.base_gear, points=self.base_points)
        generic = self.warriors.get()

        if self.icon_available:
            if self.opt.get('chic'):
                if self.opt.get('ban'):
                    self.description.add(base_daemon.clone().add_gear(self.banner_name, points=20).build(1))
                else:
                    self.description.add(base_daemon.clone().add_gear('Chaos Icon', points=10).build(1))
                generic -= 1
            if self.opt.get('ich'):
                self.description.add(base_daemon.clone().add_gear('Instrument of Chaos', points=10).build(1))
                generic -= 1
        self.description.add(base_daemon.build(generic))

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Letters(BaseDaemon):
    name = "Bloodletters of Khorne"

    def __init__(self):
        BaseDaemon.__init__(
            self,
            unit_name=self.name,
            base_name='Bloodletter',
            leader_name='Bloodreaper',
            base_gear=['Hellblade'],
            base_points=10,
            banner_name='Banner of blood'
        )


class Horrors(BaseDaemon):
    name = "Pink Horrors of Tzeentch"

    def __init__(self):
        BaseDaemon.__init__(
            self,
            unit_name=self.name,
            base_name='Pink Horror',
            leader_name='Iridescent Horror',
            base_gear=[],
            base_points=9,
            banner_name='Blasrted standard'
        )


class Bearers(BaseDaemon):
    name = "Plaguebearers of Nurgle"

    def __init__(self):
        BaseDaemon.__init__(
            self,
            unit_name=self.name,
            base_name='Plaguebearer',
            leader_name='Plagueridden',
            base_gear=['Plaguesword'],
            base_points=9,
            banner_name='Plague banner'
        )


class Daemonettes(BaseDaemon):
    name = "Daemonettes of Slaanesh"

    def __init__(self):
        BaseDaemon.__init__(
            self,
            unit_name=self.name,
            base_name='Daemonette',
            leader_name='Alluress',
            base_gear=[],
            base_points=9,
            banner_name='Rapturous standard'
        )


class Nurglings(CountUnit):
    name = "Nurglings"

    def __init__(self):
        CountUnit.__init__(self, 3, 9, 15)
