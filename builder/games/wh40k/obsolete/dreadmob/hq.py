__author__ = 'Ivan Truskov'

from builder.core.unit import Unit
from .elites import Junka


class BigMek(Unit):
    name = 'Big Mek'
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.ccw = self.opt_one_of('Close combat weapon', [
            ['Choppa', 0, 'choppa'], 
            ['Burna', 20, 'br'], 
            ['Power klaw', 25, 'klaw'], 
        ])
        self.rng = self.opt_one_of('Ranged weapon', [
            ["Slugga", 0, 'slugga'], 
            ["Shoota", 0, 'shoota'], 
            ['Twin-linked shoota', 5, 'tlsht'], 
            ["Shoota/Rokkit", 5, 'sr'], 
            ["Shoota/Skorcha", 5, 'ss'], 
            ["Kustom mega-blasta", 15, 'kmb'], 
            ["Mega armour", 40, 'mega'], 
            ["Shokk attack gun", 60, 'sag'], 
            ["Kustom force field", 50, 'kkf'], 
        ])
        self.opt = self.opt_options_list('Options', [
            ["Ammo runt", 3, 'runt'], 
            ["Attack squig", 15, 'squig'], 
            ["Cybork body", 10, 'cyborg'], 
            ["'eavy armour", 5, 'armour'], 
            ["Bosspole", 5, 'pole'], 
        ], 1)
        self.oilers = self.opt_count("Grot oilers", 0, 3, 5)
        self.ride = self.opt_optional_sub_unit('Transport', [Junka()])

    def check_rules(self):
        self.gear = ["Mek's tools"]
        if self.rng.get_cur() == 'mega':
            self.gear += ['Power klaw', 'Twin-linked Shoota']
        Unit.check_rules(self)


class PainBoss(Unit):
    name = 'Pain Boss'
    base_points = 50
    gear = ['\'Urty Siringe', 'Doc\'s Tools']

    def __init__(self):
        Unit.__init__(self)
        self.ccw = self.opt_one_of('Weapon', [
            ['Slugga', 0, 'slg'], 
            ['Big Choppa', 5, 'bchp'], 
            ['Power klaw', 25, 'klaw'], 
        ])
        self.opt = self.opt_options_list('Options', [
            ["Attack squig", 15, 'squig'], 
            ["Cybork body", 10, 'cyborg'], 
            ["'eavy armour", 5, 'armour'], 
            ["Bosspole", 5, 'pole'], 
        ])
        self.orderlies = self.opt_count("Grot orderlies", 0, 3, 5)


class KustomMekaDread(Unit):
    name = 'Kustom Meka-Dread'
    base_points = 180
    gear = ['Fixin\' Klaws', 'Armour plates', 'Grot riggers', 'Rippa klaw']

    def __init__(self):
        Unit.__init__(self)
        self.kust = self.opt_one_of('Kustom job', [
            ['Mega charga', 15, 'mch'], 
            ['Rokkit-Bom Racks', 35, 'rbr'], 
            ['Kustom Force Field', 75, 'kff']
        ])
        self.wep = self.opt_one_of('Weapon', [
            ['Rippa Klaw', 0, 'rk'], 
            ['Big Zzappa', 15, 'bz'], 
            ['Shunta', 25, 'sh'], 
            ['Rattler Kannon', 10, 'rk']
        ])
