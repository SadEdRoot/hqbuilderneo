__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor
from .troops import Trukk


class BurnaBoyz(Unit):
    name = 'Burna Boyz'

    class Mek(ListSubUnit):
        name = 'Mek'
        base_points = 15
        gear = ["Mek's Tools"]

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Kustom mega-blasta", 0, 'kmb'], 
                ["Slugga and Choppa", 0, 'sc'], 
                ["Big Shoota", 0, 'bs'], 
                ["Rokkit launcha", 5, 'rl'], 
            ])
            self.opt = self.opt_options_list('', [['Grot Oiler', 5]])

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Burna Boyz", 5, 15, 15)
        self.meks = self.opt_options_list("", [['Meks', 15, 'add']])
        self.meks_unit = self.opt_units_list('Meks Unit', self.Mek, 0, 3)
        self.ride = self.opt_optional_sub_unit('Transport', [Trukk()])

    def check_rules(self):
        have_meks = self.meks.get('add')
        self.meks_unit.set_visible(have_meks)
        self.meks_unit.set_active(have_meks)
        meks = 0
        if have_meks:
            self.meks_unit.update_range(1, 3)
            meks = self.meks_unit.get_count()
        self.ride.set_active(self.get_count() <= 12)
        norm_counts(5 - meks, 15 - meks, [self.count])
        self.set_points(self.build_points(count=1, exclude=[self.meks.id]))
        self.build_description(options=[self.meks_unit], count=1)
        self.description.add(ModelDescriptor('Burna Boyz', points=15, count=self.count.get(), gear=['Burna'])
            .build())

    def get_count(self):
        return self.count.get() + self.meks_unit.get_count()


class SlashaMob(Unit):
    name = 'Cybork Slasha Mob'

    class Painboy(Unit):
        name = 'Painboy'
        base_points = 50
        gear = ['\'Urty Siringe', 'Doc\'s Tools', 'Cybork body']
        has_sb = 0
        has_ha = 0

        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options', [['Bosspole', 5, 'bp']])
            self.help = self.opt_count('Grot Orderly', 0, 2, 5)

        def set_external(self, opt):
            options = [self.opt, self.help, opt]
            self.set_points(self.build_points(options=options))
            self.build_description(options=options)

        def check_rules(self):
            pass

    class Cybork(ListSubUnit):
        name = 'Cybork Slasha'
        base_points = 30
        gear = ['Cybork body']

        def __init__(self):
            ListSubUnit.__init__(self, min_models=4, max_models=9)
            self.ccw = self.opt_one_of('Close combat weapon', [
                ['Choppa', 0, 'chp'], 
                ['Big choppa', 5, 'bc'], 
                ['Power Klaw', 25, 'pk']
            ])
            self.rng = self.opt_one_of('Ranged weapon', [
                ['Slugga', 0, 'slg'], 
                ['Twin-linked Shoota', 5, 'tlsh'], 
                ['Shoota/rokkit', 5, 'shr'], 
                ['Shoota/scorcha', 5, 'shsc']
            ])

        def set_external(self, opt):
            options = [self.ccw, self.rng, opt]
            single_points = self.build_points(options=options, count=1)
            self.points.set(single_points * self.get_count())
            self.build_description(points=single_points, count=1, options=options)

        def has_spec(self):
            return self.get_count() if self.rng.get_cur() != 'slg' else 0

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.units = self.opt_units_list(self.Cybork.name, self.Cybork, 4, 9)
        self.boss = self.opt_sub_unit(self.Painboy())
        self.opt = self.opt_options_list('Options', [
            ['Stikkbombs', 1, 'sb'], 
            ['\'Eavy Armour', 5, 'ha']
        ])

    def get_count(self):
        return 1 + self.units.get_count()

    def check_rules(self):
        self.units.update_range()
        for unit in [self.boss.get_unit()] + self.units.get_units():
            unit.set_external(self.opt)
        spec_cnt = sum((u.has_spec() for u in self.units.get_units()))
        if spec_cnt > 2:
            self.error('{0} cannot take more then 2 special shooting weapons (taken {1})'.format(self.name, spec_cnt))
        self.set_points(self.build_points(count=1, exclude=[self.opt.id]))
        self.build_description(count=1, exclude=[self.opt.id])


class Junka(Unit):
    name = "Mekboy Junka"
    base_points = 65
    gear = ['Grot riggers', 'Turbo-charga']

    def __init__(self):
        Unit.__init__(self)
        self.dev = self.opt_one_of('Structure', [
            ["Reinforced Ram", 0, 'rr'], 
            ["Deff Rolla", 0, 'dr'], 
            ["Wreckin\' Ball", 0, 'wb'], 
            ['Grabbin\' Klaw', 0, 'gk']
        ])
        self.opt = self.opt_options_list('Options', [
            ["'ard case", 10], 
            ["Red paint job", 5], 
            ["Stikkbomb chukka", 5]
        ])
        self.wep = [self.opt_one_of('Weapon' if not i else '', [
            ['Big shoota', 0, 'bs'], 
            ['Scorcha', 0, 'sc'], 
            ['Rokkit launcha', 5, 'rl'], 
            ['Twin big shoota', 10, 'tbs'], 
            ['Twin rokkit launcha', 15, 'trl'], 
            ['Kustom mega blasta', 15, 'kmb']
        ]) for i in range(3)]
        self.spec = self.opt_options_list("Speshul Gear", [
            ['Turret-mounted supa-scorcha', 20, 'tmss'], 
            ['Turret-mounted big zzappa', 30, 'tmbz'], 
            ['Kustom force field generator', 75, 'kffg'], 
            ['Turret-mounted shokk attack gun', 100, 'tmsag']
        ], 1)
        self.bomms = self.opt_count('Grot bomm', 0, 2, 15)

    def check_rules(self):
        self.bomms.set_active(len(self.spec.get_all()) < 1)
        self.spec.set_active(self.bomms.get() < 1)
        Unit.check_rules(self)
