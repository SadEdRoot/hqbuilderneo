from builder.core2 import Gear, OneOf, UnitList, ListSubUnit, OptionsList,\
    SubUnit
from builder.games.wh40k.unit import Unit

class CustodianSquad(Unit):
    type_name = 'Custodian Guard Squad'
    type_id = 'custodians_v1'

    class Custodian(ListSubUnit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(CustodianSquad.Custodian.Weapon, self).__init__(parent, 'Weapon')
                self.spear = self.variant('Guardian spaear')
                self.variant('Sentinel blade')
                self.vexilla = self.variant('Custodes Vexilla', 30)

        class Shield(OptionsList):
            def __init__(self, parent):
                super(CustodianSquad.Custodian.Shield, self).__init__(parent, '')
                self.variant('Storm shield', 10)

        def __init__(self, parent):
            super(CustodianSquad.Custodian, self).__init__(parent, 'Custodian',
                                                           50, [Gear('Power knife')])
            self.wep = self.Weapon(self)
            self.shield = self.Shield(self)

        def check_rules(self):
            super(CustodianSquad.Custodian, self).check_rules()
            self.shield.visible = self.shield.used = self.wep.cur != self.wep.spear

        @ListSubUnit.count_unique
        def get_standard(self):
            return self.wep.description if self.wep.cur == self.wep.vexilla else []

    class ShieldCaptain(Unit):
        def __init__(self, parent):
            super(CustodianSquad.ShieldCaptain, self).__init__(parent, 'Shield-Captain',
                                                               60, [Gear('Power knife')])
            self.wep = CustodianSquad.Custodian.Weapon(self)
            self.wep.vexilla.visible = False
            self.shield = CustodianSquad.Custodian.Shield(self)

        def check_rules(self):
            super(CustodianSquad.ShieldCaptain, self).check_rules()
            self.shield.visible = self.shield.used = self.wep.cur != self.wep.spear

    def __init__(self, parent):
        super(CustodianSquad, self).__init__(parent)
        self.models = UnitList(self, self.Custodian, 4, 9)
        SubUnit(self, self.ShieldCaptain(parent=None))

    def get_count(self):
        return 1 + self.models.count

    def get_unique_gear(self):
        return sum((c.get_standard() for c in self.models.units), [])
    
