from builder.core.options import norm_counts
from builder.core.unit import Unit
from functools import reduce

__author__ = 'Ivan Truskov'

BWeapon = [
    [['Burst cannon',8,'bc'],['Twin-linked burst cannon',12,'tlbc']],
    [['Flamer',4,'flame'],['Twin-linked flamer',6,'tlflame']],
    [['Fusion blaster',12,'fblast'],['Twin-linked fusion blaster',18,'tlfblast']],
    [['Missile Pod',12,'mpod'],['Twin-linked missile Pod',18,'tlmpod']],
    [['Plasma rifle',20,'pgun'],['Twin-linked plasma rifle',30,'tlpgun']]
]


BWeaponSpec = [
    ['Airbursting fragmentation projector', 20, 'afp'],
    ['Cyclic ion blaster', 15, 'cib']
]

BSupport = [
    ['Advanced stabilisation system',10,'ass'],
    ['Blacksun filter',3,'bs'],
    ['Multi-tracker',5,'mtr'],
    ['Shield generator',20,'shgen'],
    ['Target lock',5,'tl'],
    ['Targeting array',10,'tarr'],
    ['Drone controller',0,'dc']
]
BSupportSpec = [
    ['Vectored retro-thrusters',10,'vrt'],
    ['Command & control node',10,'c&c'],
]
BWargear = [
    ['Bonding khife',5,'bk'],
    ['Hard-wired blacksun filter',3,'bs'],
    ['Hard-wired drone controller',0,'dc'],
    ['Hard-wired multi-tracker',5,'mtr'],
    ['Hard-wired target lock',5,'tl'],
 ]
Drones = [
     ('Gun drone',10),
     ('Marker drone',30),
     ('Shield drone',15)
]
BWargearSpec = [
    ['Ejection system',15,'es'],
    ['Failsafe detonator',15,'fd'],
    ['Iridium armour',20,'ia'],
    ['Stimulant injector',10,'sinj'],
]
Vehicle = [
    ['Sensor spines',10,'ssp'],
    ['Targeting array',5,'tarr'],
    ['Multi-tracker',10,'mtr'],
    ['Blacksun filter',3,'bs'],
    ['Target lock',5,'tl'],
    ['Flechette discharger',10,'fd'],
    ['Distruption pod',5,'dp'],
    ['Decoy launchers',5,'dl']
]
Infantry = [
    ['Bonding knife',5,'bk'],
    ['EMP grenades',3,'empg'],
    ['Hard-wired blacksun filter',3,'bs'],
    ['Hard-wired drone controller',0,'dc'],
    ['Hard-wired multi-tracker',5,'mt'],
    ['Hard-wired target lock',5,'tl'],
]


class Crisis(Unit):
    gear = ['XV8 Crisis battlesuit']
    name = 'Crisis'

    def __init__(self, alone=True, can_select_ass=True, max_count=1):
        Unit.__init__(self)

        if max_count > 1:
            self.count = self.opt_count(self.name, 1, max_count, self.base_points)
            self.exclude_ids = [self.count.id]
        else:
            self.exclude_ids = []
        self.exclude_description_ids = []

        self.base_weapon = [
            ['Burst cannon', 8, 'bc'],
            ['Flamer', 4, 'flame'],
            ['Fusion blaster', 12, 'fblast'],
            ['Missile pod', 12, 'mpod'],
            ['Plasma rifle', 20, 'pgun'],
        ]

        self.ids = [w[2] for w in self.base_weapon]

        add = reduce(lambda arr, w: arr + [w, ['Additional ' + w[0].lower(), int(w[1] * .5), 'add_' + w[2]]],
                     self.base_weapon, [])

        self.weapon = self.opt_options_list('Weapon systems', add)
        self.weapon_spec = self.opt_options_list('', [
            ['Airbursting fragmentation projector', 20, 'afp'],
            ['Cyclic ion blaster', 15, 'cib']
        ])

        self.sup = self.opt_options_list('Support systems', (BSupport if can_select_ass else BSupport[1:]) +
                                                            (BSupportSpec if alone else BSupportSpec[1:]))
        self.war_gear = self.opt_options_list('Wargear', BWargear + (BWargearSpec if alone else BWargearSpec[1:]))
        self.drones = [self.opt_count(tp[0], 0, 2, tp[1]) for tp in Drones]

    def check_rules(self):
        sys_count = 0

        selected = self.weapon.get_all()
        for wep_id in self.ids:
            self.weapon.set_visible_options(['add_' + wep_id], wep_id in selected)

        sys_count += len(self.weapon.get_all()) + len(self.sup.get_all()) + len(self.weapon_spec.get_all())
        if sys_count != 3:
            self.error(self.name + ' must select 3 systems from battlesuit weapon and support systems '
                       '(selected: {0}).'.format(sys_count))

        has_dc = self.war_gear.get('dc') or self.sup.get('dc')
        dr_cnt = 0
        for ctr in self.drones:
            ctr.set_active(has_dc)
            dr_cnt += ctr.get() if has_dc else 0
        if has_dc:
            norm_counts(0, 2, self.drones)
            if dr_cnt != 2:
                self.error('2 drones must be taken along with controller')

        if self.war_gear.points() > 100:
            self.error('Taken wargear shouldn\'t cost more then 100 points.')

        #set hard-wired and hardpoint systems as mutually exclusive
        for dual_id in ['bs', 'dc', 'mtr', 'tl']:
            self.sup.set_active_options([dual_id], not self.war_gear.get(dual_id))
            self.war_gear.set_active_options([dual_id], not self.sup.get(dual_id))

        single_points = self.build_points(exclude=self.exclude_ids, count=1)
        self.set_points(single_points * self.get_count())

        def has_twin_linked():
            for wep_id in self.ids:
                if self.weapon.get(wep_id) and self.weapon.get('add_' + wep_id):
                    return next(w[0] for w in self.base_weapon if w[2] == wep_id)

        tl = has_twin_linked()
        if tl:
            self.build_description(exclude=[self.weapon.id] + self.exclude_ids + self.exclude_description_ids, count=1)
            self.description.add('Twin-linked ' + tl.lower())
        else:
            self.build_description(exclude=self.exclude_ids + self.exclude_description_ids, count=1, points=single_points)

    def has_special(self, spid):
        result = 1 if self.weapon.get(spid) else 0
        result += 1 if self.weapon_spec.get(spid) else 0
        result += 1 if self.sup.get(spid) else 0
        result += 1 if self.war_gear.get(spid) else 0
        return result * self.get_count()

    def has_advanced_ss(self):
        return self.sup.get('ass')

    def count_vrt(self):
        return self.get_count() if self.sup.get('vrt') else 0

    def count_es(self):
        return self.get_count() if self.sup.get('es') else 0

    def set_name(self, name):
        self.name = name

    def add_exclude_description(self, exclude_id):
        self.exclude_description_ids.append(exclude_id)

    def enable_war_gear(self, val):
        self.sup.set_visible_options(['dc'], val)
        self.war_gear.set_visible(val)

    def enable_special(self, val):
        self.weapon_spec.set_visible(val)
        self.sup.set_visible_options([item[2] for item in BSupportSpec], val)
        self.war_gear.set_visible_options([item[2] for item in BWargearSpec], val)
