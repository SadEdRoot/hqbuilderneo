__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.tau2005.armory import BWargear, BSupport, Drones, Crisis
from builder.core.options import norm_counts
from functools import reduce


class CrisisTeam(Unit):
    name = 'XV8 Crisis battlesuit team'

    class CrisisSuit(Crisis):
        base_points = 25

        def __init__(self):
            self.name = 'Shas\'ui'
            Crisis.__init__(self, max_count=3)
            self.upgrade = self.opt_options_list('', [
                ['Team leader', 5, 'ld'],
                ['Shas\'vre', 10, 'up']
            ], limit=1)
            self.add_exclude_description(self.upgrade.id)

        def check_rules(self):
            if self.upgrade.get('up'):
                self.set_name('Shas\'vre')
            elif self.upgrade.get('ld'):
                self.set_name('Team leader Shas\'ui')
            else:
                self.set_name('Shas\'ui')
            self.enable_war_gear(self.upgrade.get('up') or self.upgrade.get('ld'))
            self.enable_special(self.upgrade.get('up'))
            Crisis.check_rules(self)

        def count_leader(self):
            return self.get_count() if (self.upgrade.get('up') or self.upgrade.get('ld')) else 0

    def __init__(self):
        Unit.__init__(self)
        self.team = self.opt_units_list('Shas\'ui', self.CrisisSuit, 1, 3)

    def check_rules(self):
        self.team.update_range()

        if reduce(lambda val, m: val + m.count_es(), self.team.get_units(), 0) > 1:
            self.error("Ejection systems may be taken for single-model units only (discounting drones).")
        if reduce(lambda val, m: val + m.count_vrt(), self.team.get_units(), 0) > 1:
            self.error("Vectored retro-thrusters may be taken for single-model units only (discounting drones).")

        flag = self.team.get_units()[0].has_advanced_ss()
        if not all([flag == suit.has_advanced_ss() for suit in self.team.get_units()]):
            self.error("Advanced stabilisation system must be taken on either all of the members of the unit or none of them.")

        leader = reduce(lambda val, m: val + m.count_leader(), self.team.get_units(), 0)
        if leader > 1:
            self.error('Only one Shas\'ui can be upgraded to Team Leader or Shas\'vre (upgraded: {0}).'.format(leader))
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return self.team.get_count()

    def has_special(self, spid):
        result = 0
        for member in self.team.get_units():
            result += member.has_special(spid)
        return result


class StealthTeam(Unit):
    name = 'Stealth Team'

    class Leader(Unit):
        name = 'Team leader'
        base_points = 35
        gear = ['XV15 Stealthsuit']

        def __init__(self):
            Unit.__init__(self)
            self.weapon = self.opt_one_of('Weapon', [
                ['Burst cannon', 0, 'bcan'],
                ['Fusion blaster', 2, 'fb']
            ])

            self.sup = self.opt_options_list('Support systems', BSupport, 1)
            self.wgear = self.opt_options_list('Wargear', BWargear)
            self.upgrade = self.opt_options_list('Promote', [['Upgrade to Shas\'vre', 5, 'up']])
            self.drones = [self.opt_count(tp[0], 0, 2, tp[1]) for tp in Drones]
            self.opt = self.opt_options_list('Options', [['Markerlight', 10, 'ml']])

        def check_rules(self):
            for dual_id in ['bs', 'dc', 'mtr', 'tl']:
                self.sup.set_active_options([dual_id],not self.wgear.get(dual_id))
                self.wgear.set_active_options([dual_id],not self.sup.get(dual_id))
            if self.wgear.points() > 100:
                self.error('Taken wargear shouldn\'t cost more then 100 points.')
            if self.upgrade.get('up'):
                fullname = 'Team leader Shas\'ui'
            else:
                fullname = 'Shas\'vre'
            has_dc = self.wgear.get('dc') or self.sup.get('dc')
            dr_cnt = 0
            for ctr in self.drones:
                ctr.set_active(has_dc)
                dr_cnt += ctr.get() if has_dc else 0
            if has_dc:
                norm_counts(0, 2, self.drones)
                if dr_cnt != 2:
                    self.error('2 drones must be taken along with controller')
            self.set_points(self.build_points())
            self.build_description(exclude=[self.upgrade.id], name=fullname)

        def has_advanced_ss(self):
            return self.sup.get('ass')

    class Suit(ListSubUnit):
        max = 6
        name = 'Shas\'ui'
        base_points = 30
        gear = ['XV15 Stealthsuit']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.weapon = self.opt_one_of('Weapon', [
                ['Burst cannon', 0, 'bcan'],
                ['Fusion blaster', 2, 'fb']
            ])
            #no drones for puny grunts
            self.sup = self.opt_options_list('Support systems', BSupport[:-1], 1)

        def has_advanced_ss(self):
            return self.sup.get('ass')

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_optional_sub_unit('Team Leader', [self.Leader()])
        self.team = self.opt_units_list(self.Suit.name, self.Suit, 1, 6)

    def check_rules(self):
        self.team.update_range(3 - self.leader.get_count(), 6 - self.leader.get_count())
        fulllist = self.team.get_units()[:] # we need copy but not ref for this array
        if self.leader.get(self.leader.name):
            fulllist += [self.leader.get_unit().get_unit()]
        flim = int(len(fulllist) / 3)
        ftaken = reduce(lambda val,u: val + (1 if u.weapon.get_cur() == 'fb' else 0),fulllist,0)
        if ftaken > flim:
            self.error('Only one blaster may be taken per 3 models in unit.')
        if not (all([len(u.sup.get_all()) > 0 for u in fulllist]) or all([len(u.sup.get_all()) == 0 for u in fulllist])):
            self.error('All members of the unit must take a support system or none of them.')
        flag = fulllist[0].has_advanced_ss()
        if not all([flag == suit.has_advanced_ss() for suit in fulllist]):
            self.error("Advanced stabilisation system must be taken on either all of the members of the unit or none of them.")
        self.points.set(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.leader.get_count() + self.team.get_count()
