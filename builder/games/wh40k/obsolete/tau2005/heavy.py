__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, StaticUnit
from builder.games.wh40k.obsolete.tau2005.armory import Vehicle,BWargear,BSupport,Drones
from builder.core.options import norm_counts
from functools import reduce


class BroadsideTeam(Unit):
    name = 'Broadside Battlesuit team'

    class BroadsideSuit(Unit):
        name = 'Shas\'ui'
        base_points = 70
        gear = ['XV88 Battlesuit','Twin-linked railgun']
        promotable = True

        def __init__(self):
            Unit.__init__(self)
            self.count = self.opt_count(self.name, 1, 3, self.base_points)
            self.weapon = self.opt_one_of('Weapon',[
                ['Smart missile system',0,'sms'],
                ['Twin-linked plasma rifle',10,'tlpgun']
            ])
            self.sup = self.opt_options_list('Support systems',BSupport,1)
            self.wgear = self.opt_options_list('Wargear',BWargear)
            self.drones  = [self.opt_count(tp[0],0,2,tp[1]) for tp in Drones]
            self.upgrade = self.opt_options_list('Promote',[
                ['Designate as team leader',5,'ld'],
                ['Upgrade to Shas\'vre',10,'up']
            ],1)

        def check_rules(self):
            self.reset_errors()
            self.upgrade.set_active(self.promotable)
            lflag = self.count_leader() > 0
            self.wgear.set_visible(lflag)
            self.sup.set_visible_options(['dc'], lflag and not self.wgear.get('dc'))
            if lflag:
                for dual_id in ['bs','dc','mtr','tl']:
                    self.sup.set_active_options([dual_id],not self.wgear.get(dual_id))
                    self.wgear.set_active_options([dual_id],not self.sup.get(dual_id))
            if self.wgear.points() > 100:
                self.error('Taken wargear shouldn\'t cost more then 100 points.')
            if len(self.sup.get_all()) < 1:
                self.error('One battlesuit support system must be taken')
            if self.upgrade.get('up'):
                fullname = 'Team leader Shas\'ui'
            elif self.upgrade.get('ld'):
                fullname = 'Shas\'vre'
            else:
                fullname = 'Shas\'ui'
            has_dc = self.wgear.get('dc') or self.sup.get('dc')
            dr_cnt = 0
            for ctr in self.drones:
                ctr.set_active(has_dc)
                dr_cnt += ctr.get() if has_dc else 0
            if has_dc:
                norm_counts(0,2,self.drones)
                if dr_cnt != 2:
                    self.error('2 drones must be taken along with controller')
            single_points = self.build_points(exclude=[self.count.id], count=1)
            self.set_points(single_points * self.get_count())
            self.build_description(exclude=[self.upgrade.id, self.count.id], name=fullname,
                                   count=1, points=single_points)

        def has_advanced_ss(self):
            return self.sup.get('ass')

        def count_leader(self):
            return self.get_count() if (self.upgrade.get('up') or self.upgrade.get('ld')) else 0

    def __init__(self):
        Unit.__init__(self)
        self.team = self.opt_units_list(self.BroadsideSuit.name,self.BroadsideSuit,1,3)

    def check_rules(self):
        self.team.update_range()
        #check advanced stabilisation systems on all members
        flag = any(member.has_advanced_ss() for member in self.team.get_units())
        if not all([flag == suit.has_advanced_ss() for suit in self.team.get_units()]):
            self.error("Advanced stabilisation system must be taken on either all of the members of the unit or none of them.")

        leader = reduce(lambda val, m: val + m.count_leader(), self.team.get_units(), 0)
        if leader > 1:
            self.error('Only one Shas\'ui can be upgraded to Team Leader or Shas\'vre (upgraded: {0}).'.format(leader))
        # flag = any(member.is_leader() for member in self.team.get_units())
        # for member in self.team.get_units():
        #     member.make_promotable(not flag)
        self.points.set(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.team.get_count()


class SniperDroneTeam(Unit):
    name = 'Sniper drone team'
    base_points = 80

    class Spotter(StaticUnit):
        name = 'Spotter'
        gear = ['Pulse pistol','Drone controller','Networked markerlight','Stealth field generator','Tergeting array']

    class Drone(StaticUnit):
        name = 'Drone'
        gear =['Rail rifle','Target lock','Stealth field generator','Tergeting array']

    def __init__(self):
        Unit.__init__(self)
        self.sp = self.opt_sub_unit(self.Spotter())
        self.dr = self.opt_sub_unit(self.Drone(count=3))


class Hammerhead(Unit):
    name = 'Hammerhead'
    base_points = 90
    gear = ['Targeting array']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Primary weapon',[
            ['Ion cannon',15,'ican'],
            ['Railgun',50,'rgun']
        ])
        self.wep2 = self.opt_one_of('Secondary weapon',[
            ['Burst cannon', 10, 'bcan'],
            ['Gun Drone', 20, 'gd'],
            ['Smart missile system',10,'sms']
        ])
        self.opt = self.opt_options_list('Options', Vehicle)
        self.mis = self.opt_count('Seeker missile', 0, 2, 10)
        self.opt.set_visible_options(['tarr'], False)

    def check_rules(self):
        Unit.check_rules(self)
        if self.wep2.get_cur() != 'sms':
            self.description.add(self.wep2.get_selected())


class SkyRay(Unit):
    name = 'Sky Ray'
    base_points = 125
    gear = ['Seeker missile' for _ in range(6)] + ['Networked markerlight' for _ in range(2)] + ['Target lock']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Secondary weapon',[
            ['Burst cannon',10,'bcan'],
            ['Gun Drone',20,'gd'],
            ['Smart missile system',10,'sms']
        ])
        self.opt = self.opt_options_list('Options', Vehicle)
        self.opt.set_visible_options(['tarr'], False)

    def check_rules(self):
        Unit.check_rules(self)
        if self.wep.get_cur() != 'sms':
            self.description.add(self.wep.get_selected())
