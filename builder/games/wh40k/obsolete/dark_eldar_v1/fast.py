__author__ = 'Ivan Truskov'

from builder.core.unit import Unit,ListSubUnit
from builder.core.options import norm_counts
from functools import reduce


class Hellions(Unit):
    name = 'Hellions'
    gear = ['Wychsuit', 'Combat drugs','Skyboard','Hellglaive']
    min = 5
    max = 20

    class Helliarch(Unit):
        name = 'Helliarch'
        base_points = 16 + 10

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon',[
                ['Hellglaive',0,'hgl'],
                ['Venom blade',0,'vsw'],
                ['Stunclaw', 5, 'sclaw' ],
                ['Power weapon', 5, 'pwep'],
                ['Agoniser', 15, 'agon' ]
            ])
            self.opt = self.opt_options_list('Options',[
                ["Phantasm grenade launcher", 25, 'phgrlnch']
            ])

        def check_rules_chain(self):
            self.gear = ['Wychsuit', 'Combat drugs','Skyboard']
            if self.wep.get_cur() != 'hlg':
                self.gear += ['Splinter pistol']
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Hellion', self.min, self.max,16)
        self.leader = self.opt_optional_sub_unit(self.Helliarch.name, self.Helliarch())

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()
        

class Scourges(Unit):
    name = 'Scourges'
    gear = ['Ghostplate armour', 'Plasma grenades']

    class Solarite(Unit):
        name = 'Solarite'
        gear = ['Ghostplate armour', 'Plasma grenades']
        base_points = 22 + 10

        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list('Weapon',[
                ["Shardcarbine", 0, 'shardcarb']
            ])
            self.opt.set('shardcarb', True)
            self.rng = self.opt_one_of('',[
                ['Splinter pistol', 0, 'splpist' ],
                ['Blast pistol', 15, 'blpist' ]
            ])
            self.ccw = self.opt_one_of('', [
                ['Close combat weapon', 0, 'ccw' ],
                ['Venom blade', 5, 'vblade' ],
                ['Power weapon', 10, 'pwep' ],
                ['Agoniser', 20, 'agon' ]
            ])

        def check_rules(self):
            self.rng.set_visible(not self.opt.get('shardcarb'))
            self.ccw.set_visible(not self.opt.get('shardcarb'))
            Unit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Scourge',3,10,22)
        weplist = [
            ['Shredder', 5, 'shred' ],
            ['Splinter cannon', 10, 'splcan' ],
            ['Haywire blaster', 10, 'hwblast'],
            ['Heat lance',12,'hlance'],
            ['Blaster', 15, 'blast' ],            
            ['Dark lance', 15, 'dlance' ]
        ]
        self.wep = [self.opt_count(o[0], 0, 1, o[1]) for o in weplist]
        self.leader = self.opt_optional_sub_unit(self.Solarite.name, self.Solarite())

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(3 - ldr, 10 - ldr)
        norm_counts(0, int(self.get_count() / 5) * 2, self.wep)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id] + [o.id for o in self.wep])
        self.description.add(['Shardcarbine'], self.warriors.get() - reduce(lambda val, i: val + i.get(), self.wep, 0))
        self.description.add([o.get_selected() for o in self.wep])

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Reavers(Unit):
    name = 'Reavers'
    min = 3
    max = 10
    gear = ['Wychsuit', 'Splinter pistol','Close combat weapon','Combat drugs','Reaver jetbike', 'Bladevanes']
#    class Reaver(ListSubUnit):
#        name = 'Reaver'
#        base_points = 22
#        min = 1
#        max = 10
#        gear = ['Wychsuit', 'Splinter pistol','Close combat weapon','Combat drugs','Reaver jetbike','Bladevanes']
#        def __init__(self):
#            ListSubUnit.__init__(self)
#            self.bkw = self.opt_one_of('Jetbike weapon',[
#                ['Splinter rifle' , 0, 'splgun'],
#                ['Heat lance', 12, 'hlance' ],
#                ['Blaster', 15, 'blast' ]
#                    ])
#            self.opt = self.opt_options_list('Options',[
#                ["Grav-talon", 10, 'gravt'],
#                ['Cluster caltrops', 20,'clust']
#                    ],1)
#        def check_rules(self):
#            self.points.set(self.bkw.points() + self.opt.points() + self.base_points)
#        def is_bike_custom(self):
#            return not self.bkw.get('splgun')
#        def customize(self,val):
#            if not self.is_bike_custom():
#                self.bkw.set_active_options(['hlance','blast'],val)
#        def has_gear(self):
#            return self.opt.get('gravt') or self.opt.get('clust')
#        def set_gear(self,val):
#            if not self.has_gear():
#                self.opt.set_active(val)

    class Champion(Unit):
        name = 'Arena Champion'
        base_points = 32
        gear = ['Wychsuit', 'Splinter pistol','Combat drugs', 'Bladevanes', 'Reaver jetbike', 'Splinter rifle']
        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Combat weapon',[
                ['Close combat weapon', 0, 'ccw' ],
                ['Venom blade', 5, 'vblade' ],
                ['Power weapon', 10, 'pwep' ],
                ['Agoniser', 20, 'agon' ]
            ])

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Reaver', self.min, self.max, 22)

        jb = [
            ['Heat lance', 12, 'hlance' ],
            ['Blaster', 15, 'blast' ]
        ]
        self.jb = [self.opt_count(o[0], 0, 1, o[1]) for o in jb]

        opt = [
            ["Grav-talon", 10, 'gravt'],
            ['Cluster caltrops', 20,'clust']
        ]
        self.opt = [self.opt_count(o[0], 0, 1, o[1]) for o in opt]
        self.leader = self.opt_optional_sub_unit(self.Champion.name, self.Champion())

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min - ldr, self.max - ldr)
        norm_counts(0, int(self.get_count() / 3), self.jb)
        norm_counts(0, int(self.get_count() / 3), self.opt)
        self.set_points(self.build_points(count=1))
        self.build_description(count=self.warriors.get(), exclude=[self.warriors.id])
        self.description.add(['Splinter rifle'], self.warriors.get() - reduce(lambda val, i: val + i.get(), self.jb, 0))

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Beastmasters(Unit): # TODO
    name = 'Beastmasters'
    class Beastmaster(Unit):
        name = 'Beastmaster'
        base_points = 12
        gear = ['Wychsuit', 'Skyboard']
        def __init__(self):
            Unit.__init__(self)
            self.spec_w = ['vblade','pwep','agon']
            self.ccw = self.opt_one_of('Combat weapon',[
                ['Close combat weapon', 0, 'ccw' ],
                ['Venom blade', 5, 'vblade' ],
                ['Power weapon', 10, 'pwep' ],
                ['Agoniser', 20, 'agon' ]
            ])
            self.khym = self.opt_count('Khymera',0,5,12)
            self.flock = self.opt_count('Razorwing flock',0,2,15)
            self.fiend = self.opt_count('Clawed Fiend',0,1,40)

        def check_rules(self):
            self.khym.set_active(self.flock.get() + self.fiend.get() == 0)
            self.flock.set_active(self.khym.get() + self.fiend.get() == 0)
            self.fiend.set_active(self.khym.get() + self.flock.get() == 0)
            self.set_points(self.build_points(count=1))
            self.build_description(count=1)

        def has_spec(self):
            return not self.ccw.get_cur() == 'ccw'

        def set_spec(self,val):
            if not self.has_spec():
                self.ccw.set_active_options(self.spec_w,val)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_units_list(self.Beastmaster.name,self.Beastmaster,1,5)

    def check_rules(self):
        spec = reduce(lambda val, u: val or u.has_spec(), self.warriors.get_units(), False)
        for u in self.warriors.get_units():
            u.set_spec(not spec)
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)

    def get_count(self):
        return reduce(lambda val, u: val + 1 + u.khym.get() + u.flock.get() + u.fiend.get(), self.warriors.get_units(), 0)
