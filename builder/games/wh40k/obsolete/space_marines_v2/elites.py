__author__ = 'Ivan Truskov'

from builder.core2 import Unit, SubUnit, ListSubUnit, Gear, OptionsList,\
    Count, UnitDescription, UnitList
from .transport import TerminatorTransport, Transport
from .armory import *
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle


class CommandSquad(Unit):
    type_id = 'command_squad_v1'
    type_name = 'Command Squad'

    model_points = 20

    class Veteran(ListSubUnit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Veteran.Options, self).__init__(parent=parent, name='Options')

                self.mbomb = self.variant('Melta bombs', 5)
                self.ss = self.variant('Storm Shield', 10)

        class Banners(OptionsList):
            def __init__(self, parent):
                super(CommandSquad.Veteran.Banners, self).__init__(parent=parent, name='Banners', limit=1)

                self.comstd = self.variant('Company Standard', 15)
                self.std = self.variant('Standard of the Emperor Ascendant', 65)

                # Sentinels
                self.staganda = self.variant('The Banner of Staganda', 25)

            def check_rules(self):
                self.staganda.visible = self.staganda.used = self.parent.roster.is_sentinels()

            def get_unique(self):
                if self.std.value:
                    return self.description
                return []

        def __init__(self, parent):
            super(CommandSquad.Veteran, self).__init__(
                parent=parent, name='Veteran', points=CommandSquad.model_points, gear=Armour.power_armour_set)

            class Weapon1(Special, Ranged, Melee, Boltgun, Chainsword):
                pass

            class Weapon2(Special, Ranged, Melee, Boltgun, BoltPistol):
                pass

            self.weapon1 = Weapon1(self, 'Weapon')
            self.weapon2 = Weapon2(self, '')
            self.opt = CommandSquad.Veteran.Options(self)
            self.banners = CommandSquad.Veteran.Banners(self)

        @ListSubUnit.count_gear
        def count_banners(self):
            return self.banners.any

    class SpecModels(OptionsList):
        def __init__(self, parent):
            super(CommandSquad.SpecModels, self).__init__(parent, 'Options')
            cham_points = CommandSquad.model_points + 15
            medic_points = CommandSquad.model_points + 15

            self.ch = self.variant('Company Champion', cham_points, gear=UnitDescription(
                name='Company Champion',
                points=cham_points,
                options=Armour.power_armour_set + [
                    Gear('Power weapon'),
                    Gear('Combat Shield'),
                    Gear('Bolt pistol'),
                ]
            ))
            self.ap = self.variant('Apothecary', medic_points, gear=UnitDescription(
                name='Apothecary',
                points=medic_points,
                options=Armour.power_armour_set + [
                    Gear('Chainsword'),
                    Gear('Narthecium'),
                ]
            ))
            self.bikes = self.variant('Bikes', 35)

    def __init__(self, parent):
        super(CommandSquad, self).__init__(parent=parent)
        self.veterans = UnitList(self, self.Veteran, min_limit=1, max_limit=5, start_value=5)
        self.up = CommandSquad.SpecModels(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.veterans.count + (self.up.count - self.up.bikes.value)

    def check_rules(self):
        self.transport.visible = self.transport.used = not self.up.bikes.value
        if self.get_count() != 5:
            self.error('Command Squad must include 5 Veterans (include Apothecary and Company champion) '
                       '(taken: {})'.format(self.get_count()))
        flag = sum(c.count_banners() for c in self.veterans.units)
        if flag > 1:
            self.error('Only one Veteran can take a banner (taken: {})'.format(flag))

    def get_unique_gear(self):
        return sum((unit.banners.get_unique() for unit in self.veterans.units), []) + self.transport.get_unique_gear()


class TerminatorAssaultSquad(Unit):
    type_name = 'Terminator Assault Squad'
    type_id = 'terminatorassaultsquad_v1'

    model_points = 40

    class Sergeant(Unit):

        def __init__(self, parent):
            super(TerminatorAssaultSquad.Sergeant, self).__init__(
                name='Terminator Sergeant', parent=parent, points=200 - 4 * TerminatorAssaultSquad.model_points,
                gear=[Gear('Terminator armour')]
            )
            self.weapon = self.Weapon(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(TerminatorAssaultSquad.Sergeant.Weapon, self).__init__(parent=parent, name='Weapon')
                self.lightningclaws = self.variant('Lightning claws', 0, gear=[Gear('Lightning claw', count=2)])
                self.thunderhammerandstormshield = self.variant(
                    'Thunder hammer and storm shield', 5, gear=[Gear('Thunder hammer'), Gear('Storm shield')])

    def __init__(self, parent):
        super(TerminatorAssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(None))
        self.terminator = Count(self, 'Terminator', min_limit=4, max_limit=9, points=40, gear=[])
        self.thunderhammerandstormshield = Count(self, 'Thunder Hammer and Storm Shield',
                                                 min_limit=0, max_limit=4, points=5, gear=[])
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terminator.cur + 1

    def check_rules(self):
        super(TerminatorAssaultSquad, self).check_rules()
        self.thunderhammerandstormshield.max = self.terminator.cur

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        term = UnitDescription('Terminator', self.model_points, options=[Gear('Terminator armour')])
        desc.add(term.clone().add([Gear('Thunder hammer'), Gear('Storm shield')]).add_points(5)
            .set_count(self.thunderhammerandstormshield.cur))
        desc.add(term.add([Gear('Lightning claw', count=2)])
            .set_count(self.terminator.cur - self.thunderhammerandstormshield.cur))
        desc.add(self.transport.description)
        return desc

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class TerminatorSquad(Unit):
    type_name = 'Terminator Squad'
    type_id = 'terminator_squad_v1'

    model_points = 40

    class Veteran(ListSubUnit):
        class LeftWeapon(OneOf):
            def __init__(self, parent):
                super(TerminatorSquad.Veteran.LeftWeapon, self).__init__(parent=parent, name='Weapon')

                self.power_fist = self.variant('Power fist', 0)
                self.chain_fist = self.variant('Chainfist', 5)

        class RightWeapon(OneOf):
            def __init__(self, parent):
                super(TerminatorSquad.Veteran.RightWeapon, self).__init__(parent=parent, name='')

                self.bolt = self.variant('Storm bolter', 0)
                self.hf = self.variant('Heavy flamer', 10)
                self.ac = self.variant('Assault Cannon', 20)

            def enable_spec(self, value):
                self.hf.active = self.ac.active = value

            def has_spec(self):
                return self.cur != self.bolt

        class Weapon(OptionsList):
            def __init__(self, parent):
                super(TerminatorSquad.Veteran.Weapon, self).__init__(parent=parent, name='')
                self.cyc = self.variant('Cyclone missile launcher', 25)

        def __init__(self, parent):
            super(TerminatorSquad.Veteran, self).__init__(
                parent=parent, name='Terminator', points=TerminatorSquad.model_points,
                gear=[Gear('Terminator Armour')]
            )

            self.left_weapon = self.LeftWeapon(self)
            self.right_weapon = self.RightWeapon(self)
            self.weapon = self.Weapon(self)

        def check_rules(self):
            super(TerminatorSquad.Veteran, self).check_rules()
            self.right_weapon.enable_spec(not self.weapon.cyc.value)
            self.weapon.cyc.active = not self.right_weapon.has_spec()

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.weapon.cyc.value or self.right_weapon.has_spec()

    def __init__(self, parent):
        super(TerminatorSquad, self).__init__(parent=parent, points=self.model_points, gear=[UnitDescription(
            'Terminator Sergeant', 200 - 4 * self.model_points,
            options=[Gear('Terminator armour'), Gear('Storm bolter'), Gear('Power sword')]
        )])
        self.terms = UnitList(self, self.Veteran, min_limit=4, max_limit=9)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.terms.count + 1

    def check_rules(self):
        spec_limit = 1 if self.get_count() < 10 else 2
        spec = sum(c.has_spec() for c in self.terms.units)
        if spec > spec_limit:
            self.error('Terminators may take only {0} special weapon (taken {1})'.format(spec_limit, spec))

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class CenturionAssault(Unit):
    type_name = 'Centurion Assault Squad'
    type_id = 'centurion_assault_squad_v1'

    model_gear = [Gear('Siege drill', count=2)]

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(CenturionAssault.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.ial = self.variant('Ironclad assault launchers', 0)
            self.hurricanebolter = self.variant('Hurricane bolter', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(CenturionAssault.Weapon2, self).__init__(parent=parent, name='')
            self.twinlinkedheavybolter = self.variant('Twin-linked flamer', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked meltagun', 5)

    class Centurion(ListSubUnit):

        def __init__(self, parent):
            super(CenturionAssault.Centurion, self).__init__(parent=parent, name='Centurion', points=60,
                                                             gear=CenturionAssault.model_gear)
            self.wep1 = CenturionAssault.Weapon1(self)
            self.wep2 = CenturionAssault.Weapon2(self)

    class Sergeant(Unit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(CenturionAssault.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.omniscope = self.variant('Omniscope', 10)

        def __init__(self, parent):
            super(CenturionAssault.Sergeant, self).__init__(parent=parent, name='Centurion Sergeant', points=70,
                                                            gear=CenturionAssault.model_gear)
            self.wep1 = CenturionAssault.Weapon1(self)
            self.wep2 = CenturionAssault.Weapon2(self)
            self.opt = self.Options(self)

    def __init__(self, parent):
        super(CenturionAssault, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=None))
        self.cent = UnitList(self, self.Centurion, min_limit=2, max_limit=5)
        self.transport = TerminatorTransport(self)

    def get_count(self):
        return self.cent.count + 1

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class VanguardVeteranSquad(Unit):
    type_name = 'Vanguard Veteran Squad'
    type_id = 'vanguard_veteran_squad_v1'

    model_points = 19

    class Options(OptionsList):
        def __init__(self, parent):
            super(VanguardVeteranSquad.Options, self).__init__(parent=parent, name='Options', limit=None)

            self.meltabombs = self.variant('Melta bombs', 5)
            self.stormshield = self.variant('Storm shield', 10)

    class Veteran(ListSubUnit):
        def __init__(self, parent):
            super(VanguardVeteranSquad.Veteran, self).__init__(
                parent=parent, name='Veteran', points=VanguardVeteranSquad.model_points, gear=Armour.power_armour_set
            )

            class Weapon1(PowerPistols, Melee, Chainsword):
                pass

            class Weapon2(PowerPistols, Melee, BoltPistol):
                pass

            self.wep1 = Weapon1(self, 'Weapon')
            self.wep1 = Weapon2(self, '')
            self.opt = VanguardVeteranSquad.Options(self)

    class Sergeant(Unit):
        def __init__(self, parent):
            super(VanguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, name='Veteran Sergeant', points=95 - 4 * VanguardVeteranSquad.model_points,
                gear=Armour.power_armour_set
            )

            class Weapon1(PowerPistols, Melee, RelicBlade, Chainsword):
                pass

            class Weapon2(PowerPistols, Melee, RelicBlade, BoltPistol):
                pass

            self.wep1 = Weapon1(self, 'Weapon')
            self.wep1 = Weapon2(self, '')
            self.opt = VanguardVeteranSquad.Options(self)

    class Jump(OptionsList):
        def __init__(self, parent):
            super(VanguardVeteranSquad.Jump, self).__init__(parent=parent, name='Options', limit=None)
            self.jump = self.variant('Jump packs', 3, per_model=True)

    def __init__(self, parent):
        super(VanguardVeteranSquad, self).__init__(parent)
        self.leader = SubUnit(self, self.Sergeant(None))
        self.vets = UnitList(self, self.Veteran, 4, 9)
        self.jump = self.Jump(self)
        self.transport = Transport(self)

    def get_count(self):
        return self.vets.count + 1

    def check_rules(self):
        super(VanguardVeteranSquad, self).check_rules()
        self.transport.visible = self.transport.used = not self.jump.jump.value
        self.jump.visible = self.jump.used = not self.transport.options.any

    def build_points(self):
        return super(VanguardVeteranSquad, self).build_points() - self.jump.points + self.jump.points * self.get_count()

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class SternguardVeteranSquad(Unit):
    type_name = 'Sternguard Veteran Squad'
    type_id = 'sternguard_veteran_squad_v1'

    model_points = 22

    class Veteran(ListSubUnit):

        class Weapon(Heavy, Special, Comby, Boltgun):
            pass

        def __init__(self, parent):
            super(SternguardVeteranSquad.Veteran, self).__init__(
                parent=parent, points=SternguardVeteranSquad.model_points, name='Veteran',
                gear=Armour.power_armour_set + [Gear('Special issue ammunition'), Gear('Bolt pistol')]
            )
            self.weapon = self.Weapon(self, 'Weapon', heavy_flamer=True)

        @ListSubUnit.count_gear
        def count_weapon(self):
            return self.weapon.is_heavy() or self.weapon.is_spec()

    class Sergeant(Unit):

        class Weapon1(PowerPistols, PowerFist, PowerWeapon, LightningClaw, Chainsword, BoltPistol):
            pass

        class Weapon2(Comby, PowerPistols, PowerFist, PowerWeapon, LightningClaw, Chainsword, Boltgun):
            pass

        def __init__(self, parent):
            super(SternguardVeteranSquad.Sergeant, self).__init__(
                parent=parent, points=120 - 4 * SternguardVeteranSquad.model_points, name='Veteran Sergeant',
                gear=Armour.power_armour_set + [Gear('Special issue ammunition')]
            )

            self.wep1 = self.Weapon1(self, 'Weapon')
            self.wep2 = self.Weapon2(self, '')
            self.opt = self.Options(self)

        class Options(OptionsList):
            def __init__(self, parent):
                super(SternguardVeteranSquad.Sergeant.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.meltabombs = self.variant('Melta bombs', 5)

    def __init__(self, parent):
        super(SternguardVeteranSquad, self).__init__(parent)
        self.leader = SubUnit(self, self.Sergeant(None))
        self.vets = UnitList(self, self.Veteran, 4, 9)
        self.transport = Transport(self)

    def check_rules(self):
        spec_total = sum(u.count_weapon() for u in self.vets.units)
        if 2 < spec_total:
            self.error('Veterans can take only 2 special or heavy weapon (taken: {0})'.format(spec_total))

    def get_count(self):
        return self.vets.count + 1

    def get_unique_gear(self):
        return self.transport.get_unique_gear()


class Dreadnought(SpaceMarinesBaseVehicle):
    type_name = 'Dreadnought'
    type_id = 'dreadnought_v1'

    def __init__(self, parent):
        super(Dreadnought, self).__init__(parent=parent, points=100, walker=True,
                                          gear=[Gear('Smoke launchers'), Gear('Searchlight')])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)
        self.opt = self.Options(self)
        self.transport = Transport(self, dreadnought=True)

    def check_rules(self):
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist

    def build_description(self):
        desc = super(Dreadnought, self).build_description()
        if self.opt.venerabledreadnought.value:
            desc.name = self.opt.venerabledreadnought.title
        return desc

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.multimelta = self.variant('Multi-melta', 0)
            self.twinlinkedautocannon = self.variant('Twin-linked autocannon', 5)
            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 5)
            self.twinlinkedheavyflamer = self.variant('Twin-linked heavy flamer', 5)
            self.plasmacannon = self.variant('Plasma cannon', 10)
            self.assaultcannon = self.variant('Assault cannon', 20)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Dreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant('Power fist', 0)
            self.missilelauncher = self.variant('Missile launcher', 10)
            self.twinlinkedautocannon = self.variant('Twin-linked autocannon', 15)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(Dreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant('Built-in Storm bolter', 0)
            self.builtinheavyflamer = self.variant('Built-in Heavy flamer', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Dreadnought.Options, self).__init__(parent=parent, name='Options', limit=None)
            self.extraarmour = self.variant('Extra armour', 10)
            self.venerabledreadnought = self.variant('Venerable Dreadnought', 25, gear=[])

    def get_unique_gear(self):
        return SpaceMarinesBaseVehicle.get_unique_gear(self) + self.transport.get_unique_gear()


class IronDred(SpaceMarinesBaseVehicle):
    type_name = 'Ironclad Dreadnought'
    type_id = 'ironcladdreadnought_v1'

    def __init__(self, parent):
        super(IronDred, self).__init__(parent=parent, points=135, walker=True,
                                       gear=[Gear('Smoke launchers'), Gear('Searchlight'), Gear('Extra armour')])
        self.wep1 = self.Weapon1(self)
        self.bin1 = self.BuildIn1(self)
        self.wep2 = self.Weapon2(self)
        self.bin2 = self.BuildIn2(self)
        self.opt = self.Options(self)
        self.missiles = Count(self, 'Hunter-killer missile', 0, 2, 10)
        self.transport = Transport(self, dreadnought=True)

    def check_rules(self):
        self.bin1.visible = self.bin1.used = self.wep1.cur == self.wep1.powerfist

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(IronDred.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.powerfist = self.variant('Power fist', 0)
            self.hurricanebolter = self.variant('Hurricane bolter', 0)

    class BuildIn1(OneOf):
        def __init__(self, parent):
            super(IronDred.BuildIn1, self).__init__(parent=parent, name='')
            self.buildinstormbolter = self.variant('Build-in Storm bolter', 0)
            self.buildinheavyflamer = self.variant('Build-in Heavy flamer', 10)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(IronDred.Weapon2, self).__init__(parent=parent, name='')
            self.seismichammer = self.variant('Seismic hammer', 0)
            self.chainfist = self.variant('Chainfist', 0)

    class BuildIn2(OneOf):
        def __init__(self, parent):
            super(IronDred.BuildIn2, self).__init__(parent=parent, name='')
            self.buildinmeltagun = self.variant('Build-in Meltagun', 0)
            self.buildinheavyflamer = self.variant('Build-in Heavy flamer', 0)

    class Options(OptionsList):
        def __init__(self, parent):
            super(IronDred.Options, self).__init__(parent=parent, name='Options')
            self.ironcladassaultlaunchers = self.variant('Ironclad assault launchers', 10)

    def get_unique_gear(self):
        return SpaceMarinesBaseVehicle.get_unique_gear(self) + self.transport.get_unique_gear()


class TheDamned(Unit):
    type_name = 'Legion of the Damned'
    type_id = 'legionofthedamned_v1'

    model_points = 25

    class DamnedSergeant(Unit):

        def __init__(self, parent):
            super(TheDamned.DamnedSergeant, self).__init__(
                parent=parent, points=125 - 4 * TheDamned.model_points, name='Legionnaire Sergeant',
                gear=Armour.power_armour_set
            )

            class Weapon1(Ranged, BoltPistol):
                pass

            class Weapon2(PowerFist, PowerWeapon, Chainsword, Boltgun):
                pass

            self.wep1 = Weapon1(self, 'Weapon')
            self.wep2 = Weapon2(self, '')

    def __init__(self, parent):
        super(TheDamned, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.DamnedSergeant(None))
        self.legionnaire = Count(self, 'Legionnaire', min_limit=4, max_limit=9, points=self.model_points)
        self.special = SpecialList(self, grav=False)
        self.heavy = HeavyList(self, heavy_flamer=True)

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, options=self.sergeant.description, count=self.get_count())
        count = self.legionnaire.cur
        leg = UnitDescription('Legionnaire', self.model_points, options=Armour.power_armour_set + [Gear('Bolt pistol')])

        for o in [self.heavy, self.special]:
            if o.used and o.any:
                spec_marine = leg.clone()
                spec_marine.points += o.points
                spec_marine.add(o.description)
                desc.add(spec_marine)
                count -= 1

        desc.add(leg.add(Gear('Boltgun')).set_count(count))
        return desc

    def get_count(self):
        return self.legionnaire.cur + 1
