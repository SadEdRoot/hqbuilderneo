__author__ = 'dante'

from builder.core.unit import Unit, StaticUnit
from functools import reduce


class CommandBarge(Unit):
    name = 'Catacomb Command Barge'
    base_points = 80
    gear = ['Quantum shielding']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Tesla Cannon', 0],
            ['Gauss Cannon', 0],
        ])


class Lord(Unit):
    name = 'Necron Lord'
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Lords", 1, 5, self.base_points)

        self.wep = self.opt_one_of('Weapon', [
            ['Staff of Light', 0],
            ['Hyperphase Sword', 0],
            ['Gauntlet of Fire', 5],
            ['Voidblade', 10],
            ['Warscythe', 10],
        ])

        self.opt = self.opt_options_list('Options', [
            ["Sempiternal Weave", 15],
            ["Mindshackle scarabs", 15],
            ["Tesseract labyrinth", 20],
            ["Resurrection Orb", 30],
            ["Phase Shifter", 45],
        ])

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.count.id]))
        self.description.reset()
        self.description.set_header(self.name, self.base_points + self.wep.points() + self.opt.points())
        self.description.add(self.wep.get_selected())
        self.description.add(self.opt.get_selected())
        self.set_description(self.description)


class Cryptek(Unit):
    name = 'Cryptek'
    base_points = 25

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Crypteks", 1, 5, self.base_points)

        self.wep = self.opt_one_of('Weapon', [
            ['Staff of Light', 0],
            ["Abyssal Staff (Harbinger of Despair)", 5, 'despair'],
            ["Eldritch Lance (Harbinger of Destruction)", 10, 'destruction'],
            ["Aeonstave (Harbinger of Eternity)", 0, 'eternity'],
            ["Voltaric Staff (Harbinger of the Storm)", 0, 'storm'],
            ["Thermostave (Harbinger of Transmogrification)", 5, 'trans'],
        ])
        self.despair = self.opt_options_list('Harbinger of Despair', [
            ["Nightmare Shroud", 10, 'ns'],
            ["Veil of Darkness", 30, 'vod'],
        ])
        self.destruction = self.opt_options_list('Harbinger of Destruction', [
            ["Gase of Flame", 10, 'gof'],
            ["Solar Pulse", 20, 'sp'],
        ])
        self.eternity = self.opt_options_list('Harbinger of Eternity', [
            ["Chronometron", 15, 'ch'],
            ["Timesplinter Cloak", 30, 'tc'],
        ])
        self.storm = self.opt_options_list('Harbinger of the Storm', [
            ["Lightning Shield", 10, 'ls'],
            ["Ether Crystal", 20, 'ec'],
        ])
        self.trans = self.opt_options_list('Harbinger of Transmogrification', [
            ["Seismic Crucible", 10, 'sc'],
            ["Harp of Dissonance", 20, 'hod'],
        ])
        self.unique_gear = [self.despair, self.destruction, self.eternity, self.storm, self.trans]

    def check_rules(self):
        self.despair.set_visible(self.wep.get_cur() == 'despair' and self.count.get() == 1)
        self.destruction.set_visible(self.wep.get_cur() == 'destruction' and self.count.get() == 1)
        self.eternity.set_visible(self.wep.get_cur() == 'eternity' and self.count.get() == 1)
        self.storm.set_visible(self.wep.get_cur() == 'storm' and self.count.get() == 1)
        self.trans.set_visible(self.wep.get_cur() == 'trans' and self.count.get() == 1)
        if self.count.get() > 1:
            for o in self.unique_gear:
                o.set_active_options(o.get_all_ids(), False)

        self.set_points(self.build_points(exclude=[self.count.id]))
        self.description.reset()
        self.description.set_header(self.name, self.base_points + self.wep.points() +
                                    reduce(lambda val, o: val + o.points(), self.unique_gear, 0))
        self.description.add(self.wep.get_selected())
        for o in self.unique_gear:
            self.description.add(o.get_selected())
        self.set_description(self.description)

    def get_unique_gear(self):
        return reduce(lambda val, o: val + o.get_all(), self.unique_gear, [])

    def disable_unique_gear(self, all_gear):
        for o in self.unique_gear:
            o.set_active_options(o.get_all_ids(), True)
            for gear_id in o.get_all_ids():
                if gear_id in all_gear:
                    o.set_active_options([gear_id], False)


class Overlord(Unit):
    name = "Necron Overlord"
    base_points = 90

    def add_gear(self):
        self.wep = self.opt_one_of("Weapon", [
            ['Staff of Light', 0],
            ['Hyperphase Sword', 0],
            ['Gauntlet of Fire', 5],
            ['Voidblade', 10],
            ['Warscythe', 10],
        ])
        self.opt = self.opt_options_list("Options", [
            ["Phaeron", 20],
            ["Phylactery", 15],
            ["Mindshackle scarabs", 15],
            ["Sempiternal Weave", 15],
            ["Tesseract labyrinth", 20],
            ["Tachyon Arrow", 30],
            ["Resurrection Orb", 30],
            ["Phase Shifter", 45],
        ])

    def add_description(self):
        self.description.add(self.wep.get_selected())
        self.description.add(self.opt.get_selected())

    def add_points(self):
        return self.wep.points() + self.opt.points()

    def __init__(self):
        Unit.__init__(self)
        self.add_gear()
        self.trans = self.opt_options_list('Transport', [
            [CommandBarge.name, CommandBarge.base_points, 'cb']
        ])
        self.cb_unit = self.opt_sub_unit(CommandBarge())
        self.court = self.opt_options_list('Royal Court', [
            [Lord.name, Lord.base_points, 'nl'],
            [Cryptek.name, Cryptek.base_points, 'ct']
        ])
        self.nl_unit = self.opt_units_list(Lord.name, Lord, 1, 5)
        self.ct_unit = self.opt_units_list(Cryptek.name, Cryptek, 1, 5)

    def check_rules(self):
        self.cb_unit.set_active(self.trans.get('cb'))
        self.nl_unit.set_active(self.court.get('nl'))
        self.ct_unit.set_active(self.court.get('ct'))
        self.nl_unit.update_range(1, 5)
        self.ct_unit.update_range(1, 5)

        opt = reduce(lambda val, o: val + o.get_unique_gear(), self.ct_unit.get_units(), [])
        for unit in self.ct_unit.get_units():
            cur_gear = unit.get_unique_gear()
            unit.disable_unique_gear(reduce(lambda val, o: (val + [o]) if o not in cur_gear else val, opt, []))

        self.points.set(self.base_points + self.cb_unit.points() + self.nl_unit.points() + self.ct_unit.points() +
                        self.add_points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.add_description()
        self.description.add(self.cb_unit.get_selected())
        self.description.add(self.nl_unit.get_selected())
        self.description.add(self.ct_unit.get_selected())
        self.set_description(self.description)


class Overlord_v2(Unit):
    name = "Necron Overlord"
    base_points = 90

    def add_gear(self):
        self.wep = self.opt_one_of("Weapon", [
            ['Staff of Light', 0],
            ['Hyperphase Sword', 0],
            ['Gauntlet of Fire', 5],
            ['Voidblade', 10],
            ['Warscythe', 10],
        ])
        self.opt = self.opt_options_list("Options", [
            ["Phaeron", 20],
            ["Phylactery", 15],
            ["Mindshackle scarabs", 15],
            ["Sempiternal Weave", 15],
            ["Tesseract labyrinth", 20],
            ["Tachyon Arrow", 30],
            ["Resurrection Orb", 30],
            ["Phase Shifter", 45],
        ])

    def add_description(self):
        self.description.add(self.wep.get_selected())
        self.description.add(self.opt.get_selected())

    def add_points(self):
        return self.wep.points() + self.opt.points()

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of("Weapon", [
            ['Staff of Light', 0],
            ['Hyperphase Sword', 0],
            ['Gauntlet of Fire', 5],
            ['Voidblade', 10],
            ['Warscythe', 10],
        ])
        self.opt = self.opt_options_list("Options", [
            ["Phaeron", 20],
            ["Phylactery", 15],
            ["Mindshackle scarabs", 15],
            ["Sempiternal Weave", 15],
            ["Tesseract labyrinth", 20],
            ["Tachyon Arrow", 30],
            ["Resurrection Orb", 30],
            ["Phase Shifter", 45],
        ])
        self.trans = self.opt_optional_sub_unit('Transport', CommandBarge())

    # def check_rules(self):
    #     self.cb_unit.set_active(self.trans.get('cb'))
    #
    #     for unit in self.ct_unit.get_units():
    #         cur_gear = unit.get_unique_gear()
    #         unit.disable_unique_gear(reduce(lambda val, o: (val + [o]) if o not in cur_gear else val, opt, []))
    #
    #     self.points.set(self.base_points + self.cb_unit.points() + self.nl_unit.points() + self.ct_unit.points() +
    #                     self.add_points())
    #     self.description.reset()
    #     self.description.set_header(self.name, self.points.get())
    #     self.add_description()
    #     self.description.add(self.cb_unit.get_selected())
    #     self.description.add(self.nl_unit.get_selected())
    #     self.description.add(self.ct_unit.get_selected())
    #     self.set_description(self.description)


class Imotekh(Overlord):
    name = "Imotekh the Stormlord"
    base_points = 225

    def add_gear(self):
        self.gear = ['Bloodswarm Nanoscarabs', 'Gauntlet of Fire', 'Phase Shifter', 'Phylactery', 'Sempiternal Weave',
                     'Staff of the Destroyer']

    def add_description(self):
        self.description.add(self.gear)

    def add_points(self):
        return 0


class Nemesor(Overlord):
    name = "Nemesor Zahndrekh"
    base_points = 185

    def add_gear(self):
        self.gear = ['Phase Shifter', 'Resurrection Orb', 'Sempiternal Weave', 'Staff of Light']

    def add_description(self):
        self.description.add(self.gear)

    def add_points(self):
        return 0


class Vargard(StaticUnit):
    name = "Vargard Obyron"
    base_points = 160
    gear = ['Ghostwalk mantle', 'Sempiternal Weave', 'Warscythe']


class Illuminor(StaticUnit):
    name = "Illuminor Szeras"
    base_points = 100
    gear = ['Eldritche Lance', 'Gaze of Flame']


class Orikan(StaticUnit):
    name = "Orikan the Diviner"
    base_points = 165
    gear = ['Phase Shifter', 'Staff of Tomorrow', 'Transdimensional Beamer']


class Anrakyr(Overlord):
    name = "Anrakyr the Traveller"
    base_points = 165

    def add_gear(self):
        self.gear = ['Tachyon Arrow', 'Warscythe']

    def add_description(self):
        self.description.add(self.gear)

    def add_points(self):
        return 0


class Trazin(Overlord):
    name = "Trazin the Infinite"
    base_points = 175

    def add_gear(self):
        self.gear = ['Empathic Obliterator',  'Mindshackle scarabs']

    def add_description(self):
        self.description.add(self.gear)

    def add_points(self):
        return 0


class DestroyerLord(Unit):
    name = "Destroyer Lord"
    base_points = 125

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of("Weapon", [
            ['Warscythe', 0],
            ['Staff of Light', 0],
            ['Gauntlet of Fire', 0],
            ['Voidblade', 5],
        ])
        self.opt = self.opt_options_list("Options", [
            ["Sempiternal Weave", 15],
            ["Mindshackle scarabs", 20],
            ["Tachyon Arrow", 30],
            ["Resurrection Orb", 30],
        ])
