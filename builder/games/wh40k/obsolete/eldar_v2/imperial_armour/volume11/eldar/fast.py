__author__ = 'Denis Romanov'

from builder.core2 import *

ia_id = ' (IA vol.11)'


class Spectres(Unit):
    clear_name = 'Shadow Spectres'
    type_name = clear_name + ia_id
    type_id = 'spectres_v1'

    model_gear = [Gear('Jetpack'), Gear('Spectre Holo-field'), Gear('Aspect armour'), Gear('Haywire grenades')]
    min_models = 3
    max_models = 6

    class Exarch(Unit):
        def __init__(self, parent):
            super(Spectres.Exarch, self).__init__(parent=parent, name='Exarch', points=40, gear=Spectres.model_gear)
            self.Weapon(self)
            self.Options(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Spectres.Exarch.Weapon, self).__init__(parent=parent, name='Weapon')
                self.variant('Prism Rifle', 0)
                self.variant('Prism Blaster', 15)
                self.variant('Haywire Launcher', 10)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Spectres.Exarch.Options, self).__init__(parent=parent, name='Warrior powers', limit=2)
                self.variant('Hit & Run', 15)
                self.variant('Night Vision', 5)
                self.variant('Monster Hunter', 10)
                self.variant('Shadow of Death', 20)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(Spectres.Leader, self).__init__(parent, '')
            SubUnit(self, Spectres.Exarch(None))

    def __init__(self, parent):
        super(Spectres, self).__init__(parent, name=self.clear_name)
        self.leader = self.Leader(self)
        self.warriors = Count(self, 'Shadow Spectre', Spectres.min_models, Spectres.max_models, 30,
                              gear=UnitDescription('Shadow Spectre', points=30,
                                                   options=Spectres.model_gear + [Gear('Prism Rifle')]))

    def check_rules(self):
        super(Spectres, self).check_rules()
        self.warriors.min = Spectres.min_models - self.leader.count
        self.warriors.max = Spectres.max_models - self.leader.count

    def get_count(self):
        return self.warriors.cur + self.leader.count


class Wasps(Unit):
    clear_name = 'Wasp Assault Walker Squadron'
    type_name = clear_name + ia_id
    type_id = 'wasps_v1'

    def __init__(self, parent):
        super(Wasps, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Wasp, min_limit=1, max_limit=3)
        self.Options(self)

    class Wasp(ListSubUnit):
        type_name = 'Hornet'
        type_id = 'hornet_v1'

        def __init__(self, parent):
            super(Wasps.Wasp, self).__init__(parent=parent, points=50)
            self.Weapon(self, name='Weapon')
            self.Weapon(self, name='')

        class Weapon(OneOf):
            def __init__(self, parent, name):
                super(Wasps.Wasp.Weapon, self).__init__(parent=parent, name=name)
                self.shurikencannon = self.variant('Shuriken cannon', 5)
                self.scatterlaser = self.variant('Scatter Laser', 15)
                self.eldarmissilelauncher = self.variant('Eldar Missile Launcher', 20)
                self.starcannon = self.variant('Starcannon', 25)
                self.brightlance = self.variant('Bright Lance', 30)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Wasps.Options, self).__init__(parent=parent, name='Options')
            self.variant('Spirit Stones', 5)

        @property
        def points(self):
            return super(Wasps.Options, self).points * self.parent.get_count()

    def get_count(self):
        return self.models.count


class Hornets(Unit):
    clear_name = 'Hornet Squadron'
    type_name = clear_name + ia_id
    type_id = 'hornet_squadron_v1'

    def __init__(self, parent):
        super(Hornets, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Hornet, min_limit=1, max_limit=3)

    class Hornet(ListSubUnit):
        type_name = 'Hornet'
        type_id = 'hornet_v1'

        def __init__(self, parent):
            super(Hornets.Hornet, self).__init__(parent=parent, points=65, gear=[Gear('Star Engines')])
            self.Weapon(self, name='Weapon')
            self.Weapon(self, name='')
            self.Options(self)

        class Weapon(OneOf):
            def __init__(self, parent, name):
                super(Hornets.Hornet.Weapon, self).__init__(parent=parent, name=name)
                self.shurikencannon = self.variant('Shuriken cannon', 0)
                self.scatterlaser = self.variant('Scatter Laser', 10)
                self.eldarmissilelauncher = self.variant('Eldar Missile Launcher', 15)
                self.starcannon = self.variant('Starcannon', 20)
                self.brightlance = self.variant('Bright Lance', 25)
                self.pulselaser = self.variant('Pulse laser', 30)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Hornets.Hornet.Options, self).__init__(parent=parent, name='Options')
                self.holofield = self.variant('Holo-field', 35)
                self.vectorengines = self.variant('Vector Engines', 20)
                self.spiritstones = self.variant('Spirit Stones', 10)

    def get_count(self):
        return self.models.count
