__author__ = 'Denis Romanow'

from builder.games.wh40k.roster import LordsOfWarSection, HQSection, HeavySection, FastSection
from .hq import *
from .fast import *
from .heavy import *
from .superheavy import *


class IA11HQSection(HQSection):
    def __init__(self, parent):
        super(IA11HQSection, self).__init__(parent)

        self.irillyth = UnitType(self, Irillyth)
        self.belannath = UnitType(self, BelAnnath)
        self.wraithseer = UnitType(self, Wraithseer)

        self.ia_units.extend([self.irillyth, self.belannath, self.wraithseer])


class IA11FastSection(FastSection):
    def __init__(self, parent):
        super(IA11FastSection, self).__init__(parent)
        self.spectres = UnitType(self, Spectres)
        self.ia_units.extend([
            self.spectres,
            UnitType(self, Wasps),
            UnitType(self, Hornets),
        ])


class IA11HeavySection(HeavySection):
    def __init__(self, parent):
        super(IA11HeavySection, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, WarpHunter),
        ])


class IA11LordsOfWar(LordsOfWarSection):
    def __init__(self, parent):
        super(IA11LordsOfWar, self).__init__(parent)
        self.ia_units.extend([
            UnitType(self, Lynx),
            UnitType(self, Scorpion),
            UnitType(self, Cobra),
            UnitType(self, VampireRaider),
            UnitType(self, VampireHunter),
            UnitType(self, Revenant),
            UnitType(self, Phantom),
        ])
