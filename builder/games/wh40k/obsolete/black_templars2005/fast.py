__author__ = 'Denis Romanov'

from builder.core.unit import Unit, ListSubUnit, ListUnit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor


class AssaultSquad(Unit):
    name = 'Assault squad'

    def get_count(self):
        return self.marines.get_count()

    class Initiate(ListSubUnit):
        name = 'Initiate'
        gear = ['Power armor', 'Frag grenades', 'Jump pack']
        base_points = 22

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.ccw = self.opt_one_of('Weapon', [
                ['Close combat weapon', 0, 'ccw'],
                ['Power weapon', 10, 'psw'],
                ['Power fist', 15, 'pfist'],
            ])
            self.rng = self.opt_one_of('', [
                ['Bolt pistol', 0],
                ['Plasma pistol', 5, 'pp'],
                ['Flamer', 6, 'fl'],
                ['Storm shied', 3]
            ])
            self.opt = None

        def set_bombs(self, opt):
            self.opt = opt

        def get_spec(self):
            return self.get_count() if self.rng.get_cur() == 'pp' or self.rng.get_cur() == 'fl' else 0

        def get_ccw(self):
            return self.get_count() if self.ccw.get_cur() != 'ccw' else 0

        def check_rules(self):
            if not self.opt:
                return
            single_points = self.build_points(exclude=[self.count.id], count=1) + self.opt.points()
            self.set_points(single_points * self.get_count())
            self.build_description(points=single_points, count=1, exclude=[self.count.id])
            self.description.add(self.opt.get_selected())

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_units_list('', self.Initiate, 5, 10)
        self.opt = self.opt_options_list('Options', [
            ['Krak grenades', 1, 'krak'],
            ["Melta bombs", 2, 'mbomb'],
        ])

    def check_rules(self):
        self.marines.update_range()
        for unit in self.marines.get_units():
            unit.set_bombs(self.opt)
            unit.check_rules()

        spec = sum((u.get_ccw() for u in self.marines.get_units()))
        if spec > 1:
            self.error('Assault squad may take only one 1 special close combat weapon (taken: {0})'.format(spec))

        spec = sum((u.get_spec() for u in self.marines.get_units()))
        if spec > 2:
            self.error('Assault squad may take not more then 2 special weapon (taken: {0})'.format(spec))

        self.set_points(self.build_points(count=1, exclude=[self.opt.id]))
        self.build_description(count=1, exclude=[self.opt.id])


class LandSpeederSquadron(ListUnit):
    name = "Land Speeder Squadron"

    class LandSpeeder(ListSubUnit):
        name = "Land Speeder"
        base_points = 50
        max = 3

        def __init__(self):
            ListSubUnit.__init__(self, max_models=3)
            self.wep = self.opt_one_of('Weapon', [
                ["Heavy Bolter", 0, "bolt"],
                ["Multi-melta", 15, "melta"],
            ])
            self.up = self.opt_options_list('Upgrade', [
                ["Typhoon Missile Launcher", 20, "tml"],
                ["Heavy Flamer", 10, "flame"],
                ["Assault Cannon", 30, "assault"],
            ], limit=1)

        def check_rules(self):
            self.up.set_visible_options(['assault'], self.wep.get_cur() == 'bolt')
            self.up.set_visible_options(['flame'], self.wep.get_cur() == 'melta')
            self.name = "Land Speeder"
            if self.up.get('tml'):
                self.name += ' Typhoon'
            elif self.up.get("flame") or self.up.get("assault"):
                self.name += ' Tornado'
            ListSubUnit.check_rules(self)

    unit_class = LandSpeeder


class AttackBikeSquad(Unit):
    name = "Attack Bike squad"

    def __init__(self):
        Unit.__init__(self)
        self.bikers = self.opt_count('Attack bikes', 2, 3, 40)
        self.mm = self.opt_count('Multi-melta', 0, 2, 15)

    def check_rules(self):
        self.mm.update_range(0, self.bikers.get())
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        desc = ModelDescriptor(name='Attack Bike', gear=['Power armour', 'Twin-linked bolter', 'Frag grenades'],
                               points=50)
        self.description.add(desc.clone().add_gear('Multi-melta', points=15).build(self.mm.get()))
        self.description.add(desc.clone().add_gear('Heavy bolter').build(self.bikers.get() - self.mm.get()))

    def get_count(self):
        return self.bikers.get()


class SpaceMarineBikers(Unit):
    name = 'Space Marine Bike squad'
    base_gear = ['Power armour', 'Twin linked bolter', 'Frag grenades']

    class AttackBike(Unit):
        name = "Attack bike"
        base_points = 50
        gear = ['Power armour', 'Twin linked bolter', 'Frag grenades']

        def __init__(self, opt):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Heavy bolter', 0, 'hbgun'],
                ['Multi-melta', 15, 'mmelta']
            ])
            self.opt = opt

        def check_rules(self):
            self.set_points(self.build_points() + self.opt.points())
            self.build_description(options=[self.wep, self.opt])

    def __init__(self):
        Unit.__init__(self)
        self.bikers = self.opt_count('Space Marine Biker', 3, 5, 32)

        self.spec = [
            self.opt_count('Flamer', 0, 3, 3),
            self.opt_count('Meltagun', 0, 3, 10),
            self.opt_count('Plasma gun', 0, 3, 6),
            self.opt_count('Power weapon', 0, 3, 10)
        ]
        self.opt = self.opt_options_list('', [
            ['Krak grenades', 1, 'krak']
        ])
        self.abike = self.opt_optional_sub_unit('', [self.AttackBike(self.opt)])

    def get_count(self):
        return self.bikers.get() + self.abike.get_count()

    def check_rules(self):
        norm_counts(0, 3, self.spec)
        self.set_points(self.build_points(count=1, exclude=[self.opt.id]) + self.opt.points() * self.bikers.get())
        self.build_description(options=[self.abike], count=self.bikers.get())
        desc = ModelDescriptor('Initiate biker', points=32, gear=self.base_gear).add_gear_opt(self.opt)
        count = self.bikers.get()
        for o in self.spec:
            self.description.add(desc.clone().add_gear(o.name, o.unit_points).build(o.get()))
            count -= o.get()
        self.description.add(desc.clone().add_gear('Bolt pistol').build(count))
