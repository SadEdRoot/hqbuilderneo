__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit, ListUnit
from builder.core.model_descriptor import ModelDescriptor


class Spectres(Unit):
    name = 'Shadow Spectres'
    base_gear = ['Prism Rifle', 'Jetpack', 'Spectre Holo-fields', 'Aspect armour', 'Haywire grenades']
    min_models = 3
    max_models = 6

    class Exarch(Unit):
        name = 'Exarch'
        base_points = 30 + 10
        gear = ['Jetpack', 'Spectre Holo-fields', 'Heavy aspect armour', 'Haywire grenades']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Prism Rifle', 0, 'prifle'],
                ['Prism Blaster', 15, 'pblast'],
                ['Haywire Launcher', 10, 'hwl']
            ])

            self.opt = self.opt_options_list('Warrior powers', [
                ['Hit & Run', 15, 'hr'],
                ['Night Vision', 5, 'nv'],
                ['Monster Hunter', 10, 'mons'],
                ['Shadow of Death', 20, 'sod']
            ], limit=2)

    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Shadow Spectre', self.min_models, self.max_models, 30)
        self.leader = self.opt_optional_sub_unit(self.Exarch.name, self.Exarch())

    def check_rules(self):
        ldr = self.leader.get_count()
        self.warriors.update_range(self.min_models - ldr, self.max_models - ldr)
        self.set_points(self.build_points(count=1))
        self.build_description(options=[self.leader])
        self.description.add(ModelDescriptor('Shadow Spectre', self.warriors.get(), 30, self.base_gear).build())

    def get_count(self):
        return self.warriors.get() + self.leader.get_count()


class Wasps(Unit):
    name = 'Wasp Assault Walker Squadron'
    base_points = 50

    class Wasp(ListSubUnit):
        name = 'Wasp'
        start_points = 40
        weplist = [
            ['Shuriken cannon', 5, 'scan'],
            ['Scatter Laser', 15, 'scat'],
            ['Eldar Missile Launcher', 20, 'eml'],
            ['Starcannon', 25, 'scan'],
            ['Bright Lance', 30, 'blnc']
        ]
        has_stones = False

        def set_stones(self, val):
            self.has_stones = val

        def __init__(self):
            ListSubUnit.__init__(self, max_models=3)
            self.wep1 = self.opt_one_of('Weapon', self.weplist)
            self.wep2 = self.opt_one_of('', self.weplist)

        def check_rules(self):
            self.gear = ['Spirit Stones'] if self.has_stones else []
            self.base_points = self.start_points + (5 if self.has_stones else 0)
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.waw = self.opt_units_list(self.Wasp.name, self.Wasp, 1, 3)
        self.opt = self.opt_options_list('Options', [['Spirit Stones', 5, 'ss']])

    def get_count(self):
        return self.waw.get_count()

    def check_rules(self):
        self.waw.update_range()
        for wasp in self.waw.get_units():
            wasp.set_stones(self.opt.get('ss'))
        self.set_points(self.build_points(count=1, base_points=0, options=[self.waw]))
        self.build_description(count=1, options=[self.waw])


class Hornets(ListUnit):
    name = 'Hornet Squadron'

    class Hornet(ListSubUnit):
        name = 'Hornet'
        gear = ['Star Engines']
        base_points = 65
        weplist = [
            ['Shuriken cannon', 0, 'scan'],
            ['Scatter Laser', 10, 'scat'],
            ['Eldar Missile Launcher', 15, 'eml'],
            ['Starcannon', 20, 'scan'],
            ['Bright Lance', 25, 'blnc'],
            ['Pulse laser', 30, 'plas']
        ]

        def __init__(self):
            ListSubUnit.__init__(self, max_models=3)
            self.wep1 = self.opt_one_of('Weapon', self.weplist)
            self.wep2 = self.opt_one_of('', self.weplist)
            self.opt = self.opt_options_list('Options', [
                ['Holo-field', 35, 'hf'],
                ['Vector Engines', 20, 'veng'],
                ['Spirit Stones', 10, 'ss']
            ])

    def __init__(self):
        ListUnit.__init__(self, self.Hornet, 1, 3)
