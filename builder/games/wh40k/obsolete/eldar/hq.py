
__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit, ListUnit
from builder.core import get_ids


class Eldrad(StaticUnit):
    name = 'Eldrad Ulthran'
    base_points = 205
    gear = ['Shuriken pistol', 'Witchblade', 'Ghosthelm', 'Runes of warding', 'Runes of witnessing',
            'Armour of the Last Runes' 'Staff of Ulthamar']


class Uriel(StaticUnit):
    name = 'Prince Yriel'
    base_points = 140
    gear = ['Heavy Aspect armour', 'Forcefield', 'Plasma grenades', 'The Eye of Wrath', 'The Spear of Twilight']


class Illic(StaticUnit):
    name = "Illic Nightspear"
    base_points = 140
    gear = ['Mesh armour', 'Shuriken pistol', 'Power sword', 'Voidbringer']


class Asurmen(StaticUnit):
    name = "Asurmen"
    base_points = 220
    gear = ['Phoenix armour', 'Twin-linked avenger shuriken catapult', 'The Sword of Asur']


class JainZar(StaticUnit):
    name = "Jain Zar"
    base_points = 200
    gear = ['Phoenix armour', 'Mask of Jain Zar', 'Blade of Destruction', 'The Silent Death']


class Karandras(StaticUnit):
    name = 'Karandras'
    base_points = 230
    gear = ['Phoenix armour', 'The Scorpion\'s Bite', 'Scorpion chainsword', 'Scorpion\'s claw', 'Plasma grenades']


class Fuegan(StaticUnit):
    name = 'Fuegan'
    base_points = 220
    gear = ['Phoenix armour', 'Firepike', 'Fire Axe', 'Melta bombs']


class Baharroth(StaticUnit):
    name = 'Baharroth'
    base_points = 195
    gear = ['Phoenix armour', 'Swooping Hawk wings', 'Hawk\'s talon', 'Haywire grenades', 'Plasma grenades',
            'Grenade pack', 'The Shining Blade']


class Maugan(StaticUnit):
    name = 'Maugan Ra'
    base_points = 195
    gear = ['Phoenix armour', 'The Maugetar']


class Avatar(Unit):
    name = 'The Avatar of Khaine'
    base_points = 195
    gear = ['The Wailing Doom']

    def __init__(self):
        Unit.__init__(self)
        self.opt_options_list('Options', [
            ['Night Vision', 5], 
            ['Crushing Blow', 10], 
            ['Fast Shot', 10], 
            ['Monster Hunter', 10], 
            ['Marksman\'s Eye', 15], 
            ['Disarm Strike', 20], 
        ], limit=2)

gifts = [
    ['The Celestial Lance', 35, 'lance'],
    ['Soulshrive', 30, 'ss'],
]
gifts_ids = get_ids(gifts)

psyker_gifts = gifts + [
    ['Spear of Teuthlas', 15, 'spear'],
]

psyker_gifts_ids = get_ids(psyker_gifts)

any_gifts = [
    ['The Wraithforge Stone', 30, 'wstone'],
    ['Guardian Helm of Xellethon', 15, 'helm'],
]
any_gifts_ids = get_ids(any_gifts)

remnants = [
    ['Uldanorethi Long Rifle', 25, 'rifle'],
    ['Faolchu\'s Wing', 30, 'wing'], 
    ['Firesabre', 30, 'sabre'], 
    ['Shard of Anaris', 40, 'shard'],
]

remnants_ids = get_ids(remnants)

any_remnants = [
    ['The Phoenix Gem', 25, 'gem'],
    ['Mantle of the Laughing God', 40, 'man'],
]
any_remnants_ids = get_ids(any_remnants)

seer_remnants = [['The Spirit Stone of Anath\'lan', 15, 'stone']] + any_remnants
seer_remnants_ids = get_ids(seer_remnants)


class Autarch(Unit):
    name = 'Autarch'
    base_points = 70
    gear = ['Plasma grenades', 'Haywire grenades', 'Forcefield', 'Heavy Aspect armour']

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Options', [
            ['Swooping Hawk wings', 15, 'wing'], 
            ['Warp jump generator', 15, 'jump'], 
            ['Eldar jetbike', 15, 'bike'],
            ['Shadow Spectre jet pack and Spectre holo-field', 20, 'shsp'],
        ], limit=1)
        self.face = self.opt_options_list('', [
            ['Banshee mask', 5, 'mask'], 
            ['Mandiblasters', 10, 'blast']
        ], limit=1)
        self.wep1 = self.opt_options_list('Weapon', [
            ['Scorpion chainsword', 3, 'sch'], 
            ['Avenger shuriken catapult', 5, 'asc'], 
            ['Lasblaster', 5, 'lblas'], 
            ['Deathspinner', 10, 'dsp'], 
            ['Fusion gun', 10, 'fgun'], 
            ['Laser lance', 10, 'lance'], 
            ['Power weapon', 15, 'pwep'], 
            ['Reaper launcher with starswarm missiles', 25, 'rlnch'], 
            ['Prism rifle', 10, 'prism'],
        ], limit=2)
        self.wep2 = self.opt_one_of('', [['Shuriken pistol', 0, 'pistol']] + remnants)
        self.rem = self.opt_options_list('', any_remnants)

    def check_rules(self):
        self.wep1.set_active_options(['lance'], self.ride.get('bike'))
        self.wep1.set_active_options(['prism'], self.get_roster().has_shadow())
        self.ride.set_active_options(['shsp'], self.get_roster().has_shadow())
        Unit.check_rules(self)

    def get_unique_gear(self):
        return ([self.wep2.get_selected()] if self.wep2.get_cur() in remnants_ids else []) + self.rem.get_selected()


class IyandenAutarch(Unit):
    name = 'Autarch'
    base_points = 70
    gear = ['Plasma grenades', 'Haywire grenades', 'Forcefield', 'Heavy Aspect armour']

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Options', [
            ['Swooping Hawk wings', 15, 'wing'],
            ['Warp jump generator', 15, 'jump'],
            ['Eldar jetbike', 15, 'bike'],
            ['Shadow Spectre jet pack and Spectre holo-field', 20, 'shsp'],
        ], limit=1)
        self.face = self.opt_options_list('', [
            ['Banshee mask', 5, 'mask'],
            ['Mandiblasters', 10, 'blast']
        ], limit=1)
        self.wep1 = self.opt_options_list('Weapon', [
            ['Scorpion chainsword', 3, 'sch'],
            ['Avenger shuriken catapult', 5, 'asc'],
            ['Lasblaster', 5, 'lblas'],
            ['Deathspinner', 10, 'dsp'],
            ['Fusion gun', 10, 'fgun'],
            ['Laser lance', 10, 'lance'],
            ['Power weapon', 15, 'pwep'],
            ['Reaper launcher with starswarm missiles', 25, 'rlnch'],
            ['Prism rifle', 10, 'prism'],
        ], limit=2)
        self.wep2 = self.opt_one_of('', [['Shuriken pistol', 0, 'pistol']] + remnants + gifts)
        self.rem = self.opt_options_list('', any_remnants + any_gifts)

    def check_rules(self):
        self.wep1.set_active_options(['lance'], self.ride.get('bike'))
        self.wep1.set_active_options(['prism'], self.get_roster().has_shadow())
        self.ride.set_active_options(['shsp'], self.get_roster().has_shadow())

        remnants = self.wep2.get_cur() in remnants_ids or \
            any([self.rem.is_selected(rem_id) for rem_id in any_remnants_ids])
        gifts = self.wep2.get_cur() in psyker_gifts_ids or \
            any([self.rem.is_selected(gift_id) for gift_id in any_gifts_ids])

        self.rem.set_active_all(True)
        self.wep2.set_active_all(True)
        if remnants:
            self.wep2.set_active_options(gifts_ids, False)
            self.rem.set_active_options(any_gifts_ids, False)
        if gifts:
            self.wep2.set_active_options(remnants_ids, False)
            self.rem.set_active_options(any_remnants_ids, False)

        Unit.check_rules(self)

    def get_unique_gear(self):
        return ([self.wep2.get_selected()] if self.wep2.get_cur() in remnants_ids or
            self.wep2.get_cur() in gifts_ids else []) + self.rem.get_selected()


class Farseer(Unit):
    name = 'Farseer'
    base_points = 100
    gear = ['Ghosthelm', 'Rune armour']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Witchblade', 0, 'wsw'], 
            ['Singing spear', 5, 'ssp']
        ] + remnants)
        self.wep2 = self.opt_one_of('', [['Shuriken pistol', 0, 'pistol']] + remnants)
        self.opt = self.opt_options_list('Options', [
            ['Runes of warding', 10, 'rwr'],
            ['Runes of witnessing', 15, 'rwt'], 
            ['Eldar jetbike', 15, 'bike']
        ])
        self.rem = self.opt_options_list('', seer_remnants)

    def check_rules(self):
        for item_id in remnants_ids:
            self.wep1.set_active_options([item_id], self.wep2.get_cur() != item_id)
            self.wep2.set_active_options([item_id], self.wep1.get_cur() != item_id)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return \
            ([self.wep1.get_selected()] if self.wep1.get_cur() in remnants_ids else []) + \
            ([self.wep2.get_selected()] if self.wep2.get_cur() in remnants_ids else []) + \
            self.rem.get_selected()


class IyandenFarseer(Unit):
    name = 'Farseer'
    base_points = 100
    gear = ['Ghosthelm', 'Rune armour']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Witchblade', 0, 'wsw'],
            ['Singing spear', 5, 'ssp']
        ] + remnants + psyker_gifts)
        self.wep2 = self.opt_one_of('', [['Shuriken pistol', 0, 'pistol']] + remnants + psyker_gifts)
        self.opt = self.opt_options_list('Options', [
            ['Runes of warding', 10, 'rwr'],
            ['Runes of witnessing', 15, 'rwt'],
            ['Eldar jetbike', 15, 'bike']
        ])
        self.rem = self.opt_options_list('', seer_remnants + any_gifts)

    def check_rules(self):
        remnants = self.wep1.get_cur() in remnants_ids or self.wep2.get_cur() in remnants_ids or \
            any([self.rem.is_selected(rem_id) for rem_id in seer_remnants_ids])
        gifts = self.wep1.get_cur() in psyker_gifts_ids or self.wep2.get_cur() in psyker_gifts_ids or \
            any([self.rem.is_selected(gift_id) for gift_id in any_gifts_ids])

        for item_id in remnants_ids + psyker_gifts_ids:
            self.wep1.set_active_options([item_id], self.wep2.get_cur() != item_id)
            self.wep2.set_active_options([item_id], self.wep1.get_cur() != item_id)
        self.rem.set_active_all(True)
        if remnants:
            self.wep1.set_active_options(psyker_gifts_ids, False)
            self.wep2.set_active_options(psyker_gifts_ids, False)
            self.rem.set_active_options(any_gifts_ids, False)
        if gifts:
            self.wep1.set_active_options(remnants_ids, False)
            self.wep2.set_active_options(remnants_ids, False)
            self.rem.set_active_options(seer_remnants_ids, False)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return \
            ([self.wep1.get_selected()] if self.wep1.get_cur() is not 'wsw' and
                self.wep1.get_cur() is not 'ssp' else []) + \
            ([self.wep2.get_selected()] if self.wep2.get_cur() is not 'pistol' else []) + \
            self.rem.get_selected()


class Spiritseer(Unit):
    name = 'Spiritseer'
    base_points = 70
    gear = ['Rune armour']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [['Witch staff', 0, 'wsw']] + remnants)
        self.wep2 = self.opt_one_of('', [['Shuriken pistol', 0, 'pistol']] + remnants)
        self.rem = self.opt_options_list('', seer_remnants)

    def check_rules(self):
        for item_id in remnants_ids:
            self.wep1.set_active_options([item_id], self.wep2.get_cur() != item_id)
            self.wep2.set_active_options([item_id], self.wep1.get_cur() != item_id)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return \
            ([self.wep1.get_selected()] if self.wep1.get_cur() in remnants_ids else []) + \
            ([self.wep2.get_selected()] if self.wep2.get_cur() in remnants_ids else []) + \
            self.rem.get_selected()


class IyandenSpiritseer(Unit):
    name = 'Spiritseer'
    base_points = 70
    gear = ['Rune armour']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [['Witch staff', 0, 'wsw']] + remnants + psyker_gifts)
        self.wep2 = self.opt_one_of('', [['Shuriken pistol', 0, 'pistol']] + remnants + psyker_gifts)
        self.rem = self.opt_options_list('', seer_remnants + any_gifts)

    def check_rules(self):
        remnants = self.wep1.get_cur() in remnants_ids or self.wep2.get_cur() in remnants_ids or \
            any([self.rem.is_selected(rem_id) for rem_id in seer_remnants_ids])
        gifts = self.wep1.get_cur() in psyker_gifts_ids or self.wep2.get_cur() in psyker_gifts_ids or \
            any([self.rem.is_selected(gift_id) for gift_id in any_gifts_ids])

        for item_id in remnants_ids + psyker_gifts_ids:
            self.wep1.set_active_options([item_id], self.wep2.get_cur() != item_id)
            self.wep2.set_active_options([item_id], self.wep1.get_cur() != item_id)
        self.rem.set_active_all(True)
        if remnants:
            self.wep1.set_active_options(psyker_gifts_ids, False)
            self.wep2.set_active_options(psyker_gifts_ids, False)
            self.rem.set_active_options(any_gifts_ids, False)
        if gifts:
            self.wep1.set_active_options(remnants_ids, False)
            self.wep2.set_active_options(remnants_ids, False)
            self.rem.set_active_options(seer_remnants_ids, False)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return \
            ([self.wep1.get_selected()] if self.wep1.get_cur() is not 'wsw' else []) + \
            ([self.wep2.get_selected()] if self.wep2.get_cur() is not 'pistol' else []) + \
            self.rem.get_selected()


class Warlocks(ListUnit):
    name = 'Warlocks'
    unique = True

    class Warlock(ListSubUnit):
        base_points = 35
        gear = ['Rune armour', 'Shuriken pistol']
        name = 'Warlock'

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.wep = self.opt_one_of('Weapon', [
                ['Witchblade', 0, 'wsw'], 
                ['Singing spear', 5, 'ssp']
            ])
            self.opt = self.opt_options_list('Options', [
                ['Eldar jetbike', 15, 'bike']
            ])

    def __init__(self):
        ListUnit.__init__(self, self.Warlock, 1, 10)

