__author__ = 'Denis Romanov'
from builder.core2 import Unit, OptionsList, Gear, OneOf, Count, SubUnit, UnitDescription
from .armory import Vehicle
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle


class BaseLandRaider(SpaceMarinesBaseVehicle):

    class Deathwing(OptionsList):
        def __init__(self, parent, forced=False):
            super(BaseLandRaider.Deathwing, self).__init__(parent, 'Deathwing')
            self.dw = self.variant('Deathwing Vehicle', 30)
            if forced:
                self.dw.active = False
                self.dw.value = True

    def __init__(self, parent, points, gear, deathwing=False):
        super(BaseLandRaider, self).__init__(parent=parent, points=points, gear=gear, tank=True, transport=True)
        self.dw = self.Deathwing(self, forced=deathwing)
        self.opt = Vehicle(self, melta=True)


class LandRaider(BaseLandRaider):
    type_id = 'land_raider_v1'
    type_name = "Land Raider"

    def __init__(self, parent, deathwing=False):
        super(LandRaider, self).__init__(parent=parent, deathwing=deathwing, points=250, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Twin-linked lascannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ])


class LandRaiderCrusader(BaseLandRaider):
    type_id = 'land_raider_crusader_v1'
    type_name = "Land Raider Crusader"

    def __init__(self, parent, deathwing=False):
        super(LandRaiderCrusader, self).__init__(parent=parent, deathwing=deathwing, points=250, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Hurricane bolter', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher')
        ])


class LandRaiderRedeemer(BaseLandRaider):
    type_id = 'land_raider_redeemer_v1'
    type_name = "Land Raider Redeemer"

    def __init__(self, parent, deathwing=False):
        super(LandRaiderRedeemer, self).__init__(parent=parent, deathwing=deathwing, points=245, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Flamestorm cannon', count=2),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
            Gear('Frag Assault Launcher'),
        ])


class Predator(SpaceMarinesBaseVehicle):
    type_name = "Predator"
    type_id = "predator_v1"

    class Turret(OneOf):
        def __init__(self, parent):
            super(Predator.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant('Autocannon', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 25)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(Predator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.sponsonswithheavybolters = self.variant('Sponsons with heavy bolters', 20,
                                                         gear=Gear('Heavy bolter', count=2))
            self.sponsonswithlascannons = self.variant('Sponsons with lascannons', 40,
                                                       gear=Gear('Lascannon', count=2))

    def __init__(self, parent):
        super(Predator, self).__init__(parent=parent, points=75, gear=[
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.opt = Vehicle(self)


class Whirlwind(SpaceMarinesBaseVehicle):
    type_name = "Whirlwind"
    type_id = "Whirlwind_v1"

    def __init__(self, parent):
        super(Whirlwind, self).__init__(parent=parent, points=65, gear=[
            Gear('Whirlwind multiple missile launcher'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self)


class Vindicator(SpaceMarinesBaseVehicle):
    type_name = "Vindicator"
    type_id = "vindicator_v1"

    def __init__(self, parent):
        super(Vindicator, self).__init__(parent=parent, points=125, gear=[
            Gear('Demolisher cannon'),
            Gear('Storm bolter'),
            Gear('Smoke launchers'),
            Gear('Searchlight'),
        ], tank=True)
        self.opt = Vehicle(self, shield=True)


class LandSpeederVengeance(SpaceMarinesBaseVehicle):
    type_name = "Land Speeder Vengeance"
    type_id = "land_speeder_vengeance_v1"

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LandSpeederVengeance.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.assaultcannon = self.variant('Assault cannon', 20)

    def __init__(self, parent):
        super(LandSpeederVengeance, self).__init__(parent=parent, points=140, gear=[Gear('Plasma storm battery')],
                                                   tank=True)
        self.weapon = self.Weapon(self)


class Devastators(Unit):
    type_name = 'Devastator Squad'
    type_id = 'devastators_v1'

    model_points = 14
    model_gear = [Gear('Power armor'), Gear('Frag grenades'), Gear('Krak grenades')]

    def __init__(self, parent):
        super(Devastators, self).__init__(parent=parent)
        from .troops import TacticalSquad
        from .transport import Transport
        self.sergeant = SubUnit(parent=self, unit=TacticalSquad.Sergeant(None, signum=True))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.hb = Count(parent=self, name='Heavy bolter', min_limit=0, max_limit=4, points=10)
        self.mm = Count(parent=self, name='Multi-melta', min_limit=0, max_limit=4, points=10)
        self.ml = Count(parent=self, name='Missile launcher', min_limit=0, max_limit=4, points=15)
        self.flakk = Count(self, name='Flakk missiles', min_limit=0, max_limit=4, points=10)
        self.pc = Count(parent=self, name='Plasma cannon', min_limit=0, max_limit=4, points=15)
        self.las = Count(parent=self, name='Lascannon', min_limit=0, max_limit=4, points=20)
        self.heavy = [self.hb, self.mm, self.ml, self.pc, self.las]
        self.transport = Transport(parent=self)

    def check_rules(self):
        Count.norm_counts(0, 4, self.heavy)
        self.flakk.max = self.ml.cur

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=Devastators.model_gear + [Gear('Bolt pistol')],
            points=Devastators.model_points
        )
        count = self.marines.cur
        for o in [self.hb, self.mm, self.pc, self.las]:
            if o.cur:
                spec_marine = marine.clone()
                spec_marine.points += o.option_points
                spec_marine.add(Gear(o.name))
                spec_marine.count = o.cur
                desc.add(spec_marine)
                count -= o.cur

        if self.ml.cur:
            ml_marine = marine.clone()
            ml_marine.points += self.ml.option_points
            ml_marine.add(Gear(self.ml.name))
            ml_marine.count = self.ml.cur - self.flakk.cur
            desc.add(ml_marine)
            count -= self.ml.cur
            if self.flakk.cur:
                ml_marine.points += self.flakk.option_points
                ml_marine.add(Gear(self.flakk.name))
                ml_marine.count = self.flakk.cur
                desc.add(ml_marine)

        marine.add(Gear('Boltgun'))
        marine.count = count
        desc.add(marine)
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1
