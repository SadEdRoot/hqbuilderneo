__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.battle_sisters2011.troops import Rhino,Immolator
from functools import reduce

class Preacher(Unit):
    name = 'Eccleiarchy Preacher'
    base_points = 45
    gear = ['Flak armour','Frag grenades','Krak grenades','Rosarius']
    def __init__(self):
        Unit.__init__(self)
        weaponlist = [
            ['Boltgun',2,'bgun'],
            ['Bolt pistol',2,'bpist'],
            ['Shotgun',2,'sgun'],
            ['Storm Bolter',3,'sbgun'],
            ['Power sword',10,'psw'],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Condemnor boltgun',15,'cbgun'],
            ['Plasma pistol',15,'ppist'],
            ['Evisecrator',25,'evsc']
        ]
        self.wep1 = self.opt_one_of('Weapon', [['Laspistol',0,'lpist']] + weaponlist)
        self.wep2 = self.opt_one_of('Weapon',[['Chainsword',0,'chsw']] + weaponlist)
        self.opt = self.opt_options_list('Options',[
            ['Melta bombs',5,'mbomb']
        ])

class Celestians(Unit):
    name = 'Celestian Squad'

    class Celestian(ListSubUnit):
        max = 9
        name = 'Celestian'
        base_points = 15
        gear = ['Power armour','Frag grenades','Krak grenades','Bolt pistol']
        def __init__(self):
            ListSubUnit.__init__(self)
            self.hlist_base = [
                ['Storm Bolter',3,'sbgun'],
                ['Flamer', 5, 'flame' ],
                ['Meltagun', 10, 'mgun' ]
            ]
            self.hlist_add = [
                ['Heavy bolter', 5,'hbgun'],
                ['Multi-melta',10, 'mmgun'],
                ['Heavy flamer',20,'hflame']
            ]
            self.wep = self.opt_one_of('Weapon', [['Boltgun',0,'bgun']] + self.hlist_base + self.hlist_add)
            self.flag = self.opt_options_list('Options',[['Simulacrum imperialis',20,'simp']])
            self.have_flag = False
            self.have_hvy1 = False
            self.have_hvy2 = False
        def has_hvy1(self):
            return any(self.wep.get_cur() == w[2] for w in self.hlist_base)
        def has_hvy2(self):
            return any(self.wep.get_cur() == w[2] for w in self.hlist_add)
        def check_rules(self):
            if self.wep.get_cur() == 'bgun':
                self.wep.set_active_options([w[2] for w in self.hlist_base], (not self.have_hvy1) or (not self.have_hvy2))
                self.wep.set_active_options([w[2] for w in self.hlist_add], not self.have_hvy2)
            if not self.flag.get('simp'):
                self.flag.set_active_options(self.flag.get_all_ids(), not self.have_flag)
            ListSubUnit.check_rules(self)
    class Superior(Unit):
        name = 'Celestian Superior'
        gear = ['Power armour','Frag grenades','Krak grenades']
        base_points = 80 - 15 * 4
        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Chainsword',0,'chsw'],
                ['Storm Bolter',3,'sbgun'],
                ['Power sword',10,'psw'],
                ['Combi-melta', 10, 'cmelta' ],
                ['Combi-flamer', 10, 'cflame' ],
                ['Combi-plasma', 10, 'cplasma' ],
                ['Condemnor boltgun',15,'cbgun'],
                ['Plasma pistol',15,'ppist']
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Boltgun',0,'bgun']] + weaponlist)
            self.wep2 = self.opt_one_of('Weapon',[['Bolt pistol',0,'bpist']] + weaponlist)
            self.opt = self.opt_options_list('Options',[
                ['Melta bombs',5,'mbomb']
            ])
    def __init__(self):
        Unit.__init__(self)
        self.squad = self.opt_units_list(self.Celestian.name,self.Celestian,4,9)
        self.sup = self.opt_sub_unit(self.Superior())
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')
    def check_rules(self):
        self.squad.update_range()
        flag = reduce(lambda val, c: val + (1 if c.flag.get('simp') else 0), self.squad.get_units(), 0)
        if flag > 1:
            self.error('Only one Simulacrum imperialis can be carried by squad of Celestians')
        h1 = reduce(lambda val, c: val + (1 if c.has_hvy1() else 0), self.squad.get_units(), 0)
        h2 = reduce(lambda val, c: val + (1 if c.has_hvy2() else 0), self.squad.get_units(), 0)
        if h2 > 1:
            self.error('Only one Celestian can carry heavy bolter, multi-melta or heavy flamer')
        if h1 + h2 > 2:
            self.error('No more then 2 special or heavy weapons can be taken by Celestians')
        for c in self.squad.get_units():
            c.have_flag = (flag > 0)
            self.have_hvy1 = h1 > 0
            self.have_hvy2 = h2 > 0 or h1 > 1
            c.check_rules()
        self.points.set(self.build_points(count=1))
        self.build_description(count=1)
    def get_count(self):
        return self.squad.get_count() + 1

class Repentia(Unit):
    name = 'Repentia squad'
    gear = ['Evisecrator']
    class Mistress(Unit):
        base_points = 90 - 4 * 17
        name = 'Mistress of Repentance'
        gear = ['Power armour','Frag grenades','Krak grenades','2 Neural whips']
        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options',[
                ['Melta bombs',5,'mbomb']
            ], )
    def __init__(self):
        Unit.__init__(self)
        self.warriors = self.opt_count('Sister Repentia',4,9,17)
        self.leader = self.opt_sub_unit(self.Mistress())

    def check_rules(self):

        self.points.set(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.warriors.get() + 1
