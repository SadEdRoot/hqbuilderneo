__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.obsolete.battle_sisters2011.hq import *
from builder.games.wh40k.obsolete.battle_sisters2011.elites import *
from builder.games.wh40k.obsolete.battle_sisters2011.troops import *
from builder.games.wh40k.obsolete.battle_sisters2011.fast import *
from builder.games.wh40k.obsolete.battle_sisters2011.heavy import *


class BattleSisters2011(LegacyWh40k):
    army_id = '7ab3ad71cac24317b8fa02f9ba44ebf7'
    army_name = 'Sisters of Battle (2011)'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(self,
            hq = [ Celestine,Canoness,{'unit': CommandSquad, 'active': False},Kyrinov,Jacobus,Confessor,{'unit': Conclave, 'active': False} ],
            elites = [Preacher,Celestians,Repentia],
            troops = [ Sisters ],
            fast = [ Seraphims, Dominions ],
            heavy = [ Retributors,Exorcist,PEngine ],
            secondary=secondary
        )
        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_units([CommandSquad,Conclave]) - self.hq.count_unit(CommandSquad)
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

        def check_elites_limit():
            count = len(self.elites.get_units()) - self.elites.count_unit(Preacher)
            return self.elites.min <= count <= self.elites.max
        self.elites.check_limits = check_elites_limit

    def check_rules(self):
        can = self.hq.count_unit(Canoness)
        self.hq.set_active_types([CommandSquad],can > 0)
        if can < self.hq.count_units([CommandSquad]):
            self.error("You can't have more Sororitas Command Squads then Canonesses.")
        conf = self.hq.count_units([Kyrinov,Jacobus,Confessor])
        self.hq.set_active_types([Conclave],conf > 0)
        if conf < self.hq.count_units([Conclave]):
            self.error("You can't have more Battle Conclaves then Confessors (including named).")
        if self.elites.count_unit(Preacher) > 5:
            self.error('You can\'t take more then 5 Preachers.')
