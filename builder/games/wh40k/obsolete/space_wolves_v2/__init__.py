__author__ = 'Denis Romanov'

from builder.games.wh40k.old_roster import Wh40k, Ally
from builder.games.wh40k.roster import HQSection, ElitesSection, TroopsSection, FastSection, HeavySection, \
    Fort
from builder.games.wh40k.escalation.space_marines import LordsOfWar as MarinesLordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour import volume2
from builder.games.wh40k.imperial_armour.volume11 import space_wolves as volume11
from builder.games.wh40k.dataslates.cypher import Cypher
from .hq import *
from .elites import *
from .troops import *
from .fast import *
from .heavy import *


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.logan = UnitType(self, Logan, slot=0.5)
        self.njal = UnitType(self, Njal, slot=0.5)
        self.ragnar = UnitType(self, Ragnar, slot=0.5)
        self.ulric = UnitType(self, Ulric, slot=0.5)
        self.canis = UnitType(self, Canis, slot=0.5)
        self.bjorn = UnitType(self, Bjorn, slot=0.5)
        self.wolflord = UnitType(self, WolfLord, slot=0.5)
        self.runepriest = UnitType(self, RunePriest, slot=0.5)
        self.wolfpriest = UnitType(self, WolfPriest, slot=0.5)
        self.battleleader = UnitType(self, BattleLeader, slot=0.5)
        UnitType(self, Cypher, slot=0)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        self.wolfguard = UnitType(self, WolfGuards)
        UnitType(self, Dreadnought)
        UnitType(self, Venerable)
        UnitType(self, WolfScouts)
        UnitType(self, IronPriest)
        UnitType(self, LoneWolf)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, GreyHunters)
        UnitType(self, BloodClaws)
        self.wolfguard = UnitType(self, WolfGuards)
        self.wolfpack = UnitType(self, WolfPack)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.cavalry = UnitType(self, Cavalry)
        self.biker = UnitType(self, Biker)
        self.assault = UnitType(self, Assault)
        self.landspeedersquadron = UnitType(self, LandSpeederSquadron)
        self.wolfpack = UnitType(self, WolfPack)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, LongFangs)
        UnitType(self, Predator)
        UnitType(self, Whirlwind)
        UnitType(self, Vindicator)
        UnitType(self, LandRaider)
        UnitType(self, LandRaiderCrusader)
        UnitType(self, LandRaiderRedeemer)


class HQ(volume2.SpaceMarineHQ, volume11.IA11HQSection, BaseHQ):
    pass


class FastAttack(volume2.SpaceMarineFast, BaseFastAttack):
    pass


class Elites(volume2.SpaceWolvesElites, BaseElites):
    pass


class HeavySupport(volume2.SpaceMarineHeavySupport, BaseHeavySupport):
    pass


class Lords(volume2.SpaceMarineLordsOfWar, Titans, MarinesLordsOfWar):
    pass


class SpaceWolvesV2(Wh40k):
    army_id = 'space_wolves_v2'
    army_name = 'Space Wolves'
    obsolete = True

    def __init__(self, secondary=False):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(SpaceWolvesV2, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast, heavy=self.heavy,
            lords=Lords(self), fort=Fort(parent=self),
            ally=Ally(parent=self, ally_type=Ally.SW),
            secondary=secondary
        )

    def check_rules(self):
        super(SpaceWolvesV2, self).check_rules()
        if sum(u.has_landraider() for u in self.elites.wolfguard.units) > 1:
            self.error("You can't have more then one Land Raider of any type as Wolf Guards dedicated transport.")
        self.move_units(self.hq.logan.count > 0, self.elites.wolfguard, self.troops.wolfguard)
        self.move_units(self.hq.canis.count > 0, self.fast.wolfpack, self.troops.wolfpack)
