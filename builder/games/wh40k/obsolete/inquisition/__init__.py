__author__ = 'Denis Romanov'
from builder.core2 import UnitType
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, Wh40kImperial
from builder.games.wh40k.section import HQSection, ElitesSection
from builder.games.wh40k.dataslates.cypher import Cypher
from builder.games.wh40k.imperial_armour.volume5_7 import InquisitorCharacters
from .hq import *
from .elites import *


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.coteaz = UnitType(self, Coteaz)
        self.karamazov = UnitType(self, Karamazov)
        self.malleus = UnitType(self, Malleus)
        self.hereticus = UnitType(self, Hereticus)
        self.xenos = UnitType(self, Xenos)
        UnitType(self, Cypher, slot=0)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        self.warband = UnitType(self, Warband)


class HQ(InquisitorCharacters, BaseHQ):
    pass


class InquisitionDetachment(Wh40kBase):
    army_id = 'inquisition_det'
    army_name = 'Inquisition detachment'

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        super(InquisitionDetachment, self).__init__(
            hq=self.hq, elites=self.elites
        )

faction = 'Inquisition'


class Inquisition(Wh40kImperial, Wh40k7ed):
    army_id = 'inquisition'
    army_name = 'Inquisition'
    faction = faction
    obsolete = True

    def __init__(self):
        super(Inquisition, self).__init__([InquisitionDetachment])

detachments = [InquisitionDetachment]
