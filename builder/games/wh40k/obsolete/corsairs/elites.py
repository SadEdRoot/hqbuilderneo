__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.corsairs.hq import Venom
from functools import reduce


class VoidStorm(Unit):
    name = 'Corsair Voidstorm squad'

    class Veteran(ListSubUnit):
        name = 'Veteran Corsair'
        base_points = 12
        start_gear = ['Plasma grenades', 'Shimmershield', 'Corsair Jet Pack']
        has_hw = False
        has_mb = False
        weplist = [
            ['Flamer', 6, 'flame'],
            ['Fusion pistol', 10, 'fpist'],
            ['Fusion Gun', 15, 'fgun'],
            ['Power weapon', 10, 'pwep']
        ]

        def set_extra_bombs(self, hw, mb):
            self.has_hw = hw
            self.has_mb = mb

        def has_spec(self):
            return self.wep1.get_cur() != 'spist' or self.wep2.get_cur() != 'ccw'

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.wep1 = self.opt_one_of('Weapon', [['Shuriken pistol', 0, 'spist']] + self.weplist)
            self.wep2 = self.opt_one_of('', [['Close combat weapon', 0, 'ccw']] + self.weplist)

        def check_rules(self):
            self.wep2.set_active_options([item[2] for item in self.weplist], self.wep1.get_cur() == 'spist')
            self.wep1.set_active_options([item[2] for item in self.weplist], self.wep2.get_cur() == 'ccw')
            self.gear = self.start_gear + (['Haywire grenades'] if self.has_hw else []) + (['Meltabombs']
                                                                                           if self.has_mb else [])
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.wars = self.opt_units_list(self.Veteran.name, self.Veteran, 5, 10)
        self.opt = self.opt_options_list('Options', [
            ['Haywire grenades', 15, 'hwg'],
            ['Meltabombs', 15, 'mb']
        ])

    def get_count(self):
        return self.wars.get_count()

    def check_rules(self):
        self.wars.update_range()
        specmax = int(self.get_count() / 3)
        speccnt = 0
        for war in self.wars.get_units():
            if war.has_spec():
                speccnt += 1
            war.set_extra_bombs(self.opt.get('hwg'), self.opt.get('mb'))
        if speccnt > specmax:
            self.error("Squad of {0} can take no more then {1} special weapons. Taken: {2}".format(self.get_count(),
                                                                                                   specmax, speccnt))
        self.set_points(self.build_points(base_points=0, count=1))
        self.build_description(count=1, exclude=[self.opt.id])


class Harlequins(Unit):
    name = 'Harlequin Troupe'

    class Harlequin(ListSubUnit):
        name = 'Harlequin'
        base_points = 18
        gear = ['Flip belt', 'Holo-suit']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=10)
            self.ccw = self.opt_one_of('Weapon', [
                ['Close combat weapon', 0, 'ccw'],
                ['Harlequin\'s kiss', 4, 'hkiss']
            ])
            self.rng = self.opt_one_of('', [
                ['Shuriken pistol', 0, 'spist'],
                ['Fusion pistol', 10, 'fpist']
            ])

        def has_fusion(self):
            return self.count.get() if self.rng.get_cur() == 'fpist' else 0

    class DeathJester(Unit):
        name = 'Death Jester'
        base_points = 28
        gear = ['Flip belt', 'Holo-suit', 'Shrieker cannon']

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon', [
                ['Close combat weapon', 0, 'ccw'],
                ['Harlequin\'s kiss', 4, 'hkiss']
            ])

    class Shadowseer(Unit):
        gear = ['Flip belt', 'Holo-suit', 'Hallucinogen grenades']
        name = 'Shadowseer'
        base_points = 48

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon', [
                ['Close combat weapon', 0, 'ccw'],
                ['Harlequin\'s kiss', 4, 'hkiss']
            ])
            self.rng = self.opt_one_of('', [
                ['Shuriken pistol', 0, 'spist'],
                ['Fusion pistol', 10, 'fpist']
            ])

        def has_fusion(self):
            return 1 if self.rng.get_cur() == 'fpist' else 0

    class TroupeMaster(Unit):
        name = 'Troupe Master'
        base_points = 38
        gear = ['Flip belt', 'Holo-suit']

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon', [
                ['Close combat weapon', 0, 'ccw'],
                ['Power weapon', 0, 'psw'],
                ['Harlequin\'s kiss', 0, 'hkiss']
            ])
            self.rng = self.opt_one_of('', [
                ['Shuriken pistol', 0, 'spist'],
                ['Fusion pistol', 10, 'fpist']
            ])

        def has_fusion(self):
            return 1 if self.rng.get_cur() == 'fpist' else 0

    def __init__(self):
        Unit.__init__(self)
        self.troupe = self.opt_units_list(self.Harlequin.name, self.Harlequin, 5, 10)
        self.dj = self.opt_optional_sub_unit(self.DeathJester.name, self.DeathJester())
        self.shs = self.opt_optional_sub_unit(self.Shadowseer.name, self.Shadowseer())
        self.tm = self.opt_optional_sub_unit(self.TroupeMaster.name, self.TroupeMaster())
        self.boat = self.opt_optional_sub_unit('Transport', [Venom()])

    def get_count(self):
        return self.troupe.get_count() + self.dj.get_count() + self.shs.get_count() + self.tm.get_count()

    def check_rules(self):
        ldrs = self.dj.get_count() + self.shs.get_count() + self.tm.get_count()
        self.troupe.update_range(5 - ldrs, 10 - ldrs)
        self.boat.set_active(self.get_count() <= 5)

        def get_unit(u):
            u = u.get_unit()
            return [u.get_unit()] if u else []

        unit_list = self.troupe.get_units() + get_unit(self.shs) + get_unit(self.tm)
        fusions = reduce(lambda val, u: val + u.has_fusion() if u else val, unit_list, 0)
        if fusions > 2:
            self.error("You can't take more then 2 fusion pistols")
        self.set_points(self.build_points(count=1))
        self.build_description(count=1)
