__author__ = 'dante'

from builder.core.unit import Unit

class DropPod(Unit):
    name = 'Drop Pod'
    base_points =  35

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of("Weapon", [
            ["Storm Bolter", 0, "bolt"],
            ["Deathwind Missile Launcher", 20, "missile"],
            ])
        self.opt = self.opt_options_list("Options", [["Locator Beacon", 10]])

class Rhino(Unit):
    name = 'Rhino'
    base_points = 50
    gear = ["Storm Bolter", "Smoke Launchers"]

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Extra Armour", 15, "rh_armor"],
            ["Searchlight", 1, "sl"],
            ])

class Razorback(Unit):
    name = 'Razorback'
    base_points = 55
    gear = ["Smoke Launchers"]

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of("Weapon", [
            ["Twin-linked Heavy Bolter", 0, "heavy"],
            ["Twin-linked Heavy Flamer", 0, "flamer"],
            ["Twin-linked Assault Cannon", 35, "assault"],
            ["Twin-linked Lascannon", 35, "las"],
            ["Lascannon and Twin-linked Plasma Gun", 35, "plasma"],
            ])
        self.opt = self.opt_options_list("Options", [
            ["Dozen Blade", 5, "rh_dozen"],
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Extra Armour", 15, "rh_armor"],
            ["Searchlight", 1, "sl"],
            ])

class LandRaider(Unit):
    name = 'Land Raider'
    base_points = 250
    gear = ["Twin-linked Heavy Bolter", "Twin-linked Lascannon", "Twin-linked Lascannon", "Smoke Launchers"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Multi-melta", 10, "melta"],
            ["Extra Armour", 15, "rh_armor"],
            ["Searchlight", 1, "sl"],
            ])

class LandRaiderCrusader(Unit):
    name = 'Land Raider Crusader'
    base_points = 250
    gear = ["Twin-linked Assault Cannon", "Hurricane Bolter", "Hurricane Bolter", "Smoke Launchers", "Frag Assault Launcher"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Multi-melta", 10, "melta"],
            ["Extra Armour", 15, "rh_armor"],
            ["Searchlight", 1, "sl"],
            ])

class LandRaiderRedeemer(Unit):
    name = 'Land Raider Redeemer'
    base_points = 240
    gear = ["Twin-linked Assault Cannon", "Flamestorm Cannon", "Flamestorm Cannon", "Frag Assault Launcher", "Smoke Launchers"]
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list("Options", [
            ["Storm Bolter", 10, "rh_storm"],
            ["Hinter-killer Missile", 10, "rh_killer"],
            ["Multi-melta", 10, "melta"],
            ["Extra Armour", 15, "rh_armor"],
            ["Searchlight", 1, "sl"],
            ])
