__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.obsolete.tyranids.hq import *
from builder.games.wh40k.obsolete.tyranids.elites import *
from builder.games.wh40k.obsolete.tyranids.troops import *
from builder.games.wh40k.obsolete.tyranids.fast import *
from builder.games.wh40k.obsolete.tyranids.heavy import *

class Tyranids(LegacyWh40k):
    army_id = '299fcc54ab0349fea1b7e23614096cdb'
    army_name = 'Tyranids (2010)'
    obsolete = True
    def __init__(self):
        LegacyWh40k.__init__(self,
            hq = [HiveTyrant, TheSwarmlord, Tervigon, TyranidPrime, TheParasite],
            elites = [HiveGuard, Lictor, Deathleaper, Venomethrope, Zoanthrope, TheDoom, Pyrovore, Ymgarl],
            troops = [Warrior, Genestealer, Termagant, Hormagant, Ripper, {'unit': Tervigon, 'active': False}],
            fast = [Shrike, Ravener, SkySlasher, Gargoyle, Harpy, SporeMine],
            heavy = [Carnifex, TheOld, Biovore, Trygon, Mawloc, Tyrannofex]
        )

    def check_rules(self):
        termagant = self.troops.count_units([Termagant])
        self.troops.set_active_types([Tervigon], termagant > 0)
        if termagant < self.troops.count_units([Tervigon]):
            self.error("You can't have more Tervigons then Termagant units in Troops.")
