__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.core.model_descriptor import ModelDescriptor

from builder.games.wh40k.obsolete.space_marines.transport import Rhino, Razorback, DropPod
from builder.games.wh40k.obsolete.space_marines.heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer
from builder.games.wh40k.obsolete.space_marines.armory import *
from functools import reduce


class TerminatorAssaultSquad(Unit):
    name = 'Terminator Assault Squad'
    model_points = 40

    class Sergeant(Unit):
        name = 'Terminator Sergeant'
        base_points = 40
        gear = ['Terminator armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Lightning Claws', 0, 'lc'],
                ['Thunder Hammer and Storm Shield', 5, 'th']
            ])

        def check_rules_chain(self):
            self.set_points(self.build_points())
            self.build_description(exclude=[self.wep.id])
            if self.wep.get_cur() == 'lc':
                self.description.add('Lightning Claw', 2)
            else:
                self.description.add('Thunder Hammer')
                self.description.add('Storm Shield')

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Sergeant())
        self.terms = self.opt_count('Terminator', 4, 9, self.model_points)
        self.th = self.opt_count('Thunder Hammer and Storm Shield', 0, 4, 5)
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(), LandRaiderCrusader(),
                                                                  LandRaiderRedeemer()])

    def check_rules(self):
        self.th.update_range(0, self.terms.get())
        self.set_points(self.build_points(exclude=[self.th.id], count=1) + self.th.points())
        self.build_description(options=[self.leader])
        desc = ModelDescriptor('Terminator', gear=['Terminator armour'], points=self.model_points)
        self.description.add(desc.clone().add_gear('Lightning Claw', count=2).build(self.terms.get() - self.th.get()))
        self.description.add(desc.clone().add_gear('Thunder Hammer',
                                                   points=5).add_gear('Storm Shield').build(self.th.get()))
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return self.terms.get() + 1

    def have_lr(self):
        return self.transport.get_unit() is not None


class TerminatorSquad(Unit):
    name = 'Terminator Squad'

    class Terminator(ListSubUnit):
        name = 'Terminator'
        base_points = 40
        gear = ['Terminator armour']

        def __init__(self):
            ListSubUnit.__init__(self, min_models=1, max_models=9)
            self.ccw = self.opt_one_of('Weapon', [
                ['Power fist', 0, 'pfist'],
                ['Chainfist', 5, 'chfist']
            ])
            self.rng = self.opt_one_of('', [
                ['Storm bolter', 0, 'sbgun'],
                ['Heavy flamer', 10, 'hflame'],
                ['Assault cannon', 20, 'asscan']
            ])
            self.cym = self.opt_options_list('', [
                ["Cyclone missile launcher", 30, 'cmlnch']
            ])

        def has_special(self):
            return (1 if self.cym.get('cmlnch') or self.rng.get_cur() != 'sbgun' else 0) * self.count.get()

        def check_rules(self):
            self.cym.set_active(self.rng.get_cur() == 'sbgun')
            self.rng.set_active_options(['hflame', 'asscan'], not self.cym.get('cmlnch'))
            ListSubUnit.check_rules(self)

    class TerminatorSergeant(StaticUnit):
        name = "Terminator Sergeant"
        base_points = 40
        gear = ['Terminator armour', 'Storm bolter', 'Power sword']
        static = True

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.TerminatorSergeant()
        self.terms = self.opt_units_list(self.Terminator.name, self.Terminator, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(), LandRaiderCrusader(),
                                                                  LandRaiderRedeemer()])

    def check_rules(self):
        self.terms.update_range()
        spec_limit = 1 if self.terms.get_count() < 9 else 2
        spec_total = reduce(lambda val, u: u.has_special() + val, self.terms.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Special weapon (Cyclone missile launcher, Heavy flamer, Assault cannon) '
                       'is over limit ({0})'.format(spec_limit))
        self.set_points(self.build_points(count=1, base_points=self.TerminatorSergeant.base_points))
        self.build_description(add=[self.leader.get_description()])

    def get_count(self):
        return self.terms.get_count() + 1

    def have_lr(self):
        return self.transport.get_unit() is not None


class CenturionAssault(Unit):
    name = 'Centurion Assault Squad'

    class Centurion(ListSubUnit):
        name = 'Centurion'
        base_points = 60
        gear = ['Siege drill' for _ in range(2)]

        def __init__(self):
            ListSubUnit.__init__(self, max_models=5)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Ironclad assault launchers', 0, 'ial'],
                ['Hurricane bolter', 0, 'bolt'],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Twin-linked flamer', 0, 'fl'],
                ['Twin-linked meltagun', 5, 'melta'],
            ])

    class Sergeant(Unit):
        base_points = 70
        name = 'Centurion Sergeant'

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Ironclad assault launchers', 0, 'ial'],
                ['Hurricane bolter', 0, 'bolt'],
            ])
            self.wep2 = self.opt_one_of('', [
                ['Twin-linked flamer', 0, 'fl'],
                ['Twin-linked meltagun', 5, 'melta'],
            ])
            self.opt = self.opt_options_list('Options', [
                ['Omniscope', 10, 'scope'],
            ])

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.marines = self.opt_units_list(self.Centurion.name, self.Centurion, 2, 5)
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(), LandRaiderCrusader(),
                                                                  LandRaiderRedeemer()])

    def get_count(self):
        return self.marines.get_count() + 1

    def check_rules(self):
        self.marines.update_range()
        self.points.set(self.build_points(count=1))
        self.build_description()


class VanguardVeteranSquad(Unit):
    name = 'Vanguard Veteran Squad'
    weapon_list = [
        ['Plasma pistol', 15, 'ppist'],
        ['Grav-pistol', 15, 'gpist'],
    ] + melee

    class Veteran(ListSubUnit):
        name = 'Veteran'
        base_points = 19
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Chainsword', 0, 'csw']
            ] + VanguardVeteranSquad.weapon_list)
            self.wep2 = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bpist']
            ] + VanguardVeteranSquad.weapon_list)

            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Storm shield', 10, 'ss'],
            ])

    class Sergeant(Unit):
        name = 'Veteran Sergeant'
        base_points = 95 - 4 * 19
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Chainsword', 0, 'csw']
            ] + VanguardVeteranSquad.weapon_list + [['Relic blade', 25, 'relic']])
            self.wep2 = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bpist']
            ] + VanguardVeteranSquad.weapon_list + [['Relic blade', 25, 'relic']])

            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Storm shield', 10, 'ss'],
            ])

    def __init__(self):
        Unit.__init__(self)
        self.vets = self.opt_units_list(self.Veteran.name, self.Veteran, 4, 9)
        self.leader = self.opt_sub_unit(self.Sergeant())
        self.ride = self.opt_options_list('Options', [
            ['Jump packs', 3, 'jp']
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def get_count(self):
        return self.vets.get_count() + 1

    def check_rules(self):
        self.vets.update_range()
        self.transport.set_active(not self.ride.get('jp'))
        self.set_points(self.build_points(count=1, exclude=[self.ride.id]) + self.ride.points() * self.get_count())
        self.build_description(exclude=[self.ride.id], count=1)
        if self.ride.get('jp'):
            self.description.add('Jump Packs')


class SternguardVeteranSquad(Unit):
    name = 'Sternguard Veteran Squad'

    combilist = [
        ['Storm bolter', 5, 'sbgun'],
        ['Combi-melta', 10, 'cmelta'],
        ['Combi-flamer', 10, 'cflame'],
        ['Combi-grav', 10, 'cgrav'],
        ['Combi-plasma', 10, 'cplasma']
    ]

    class Sergeant(Unit):
        name = 'Veteran Sergeant'
        base_points = 120 - 22 * 4
        gear = ['Power armor', 'Frag grenades', 'Krak grenades',  'Special issue ammunition']

        def __init__(self):
            Unit.__init__(self)
            weapon_list = [
                ['Chainsword', 0, 'chsw'],
                ['Plasma pistol', 15, 'ppist'],
                ['Lightning claw', 15, 'lclaw'],
                ['Grav-pistol', 15, 'gpist'],
                ['Power weapon', 15, 'psw'],
                ['Power fist', 25, 'pfist']
            ]
            self.wep1 = self.opt_one_of('Weapon', [['Bolt pistol', 0, 'bpist']] + weapon_list)
            self.wep2 = self.opt_one_of('', [
                ['Boltgun', 0, 'bgun']
            ] + weapon_list + SternguardVeteranSquad.combilist)
            self.opt = self.opt_options_list('Options', [["Melta bombs", 5, 'mbomb']])

    class Veteran(ListSubUnit):
        name = "Veteran"
        base_points = 22
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Bolt pistol', 'Special issue ammunition']

        def __init__(self):
            ListSubUnit.__init__(self, min_models=4, max_models=9)
            self.wep = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bgun']
            ] + SternguardVeteranSquad.combilist + special + leg_heavy)
            self.flakk = self.opt_options_list('', flakk)

        def has_special(self):
            return self.get_count() if self.wep.get_cur() in spec_id + leg_heavy_id else 0

        def check_rules(self):
            self.flakk.set_visible(self.wep.get_cur() == 'mlaunch')
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.Sergeant())
        self.vets = self.opt_units_list(self.Veteran.name, self.Veteran, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        self.vets.update_range()
        spec_limit = 2
        spec_total = sum([u.has_special() for u in self.vets.get_units()])
        if spec_limit < spec_total:
            self.error('Veterans can take only 2 special or heavy weapon (taken: {0})'.format(spec_total))
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.vets.get_count() + 1


class Dred(Unit):
    name = 'Dreadnought'
    base_points = 100
    gear = ['Smoke launchers', 'Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Multi-melta', 0, 'mmelta'],
            ['Twin-linked autocannon', 5, 'tlacan'],
            ['Twin-linked heavy bolter', 5, 'tlhbgun'],
            ['Twin-linked heavy flamer', 5, 'tlhflame'],
            ['Plasma cannon', 10, 'pcan'],
            ['Assault cannon', 20, 'asscan'],
            ['Twin-linked lascannon', 25, 'tllcan']
        ])
        self.wep2 = self.opt_one_of('', [
            ['Power fist', 0, 'dccw'],
            ['Missile launcher', 10, 'mlnch'],
            ['Twin-linked autocannon', 15, 'tlacan'],
        ])
        self.bin = self.opt_one_of('', [
            ['Built-in Storm bolter', 0, 'sbgun'],
            ['Built-in Heavy flamer', 10, 'hflame']
        ])
        self.opt = self.opt_options_list('Options', [
            ["Extra armour", 10, 'earmr'],
        ])
        self.ven = self.opt_options_list('', [
            ["Venerable Dreadnought", 25, 'ven']
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        self.bin.set_visible(self.wep2.get_cur() == 'dccw')
        self.set_points(self.build_points())
        if self.ven.get('ven'):
            self.build_description(name='Venerable Dreadnought', exclude=[self.ven.id])
        else:
            self.build_description()


class IronDred(Unit):
    name = 'Ironclad Dreadnought'
    base_points = 135
    gear = ['Smoke launchers', 'Searchlight', 'Extra armour']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Power fist', 0, 'dccw'],
            ['Hurricane bolter', 0, 'hurrbolt']
        ])
        self.bin1 = self.opt_one_of('', [
            ['Build-in Storm bolter', 0, 'sbgun'],
            ['Build-in Heavy flamer', 10, 'hflame']
        ])
        self.wep2 = self.opt_one_of('', [
            ['Seismic hammer', 0, 'sham'],
            ['Chainfist', 0, 'chfist']
        ])
        self.bin2 = self.opt_one_of('', [
            ['Build-in Meltagun', 0, 'mgun'],
            ['Build-in Heavy flamer', 0, 'hflame']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Hunter-killer missile', 10, 'hkm1'],
            ['Hunter-killer missile', 10, 'hkm2'],
            ["Ironclad assault launchers", 10, 'ialnch']
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        self.bin1.set_visible(self.wep1.get_cur() == 'dccw')
        Unit.check_rules(self)


class TheDamned(Unit):
    name = 'Legion of the Damned'
    gear = ['Power armor', 'Frag grenades', 'Krak grenades']

    class DamnedSergeant(Unit):
        name = 'Legionnaire Sergeant'
        gear = ['Power armor', 'Frag grenades', 'Krak grenades', 'Bolt pistol']
        base_points = 125 - 25 * 4

        def __init__(self):
            Unit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bgun'],
                ['Chainsword', 0, 'chain'],
                ['Power weapon', 15, 'pw'],
                ['Power fist', 25, 'pf']
            ])
            self.wep2 = self.opt_one_of('', ranged_pistol)

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.DamnedSergeant())
        self.marines = self.opt_count('Legionnaire', 4, 9, 25)
        self.hvy1 = self.opt_one_of('Heavy weapon', [
            ['Boltgun', 0, 'bgun'],
            ['Flamer', 20, 'flame'],
            ['Meltagun', 20, 'mgun'],
            ['Plasma gun', 20, 'pgun']
        ])
        self.hvy2 = self.opt_one_of('', [['Boltgun', 0, 'bgun']] + leg_heavy)
        self.flakk = self.opt_options_list('', flakk)

    def check_rules(self):
        self.flakk.set_visible(self.hvy2.get_cur() == 'mlaunch')

        self.set_points(self.build_points(options=[self.hvy1, self.hvy2, self.sergeant],
                                          count=1) + self.marines.points())
        self.build_description(options=[self.sergeant], gear=[], count=1)
        desc = ModelDescriptor('Legionnaire', gear=self.gear, points=25)
        full_count = self.marines.get()
        if self.hvy1.get_cur() != 'bgun':
            self.description.add(desc.clone().add_gear_opt(self.hvy1).build(1))
            full_count -= 1
        if self.hvy2.get_cur() != 'bgun':
            self.description.add(desc.clone().add_gear_opt(self.hvy2).add_gear_opt(self.flakk).build(1))
            full_count -= 1
        self.description.add(desc.add_gear('Boltgun').build(full_count))

    def get_count(self):
        return self.marines.get() + 1
