from builder.games.wh40k.obsolete.space_marines.transport import DropPod, Rhino, Razorback

__author__ = 'Ivan Truskov'

from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor
from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.games.wh40k.obsolete.space_marines.armory import *


class Calgar(Unit):
    name = 'Marneus Calgar'
    base_points = 275
    base_gear = ['Gauntlets of Ultramar', 'Power sword', 'Iron Halo']
    unique = True

    def __init__(self):
        Unit.__init__(self)
        self.armr = self.opt_one_of('Armour', [
            ['Power Armour', 0, 'pwr'],
            ['Armour of Antilochus', 10, 'ant']
        ])

    def check_rules(self):
        self.gear = self.base_gear + (['Frag grenades', 'Krak grenades'] if self.armr.get_cur() is 'pwr' else [])
        Unit.check_rules(self)


class Sicarius(StaticUnit):
    name = 'Captain Sicarius'
    base_points = 185
    gear = ['Mantle of the Suzerain', 'Talassarian Tempest Blade', 'Plasma pistol', 'Frag grenades', 'Krak grenades',
            'Iron Halo']


class Tigurius(StaticUnit):
    name = 'Chief Librarian Tigurius'
    base_points = 165
    gear = ['Power armour', 'Bolt pistol', 'Hood of Hellfire', 'Frag grenades', 'Krak grenades', 'Rod of Tigurius']


class Cassius(StaticUnit):
    name = 'Chaplain Cassius'
    base_points = 130
    gear = ['Power armour', 'Bolt pistol', 'Crozius Arcanum', 'Frag grenades', 'Krak grenades', 'Rosarius', 'Infernus']


class Khan(Unit):
    name = 'Kor\'sarro Khan'
    base_points = 125
    gear = ['Power armour', 'Bolt Pistol', 'Moonfang', 'Frag grenades', 'Krak grenades', 'Iron Halo']
    unique = True

    def __init__(self):
        Unit.__init__(self)
        self.ride = self.opt_options_list('Bike', [['Moondrakkan', 25, 'bike']])

    def has_bike(self):
        return self.ride.get('bike')


class Vulkan(StaticUnit):
    name = 'Vulkan He\'Stan'
    base_points = 190
    gear = ['Artificer armour', 'Bolt Pistol', 'Frag grenades', 'Krak grenades', 'Kesare\'s Mantle',
            'The Spear of Vulkan', 'The Gauntlet of the Forge']


class Shrike(StaticUnit):
    name = 'Shadow Captain Shrike'
    base_points = 185
    gear = ['Power armour', 'Bolt Pistol', 'The Raven\'s Talons', 'Frag grenades', 'Krak grenades', 'Jump pack',
            'Iron Halo']


class Lysander(StaticUnit):
    name = 'Captain Lysander'
    base_points = 230
    gear = ['Terminator armour', 'The Fist of Dorn', 'Storm shield', 'Iron Halo']


class Kantor(StaticUnit):
    name = 'Pedro Cantor'
    base_points = 185
    gear = ['Power armour', 'Dorn\'s arrow', 'Power fist', 'Frag grenades', 'Krak grenades', 'Iron Halo']


class HighMarshall(StaticUnit):
    name = 'High Marshall Helbrecht'
    base_points = 185
    gear = ['Artificer armour', 'Iron halo', 'Frag grenades', 'Krak grenades', 'Combi-melta', 'Sword of High Marshals']


class Grimaldus(Unit):
    unique = True
    name = 'Chaplain Grimaldus'
    gear = ['Crozius arcanum', 'Rosarius', 'Power armour', 'Master-crafter plasma pistol', 'Frag grenades',
            'Krak grenades']
    base_points = 180

    def __init__(self):
        Unit.__init__(self)
        self.serv = self.opt_options_list('', [['Cenobyte Servitors', 30, 'serv']])
        self.serv_count = self.opt_count('Servitors', 3, 5, 10)

    def check_rules(self):
        self.serv_count.set_active(self.serv.get('serv'))
        self.points.set(self.build_points(exclude=[self.serv.id]))
        self.build_description(exclude=[self.serv.id, self.serv_count.id])
        self.description.add(ModelDescriptor('Cenobyte Servitor', points=10).build(self.serv_count.get()))


class Champion(StaticUnit):
    name = 'The Emperors\'s Champion'
    base_points = 140
    gear = ['Black Sword', 'The Armour of Faith', 'Bolt pistol', 'Frag grenades', 'Krak grenades']


def check_relics(w1, w2):
    w1.set_active_options(relic_weapon_ids, True)
    w2.set_active_options(relic_weapon_ids, True)
    if w1.get_cur() in relic_weapon_ids:
        w2.set_active_options([w1.get_cur()], False)
    if w2.get_cur() in relic_weapon_ids:
        w1.set_active_options([w2.get_cur()], False)


def get_relics(opt):
    if opt.get_cur() in relic_weapon_ids:
        return [opt.get_selected()]
    else:
        return []


class ChapterMaster(Unit):
    name = 'Chapter Master'
    base_points = 130

    def __init__(self):
        Unit.__init__(self)

        self.armr = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Artificer armour', 20, 'art'],
            ['Terminator armour', 40, 'tda']
        ])
        self.wep1 = self.opt_one_of('Weapon', [
            ['Chainsword', 0, 'csw'],
            ['Relic blade', 25, 'rb'],
        ] + melee + ranged + relic_weapon)
        self.wep2 = self.opt_one_of('', [
            ['Bolt pistol', 0, 'bpist'],
            ['Boltgun', 0, 'bg'],
        ] + melee + ranged + relic_weapon)

        self.twep1 = self.opt_one_of('Weapon', tda_ranged + relic_weapon)
        self.twep2 = self.opt_one_of('', tda_power + relic_weapon)
        self.rel = self.opt_options_list('Relics', relic_gear)
        self.opt = self.opt_options_list('Options', spec + [['Storm shield', 15, 'sshield']])
        self.ride = self.opt_options_list('', spec_bike + spec_jump, limit=1)

    def check_rules(self):
        tda = self.armr.get_cur() == 'tda'
        self.ride.set_visible(not tda)
        self.wep1.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.rel.set_visible(not tda)
        self.twep1.set_visible(tda)
        self.twep2.set_visible(tda)

        check_relics(self.wep1, self.wep2)
        check_relics(self.twep1, self.twep2)
        self.gear = ['Iron halo']
        if not tda:
            self.gear += ['Frag grenades', 'Krak grenades']
        Unit.check_rules(self)

    def get_unique_gear(self):
        return (self.rel.get_selected() if self.rel.is_used() else []) + \
            sum(list(map(get_relics, [self.wep1, self.wep2, self.twep1, self.twep2])), [])

    def has_bike(self):
        return self.ride.get('bike')


class TerminatorCaptain(Unit):
    name = 'Terminator Captain'
    base_points = 120
    gear = ['Iron halo', 'Terminator armour']

    def __init__(self):
        Unit.__init__(self)

        self.twep1 = self.opt_one_of('Weapon', tda_ranged + relic_weapon)
        self.twep2 = self.opt_one_of('', tda_power + relic_weapon)

    def check_rules(self):
        check_relics(self.twep1, self.twep2)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum(list(map(get_relics, [self.twep1, self.twep2])), [])


class Captain(Unit):
    name = 'Captain'
    base_points = 90
    gear = ['Iron halo', 'Frag grenades', 'Krak grenades']

    def __init__(self):
        Unit.__init__(self)

        self.armr = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Artificer armour', 20, 'art'],
        ])
        self.wep1 = self.opt_one_of('Weapon', [
            ['Chainsword', 0, 'csw'],
            ['Relic blade', 25, 'rb'],
        ] + melee + ranged + relic_weapon)
        self.wep2 = self.opt_one_of('', [
            ['Bolt pistol', 0, 'bpist'],
            ['Boltgun', 0, 'bg'],
        ] + melee + ranged + relic_weapon)

        self.rel = self.opt_options_list('Relics', relic_gear)
        self.opt = self.opt_options_list('Options', spec + [['Storm shield', 15, 'sshield']])
        self.ride = self.opt_options_list('', spec_bike + spec_jump, limit=1)

    def check_rules(self):
        check_relics(self.wep1, self.wep2)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return (self.rel.get_selected() if self.rel.is_used() else []) + \
            sum(list(map(get_relics, [self.wep1, self.wep2])), [])

    def has_bike(self):
        return self.ride.get('bike')


class HonourGuardSquad(Unit):
    name = 'Honour Guard'

    class HonourGuard(ListSubUnit):
        name = 'Honour Guard'

        base_points = 25
        gear = ['Artificer armour', 'Frag grenades', 'Krak grenades', 'Bolt pistol', 'Boltgun']

        def __init__(self):
            ListSubUnit.__init__(self, max_models=9)
            self.opt = self.opt_one_of('Weapon', [
                ['Power weapon', 0, 'pwr'],
                ['Relic blade', 15, 'rsw'],
            ])
            self.banner = self.opt_options_list('Banner', [
                ['Chapter Banner', 25, 'chban'],
                ['Standard of the Emperor Ascendant', 65, 'std'],
            ], limit=1)

        def have_banner(self):
            return self.get_count() if self.banner.is_any_selected() else 0

        def get_banner(self):
            return self.banner.get_selected()

    class ChapterChampion(Unit):
        name = 'Chapter Champion'
        gear = ['Artificer armour', 'Frag grenades', 'Krak grenades', 'Bolt pistol']
        base_points = 85 - 25 * 2

        def __init__(self):
            Unit.__init__(self)
            self.rng = self.opt_one_of('Weapon', [
                ['Boltgun', 0, 'bgun'],
                ['Close combat weapon', 0, 'ccw']
            ])
            self.ccw = self.opt_one_of('', [
                ['Power sword', 0, 'psw'],
                ['Thunder hammer', 15, 'ham']
            ])

    def __init__(self):
        Unit.__init__(self)
        self.guards = self.opt_units_list(self.HonourGuard.name, self.HonourGuard, 2, 9)
        self.champ = self.opt_sub_unit(self.ChapterChampion())
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        self.guards.update_range()
        flag = sum([u.have_banner() for u in self.guards.get_units()])
        if flag > 1:
            self.error('Only one Honor Guard can take a banner (taken: {})'.format(flag))
        self.points.set(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.champ.get_count() + self.guards.get_count()

    def get_unique_gear(self):
        return sum([u.get_banner() for u in self.guards.get_units()], [])


class CommandSquad(Unit):
    name = 'Command Squad'

    class Veteran(ListSubUnit):
        name = 'Veteran'
        base_points = 20
        gear = ["Power Armour", 'Frag grenades', 'Krak grenades']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Chainsword', 0, 'csw'],
                ['Boltgun', 0, 'bg'],
            ] + melee + ranged)
            self.wep2 = self.opt_one_of('', [
                ['Bolt pistol', 0, 'bpist'],
                ['Boltgun', 0, 'bg'],
            ] + melee + ranged)

            self.opt = self.opt_options_list('Options', [
                ['Melta bombs', 5, 'mbomb'],
                ['Storm shield', 10, 'ss']
            ])
            self.banner = self.opt_options_list('Banner', [
                ['Company standard', 15, 'cst'],
                ['Standard of the Emperor Ascendant', 65, 'std'],
            ], limit=1)

        def have_banner(self):
            return self.get_count() if self.banner.is_any_selected() else 0

        def get_unique_banner(self):
            return self.banner.get_selected() if self.banner.get('std') else []

    def __init__(self):
        Unit.__init__(self)
        self.hg = self.opt_units_list(self.name, self.Veteran, 1, 5)
        self.hg.get_units()[0].count.set(5)
        self.up = self.opt_options_list('', [
            ["Company champion", self.Veteran.base_points + 15, 'ch'],
            ["Apothecary", self.Veteran.base_points + 15, 'ap'],
        ])
        self.bikes = self.opt_options_list('', [['Bikes', 35, 'bikes']])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        self.hg.update_range()
        flag = sum((c.have_banner() for c in self.hg.get_units()))
        if flag > 1:
            self.error('Only one Veteran can take a banner (taken: {})'.format(flag))
        if self.get_count() != 5:
            self.error('Command Squad must include 5 Veterans (include Apothecary and Company champion) '
                       '(taken: {})'.format(self.get_count()))
        self.points.set(self.build_points(count=1))
        self.build_description(count=1, exclude=[self.up.id])
        if self.up.get('ch'):
            self.description.add(ModelDescriptor(
                'Company champion',
                points=self.Veteran.base_points + 15,
                gear=["Power Armour", 'Frag grenades', 'Krak grenades', 'Power weapon', 'Combat Shield',
                      'Bolt pistol'],
                count=1).build())
        if self.up.get('ap'):
            self.description.add(ModelDescriptor(
                'Apothecary',
                points=self.Veteran.base_points + 15,
                gear=["Power Armour", 'Frag grenades', 'Krak grenades', 'Chainsword', 'Narthecium'],
                count=1).build())

    def get_count(self):
        return self.hg.get_count() + len(self.up.get_all())

    def get_unique_gear(self):
        return sum((u.get_unique_banner() for u in self.hg.get_units()), [])


class Librarian(Unit):
    name = 'Librarian'
    base_points = 65
    gear = ['Psychic hood']

    def __init__(self):
        Unit.__init__(self)
        self.armr = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Terminator armour', 25, 'tda']
        ])

        self.wep1 = self.opt_one_of('Weapon', [
            ['Force weapon', 0, 'forcewep'],
        ] + relic_weapon)
        self.wep2 = self.opt_one_of('', [
            ['Bolt pistol', 0, 'bpist'],
            ['Boltgun', 0, 'bgun']
        ] + ranged + relic_weapon)
        self.twep = self.opt_options_list('', [
            ['Storm Bolter', 5, 'sbgun'],
            ['Combi-melta', 10, 'cmelta'],
            ['Combi-flamer', 10, 'cflame'],
            ['Combi-plasma', 10, 'cplasma'],
            ['Storm shield', 10, 'ss']
        ], limit=1)
        self.relic = self.opt_options_list('Relic', relic_gear)
        self.psy = self.opt_one_of('Psyker', [['Mastery Level 1', 0, 'lvl1'], ['Mastery Level 2', 25, 'lvl2']])
        self.opt = self.opt_options_list('Options', spec)
        self.ride = self.opt_options_list('', spec_jump + spec_bike, limit=1)

    def check_rules(self):
        tda = (self.armr.get_cur() == 'tda')
        self.ride.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.relic.set_visible(not tda)
        self.twep.set_visible(tda)
        check_relics(self.wep1, self.wep2)
        Unit.check_rules(self)
        if not tda:
            self.description.add(['Frag grenades', 'Krak grenades'])

    def has_bike(self):
        return self.ride.get('bike')

    def get_unique_gear(self):
        return (self.relic.get_selected() if self.relic.is_used() else []) + \
            sum(list(map(get_relics, [self.wep1, self.wep2])), [])


class Chaplain(Unit):
    name = 'Chaplain'
    base_points = 90

    def __init__(self):
        Unit.__init__(self)
        self.armr = self.opt_one_of('Armour', [
            ['Power armour', 0, 'pwr'],
            ['Terminator armour', 30, 'tda']
        ])
        self.wep1 = self.opt_one_of('Weapon', [
            ['Crozius Arcanum', 0, 'forcewep'],
        ] + relic_weapon)
        self.wep2 = self.opt_one_of('', [
            ['Bolt pistol', 0, 'bpist'],
            ['Boltgun', 0, 'bgun'],
            ['Power fist', 25, 'pfist']
        ] + ranged + relic_weapon)
        self.twep = self.opt_one_of('', [
            ['Storm Bolter', 0, 'sbgun'],
            ['Combi-melta', 5, 'cmelta'],
            ['Combi-flamer', 5, 'cflame'],
            ['Combi-plasma', 5, 'cplasma'],
        ] + relic_weapon)

        self.relic = self.opt_options_list('Relic', relic_gear)
        self.opt = self.opt_options_list('Options', spec)
        self.ride = self.opt_options_list('', spec_jump + spec_bike, limit=1)

    def check_rules(self):
        tda = (self.armr.get_cur() == 'tda')
        self.ride.set_visible(not tda)
        self.wep2.set_visible(not tda)
        self.relic.set_visible(not tda)
        self.twep.set_visible(tda)
        check_relics(self.wep1, self.wep2)
        check_relics(self.wep1, self.twep)
        self.gear = ['Rozarius']
        if not tda:
            self.gear += ['Frag grenades', 'Krak grenades']
        Unit.check_rules(self)

    def get_unique_gear(self):
        return (self.relic.get_selected() if self.relic.is_used() else []) + \
            sum(list(map(get_relics, [self.wep1, self.wep2, self.twep])), [])


class ForgeMaster(Unit):
    name = 'Master Of the Forge'
    base_points = 90
    gear = ['Frag grenades', 'Krak grenades', 'Artificar armour']

    def __init__(self):
        Unit.__init__(self)
        self.harness = self.opt_one_of('Special', [
            ['Servo-harness', 0, 'servo'],
            ['Conversion beamer', 20, 'cbeam']
        ])
        self.wep1 = self.opt_one_of('Weapon', [
            ['Bolt pistol', 0, 'bpist'],
            ['Power axe', 15, 'pwraxe'],
        ] + melee + ranged + relic_weapon)
        self.wep2 = self.opt_one_of('', [
            ['Boltgun', 0, 'bgun'],
            ['Power axe', 15, 'pwraxe'],
        ] + relic_weapon)

        self.relic = self.opt_options_list('Relic', relic_gear)
        self.opt = self.opt_options_list('Options', spec)
        self.ride = self.opt_options_list('', spec_bike, limit=1)

    def check_rules(self):
        self.wep2.set_visible(not self.harness.get_cur() == 'cbeam')
        check_relics(self.wep1, self.wep2)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return (self.relic.get_selected() if self.relic.is_used() else []) + \
            sum(list(map(get_relics, [self.wep1, self.wep2])), [])


class Techmarine(Unit):
    name = 'Techmarine'
    base_points = 50
    gear = ['Frag grenades', 'Krak grenades', 'Artificar armour']

    def __init__(self):
        Unit.__init__(self)
        self.harness = self.opt_one_of('Special', [
            ['Servo-arm', 0],
            ['Servo-harness', 25],
        ])
        self.wep1 = self.opt_one_of('Weapon', [
            ['Bolt pistol', 0, 'bpist'],
            ['Power axe', 15, 'pwraxe'],
        ] + melee + ranged)
        self.wep2 = self.opt_one_of('', [
            ['Boltgun', 0, 'bgun'],
            ['Power axe', 15, 'pwraxe'],
        ])
        self.ride = self.opt_options_list('Options', spec)
        self.opt = self.opt_options_list('', spec_bike)


class Servitors(Unit):
    name = "Servitors"
    base_points = 10

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count(self.name, 1, 5, self.base_points)
        self.hb = self.opt_count('Heavy bolter', 0, 2, 10)
        self.mm = self.opt_count('Multi-melta', 0, 2, 20)
        self.pc = self.opt_count('Plasma cannon', 0, 2, 20)
        self.hw = [self.hb, self.mm, self.pc]

    def check_rules(self):
        norm_counts(0, min(2, self.count.get()), self.hw)
        self.set_points(self.build_points(count=1, base_points=0))
        self.build_description(options=[])
        desc = ModelDescriptor('Servitors', points=10)
        self.description.add(desc.clone().add_gear('Heavy bolter', points=10).build(self.hb.get()))
        self.description.add(desc.clone().add_gear('Multi-melta', points=20).build(self.mm.get()))
        self.description.add(desc.clone().add_gear('Plasma cannon', points=20).build(self.pc.get()))
        self.description.add(desc.clone().add_gear('Servo-Arm').build(self.count.get() -
                                                                      sum([h.get() for h in self.hw])))
