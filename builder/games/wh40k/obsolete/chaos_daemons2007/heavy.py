__author__ = 'Ivan Truskov'
from builder.core.unit import Unit


class SoulGrinder(Unit):
    name = 'SoulGrinder'
    base_points = 135
    gear = ['Dreadnought close combat weapon','Dreadnought close combat weapon with built in Harvester','Mawcannon (Vomit)']
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Mawcannon upgrades',[
            ['Phlegm',25,'ph'],
            ['Tongue',25,'tng']
        ])


class DaemonPrince(Unit):
    name = "Daemon Prince"
    base_points = 80
    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options',[
                    ['Daemonic Flight',60,'wing'],
                    ['Iron Hide',30,'ihd'],
                    ['Instrument of Chaos',5,'ich'],
                    ['Unholy Might',20,'umght']
                        ])
        self.mark = self.opt_options_list('Mark',[
                    ['Mark of Khorne',15,'kh'],
                    ['Mark of Slaanesh',10,'sl'],
                    ['Mark of Nurgle',30,'ng'],
                    ['Mark of Tzeentch',25,'tz']
                        ],1)
        self.kh = self.opt_options_list('Blessings',[
                    ['Blessing of the Blood God',10,'bless'],
                    ['Death Strike',20,'dstr']
                        ])
        self.tz = self.opt_options_list('Blessings',[
                    ['Master of Sorcery',10,'mos'],
                    ['Soul Devourer',10,'sdev'],
                    ['Bolt of Tzeentch',35,'bot']
                    ])
        self.sl = self.opt_options_list('Blessings',[
                    ['Aura of Acquiescence',5,'aura'],
                    ['Soporific Musk',20,'smusk'],
                    ['Transfixing Gaze',10,'tgaze'],
                    ['Pavane of Slaanesh',20,'pav']
                    ])
        self.ng = self.opt_options_list('Blessings',[
                    ['Cloud of Flies',5,'cof'],
                    ['Noxious Touch',10,'ntch'],
                    ['Aura of Decay',20,'aodec']
                        ])
        self.var = self.opt_options_list('Powers',[
                    ['Daemonic gaze',20,'dgaze'],
                    ['Breath of Chaos',30,'boc'],
                    ['Boon of Mutation',30,'bom']
                    ], limit=1)
    def check_rules(self):
        self.kh.set_visible(self.mark.get('kh'))
        self.tz.set_visible(self.mark.get('tz'))
        self.ng.set_visible(self.mark.get('ng'))
        self.sl.set_visible(self.mark.get('sl'))
        self.var.set_visible(not self.mark.get('kh'))
        self.var.set_limit(2 if self.mark.get('tz') else 1)

        Unit.check_rules(self)