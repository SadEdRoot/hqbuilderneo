__author__ = 'Ivan Truskov'

from builder.games.wh40k.escalation.imperial_titans import LordsOfWar
from .hq import Celestine, Jacobus, Canoness, CommandSquad, Priest, Conclave
from .elites import Celestians, Repentia
from .troops import Sisters
from .fast import Seraphims, Dominions
from .heavy import Retributors, Exorcist, PenitentEngine
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType
from builder.games.wh40k.fortifications import Fort


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.celestine = UnitType(self, Celestine)
        self.jacobus = UnitType(self, Jacobus)
        self.canoness = UnitType(self, Canoness)
        self.commandsquad = UnitType(self, CommandSquad, slot=0)
        self.priest = UnitType(self, Priest, slot=0)
        self.conclave = UnitType(self, Conclave, slot=0)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        self.celestians = UnitType(self, Celestians)
        self.repentia = UnitType(self, Repentia)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.sisters = UnitType(self, Sisters)


class FastAttack(FastSection):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        self.seraphims = UnitType(self, Seraphims)
        self.dominions = UnitType(self, Dominions)


class HeavySupport(HeavySection):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        self.retributors = UnitType(self, Retributors)
        self.exorcist = UnitType(self, Exorcist)
        self.penitentengine = UnitType(self, PenitentEngine)


class AdeptaSororitasV3Base(Wh40kBase):
    army_id = 'adepta_sororitas_v3'
    army_name = 'Adepta Sororitas'

    def __init__(self):

        super(AdeptaSororitasV3Base, self).__init__(
            hq=HQ(parent=self),
            elites=Elites(parent=self),
            troops=Troops(parent=self),
            fast=FastAttack(parent=self),
            heavy=HeavySupport(parent=self),
            fort=Fort(parent=self),
            lords=LordsOfWar(parent=self)
        )

    def check_rules(self):
        super(AdeptaSororitasV3Base, self).check_rules()

        self.check_unit_limit(
            self.hq.jacobus.count + self.hq.priest.count, self.hq.conclave,
            "You can't have more Battle Conclaves then Confessors (including Uriah Jacobus)."
        )
        self.check_unit_limit(
            self.hq.canoness.count, self.hq.commandsquad,
            "You can't have more Sororitas Command Squads then Canonesses."
        )
        if self.hq.priest.count > 5:
            self.error('You can\'t take more then 5 Preachers.')

class AdeptaSororitasV3CAD(AdeptaSororitasV3Base, CombinedArmsDetachment):
    army_id = 'adepta_sororitas_v3_cad'
    army_name = 'Adepta Sororitas (Combined arms detachment)'

class AdeptaSororitasV3AD(AdeptaSororitasV3Base, AlliedDetachment):
    army_id = 'adepta_sororitas_v3_ad'
    army_name = 'Adepta Sororitas (Allied detachment)'

faction = 'Adepta Sororotas'

class AdeptaSororitasV3(Wh40k7ed):
    army_id = 'adepta_sororitas_v3'
    army_name = 'Adepta Sororitas'
    faction = faction
    obsolete = True

    def __init__(self):
        super(AdeptaSororitasV3, self).__init__([AdeptaSororitasV3CAD])

detachments = [AdeptaSororitasV3CAD, AdeptaSororitasV3AD]
