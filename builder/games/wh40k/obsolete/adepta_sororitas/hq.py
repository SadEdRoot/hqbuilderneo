__author__ = 'dromanow'

from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.games.wh40k.obsolete.adepta_sororitas.troops import Rhino, Immolator
from builder.games.wh40k.obsolete.adepta_sororitas.armory import *
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor


class Celestine(StaticUnit):
    name = 'Saint Celestine'
    base_points = 135
    gear = ['Armour of Saint Katherine', 'The Ardent Blade', 'Frag grenades', 'Krak grenades', 'Jump pack']


class Jacobus(StaticUnit):
    name = 'Uriah Jacobus, Protector of the Faith'
    base_points = 100
    gear = ['Flak armour', 'Bolt pistol', 'The Redeemer', 'Chainsword', 'Frag grenades', 'Krak grenades',
            'Banner of Sanctity', 'Rosarius']


class Canoness(Unit):
    name = 'Canoness'
    base_points = 65
    gear = ['Power armour', 'Frag grenades', 'Krak grenades']

    def __init__(self):
        Unit.__init__(self)

        self.wep1 = self.opt_one_of('Weapon', [
            ['Bolt pistol', 0, 'bpist'],
            ['Boltgun', 0, 'bgun'],
        ] + canoness_melee + canoness_ranged + weapon_relic)

        self.wep2 = self.opt_one_of('', canoness_melee + canoness_ranged + weapon_relic)

        self.opt = self.opt_options_list('Options', [
            ['Melta bombs', 5, 'mbomb'],
            ['Rosarius', 15, 'rs']
        ])
        self.relics = self.opt_options_list('Ecclesiarchy Relics', relic, limit=1)

    def check_rules(self):
        wep_1_relic = self.wep1.get_cur() in weapon_relic_ids
        wep_2_relic = self.wep2.get_cur() in weapon_relic_ids
        gear_relic = self.relics.is_any_selected()
        self.wep1.set_active_options(weapon_relic_ids, not wep_2_relic and not gear_relic)
        self.wep2.set_active_options(weapon_relic_ids, not wep_1_relic and not gear_relic)
        self.relics.set_active_all(not wep_1_relic and not wep_2_relic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        def is_relic(o):
            return [o.get_selected()] if o.get_cur() in weapon_relic_ids else []
        return self.relics.get_selected() + is_relic(self.wep1) + is_relic(self.wep2)


class CommandSquad(Unit):
    name = 'Sororitas Command Squad'
    common_gear = ['Power armour', 'Frag grenades', 'Krak grenades']
    model_points = 65 / 5

    class Dialogus(Unit):
        name = 'Dialogus'

        def __init__(self):
            self.base_points = CommandSquad.model_points
            self.gear = CommandSquad.common_gear + ['Loud hailer', 'Bolt pistol']

            Unit.__init__(self)
            self.relics = self.opt_options_list('Ecclesiarchy Relics', relic, limit=1)

        def get_unique_gear(self):
            return self.relics.get_selected() if self.relics.is_any_selected() else []

    class Celestian(ListSubUnit):
        name = 'Celestian'

        def __init__(self):
            self.base_points = CommandSquad.model_points
            self.gear = CommandSquad.common_gear

            ListSubUnit.__init__(self)
            self.wep1 = self.opt_one_of('Weapon', [
                ['Bolt pistol', 0, 'bpist']
            ] + melee + ranged)
            self.wep2 = self.opt_one_of('', [
                ['Boltgun', 0, 'bgun']
            ] + melee + ranged + special + heavy)
            self.opt = self.opt_options_list('Options', [
                ['Simulacrum imperialis', 10, 'simp'],
                ['Blessed banner', 15, 'bban'],
                ['Sacred Banner of the Order Militant', 40, 'sac']
            ], limit=1)

        def check_rules(self):
            spec_gear = self.opt.is_any_selected()
            self.wep1.set_visible(not spec_gear)
            self.wep2.set_visible(not spec_gear)
            if spec_gear:
                self.gear = CommandSquad.common_gear + ['Bolt pistol', 'Boltgun']
            ListSubUnit.check_rules(self)

        def get_unique_gear(self):
            return self.opt.get_selected() if self.opt.get('sac') else []

        def has_flag(self):
            return self.get_count() if any(map(self.opt.get, ['bban', 'sac'])) else 0

        def has_sim(self):
            return self.get_count() if self.opt.get('simp') else 0

    def __init__(self):
        Unit.__init__(self)
        self.cel = self.opt_units_list(self.Celestian.name, self.Celestian, 3, 5)
        self.cel.update_range(5, 5)
        self.doc = self.opt_options_list('', [['Hospitaler', self.model_points, 'doc']])
        self.speaker = self.opt_optional_sub_unit('', self.Dialogus())
        self.opt = self.opt_options_list('Options', [
            ['Melta bombs', 25, 'mbomb'],
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()])

    def check_rules(self):
        self.cel.update_range(1, 5 - (1 if self.doc.get('doc') else 0) - self.speaker.get_count())
        if self.get_count() != 5:
            self.error('Command Squad must include 5 Veterans (include Dialogus and Hospitaler) '
                       '(taken: {})'.format(self.get_count()))

        flag = sum((c.has_flag() for c in self.cel.get_units()))
        if flag > 1:
            self.error('Only one Celestian can take a banner (taken: {})'.format(flag))

        sim = sum((c.has_sim() for c in self.cel.get_units()))
        if sim > 1:
            self.error('Only one Celestian can take a Simulacrum imperialis (taken: {})'.format(sim))
        self.points.set(self.build_points(count=1))
        self.build_description(count=1, exclude=[self.transport.id, self.doc.id])
        if self.doc.get('doc'):
            self.description.add(
                ModelDescriptor('Hospitaler', points=self.model_points,
                                gear=self.common_gear + ['Chirurgeon\'s tools', 'Bolt pistol']).build(1)
            )
        self.description.add(self.transport.get_selected())

    def get_count(self):
        return (1 if self.doc.get('doc') else 0) + self.speaker.get_count() + self.cel.get_count()

    def get_unique_gear(self):
        return sum([u.get_unique_gear() for u in self.cel.get_units()], []) + \
            (self.speaker.get_unit().get_unit().get_unique_gear() if self.speaker.get_count() else [])


class Priest(Unit):
    name = 'Ministorum Priest'
    base_points = 25
    gear = ['Flak armour', 'Frag grenades', 'Krak grenades', 'Rosarius']

    def __init__(self):
        Unit.__init__(self)

        self.wep1 = self.opt_one_of('Weapon', [['Laspistol', 0, 'lpist']] + melee + priest_ranged + [
            ['Autogun', 0, 'ag'],
            ['Bolt pistol', 1, 'bpist'],
            ['Boltgun', 1, 'bgun'],
            ['Plasma gun', 15, 'plasma'],
        ])
        self.wep2 = self.opt_one_of('', [['Close combat weapon', 0, 'chsw']] + melee + priest_ranged)
        self.opt = self.opt_options_list('Options', [
            ['Melta bombs', 5, 'mbomb'],
        ])
        self.relics = self.opt_options_list('Ecclesiarchy Relics', priest_relic, limit=1)

    def get_unique_gear(self):
        return self.relics.get_selected()


class Conclave(Unit):
    name = 'Ecclesiarchy Battle Conclave'

    def __init__(self):
        Unit.__init__(self)
        self.arc = self.opt_count('Arco-flagellant', 3, 10, 10)
        self.ass = self.opt_count('Death Cult Assassin', 0, 3, 15)
        self.crus = self.opt_count('Crusader', 0, 3, 15)
        self.transport = self.opt_optional_sub_unit('Transport', [Rhino(), Immolator()], id='trans')

    def check_rules(self):
        norm_counts(3, 10, [self.ass, self.arc, self.crus])
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(
            ModelDescriptor('Arco-flagellant', points=10).add_gear('Arco-flail', count=2).build(self.arc.get())
        )
        self.description.add(
            ModelDescriptor('Death Cult Assassin', points=15).add_gear('Power sword', count=2)
            .add_gear('Flak armour').build(self.ass.get())
        )
        self.description.add(
            ModelDescriptor('Crusader', points=15)
            .add_gear('Power sword')
            .add_gear('Storm shield').
            build(self.crus.get())
        )

    def get_count(self):
        return self.ass.get() + self.arc.get() + self.crus.get()
