__author__ = 'Denis Romanov'

from builder.games.wh40k.legacy_roster import LegacyWh40k
from builder.games.wh40k.obsolete.space_marines2008.hq import *
from builder.games.wh40k.obsolete.space_marines2008.elites import *
from builder.games.wh40k.obsolete.space_marines2008.troops import *
from builder.games.wh40k.obsolete.space_marines2008.fast import *
from builder.games.wh40k.obsolete.space_marines2008.heavy import *
from functools import reduce

class SpaceMarines2008(LegacyWh40k):
    army_id = 'd23d142900d24839aa31afb109986cef'
    army_name = 'Space Marines (2008)'
    obsolete = True

    def __init__(self, secondary=False):
        LegacyWh40k.__init__(self,
            hq = [ Calgar,Sicarius,Tigurius,Cassius,Kantor,Lysander,Shrike,Vulkan,Khan,ChapterMaster,{'unit': HonourGuardSquad, 'active': False},Captain,{'unit': CommandSquad, 'active': False},Librarian,Chaplain,ForgeMaster ],
            elites = [TerminatorAssaultSquad,TerminatorSquad,SternguardVeteranSquad,VenDred,Dred,IronDred,Techmarine,{'unit': Servitors, 'active': False},TheDamned],
            troops = [ TacticalSquad, ScoutSquad,{'unit': SpaceMarineBikers, 'active': False} ],
            fast = [Stormtalon, AssaultSquad, VanguardVeteranSquad, LandSpeederSquadron, LandSpeederStorm, SpaceMarineBikers, AttackBikeSquad, ScoutBikers ],
            heavy = [StormravenGunship, LandRaider, LRCrusader, LRRedeemer, Predator, Whirlwind, Vindicator, Thunderfire, Devastators, Chronus, {'unit': VenDred, 'active': False},{'unit': Dred, 'active': False},{'unit': IronDred, 'active': False}],
            secondary=secondary
        )
        def check_hq_limit():
            count = len(self.hq.get_units()) - self.hq.count_unit(HonourGuardSquad) - self.hq.count_unit(CommandSquad)
            return self.hq.min <= count <= self.hq.max
        self.hq.check_limits = check_hq_limit

        def check_elites_limit():
            count = len(self.elites.get_units()) - self.elites.count_unit(Servitors)
            return self.elites.min <= count <= self.elites.max
        self.elites.check_limits = check_elites_limit

        def check_heavy_limit():
            count = len(self.heavy.get_units()) - self.heavy.count_unit(Chronus)
            return self.heavy.min <= count <= self.heavy.max
        self.heavy.check_limits = check_heavy_limit

        def check_troop_limit():
            min_count = len(self.troops.get_units())
            max_count = reduce(lambda val,tr: val + (2 if tr.get_count() == 10 else 1),self.troops.get_units(),0)
            return self.troops.min <= max_count and min_count <= self.heavy.max
        self.troops.check_limits = check_troop_limit
    def check_rules(self):
        honor_guards = self.hq.count_units([ChapterMaster,Kantor]) + self.hq.count_units([Calgar]) * 3
        self.hq.set_active_types([HonourGuardSquad],honor_guards > 0)
        if honor_guards < self.hq.count_units([HonourGuardSquad]):
            self.error("You can't have more Honour Guard squads then Chapter Masters (except for Calgar, he allows 3 of them).")

        command_squads = self.hq.count_units([Sicarius,Lysander,Shrike,Khan,Captain])
        self.hq.set_active_types([CommandSquad],command_squads > 0)
        if command_squads < self.hq.count_units([CommandSquad]):
            self.error("You can't have more Command squads then Captains (including Sicarius, Shrike, Lysander, Khan).")

        serv_units = self.elites.count_units([Techmarine])
        self.elites.set_active_types([Servitors],serv_units > 0)
        if serv_units < self.elites.count_units([Techmarine]):
            self.error("You can only have one unit of Servitors per Techmarine.")

        self.heavy.set_active_types([VenDred,Dred,IronDred],self.hq.count_units([ForgeMaster]) > 0)
        if self.hq.count_units([ForgeMaster]) == 0 and self.heavy.count_units([VenDred,Dred,IronDred]) > 0:
            self.error("You can only take dreadnoughts in heavy support if your army includes Master of the Forge.")
        if self.heavy.count_units([Chronus]) > 0 and self.heavy.count_units([LandRaider, LRCrusader, LRRedeemer, Predator, Whirlwind, Vindicator]) < 1:
            self.error("There must be a tank for Chronus to command.")

        bike_caps = self.hq.get_units(Captain) + self.hq.get_units(Khan)
        biker_troops = reduce(lambda val, u: val or u.has_bike(), bike_caps, False)
        self.troops.set_active_types([SpaceMarineBikers],biker_troops)
        if not biker_troops:
            if self.troops.count_units([SpaceMarineBikers]) > 0:
                self.error("Space Maine bikers cn only be taken as troops if Captain on the bike is in HQ.")
        else:
            small_squads = reduce(lambda val, u: val + 1 if u.get_count() < 5 else val, self.troops.get_units(SpaceMarineBikers), 0)
            if small_squads > 0:
                self.error("Space Marine bikers taken as troops must be at least 5 man strong (not counting Attack Bikes).")

        sh_term_lr = reduce(lambda val, u: val + 1 if u.have_lr() else val, self.elites.get_units(TerminatorSquad), 0)
        if sh_term_lr > 1:
            self.error("Only one Terminator squad can take Land Raider as a dedicated transport.")
        ass_term_lr = reduce(lambda val, u: val + 1 if u.have_lr() else val, self.elites.get_units(TerminatorAssaultSquad), 0)
        if ass_term_lr > 1:
            self.error("Only one Assault Terminator squad can take Land Raider as a dedicated transport.")
