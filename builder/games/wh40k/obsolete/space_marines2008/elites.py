__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, StaticUnit, ListSubUnit
from builder.games.wh40k.obsolete.space_marines2008.troops import Rhino,Razorback,DropPod
from builder.games.wh40k.obsolete.space_marines2008.heavy import LandRaider,LRCrusader,LRRedeemer
from builder.core.options import norm_counts
from functools import reduce

class TerminatorAssaultSquad(Unit):
    name = 'Terminator Assault Squad'

    class Sergeant(Unit):
        name = 'Terminator Sergeant'
        base_points = 40
        gear = ['Terminator armour']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [['Lightning Claws', 0], [' Thunder Hammer and Storm Shield', 0] ])

    class Terminator(ListSubUnit):
        name = 'Terminator'
        base_points = 40
        gear = ['Terminator armour']
        min = 1
        max = 9

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [['Lightning Claws', 0], [' Thunder Hammer and Storm Shield', 0] ])

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Sergeant())
        self.terms = self.opt_units_list(self.Terminator.name, self.Terminator, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(), LRCrusader(), LRRedeemer()])

    def check_rules(self):
        self.terms.update_range()
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.terms.get_count() + 1

    def have_lr(self):
        return self.transport.get_unit() is not None

class TerminatorSquad(Unit):
    name = 'Terminator Squad'

    class Terminator(ListSubUnit):
        name = 'Terminator'
        base_points = 40
        min = 1
        max = 9

        def __init__(self):
            ListSubUnit.__init__(self)
            self.ccw = self.opt_one_of('Close combat weapon',[
                ['Power fist',0,'pfist'],
                ['Chainfist',5,'chfist']
                    ])
            self.rng = self.opt_one_of('Ranged weapon',[
                ['Storm bolter',0,'sbgun'],
                ['Heavy flamer',5,'hflame'],
                ['Assault cannon',30,'asscan']
                ])
            self.cym = self.opt_options_list('Take missile launcher',[["Cyclone missile launcher", 30, 'cmlnch']])

        def has_special(self):
            return (1 if self.cym.get('cmlnch') or self.rng.get_cur() != 'sbgun' else 0) * self.count.get()

        def check_rules(self):
            self.cym.set_active(self.rng.get_cur() =='sbgun')
            self.rng.set_active_options(['hflame','asscan'], not self.cym.get('cmlnch'))
            ListSubUnit.check_rules(self)

    class TerminatorSergeant(StaticUnit):
        name = "Terminator Sergeant"
        base_points = 40
        gear = ['Terminator armour','Storm bolter','Power sword']
        static = True

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.TerminatorSergeant()
        self.terms = self.opt_units_list(self.Terminator.name, self.Terminator, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [LandRaider(), LRCrusader(), LRRedeemer()])

    def check_rules(self):
        self.terms.update_range()
        spec_limit = 1 if self.terms.get_count() < 9 else 2
        spec_total = reduce(lambda val, u: u.has_special() + val, self.terms.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Special weapon (Cyclone missile launcher, Heavy flamer, Assault cannon) is over limit ({0})'.format(spec_limit))
        self.set_points(self.build_points(count=1, base_points=self.TerminatorSergeant.base_points))
        self.build_description(add=[self.leader.get_description()])

    def get_count(self):
        return self.terms.get_count() + self.leader.get_count()

    def have_lr(self):
        return self.transport.get_unit() is not None

class SternguardVeteranSquad(Unit):
    name = 'Sternguard Veteran Squad'

    class SpaceMarineSergeant(Unit):
        name = 'Space Marine Sergeant'
        base_points = 25
        gear = ['Power armor', 'Frag grenades', 'Krak grenades']

        def __init__(self):
            Unit.__init__(self)
            weaponlist = [
                ['Chainsword',0,'chsw'],
                ['Plasma pistol',15,'ppist'],
                ['Power weapon',15,'psw'],
                ['Lightning claw',15,'lclaw'],
                ['Power fist',25,'pfist']
            ]
            combilist = [
                ['Storm bolter', 5, 'sbgun'],
                ['Combi-melta', 5, 'cmelta' ],
                ['Combi-flamer', 5, 'cflame' ],
                ['Combi-plasma', 5, 'cplasma' ]
            ]
            self.wep1 = self.opt_one_of('Weapon',
                [['Bolt pistol', 0, 'bpist' ]] + weaponlist)
            self.wep2 = self.opt_one_of('Weapon',
                [['Boltgun', 0, 'bgun' ]] + weaponlist + combilist)
            self.opt = self.opt_options_list('Options',[["Melta bombs", 5, 'mbomb']])

    class Veteran(ListSubUnit):
        name = "Veteran"
        base_points = 25
        gear = ['Power armor', 'Frag grenades', 'Krak grenades','Bolt pistol']
        min = 1
        max = 9
        specweaponlist = ['flame','mgun','hbgun','mulmgun','mlaunch','pgun','pcannon','hflame','lcannon']
        def __init__(self):
            ListSubUnit.__init__(self)
            combilist = [
                ['Storm bolter', 5, 'sbgun'],
                ['Combi-melta', 5, 'cmelta' ],
                ['Combi-flamer', 5, 'cflame' ],
                ['Combi-plasma', 5, 'cplasma' ]
                    ]
            speclist = [
                ['Flamer', 5, 'flame' ],
                ['Meltagun', 5, 'mgun' ],
                ['Heavy bolter', 5, 'hbgun' ],
                ['Multi-melta', 5, 'mulmgun' ],
                ['Missile launcher', 5, 'mlaunch' ],
                ['Plasmagun', 10, 'pgun' ],
                ['Plasma cannon', 10, 'pcannon' ],
                ['Heavy flamer', 10, 'hflame' ],
                ['Lascannon', 15, 'lcannon']
                    ]
            self.wep = self.opt_one_of('Weapon',
                [['Boltgun', 0, 'bgun' ]] + combilist + speclist)

        def has_special(self):
            return (1 if self.wep.get_cur() in self.specweaponlist else 0) * self.get_count()

    def __init__(self):
        Unit.__init__(self)
        self.sergeant = self.opt_sub_unit(self.SpaceMarineSergeant())
        self.vets = self.opt_units_list(self.Veteran.name, self.Veteran, 4, 9)
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod(), Rhino(), Razorback()])

    def check_rules(self):
        self.vets.update_range()
        spec_limit = 2
        spec_total = reduce(lambda val, u: u.has_special() + val, self.vets.get_units(), 0)
        if spec_limit < spec_total:
            self.error('Special weapon is over limit ({0})'.format(spec_limit))
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return self.vets.get_count() + self.sergeant.get_count()


class VenDred(Unit):
    name = 'Venerable Dreadnought'
    base_points = 165
    gear = ['Smoke launchers','Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon',[
            ['Multi-melta',0,'mmelta'],
            ['Twin-linked heavy flamer',0,'tlhflame'],
            ['Twin-linked heavy bolter',5,'tlhbgun'],
            ['Twin-linked autocannon',10,'tlacan'],
            ['Plasma cannon',10,'pcan'],
            ['Assault cannon',10,'asscan'],
            ['Twin-linked lascannon',30,'tllcan']
            ])
        self.wep2 = self.opt_one_of('',[
            ['Dreadnought close combat weapon',0,'dccw'],
            ['Twin-linked autocannon',10,'tlacan'],
            ['Missile launcher',10,'mlnch']
            ])
        self.bin = self.opt_one_of('',[
            ['Built-in Storm bolter',0,'sbgun'],
            ['Built-in Heavy flamer',10,'hflame']
            ])
        self.opt = self.opt_options_list('Options',[
            ["Extra armour", 15, 'earmr']
            ])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        self.bin.set_visible(self.wep2.get_cur() == 'dccw')
        Unit.check_rules(self)


class Dred(Unit):
    name = 'Dreadnought'
    base_points = 105
    gear = ['Smoke launchers','Searchlight']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon',[
            ['Multi-melta',0,'mmelta'],
            ['Twin-linked heavy flamer',0,'tlhflame'],
            ['Twin-linked heavy bolter',5,'tlhbgun'],
            ['Twin-linked autocannon',10,'tlacan'],
            ['Plasma cannon',10,'pcan'],
            ['Assault cannon',10,'asscan'],
            ['Twin-linked lascannon',30,'tllcan']
        ])
        self.wep2 = self.opt_one_of('',[
            ['Dreadnought close combat weapon',0,'dccw'],
            ['Twin-linked autocannon',10,'tlacan'],
            ['Missile launcher',10,'mlnch']
        ])
        self.bin = self.opt_one_of('',[
            ['Built-in Storm bolter',0,'sbgun'],
            ['Built-in Heavy flamer',10,'hflame']
        ])
        self.opt = self.opt_options_list('Options',[
            ["Extra armour", 15, 'earmr']
        ])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        self.bin.set_visible(self.wep2.get_cur() == 'dccw')
        Unit.check_rules(self)


class IronDred(Unit):
    name = 'Ironclad Dreadnought'
    base_points = 135
    gear = ['Smoke launchers','Searchlight','Extra armour']
    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon',[
            ['Dreadnought close combat weapon',0,'dccw'],
            ['Hurricane bolter',0,'hurrbolt']
            ])
        self.bin1 = self.opt_one_of('',[
            ['Build-in Storm bolter',0,'sbgun'],
            ['Build-in Heavy flamer',10,'hflame']
            ])
        self.wep2 = self.opt_one_of('',[
            ['Seismic hammer',0,'sham'],
            ['Chainfist',0,'chfist']
        ])
        self.bin2 = self.opt_one_of('',[
            ['Build-in Meltagun',0,'mgun'],
            ['Build-in Heavy flamer',5,'hflame']
            ])
        self.opt = self.opt_options_list('Options',
            [
                ['Hunter-killer missile',10,'hkm1'],
                ['Hunter-killer missile',10,'hkm2'],
                ["Ironclad assault launchers", 15, 'ialnch']
                ])
        self.transport = self.opt_optional_sub_unit('Transport', [DropPod()])

    def check_rules(self):
        self.bin1.set_visible(self.wep1.get_cur() == 'dccw')
        Unit.check_rules(self)


class Techmarine(Unit):
    name = 'Techmarine'
    base_points = 50
    gear = ['Frag grenades','Krak grenades','Artificar armour']

    def __init__(self):
        Unit.__init__(self)
        self.harness = self.opt_one_of('Special',[
            ['Servo-arm',0,'arm'],
            ['Servo-harness',25,'sharn']
                ])
        self.rng = self.opt_one_of('Ranged weapon',[
            ['Bolt pistol',0,'bpist'],
            ['Boltgun',0,'bgun'],
            ['Storm Bolter',5,'sbgun'],
            ['Combi-melta', 10, 'cmelta' ],
            ['Combi-flamer', 10, 'cflame' ],
            ['Combi-plasma', 10, 'cplasma' ],
            ['Plasma pistol',15,'ppist']
                ])
        self.ccw = self.opt_options_list('Close combat weapon',[
            ['Power weapon',15,'psw'],
            ['Thunder hammer', 30, 'ham' ]
                ],1)
        self.ride = self.opt_options_list('Movement options',[
            ['Space Marine Bike',35,'bike']])


class Servitors(Unit):
    name = "Servitors"
    base_points = 10

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count(self.name, 1, 5, self.base_points)
        self.hw = [
            self.opt_count('Heavy bolter', 0, 2, 20),
            self.opt_count('Multi-melta', 0, 2, 30),
            self.opt_count('Plasma cannon', 0, 2, 30)
        ]

    def check_rules(self):
        norm_counts(0, min(2, self.count.get()), self.hw)
        self.set_points(self.build_points(count=1, base_points=0))
        self.build_description(exclude=[self.count.id])
        self.description.add('Servo-Arm', self.count.get() - reduce(lambda val, h: val + h.get(), self.hw, 0))



class TheDamned(Unit):
    name = 'Legion of the Damned squad'
    gear = ['Power armor', 'Bolt pistol', 'Frag grenades', 'Krak grenades']
    class DamnedSergeant(Unit):
        name = 'Damned Sergeant'
        gear = ['Frag grenades', 'Krak grenades', 'Bolt pistol']
        base_points = 155 - 30 * 4
        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon',
                [
                    ['Boltgun', 0, 'bgun' ],
                    ['Chainsword', 0, 'chain' ],
                    ['Combi-melta', 10, 'cmelta' ],
                    ['Combi-flamer', 10, 'cflame' ],
                    ['Combi-plasma', 10, 'cplasma' ],
                    ['Stormbolter', 10, 'sbgun' ],
                    ['Plasma pistol', 15, 'ppist' ],
                    ['Power weapon', 15, 'pw' ],
                    ['Power fist', 25, 'pf' ]
                    ])

    def __init__(self):
        Unit.__init__(self)
        self.marines = self.opt_count('Damned Legionnare',4,9,30)
        self.hvy1 = self.opt_one_of('Heavy weapon',[
            ['Boltgun', 0, 'bgun' ],
            ['Flamer', 20, 'flame' ],
            ['Meltagun', 20, 'mgun' ],
            ['Plasma gun',20,'pgun']
        ])
        self.hvy2 = self.opt_one_of('',[
            ['Boltgun', 0, 'bgun' ],
            ['Heavy bolter', 10, 'hbgun' ],
            ['Missile launcher', 15, 'mlaunch' ],
            ['Plasma cannon', 20, 'pcannon' ],
            ['Lascannon', 30, 'lcannon'],
            ['Multi-melta',30,'mmgun'],
            ['Heavy flamer',30,'hflame']
        ])
        self.sergeant = self.opt_sub_unit(self.DamnedSergeant())

    def check_rules(self):
        self.set_points(self.build_points(options=[self.hvy1, self.hvy2, self.sergeant], count=1) + self.marines.points())
        self.description.reset()
        self.description.set_header(self.name, self.points.get())
        self.description.add(self.gear, self.marines.get())
        self.description.add(self.hvy1.get_selected())
        self.description.add(self.hvy2.get_selected())
        self.description.add('Boltgun', self.marines.get() - 2)
        self.description.add(self.sergeant.get_selected())

#        self.points.set(self.base_points + (self.marines.get() - 4) * 30 + self.hvy1.points() + self.hvy2.points() + self.sergeant.points())
    def get_count(self):
        return self.marines.get() + 1
