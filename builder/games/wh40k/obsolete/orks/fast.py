__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit, ListUnit, ListSubUnit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor


class Stormboyz(Unit):
    name = 'Stormboyz'

    class Nob(Unit):
        name = 'Nob'
        base_points = 22
        gear = ['Stikkbombz', 'Rokkit Pack', 'Slugga']

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Weapon', [
                ['Choppa', 0, 'choppa'],
                ['Big choppa', 5, 'bc'],
                ['Power klaw', 25, 'klaw'],
            ])
            self.opt = self.opt_options_list('', [
                ["Bosspole", 5, 'pole'],
                ["'eavy armour", 5, 'armour'],
            ])

        def set_cyborg(self, cyborg):
            self.gear = ['Stikkbombz', 'Rokkit Pack', 'Slugga']
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            Unit.check_rules(self)

        def check_rules(self):
            pass

    class BossZagstruk(StaticUnit):
        name = 'Boss Zagstruk'
        base_points = 85
        gear = ['Slugga', 'Choppa', 'Stikkbombz', 'Cybork Body', "Da Vulcha's Klaws", 'Rokkit Pack']

    def get_unique(self):
        if self.boss.get('zag'):
            return self.BossZagstruk.name
        return None

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Stormboyz', 5, 20, 12)
        self.boss = self.opt_options_list('Boss', [
            ["Nob", self.Nob.base_points, 'nob'],
            ["Boss Zagstruk", self.BossZagstruk.base_points, 'zag'],
        ], limit=1)
        self.nob_unit = self.opt_sub_unit(self.Nob())
        self.zag_unit = self.opt_sub_unit(self.BossZagstruk())
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        self.nob_unit.set_active(self.boss.get('nob'))
        self.zag_unit.set_active(self.boss.get('zag'))
        boss = 1 if self.boss.get('nob') else 0
        norm_counts(5 - boss, 20 - boss, [self.count])
        if self.nob_unit.get_unit():
            self.nob_unit.get_unit().set_cyborg(self.cyborg.get('cyborg'))
        self.set_points(self.build_points(exclude=[self.boss.id, self.cyborg.id], count=1) +
                        self.count.get() * self.cyborg.points())
        self.build_description(options=[self.nob_unit, self.zag_unit], count=1)
        self.description.add(ModelDescriptor('Stormboyz', count=self.count.get(), points=12,
                                             gear=['Stikkbombz', 'Rokkit Pack', 'Slugga', 'Choppa'])
            .add_gear_opt(self.cyborg).build())

    def get_count(self):
        return self.count.get() + self.nob_unit.get_count() + self.zag_unit.get_count()


class Warbikers(Unit):
    name = 'Warbikers'

    class Nob(Unit):
        name = 'Nob'
        base_points = 35
        gear = ['Warbike', 'Dakkagun', 'Slugga']

        def __init__(self):
            Unit.__init__(self)
            self.ccw = self.opt_one_of('Close combat weapon', [
                ['Choppa', 0, 'choppa'],
                ['Big choppa', 5, 'bc'],
                ['Power klaw', 25, 'klaw'],
            ])
            self.opt = self.opt_options_list('Options', [
                ["Bosspole", 5, 'pole'],
            ])

        def set_cyborg(self, cyborg):
            self.gear = ['Warbike', 'Dakkagun', 'Slugga']
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            Unit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)

        self.count = self.opt_count('Warbikers', 3, 12, 25)
        self.boss = self.opt_options_list('Boss', [
            ["Nob", self.Nob.base_points, 'nob'],
        ])
        self.nob_unit = self.opt_sub_unit(self.Nob())
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        self.nob_unit.set_active(self.boss.get('nob'))
        boss = 1 if self.boss.get('nob') else 0
        norm_counts(3 - boss, 12 - boss, [self.count])
        if self.nob_unit.get_unit():
            self.nob_unit.get_unit().set_cyborg(self.cyborg.get('cyborg'))
        self.set_points(self.build_points(exclude=[self.boss.id, self.cyborg.id], count=1) +
                        self.count.get() * self.cyborg.points())
        self.build_description(options=[self.nob_unit])
        self.description.add(ModelDescriptor('Warbiker', count=self.count.get(), points=25,
                                             gear=['Warbike', 'Dakkagun', 'Slugga', 'Choppa'])
            .add_gear_opt(self.cyborg).build())

    def get_count(self):
        return self.count.get() + self.nob_unit.get_count()


class Warbuggies(ListUnit):
    name = 'Warbuggies'

    class Warbuggie(ListSubUnit):
        name = 'Warbuggie'
        base_points = 30

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Twin-linked Big Shoota", 0],
                ["Twin-linked Rokkit Launcha", 5],
                ["Scorcha", 10],
            ])
            self.opt = self.opt_options_list('Options', [
                ["Red paint job", 5],
                ["Grot riggers", 5],
                ["Armor plates", 10],
                ["Wartrakk", 5],
            ])

    def __init__(self):
        ListUnit.__init__(self, self.Warbuggie, 1, 3)


class Deffkoptas(ListUnit):
    name = 'Deffkoptas'

    class Deffkopta(ListSubUnit):
        name = 'Deffkopta'
        base_points = 35

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Twin-linked Big Shoota", 0],
                ["Twin-linked Rokkit Launcha", 10],
                ["Kustom Mega-Blasta", 5],
            ])
            self.opt = self.opt_options_list('Options', [
                ["Bigbomm", 15],
                ["Buzzsaw", 25],
            ])

        def set_cyborg(self, cyborg):
            self.gear = []
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            ListSubUnit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        ListUnit.__init__(self, self.Deffkopta, 1, 5)
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        for unit in self.units.get_units():
            unit.set_cyborg(self.cyborg.get('cyborg'))
        self.units.update_range()
        self.set_points(self.build_points(count=1, exclude=[self.cyborg.id]))
        self.build_description(count=1, exclude=[self.cyborg.id])


class Dakkajet(Unit):
    name = 'Dakkajet'
    base_points = 110
    gear = ['Twin-linked Supa Shoota', 'Twin-linked Supa Shoota']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Twin-linked Supa Shoota', 10]
        ])
        self.opt = self.opt_options_list('Options', [
            ["Red paint job", 5],
            ["Flyboss", 10],
        ])


class BurnaBommer(Unit):
    name = 'Burna-Bommer'
    base_points = 125
    gear = ['Twin-linked Supa Shoota', 'Twin-linked Big Shoota', 'Burna Bomb', 'Burna Bomb']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_count('Skorcha Missile', 0, 6, 10)
        self.opt = self.opt_options_list('Options', [
            ["Red paint job", 5],
        ])


class BlitzaBommer(Unit):
    name = 'Blitza-Bommer'
    base_points = 135
    gear = ['Twin-linked Supa Shoota', 'Twin-linked Big Shoota', 'Boom Bomb', 'Boom Bomb']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ["Red paint job", 5],
        ])
