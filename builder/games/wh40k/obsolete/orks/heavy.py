__author__ = 'Denis Romanov'

from builder.core.unit import Unit, StaticUnit, ListUnit, ListSubUnit
from builder.core.options import norm_counts
from builder.core.model_descriptor import ModelDescriptor


class Battlewagon(Unit):
    name = "Battlewagon"
    base_points = 90

    def __init__(self, transport=False):
        Unit.__init__(self)
        self.big = self.opt_options_list('Weapon', [
            ["Kannon", 10, 'kan'],
            ["Lobba", 15, 'lb'],
            ["Zzap gun", 15, 'zz'],
        ], limit=1)

        self.bs = self.opt_count("Big shoota", 0, 4, 5)
        self.rl = self.opt_count("Rokkit launcha", 0, 4, 10)

        if not transport:
            self.kill = self.opt_options_list('', [["Killkannon", 60]])

        self.opt = self.opt_options_list('Options', [
            ["Deff rolla", 20, 'rolla'],
            ["Reinforced ram", 5, 'ram'],
            ["'ard case", 15],
            ["Red paint job", 5],
            ["Grot riggers", 5],
            ["Stikkbomb chukka", 5],
            ["Armor plates", 10],
            ["Boarding plank", 5],
            ["Wreckin' ball", 10],
            ["Grabbin' klaw", 5],
        ])

    def check_rules(self):
        self.opt.set_active_options(['ram'], not self.opt.get('rolla'))
        self.opt.set_active_options(['rolla'], not self.opt.get('ram'))

        norm_counts(0, 4, [self.bs, self.rl])

        Unit.check_rules(self)


class DeffDread(Unit):
    name = 'Deff Dread'
    base_points = 75
    gear = ["Drednought CCW", "Drednought CCW"]

    def __init__(self):
        Unit.__init__(self)
        self.w1 = self.opt_one_of('Weapon', [
            ["Big Shoota", 5],
            ["Rokkit Launcha", 10],
            ["Kustom mega-blasta", 15],
            ["Scorcha", 5],
            ["Drednought CCW", 15],
        ])
        self.w2 = self.opt_one_of('', [
            ["Big Shoota", 5],
            ["Rokkit Launcha", 10],
            ["Kustom mega-blasta", 15],
            ["Scorcha", 5],
            ["Drednought CCW", 15],
        ])

        self.opt = self.opt_options_list('Options', [
            ["Grot riggers", 5],
            ["Armor plates", 10],
        ])


class LootedWagon(Unit):
    name = "Looted Wagon"
    base_points = 35

    def __init__(self):
        Unit.__init__(self)
        self.big = self.opt_options_list('Weapon', [
            ["Boomgun", 70],
            ["Skorcha", 15],
        ], limit=1)

        self.bs = self.opt_count("Big shoota", 0, 2, 5)
        self.rl = self.opt_count("Rokkit launcha", 0, 2, 10)

        self.opt = self.opt_options_list('Options', [
            ["Reinforced ram", 5, 'ram'],
            ["'ard case", 10],
            ["Red paint job", 5],
            ["Grot riggers", 5],
            ["Stikkbomb chukka", 5],
            ["Armor plates", 10],
            ["Boarding plank", 5],
            ["Wreckin' ball", 10],
            ["Grabbin' klaw", 5],
        ])

    def check_rules(self):
        norm_counts(0, 2, [self.bs, self.rl])

        Unit.check_rules(self)


class Kans(ListUnit):
    name = "Killa Kans"

    class Kan(ListSubUnit):
        name = 'Killa Kan'
        base_points = 35

        def __init__(self):
            ListSubUnit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ["Big Shoota", 5],
                ["Rokkit Launcha", 15],
                ["Kustom mega-blasta", 20],
                ["Scorcha", 5],
                ["Grotzooka", 10],
            ])
            self.opt = self.opt_options_list('Options', [
                ["Grot riggers", 5],
                ["Armor plates", 10],
            ])

    def __init__(self):
        ListUnit.__init__(self, self.Kan, 1, 3)


class Gunz(Unit):
    name = "Big Gunz"
    base_points = 20

    class Runtherd(Unit):
        name = 'Runtherd'
        base_points = 10
        gear = ['Slugga', 'Squig hound']

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_one_of('Weapon', [
                ['Grabba Stikk', 0],
                ['Grot Pod', 5],
            ])

        def set_cyborg(self, cyborg):
            self.gear = ['Slugga', 'Squig hound']
            if cyborg:
                self.gear += ['Cybork body']
                self.base_points += 5
            Unit.check_rules(self)

        def check_rules(self):
            pass

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Big Gun", 1, 3, self.base_points)
        self.type = self.opt_one_of("Type", [
            ["Kannon", 0],
            ["Lobba", 5],
            ["Zzap gun", 10],
        ])
        self.runts = self.opt_count("Ammo runt", 0, 3, 3)
        self.krew = self.opt_count("Grot krew", 0, 6, 3)

        self.runth = self.opt_options_list('Runtherd', [
            ['Add Runtherd', 10, 'add']
        ])
        self.runth_unit = self.opt_sub_unit(self.Runtherd())
        self.cyborg = self.opt_options_list('Grotsnik\'s upgrade', [
            ["Cybork body", 5, 'cyborg'],
        ])

    def check_rules(self):
        self.cyborg.set_active_options(['cyborg'], self.get_roster().has_grotsnik())
        self.runth_unit.set_active(self.runth.get('add'))
        if self.runth_unit.get_unit():
            self.runth_unit.get_unit().set_cyborg(self.cyborg.get('cyborg'))
        krew_count = self.count.get() * 2 + self.krew.get()
        self.set_points((self.base_points + self.type.points()) * self.count.get() + self.runts.points() +
                        self.krew.points() + self.runth_unit.points() + (self.cyborg.points() * krew_count))
        self.build_description(options=[self.runth_unit, self.runts])
        self.description.add(ModelDescriptor("Big Gun", points=20).add_gear_opt(self.type).build(self.count.get()))
        self.description.add(ModelDescriptor("Gretchin", points=3).add_gear_opt(self.cyborg).build(krew_count))

    def get_count(self):
        return self.count.get()


class FlashGitz(Unit):
    name = 'Flash Gitz'
    base_points = 25

    class Painboy(Unit):
        name = 'Painboy'
        base_points = 55
        gear = ['Urty syringe', "Dok's tools"]

        def __init__(self):
            Unit.__init__(self)
            self.opt = self.opt_options_list('Options', [["Grot orderly", 5]])

        def set_cyborg(self, opt):
            self.set_points(self.build_points() + opt.points())
            self.build_description()
            self.description.add(opt.get_selected())

        def check_rules(self):
            pass

    class KaptinBadrukk(StaticUnit):
        name = 'Kaptin Badrukk'
        base_points = 135
        gear = ['Goldtoof armour', 'Da Rippa', 'Powder Grot', 'Powder Grot', 'Powder Grot', 'Bosspole',
                'Slugga', 'Choppa', 'Gitfinda', 'Stikkbombs']

    def get_unique(self):
        if self.kaptin.get('kaptin'):
            return self.KaptinBadrukk.name
        return None

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count("Flash Gitz", 5, 10, 25)
        self.opt = self.opt_options_list("Upgrade", [
            ['More Dakka', 5],
            ['Shootier', 5],
            ['Blastas', 5]
        ])
        self.runts = self.opt_count("Ammo runt", 0, 3, 5)
        self.pain = self.opt_options_list('', [["Painboy", self.Painboy.base_points, 'up']])
        self.pain_unit = self.opt_sub_unit(self.Painboy())
        self.cyborg = self.opt_options_list('Options', [
            ["Cybork body", 5, 'cyborg'],
        ], id='cybork')
        self.kaptin = self.opt_options_list('Kaptin Badrukk', [['Add Kaptin Badrukk', 135, 'kaptin']])
        self.kaptin_unit = self.opt_sub_unit(self.KaptinBadrukk())

    def check_rules(self):
        self.pain_unit.set_active(self.pain.get('up'))
        self.cyborg.set_visible(self.pain.get('up') or self.get_roster().has_grotsnik())
        self.kaptin_unit.set_active(self.kaptin.get('kaptin'))
        painboy = 1 if self.pain.get('up') else 0
        norm_counts(5 - painboy, 10 - painboy, [self.count], 4)
        self.pain_unit.get_unit().set_cyborg(self.cyborg)

        self.set_points(self.build_points(count=1, exclude=[self.pain.id, self.kaptin.id, self.opt.id, self.cyborg.id],
                                          base_points=(self.cyborg.points() + self.opt.points()) * self.count.get()))
        self.build_description(count=1, options=[self.kaptin_unit, self.pain_unit, self.runts])
        self.description.add(ModelDescriptor(
            name='Flash Gitz',
            points=25,
            count=self.count.get(),
            gear=["'Eavy Armour", "Snazzgun", "Gitfinda"]).add_gear_opt(self.opt).add_gear_opt(self.cyborg).build()
        )

    def get_count(self):
        return self.count.get() + self.pain_unit.get_count() + self.kaptin_unit.get_count()
