__author__ = 'Ivan Truskov'


from builder.games.wh40k.obsolete.orks import Orks
from builder.games.wh40k.obsolete.orks.orks_ex.hq import *
from builder.games.wh40k.obsolete.orks.hq import WazdakkaGutsmek
from builder.games.wh40k.obsolete.orks.heavy import Kans,DeffDread,Gunz


class OrksExtended(Orks):
    def __init__(self):
        Orks.__init__(self,[Zhadsnark,Buzzgob])

    def has_biker_boss(self):
        return self.hq.count_units([WazdakkaGutsmek,Zhadsnark]) > 0

    def biker_error(self):
        self.error("You can't have Warbikers in Troops without Wazdakka Gutsmek or Zhadsnark in HQ.")
    def count_big_meks(self):
        return self.hq.count_units([BigMek,Buzzgob])
    def check_rules(self):
        Orks.check_rules(self)
        if self.hq.count_unit(Zhadsnark) > 0:
            if self.troops.count_unit(DeffDread) + self.heavy.count_unit(DeffDread) > 0:
                self.error("Army lead by Zhadsnark cannot include any Deff Dreads.")
            if self.heavy.count_unit(Kans) > 0:
                self.error("Army lead by Zhadsnark cannot include any Killa Kans.")
            if self.heavy.count_unit(Gunz) > 0:
                self.error("Army lead by Zhadsnark cannot include any Big Gunz.")
