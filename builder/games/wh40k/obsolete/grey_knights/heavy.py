__author__ = 'Ivan Truskov'

from builder.core.unit import Unit, ListSubUnit
from builder.games.wh40k.obsolete.grey_knights.elites import Rhino, Razorback


class Purgations(Unit):
    name = 'Purgation squad'

    class Justicar(Unit):
        name = "Justicar"
        base_points = 20
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

        def __init__(self):
            Unit.__init__(self)
            self.urwep = self.opt_options_list('Weapon', [
                ['Master-crafted storm bolter', 5, 'uwep']
            ])
            self.wep = self.opt_one_of('Weapon', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 5, 'nfhlb'],
                ['Nemesis Daemon Hammer', 10, 'nham'],
                ['Pair of Nemesis falcions', 10, 'nfalc'],
                ['Nemesis warding stave', 25, 'stave']
            ])
            self.uwep = self.opt_options_list('', [
                ['Make master-crafted', 5, 'uwep']
            ])
            self.opt = self.opt_options_list('Options', [
                ['Teleport homer', 5, 'thmr']
            ])

        def has_stave(self):
            return 1 if self.wep.get_cur() == 'stave' else 0

        def check_rules(self):
            self.set_points(self.build_points())
            self.build_description(exclude=[self.urwep.id, self.uwep.id, self.wep.id])
            if self.urwep.get('uwep'):
                self.description.add('Master-crafted Storm bolter')
            else:
                self.description.add('Storm bolter')
            if self.uwep.get('uwep'):
                self.description.add('Master-crafted ' + self.wep.get_selected())
            else:
                self.description.add(self.wep.get_selected())

    class Knight(ListSubUnit):
        name = 'Grey Knight'
        base_points = 20
        gear = ['Power armour', 'Frag grenades', 'Krak grenades', 'Psyk-out grenades']

        def __init__(self):
            ListSubUnit.__init__(self)
            self.rng = self.opt_one_of('Ranged weapon', [
                ['Storm bolter', 0, 'sbgun'],
                ['Incinerator', 20, 'inc'],
                ['Psilencer', 10, 'psil'],
                ['Psycannon', 20, 'pcan']
            ])
            self.wep = self.opt_one_of('Close combar weapon', [
                ['Nemesis force sword', 0, 'nfsw'],
                ['Nemesis force halberd', 5, 'nfhlb'],
                ['Nemesis Daemon Hammer', 10, 'nham'],
                ['Pair of Nemesis falcions', 10, 'nfalc'],
                ['Nemesis warding stave', 25, 'stave']
            ])

        def has_stave(self):
            return self.wep.get_cur() == 'stave'

        def has_hvy(self):
            return self.rng.get_cur() != 'sbgun'

        def check_rules(self):
            self.wep.set_active(not self.has_hvy())
            ListSubUnit.check_rules(self)

    def __init__(self):
        Unit.__init__(self)
        self.leader = self.opt_sub_unit(self.Justicar())
        self.squad = self.opt_units_list(self.Knight.name, self.Knight, 4, 9)
        self.opt = self.opt_options_list('Options', [['Psybolt ammunition', 20, 'pba']])
        self.ride = self.opt_optional_sub_unit('Transport', [Rhino(), Razorback()])

    def check_rules(self):
        self.squad.update_range()
        fullunit = self.squad.get_units() + [self.leader.get_unit()]
        stave_cnt = sum((cur.has_stave() for cur in fullunit))
        if stave_cnt > 1:
            self.error('Only one Warding Stave may be present in unit. Taken: {0}'.format(stave_cnt))
        hvy_lim = 4
        hvy_cnt = sum((cur.has_hvy() for cur in self.squad.get_units()))
        if hvy_cnt > hvy_lim:
            self.error("In squad of {0} models only {1} special weapons are allowed. "
                       "Taken: {2}".format(self.get_count(), hvy_lim, hvy_cnt))
        self.set_points(self.build_points(count=1))
        self.build_description()

    def get_count(self):
        return 1 + self.squad.get_count()


class Dred(Unit):
    name = 'Dreadnought'
    base_points = 115
    gear = ['Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_one_of('Weapon', [
            ['Multi-melta', 0, 'mmelta'],
            ['Twin-linked heavy flamer', 0, 'tlhflame'],
            ['Twin-linked heavy bolter', 5, 'tlhbgun'],
            ['Twin-linked autocannon', 10, 'tlacan'],
            ['Plasma cannon', 10, 'pcan'],
            ['Assault cannon', 10, 'asscan'],
            ['Twin-linked lascannon', 30, 'tllcan']
        ])
        self.wep2 = self.opt_one_of('', [
            ['Nemesis doomfist', 0, 'dccw'],
            ['Twin-linked autocannon', 5, 'tlacan'],
            ['Missile launcher', 5, 'mlnch']
        ])
        self.bin = self.opt_one_of('', [
            ['Built-in Storm bolter', 0, 'sbgun'],
            ['Built-in Heavy flamer', 10, 'hflame']
        ])
        self.opt = self.opt_options_list('Options', [
            ['Searchlight', 1, 'sl'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Psyflame ammunition', 5, 'pfl'],
            ['Warp stabilisation field', 5, 'wsf'],
            ['Truesilver armour', 10, 'tsa'],
            ["Extra armour", 15, 'exarm']
        ])

    def check_rules(self):
        self.bin.set_visible(self.wep2.get_cur() == 'dccw')
        Unit.check_rules(self)


class Dreadknight(Unit):
    name = 'Nemesis Dreadknight'
    gear = ['Dreadknight Armour', 'Nemesis Doomfist']
    base_points = 130

    def __init__(self):
        Unit.__init__(self)
        self.wep1 = self.opt_options_list('Weapons', [
            ['Heavy incinerator', 30, 'hinc'],
            ['Gatling psilencer', 35, 'gpsyl'],
            ['Heavy psycannon', 40, 'hpcan']
        ], 2)
        self.wep2 = self.opt_one_of('', [
            ['Nemesis Doomfist', 0, 'ndf'],
            ['Nemesis Daemon Hammer', 10, 'nham'],
            ['Nemesis greatsword', 25, 'nsw']
        ])
        self.opt = self.opt_options_list('Options', [['Personal teleporter', 75, 'ptlp']])


class LandRaider(Unit):
    name = "Land Raider"
    base_points = 255
    gear = ['Twin-linked heavy bolter', 'Twin-linked lascannon', 'Twin-linked lascannon', 'Smoke launchers']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Searchlight', 1, 'sl'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Warp stabilisation field', 5, 'wsf'],
            ["Hunter-killer missile", 10, 'hkm'],
            ['Multi-melta', 10, 'mmgun'],
            ["Storm bolter", 10, 'sbgun'],
            ['Truesilver armour', 10, 'tsa'],
            ["Extra armour", 15, 'exarm']
        ])


class LRCrusader(Unit):
    name = "Land Raider Crusader"
    base_points = 255
    gear = ['Twin-linked assault cannon', 'Hurricane bolter', 'Hurricane bolter', 'Smoke launchers',
            'Frag Assault Launcher']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Searchlight', 1, 'sl'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Warp stabilisation field', 5, 'wsf'],
            ["Hunter-killer missile", 10, 'hkm'],
            ['Multi-melta', 10, 'mmgun'],
            ["Storm bolter", 10, 'sbgun'],
            ['Truesilver armour', 10, 'tsa'],
            ["Extra armour", 15, 'exarm']
        ])


class LRRedeemer(Unit):
    name = "Land Raider Redeemer"
    base_points = 245
    gear = ['Twin-linked assault cannon', 'Flamestorm cannon', 'Flamestorm cannon', 'Smoke launchers', 'Searchlight',
            'Frag Assault Launcher']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Searchlight', 1, 'sl'],
            ['Psybolt ammunition', 5, 'pba'],
            ['Warp stabilisation field', 5, 'wsf'],
            ["Hunter-killer missile", 10, 'hkm'],
            ['Multi-melta', 10, 'mmgun'],
            ["Storm bolter", 10, 'sbgun'],
            ['Truesilver armour', 10, 'tsa'],
            ["Extra armour", 15, 'exarm'],
            ['Psyflame ammunition', 15, 'psf']
        ])
