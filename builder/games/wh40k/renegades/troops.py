__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear,\
    Count, SubUnit, OptionalSubUnit, UnitDescription,\
    ListSubUnit, UnitList
from .hq import RenegadeCommand, Covenant
from builder.games.wh40k.roster import Unit


class RenegadePlatoon(Unit):
    type_name = 'Renegade Infantry Platoon'
    type_id = 'r_platoon_v1'

    class UnitOptions(OptionsList):
        def __init__(self, parent):
            super(RenegadePlatoon.UnitOptions, self).__init__(parent, 'Platoon options')
            self.fan = self.variant('Fanatic', 20)
            self.fnp = self.variant('Feel No Pain (6+)', 10)
            self.flak = self.variant('Flak armour', 10, gear=[])

        def check_rules(self):
            super(RenegadePlatoon.UnitOptions, self).check_rules()
            self.fan.used = self.fan.visible = self.roster.demagogue.is_revolutionary()
            self.fnp.used = self.fnp.visible = self.roster.demagogue.is_heretek()
            self.flak.used = self.flak.visible = self.roster.demagogue.is_reaver()

    class Command(Unit):
        subunit_name = 'Platoon Command Squad'
        champ_name = 'Demagogue'
        vox_name = 'Command net vox'

        class SquadOptions(OptionsList):
            def __init__(self, parent):
                super(RenegadePlatoon.Command.SquadOptions, self).__init__(parent, 'Squad options')
                self.variant('Sub-flak armour', 5)
                self.mil = self.variant('Militia training', 10)
                self.variant('Krak grenades', 10)

            def check_rules(self):
                super(RenegadePlatoon.Command.SquadOptions, self).check_rules()
                if self.roster.demagogue.is_reaver():
                    self.mil.value = True
                self.mil.active = not self.roster.demagogue.is_reaver()

        class Weapon(OneOf):
            def __init__(self, parent, text='Weapon'):
                super(RenegadePlatoon.Command.Weapon, self).__init__(parent, text)
                self.variant('Autopistol')
                self.variant('Laspistol')
                self.variant('Stub gun')
                self.variant('Autogun')
                self.variant('Lasgun')
                self.variant('Shotgun')

        class Demagogue(Unit):
            class Options(OptionsList):
                def __init__(self, parent):
                    super(RenegadePlatoon.Command.Demagogue.Options, self).__init__(parent, 'Options')
                    self.variant('Caparace armour', 10)
                    self.variant('Melta bombs', 5)
                    self.variant('Power weapon', 15)
                    self.variant('Power fist', 25)
                    self.variant('Bolt pistol', 5)

            def __init__(self, parent):
                super(RenegadePlatoon.Command.Demagogue, self).__init__(
                    parent, parent.champ_name, 3 + 5, [Gear('Close combat weapon'), Gear('Frag grenades')])
                RenegadePlatoon.Command.Weapon(self)
                self.Options(self)
                self.cov = Covenant(self, 20)

        class Renegade(ListSubUnit):
            class SpecialWeapon(OptionsList):
                def __init__(self, parent):
                    super(RenegadePlatoon.Command.Renegade.SpecialWeapon, self).__init__(
                        parent, 'Special wargear', limit=1)
                    self.variant('Flamer', 5)
                    self.variant('Grenade launcher', 5)
                    self.variant('Meltagun', 10)
                    self.variant('Plasma gun', 15)
                    self.vox = self.variant(parent.root_unit.vox_name, 5)
                    self.sigil = self.variant('Chaos Sigil', 5)

            def __init__(self, parent):
                super(RenegadePlatoon.Command.Renegade, self).__init__(
                    parent, 'Renegade', 3, [Gear('Close combat weapon'), Gear('Frag grenades')])
                RenegadePlatoon.Command.Weapon(self)
                self.opt = self.SpecialWeapon(self)

            @ListSubUnit.count_gear
            def has_special(self):
                return self.opt.any

            @ListSubUnit.count_gear
            def has_vox(self):
                return self.opt.vox.value

            @ListSubUnit.count_gear
            def has_sigil(self):
                return self.opt.sigil.value

        class HeavyWeaponsTeam(ListSubUnit):
            class HeavyWeapon(OneOf):
                def __init__(self, parent):
                    super(RenegadePlatoon.Command.HeavyWeaponsTeam.HeavyWeapon, self).__init__(parent, 'Weapon')
                    self.variant('Heavy stubber', 5),
                    self.variant('Heavy bolter', 10),
                    self.variant('Autocannon', 10)
                    self.ml = self.variant('Missile launcher', 15)
                    self.variant('Lascannon', 20)

            def __init__(self, parent):
                super(RenegadePlatoon.Command.HeavyWeaponsTeam, self).__init__(
                    parent, 'Renegade Weapons Team', 6,
                    [Gear('Close combat weapon'), Gear('Frag grenades')])
                self.HeavyWeapon(self)

        class Upgrades(OptionsList):
            def __init__(self, parent):
                super(RenegadePlatoon.Command.Upgrades, self).__init__(parent, 'Upgrades')
                self.boss = self.variant(parent.champ_name, 5, gear=[])
                self.hwt = self.variant('Weapons Team', gear=[])

            @property
            def points(self):
                return 0

        def __init__(self, parent):
            super(RenegadePlatoon.Command, self).__init__(parent, self.subunit_name)
            self.opt = self.SquadOptions(self)
            self.up = self.Upgrades(self)
            self.boss = SubUnit(self, self.Demagogue(parent=self))
            self.troopers = UnitList(self, self.Renegade, 8, 30)
            self.hwt = UnitList(self, self.HeavyWeaponsTeam, 1, 3)
            self.transport = RenegadeCommand.Transport(self)

        def build_description(self):
            res = super(RenegadePlatoon.Command, self).build_description()
            root = self.parent.parent
            if root.opt.flak.used and root.opt.flak.value:
                res.add(Gear('Flak armour'))
            return res

        def build_statistics(self):
            return self.note_transport({
                'Models': self.up.boss.value + self.hwt.used * self.hwt.count + self.troopers.count,
                'Units': 1
            })

        def check_rules(self):
            super(RenegadePlatoon.Command, self).check_rules()
            self.boss.used = self.boss.visible = self.up.boss.value
            self.hwt.used = self.hwt.visible = self.up.hwt.value
            diff = self.boss.used + 2 * self.hwt.count * self.hwt.used
            self.troopers.update_range(10 - diff,
                                       (30 if self.roster.demagogue.is_horde() else 20) - diff)
            self.hwt.update_range(1, (self.boss.used + self.troopers.count) / 8)
            self.transport.used = self.transport.visible = (self.troopers.count + diff) <= 12
            spec_limit = (self.troopers.count + diff) / 5
            if sum(u.has_special() for u in self.troopers.units) > spec_limit:
                self.error("Only {} Renegades can take special wargear in this squad".format(spec_limit))
            if sum(u.has_vox() for u in self.troopers.units) > 1:
                self.error("Only one Renegade in squad may be a vox operator")
            if sum(u.has_sigil() for u in self.troopers.units) > 1:
                self.error("Only one Renegade in squad may carry Chaos Sigil")

        def not_nurgle(self):
            return self.boss.champ.unit.cov.not_nurgle()

    class Infantry(Command, ListSubUnit):
        subunit_name = 'Renegade Infantry Squad'
        champ_name = 'Renegade Champion'
        vox_name = 'Vox-caster'

        def build_description(self):
            res = super(RenegadePlatoon.Command, self).build_description()
            root = self.root_unit
            if root.opt.flak.used and root.opt.flak.value:
                res.add(Gear('Flak armour'))
            return res

    def __init__(self, parent):
        super(RenegadePlatoon, self).__init__(parent)
        self.opt = self.UnitOptions(self)
        self.command = SubUnit(self, self.Command(parent=self))
        self.infantry = UnitList(self, self.Infantry, 2, 4)

    def build_statistics(self):
        res = self.command.unit.build_statistics()
        for unit in self.infantry.units:
            inf_stat = unit.build_statistics()
            res['Models'] += unit.models.cur * inf_stat['Models']
            res['Units'] += unit.models.cur * inf_stat['Units']
        return res

    def not_nurgle(self):
        if self.command.unit.up.boss.value:
            return self.command.unit.boss.unit.cov.not_nurgle()
        else:
            return False


class Mutants(Unit):
    type_name = 'Renegade Mutant Rabble'
    type_id = 'r_mutants_v1'

    class Armour(OptionsList):
        def __init__(self, parent):
            super(Mutants.Armour, self).__init__(parent, 'Armour', limit=1)
            self.subflak = self.variant('Sub-flak armour', 10, gear=[])
            self.flak = self.variant('Flak armour', 10, gear=[])

        def check_rules(self):
            self.flak.used = self.flak.visible = self.roster.demagogue.is_reaver()

    class Champion(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(Mutants.Champion.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)
                self.variant('Power weapon', 15)

        def __init__(self, parent, armour):
            super(Mutants.Champion, self).__init__(parent, 'Mutant Champion', 3 + 5,
                                                   [Gear('Close combat weapon'), Gear('Frag grenades')])
            self.armour = armour
            self.Options(self)
            RenegadePlatoon.Command.Weapon(self)
            self.cov = Covenant(self, 15)

        def buid_description(self):
            res = super(Mutants.Champion, self).buid_description()
            if self.armour.subflak.value:
                res.add(Gear('Sub-flak armour'))
            if self.armour.flak.used and self.armour.flak.value:
                res.add(Gear('Flak armour'))
            return res

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(Mutants.Leader, self).__init__(parent, 'Leader')
            self.champ = SubUnit(self, Mutants.Champion(self, parent.armour))

    def __init__(self, parent):
        super(Mutants, self).__init__(parent)
        self.armour = self.Armour(self)
        self.boss = self.Leader(self)
        self.weapons = []
        self.models = Count(self, 'Mutants', 10, 50, 3, True)
        for wep_name in ['Laspistol', 'Stub gun'
                         'Lasgun', 'Autogun', 'Shutgun']:
            self.weapons.append(Count(self, wep_name, 0, 10))

    def check_rules(self):
        super(Mutants, self).check_rules()
        self.models.min = 10 - self.boss.any
        self.models.max = 50 - self.boss.any
        Count.norm_counts(0, self.models.cur, self.weapons)

    def get_count(self):
        return self.models.cur + self.boss.count

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.count)
        res.add(self.boss.description)
        model = UnitDescription('Mutant', 3,
                                options=[Gear('Close combat weapon'), Gear('Frag grenades')])
        if self.armour.subflak.value:
            model.add(Gear('Sub-flak armour'))
        if self.armour.flak.used and self.armour.flak.value:
            model.add(Gear('Flak armour'))

        apist_count = self.models.cur
        for cnt in self.weapons:
            if cnt.cur:
                res.add(model.clone().add(cnt.gear).set_count(cnt.cur))
        if apist_count:
            res.add(model.clone().add(Gear('Autopistol')).set_count(apist_count))
        return res

    def not_nurgle(self):
        if self.boss.count:
            return self.boss.champ.unit.cov.not_nurgle()
        else:
            return False


class Veterans(Unit):
    type_name = 'Renegade Infantry Veterans'
    type_id = 'r_veterans_v1'
    default_gear = [Gear('Close combat weapon'), Gear('Frag grenades'),
                    Gear('Krak grenades')]

    class SquadOptions(OptionsList):
        def __init__(self, parent):
            super(Veterans.SquadOptions, self).__init__(parent, 'Squad options')
            self.fnp = self.variant('Feel No Pain (6+)', 10)
            self.fan = self.variant('Fanatic', 20)
            self.gren = self.variant('Upgrade to Grenadeurs', 15, gear=[])
            self.cap = self.variant('Caparace armour', 20, gear=[])
            self.spec = [
                self.cap,
                self.variant('Scout special rule', 15),
                self.variant('Furious charge special rule', 25),
                self.variant('Deep Strike special rule', 15),
                self.variant('Tank Hunters special rule', 15)
            ]

        def check_rules(self):
            super(Veterans.SquadOptions, self).check_rules()
            OptionsList.process_limit(self.spec, 1)
            self.gren.used = self.gren.visible = self.roster.demagogue.is_reaver()
            self.fnp.used = self.fnp.visible = self.roster.demagogue.is_heretek()
            self.fan.used = self.fan.visible = self.roster.demagogue.is_revolutionary()

    class Weapon(OneOf):
        def __init__(self, parent, text='Weapon'):
            super(Veterans.Weapon, self).__init__(parent, text)
            self.cold = [self.variant('Autopistol'),
                         self.variant('Laspistol'),
                         self.variant('Stub gun')]
            self.hot = [self.variant('Hot-shot laspistol'),
                        self.variant('Hot-shot lasgun')]
            self.variant('Autogun')
            self.variant('Lasgun')
            self.variant('Shotgun')
            self.volley = self.variant('Hot-shot volley gun', 10)
            self.hot += [self.volley]

        def equip_grenadeur(self, equip=True):
            for opt in self.cold:
                opt.active = not equip
            for opt in self.hot:
                opt.active = equip

    class Champion(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(Veterans.Champion.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)
                self.variant('Power weapon', 15)
                self.variant('Power fist', 25)
                self.variant('Bolt pistol', 5)

        def __init__(self, parent, squad_options):
            super(Veterans.Champion, self).__init__(
                parent, 'Renegade Champion', 10 + 5)
            self.squad_options = squad_options

            self.wep = Veterans.Weapon(self)
            self.Options(self)
            self.cov = Covenant(self, 10)

        def check_rules(self):
            super(Veterans.Champion, self).check_rules()
            self.gear = [Gear('Caparace armour')
                         if self.squad_options.cap.value
                         else Gear('Flak armour')] + Veterans.default_gear
            self.wep.equip_grenadeur(self.squad_options.gren.used and self.squad_options.gren.value)

        def has_popgun(self):
            return self.wep.cur == self.wep.volley

    class Veteran(ListSubUnit):
        class SpecialWeapon(OptionsList):
            def __init__(self, parent):
                super(Veterans.Veteran.SpecialWeapon, self).__init__(
                    parent, 'Special wargear', limit=1)
                self.variant('Flamer', 5)
                self.variant('Grenade launcher', 5)
                self.variant('Meltagun', 10)
                self.variant('Plasma gun', 15)
                self.vox = self.variant('Vox-caster', 5)
                self.sigil = self.variant('Chaos Sigil', 5)

        def __init__(self, parent):
            super(Veterans.Veteran, self).__init__(parent, 'Renegade Veteran', 10)
            self.wep = Veterans.Weapon(self)
            self.spec = self.SpecialWeapon(self)

        def check_rules(self):
            super(Veterans.Veteran, self).check_rules()
            self.gear = [Gear('Caparace armour')
                         if self.root_unit.squad_options.cap.value
                         else Gear('Flak armour')] + Veterans.default_gear
            self.wep.equip_grenadeur(self.root_unit.squad_options.gren.used and self.root_unit.squad_options.gren.value)

        def build_description(self):
            res = super(Veterans.Veteran, self).build_description()
            if self.root_unit.squad_options.gren.used\
               and self.root_unit.squad_options.gren.value:
                res.name = 'Renegade Grenadeur'
            return res

        @ListSubUnit.count_gear
        def has_special(self):
            return self.spec.any

        @ListSubUnit.count_gear
        def has_vox(self):
            return self.spec.vox.value

        @ListSubUnit.count_gear
        def has_sigil(self):
            return self.spec.sigil.value

        @ListSubUnit.count_gear
        def has_popgun(self):
            return self.wep.cur == self.wep.volley

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(Veterans.Leader, self).__init__(parent, 'Commander')
            self.boss = SubUnit(self, Veterans.Champion(parent=None, squad_options=parent.squad_options))

    def __init__(self, parent):
        # 35 for 5 10-point veterans...
        super(Veterans, self).__init__(parent, points=35 - 10 * 5)
        self.squad_options = self.SquadOptions(self)
        self.boss = self.Leader(self)
        self.models = UnitList(self, self.Veteran, 5, 10)
        self.transport = RenegadeCommand.Transport(self)

    def check_rules(self):
        super(Veterans, self).check_rules()
        self.models.min = 5 - self.boss.count
        self.models.max = 10 - self.boss.count
        spec_limit = (self.models.count + self.boss.count) / 5
        if sum(u.has_special() for u in self.models.units) > spec_limit:
            self.error("Only {} Veteran(s) can take special wargear in this squad".format(spec_limit))
        if sum(u.has_vox() for u in self.models.units) > 1:
            self.error("Only one Veteran in squad may be a vox operator")
        if sum(u.has_sigil() for u in self.models.units) > 1:
            self.error("Only one Veteran in squad may carry Chaos Sigil")

        if self.squad_options.gren.used and self.squad_options.gren.value:
            boss_pop = self.boss.any and self.boss.boss.unit.has_popgun()
            if sum((u.has_popgun() for u in self.models.units), boss_pop) > spec_limit:
                self.error("Only {} Veterans can take hot-shot volley guns in this squad".format(spec_limit))

    def build_description(self):
        res = super(Veterans, self).build_description()
        if self.squad_options.gren.used\
           and self.squad_options.gren.value:
            res.name = 'Renegade Grenadeurs'
        return res

    def get_count(self):
        return self.models.count + self.boss.count

    def build_statistics(self):
        return self.note_transport(super(Veterans, self).build_statistics())

    def not_nurgle(self):
        if self.boss.count:
            return self.boss.boss.unit.cov.not_nurgle()
        else:
            return False


class Zombies(Unit):
    type_name = 'Plague Zombie Horde'
    type_id = 'r_zombies_v1'

    def __init__(self, parent):
        super(Zombies, self).__init__(parent)
        self.models = Count(self, 'Plague Zombie', 10, 50, 3, True,
                            gear=UnitDescription('Plague Zombie', 3,
                                                 options=[Gear('Close combat weapon')]))

    def get_count(self):
        return self.models.cur
