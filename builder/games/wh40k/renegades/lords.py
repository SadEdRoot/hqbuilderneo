__author__ = 'Ivan Truskov'


from builder.core2 import OptionsList, OneOf, Gear,\
     ListSubUnit, UnitList
from builder.games.wh40k.roster import Unit


class Malkador(Unit):
    type_name = 'Renegade Malkador Heavy Tank'
    type_id = 'r_malkador_v1'

    class Vehicle(OptionsList):
        def __init__(self, parent):
            super(Malkador.Vehicle, self).__init__(parent, 'Options')
            self.mt = self.variant('Militia training', 10)
            self.sb = self.variant('Storm bolter', 10)
            self.hs = self.variant('Heavy stubber', 10)
            self.variant('Hunter-killer missile', 10)

        def check_rules(self):
            super(Malkador.Vehicle, self).check_rules()
            self.process_limit([self.hs, self.sb], 1)
            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True

    class HullMounted(OneOf):
        def __init__(self, parent):
            super(Malkador.HullMounted, self).__init__(parent, 'Hull-mounted weapon')
            self.variant('Heavy bolter')
            self.variant('Lascannon', 15)
            self.variant('Autocannon', 5)

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Malkador.Sponsons, self).__init__(parent, 'Sponsons')
            self.variant('Heavy stubbers', 0, gear=[Gear('Heavy stubber', count=2)])
            self.variant('Lascannon', 30, gear=[Gear('Lascannon', count=2)])
            self.variant('Heavy bolters', 10, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Autocannon', 20, gear=[Gear('Autocannon', count=2)])

    def __init__(self, parent):
        super(Malkador, self).__init__(parent, 'Renegade Malkador', 225,
                                       [Gear('Searchlight'), Gear('Battle cannon')])
        self.HullMounted(self)
        self.Sponsons(self)
        self.Vehicle(self)


class Minotaur(Unit):
    type_name = 'Renegade Minotaur Artillery Tank'
    type_id = 'r_minotaur_v1'

    class Vehicle(OptionsList):
        def __init__(self, parent):
            super(Minotaur.Vehicle, self).__init__(parent, 'Options')
            self.variant('Enclosed Crew Compartments', 15)
            self.mt = self.variant('Militia training', 10)
            self.sb = self.variant('Storm bolter', 10)
            self.hs = self.variant('Heavy stubber', 10)
            self.variant('Hunter-killer missile', 10)

        def check_rules(self):
            super(Minotaur.Vehicle, self).check_rules()
            self.process_limit([self.hs, self.sb], 1)
            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True

    def __init__(self, parent):
        super(Minotaur, self).__init__(parent, 'Renegade Minotaur', 260, gear=[
            Gear('Double Earthshaker cannon'), Gear('Searchlight'), Gear('Smoke launchers')
        ])
        self.Vehicle(self)


class MalkadorDefender(Unit):
    type_name = "Renegade Malkador 'Defender'"
    type_id = 'r_defender_v1'

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(MalkadorDefender.Sponsons, self).__init__(parent, 'Sponsons')
            self.variant('Heavy bolters', 0, gear=[Gear('Heavy bolter', count=2)])
            self.variant('Heavy stubbers', 0, gear=[Gear('Heavy stubber', count=2)])
            self.variant('Lascannon', 30, gear=[Gear('Lascannon', count=2)])
            self.variant('Autocannon', 20, gear=[Gear('Autocannon', count=2)])

    def __init__(self, parent):
        super(MalkadorDefender, self).__init__(parent, 'Renegade Malkador Defender',
                                               275, gear=[Gear('Heavy bolter', count=5),
                                                          Gear('Demolisher siege cannon'),
                                                          Gear('Searchlight')])
        self.Sponsons(self)
        Malkador.Vehicle(self)


class Baneblade(Unit):
    type_name = 'Renegade Baneblade Super-heavy Tank'
    type_id = 'r_baneblade_v1'

    class Sponsons(OneOf):
        def __init__(self, parent):
            super(Baneblade.Sponsons, self).__init__(parent, 'Sponsons')
            self.variant('Two sponsons with lascannon and twin-linked heavy bolters each', 0,
                         gear=[Gear('Lascannon', count=2), Gear('Twin-linked heavy bolter', count=2)])
            self.variant('Extra side armour plating')
            self.variant('Four sponsons with lascannon and twin-linked heavy bolters each', 100,
                         gear=[Gear('Lascannon', count=4), Gear('Twin-linked heavy bolter', count=4)])

    def __init__(self, parent):
        super(Baneblade, self).__init__(parent, 'Renegade Baneblade', 490,
                                        gear=[Gear('Baneblade cannon woth co-axial autocannon'),
                                              Gear('Demolisher cannon'), Gear('Twin-linked heavy bolters'),
                                              Gear('Searchlight'), Gear('Smoke launchers')])
        self.Sponsons(self)
        Malkador.Vehicle(self)


from builder.games.wh40k.imperial_armour.volume13.elites import SpinedBeast as DefaultBeast


class SpinedBeast(DefaultBeast):
    def __init__(self, parent):
        super(SpinedBeast, self).__init__(parent)
        if self.roster.is_vraks:
            self.god.sl.used = self.god.sl.visible = False
            self.god.tz.used = self.god.tz.visible = False


class SpinedBeasts(Unit):
    type_name = 'Spined Chaos Beasts'
    type_id = 'r_spined_v1'

    class SingleBeast(SpinedBeast, ListSubUnit):
        pass

    def __init__(self, parent):
        super(SpinedBeasts, self).__init__(parent)
        self.models = UnitList(self, self.SingleBeast, 1, 3)

    def get_count(self):
        return self.models.count

    def build_statistics(self):
        return {'Models': self.get_count(),
                'Units': self.get_count()}


from builder.games.wh40k.imperial_armour.volume13.heavy import GiantChaosSpawn


class GiantSpawns(Unit):
    type_name = 'Giant Chaos Spawns'
    type_id = 'r_giant_spawn_v1'

    class SingleSpawn(GiantChaosSpawn, ListSubUnit):
        pass

    def __init__(self, parent):
        super(GiantSpawns, self).__init__(parent)
        self.models = UnitList(self, self.SingleSpawn, 1, 3)

    def get_count(self):
        return self.models.count

    def build_statistics(self):
        return {'Models': self.get_count(),
                'Units': self.get_count()}
