__author__ = 'Ivan Truskov'

from builder.core2 import OptionsList, OneOf, Gear,\
    Count, SubUnit, OptionalSubUnit, UnitDescription
from builder.games.wh40k.roster import Unit
from .fast import SingleArvus
from .transport import Chimera
from .hq import RenegadeCommand, Covenant


class Maradeurs(Unit):
    type_name = 'Renegade Maradeur Squad'
    type_id = 'r_maradeur_v1'

    class Chef(Unit):
        class Weapon(OneOf):
            def __init__(self, parent, ccw=False):
                super(Maradeurs.Chef.Weapon, self).__init__(
                    parent, '' if ccw else 'Weapon')
                if ccw:
                    self.variant('Close combat weapon')
                else:
                    self.variant('Autogun')
                    self.variant('Lasgun')
                    self.variant('Shotgun')
                    self.variant('Laspistol')
                self.variant('Bolt pistol', 5)
                self.variant('Boltgun', 5)
                self.variant('Power weapon', 15)
                self.variant('Plasma pistol', 15)
                self.variant('Power fist', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Maradeurs.Chef.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)
                self.variant('Refractor field', 15)
                self.variant('Breacher charge', 15)

        def __init__(self, parent):
            super(Maradeurs.Chef, self).__init__(parent, 'Maradeur Chief', 55 - 4 * 10,
                                                 gear=[Gear('Frag grenades')])
            self.Weapon(self)
            self.Weapon(self, True)
            self.Options(self)

        def build_description(self):
            res = super(Maradeurs.Chef, self).build_description()
            res.add(self.parent.parent.get_common_gear())
            return res

    class Specialty(OneOf):
        def __init__(self, parent):
            super(Maradeurs.Specialty, self).__init__(parent, 'Specialty')
            self.variant('Stalkers')
            self.variant('Murder cultists')
            self.teks = self.variant('Hereteks')

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Maradeurs.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Chimera(parent=self))
            SubUnit(self, SingleArvus(parent=self))

    def __init__(self, parent):
        super(Maradeurs, self).__init__(parent)
        self.spec = self.Specialty(self)
        self.boss = SubUnit(self, self.Chef(parent=None))
        self.models = Count(self, 'Maradeurs', 4, 9, 10, True)
        self.default = [
            Count(self, 'Autogun', 0, 4),
            Count(self, 'Shotgun', 0, 4),
            Count(self, 'Laspistol', 0, 4)
        ]
        self.advanced = [
            Count(self, 'Flamer', 0, 2, 5),
            Count(self, 'Grenade launcher', 0, 2, 5),
            Count(self, 'Sniper rifle', 0, 2, 2),
            Count(self, 'Meltagun', 0, 2, 10),
            Count(self, 'Heavy stubber', 0, 2, 5),
            Count(self, 'Power weapon', 0, 2, 15)
        ]
        self.brutes = Count(self, 'Brutes', 0, 2, 30, True)
        self.transport = self.Transport(self)

    def check_rules(self):
        super(Maradeurs, self).check_rules()
        Count.norm_counts(0, 2, self.advanced)
        advanced = sum(c.cur for c in self.advanced)
        Count.norm_counts(0, self.models.cur - advanced, self.default)
        trans = 1 + self.models.cur + 2 * self.brutes.cur
        self.transport.used = self.transport.visible = trans <= 12

    def get_common_gear(self):
        if self.spec.cur == self.spec.teks:
            return [Gear('Krak grenades'), Gear('Caparace armour')]
        else:
            return [Gear('Flak armour')]

    def get_count(self):
        return 1 + self.models.cur + self.brutes.cur

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        res.add(self.spec.description)
        res.add(self.boss.description)
        model = UnitDescription('Maradeur', 10, options=[
            Gear('Close combat weapon'), Gear('Frag grenades')
        ] + self.get_common_gear())
        laser = self.models.cur
        for wep in self.advanced:
            if wep.cur:
                res.add(model.clone().add(wep.gear).add_points(wep.option_points).set_count(wep.cur))
                laser -= wep.cur
        for wep in self.default:
            if wep.cur:
                res.add(model.clone().add(wep.gear).set_count(wep.cur))
                laser -= wep.cur
        if laser:
            res.add(model.clone().add(Gear('Lasgun')).set_count(laser))
        if self.brutes.cur:
            res.add(UnitDescription('Brute', 30, self.brutes.cur,
                                    options=[Gear('Frag grenades')] + self.get_common_gear()))
        if self.transport.any and self.transport.used:
            res.add(self.transport.description)
        return res

    def build_statistics(self):
        return self.note_transport(super(Maradeurs, self).build_statistics())


class Spawn(Unit):
    type_name = 'Renegade Chaos Spawn'
    type_id = 'r_spawn_v1'

    def __init__(self, parent):
        super(Spawn, self).__init__(parent, points=55, gear=UnitDescription('Chaos Spawn', count=3))

    def get_count(self):
        return 3


class Disciples(Unit):
    type_name = 'Renegade Disciple Squad'
    type_id = 'r_disciples_v1'

    class Champion(Unit):
        class Weapon(OneOf):
            def __init__(self, parent, ccw=False):
                super(Disciples.Champion.Weapon, self).__init__(
                    parent, '' if ccw else 'Weapon')
                if ccw:
                    self.variant('Close combat weapon')
                else:
                    self.variant('Autogun')
                    self.variant('Lasgun')
                    self.variant('Shotgun')
                self.variant('Laspistol')
                self.variant('Autopistol')
                self.variant('Stubgun')
                self.variant('Bolt pistol', 2)
                self.variant('Power weapon', 15)
                self.variant('Plasma pistol', 15)
                self.variant('Power fist', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(Disciples.Champion.Options, self).__init__(parent, 'Options')
                self.variant('Melta bombs', 5)
                self.cap = self.variant('Caparace armour', 5)

        def __init__(self, parent):
            super(Disciples.Champion, self).__init__(parent, 'Disciple Champion',
                                                 gear=[Gear('Frag grenades')])
            self.Weapon(self)
            self.Weapon(self, True)
            self.cov = Covenant(self, cost=10)
            self.opt = self.Options(self)

        def build_description(self):
            res = super(Disciples.Champion, self).build_description()
            if not self.opt.cap:
                res.add(Gear('Flak armour'))
            if self.parent.parent.opt.krak:
                res.add(Gear('Krak grenades'))
            return res

    class Upgrades(OptionsList):
        common_gear = []

        def __init__(self, parent):
            super(Disciples.Upgrades, self).__init__(parent, 'Upgrades', limit=1)
            self.variant('Vox-caster', 5)
            self.variant('Chaos sigil', 10)
            self.weapon = RenegadeCommand.Weapon(parent)

        def check_rules(self):
            super(Disciples.Upgrades, self).check_rules()
            self.weapon.used = self.weapon.visible = self.any

        @property
        def description(self):
            res = []
            model = UnitDescription('Disciple', options=self.common_gear)
            banner = [opt for opt in self.options if opt.value]
            if len(banner):
                res.append(model.add(self.weapon.description)
                        .add(banner[0].gear))
            return res

    class Special(OptionsList):
        common_gear = []

        def __init__(self, parent):
            super(Disciples.Special, self).__init__(parent, 'Special weapon', limit=1)
            self.variant('Flamer', 5)
            self.variant('Grenade launcher', 5)
            self.variant('Meltagun', 10)
            self.variant('Plasmagun', 15)

        @property
        def description(self):
            res = []
            model = UnitDescription('Disciple', options=self.common_gear)
            weapon = [opt for opt in self.options if opt.value]
            if len(weapon):
                res.append(model.add(weapon[0].gear))
            return res

    def __init__(self, parent):
        super(Disciples, self).__init__(parent, points=-5)
        self.boss = SubUnit(self, self.Champion(parent=None))
        self.models = Count(self, 'Disciples', 4, 9, 10, per_model=True)
        self.up = self.Upgrades(self)
        self.spec = self.Special(self)
        self.heavy = RenegadeCommand.HeavyWeapon(self)
        self.wep3 = Count(self, 'Lasguns', 0, 4)
        self.wep4 = Count(self, 'Shotguns', 0, 4)
        self.opt = RenegadeCommand.SquadOptions(self)
        self.transport = RenegadeCommand.Transport(self)

    def check_rules(self):
        super(Disciples, self).check_rules()
        self.opt.fnp.used = self.opt.fnp.visible = self.roster.demagogue.is_heretek()
        has_heavy = 2 * self.heavy.any
        min_free = has_heavy + self.up.any + self.spec.any
        # in case there are all upgrades and HWT, at least 1 Disciple must be also taken
        max_free = self.models.cur - min_free
        Count.norm_counts(0, max_free, [self.wep3, self.wep4])

    def get_count(self):
        return 1 + self.models.cur

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        if self.opt.fnp.value and self.opt.fnp.used:
            res.add(self.opt.fnp.gear)
        res.add(self.boss.description)
        common_gear = [Gear('Close combat weapon'), Gear('Frag grenades')]
        if self.opt.krak.value:
            common_gear.append(Gear('Krak grenades'))
        if self.opt.cap.value:
            common_gear.append(Gear('Caparace armour'))
        else:
            common_gear.append(Gear('Flak armour'))
        model = UnitDescription('Disciple', options=common_gear)
        self.up.common_gear = common_gear
        res.add(self.up.description)
        self.spec.common_gear = common_gear
        res.add(self.spec.description)
        has_heavy = 2 * self.heavy.any
        min_free = has_heavy + self.up.any + self.spec.any
        autoguns = self.models.cur - min_free
        if self.wep3.cur:
            res.add(model.clone().add(Gear('Lasgun')).set_count(self.wep3.cur))
            autoguns -= self.wep3.cur
        if self.wep4.cur:
            res.add(model.clone().add(Gear('Shotgun')).set_count(self.wep4.cur))
            autoguns -= self.wep4.cur
        if autoguns:
            res.add(model.clone().add(Gear('Autogun')).set_count(autoguns))
        if has_heavy:
            self.heavy.common_gear = common_gear
            res.add(self.heavy.description)

        res.add(self.transport.description)
        return res

    def build_statistics(self):
        return self.note_transport(super(Disciples, self).build_statistics())

    def not_nurgle(self):
        return self.boss.unit.cov.not_nurgle()


class OgrynBrutes(Unit):
    type_name = 'Renegade Ogryn Brutes'
    type_id = 'r_ogryn_v1'

    class Dedication(OptionsList):
        def __init__(self, parent):
            super(OgrynBrutes.Dedication, self).__init__(parent, 'Dedication', limit=1)
            self.variant('Dedicated to Khorne', 25)
            self.ng = self.variant('Dedicated to Nurgle', 35)
            self.sl = self.variant('Dedicated to Slaanesh', 25)
            self.tz = self.variant('Dedicated to Tzeentch', 25)

        def check_rules(self):
            if self.roster.is_vraks:
                self.sl.used = self.sl.visible = False
                self.tz.used = self.tz.visible = False

    class Armour(OptionsList):
        def __init__(self, parent):
            super(OgrynBrutes.Armour, self).__init__(parent, 'Armour', limit=1)
            self.variant('Flak armour', 10)
            self.variant('Caparace armour', 25)

    class Curse(OptionsList):
        def __init__(self, parent):
            super(OgrynBrutes.Curse, self).__init__(parent, 'Mutation')
            self.variant('Curse of Mutation', 15)

    class Upgrades(OptionsList):
        common_gear = []

        def __init__(self, parent):
            super(OgrynBrutes.Upgrades, self).__init__(parent, 'Upgrades')
            self.master = self.variant('Packmaster', 0)
            self.weapons = [
                self.variant('Lascutter', 15),
                self.variant('Power drill', 15)
            ]
            self.hounds = Count(parent, 'Chaos Hounds', 0, 6, 30, True,
                                gear=UnitDescription('Chaos Hound', 30))

        def check_rules(self):
            super(OgrynBrutes.Upgrades, self).check_rules()
            OptionsList.process_limit(self.weapons, 1)
            self.hounds.used = self.hounds.visible = self.master.value

        @property
        def description(self):
            res = []
            for opt in self.weapons:
                if opt.value:
                    res.append(UnitDescription('Ogryn Brute', 60 + opt.points,
                                               options=self.common_gear + [opt.gear]))
            if self.master.value:
                res.append(UnitDescription('Packmaster', 60, options=self.common_gear))
                res.extend(self.hounds.description)
            return res

    def __init__(self, parent):
        super(OgrynBrutes, self).__init__(parent)
        self.models = Count(self, 'Ogryn Brutes', 1, 10, 60, True)
        self.up = self.Upgrades(self)
        self.armour = self.Armour(self)
        self.curse = self.Curse(self)
        self.god = self.Dedication(self)

    def check_rules(self):
        super(OgrynBrutes, self).check_rules()
        self.models.min = 2 if sum(opt.value for opt in self.up.options) > 1 else 1
        self.curse.used = self.curse.visible = self.roster.demagogue.is_mutant()

    def get_count(self):
        return self.models.cur

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        res.add(self.god.description)
        if self.curse.used:
            res.add(self.curse.description)
        self.up.common_gear = [Gear('Close combat weapon'), Gear('Frag grenades')]\
                              + self.armour.description
        simple = self.models.cur - sum(opt.value for opt in self.up.options)
        if simple:
            res.add(UnitDescription('Ogryn Brute', 60, options=self.up.common_gear))
        res.add(self.up.description)
        return res

    def build_statistics(self):
        return {'Models': self.get_count() + self.up.hounds.cur,
                'Units': 1}

    def not_nurgle(self):
        return self.god.any and not self.god.ng.value


from builder.games.wh40k.imperial_armour.volume13.heavy import Slaughterers


class RenegadeSlaughterers(Slaughterers):
    type_name = 'Renegade Blood Slaughterers'
    type_id = 'r_slaughterers_v1'


from builder.games.wh40k.imperial_armour.volume13.fast import BlightDrones


class RenegadeBlightDrones(BlightDrones):
    type_name = 'Renegade Blight Drones'
    type_id = 'r_bdrones_v1'
