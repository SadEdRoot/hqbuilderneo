__author__ = 'Ivan Truskov'

from builder.core2 import OneOf, OptionsList, Gear
from builder.games.wh40k.roster import Unit


class Chimera(Unit):
    type_name = 'Renegade Chimera'
    type_id = 'r_chimera_v1'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.variant('Heavy bolter', 0)
            self.variant('Heavy flamer', 0)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Chimera.Weapon2, self).__init__(parent=parent, name='')
            self.variant('Multi-laser', 0)
            self.variant('Heavy flamer', 0)
            self.variant('Heavy bolter', 0)
            self.variant('Autocannon', 5)

    class Vehicle(OptionsList):
        def __init__(self, parent):
            super(Chimera.Vehicle, self).__init__(parent, name='Options')
            self.mt = self.variant('Militia training', 10)
            self.sb = self.variant('Storm bolter', 5)
            self.hs = self.variant('Heavy stubber', 5)
            self.variant('Hunter-killer missile', 10)
            self.dozen = self.variant('Dozer blade', 5)
            self.variant('Mine plough', 10)
            self.variant('Extra armour', 10)
            self.variant('Camo netting', 15)

        def check_rules(self):
            super(Chimera.Vehicle, self).check_rules()
            self.process_limit([self.hs, self.sb], 1)

            if self.roster.demagogue.is_reaver():
                self.mt.value = True
                self.mt.active = False
            else:
                self.mt.active = True

    def __init__(self, parent):
        super(Chimera, self) .__init__(parent=parent, points=65, gear=[
            Gear('Searchlight'), Gear('Smoke launchers')])
        self.Weapon1(self)
        self.Weapon2(self)
        self.opt = self.Vehicle(self)
