__author__ = 'Ivan Truskov'

from builder.core2 import SubUnit, Gear, OptionsList
from builder.games.wh40k.roster import Unit


class SpaceMarineCommandTanks(Unit):
    type_id = 'command_tanks_v1'
    type_name = 'Space Marine HQ Command Tanks (Dataslate)'

    class Options(OptionsList):
        def __init__(self, parent, blade=False):
            super(SpaceMarineCommandTanks.Options, self).__init__(parent, 'Options')
            self.variant('Storm bolter', 5)
            self.variant('Hunter-killer missile', 10)
            self.variant('Extra armour', 10)
            if blade:
                self.variant('Dozer blade', 5)

    class RhinoPrimaris(Unit):
        def __init__(self, parent):
            super(SpaceMarineCommandTanks.RhinoPrimaris, self).__init__(
                parent, name='Rhino Primaris', gear=[
                    Gear('Twin-linked plasma gun'),
                    Gear('Searchlight'),
                    Gear('Smoke launchers')
                ])
            SpaceMarineCommandTanks.Options(self, blade=True)

    class LandRaiderExcelsior(Unit):

        class ExcelsiorOptions(OptionsList):
            def __init__(self, parent):
                super(SpaceMarineCommandTanks.LandRaiderExcelsior.ExcelsiorOptions, self).__init__(parent, '', limit=1)
                self.variant('Combi-plasma', 10)
                self.variant('Rod of command', 10)

        def __init__(self, parent):
            super(SpaceMarineCommandTanks.LandRaiderExcelsior, self).__init__(
                parent, name='Land Raider Excelsior', gear=[
                    Gear('Grav cannon'),
                    Gear('Twin-linked lascannon', count=2),
                    Gear('Searchlight'),
                    Gear('Smoke launchers')
                ])
            SpaceMarineCommandTanks.Options(self)
            self.ExcelsiorOptions(self)

    def __init__(self, parent):
        super(SpaceMarineCommandTanks, self).__init__(parent, points=400)
        SubUnit(self, self.RhinoPrimaris(parent=None))
        SubUnit(self, self.LandRaiderExcelsior(parent=None))

    def get_count(self):
        return 2
