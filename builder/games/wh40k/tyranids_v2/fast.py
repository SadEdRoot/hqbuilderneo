__author__ = 'Denis Romanov'

from builder.core2 import *
from builder.games.wh40k.roster import Unit
from .armory import *


class TyranidShrikeBrood(Unit):
    type_name = 'Tyranid Shrike Brood'
    type_id = 'tyranid_shrike_brood_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(TyranidShrikeBrood.Options, self).__init__(parent, name='Options')
            self.variant('Adrenalin glands', 4, per_model=True)
            self.variant('Toxin sacs', 3, per_model=True)
            self.variant('Flesh hooks', 4, per_model=True)

        @property
        def points(self):
            return super(TyranidShrikeBrood.Options, self).points * self.parent.get_count()

    class Warrior(ListSubUnit):
        class Weapon1(MeleeBioWeapons, ScythingTalons):
            pass

        class Weapon2(BasicBioCannons, MeleeBioWeapons, BasicBioWeapons, Devourer):
            pass

        def __init__(self, parent):
            super(TyranidShrikeBrood.Warrior, self).__init__(parent, name='Tyranid Shrike', points=30)

            self.w1 = self.Weapon1(self, name='Weapon')
            self.w2 = self.Weapon2(self, name='')

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.w2.has_heavy()

    def __init__(self, parent):
        super(TyranidShrikeBrood, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=3, max_limit=9)
        self.Options(self)

    def check_rules(self):
        super(TyranidShrikeBrood, self).check_rules()
        spec = sum(c.has_heavy() for c in self.models.units)
        if spec > 1:
            self.error('Only one Tyranid Shrike may take basic bio-cannons (taken {})'.format(spec))

    def get_count(self):
        return self.models.count


class RavenerBrood(Unit):
    type_name = 'Ravener Brood'
    type_id = 'ravener_brood_v1'

    class Options(OptionsList):
        def __init__(self, parent):
            super(RavenerBrood.Options, self).__init__(parent, name='')
            self.terror = self.variant('The Red Terror', 85, gear=[
                UnitDescription('The Red Terror', points=85, options=[
                    Gear('Scything talons', count=2),
                    Gear('Prehensile pincer')
                ])
            ])

    class Warrior(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(RavenerBrood.Warrior.Weapon1, self).__init__(parent, name='Weapon')
                self.variant('Scything talons', 0)
                self.variant('Rending claws', 5)

        class Weapon2(OptionsList):
            def __init__(self, parent):
                super(RavenerBrood.Warrior.Weapon2, self).__init__(parent, name='')
                self.variant('Spinefists', 3)
                self.variant('Devourer', 5)
                self.variant('Deathspitter', 10)

        def __init__(self, parent):
            super(RavenerBrood.Warrior, self).__init__(parent, name='Ravener', points=30,
                                                       gear=[Gear('Scything talons')])

            self.Weapon1(self)
            self.Weapon2(self)

    def __init__(self, parent):
        super(RavenerBrood, self).__init__(parent=parent)
        self.models = UnitList(parent=self, unit_class=self.Warrior, min_limit=3, max_limit=9)
        self.leader = self.Options(self)

    def get_count(self):
        return self.models.count + self.leader.count

    def get_unique(self):
        if self.leader.count:
            return self.leader.terror.title


class SkySlasherBrood(Unit):
    type_name = 'Sky-Slasher Swarm Brood'
    type_id = 'sky_slasher_brood_v1'

    model_name = 'Sky-Slasher Swarm'
    model_points = 18

    class Options(OptionsList):
        def __init__(self, parent):
            super(SkySlasherBrood.Options, self).__init__(parent, name='Options')
            self.fist = self.variant('Spinefists', points=4, per_model=True, gear=[])
            self.variant('Toxin sacs', points=4, per_model=True)
            self.variant('Adrenal glands', points=5, per_model=True)

        @property
        def points(self):
            return super(SkySlasherBrood.Options, self).points * self.parent.models.cur

    class SwarmCount(Count):
        @property
        def description(self):
            return [UnitDescription(
                SkySlasherBrood.model_name,
                points=SkySlasherBrood.model_points,
                options=[Gear('Spinefist')] if self.parent.opt.fist.value else [],
                count=self.cur
            )]

    def __init__(self, parent):
        super(SkySlasherBrood, self).__init__(parent)
        self.models = self.SwarmCount(self, self.model_name, 3, 9, self.model_points, per_model=True)
        self.opt = self.Options(self)

    def get_count(self):
        return self.models.cur


class Gargoyle(Unit):
    type_name = 'Gargoyle Brood'
    type_id = 'gargoyle_brood_v1'

    model_name = 'Gargoyle'
    model_points = 6

    class Options(OptionsList):
        def __init__(self, parent):
            super(Gargoyle.Options, self).__init__(parent, name='Options')
            self.variant('Adrenal glands', points=2, per_model=True)
            self.variant('Toxin sacs', points=2, per_model=True)

        @property
        def points(self):
            return super(Gargoyle.Options, self).points * self.parent.models.cur

    def __init__(self, parent):
        super(Gargoyle, self).__init__(parent)
        self.models = Count(
            self, self.model_name, 10, 30, self.model_points, per_model=True,
            gear=UnitDescription(self.model_name, points=self.model_points, options=[
                Gear('Fleshborer'),
                Gear('Blinding venom'),
            ])
        )
        self.Options(self)

    def get_count(self):
        return self.models.cur


class Harpy(Unit):
    type_name = 'Harpy'
    type_id = 'harpy_v1'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Harpy.Weapon1, self).__init__(parent, name='Weapon')
            self.variant('Twin-linked stranglethorn cannon', 0)
            self.variant('Twin-linked heavy venom cannon', 5)

    class Weapon2(OptionsList):
        def __init__(self, parent):
            super(Harpy.Weapon2, self).__init__(parent, name='', limit=1)
            self.variant('Stinger salvo', 10)
            self.variant('Cluster spines', 15)

    def __init__(self, parent):
        super(Harpy, self).__init__(parent, points=135, gear=[
            Gear('Scything talons'),
            Gear('Spore mine cysts')
        ])
        self.Weapon1(self)
        self.Weapon2(self)
        Biomorphs(self)


class HiveCrone(Unit):
    type_name = 'Hive Crone'
    type_id = 'crone_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(HiveCrone.Weapon, self).__init__(parent, name='Weapon', limit=1)
            self.variant('Stinger salvo', 10)
            self.variant('Cluster spines', 15)

    def __init__(self, parent):
        super(HiveCrone, self).__init__(parent, points=155, gear=[
            Gear('Drool cannon'),
            Gear('Four tentaclids'),
            Gear('Scything talons')
        ])
        self.Weapon(self)
        Biomorphs(self)


class SporeMine(Unit):
    type_name = 'Spore Mine Cluster'
    type_id = 'spore_mine_cluster_v1'

    def __init__(self, parent):
        super(SporeMine, self).__init__(parent)
        self.mines = Count(self, 'Spore Mine', 3, 6, 5, gear=UnitDescription('Spore Mine'), per_model=True)

    def get_count(self):
        return self.mines.cur
