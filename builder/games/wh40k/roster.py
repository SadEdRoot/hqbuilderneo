from builder.core2 import Roster
from builder.core2.options import OptionsList
# to export all sections from a single module
from .section import *
from .unit import Unit
from .fortifications import Fort

__author__ = 'Denis Romanov'


class PrimaryDetachment(Detachment):
    # one per army, must belong to titular faction
    def __init__(self, parent, roster_types, faction=None):
        super(PrimaryDetachment, self).__init__(parent, 'primary', 'Primary Detachment', roster_types, 1, 1, faction)

    def get_limits_error(self):
        if self.min is None or self.max is None:
            return
        if not self.check_limits():
            return 'Roster must include exactly one primary detachment'


def get_possible_allies(faction, allow_mission):
    result = {}
    for key in ally_matrix:
        if key == faction:
            continue
        if faction in enemy_matrix and key in enemy_matrix[faction]:
            continue
        if key in enemy_matrix and faction in enemy_matrix[key]:
            continue
        if allow_mission:
            result[key] = ally_matrix[key]
        else:
            result[key] = [det for det in ally_matrix[key] if not hasattr(det, 'mission_type')]
    return result


def get_obsolete_types():
    from .obsolete.adepta_sororitas_v2 import detachments as ob_d1
    from .obsolete.adepta_sororitas_v3 import detachments as ob_d2
    from .obsolete.blood_angels_v2 import detachments as ob_d3
    from .obsolete.chaos_daemons_v2 import detachments as ob_d12
    from .obsolete.custodians_v1 import detachments as ob_d4
    from .obsolete.dark_angels_v2 import detachments as ob_d5
    from .obsolete.eldar_v2 import detachments as ob_d6
    from .obsolete.imperial_knight import detachments as ob_d7
    from .obsolete.inquisition import detachments as ob_d8
    from .obsolete.necrons_v2 import detachments as ob_d9
    from .obsolete.silent_sisters_v1 import detachments as ob_d10
    from .obsolete.space_marines_v2 import detachments as ob_d11
    from .obsolete.space_wolves_v3 import detachments as ob_d13
    from .obsolete.tau_v2 import detachments as ob_d14
    return ob_d1 + ob_d2 + ob_d3 + ob_d4 + ob_d5 + ob_d6 + ob_d7 +\
        ob_d8 + ob_d9 + ob_d10 + ob_d11 + ob_d12 + ob_d13 + ob_d14


class SecondaryDetachment(Detachment):
    # unlimited per army
    # may include allied, so option for building list of
    # allowed detachments is reserved
    allied_types = {}

    def __init__(self, parent, roster_types, faction=None, allow_mission=False):
        if faction:
            self.allied_types = get_possible_allies(faction, allow_mission)
        super(SecondaryDetachment, self).__init__(
            parent, 'secondary', 'Secondary Detachment', roster_types, None,
            None, grouped_roster_types=self.allied_types,
            obsolete_types=get_obsolete_types(), faction=faction)


class Wh40kBase(Roster):
    wikilink = ''

    @staticmethod
    def move_units(move, from_units, to_units):
        to_units.active = move
        from_units.active = not move

        if move:
            to_units.parent.move_units(from_units.units)
        else:
            from_units.parent.move_units(to_units.units)

    def check_unit_limit(self, count, units, message):
        units.active = count > 0
        if count < units.count:
            self.error(message)

    # override on the base class (Node)
    # instead of a top-level node, detachment is returned instead
    @property
    def roster(self):
        return self

    def __init__(self, parent=None, limit=None, **kwargs):
        sections = []
        for name, value in list(kwargs.items()):
            if isinstance(value, list):
                sections.extend(value)
            else:
                sections.append(value)
        super(Wh40kBase, self).__init__(sections, parent, limit)


class Wh40kImperial(Roster):
    class ImperialFlag(OptionsList):
        def __init__(self, parent):
            super(Wh40kImperial.ImperialFlag, self).__init__(parent, 'Imperial Armour enabled')
            self.variant('')

    def __init__(self, *args, **kwargs):
        super(Wh40kImperial, self).__init__(*args, **kwargs)
        self.flag = self.ImperialFlag(self)

    @staticmethod
    def recursive_check(roster, flag):
        if hasattr(roster, 'custom_imperial_check') and\
           roster.custom_imperial_check:
            return
        if isinstance(roster, Formation):
            for _type in roster.ia_units:
                _type.active = flag
            for unit in roster.units:
                if hasattr(unit, 'sub_roster'):
                    Wh40kImperial.recursive_check(unit.sub_roster.roster, flag)
            return
        elif isinstance(roster, section.Formation):
            return
        for sec in roster.sections:
            if not hasattr(sec, 'ia_units'):
                continue
            # units in sections are Detachment.DetachUnit
            for _type in sec.ia_units:
                _type.active = flag
            for unit in sec.units:
                if hasattr(unit, 'sub_roster'):
                    Wh40kImperial.recursive_check(unit.sub_roster.roster, flag)

    @property
    def imperial_armour_on(self):
        return self.flag.any

    def check_rules(self):
        super(Wh40kImperial, self).check_rules()
        Wh40kImperial.recursive_check(self, self.flag.any)


class CombinedArmsDetachment(Wh40kBase):
    def __init__(self, hq=None, elites=None, troops=None, fast=None, heavy=None, fort=None, lords=None, **kwargs):
        self.hq = hq
        if hq is not None:
            self.hq.min, self.hq.max = (1, 2)
        self.elites = elites
        if elites is not None:
            self.elites.min, self.elites.max = (0, 3)
        self.troops = troops
        if troops is not None:
            self.troops.min, self.troops.max = (2, 6)
        self.fast = fast
        if fast is not None:
            self.fast.min, self.fast.max = (0, 3)
        self.heavy = heavy
        if heavy is not None:
            self.heavy.min, self.heavy.max = (0, 3)
        self.fortification = fort
        if fort is not None:
            self.fortification.min, self.fortification.max = (0, 1)
        self.lords = lords
        if lords is not None:
            self.lords.min, self.lords.max = (0, 1)

        super(CombinedArmsDetachment, self).__init__(sections=[
            self.hq, self.elites, self.troops, self.fast,
            self.heavy, self.fortification, self.lords], **kwargs)


class AlliedDetachment(Wh40kBase):
    def __init__(self, hq=None, elites=None, troops=None, fast=None, heavy=None, **kwargs):
        self.hq = hq
        if hq is not None:
            self.hq.min, self.hq.max = (1, 1)
        self.elites = elites
        if elites is not None:
            self.elites.min, self.elites.max = (0, 1)
        self.troops = troops
        if troops is not None:
            self.troops.min, self.troops.max = (1, 2)
        self.fast = fast
        if fast is not None:
            self.fast.min, self.fast.max = (0, 1)
        self.heavy = heavy
        if heavy is not None:
            self.heavy.min, self.heavy.max = (0, 1)
        self.lords = None
        # other arguments are consumed and ignored
        super(AlliedDetachment, self).__init__(sections=[
            self.hq, self.elites, self.troops, self.fast, self.heavy])


class PlanetstrikeAttacker(Wh40kBase):

    mission_type = 'Planetstrike (Attacker)'

    def __init__(self, hq=None, elites=None, troops=None, fast=None, heavy=None, **kwargs):
        self.hq = hq
        if hq is not None:
            self.hq.min, self.hq.max = (1, 3)
        self.elites = elites
        if elites is not None:
            self.elites.min, self.elites.max = (0, 6)
        self.troops = troops
        if troops is not None:
            self.troops.min, self.troops.max = (0, 6)
        self.fast = fast
        if fast is not None:
            self.fast.min, self.fast.max = (0, 6)
        self.heavy = heavy
        if heavy is not None:
            self.heavy.min, self.heavy.max = (0, 3)
        self.lords = None
        # other arguments are consumed and ignored
        super(PlanetstrikeAttacker, self).__init__(sections=[
            self.hq, self.elites, self.troops, self.fast, self.heavy])


class PlanetstrikeDefender(Wh40kBase):

    mission_type = 'Planetstrike (Defender)'

    def __init__(self, hq=None, elites=None, troops=None, fast=None, heavy=None, **kwargs):
        self.hq = hq
        if hq is not None:
            self.hq.min, self.hq.max = (1, 3)
        self.elites = elites
        if elites is not None:
            self.elites.min, self.elites.max = (0, 9)
        self.troops = troops
        if troops is not None:
            self.troops.min, self.troops.max = (0, 3)
        self.fast = fast
        if fast is not None:
            self.fast.min, self.fast.max = (0, 3)
        self.heavy = heavy
        if heavy is not None:
            self.heavy.min, self.heavy.max = (0, 6)
        self.lords = None
        # other arguments are consumed and ignored
        super(PlanetstrikeDefender, self).__init__(sections=[
            self.hq, self.elites, self.troops, self.fast, self.heavy])


class SiegeAttacker(Wh40kBase):

    mission_type = 'Siege War (Attacker)'

    def __init__(self, hq=None, elites=None, troops=None, fast=None, heavy=None, fort=None, **kwargs):
        self.hq = hq
        if hq is not None:
            self.hq.min, self.hq.max = (1, 2)
        self.elites = elites
        if elites is not None:
            self.elites.min, self.elites.max = (0, 3)
        self.troops = troops
        if troops is not None:
            self.troops.min, self.troops.max = (2, 6)
        self.fast = fast
        if fast is not None:
            self.fast.min, self.fast.max = (0, 3)
        self.heavy = heavy
        if heavy is not None:
            self.heavy.min, self.heavy.max = (0, 4)
        self.fortification = fort
        if fort is not None:
            self.fortification.min, self.fortification.max = (0, 1)
        self.lords = None
        # other arguments are consumed and ignored
        super(SiegeAttacker, self).__init__(sections=[
            self.hq, self.elites, self.troops, self.fast, self.heavy, self.fortification])


class SiegeDefender(Wh40kBase):

    mission_type = 'Siege War (Defender)'

    def __init__(self, hq=None, elites=None, troops=None, fast=None, heavy=None, fort=None, **kwargs):
        self.hq = hq
        if hq is not None:
            self.hq.min, self.hq.max = (1, 2)
        self.elites = elites
        if elites is not None:
            self.elites.min, self.elites.max = (0, 3)
        self.troops = troops
        if troops is not None:
            self.troops.min, self.troops.max = (2, 6)
        self.fast = fast
        if fast is not None:
            self.fast.min, self.fast.max = (0, 3)
        self.heavy = heavy
        if heavy is not None:
            self.heavy.min, self.heavy.max = (0, 3)
        self.fortification = fort
        if fort is not None:
            self.fortification.min, self.fortification.max = (1, 3)
        self.lords = None
        # other arguments are consumed and ignored
        super(SiegeDefender, self).__init__(sections=[
            self.hq, self.elites, self.troops, self.fast, self.heavy, self.fortification])


class Wh40k7ed(Roster):
    game_name = 'Warhammer 40k (7th edition)'
    roster_type = 'Battle-forged'
    base_tags = ['Warhammer 40k', '7th edition', roster_type]
    faction = None
    imperial_armour = False

    @classmethod
    def post_process(cls):
        cls.base_tags = [cls.army_name] + cls.base_tags
        if cls.imperial_armour:
            cls.base_tags += ['Imperial armour']
            cls.game_name = '{0}: Imperial armour'.format(cls.game_name)

    def __init__(self, primary, secondary=[], parent=None):
        self.primary = PrimaryDetachment(self, roster_types=primary)
        self.secondary = SecondaryDetachment(self, roster_types=primary + secondary, faction=self.faction)
        sections = [self.primary, self.secondary]

        super(Wh40k7ed, self).__init__(sections, parent, limit=1850)
        self.extra_checks = dict()

    def _build_unique_map(self, names_getter):
        unique_map = {}
        for sec in self.sections:
            for unit in sec.units:
                if hasattr(unit, 'get_unique_map'):
                    submap = unit.get_unique_map(names_getter)
                    for (k, v) in submap.items():
                        if k in list(unique_map.keys()):
                            unique_map[k] += v
                        else:
                            unique_map[k] = v
                else:
                    un = names_getter(unit)
                    if un is None:
                        continue
                    if not isinstance(un, (list, type)):
                        un = [un]
                    for name in un:
                        if name in list(unique_map.keys()):
                            unique_map[name] += 1
                        else:
                            unique_map[name] = 1
        return unique_map

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        # import pdb; pdb.set_trace()
        # perform registered checks
        for key in self.extra_checks:
            self.extra_checks[key](self)

    def dump(self):
        res = super(Wh40k7ed, self).dump()
        if self == self.root:
            res.update({
                'statistics': self.build_statistics()
            })
        return res

    @property
    def delta(self):
        res = super(Wh40k7ed, self).delta
        if self == self.root:
            res.update({
                'statistics': self.build_statistics()
            })
        return res


class Wh40k7edMissions(Wh40k7ed):
    game_name = 'Warhammer 40k (7th edition)'
    roster_type = 'Battle-forged (Mission)'
    base_tags = ['Warhammer 40k', '7th edition', roster_type]

    def __init__(self, primary, secondary=[], parent=None):
        self.primary = PrimaryDetachment(self, roster_types=primary)
        self.secondary = SecondaryDetachment(self, roster_types=primary + secondary, faction=self.faction,
                                             allow_mission=True)
        sections = [self.primary, self.secondary]

        super(Wh40k7ed, self).__init__(sections, parent, limit=1850)
        self.extra_checks = dict()

    def check_rules(self):
        super(Wh40k7edMissions, self).check_rules()
        mis_set = set()
        for sec in self.sections:
            for unit in sec.units:
                if hasattr(unit.sub_roster.roster, 'mission_type'):
                    mis_set.add(unit.sub_roster.roster.mission_type)
        if len(mis_set) > 1:
            self.error('Roster cannot have more then 1 mission role (taken up: {} and {})'.format(
                list(mis_set)[0], list(mis_set)[1]))


class Wh40kKillTeam(Roster):
    game_name = 'Warhammer 40k (7th edition)'
    roster_type = 'Kill Team'
    base_tags = ['Warhammer 40k', '7th edition', roster_type]
    imperial_armour_on = False

    @staticmethod
    def process_transported_units(section, types, restricted=lambda s: []):
        res = []
        for _type in types:
            class TransportedUnit(_type):
                def note_transport(self, statistic):
                    if getattr(self, 'transport', None) and self.transport.any:
                        statistic['Models'] += 1
                        statistic['Units'] += 1
                        statistic['Vehicles'] = 1
                    return statistic

                def __init__(self, parent):
                    # must be a final class!
                    super(self.__class__, self).__init__(parent)
                    rlist = restricted(self)
                    self.transport.options.set_visible(rlist, False)

            res.append(UnitType(section, TransportedUnit))
        return res

    @staticmethod
    def process_vehicle_units(section, types):
        for _type in types:
            class VehicleUnit(_type):
                def build_statistics(self):
                    #must be a final class!
                    statistic = super(self.__class__, self).build_statistics()
                    statistic['Vehicles'] = statistic['Models']
                    return statistic
            UnitType(section, VehicleUnit)

    def dump(self):
        res = super(Wh40kKillTeam, self).dump()
        res.update({
            'statistics': self.build_statistics()
        })
        return res

    @property
    def delta(self):
        res = super(Wh40kKillTeam, self).delta
        res.update({
            'statistics': self.build_statistics()
        })
        return res

    def __init__(self, troops, elites, fast, parent=None):
        sections = []
        if troops:
            troops.min, troops.max = (0, 2)
            sections.append(troops)
        if elites:
            elites.min, elites.max = (0, 1)
            sections.append(elites)
        if fast:
            fast.min, fast.max = (0, 1)
            sections.append(fast)
        super(Wh40kKillTeam, self).__init__(sections=sections, parent=parent, limit=200)

    def check_rules(self):
        super(Wh40kKillTeam, self).check_rules()
        stat = self.build_statistics()
        veh = stat['Vehicles'] if 'Vehicles' in stat else 0
        mod = stat['Models'] if 'Models' in stat else 0
        if mod - veh < 4:
            self.error('A Kill Team must include at least four non-vehicle models')


# global ally matrix
# should be filled when appropriate

from .orks_v3 import faction as fac_orks, detachments as det_orks,\
    unit_types as ut_orks
from .chaos_marines_v2 import faction as fac_csm, detachments as det_csm,\
    unit_types as ut_csm
from .inquisition_v2 import faction as fac_inq, detachments as det_inq,\
    unit_types as ut_inq
from .grey_knights_v3 import faction as fac_gk, detachments as det_gk,\
    unit_types as ut_gk
from .necrons_v3 import faction as fac_necr, detachments as det_necr,\
    unit_types as ut_necr
from .astra_militarum import faction as fac_asmil, detachments as det_asmil,\
    unit_types as ut_asmil
from .blood_angels_v3 import faction as fac_ba, detachments as det_ba,\
    unit_types as ut_ba
from .chaos_daemons_v3 import faction as fac_cd, detachments as det_cd,\
    unit_types as ut_cd
from .dark_angels_v3 import faction as fac_da, detachments as det_da,\
    unit_types as ut_da
from builder.games.wh40k.eldar_v3 import faction as fac_e, detachments as det_e,\
    unit_types as ut_e
from .dark_eldar_v2 import faction as fac_de, detachments as det_de,\
    unit_types as ut_de
from .imperial_knight_v2 import faction as fac_ik, detachments as det_ik,\
    unit_types as ut_ik
from .legion_of_damned import faction as fac_lod, detachments as det_lod
from .militarum_tempestus import faction as fac_mt, detachments as det_mt,\
    unit_types as ut_mt
from .officio_assasinorum import faction as fac_oa, detachments as det_oa,\
    unit_types as ut_oa
from .space_marines_v3 import faction as fac_sm, detachments as det_sm,\
    unit_types as ut_sm
from .tau_v3 import faction as fac_t, detachments as det_t,\
    unit_types as ut_t
from .tyranids_v2 import faction as fac_tyr, detachments as det_tyr,\
    unit_types as ut_tyr
from .adepta_sororitas_v4 import faction as fac_as, detachments as det_as,\
    unit_types as ut_as
from .space_wolves_v4 import faction as fac_sw, detachments as det_sw,\
    unit_types as ut_sw
from .harlequins import faction as fac_har, detachments as det_har,\
    unit_types as ut_har
from .skitarii import faction as fac_sk, detachments as det_sk,\
    unit_types as ut_sk
from .khorne_daemonkin import faction as fac_kd, detachments as det_kd,\
    unit_types as ut_kd
from .cult_mechanicus import faction as fac_cm, detachments as det_cm,\
    unit_types as ut_cm
from .dreadmob_v2 import faction as fac_dm, detachments as det_dm
from .corsairs_v2 import faction as fac_cor, detachments as det_cor,\
    allies_exclusion as ex_cor
from .deathwatch import faction as fac_dw, detachments as det_dw,\
    unit_types as ut_dw
from .genestealers import faction as fac_gs, detachments as det_gs,\
    unit_types as ut_gs
from .renegades import faction as fac_rah, detachments as det_rah
from .dataslates.renegade_knight import faction as fac_rk, detachments as det_rk,\
    unit_types as ut_rk
from .dataslates.cypher import faction as fac_fa, detachments as det_fa,\
    unit_types as ut_fa
from .silent_sisters_v2 import faction as fac_ss, detachments as det_ss
from .custodians_v2 import faction as fac_ac, detachments as det_ac
from .astra_telepathica import faction as fac_at, detachments as det_at,\
    unit_types as ut_at
from .imperium_v1 import faction as fac_aoi, detachments as det_aoi
from .ynnari import faction as fac_y, detachments as det_y,\
    unit_types as ut_y
from .aeronautica_imperialis import faction as fac_nav, detachments as det_nav,\
    unit_types as ut_nav
from .krieg import faction as fac_kr, detachments as det_kr

ally_matrix = {
    fac_orks: det_orks,
    fac_gk: det_gk,
    fac_necr: det_necr,
    fac_asmil: det_asmil,
    fac_ba: det_ba,
    fac_cd: det_cd,
    fac_csm: det_csm,
    fac_da: det_da,
    fac_e: det_e,
    fac_de: det_de,
    fac_ik: det_ik,
    fac_inq: det_inq,
    fac_lod: det_lod,
    fac_mt: det_mt,
    fac_oa: det_oa,
    fac_sm: det_sm,
    fac_t: det_t,
    fac_tyr: det_tyr,
    fac_as: det_as,
    fac_sw: det_sw,
    fac_har: det_har,
    fac_sk: det_sk,
    fac_kd: det_kd,
    fac_cm: det_cm,
    fac_dm: det_dm,
    fac_cor: det_cor,
    fac_dw: det_dw,
    fac_gs: det_gs,
    fac_rah: det_rah,
    fac_rk: det_rk,
    fac_ss: det_ss,
    fac_ac: det_ac,
    fac_aoi: det_aoi,
    fac_at: det_at,
    fac_y: det_y,
    fac_fa: det_fa,
    fac_nav: det_nav,
    fac_kr: det_kr
}

enemy_matrix = {
    fac_cor: ex_cor,
    fac_fa: [fac_da]
}

all_types = {
    fac_orks: ut_orks,
    fac_gk: ut_gk,
    fac_necr: ut_necr,
    fac_asmil: ut_asmil,
    fac_ba: ut_ba,
    fac_cd: ut_cd,
    fac_csm: ut_csm,
    fac_da: ut_da,
    fac_e: ut_e,
    fac_de: ut_de,
    fac_ik: ut_ik,
    fac_mt: ut_mt,
    fac_oa: ut_oa,
    fac_sm: ut_sm,
    fac_t: ut_t,
    fac_tyr: ut_tyr,
    fac_sw: ut_sw,
    fac_har: ut_har,
    fac_sk: ut_sk,
    fac_kd: ut_kd,
    fac_dw: ut_dw,
    fac_gs: ut_gs,
    fac_rk: ut_rk,
    fac_inq: ut_inq,
    fac_as: ut_as,
    fac_at: ut_at,
    fac_cm: ut_cm,
    fac_y: ut_y,
    fac_fa: ut_fa,
    fac_nav: ut_nav,
}

# add faction to unit types


def add_faction(tuple_):
    fac, utypes = tuple_
    for ut in utypes:
        ut.faction = fac


list(map(add_faction, iter(all_types.items())))
list(map(add_faction, iter(ally_matrix.items())))
