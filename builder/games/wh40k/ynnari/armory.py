__author__ = 'Ivan Truskov'
from builder.core2.options import Option, OptionsList


class YnnariArtefacts(OptionsList):
    def __init__(self, *args, **kwargs):
        super(YnnariArtefacts, self).__init__(*args, **kwargs)
        self.ynnari_opts = [
            self.variant("Corag hai's locket", 15),
            self.variant('The Lost Shroud', 35),
            self.variant('Mirrorgaze', 30),
            self.variant('Soulsnare', 25)
        ]

    def check_rules(self):
        super(YnnariArtefacts, self).check_rules()
        flag = getattr(self.parent.roster, 'is_ynnari', False)
        for opt in self.ynnari_opts:
            opt.used = opt.visible = flag


class YnnariBlade(Option):
    def __init__(self, *args, **kwargs):
        super(YnnariBlade, self).__init__(*args, **kwargs)
        self.hblade = self.variant('Hungering blade', 15)

    def check_rules(self):
        super(YnnariBlade, self).check_rules()
        flag = getattr(self.parent.roster, 'is_ynnari', False)
        self.hblade.used = self.hblade.visible = flag


class YnnariPistol(Option):
    def __init__(self, *args, **kwargs):
        super(YnnariPistol, self).__init__(*args, **kwargs)
        self.ysong = self.variant('Song of Ynnead', 10)

    def check_rules(self):
        super(YnnariPistol, self).check_rules()
        flag = getattr(self.parent.roster, 'is_ynnari', False)
        self.ysong.used = self.ysong.visible = flag
