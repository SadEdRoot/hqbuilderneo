
__author__ = 'Denis Romanov'

from builder.games.wh40k.obsolete.chaos_daemons2007 import ChaosDaemons as ChaosDaemons2007
from builder.games.wh40k.obsolete.tau2005 import Tau2005
from builder.games.wh40k.obsolete.eldar2006 import Eldar2006
from builder.games.wh40k.obsolete.space_marines2008 import SpaceMarines2008
from builder.games.wh40k.obsolete.black_templars2005 import BlackTemplars2005
from builder.games.wh40k.obsolete.battle_sisters2011 import BattleSisters2011

from builder.games.wh40k.obsolete.orks import Orks
from builder.games.wh40k.obsolete.orks_v2 import OrksV2
from builder.games.wh40k.obsolete.tyranids import Tyranids
from builder.games.wh40k.obsolete.necrons import Necrons
from builder.games.wh40k.obsolete.space_wolves import SpaceWolves
from builder.games.wh40k.obsolete.space_wolves_v2 import SpaceWolvesV2
from builder.games.wh40k.obsolete.blood_angels import BloodAngels
from builder.games.wh40k.obsolete.blood_angels_v2 import BloodAngelsV2
from builder.games.wh40k.obsolete.dark_angels import DarkAngels
from builder.games.wh40k.obsolete.eldar import Eldar, Iyanden
from builder.games.wh40k.obsolete.eldar_v2 import EldarV2
from builder.games.wh40k.obsolete.chaos_marines import ChaosMarines, BlackLegion
from builder.games.wh40k.obsolete.chaos_daemons import ChaosDaemons
from builder.games.wh40k.obsolete.chaos_daemons_v2 import ChaosDaemonsV2
from builder.games.wh40k.obsolete.imperial_guard import ImperialGuard
from builder.games.wh40k.obsolete.imperial_guard_v2 import ImperialGuardV2
from builder.games.wh40k.obsolete.tau import Tau, FarsightEnclaves
from builder.games.wh40k.obsolete.grey_knights import GreyKnights
from builder.games.wh40k.obsolete.grey_knights_v2 import GreyKnightsV2
from builder.games.wh40k.obsolete.space_marines import SpaceMarines
from builder.games.wh40k.obsolete.adepta_sororitas import AdeptaSororitas
from builder.games.wh40k.obsolete.dark_eldar_v1 import DarkEldarV1
from builder.games.wh40k.obsolete.space_marines_v2 import SpaceMarinesV2
from builder.games.wh40k.obsolete.dark_angels_v2 import DarkAngelsV2
from builder.games.wh40k.obsolete.tau_v2 import TauV2
from builder.games.wh40k.obsolete.necrons_v2 import NecronV2
from builder.games.wh40k.obsolete.adepta_sororitas_v2 import AdeptaSororitasV2
from builder.games.wh40k.obsolete.imperial_knight import ImperialKnights
from builder.games.wh40k.obsolete.adepta_sororitas_v3 import AdeptaSororitasV3
from builder.games.wh40k.obsolete.silent_sisters_v1 import SistersOfSilence
from builder.games.wh40k.obsolete.custodians_v1 import CustodianGuard
from builder.games.wh40k.obsolete.legion_of_damned_old import DamnedLegion
from .dark_eldar_v2 import DarkEldarV2, DarkEldarV2KillTeam, DarkEldarV2Missions
from .tau_v3 import TauV3, TauV3KillTeam, TauV3Missions
from .inquisition import Inquisition
from .inquisition_v2 import InquisitionV2, InquisitionV2KillTeam
from .tyranids_v2 import TyranidsV2, TyranidsV2KillTeam, TyranidsV2Missions
from .chaos_marines_v2 import CsmV2, CsmV2_5, CsmV2_5KillTeam, CsmV2_5Missions
from .eldar_v3 import EldarV3, EldarV3KillTeam, EldarV3Missions
from .necrons_v3 import NecronV3, NecronV3KillTeam, NecronV3Missions
from .adepta_sororitas_v4 import AdeptaSororitasV4, AdeptaSororitasV4KillTeam, AdeptaSororitasV4Missions
from .imperial_knight_v2 import ImperialKnightsV2
from .aeronautica_imperialis import NavyV1, NavyV1Missions
from .legion_of_damned import Damned, DamnedKillTeam
from .militarum_tempestus import MilitarumTempestus, MilitarumTempestusKillTeam, MilitarumTempestusMissions
from .astra_militarum import AstraMilitarum, AstraMilitarumKillTeam, AstraMilitarumMissions
from .orks_v3 import OrksV3, OrksV3KillTeam, OrksV3Missions
from .grey_knights_v3 import GreyKnightsV3, GreyKnightsV3KillTeam, GreyKnightsV3Missions
from .space_wolves_v4 import SpaceWolvesV4, SpaceWolvesV4KillTeam, SpaceWolvesV4Missions
from .harlequins import Harlequins, HarlequinKillTeam
from .blood_angels_v3 import BloodAngelsV3, BloodAngelsV3KillTeam, BloodAngelsV3Missions
from .skitarii import Skitarii, SkitariiKillTeam
from .khorne_daemonkin import KhorneDaemonkin, KhorneDaemonkinKillTeam, KhorneDaemonkinMissions
from .cult_mechanicus import CultMechanicus, CultMechanicusKillTeam, CultMechanicusMissions
from .space_marines_v3 import SpaceMarinesV3, SpaceMarinesV3KillTeam
from .dark_angels_v3 import DarkAngelsV3, DarkAngelsV3KillTeam, DarkAngelsV3Missions
from .officio_assasinorum import Assasin
from .chaos_daemons_v3 import ChaosDaemonsV3, ChaosDaemonsV3KillTeam, ChaosDaemonsV3Missions
from .deathwatch import Deathwatch, DeathwatchKillTeam, DeathwatchMissions
from .genestealers import GenestealerCult, GenestealerCultKillTeam, GenestealerCultMissions
# Imperial armour
# from corsairs import Corsairs
from builder.games.wh40k.obsolete.dreadmob import Dreadmob
from .corsairs_v2 import CorsairV2, CorsairV2Missions
from builder.games.wh40k.dreadmob_v2 import DreadmobV2, DreadmobV2Missions
from .renegades import Renegades
from .astra_telepathica import TelepathicaV1, TelepathicaV1Missions
from builder.games.wh40k.imperium_v1 import ImperiumV1
from builder.games.wh40k.ynnari import Ynnari
from .dataslates.cypher import FallenAngels
from .krieg import DeathKorps, DeathKorpsMissions
from .silent_sisters_v2 import SistersOfSilenceV2
from .custodians_v2 import CustodianGuardV2, CustodianGuardV2Missions

armies = [
    AstraMilitarum, AstraMilitarumMissions,
    MilitarumTempestus, MilitarumTempestusMissions,
    Damned,
    ImperialKnights,
    SpaceWolvesV2,
    SpaceWolvesV4, SpaceWolvesV4Missions,
    ImperialGuardV2,
    Eldar,
    DarkEldarV2, DarkEldarV2Missions,
    DarkEldarV1,
    DarkAngels,
    BloodAngels,
    Orks,
    Tyranids,
    Necrons,
    SpaceWolves,
    ChaosMarines,
    ChaosDaemons,
    ImperialGuard,
    GreyKnights,
    Tau,
    Iyanden,
    SpaceMarines,
    FarsightEnclaves,
    BlackLegion,
    AdeptaSororitas,
    # Corsairs,
    Dreadmob,
    ChaosDaemons2007,
    Tau2005,
    Eldar2006,
    EldarV2,
    SpaceMarines2008,
    BlackTemplars2005,
    BattleSisters2011,
    DarkAngelsV2,
    SpaceMarinesV2,
    TauV2,
    Inquisition,
    InquisitionV2,
    BloodAngelsV2,
    TyranidsV2, TyranidsV2Missions,
    CsmV2,
    ChaosDaemonsV2,
    NecronV2,
    AdeptaSororitasV2,
    DamnedLegion,
    OrksV2,
    OrksV3, OrksV3Missions,
    GreyKnightsV3, GreyKnightsV3Missions,
    GreyKnightsV2,
    Harlequins,
    Skitarii,
    BloodAngelsV3, BloodAngelsV3Missions,
    NecronV3, NecronV3Missions,
    ImperialKnightsV2,
    KhorneDaemonkin, KhorneDaemonkinMissions,
    CultMechanicus, CultMechanicusMissions,
    SpaceMarinesV3,
    DarkAngelsV3, DarkAngelsV3Missions,
    EldarV3, EldarV3Missions,
    TauV3, TauV3Missions,
    DreadmobV2, DreadmobV2Missions,
    Assasin,
    CorsairV2, CorsairV2Missions,
    CsmV2_5, CsmV2_5Missions,
    ChaosDaemonsV3, ChaosDaemonsV3Missions,
    Deathwatch, DeathwatchMissions,
    GenestealerCult, GenestealerCultMissions,
    AdeptaSororitasV3,
    AdeptaSororitasV4, AdeptaSororitasV4Missions,
    Renegades,
    SistersOfSilence,
    CustodianGuard,
    NavyV1, NavyV1Missions,
    AstraMilitarumKillTeam,
    BloodAngelsV3KillTeam,
    ChaosDaemonsV3KillTeam,
    CsmV2_5KillTeam,
    CultMechanicusKillTeam,
    DarkAngelsV3KillTeam,
    DarkEldarV2KillTeam,
    DeathwatchKillTeam,
    EldarV3KillTeam,
    GenestealerCultKillTeam,
    GreyKnightsV3KillTeam,
    HarlequinKillTeam,
    KhorneDaemonkinKillTeam,
    DamnedKillTeam,
    MilitarumTempestusKillTeam,
    NecronV3KillTeam,
    OrksV3KillTeam,
    SkitariiKillTeam,
    SpaceMarinesV3KillTeam,
    SpaceWolvesV4KillTeam,
    TauV3KillTeam,
    TyranidsV2KillTeam,
    AdeptaSororitasV4KillTeam,
    TelepathicaV1, TelepathicaV1Missions,
    InquisitionV2KillTeam,
    ImperiumV1,
    Ynnari,
    FallenAngels,
    DeathKorps, DeathKorpsMissions,
    CustodianGuardV2, CustodianGuardV2Missions,
    SistersOfSilenceV2,
]
