from .hq import *
from .elites import *
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    PrimaryDetachment, PlanetstrikeAttacker, PlanetstrikeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, UnitType


class HQ(HQSection):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        UnitType(self, PrimarisPsyker)
        UnitType(self, Astropath)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)


class Elites(ElitesSection):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, WyrdvanePsykers)


class TelepathicaV1Base(Wh40kBase):
    def __init__(self):
        super(TelepathicaV1Base, self).__init__(
            elites=Elites(parent=self), troops=None, heavy=None,
            fort=None,
            lords=None,
            hq=HQ(parent=self),
            fast=None
        )


class TelepathicaV1PD(TelepathicaV1Base, CombinedArmsDetachment):
    army_id = 'astra_telepathica_v1_pd'
    army_name = 'Psykana Division'

    def __init__(self):
        super(TelepathicaV1PD, self).__init__()
        self.hq.min, self.hq.max = (1, 1)
        self.elites.min, self.elites.max = (0, 3)


class TelepathicaV1PA(TelepathicaV1Base, PlanetstrikeAttacker):
    army_id = 'astra_telepathica_v1_pa'
    army_name = 'Astra Telepathica (Planetstrike attacker detachment)'

class TelepathicaV1PlD(TelepathicaV1Base, PlanetstrikeDefender):
    army_id = 'astra_telepathica_v1_pld'
    army_name = 'Astra Telepathica (Planetstrike defender detachment)'


faction = 'Astra Telepathica'


class TelepathicaV1(Wh40k7ed):
    army_id = 'astra_telepathica_v1'
    army_name = 'Astra Telepathica'
    faction = faction

    def __init__(self):
        super(TelepathicaV1, self).__init__([TelepathicaV1PD])


class TelepathicaV1Missions(Wh40k7edMissions):
    army_id = 'astra_telepathica_v1_mis'
    army_name = 'Astra Telepathica'
    faction = faction

    def __init__(self):
        super(TelepathicaV1Missions, self).__init__([TelepathicaV1PD, TelepathicaV1PA,
                                                     TelepathicaV1PlD])


detachments = [TelepathicaV1PD, TelepathicaV1PA, TelepathicaV1PlD]


unit_types = [PrimarisPsyker, Astropath, WyrdvanePsykers]
