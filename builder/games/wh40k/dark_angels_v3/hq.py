__author__ = 'Denis Romanow'

from builder.core2 import *
from .armory import ArcanaWeapon, HolyWeapon, SpecialIssue, Armour, HolyRelics
from builder.games.wh40k.roster import Unit


class Belial(Unit):
    type_id = 'belial_v3'
    type_name = 'Belial, Grand master of the Deathwing'
    wikilink = "https://sites.google.com/site/wh40000rules/codices/dark-angels/profiles#TOC-Belial"

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Belial.Weapon, self).__init__(parent, 'Weapon')
            self.sos = self.variant(
                name='Storm bolter and Sword of Silence',
                points=None,
                gear=[Gear('Storm bolter'), Gear('Sword of Silence')]
            )
            self.hammer = self.variant(
                name='Thunder hammer and storm shield',
                points=None,
                gear=[Gear('Thunder hammer'), Gear('Storm shield')]
            )
            self.claws = self.variant(
                name='Pair of lightning claws',
                points=None,
                gear=Gear('Lightning claw', count=2)
            )

    def __init__(self, parent):
        super(Belial, self).__init__(parent=parent, name='Belial', points=190, unique=True, gear=[
            Gear('Terminator armour'),
            Gear('Iron halo'),
            Gear('Teleport homer'),
        ])
        self.weapon = Belial.Weapon(self)


class Sammael(Unit):
    type_id = 'sammael_v3'
    type_name = 'Sammael, Grand Master of the Ravenwing'

    def __init__(self, parent):
        super(Sammael, self).__init__(parent=parent, name='Sammael',
                                      points=200, unique=True, gear=[
                                          Gear('Bolt pistol'),
                                          Gear('Frag grenades'),
                                          Gear('Krak grenades'),
                                          Gear('Teleport homer'),
                                          Gear('Raven Sword'),
                                          Gear('Iron Halo'),
                                          Gear('Plasma cannon'),
                                          Gear('win-linked storm bolter')
                                      ], static=True)

    def get_unique_gear(self):
        return [Gear('Raven Sword')]


class Sableclaw(Unit):
    type_id = 'sableclaw_v3'
    type_name = 'Sableclaw'

    def __init__(self, parent):
        super(Sableclaw, self).__init__(parent,
                                        name='Sableclaw',
                                        points=200,
                                        gear=[
                                            Gear('Twin-linked assault cannon'),
                                            Gear('Twin-linked heavy bolter'),
                                            Gear('Iron Halo'),
                                            Gear('Raven Sword')
                                        ], unique=True, static=True)

    def get_unique_gear(self):
        return [Gear('Raven Sword')]


class InterrogatorChaplain(Unit):
    type_id = 'interrogator_chaplain_v3'
    type_name = 'Interrogator-Chaplain'
    wikilink = "https://sites.google.com/site/wh40000rules/codices/dark-angels/profiles#TOC-Interrogator-chaplain"

    class ExtraWeapon(OptionsList):
        def __init__(self, parent):
            super(InterrogatorChaplain.ExtraWeapon, self).__init__(parent, '')
            self.variant('Power fist', 25)

    def __init__(self, parent):
        super(InterrogatorChaplain, self).__init__(parent=parent, points=110, gear=Gear('Rosarius'))

        self.armour = Armour(self, tda=30)
        self.weapon1 = HolyWeapon(self, 'Weapon', armour=self.armour, crozius=True, relic=True)
        self.weapon2 = ArcanaWeapon(self, '', armour=self.armour, pistol=True,
                              melee=True, ranged=True, relic=True,
                              tda_ranged=True)
        self.weapon3 = self.ExtraWeapon(self)
        self.opt = SpecialIssue(self, armour=self.armour, jump=True, bike=True)
        self.relic = HolyRelics(self)

    def check_rules(self):
        self.weapon3.used = self.weapon3.visible = not self.armour.is_tda()
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()

    def has_bike(self):
        return self.opt.has_bike()


class Asmodai(Unit):
    type_id = 'asmodai_v3'
    type_name = 'Asmodai, Master Interrogator-Chaplain'
    wikilink = "https://sites.google.com/site/wh40000rules/codices/dark-angels/profiles#TOC-Asmodai"

    def __init__(self, parent):
        super(Asmodai, self).__init__(parent=parent, name='Asmodai', points=140, unique=True, static=True, gear=[
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            #Gear('Power armour'),
            Gear('Crozius arcanum'),
            Gear('Rosarius'),
            Gear('Blades of Reason'),
        ])


class Librarian(Unit):
    type_id = 'librarian_v3'
    type_name = 'Librarian'

    class Level(OneOf):
        def __init__(self, parent):
            super(Librarian.Level, self).__init__(parent, 'Psyker')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            else:
                return 2

    class TerminatorRanged(OptionsList):
        def __init__(self, parent):
            super(Librarian.TerminatorRanged, self).__init__(parent, '', limit=1)
            self.variant('Storm bolter', 5)
            self.variant('Combi-flamer', 10)
            self.variant('Combi-melta', 10)
            self.variant('Combi-plasma', 10)

    def __init__(self, parent):
        super(Librarian, self).__init__(parent=parent, points=65, gear=Gear('Psychic hood'))

        self.armour = Armour(self, tda=25)
        self.weapon1 = HolyWeapon(self, 'Weapon', armour=self.armour, force=True, relic=True)
        self.weapon2 = ArcanaWeapon(self, '', pistol=True, melee=True, ranged=True, relic=True)
        self.weapon3 = self.TerminatorRanged(self)
        self.opt = SpecialIssue(self, armour=self.armour, jump=True, bike=True)
        self.relic = HolyRelics(self)
        self.psy = self.Level(self)

    def check_rules(self):
        self.weapon3.used = self.weapon3.visible = self.armour.is_tda()
        self.weapon2.used = self.weapon2.visible = not self.armour.is_tda()
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()

    def has_bike(self):
        return self.opt.has_bike()

    def count_charges(self):
        return self.psy.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Librarian, self).build_statistics())


class Ezekiel(Unit):
    type_id = 'ezekiel_v3'
    type_name = 'Ezekiel, Grand Master of Librarians'
    wikilink = "https://sites.google.com/site/wh40000rules/codices/dark-angels/profiles#TOC-Ezekiel"

    def __init__(self, parent):
        super(Ezekiel, self).__init__(parent=parent, points=145, name='Ezekiel', unique=True, static=True, gear=[
            Gear('Master crafted bolt pistol'),
            Gear('Master-crafted force sword'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            # Gear('Artificer armour'),
            Gear('Psychic hood'),
            # Gear('Traitor\'s Bane'),
            Gear('Book of Salvation'),
        ])

    def count_charges(self):
        return 3

    def build_statistics(self):
        return self.note_charges(super(Ezekiel, self).build_statistics())


class CompanyMaster(Unit):
    type_id = 'company_master_v3'
    type_name = 'Company Master'

    def __init__(self, parent):
        super(CompanyMaster, self).__init__(parent=parent, points=90, gear=Gear('Iron halo'))

        self.armour = Armour(self, art=20, tda=30)
        self.weapon1 = HolyWeapon(self, 'Weapon', armour=self.armour, melee=True, ranged=True, relic=True, tda_melee=True, relic_blade=True)
        self.weapon2 = ArcanaWeapon(self, '', armour=self.armour, pistol=True, melee=True, ranged=True, relic=True,
                              tda_ranged=True)
        self.opt = SpecialIssue(self, armour=self.armour, jump=True, storm_shield=True)
        self.relic = HolyRelics(self)

    def check_rules(self):
        for r1, r2 in zip(self.weapon1.relic_weapon, self.weapon2.relic_weapon):
            r2.active = not self.weapon1.cur == r1
            r1.active = not self.weapon2.cur == r2

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.weapon1.get_unique() + self.weapon2.get_unique() + self.relic.get_unique()


class Chaplain(Unit):
    type_id = 'chaplain_v3'
    type_name = 'Chaplain'

    def __init__(self, parent):
        super(Chaplain, self).__init__(parent=parent, points=90, gear=[Gear('Crozius Arcanum'), Gear('Rosarius'), Gear('Power armour'),
                                                                       Gear('Frag grenades'), Gear('Krak grenades')])

        # self.weapon1 = Weaponself, 'Weapon', crozius=True)
        self.weapon2 = ArcanaWeapon(self, '', pistol=True, melee=True, ranged=True)
        self.opt = SpecialIssue(self, jump=True, bike=True)

    def has_bike(self):
        return self.opt.has_bike()


class Servitors(Unit):
    type_name = "Servitors"
    type_id = 'servitors_v3'
    model_points = 10

    def __init__(self, parent):
        super(Servitors, self).__init__(parent=parent)
        self.models = Count(self, self.name, points=self.model_points, min_limit=1, max_limit=5, per_model=True)
        self.hw = [
            Count(self, 'Heavy bolter', 0, 2, 10),
            Count(self, 'Multi-melta', 0, 2, 20),
            Count(self, 'Plasma cannon', 0, 2, 20)
        ]

    def check_rules(self):
        Count.norm_counts(0, min(2, self.get_count()), self.hw)

    def build_description(self):
        desc = UnitDescription(self.name, points=self.points, count=self.get_count())
        serv = UnitDescription('Servitor', points=10)
        arms_count = self.get_count()
        for wep in self.hw:
            hw_serv = serv.clone()
            hw_serv.add(Gear(wep.name))
            hw_serv.points += wep.option_points
            hw_serv.count = wep.cur
            desc.add(hw_serv)
            arms_count -= wep.cur
        serv.add(Gear('Servo-Arm'))
        if arms_count > 0:
            serv.count = arms_count
            desc.add(serv)
        return desc

    def get_count(self):
        return self.models.cur


class Techmarine(Unit):
    type_id = 'techmarine_v1'
    type_name = 'Techmarine'

    class Gear(OneOf):
        def __init__(self, parent):
            super(Techmarine.Gear, self).__init__(parent, 'Gear')
            self.arm = self.variant('Servo-arm', 0)
            self.harness = self.variant('Servo-harness', 25)

    class Servitors(OptionalSubUnit):
        def __init__(self, parent):
            super(Techmarine.Servitors, self).__init__(parent, 'Servitors')
            SubUnit(self, Servitors(parent=None))

    def __init__(self, parent):
        super(Techmarine, self).__init__(
            parent=parent,
            points=65,
            gear=[Gear('Power Axe'), Gear('Frag grenades'), Gear('Krak grenades')]
        )
        self.weapon = ArcanaWeapon(self, 'Weapon', pistol=True, melee=True, ranged=True)
        self.tech_gear = self.Gear(self)
        self.opt = SpecialIssue(self, bike=True)
        self.serv = self.Servitors(self)

    def has_bike(self):
        return self.opt.has_bike()
