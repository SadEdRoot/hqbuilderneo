__author__ = 'Ivan Truskov'

from builder.core2 import Gear
from builder.games.wh40k.roster import Unit


class Azrael(Unit):
    type_id = 'azrael_v3'
    type_name = 'Azrael'

    def __init__(self, parent):
        super(Azrael, self).__init__(parent=parent, points=215, unique=True, static=True, gear=[
            Gear('Bolt pistol'),
            Gear('Frag grenades'),
            Gear('Krak grenades'),
            Gear('Protector'),
            Gear('Lion\'s Wrath'),
            Gear('Sword of Secrets'),
            Gear('Lion Helm'),
        ])
