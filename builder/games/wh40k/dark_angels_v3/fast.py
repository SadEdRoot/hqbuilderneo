__author__ = 'Denis Romanov'
__mainainer__ = 'Ivan Truskov'

from builder.core2 import UnitDescription, SubUnit, UnitList, OneOf, OptionsList,\
    ListSubUnit, Gear, Count, OptionalSubUnit
from .armory import Weapon, Special
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle,\
    SpaceMarinesBaseSquadron
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from .transport import Transport
from builder.games.wh40k.unit import Unit


class AssaultSquad(IATransportedUnit):
    type_name = 'Assault squad'
    type_id = 'assault_squad_v3'

    model_points = 14
    model_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    class Sergeant(Unit):

        class WeaponMelee(Weapon):
            def __init__(self, parent):
                super(AssaultSquad.Sergeant.WeaponMelee, self).__init__(parent, 'Weapon', melee=True, ranged=True)
                self.evs = self.variant('Eviscerator', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(AssaultSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.combatshiled = self.variant('Combat shield', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(AssaultSquad.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=70 - 4 * AssaultSquad.model_points,
                gear=AssaultSquad.model_gear
            )
            self.wep2 = self.WeaponMelee(self)
            self.wep1 = Weapon(self, '', pistol=True, melee=True, ranged=True)
            self.opt = self.Options(self)

        def check_rules(self):
            super(AssaultSquad.Sergeant, self).check_rules()
            self.wep1.visible = self.wep1.used = not self.has_evs()

        def has_evs(self):
            return self.wep2.cur == self.wep2.evs

        def build_description(self):
            desc = super(AssaultSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            if self.parent.parent.pack.used and self.parent.parent.pack.any:
                desc.add(Gear('Jump pack'))
                desc.add_points(3)
            return desc

    class Jumppack(OptionsList):
        def __init__(self, parent):
            super(AssaultSquad.Jumppack, self).__init__(parent=parent, name='')
            self.jumppack = self.variant('Jump Packs', 3, gear=[], per_model=True)
            self.jumppack.value = True

    def build_points(self):
        return super(AssaultSquad, self).build_points() + self.pack.points * self.marines.cur * self.pack.used

    def __init__(self, parent):
        super(AssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.flame = Count(self, 'Flamer', 0, 2, 5)
        self.pp = Count(self, 'Plasma pistol', 0, 2, 15)
        self.evs = Count(self, 'Eviscerator', 0, 2, 25)
        self.pack = self.Jumppack(parent=self)
        self.transport = Transport(parent=self)

    def check_rules(self):
        super(AssaultSquad, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.pp])
        evs_max = (self.get_count() / 5) - self.sergeant.unit.has_evs()
        self.evs.used = self.evs.visible = evs_max > 0
        self.evs.max = evs_max
        self.pack.used = self.pack.visible = not self.transport.any

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=AssaultSquad.model_gear,
            points=AssaultSquad.model_points
        )
        if self.pack.any and self.pack.used:
            marine.add(Gear('Jump Pack'))
            marine.add_points(self.pack.points)
        if self.evs.cur:
            desc.add(marine.clone().
                     add(Gear('Eviscerator')).
                     add_points(25).set_count(self.evs.cur))
        marine.add(Gear('Chainsword'))
        count = self.marines.cur - self.evs.cur
        for o in [self.pp, self.flame]:
            if o.cur:
                desc.add_dup(marine.clone().add_points(o.option_points).add(Gear(o.name)).set_count(o.cur))
                count -= o.cur

        desc.add(marine.add(Gear('Bolt pistol')).set_count(count))
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1


class RavenwingBikeSquad(Unit):
    type_name = 'Ravenwing Bike Squad'
    type_id = 'ravenwing_bikers_v3'

    model_gear = [Gear('Power Armour'), Gear('Frag grenades'), Gear('Krak grenades'), Gear('Teleport Homer'),
                  Gear('Space Marine bike')]
    model_points = 25

    class Sergeant(Unit):

        class Options(OptionsList):
            def __init__(self, parent):
                super(RavenwingBikeSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(RavenwingBikeSquad.Sergeant, self).__init__(
                name='Ravenwing Sergeant',
                parent=parent, points=75 - RavenwingBikeSquad.model_points * 2,
                gear=RavenwingBikeSquad.model_gear)
            self.weapon = Weapon(self, 'Weapon', pistol=True, melee=True, ranged=True)
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(RavenwingBikeSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Ravenwing Veteran Sergeant'
            return desc

    class AttackBike(Unit):
        model_name = 'Ravenwing Attack Bike'
        model_points = 45

        class Weapon(OneOf):
            def __init__(self, parent):
                super(RavenwingBikeSquad.AttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy bolter', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(RavenwingBikeSquad.AttackBike.Weapon2, self).__init__(parent=parent, name='')
                self.heavybolter = self.variant('Bolt pistol', 0)
                self.multimelta = self.variant('Chainsword', 0)

        def __init__(self, parent):
            super(RavenwingBikeSquad.AttackBike, self).__init__(
                parent=parent, name=self.model_name, points=self.model_points,
                gear=RavenwingBikeSquad.model_gear)
            self.wep = self.Weapon(self)
            self.wep2 = self.Weapon2(self)

    class OptionBike(OptionalSubUnit):
        def __init__(self, parent):
            super(RavenwingBikeSquad.OptionBike, self).__init__(parent, 'Attack bike')
            SubUnit(self, RavenwingBikeSquad.AttackBike(parent=None))

    def __init__(self, parent):
        super(RavenwingBikeSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Ravenwing Biker', min_limit=2, max_limit=5,
                             points=self.model_points, per_model=True)
        self.spec1 = Special(self, 'Special weapon', pistol=True, ccw=True)
        self.spec2 = Special(self, '', pistol=True, ccw=True)
        self.mle = Count(self, 'Melee weapons', 0, 0, 3)
        self.abike = self.OptionBike(self)

    def get_count(self):
        return self.marines.cur + 1 + self.abike.count

    def check_rules(self):
        super(RavenwingBikeSquad, self).check_rules()
        self.mle.max = self.marines.cur - 2

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Ravenwing Biker',
            options=RavenwingBikeSquad.model_gear,
            points=RavenwingBlackKnights.model_points
        )
        csw_marine = marine.clone()
        csw_marine.set_count(self.mle.cur + sum(opt.cur == opt.csw for opt in [self.spec1, self.spec2]))
        csw_marine.add(Gear('Chainsword'))
        if csw_marine.count > 0:
            desc.add(csw_marine)

        bolt_marine = marine.clone()
        bolt_marine.add(Gear('Bolt pistol'))
        bolt_marine.set_count(self.marines.cur - 2 - self.mle.cur + sum(opt.cur == opt.pistol for opt in [self.spec1, self.spec2]))
        if bolt_marine.count > 0:
            desc.add(bolt_marine)
        for opt in [self.spec1, self.spec2]:
            if opt.cur not in [opt.pistol, opt.csw]:
                desc.add_dup(marine.clone().add(opt.description).add_points(opt.points))
        desc.add(self.abike.description)
        return desc


class RavenwingAttackBikeSquad(Unit):
    type_name = 'Ravenwing Attack Bike Squad'
    type_id = 'attack_bikes_v3'

    class ListBike(RavenwingBikeSquad.AttackBike, ListSubUnit):
        def __init__(self, parent):
            super(RavenwingAttackBikeSquad.ListBike, self).__init__(parent)
            self.wep2.visible = False

    def __init__(self, parent):
        super(RavenwingAttackBikeSquad, self).__init__(parent)
        self.models = UnitList(self, self.ListBike, 1, 3)

    def get_count(self):
        return self.models.count


class RavenwingSpeeders(Unit):
    type_name = "Ravenwing Land Speeders"
    type_id = "ravenwing_speeders_v3"

    class LandSpeeder(ListSubUnit, SpaceMarinesBaseVehicle):
        class BaseWeapon(OneOf):
            def __init__(self, parent):
                super(RavenwingSpeeders.LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy Bolter', 0)
                self.heavyflamer = self.variant('Heavy Flamer', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        class UpWeapon(OptionsList):
            def __init__(self, parent):
                super(RavenwingSpeeders.LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Upgrade',
                                                                                    limit=1)
                self.heavybolter = self.variant('Heavy Bolter', 5)
                self.heavyflamer = self.variant('Heavy Flamer', 5)
                self.multimelta = self.variant('Multi-melta', 15)
                self.assaultcannon = self.variant('Assault Cannon', 20)
                self.typhoonmissilelauncher = self.variant('Typhoon Missile Launcher', 25)

        def __init__(self, parent):
            super(RavenwingSpeeders.LandSpeeder, self).__init__(parent=parent, points=50, name="Ravenwing Land Speeder")
            self.wep = self.BaseWeapon(self)
            self.up = self.UpWeapon(self)

    def __init__(self, parent):
        super(RavenwingSpeeders, self).__init__(parent=parent)
        self.speeders = UnitList(parent=self, unit_class=self.LandSpeeder, min_limit=1, max_limit=5)

    def get_count(self):
        return self.speeders.count

    def get_unique_gear(self):
        return sum((u.get_unique_gear() * u.count for u in self.speeders.units), [])


class RavenwingDarkShroud(SpaceMarinesBaseVehicle):
    type_name = 'Ravenwing Darkshroud'
    type_id = 'ravenwing_darkshroud_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(RavenwingDarkShroud.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.assaultcannon = self.variant('Assault cannon', 15)

    def __init__(self, parent):
        super(RavenwingDarkShroud, self) .__init__(parent=parent, points=80)
        self.wep = self.Weapon(self)


class NephilimJetfighter(SpaceMarinesBaseVehicle):
    type_name = 'Nephilim Jetfighter'
    type_id = 'nephilim_jetfighter_v3'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(NephilimJetfighter.Weapon, self).__init__(parent=parent, name='Weapon')
            self.avangermegabolter = self.variant('Avanger mega bolter', 0)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 5)

    def __init__(self, parent):
        super(NephilimJetfighter, self) .__init__(parent=parent, points=170, gear=[
            Gear('Twin-linked heavy bolter'),
            Gear('Blacksword missile', count=6)
        ])
        self.wep = self.Weapon(self)


class Jetfighters(SpaceMarinesBaseSquadron):
    type_name = 'Nephilim Jetfighters'
    type_id = 'nephilim_jetfighter_wing_v3'
    unit_class = NephilimJetfighter
    unit_max = 4


class RavenwingDarkTalon(SpaceMarinesBaseVehicle):
    type_name = 'Ravenwing Dark Talon'
    type_id = 'ravenwing_dark_talon_v3'

    def __init__(self, parent):
        super(RavenwingDarkTalon, self) .__init__(parent=parent, points=160, gear=[
            Gear('Hurricane bolter', count=2),
            Gear('Rift cannon'),
            Gear('Stasis bomb'),
        ], static=True)


class DarkTalons(SpaceMarinesBaseSquadron):
    type_name = 'Ravenwing Dark Talons'
    type_id = 'dark_talons_wing_v3'
    unit_class = RavenwingDarkTalon
    unit_max = 4


class RavenwingBlackKnights(Unit):
    type_name = 'Ravenwing Black Knights'
    type_id = 'ravenwing_black_knights_v3'

    model_gear = [Gear('Power Armour'), Gear('Frag grenades'), Gear('Krak grenades'),
                  Gear('Teleport Homer'), Gear('Bolt pistol')]
    model_points = 40

    class Huntmaster(Unit):
        class Weapon(OneOf):
            def __init__(self, parent):
                super(RavenwingBlackKnights.Huntmaster.Weapon, self).__init__(parent=parent, name='Weapon')
                self.corvushummer = self.variant('Corvus hammer', 0)
                self.powersword = self.variant('Power weapon', 12)

        class Options(OptionsList):
            def __init__(self, parent):
                super(RavenwingBlackKnights.Huntmaster.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)

        def __init__(self, parent):
            super(RavenwingBlackKnights.Huntmaster, self).__init__(
                name='Ravenwing Huntmaster',
                parent=parent, points=120 - RavenwingBlackKnights.model_points * 2,
                gear=RavenwingBlackKnights.model_gear + [Gear('Plasma talon')])
            self.weapon = self.Weapon(self)
            self.opt = self.Options(self)

    def __init__(self, parent):
        super(RavenwingBlackKnights, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Huntmaster(None))
        self.marines = Count(parent=self, name='Ravenwing Black Knight', min_limit=2, max_limit=9,
                             points=self.model_points, per_model=True)
        self.gl = Count(self, 'Ravenwing grenade launcher', 0, 1, 0)

    def get_count(self):
        return self.marines.cur + 1

    def check_rules(self):
        super(RavenwingBlackKnights, self).check_rules()
        self.gl.max = int(self.get_count() / 3)

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Ravenwing Black Knight',
            options=RavenwingBlackKnights.model_gear + [Gear('Corvus hammer')],
            points=RavenwingBlackKnights.model_points
        )
        spec_marine = marine.clone()
        spec_marine.add(Gear(self.gl.name))
        spec_marine.count = self.gl.cur
        desc.add(spec_marine)
        marine.add(Gear('Plasma talon'))
        marine.count = self.marines.cur - self.gl.cur
        desc.add(marine)
        return desc
