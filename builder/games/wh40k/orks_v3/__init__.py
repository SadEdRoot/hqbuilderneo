__author__ = 'Denis Romanov'

from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, Detachment, PrimaryDetachment, Wh40kKillTeam,\
    PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, FlyerSection
from builder.games.wh40k.fortifications import Fort
from builder.games.wh40k.escalation.orks import LordsOfWar as EscLordsofWar, Stompa
from builder.games.wh40k.imperial_armour.apocalypse.orks import KustomFortress,\
    KillKrusha, KillBursta, KillBlasta
from builder.games.wh40k.dreadmob_v2 import Junka, GrotTanks, GrotMegaTank,\
    Warkoptas
from builder.games.wh40k.imperial_armour.volume8.orks import Zhadsnark, Buzzgob
from builder.games.wh40k.imperial_armour.aeronautika.orks import FightaBommer,\
    FlakkTrukks, AttakFightas, FlakkTrakk
from .hq import *
from .elites import *
from .troops import *
from .fast import *
from .heavy import *
from .lords import Ghazghkull
from .formations import *


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        UnitType(self, Warboss)
        UnitType(self, Weirdboy)
        UnitType(self, Mek, slot=0)
        self.big_mek = UnitType(self, BigMek)
        UnitType(self, Painboy)
        UnitType(self, Grotsnik)
        UnitType(self, Badrukk)
        UnitType(self, Zagstruk)
        UnitType(self, Grukk)


class HQ(BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.rippa = UnitType(self, Zhadsnark)
        self.buzzgob = UnitType(self, Buzzgob)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        UnitType(self, BurnaBoyz)
        UnitType(self, Tankbustas)
        UnitType(self, Nobz)
        UnitType(self, Meganobz)
        self.kmdz = UnitType(self, Kommandos)
        self.snikrot = UnitType(self, Snikrot)
        UnitType(self, SkrakNobz)


class Elites(BaseElites):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, Junka)
        self.grot_tanks = UnitType(self, GrotTanks)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        UnitType(self, Boyz)
        UnitType(self, Gretchin)
        UnitType(self, RustgobRunts)


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, Trukk)
        UnitType(self, Stormboyz)
        UnitType(self, Deffkoptas)
        jet = UnitType(self, Dakkajet)
        jet.visible = False
        UnitType(self, Dakkajets)
        burna = UnitType(self, BurnaBommer)
        burna.visible = False
        UnitType(self, BurnaBommers)
        blitza = UnitType(self, BlitzaBommer)
        blitza.visible = False
        UnitType(self, BlitzaBommers)
        UnitType(self, Warbikers)
        UnitType(self, Warbuggies)
        UnitType(self, WazbomBlastajets)


class FastAttack(BaseFastAttack):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        self.grot_mega = UnitType(self, GrotMegaTank)
        UnitType(self, Warkoptas)
        UnitType(self, FightaBommer)
        UnitType(self, AttakFightas)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, Battlewagon)
        self.slow = [
            UnitType(self, MekGunz),
            UnitType(self, DeffDread),
            UnitType(self, KillaKans)
        ]
        UnitType(self, Lootas)
        UnitType(self, Flashgitz)
        UnitType(self, Gorkanaut)
        UnitType(self, Morkanaut)
        UnitType(self, LootedWagon)
        UnitType(self, KrumpaKanz)


class HeavySupport(BaseHeavySupport):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        UnitType(self, FlakkTrukks)
        UnitType(self, FlakkTrakk)


class LordsOfWar(EscLordsofWar):
    def __init__(self, parent):
        super(LordsOfWar, self).__init__(parent)
        UnitType(self, Ghazghkull)
        UnitType(self, KustomFortress)
        UnitType(self, KillKrusha)
        UnitType(self, KillBursta)
        UnitType(self, KillBlasta)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [
            Dakkajets, BlitzaBommers, BurnaBommers, WazbomBlastajets])


class OrksV3Base(Wh40kBase):

    def __init__(self):
        super(OrksV3Base, self).__init__(
            hq=HQ(parent=self), elites=Elites(parent=self), troops=Troops(parent=self),
            fast=FastAttack(parent=self), heavy=HeavySupport(parent=self),
            fort=Fort(parent=self), lords=LordsOfWar(parent=self)
        )

    def check_rules(self):
        super(OrksV3Base, self).check_rules()
        self.elites.snikrot.slot = self.elites.kmdz.count < 1
        has_meks = self.hq.big_mek.count + self.hq.buzzgob.count
        if has_meks == 0 and (self.elites.grot_tanks.count > 0 or self.fast.grot_mega.count > 0):
            self.error("Grot tank mobs and Grot Mega-tanks can only be taken if army includes a Big Mek!")
        self.fast.grot_mega.active = has_meks
        self.elites.grot_tanks.active = has_meks

        if self.hq.rippa.count and self.hq.rippa.count == len(self.hq.units)\
           and type(self.parent.parent) is PrimaryDetachment:
            if any(_type.count for _type in self.heavy.slow):
                self.error("Deff Dreads, Mek Guns and Killa Kans cannot be taken in Zhadsnark is Warlord")

    def is_ghazkull(self):
        return False


class OrksV3CAD(OrksV3Base, CombinedArmsDetachment):
    army_id = 'orks_v3_cad'
    army_name = 'Orks (Combined arms detachment)'


class OrksV3AD(OrksV3Base, AlliedDetachment):
    army_id = 'orks_v3_ally'
    army_name = 'Orks (Allied detachment)'


class OrksV3PA(OrksV3Base, PlanetstrikeAttacker):
    army_id = 'orks_v3_pa'
    army_name = 'Orks (Planetstrike attacker detachment)'


class OrksV3PD(OrksV3Base, PlanetstrikeDefender):
    army_id = 'orks_v3_pd'
    army_name = 'Orks (Planetstrike defender detachment)'


class OrksV3SA(OrksV3Base, SiegeAttacker):
    army_id = 'orks_v3_sa'
    army_name = 'Orks (Siege War attaker detachment)'


class OrksV3SD(OrksV3Base, SiegeDefender):
    army_id = 'orks_v3_sd'
    army_name = 'Orks (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'orks_v3_asd'
    army_name = 'Orks (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class OrksV3Horde(OrksV3CAD):
    army_id = 'orks_v3_horde'
    army_name = 'Orks (Horde detachment)'

    def __init__(self):
        super(OrksV3Horde, self).__init__()
        self.hq.min, self.hq.max = (1, 3)
        self.troops.min, self.troops.max = (3, 9)


class GreatWaagh(OrksV3CAD):
    army_id = 'orks_v3_ghazghkull'
    army_name = 'Great Waaagh! detachment'

    def __init__(self):
        super(GreatWaagh, self).__init__()
        self.elites.min, self.elites.max = (1, 4)

    def is_ghazkull(self):
        return True


class OrksV3KillTeam(Wh40kKillTeam):
    army_id = 'orks_v3_kt'
    army_name = 'Orks'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(OrksV3KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [Boyz])
            UnitType(self, Gretchin)
            UnitType(self, RustgobRunts)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(OrksV3KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [BurnaBoyz, Tankbustas])
            Wh40kKillTeam.process_transported_units(self, [Nobz], lambda u: [u.transport.wagon])
            self.kmdz = UnitType(self, Kommandos)
            self.snikrot = UnitType(self, Snikrot)
            UnitType(self, SkrakNobz)

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(OrksV3KillTeam.KTFastAttack, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [Trukk, Warbuggies])
            UnitType(self, Stormboyz)
            UnitType(self, Deffkoptas)
            UnitType(self, Warbikers)

    def __init__(self):
        super(OrksV3KillTeam, self).__init__(self.KTTroops(self),
                                             self.KTElites(self),
                                             self.KTFastAttack(self))


class GreatWaaghBand(Wh40k7ed):
    army_id = 'orks_v3_great_band'
    army_name = 'Great Waaagh!-Band Detachment'

    class Core(Detachment):
        def __init__(self, parent):
            super(GreatWaaghBand.Core, self).__init__(parent, 'core', 'Core',
                                                [WaaghBand, GoffKillmob], 1, None)

    class Oddboyz(GhazkullFormation):
        army_name = 'Oddboyz'
        army_id = 'orks_v3_oddboyz'

        def __init__(self):
            super(GreatWaaghBand.Oddboyz, self).__init__()
            self.add_type_restriction([
                UnitType(self, Weirdboy),
                UnitType(self, Painboy),
                UnitType(self, Mek),
                UnitType(self, BigMek)
            ], 1, 1)

    class Command(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(GreatWaaghBand.Command, self).__init__(parent, 'command', 'Command',
                                                   [Council, GreatWaaghBand.Oddboyz], 0, 1)

        def check_limits(self):
            self.max = max([1, len(self.core.units)])
            return super(GreatWaaghBand.Command, self).check_limits()

    class Runtz(GhazkullFormation):
        army_name = 'Runtz'
        army_id = 'orks_v3_runtz'

        def __init__(self):
            super(GreatWaaghBand.Runtz, self).__init__()
            UnitType(self, Gretchin, min_limit=1, max_limit=1)

    class Speshulists(GhazkullFormation):
        army_name = 'Speshulists'
        army_id = 'orks_v3_speshulists'

        def __init__(self):
            super(GreatWaaghBand.Speshulists, self).__init__()
            types = [
                UnitType(self, Lootas),
                UnitType(self, BurnaBoyz),
                UnitType(self, Tankbustas),
                UnitType(self, Kommandos),
                UnitType(self, Stormboyz),
                UnitType(self, Flashgitz)
            ]
            self.add_type_restriction(types, 1, 1)

    class Freaks(GhazkullFormation):
        army_name = 'Speed Freeks'
        army_id = 'orks_v3_freeks'

        def __init__(self):
            super(GreatWaaghBand.Freaks, self).__init__()
            types = [
                UnitType(self, Warbikers),
                UnitType(self, Warbuggies),
                UnitType(self, Deffkoptas)
            ]
            self.add_type_restriction(types, 1, 1)

    class Stuff(GhazkullFormation):
        army_name = "Mekboyz' Big Stuff"
        army_id = 'orks_v3_stuff'

        def __init__(self):
            super(GreatWaaghBand.Stuff, self).__init__()
            types = [
                UnitType(self, Stompa),
                UnitType(self, Gorkanaut),
                UnitType(self, Morkanaut),
                UnitType(self, Battlewagon),
                UnitType(self, MekGunz)
            ]
            self.add_type_restriction(types, 1, 1)

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(GreatWaaghBand.Auxilary, self).__init__(parent, 'auxilary', 'Auxilary',
                                                    [JetSkwadron,
                                                     Bullyboyz,
                                                     Vulcha,
                                                     BadrukkGitz,
                                                     Blitz,
                                                     RedSkull,
                                                     KrushingKrew,
                                                     Dreadmob,
                                                     AirArmada,
                                                     GreatWaaghBand.Runtz,
                                                     BlitzaSkwadron,
                                                     BurnaSkwadron,
                                                     GreatWaaghBand.Speshulists,
                                                     GreatWaaghBand.Freaks,
                                                     GreatWaaghBand.Stuff], 1, 10)

        def check_limits(self):
            self.min = max([1, len(self.core.units)])
            self.max = max([1, len(self.core.units)]) * 10
            return super(GreatWaaghBand.Auxilary, self).check_limits()

    def __init__(self):
        core = self.Core(self)
        command = self.Command(self, core)
        aux = self.Auxilary(self, core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, core, aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()


faction = 'Orks'


class OrksV3(Wh40kImperial, Wh40k7ed):
    army_id = 'orks_v3'
    army_name = 'Orks'
    # development = True
    faction = faction

    def __init__(self):
        super(OrksV3, self).__init__([
            OrksV3CAD,
            OrksV3Horde,
            GreatWaagh,
            GreatWaaghBand,
            FlierDetachment,
            OrksWarband,
            Council,
            Bullyboyz,
            Vulcha,
            Blitz,
            Dreadmob,
            RedSkull,
            GreenTide,
            BadrukkGitz,
            AirArmada,
            JetSkwadron,
            BlitzaSkwadron,
            BurnaSkwadron,
            WaaghBand,
            GoffKillmob,
            RippingKrew,
            KrushingKrew,
            Bossboyz,
            Skwadron,
            KustomWazmob
        ])


class OrksV3Missions(Wh40kImperial, Wh40k7edMissions):
    army_id = 'orks_v3_mis'
    army_name = 'Orks'
    # development = True
    faction = faction

    def __init__(self):
        super(OrksV3Missions, self).__init__([
            OrksV3CAD,
            OrksV3Horde,
            GreatWaagh,
            OrksV3PA,
            OrksV3PD,
            OrksV3SA,
            OrksV3SD,
            GreatWaaghBand,
            FlierDetachment,
            OrksWarband,
            Council,
            Bullyboyz,
            Vulcha,
            Blitz,
            Dreadmob,
            RedSkull,
            GreenTide,
            BadrukkGitz,
            AirArmada,
            JetSkwadron,
            BlitzaSkwadron,
            BurnaSkwadron,
            WaaghBand,
            GoffKillmob,
            RippingKrew,
            KrushingKrew,
            Bossboyz,
            Skwadron,
            KustomWazmob
        ])


detachments = [
    OrksV3CAD, OrksV3PA, OrksV3PD,
    OrksV3SA, OrksV3SD, OrksV3AD, OrksV3Horde, GreatWaagh,
    GreatWaagh, GreatWaaghBand, FlierDetachment, OrksWarband, Council,
    Bullyboyz, Vulcha, Blitz, Dreadmob, RedSkull, GreenTide,
    BadrukkGitz, AirArmada, JetSkwadron, BlitzaSkwadron,
    BurnaSkwadron, WaaghBand, GoffKillmob, RippingKrew, KrushingKrew,
    Skwadron, KustomWazmob
]


unit_types = [
    Warboss, Weirdboy, Mek, BigMek, Painboy, Grotsnik,
    Badrukk, Zagstruk, Grukk, Zhadsnark, Buzzgob, BurnaBoyz,
    Tankbustas, Nobz, Meganobz, Kommandos, Snikrot, SkrakNobz, Junka,
    GrotTanks, Boyz, Gretchin, RustgobRunts, Trukk, Stormboyz,
    Deffkoptas, Dakkajets, BurnaBommers, BlitzaBommers, Warbikers,
    Warbuggies, WazbomBlastajets, GrotMegaTank, Warkoptas,
    FightaBommer, AttakFightas, Battlewagon, MekGunz, DeffDread,
    KillaKans, Lootas, Flashgitz, Gorkanaut, Morkanaut, LootedWagon,
    KrumpaKanz, FlakkTrukks, FlakkTrakk, Ghazghkull, KustomFortress,
    KillKrusha, KillBursta, KillBlasta,
]
