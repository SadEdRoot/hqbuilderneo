__author__ = 'Denis Romanow'

from builder.core2 import OptionsList, ListSubUnit, UnitList,\
    OneOf, Count, Gear, UnitDescription, OptionalSubUnit, SubUnit
from builder.games.wh40k.roster import Unit
from .armory import VehicleEquipment
from .fast import Trukk
from .hq import Mek


class Battlewagon(Unit):
    type_id = 'wagon_v1'
    type_name = "Battlewagon"

    class MainWeapon(OptionsList):
        def __init__(self, parent):
            super(Battlewagon.MainWeapon, self).__init__(parent, name='Weapons')
            self.variant('Killkannon', 30)

    class ExclusiveWeapon(OptionsList):
        def __init__(self, parent):
            super(Battlewagon.ExclusiveWeapon, self).__init__(parent, name='', limit=1)
            self.variant("Kannon", 10)
            self.variant("Lobba", 15)
            self.variant("Zzap gun", 10)

    class Options(VehicleEquipment):
        def __init__(self, parent):
            super(Battlewagon.Options, self).__init__(parent)
            self.rolla = self.variant("Deff rolla", 10)
            self.variant("'ard case", 15)
            self.variant("Grabbin' klaw", 5)

        def has_deffrolla(self):
            return self.rolla.value

    def __init__(self, parent):
        super(Battlewagon, self).__init__(parent, name=self.type_name, points=110)
        self.primary = self.MainWeapon(self)
        self.secondary = self.ExclusiveWeapon(self)
        self.bs = Count(self, "Big shoota", 0, 4, 5)
        self.rl = Count(self, "Rokkit launcha", 0, 4, 5)
        self.opt = self.Options(self)

    def check_rules(self):
        super(Battlewagon, self).check_rules()
        Count.norm_counts(0, 4, [self.bs, self.rl])


class MekGunz(Unit):
    type_id = 'gunz_v1'
    type_name = 'Mek Gunz'

    class MekGun(ListSubUnit):
        name = 'MekGun'
        base_points = 18 - 6

        class Weapon(OneOf):
            def __init__(self, parent):
                super(MekGunz.MekGun.Weapon, self).__init__(parent, 'Piece')
                self.variant("Kannon", 0)
                self.variant("Lobba", 0)
                self.variant("Zzap gun", 5)
                self.variant("Bubblechukka", 12)
                self.variant("Kustom mega-kannon", 12)
                self.variant("Smasha gun", 12)
                self.variant("Traktor kannon", 12)

        class Options(OptionsList):
            def __init__(self, parent):
                super(MekGunz.MekGun.Options, self).__init__(parent, 'Options')
                self.variant("Ammo runt", 3)

        def __init__(self, parent):
            super(MekGunz.MekGun, self).__init__(parent, self.name, self.base_points)
            self.crew = Count(self, 'Gretchin', 2, 4, 3, True, gear=UnitDescription('Gretchin', 3, options=[Gear('Close combat weapon')]))
            self.wep = self.Weapon(self)
            self.opt = self.Options(self)

    def __init__(self, parent):
        super(MekGunz, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.MekGun, 1, 5)

    def build_statistics(self):
        return {'Models': self.models.count * 4, 'Units': 1}


class DeffDread(Unit):
    type_id = 'ddred_v1'
    type_name = "Deff Dread"

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DeffDread.Weapon, self).__init__(parent, name='Weapon')
            self.variant("Big shoota", 00)
            self.variant("Rokkit launcha", 0)
            self.variant("Kustom mega-blasta", 5)
            self.variant('Skorcha', 5)
            self.variant('Power klaw', 10)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DeffDread.Options, self).__init__(parent, name='Options')
            self.variant("Grot riggers", 10)
            self.variant("Extra armour", 10)

    def __init__(self, parent, points=80):
        super(DeffDread, self).__init__(parent, name=self.type_name,
                                        points=points, gear=[Gear('Power klaw', count=2)])
        self.wep1 = self.Weapon(self)
        self.wep2 = self.Weapon(self)
        self.opt = self.Options(self)


class KillaKans(Unit):
    type_id = 'kans_v1'
    type_name = "Killa kans"
    model_points = 50

    class KillaKan(ListSubUnit):
        model_name = 'Killa kan'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(KillaKans.KillaKan.Weapon, self).__init__(parent, name='Weapon')
                self.variant("Big shoota", 0)
                self.variant("Rokkit launcha", 0)
                self.variant("Grotzooka", 5)
                self.variant("Kustom mega-blasta", 5)
                self.variant('Skorcha', 5)

        class Options(OptionsList):
            def __init__(self, parent):
                super(KillaKans.KillaKan.Options, self).__init__(parent, name='Options')
                self.variant("Grot riggers", 5)
                self.variant("Extra armour", 10)

        def __init__(self, parent):
            super(KillaKans.KillaKan, self).__init__(parent, name=self.model_name, gear=[Gear('Kan klaw')])
            self.wep = self.Weapon(self)
            self.opt = self.Options(self)
            self.base_points = self.root_unit.model_points

    def __init__(self, parent):
        super(KillaKans, self).__init__(parent, name=self.type_name)
        self.models = UnitList(self, self.KillaKan, 1, 5)

    def get_count(self):
        return self.models.count


class Gorkanaut(Unit):
    type_id = 'gorka_v1'
    type_name = "Gorkanaut"

    class Options(OptionsList):
        def __init__(self, parent):
            super(Gorkanaut.Options, self).__init__(parent, name='Options')
            self.variant("Grot riggers", 20)
            self.variant("Extra armour", 10)

    def __init__(self, parent):
        super(Gorkanaut, self).__init__(parent, name=self.type_name, points=245, gear=[Gear('Deffstorm mega-shoota'), Gear('Twin-linked big shoota', count=2), Gear('Rokkit launcha', count=2), Gear('Skorcha'), Gear('Klaw of Gork')])
        self.opt = self.Options(self)


class Morkanaut(Unit):
    type_id = 'morka_v1'
    type_name = "Morkanaut"

    class Options(OptionsList):
        def __init__(self, parent):
            super(Morkanaut.Options, self).__init__(parent, name='Options')
            self.variant('Kustom force field', 50)
            self.variant("Grot riggers", 20)
            self.variant("Extra armour", 10)

    def __init__(self, parent):
        super(Morkanaut, self).__init__(parent, name=self.type_name, points=230, gear=[Gear('Kustom mega-kannon'), Gear('Kustom mega-blasta'), Gear('Twin-linked big shoota', count=2), Gear('Rokkit launcha', count=2), Gear('Klaw of Mork')])
        self.opt = self.Options(self)


class Lootas(Unit):
    type_id = 'lootas_v1'
    type_name = 'Lootas'

    base_points = 0
    model_points = 14
    model_description = UnitDescription(name=type_name, points=model_points, options=[Gear('Deffgun'), Gear('Stikkbombs')])
    boyz_min_def = 5
    boyz_max_def = 15

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Lootas.Transport, self).__init__(parent, 'Transport')
            self.trukk = SubUnit(self, Trukk(parent=None))

    def get_transport(self):
        return Lootas.Transport

    class Upgrades(OptionsList):
        def __init__(self, parent):
            super(Lootas.Upgrades, self).__init__(parent, 'Upgrades')
            self.to_mek = self.variant('To Mek', 0)

        @property
        def description(self):
            return []

    class LootaMeks(UnitList):
        class LootaMek(Mek, ListSubUnit):
            model_points = 14
            killsaw_cost = 20
            pass

        def __init__(self, parent):
            super(Lootas.LootaMeks, self).__init__(parent, self.LootaMek, min_limit=0, max_limit=3)

    def __init__(self, parent):
        super(Lootas, self).__init__(parent, points=self.base_points)
        self.boyz = Count(self, 'Loota', self.boyz_min_def, self.boyz_max_def, self.model_points, gear=self.model_description)

        self.bosses = self.LootaMeks(self)
        self.transport = self.get_transport()(self)
        self.opt = self.Upgrades(self)

    def get_count(self):
        return self.boyz.cur + (self.bosses.count if self.opt.any else 0)

    def check_rules(self):
        self.bosses.used = self.bosses.visible = self.opt.any
        self.boyz.min = self.boyz_min_def - (self.bosses.count if self.opt.any else 0)
        self.boyz.max = self.boyz_max_def - (self.bosses.count if self.opt.any else 0)

    def build_statistics(self):
        return self.note_transport(super(Lootas, self).build_statistics())


class Flashgitz(Unit):
    type_id = 'flashg_v1'
    type_name = 'Flash Gitz'

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Flashgitz.Transport, self).__init__(parent, 'Transport')
            self.trukk = SubUnit(self, Trukk(parent=None))
            self.wagon = SubUnit(self, Battlewagon(parent=None))

    class Basegit(Unit):
        base_points = 22

        class Options(OptionsList):
            def __init__(self, parent):
                super(Flashgitz.Basegit.Options, self).__init__(parent, 'Options')
                self.Variant(self, 'Ammo runt', 3)

        def __init__(self, parent, name='Flash Gitz'):
            super(Flashgitz.Basegit, self).__init__(parent, name=name, points=self.base_points, gear=[Gear('Snazzgun'), Gear('Stikkbombz'), Gear('Bosspole'), Gear('Gitfinda')])
            self.opt = self.Options(self)

    class Flashgit(Basegit, ListSubUnit):
        pass

    def __init__(self, parent):
        super(Flashgitz, self).__init__(parent, self.type_name)
        self.boss = SubUnit(self, self.Basegit(parent=None, name='Kaptin'))
        self.gitz = UnitList(self, unit_class=self.Flashgit, min_limit=4, max_limit=9)
        self.transport = self.Transport(self)

    def get_count(self):
        return self.gitz.count + self.boss.count

    def build_statistics(self):
        return self.note_transport(super(Flashgitz, self).build_statistics())


class LootedWagon(Unit):
    type_id = 'looted_v3'
    type_name = 'Looted Wagon'
    base_points = 37

    def __init__(self, parent):
        super(LootedWagon, self).__init__(parent, 'Looted Wagon', self.base_points)
        self.primary = Battlewagon.MainWeapon(self)
        self.extra = [
            Count(self, "Big shoota", 0, 3, 5),
            Count(self, "Rokkit launcha", 0, 3, 5),
            Count(self, "Skorcha", 0, 3, 5)
        ]
        self.opt = Battlewagon.Options(self)

    def check_rules(self):
        super(LootedWagon, self).check_rules()
        Count.norm_counts(0, 3, self.extra)


class KrumpaKanz(Unit):
    type_name = "Krumpa's Killa Kanz"
    type_id = 'krumpa_v1'

    def __init__(self, parent):
        super(KrumpaKanz, self).__init__(parent, points=160, static=True, gear=[
            UnitDescription('Killa Kan', options=[Gear('Kan klaw'), Gear('Big shoota')]),
            UnitDescription('Killa Kan', options=[Gear('Kan klaw'), Gear('Rokkit launcha')]),
            UnitDescription('Krumpa', options=[Gear('Kan klaw'), Gear('Grotzooka')])
        ])

    def get_count(self):
        return 3

    def get_unique(self):
        return 'Krumpa'
