__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'
__summary__ = ['Space Marine Codex 7th edition', 'Warzone Damocles: Kayoun', 'Space Marine Codex Suppliment Angels Of Death']


from builder.games.wh40k.roster import Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Wh40kImperial, PrimaryDetachment, Wh40kBase, Wh40kKillTeam
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, LordsOfWarSection, FlyerSection, Detachment
from builder.games.wh40k.fortifications import Fort

from builder.games.wh40k.escalation.space_marines\
    import LordsOfWar as MarinesLordsOfWar
from builder.games.wh40k.escalation.imperial_titans import LordsOfWar as Titans
from builder.games.wh40k.imperial_armour import volume9_10
from builder.games.wh40k.imperial_armour.volume8.space_marines import Korvydae
from builder.games.wh40k.imperial_armour.volume4.space_marines import Culln,\
    Halar
from builder.games.wh40k.imperial_armour.volume12.space_marines import Hecaton
from builder.games.wh40k.imperial_armour.dataslates.space_marines import Deredeo, Xiphon,\
    Leviathan
from builder.games.wh40k.imperial_armour.dataslates.imperial_knights import CerastusSection
from builder.games.wh40k.imperial_armour.aeronautika.space_marines import HyperiosBattery
from builder.games.wh40k.dataslates.command_tanks import SpaceMarineCommandTanks
from .tyrannic_veterans import SaintTylus
from .hq import Sicarius, Tigurius, Cassius, Telion, Chronus,\
    Khan, Vulkan, Shrike, Lysander, Cantor, Helbrecht, Champion, Grimaldus,\
    Captain, Librarian, Chaplain, Techmarine, Solaq, TerminatorCaptain
from .elites import CommandSquad, HonourGuardSquad, CenturionAssault,\
    VanguardVeteranSquad, SternguardVeteranSquad, Dreadnoughts,\
    VenDreadnoughts, IronDreds, TheDamned, TerminatorSquad,\
    TerminatorAssaultSquad, SquadAmerex, SquadDarvos, CataphractiiTerminatorSquad, ContemptorDreadnoughts
from .troops import TacticalSquad, ScoutSquad, CrusaderSquad, GaradonSquad
from .fast import LandSpeederStorm, AssaultSquad, ScoutBikers, BikeSquad,\
    AttackBikeSquad, LandSpeederSquadron, Stormtalon, Darkwind, Stormhawks,\
    Stormtalons
from .heavy import LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Devastators,\
    Thunderfire, CenturionDevastators, Predators, Stalkers, Hunters, Vindicators,\
    Whirlwinds, StormravenGunship, Stormravens, Predator, Vindicator, DeimosVindicator
from .lords import Calgar, Guilliman
from .formations import *

from .transport import DropPod, Rhino, Razorback
from itertools import chain


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.sicarius = UnitType(self, Sicarius)
        self.tigurius = UnitType(self, Tigurius)
        self.cassius = UnitType(self, Cassius)
        UnitType(self, Telion)
        UnitType(self, Chronus)
        self.khan = UnitType(self, Khan)
        self.vulkan = UnitType(self, Vulkan)
        self.shrike = UnitType(self, Shrike)
        UnitType(self, Solaq)
        self.lysander = UnitType(self, Lysander)
        self.cantor = UnitType(self, Cantor)
        self.helbrecht = UnitType(self, Helbrecht)
        self.grimaldus = UnitType(self, Grimaldus)
        self.champion = UnitType(self, Champion)
        self.captain = UnitType(self, Captain)
        self.terminator_capitan = UnitType(self, TerminatorCaptain)
        self.librarian = UnitType(self, Librarian)
        self.chaplain = UnitType(self, Chaplain)
        self.techmarine = UnitType(self, Techmarine)
        self.bike_hq = [self.khan, self.captain, self.librarian,
                        self.chaplain, self.techmarine]
        self.garadon = UnitType(self, GaradonSquad, active=False)
        UnitType(self, SpaceMarineCommandTanks)

    def has_biker_hq(self):
        return any([unit.has_bike() for unit in
                    chain(*[_type.units for _type in self.bike_hq])])

        # self.tactical_squad = UnitType(self, TacticalSquad, visible=False)

    def check_limits(self):
        if not self.roster.is_hands():
            return super(BaseHQ, self).check_limits()
        full_count = len(self.units)
        tech_count = self.techmarine.count
        part_count = full_count - tech_count
        discount = min(part_count * 2, tech_count)
        return self.min <= (full_count - discount) <= self.max


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        UnitType(self, CommandSquad)
        UnitType(self, HonourGuardSquad)
        self.vanguard_veteran_squad = UnitType(self, VanguardVeteranSquad)
        self.sternguard_veteran_squad = UnitType(self, HaasSternguards)
        self.dreadnought = UnitType(self, Dreadnoughts)
        UnitType(self, VenDreadnoughts)
        UnitType(self, ContemptorDreadnoughts)
        self.iron_dred = UnitType(self, IronDreds)
        self.damned = UnitType(self, TheDamned)
        self.terminator_assault_squad = UnitType(self, TerminatorAssaultSquad)
        self.terminator_squad = UnitType(self, TerminatorSquad)
        self.cataphractii_terminator_squad = UnitType(self, CataphractiiTerminatorSquad)
        self.centurion_assault = UnitType(self, CenturionAssault)
        self.centurion_devastators = UnitType(self, CenturionDevastators, active=False)
        self.devastators = UnitType(self, KraatosDevastators, active=False)
        UnitType(self, SquadDarvos)
        UnitType(self, SquadAmerex)


class Troops(TroopsSection):
    def __init__(self, parent):
        super(Troops, self).__init__(parent)
        self.tactical_squad = UnitType(self, TacticalSquad)
        self.scout_squad = UnitType(self, ScoutSquad)
        self.crusader_squad = UnitType(self, CrusaderSquad)
        self.bike_squad = UnitType(self, BikeSquad, active=False)
        self.assault_squad = UnitType(self, AssaultSquad, active=False)

    def check_limits(self):
        min_count = len(self.units)
        max_count = sum([2 if tr.get_count() == 10 else 1 for tr in self.units])
        return self.min <= max_count and min_count <= self.max


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        self.assault_squad = UnitType(self, AssaultSquad)
        self.land_speeder = UnitType(self, LandSpeederSquadron)
        talon = UnitType(self, Stormtalon)
        talon.visible = False
        UnitType(self, Stormtalons)
        UnitType(self, Stormhawks)
        self.bike_squad = UnitType(self, BikeSquad)
        self.attack_bike_squad = UnitType(self, AttackBikeSquad)
        self.scout_bike_squad = UnitType(self, ScoutBikers)
        self.centurion_assault = UnitType(self, CenturionAssault, active=False)
        UnitType(self, Rhino)
        UnitType(self, Razorback)
        UnitType(self, DropPod)
        UnitType(self, LandSpeederStorm)
        UnitType(self, Darkwind)


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        self.devastators = UnitType(self, KraatosDevastators)
        self.centurion_devastators = UnitType(self, CenturionDevastators)
        self.thunderfire = UnitType(self, Thunderfire)
        self.predator = UnitType(self, Predators)
        self.whirlwind = UnitType(self, Whirlwinds)
        self.vindicator = UnitType(self, Vindicators)
        self.hunter = UnitType(self, Hunters)
        self.stalker = UnitType(self, Stalkers)
        self.land_raider = UnitType(self, LandRaider)
        self.land_raider_crusader = UnitType(self, LandRaiderCrusader)
        self.land_raider_redeemer = UnitType(self, LandRaiderRedeemer)
        raven = UnitType(self, StormravenGunship)
        raven.visible = False
        UnitType(self, Stormravens)
        self.dreads = [
            UnitType(self, Dreadnoughts, active=False),
            UnitType(self, VenDreadnoughts, active=False),
            UnitType(self, IronDreds, active=False),
            UnitType(self, ContemptorDreadnoughts, active=False)
        ]


class HQ(volume2.SpaceMarineHQ, volume9_10.SpaceMarinesBadabHq, BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        self.korvydae = UnitType(self, Korvydae)
        self.young_culln = UnitType(self, Culln)
        self.honour = UnitType(self, HonourGuardSquad, slot=0, active=False)
        from builder.games.wh40k.dataslates.cypher import Cypher
        self.cypher = UnitType(self, Cypher, slot=0)
        from builder.games.wh40k.adepta_sororitas_v4 import Celestine
        UnitType(self, Celestine)
        from builder.games.wh40k.inquisition_v2 import Greyfax
        UnitType(self, Greyfax)
        from builder.games.wh40k.cult_mechanicus import Cawl
        UnitType(self, Cawl)
        from builder.games.wh40k.grey_knights_v3 import Voldus
        UnitType(self, Voldus)

    def check_rules(self):
        super(HQ, self).check_rules()
        self.cypher.visible = isinstance(self.parent.parent.parent, PrimaryDetachment)

    def chapter_master_count(self):
        return super(HQ, self).chapter_master_count() +\
            self.cantor.count + self.helbrecht.count +\
            sum(u.is_master() for u in self.captain.units)


class FastAttack(volume2.SpaceMarineFast, BaseFastAttack):
    def __init__(self, parent):
        super(FastAttack, self).__init__(parent)
        UnitType(self, HyperiosBattery)
        UnitType(self, Xiphon)


class Elites(volume2.SpaceMarineElites, BaseElites):
    def __init__(self, parent):
        super(Elites, self).__init__(parent)
        UnitType(self, Halar)
        UnitType(self, Hecaton)


class HeavySupport(volume2.MinotaurHeavySupport, BaseHeavySupport):
    def __init__(self, parent):
        super(HeavySupport, self).__init__(parent)
        self.relics.append(UnitType(self, Deredeo))
        UnitType(self, DeimosVindicator)
        self.relics.append(UnitType(self, Leviathan))


class BaseLords(LordsOfWarSection):
    def __init__(self, parent):
        super(BaseLords, self).__init__(parent)
        self.calgar = UnitType(self, Calgar)
        UnitType(self, Guilliman)

    def chapter_master_count(self):
        return self.calgar.count


class Lords(CerastusSection, volume2.SpaceMarineLordsOfWar, Titans,
            MarinesLordsOfWar, BaseLords):
    pass


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [
            Stormravens, Stormtalons, Stormhawks])


class SpaceMarinesRosterCommon(volume2.SpaceMarinesLegaciesRoster):
    custom_imperial_check = True

    def get_chapter(self):
        result = []
        for section in self.sections:
            for unit in section.units:
                result.extend(check_chapter(unit))
        return [chap for chap in result if chap is not None]

    def check_rules(self):
        super(SpaceMarinesRosterCommon, self).check_rules()
        chaplist = list(set(self.get_chapter()))
        if len(chaplist) > 1:
            self.error('Cannot include units from more then one chapter')
            for section in self.sections:
                for utype in section.ia_units:
                    if utype in section.ia_units:
                        utype.active = self.ia_enabled
        elif len(chaplist) == 1:
            for section in self.sections:
                for utype in section.types:
                    flag = True
                    if utype in section.ia_units:
                        flag = self.ia_enabled
                        # import pdb; pdb.set_trace()
                        if flag and hasattr(utype.unit_class, 'chapter'):
                            flag = utype.unit_class.chapter == chaplist[0]
                    else:
                        if hasattr(utype.unit_class, 'chapter'):
                            flag = utype.unit_class.chapter == chaplist[0]
                    utype.active = flag
        else:
            for section in self.sections:
                for utype in section.types:
                    if utype in section.ia_units:
                        utype.active = self.ia_enabled
                    else:
                        if hasattr(utype.unit_class, 'chapter'):
                            utype.active = True


class Gladius(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'space_marines_v3_gladius'
    army_name = 'Gladius strike force'

    class MarineSubFormation(SpaceMarineFormation):
        def __init__(self, *args, **kwargs):
            super(Gladius.MarineSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_base_codex(self):
            return self.parent.roster.is_base_codex()

        def is_fists(self):
            return self.parent.roster.is_fists()

        def is_hands(self):
            return self.parent.roster.is_hands()

        def is_scars(self):
            return self.parent.roster.is_scars()

        def is_ravens(self):
            return self.parent.roster.is_ravens()

        def is_scorpions(self):
            return self.parent.roster.is_scorpions()

        def is_karchodons(self):
            return self.parent.roster.is_karchodons()

        def is_hawks(self):
            return self.parent.roster.is_hawks()

        def is_minotaurs(self):
            return self.parent.roster.is_minotaurs()

        def is_salamanders(self):
            return self.parent.roster.is_salamanders()

        def is_templars(self):
            return self.parent.roster.is_templars()

    @staticmethod
    def make_gladius(formation):
        class BaseSubformation(Gladius.MarineSubFormation, formation):
            pass
        return BaseSubformation

    class Core(Detachment):
        def __init__(self, parent):
            super(Gladius.Core, self).__init__(
                parent, 'core', 'Core',
                [Gladius.make_gladius(DemiCompany)], 1, 2)
            self.dctype = self.types[0]

        def is_full(self):
            if self.dctype.count != 2:
                return False
            chap_cnt = sum([unit.sub_roster.roster.has_chap() for unit in
                            self.dctype.units])
            cap_cnt = sum([unit.sub_roster.roster.has_cap() for unit in
                           self.dctype.units])
            return chap_cnt == 1 and cap_cnt == 1

    class CommandSection(Detachment):
        def __init__(self, parent):
            super(Gladius.CommandSection, self).__init__(
                parent, 'command', 'Command', [
                    Gladius.make_gladius(f) for f in [
                        ReclusiamCommand,
                        StrikeForceCommand,
                        LibrariusConclave
                    ]
                ], 0, 3)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(Gladius.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    Gladius.make_gladius(f) for f in [
                        ArmoredTaskForce,
                        FirstComp,
                        AntiAir,
                        TenthCompany,
                        Stormwing,
                        Suppression,
                        Siegebreaker,
                        RaiderSpearhead
                    ]
                ],
                1, None)

    def __init__(self):
        self.core = self.Core(self)
        command = self.CommandSection(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, self.core, aux], None)
        self.codex = SupplementOptions(parent=self)

    def is_base_codex(self):
        return self.codex.cur in [self.codex.base, self.codex.scars, self.codex.salamanders,
                                  self.codex.ravens, self.codex.karchodons,
                                  self.codex.hawks, self.codex.scorpions]

    def is_fists(self):
        return self.codex.cur == self.codex.fists

    def is_hands(self):
        return self.codex.cur == self.codex.hands

    def is_scars(self):
        return self.codex.cur == self.codex.scars

    def is_ravens(self):
        return self.codex.cur == self.codex.ravens

    def is_scorpions(self):
        return self.codex.cur == self.codex.scorpions

    def is_karchodons(self):
        return self.codex.cur == self.codex.karchodons

    def is_hawks(self):
        return self.codex.cur == self.codex.hawks

    def is_minotaurs(self):
        return self.codex.cur == self.codex.minotaurs

    def is_salamanders(self):
        return self.codex.cur == self.codex.salamanders

    def is_templars(self):
        return self.codex.cur == self.codex.templars

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        discount = self.core.is_full()
        for unit in self.core.units:
            if unit.sub_roster.roster.discount != discount:
                unit.sub_roster.roster.discount = discount
                unit.check_rules_chain()
        chaplist = []
        for section in self.sections:
            for unit in section.units:
                chaplist.extend(unit.sub_roster.roster.get_chapter())
        if len(set(chaplist)) > 1:
            self.error('Cannot include units from more then one chapter')


class Scarblade(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'space_marines_v3_scarblade'
    army_name = 'Scarblade strike force'

    class MarineSubFormation(SpaceMarineFormation):
        def __init__(self, *args, **kwargs):
            super(Scarblade.MarineSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_ultra(self):
            return False

        def is_base_codex(self):
            return True

        def is_fists(self):
            return False

        def is_hands(self):
            return False

        def is_scars(self):
            return True

        def is_ravens(self):
            return False

        def is_karchodons(self):
            return False

        def is_hawks(self):
            return False

        def is_scorpions(self):
            return False

        def is_minotaurs(self):
            return False

        def is_salamanders(self):
            return False

        def is_templars(self):
            return False

    @staticmethod
    def make_scarblade(formation):
        class BaseSubformation(Scarblade.MarineSubFormation, formation):
            pass
        return BaseSubformation

    class Core(Detachment):
        def __init__(self, parent):
            super(Scarblade.Core, self).__init__(
                parent, 'core', 'Core', [
                    Scarblade.make_scarblade(f) for f in [
                        DemiCompany,
                        StormlanceDemiCpmpany,
                        Hunting
                    ]
                ], 1, 2)

    class CommandSection(Detachment):
        def __init__(self, parent):
            super(Scarblade.CommandSection, self).__init__(
                parent, 'command', 'Command', [
                    Scarblade.make_scarblade(f) for f in [
                        ReclusiamCommand,
                        WhiteScarStrikeForceCommand,
                        LibrariusConclave
                    ]
                ], 0, 3)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(Scarblade.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    Scarblade.make_scarblade(f) for f in [
                        Stormbringer,
                        Speartip,
                        ArmoredTaskForce,
                        FirstComp,
                        TenthCompany,
                        Ultra,
                        Stormwing,
                        RaiderSpearhead,
                        AntiAir,
                        Suppression
                    ]
                ],
                1, None)

    def __init__(self):
        self.core = self.Core(self)
        command = self.CommandSection(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, self.core, aux], None)

    def is_scars(self):
        return self.codex.cur == self.codex.scars

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        chaplist = []
        for section in self.sections:
            for unit in section.units:
                chaplist.extend(unit.sub_roster.roster.get_chapter())
        if len(set(chaplist)) > 1:
            self.error('Cannot include units from more then one chapter')


class SpaceMarinesV3Base(volume2.SpaceMarinesRelicRoster,
                         SpaceMarinesRosterCommon):
    army_id = 'space_marines_v3_base'
    army_name = 'Space Marines'

    def has_keeper(self):
        return self.hq.techmarine.count + self.hq.vayland.count\
            + self.hq.armenneus.count > 0

    def is_base_codex(self):
        return self.codex.cur in [self.codex.base, self.codex.scars,
                                  self.codex.ravens, self.codex.karchodons,
                                  self.codex.hawks, self.codex.scorpions,
                                  self.codex.salamanders, self.codex.fists,
                                  self.codex.hands, self.codex.ultra]

    def is_ultra(self):
        return self.codex.cur == self.codex.ultra

    def is_fists(self):
        return self.codex.cur == self.codex.fists

    def is_hands(self):
        return self.codex.cur == self.codex.hands

    def is_scars(self):
        return self.codex.cur == self.codex.scars

    def is_ravens(self):
        return self.codex.cur == self.codex.ravens

    def is_scorpions(self):
        return self.codex.cur == self.codex.scorpions

    def is_karchodons(self):
        return self.codex.cur == self.codex.karchodons

    def is_hawks(self):
        return self.codex.cur == self.codex.hawks

    def is_minotaurs(self):
        return self.codex.cur == self.codex.minotaurs

    def is_salamanders(self):
        return self.codex.cur == self.codex.salamanders

    def is_templars(self):
        return self.codex.cur == self.codex.templars

    def __init__(self):
        self.hq = HQ(parent=self)
        self.elites = Elites(parent=self)
        self.troops = Troops(parent=self)
        self.fast = FastAttack(parent=self)
        self.heavy = HeavySupport(parent=self)
        super(SpaceMarinesV3Base, self).__init__(
            hq=self.hq, elites=self.elites, troops=self.troops, fast=self.fast,
            heavy=self.heavy, lords=Lords(self), fort=Fort(parent=self)
        )

        self.codex = SupplementOptions(self)
        # self.chapters = dict()

    def get_chapter(self):
        chaps = super(SpaceMarinesV3Base, self).get_chapter()
        if self.is_hands():
            chaps.append('iron')
        if self.is_fists():
            chaps.append('fist')
        if self.is_scars():
            chaps.append('scar')
        if self.is_ravens():
            chaps.append('raven')
        if self.is_hawks():
            chaps.append('fire')
        if self.is_scorpions():
            chaps.append('scorpions')
        if self.is_karchodons():
            chaps.append('karchodons')
        if self.is_minotaurs():
            chaps.append('minotaurs')
        if self.is_salamanders():
            chaps.append('salamander')
        if self.is_templars():
            chaps.append('templar')
        return chaps

    def check_rules(self):
        super(SpaceMarinesV3Base, self).check_rules()

        # Bikers in troops
        biker_troops = self.hq.has_biker_hq()
        self.troops.bike_squad.active = biker_troops
        self.move_units(biker_troops, self.fast.bike_squad, self.troops.bike_squad)
        if not biker_troops and self.troops.bike_squad.count > 0:
            self.error("Space Maine bikers can only be taken as troops there is an HQ selection on a bike in the army")
        # no librarians for templars!
        is_templar = self.is_templars()
        self.hq.librarian.active = not is_templar
        if is_templar and self.hq.librarian.count > 0:
            self.error("Black Templars detachments cannot include librarians")

        # dreds in heavy for Iron Hands
        dc = 0
        for dredtype in self.heavy.dreads:
            dc += dredtype.count
            dredtype.active = self.is_hands() or self.hq.armenneus.count > 0
        if not (self.is_hands() or self.hq.armenneus.count > 0) and dc > 0:
            self.error("Only Iron Hands detachment may take dreadnoughts as heavy support")

        # centurions for Imperial Fists
        cc = self.elites.centurion_devastators.count + self.fast.centurion_assault.count
        self.elites.centurion_devastators.active = self.is_fists()
        self.fast.centurion_assault.active = self.is_fists()
        self.hq.garadon.active = self.is_fists()
        if not self.is_fists() and cc > 0:
            self.error("Only Imperial Fists detachment can take Devastators Centurions in Elite or Assault Centurions in Fast Attaack")
        if not self.is_fists() and self.hq.garadon.count > 0:
            self.error("Only Imperial Fists detachment can have Sergeant Garadon in HQ")

        # scouts for Korvydae
        if self.hq.korvydae.count > 0:
            if not self.troops.scout_squad.count:
                self.error("Army led by Shadow Captain Korvydae must include at least one scout squad")
            self.troops.assault_squad.active = True
        else:
            self.troops.assault_squad.active = False
            if self.troops.assault_squad.count > 0:
                self.error("Assault squads cannot be taken as troops!")

        # devastators for Androcles
        if self.hq.androcles.count > 0:
            self.elites.devastators.active = True
        else:
            self.elites.devastators.active = False
            if self.elites.devastators.count > 0:
                self.error("Devastators cannot be taken as elites!")

        # check for characters that always Warlord
        if not type(self.parent.parent) is PrimaryDetachment:
            for _type in [self.hq.culln, self.hq.tyberos, self.hq.issodon,
                          self.hq.ahazra, self.hq.thulsa, self.hq.huron,
                          self.hq.young_culln]:
                if _type.count > 0:
                    self.error("{} must always be your army's Warlord".
                               format(_type.units[0].name))
        # check for allowed honour guards
        honour_count = sum(_type.count > 0 for _type in [
            self.hq.culln, self.hq.sevrin, self.hq.tyberos, self.hq.issodon,
            self.hq.ahazra, self.hq.thulsa, self.hq.vayland, self.hq.huron,
            self.hq.moloc])
        self.hq.honour.active = honour_count > 0
        if self.hq.honour.count > honour_count:
            self.error("Only {} honour guard squads allowed in HQ; taken: {}".
                       format(honour_count, self.hq.honour.count))
        # check number of chapter masters
        master_count = self.hq.chapter_master_count() + (self.lords.chapter_master_count() if self.lords is not None else 0)
        if master_count > 1:
            for _type in [self.hq.culln, self.hq.tyberos, self.hq.issodon,
                          self.hq.vayland, self.hq.huron, self.hq.moloc]:
                if _type.count > 0:
                    self.error("{} must be only Chapter Master in detachment; taken: {}".
                               format(_type.units[0].name, master_count))
        # check for named Masters of the Forge
        if self.hq.vayland.count + self.hq.armenneus.count > 1:
            self.error("Only one named master of the Forge may be present in the army")


class Talon(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'space_marines_v3_talon'
    army_name = 'Talon strike force'

    class MarineSubFormation(SpaceMarineFormation):
        def __init__(self, *args, **kwargs):
            super(Talon.MarineSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_base_codex(self):
            return True

        def is_ultra(self):
            return False

        def is_fists(self):
            return False

        def is_hands(self):
            return False

        def is_scars(self):
            return False

        def is_ravens(self):
            return True

        def is_karchodons(self):
            return False

        def is_hawks(self):
            return False

        def is_scorpions(self):
            return False

        def is_minotaurs(self):
            return False

        def is_salamanders(self):
            return False

        def is_templars(self):
            return False

    @staticmethod
    def make_talon(formation):
        class BaseSubformation(Talon.MarineSubFormation, formation):
            pass
        return BaseSubformation

    class Core(Detachment):
        def __init__(self, parent):
            super(Talon.Core, self).__init__(
                parent, 'core', 'Core', [
                    Talon.make_talon(f) for f in [
                        DemiCompany,
                        PinionDemiCpmpany
                    ]
                ], 1, 2)

    class CommandSection(Detachment):
        def __init__(self, parent):
            super(Talon.CommandSection, self).__init__(
                parent, 'command', 'Command', [
                    Talon.make_talon(f) for f in [
                        ReclusiamCommand,
                        RavenGuardStrikeForceCommand
                    ]
                ], 0, 2)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(Talon.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    Talon.make_talon(f) for f in [
                        FirstComp,
                        TenthCompany,
                        Shadowstrike,
                        RaptorWing,
                        OrbitalSkyhammer,
                        ShadowForce,
                        Bladewing,
                        Ravenhawk,
                        Stormwing,
                        AntiAir,
                        Suppression,
                    ]
                ],
                1, None)

    def __init__(self):
        self.core = self.Core(self)
        command = self.CommandSection(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, self.core, aux], None)

    def is_ravens(self):
        return True

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        chaplist = []
        for section in self.sections:
            for unit in section.units:
                chaplist.extend(unit.sub_roster.roster.get_chapter())
        if len(set(chaplist)) > 1:
            self.error('Cannot include units from more then one chapter')


class Fist(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'space_marines_v3_fist'
    army_name = 'Fist of Medusa Strike Force'

    class MarineSubFormation(SpaceMarineFormation):
        def __init__(self, *args, **kwargs):
            super(Fist.MarineSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_base_codex(self):
            return True

        def is_ultra(self):
            return False

        def is_fists(self):
            return False

        def is_hands(self):
            return True

        def is_scars(self):
            return False

        def is_ravens(self):
            return False

        def is_karchodons(self):
            return False

        def is_hawks(self):
            return False

        def is_scorpions(self):
            return False

        def is_minotaurs(self):
            return False

        def is_salamanders(self):
            return False

        def is_templars(self):
            return False

    @staticmethod
    def make_fist(formation):
        class BaseSubformation(Fist.MarineSubFormation, formation):
            pass
        return BaseSubformation

    class Core(Detachment):
        def __init__(self, parent):
            super(Fist.Core, self).__init__(
                parent, 'core', 'Core', [
                    Fist.make_fist(f) for f in [
                        StormlanceDemiCpmpany,
                        ArmoredTaskForce,
                        DemiCompany
                    ]
                ], 1, 2)

    class CommandSection(Detachment):
        def __init__(self, parent):
            super(Fist.CommandSection, self).__init__(
                parent, 'command', 'Command', [
                    Fist.make_fist(f) for f in [
                        IronHandStrikeForceCommand,
                        ReclusiamCommand,
                        LibrariusConclave
                    ]
                ], 0, 3)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(Fist.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    Fist.make_fist(f) for f in [
                        FirstComp,
                        TenthCompany,
                        Stormwing,
                        AntiAir,
                        Suppression,
                        Siegebreaker,
                        RaiderSpearhead,
                        Ultra,
                        OrbitalSkyhammer,
                        Skyhammer,
                        RaptorWing,
                        HonouredAncients,
                        IronGuardians
                    ]
                ],
                1, None)

    def __init__(self):
        self.core = self.Core(self)
        command = self.CommandSection(self)
        aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([command, self.core, aux], None)

    def is_hands(self):
        return True

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        chaplist = []
        for section in self.sections:
            for unit in section.units:
                chaplist.extend(unit.sub_roster.roster.get_chapter())
        if len(set(chaplist)) > 1:
            self.error('Cannot include units from more then one chapter')


class Sternhammer(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'space_marines_v3_sternhammer'
    army_name = 'Sternhammer Strike Force'

    class MarineSubFormation(SpaceMarineFormation):
        def __init__(self, *args, **kwargs):
            super(Sternhammer.MarineSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_base_codex(self):
            return True

        def is_ultra(self):
            return False

        def is_fists(self):
            return True

        def is_hands(self):
            return False

        def is_scars(self):
            return False

        def is_ravens(self):
            return False

        def is_karchodons(self):
            return False

        def is_hawks(self):
            return False

        def is_scorpions(self):
            return False

        def is_minotaurs(self):
            return False

        def is_salamanders(self):
            return False

        def is_templars(self):
            return False

    @staticmethod
    def make_sternhammer(formation):
        class BaseSubformation(Sternhammer.MarineSubFormation, formation):
            pass
        return BaseSubformation

    class Core(Detachment):
        def __init__(self, parent):
            super(Sternhammer.Core, self).__init__(
                parent, 'core', 'Core', [
                    Sternhammer.make_sternhammer(f) for f in [
                        DemiCompany,
                        Siegebreaker
                    ]
                ], 1, None)

    class StrikeForceCommand(MarineSubFormation):
        army_id = 'space_marines_v3_fists_strike_force_command'
        army_name = 'Strike Force Command'

        def __init__(self):
            super(Sternhammer.StrikeForceCommand, self).__init__()
            self.hq = [
                UnitType(self, Lysander),
                UnitType(self, Captain),
                UnitType(self, TerminatorCaptain),
                UnitType(self, Chaplain)
            ]
            self.add_type_restriction(self.hq, 1, 1)
            UnitType(self, HonourGuardSquad, max_limit=1)
            UnitType(self, CommandSquad, max_limit=1)

    class CommandSection(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(Sternhammer.CommandSection, self).__init__(
                parent, 'command', 'Command', [
                    Sternhammer.make_sternhammer(f) for f in [
                        ReclusiamCommand,
                        LibrariusConclave
                    ]
                ] + [Sternhammer.StrikeForceCommand], 0, 1)

        def check_limits(self):
            self.min = 0
            self.max = max([0, len(self.core.units)]) * 1
            return super(Sternhammer.CommandSection, self).check_limits()

    class Devastators(MarineSubFormation):
        army_id = 'space_marines_v3_fists_devastators'
        army_name = 'Devastators'

        def __init__(self):
            super(Sternhammer.Devastators, self).__init__()
            UnitType(self, KraatosDevastators, min_limit=1, max_limit=1)

    class Centurions(MarineSubFormation):
        army_id = 'space_marines_v3_fists_centurions'
        army_name = 'Centurions'

        def __init__(self):
            super(Sternhammer.Centurions, self).__init__()
            self.cents = [
                UnitType(self, CenturionAssault),
                UnitType(self, CenturionDevastators)
            ]
            self.add_type_restriction(self.cents, 1, 1)

    class LineBreakers(MarineSubFormation):
        army_id = 'space_marines_v3_fists_line_breakers'
        army_name = 'Line Breakers'

        def __init__(self):
            super(Sternhammer.LineBreakers, self).__init__()
            UnitType(self, Vindicators, min_limit=1, max_limit=1)

    class Ordance(MarineSubFormation):
        army_id = 'space_marines_v3_fists_ordance'
        army_name = 'Ordance'

        def __init__(self):
            super(Sternhammer.Ordance, self).__init__()
            UnitType(self, Thunderfire, min_limit=1, max_limit=1)

    class SiegeAncients(MarineSubFormation):
        army_id = 'space_marines_v3_fists_siege_ancients'
        army_name = 'Siege Ancients'

        def __init__(self):
            super(Sternhammer.SiegeAncients, self).__init__()
            self.dreds = [
                UnitType(self, IronDreds),
                UnitType(self, ContemptorDreadnoughts)
            ]
            self.add_type_restriction(self.dreds, 1, 1)

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(Sternhammer.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    Sternhammer.make_sternhammer(f) for f in [
                        ArmoredTaskForce,
                        FirstComp,
                        TenthCompany,
                        Stormwing,
                        AntiAir,
                        Suppression,
                        RaiderSpearhead,
                        Ultra,
                        OrbitalSkyhammer,
                        Skyhammer,
                        RaptorWing
                    ]
                ] + [
                    Sternhammer.Devastators,
                    Sternhammer.Centurions,
                    Sternhammer.LineBreakers,
                    Sternhammer.Ordance,
                    Sternhammer.SiegeAncients
                ],
                1, 10)

        def check_limits(self):
            self.min = 0
            self.max = max([0, len(self.core.units)]) * 10
            return super(Sternhammer.Auxilary, self).check_limits()

    def __init__(self):
        self.core = self.Core(self)
        self.command = self.CommandSection(self, self.core)
        self.aux = self.Auxilary(self, self.core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([self.command, self.core, self.aux], None)

    def is_fists(self):
        return True

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        chaplist = []
        for section in self.sections:
            for unit in section.units:
                chaplist.extend(unit.sub_roster.roster.get_chapter())
        if len(set(chaplist)) > 1:
            self.error('Cannot include units from more then one chapter')


class Flameblade(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'space_marines_v3_flameblade'
    army_name = 'Flameblade Strike Force'

    class MarineSubFormation(SpaceMarineFormation):
        def __init__(self, *args, **kwargs):
            super(Flameblade.MarineSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_base_codex(self):
            return False

        def is_ultra(self):
            return False

        def is_fists(self):
            return False

        def is_hands(self):
            return False

        def is_scars(self):
            return False

        def is_ravens(self):
            return False

        def is_karchodons(self):
            return False

        def is_hawks(self):
            return False

        def is_scorpions(self):
            return False

        def is_minotaurs(self):
            return False

        def is_salamanders(self):
            return True

        def is_templars(self):
            return False

    @staticmethod
    def make_flameblade(formation):
        class BaseSubformation(Flameblade.MarineSubFormation, formation):
            pass
        return BaseSubformation

    class Core(Detachment):
        def __init__(self, parent):
            super(Flameblade.Core, self).__init__(
                parent, 'core', 'Core', [
                    Flameblade.make_flameblade(f) for f in [
                        DemiCompany,
                        StormlanceDemiCpmpany
                    ]
                ], 1, 2)

    class StrikeForceCommand(MarineSubFormation):
        army_id = 'space_marines_v3_salamanders_strike_force_command'
        army_name = 'Strike Force Command'

        def __init__(self):
            super(Flameblade.StrikeForceCommand, self).__init__()
            self.hq = [
                UnitType(self, Vulkan),
                UnitType(self, Captain),
                UnitType(self, TerminatorCaptain),
                UnitType(self, Chaplain)
            ]
            self.add_type_restriction(self.hq, 1, 1)
            UnitType(self, HonourGuardSquad, max_limit=1)
            UnitType(self, CommandSquad, max_limit=1)

    class CommandSection(Detachment):
        def __init__(self, parent):
            super(Flameblade.CommandSection, self).__init__(
                parent, 'command', 'Command', [
                    Flameblade.make_flameblade(f) for f in [
                        ReclusiamCommand,
                        LibrariusConclave
                    ]
                ] + [Flameblade.StrikeForceCommand], 0, 3)

    class Flamebringers(MarineSubFormation):
        army_id = 'space_marines_v3_salamanders_flamebringers'
        army_name = 'Flamebringers'

        def __init__(self):
            super(Flameblade.Flamebringers, self).__init__()
            UnitType(self, LandRaiderRedeemer, min_limit=1, max_limit=1)

    class Auxilary(Detachment):
        def __init__(self, parent):
            super(Flameblade.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    Flameblade.make_flameblade(f) for f in [
                        ArmoredTaskForce,
                        FirstComp,
                        TenthCompany,
                        Stormwing,
                        AntiAir,
                        Suppression,
                        RaiderSpearhead,
                        Ultra,
                        OrbitalSkyhammer,
                        Skyhammer,
                        RaptorWing,
                        Stormbringer
                    ]
                ] + [
                    Flameblade.Flamebringers,
                ],
                1, None)

    def __init__(self):
        self.core = self.Core(self)
        self.command = self.CommandSection(self)
        self.aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([self.command, self.core, self.aux], None)

    def is_salamanders(self):
        return True

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        chaplist = []
        for section in self.sections:
            for unit in section.units:
                chaplist.extend(unit.sub_roster.roster.get_chapter())
        if len(set(chaplist)) > 1:
            self.error('Cannot include units from more then one chapter')


class VictrixStrikeForce(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'space_marines_v3_vitrix_strike'
    army_name = 'Victrix Strike Force'

    class MarineSubFormation(SpaceMarineFormation):
        def __init__(self, *args, **kwargs):
            super(VictrixStrikeForce.MarineSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_base_codex(self):
            return False

        def is_ultra(self):
            return True

        def is_fists(self):
            return False

        def is_hands(self):
            return False

        def is_scars(self):
            return False

        def is_ravens(self):
            return False

        def is_karchodons(self):
            return False

        def is_hawks(self):
            return False

        def is_scorpions(self):
            return False

        def is_minotaurs(self):
            return False

        def is_salamanders(self):
            return False

        def is_templars(self):
            return False

    @staticmethod
    def make_victrix(formation):
        class BaseSubformation(VictrixStrikeForce.MarineSubFormation, formation):
            pass
        return BaseSubformation

    class Core(Detachment):
        def __init__(self, parent):
            super(VictrixStrikeForce.Core, self).__init__(
                parent, 'core', 'Core', [
                    VictrixStrikeForce.make_victrix(f) for f in [
                        DemiCompany,
                        Ultra
                    ]
                ], 1, None)

    class StrikeForceCommand(MarineSubFormation):
        army_id = 'space_marines_v3_victrix_strike_force_command'
        army_name = 'Strike Force Command'

        def __init__(self):
            super(VictrixStrikeForce.StrikeForceCommand, self).__init__()
            self.hq = [
                UnitType(self, Guilliman),
                UnitType(self, Calgar),
                UnitType(self, Sicarius),
                UnitType(self, Captain),
                UnitType(self, TerminatorCaptain),
                UnitType(self, Cassius),
                UnitType(self, Chaplain)
            ]
            self.add_type_restriction(self.hq, 1, 1)
            UnitType(self, HonourGuardSquad, max_limit=1)
            UnitType(self, CommandSquad, max_limit=1)

    class CommandSection(Detachment):
        def __init__(self, parent):
            super(VictrixStrikeForce.CommandSection, self).__init__(
                parent, 'command', 'Command', [
                    VictrixStrikeForce.make_victrix(f) for f in [
                        ReclusiamCommand,
                        LibrariusConclave
                    ]
                ] + [VictrixStrikeForce.StrikeForceCommand], 0, 3)

    class Auxilary(Detachment):
        def __init__(self, parent, core):
            self.core = core
            super(VictrixStrikeForce.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    VictrixStrikeForce.make_victrix(f) for f in [
                        VictrixGuard,
                        ArmoredTaskForce,
                        FirstComp,
                        AntiAir,
                        TenthCompany,
                        Stormwing,
                        Suppression,
                        RaiderSpearhead,
                        Siegebreaker
                    ]
                ],
                1, 10)

        def check_limits(self):
            self.min = max([1, len(self.core.units)])
            self.max = max([1, len(self.core.units)]) * 10
            return super(VictrixStrikeForce.Auxilary, self).check_limits()

    def __init__(self):
        self.core = self.Core(self)
        self.command = self.CommandSection(self)
        self.aux = self.Auxilary(self, self.core)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([self.command, self.core, self.aux], None)

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        chaplist = []
        for section in self.sections:
            for unit in section.units:
                chaplist.extend(unit.sub_roster.roster.get_chapter())
        if len(set(chaplist)) > 1:
            self.error('Cannot include units from more then one chapter')


class Anvil(Wh40k7ed, volume2.SpaceMarinesLegaciesRoster):
    army_id = 'space_marines_v3_anvil'
    army_name = 'Anvil strike force'

    class MarineSubFormation(SpaceMarineFormation):
        def __init__(self, *args, **kwargs):
            super(Anvil.MarineSubFormation, self).__init__(*args, **kwargs)
            self.codex.used = self.codex.visible = False

        def is_base_codex(self):
            return self.parent.roster.is_base_codex()

        def is_ultra(self):
            return self.parent.roster.is_ultra()

        def is_fists(self):
            return self.parent.roster.is_fists()

        def is_hands(self):
            return self.parent.roster.is_hands()

        def is_scars(self):
            return self.parent.roster.is_scars()

        def is_ravens(self):
            return self.parent.roster.is_ravens()

        def is_scorpions(self):
            return self.parent.roster.is_scorpions()

        def is_karchodons(self):
            return self.parent.roster.is_karchodons()

        def is_hawks(self):
            return self.parent.roster.is_hawks()

        def is_minotaurs(self):
            return self.parent.roster.is_minotaurs()

        def is_salamanders(self):
            return self.parent.roster.is_salamanders()

        def is_templars(self):
            return self.parent.roster.is_templars()

    @staticmethod
    def make_anvil(formation):
        class BaseSubformation(Anvil.MarineSubFormation, formation):
            pass
        return BaseSubformation

    class Core(Detachment):
        def __init__(self, parent):
            class ChronusSpearhead(RaiderSpearhead):
                def __init__(self):
                    super(ChronusSpearhead, self).__init__()
                    UnitType(self, Chronus, max_limit=1)

            super(Anvil.Core, self).__init__(
                parent, 'core', 'Core', [
                    Anvil.make_anvil(f) for f in [
                        ArmoredTaskForce,
                        ChronusSpearhead
                    ]
                ], 1, 2)

    class Masters(MarineSubFormation):
        army_id = 'space_marines_v3_anvil_masters'
        army_name = 'Masters of the Armory'

        def __init__(self):
            super(Anvil.Masters, self).__init__()
            self.masters = [
                UnitType(self, Chronus),
                UnitType(self, SpaceMarineCommandTanks),
                UnitType(self, Predator),
                UnitType(self, Vindicator),
                UnitType(self, LandRaider),
                UnitType(self, LandRaiderRedeemer),
                UnitType(self, LandRaiderCrusader)
            ]
            self.add_type_restriction(self.masters, 1, 1)

    class Keepers(MarineSubFormation):
        army_id = 'space_marines_v3_anvil_keepers'
        army_name = 'Keepers of the Forge'

        def __init__(self):
            super(Anvil.Keepers, self).__init__()
            UnitType(self, Techmarine, min_limit=1, max_limit=1)
            self.rh = [
                UnitType(self, Rhino),
                UnitType(self, Razorback)
            ]
            self.add_type_restriction(self.rh, 1, 1)

    class CommandSection(Detachment):
        def __init__(self, parent):
            super(Anvil.CommandSection, self).__init__(
                parent, 'command', 'Command', [Anvil.Masters, Anvil.Keepers], 0, 2)

    class Infantry(MarineSubFormation):
        army_id = 'space_marines_v3_anvil_infantry'
        army_name = 'Mechanised Infantry'

        def __init__(self):
            super(Anvil.Infantry, self).__init__()
            self.infantry = [
                UnitType(self, TacticalSquad),
                UnitType(self, AssaultSquad),
                UnitType(self, KraatosDevastators),
                UnitType(self, ScoutSquad),
                UnitType(self, TerminatorSquad),
                UnitType(self, TerminatorAssaultSquad),
                UnitType(self, CataphractiiTerminatorSquad),
                UnitType(self, SternguardVeteranSquad),
                UnitType(self, VanguardVeteranSquad),
                UnitType(self, CenturionDevastators),
                UnitType(self, CenturionAssault),
            ]
            self.add_type_restriction(self.infantry, 1, 1)

        def need_transport(self):
            units = []
            for u in self.infantry:
                units.extend(u.units)
            if not units:
                return False
            return not units[0].has_transport()

        # def check_rules(self):
        #     super(Anvil.Infantry, self).check_rules()
        #     units = []
        #     for u in self.infantry:
        #         units.extend(u.units)

        #     for unit in units:
        #         if not unit.has_transport():
        #             self.error("You must include Transport for each unit of Infantry")
        #             break

    class Techmarine(MarineSubFormation):
        army_id = 'space_marines_v3_anvil_techmarine'
        army_name = 'Techmarine'

        def __init__(self):
            super(Anvil.Techmarine, self).__init__()
            self.tech = [
                UnitType(self, Techmarine),
                UnitType(self, Thunderfire),
            ]
            self.add_type_restriction(self.tech, 1, 1)

    class Recon(MarineSubFormation):
        army_id = 'space_marines_v3_anvil_recon'
        army_name = 'Recon Outriders'

        def __init__(self):
            super(Anvil.Recon, self).__init__()
            UnitType(self, LandSpeederSquadron, min_limit=1, max_limit=1)

    class Honoured(MarineSubFormation):
        army_id = 'space_marines_v3_anvil_honoured'
        army_name = 'Honoured Ancients'

        def __init__(self):
            super(Anvil.Honoured, self).__init__()
            self.dreds = [
                UnitType(self, Dreadnoughts),
                UnitType(self, IronDreds),
                UnitType(self, VenDreadnoughts),
                UnitType(self, ContemptorDreadnoughts),
            ]
            self.add_type_restriction(self.dreds, 1, 1)

    class Auxilary(Detachment):
        def __init__(self, parent):
            class ChronusSuppression(Suppression):
                def __init__(self):
                    super(ChronusSuppression, self).__init__()
                    UnitType(self, Chronus, max_limit=1)

            class ChronusAntiAir(AntiAir):
                def __init__(self):
                    super(ChronusAntiAir, self).__init__()
                    UnitType(self, Chronus, max_limit=1)

            super(Anvil.Auxilary, self).__init__(
                parent, 'aux', 'Auxilary', [
                    Anvil.make_anvil(f) for f in [
                        ChronusSuppression,
                        ChronusAntiAir,
                        RaptorWing,
                        Stormwing,
                    ]
                ] + [
                    Anvil.Infantry,
                    Anvil.Techmarine,
                    Anvil.Recon,
                    Anvil.Honoured
                ],
                1, None)

        def count_foot_infantry(self):
            """
            Return number of infantry units without transport
            """
            infantry_units = [unit.sub_roster.roster for unit in self.units
                              if isinstance(unit.sub_roster.roster, Anvil.Infantry)]
            return sum(unit.need_transport() for unit in infantry_units)

    def __init__(self):
        self.core = self.Core(self)
        self.command = self.CommandSection(self)
        self.aux = self.Auxilary(self)
        # ok. We do not need our superconstructor, so we try to make that call to grandparent
        super(Wh40k7ed, self).__init__([self.command, self.core, self.aux], None)
        self.codex = SupplementOptions(parent=self)

    def is_base_codex(self):
        return self.codex.cur in [self.codex.base, self.codex.scars, self.codex.salamanders,
                                  self.codex.ravens, self.codex.karchodons,
                                  self.codex.hawks, self.codex.scorpions, self.codex.ultra]

    def is_fists(self):
        return self.codex.cur == self.codex.fists

    def is_hands(self):
        return self.codex.cur == self.codex.hands

    def is_scars(self):
        return self.codex.cur == self.codex.scars

    def is_ravens(self):
        return self.codex.cur == self.codex.ravens

    def is_scorpions(self):
        return self.codex.cur == self.codex.scorpions

    def is_karchodons(self):
        return self.codex.cur == self.codex.karchodons

    def is_hawks(self):
        return self.codex.cur == self.codex.hawks

    def is_minotaurs(self):
        return self.codex.cur == self.codex.minotaurs

    def is_salamanders(self):
        return self.codex.cur == self.codex.salamanders

    def is_templars(self):
        return self.codex.cur == self.codex.templars

    def is_ultra(self):
        return self.codex.cur == self.codex.ultra

    def check_rules(self):
        super(Wh40k7ed, self).check_rules()
        chaplist = []
        for section in self.sections:
            for unit in section.units:
                chaplist.extend(unit.sub_roster.roster.get_chapter())
        if len(set(chaplist)) > 1:
            self.error('Cannot include units from more then one chapter')
        # ensure there is a transport for every infantry unit
        foot_count = self.aux.count_foot_infantry()
        transport_count = sum(1 for unit in self.command.units
                              if isinstance(unit.sub_roster.roster, Anvil.Keepers)) +\
                          sum(3 for unit in self.core.units
                              if isinstance(unit.sub_roster.roster, RaiderSpearhead))
        if foot_count > transport_count:
            self.error("This Detachment must include a transport vehicle for each unit of Infantry")


class SpaceMarinesV3CAD(SpaceMarinesV3Base, CombinedArmsDetachment):
    army_id = 'space_marines_v3_cad'
    army_name = 'Space Marines (Combined arms detachment)'


class SpaceMarinesV3AD(SpaceMarinesV3Base, AlliedDetachment):
    army_id = 'space_marines_v3_ad'
    army_name = 'Space Marines (Allied detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'space_marines_v3_asd'
    army_name = 'Space Marines (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class SpaceMarinesV3KillTeam(Wh40kKillTeam):
    army_id = 'space_marines_v3_kt'
    army_name = 'Space Marines'
    ia_enabled = False

    def get_chapter(self):
        result = []
        for section in self.sections:
            for unit in section.units:
                result.extend(check_chapter(unit))
        return [chap for chap in result if chap is not None]

    def check_rules(self):
        super(SpaceMarinesV3KillTeam, self).check_rules()
        chaplist = list(set(self.get_chapter()))
        if len(chaplist) > 1:
            self.error('Cannot include units from more then one chapter')

    def is_base_codex(self):
        return self.codex.cur in [self.codex.base, self.codex.scars,
                                  self.codex.ravens, self.codex.karchodons,
                                  self.codex.hawks, self.codex.scorpions, self.codex.salamanders, self.codex.fists,
                                  self.codex.hands]

    def is_fists(self):
        return self.codex.cur == self.codex.fists

    def is_hands(self):
        return self.codex.cur == self.codex.hands

    def is_scars(self):
        return self.codex.cur == self.codex.scars

    def is_ravens(self):
        return self.codex.cur == self.codex.ravens

    def is_scorpions(self):
        return self.codex.cur == self.codex.scorpions

    def is_karchodons(self):
        return self.codex.cur == self.codex.karchodons

    def is_hawks(self):
        return self.codex.cur == self.codex.hawks

    def is_minotaurs(self):
        return self.codex.cur == self.codex.minotaurs

    def is_salamanders(self):
        return self.codex.cur == self.codex.salamanders

    def is_templars(self):
        return self.codex.cur == self.codex.templars

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(SpaceMarinesV3KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [TacticalSquad, CrusaderSquad],
                                                    lambda u: [u.transport.drop] +
                                                    ([u.transport.lrc] if u.transport.lrc else []))
            self.scout_squad = UnitType(self, ScoutSquad)

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(SpaceMarinesV3KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                CommandSquad, VanguardVeteranSquad, SternguardVeteranSquad
            ], lambda u: [u.transport.drop])
            self.damned = UnitType(self, TheDamned)
            UnitType(self, SquadDarvos)
            UnitType(self, SquadAmerex)

    class KTFast(FastSection):
        def __init__(self, parent):
            super(SpaceMarinesV3KillTeam.KTFast, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [
                Rhino, Razorback, LandSpeederSquadron
            ])
            Wh40kKillTeam.process_transported_units(self, [AssaultSquad],
                                                    lambda u: [u.transport.drop])
            self.bike_squad = UnitType(self, BikeSquad)
            self.attack_bike_squad = UnitType(self, AttackBikeSquad)
            self.scout_bike_squad = UnitType(self, ScoutBikers)

    def __init__(self):
        super(SpaceMarinesV3KillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self), self.KTFast(self)
        )
        self.codex = SupplementOptions(self)


faction = 'Space Marines'


class SpaceMarinesV3(Wh40kImperial, Wh40k7ed):
    army_id = 'space_marines_v3'
    army_name = 'Space Marines'
    faction = faction
    # development = True

    def __init__(self):
        super(SpaceMarinesV3, self).__init__([SpaceMarinesV3CAD,
                                              FlierDetachment,
                                              Gladius,
                                              Scarblade,
                                              Talon,
                                              Fist,
                                              Sternhammer,
                                              Flameblade,
                                              VictrixStrikeForce,
                                              Anvil,
                                              DemiCompany,
                                              AntiAir,
                                              FirstComp,
                                              Ultra,
                                              ReclusiamCommand,
                                              TenthCompany,
                                              Stormwing,
                                              Siegebreaker,
                                              RaiderSpearhead,
                                              LibrariusConclave,
                                              ArmoredTaskForce,
                                              Suppression,
                                              SaintTylus,
                                              Skyhammer,
                                              StormlanceDemiCpmpany,
                                              Hunting,
                                              Stormbringer,
                                              Speartip,
                                              PinionDemiCpmpany,
                                              Shadowstrike,
                                              Bladewing,
                                              OrbitalSkyhammer,
                                              Ravenhawk,
                                              RaptorWing,
                                              ShadowForce,
                                              Firespear,
                                              ForceSolaq,
                                              Strikewing,
                                              VictrixGuard],
                                             [SpaceMarinesV3AD])

    def check_rules(self):
        super(SpaceMarinesV3, self).check_rules()
        # ensure there is no same-shapter allying going on
        # find space marine allied detachments
        if any([isinstance(m.sub_roster.roster, SpaceMarinesV3CAD)
               for m in self.primary.units]):
            ally_marines = [m.sub_roster.roster
                            for m in self.secondary.units
                            if isinstance(m.sub_roster.roster, SpaceMarinesV3AD)]
            ally_chapters = set(sum([ally.get_chapter()
                                 for ally in ally_marines], []))
            own_chapter = set(self.primary.units[0].sub_roster.roster.get_chapter())
            if len(own_chapter & ally_chapters):
                self.error("Roster cannot contain allied Space Marine detachments with chapter tactics of {}".format((own_chapter & ally_chapters).pop()))


detachments = [
    SpaceMarinesV3CAD,
    SpaceMarinesV3AD,
    FlierDetachment,
    Gladius,
    Scarblade,
    Talon,
    Fist,
    Sternhammer,
    Flameblade,
    VictrixStrikeForce,
    Anvil,
    DemiCompany,
    AntiAir,
    FirstComp,
    Ultra,
    ReclusiamCommand,
    TenthCompany,
    Stormwing,
    Siegebreaker,
    RaiderSpearhead,
    LibrariusConclave,
    ArmoredTaskForce,
    Suppression,
    SaintTylus,
    Skyhammer,
    StormlanceDemiCpmpany,
    Hunting,
    Stormbringer,
    Speartip,
    PinionDemiCpmpany,
    Shadowstrike,
    Bladewing,
    OrbitalSkyhammer,
    Ravenhawk,
    RaptorWing,
    ShadowForce,
    Firespear,
    ForceSolaq,
    Strikewing,
    VictrixGuard
]


unit_types = [
    Sicarius, Tigurius, Cassius, Telion, Chronus,
    Khan, Vulkan, Shrike, Lysander, Cantor, Helbrecht, Champion, Grimaldus,
    Captain, Librarian, Chaplain, Techmarine, Solaq, TerminatorCaptain,
    CommandSquad, HonourGuardSquad, CenturionAssault,
    VanguardVeteranSquad, SternguardVeteranSquad, Dreadnoughts,
    VenDreadnoughts, IronDreds, TheDamned, TerminatorSquad,
    TerminatorAssaultSquad, SquadAmerex, SquadDarvos, CataphractiiTerminatorSquad, ContemptorDreadnoughts,
    TacticalSquad, ScoutSquad, CrusaderSquad,
    LandSpeederStorm, AssaultSquad, ScoutBikers, BikeSquad,
    AttackBikeSquad, LandSpeederSquadron, Stormtalon, Darkwind, Stormhawks,
    Stormtalons,
    LandRaider, LandRaiderCrusader, LandRaiderRedeemer, Devastators,
    Thunderfire, CenturionDevastators, Predators, Stalkers, Hunters, Vindicators,
    Whirlwinds, Stormravens,
    Calgar, Guilliman,
    DropPod, Rhino, Razorback
]
