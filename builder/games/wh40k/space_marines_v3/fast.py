__author__ = 'Denis Romanov'
__maintainer__ = 'Ivan Truskov'

from .armory import *
from builder.games.wh40k.imperial_armour.volume2.options import SpaceMarinesBaseVehicle,\
    SpaceMarinesBaseSquadron
from builder.games.wh40k.imperial_armour.volume2.transport import IATransportedUnit
from .transport import Transport
from builder.core2 import *
from builder.games.wh40k.unit import Unit


class AssaultSquad(IATransportedUnit):
    type_name = 'Assault squad'
    type_id = 'assault_squad_v3'

    model_points = 14
    model_gear = Armour.power_armour_set

    class Sergeant(Unit):

        class Weapon1(PowerPistols, Melee, BoltPistol):
            pass

        class Weapon2(PowerPistols, Melee, Chainsword):
            def __init__(self, *args, **kwargs):
                super(AssaultSquad.Sergeant.Weapon2, self).__init__(*args, **kwargs)
                self.evs = self.variant('Eviscerator', 25)

        class Options(OptionsList):
            def __init__(self, parent):
                super(AssaultSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.combatshiled = self.variant('Combat shield', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        def __init__(self, parent):
            super(AssaultSquad.Sergeant, self).__init__(
                name='Space Marine Sergeant',
                parent=parent, points=70 - 4 * AssaultSquad.model_points,
                gear=AssaultSquad.model_gear
            )
            self.wep2 = self.Weapon2(self, 'Weapon')
            self.wep1 = self.Weapon1(self, '')
            self.opt = self.Options(self)

        def check_rules(self):
            super(AssaultSquad.Sergeant, self).check_rules()
            self.wep1.visible = self.wep1.used = not self.has_evs()

        def has_evs(self):
            return self.wep2.cur == self.wep2.evs

        def build_description(self):
            desc = super(AssaultSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Sergeant'
            if self.parent.parent.pack.used and self.parent.parent.pack.any:
                desc.add(Gear('Jump pack'))
                desc.add_points(3)
            return desc

    class Jumppack(OptionsList):
        def __init__(self, parent):
            super(AssaultSquad.Jumppack, self).__init__(parent=parent, name='')
            self.jumppack = self.variant('Jump Packs', 3, gear=[], per_model=True)
            self.jumppack.value = True

    def build_points(self):
        return super(AssaultSquad, self).build_points() + self.pack.points * self.marines.cur * self.pack.used

    def __init__(self, parent):
        super(AssaultSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9, points=self.model_points,
                             per_model=True)
        self.flame = Count(self, 'Flamer', 0, 2, 5)
        self.pp = Count(self, 'Plasma pistol', 0, 2, 15)
        self.evs = Count(self, 'Eviscerator', 0, 2, 25)
        self.pack = self.Jumppack(parent=self)
        self.transport = Transport(parent=self)

    def check_rules(self):
        super(AssaultSquad, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.pp])
        evs_max = (self.get_count() / 5) - self.sergeant.unit.has_evs()
        self.evs.used = self.evs.visible = evs_max > 0
        self.evs.max = evs_max
        self.pack.used = self.pack.visible = not self.transport.any

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=AssaultSquad.model_gear,
            points=AssaultSquad.model_points
        )
        if self.pack.any and self.pack.used:
            marine.add(Gear('Jump Pack'))
            marine.add_points(self.pack.points)
        if self.evs.cur:
            desc.add(marine.clone().
                     add(Gear('Eviscerator')).
                     add_points(25).set_count(self.evs.cur))
        marine.add(Gear('Chainsword'))
        count = self.marines.cur - self.evs.cur
        for o in [self.pp, self.flame]:
            if o.cur:
                desc.add_dup(marine.clone().add_points(o.option_points).add(Gear(o.name)).set_count(o.cur))
                count -= o.cur

        desc.add(marine.add(Gear('Bolt pistol')).set_count(count))
        desc.add(self.transport.description)
        return desc

    def get_count(self):
        return self.marines.cur + 1


class ScoutBikers(Unit):
    type_name = 'Scout Bike Squad'
    type_id = 'scout_bike_squad_v3'

    model_gear = Armour.scout_armour_set + [Gear('Space Marine bike'), Gear('Space Marine shotgun')]
    model_points = 18

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(ScoutBikers.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.locator = self.variant('Locator beacon', 10)
                self.veteran = self.variant('Veteran', 10, gear=[])

        class SergeantWeapon(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(ScoutBikers.Sergeant, self).__init__(
                name='Scout Biker Sergeant',
                parent=parent, points=54 - 2 * ScoutBikers.model_points,
                gear=ScoutBikers.model_gear + [Gear('Twin-linked boltgun')]
            )
            self.wep = self.SergeantWeapon(self, 'Weapon')
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(ScoutBikers.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Scout Biker Veteran Sergeant'
            return desc

    class Options(OptionsList):
        def __init__(self, parent):
            super(ScoutBikers.Options, self).__init__(parent=parent, name='Options')
            self.mines = self.variant('Cluster mines', 20)

    def __init__(self, parent):
        super(ScoutBikers, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))

        self.bikers = Count(self, 'Scout Biker', 2, 9, self.model_points, per_model=True, gear=[])
        self.grenade = Count(self, 'Astartes grenade launcher', 0, 3, 5, gear=[])
        self.opt = self.Options(self)

    def get_count(self):
        return self.bikers.cur + 1

    def check_rules(self):
        super(ScoutBikers, self).check_rules()
        self.grenade.max = min(self.bikers.cur, 3)

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Scout Biker',
            options=ScoutBikers.model_gear + [Gear('Bolt pistol')],
            points=ScoutBikers.model_points
        )
        count = self.bikers.cur
        if self.grenade.cur:
            desc.add(marine.clone()
                .add_points(self.grenade.option_points)
                .add(Gear(self.grenade.name))
                .set_count(self.grenade.cur))
            count -= self.grenade.cur
        if count:
            desc.add(marine.clone()
                .add(Gear('Twin-linked boltgun'))
                .set_count(count))
        desc.add(self.opt.description)
        return desc


class BikeSquad(Unit):
    type_name = 'Bike Squad'
    type_id = 'bike_squad_v3'

    model_gear = Armour.power_armour_set + [Gear('Space Marine bike'), Gear('Twin-linked boltgun')]
    model_points = 21

    class Sergeant(Unit):
        class Options(OptionsList):
            def __init__(self, parent):
                super(BikeSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.meltabombs = self.variant('Melta bombs', 5)
                self.veteran = self.variant('Veteran', 10, gear=[])

        class SergeantWeapon(Ranged, Melee, BoltPistol):
            pass

        def __init__(self, parent):
            super(BikeSquad.Sergeant, self).__init__(
                name='Biker Sergeant',
                parent=parent, points=63 - 2 * BikeSquad.model_points,
                gear=BikeSquad.model_gear
            )
            self.wep = self.SergeantWeapon(self, 'Weapon')
            self.opt = self.Options(self)

        def build_description(self):
            desc = super(BikeSquad.Sergeant, self).build_description()
            if self.opt.veteran.value:
                desc.name = 'Veteran Biker Sergeant'
            return desc

    class AttackBike(Unit):
        model_name = 'Attack Bike'
        model_points = 40

        class Weapon(OneOf):
            def __init__(self, parent):
                super(BikeSquad.AttackBike.Weapon, self).__init__(parent=parent, name='Weapon')
                self.heavybolter = self.variant('Heavy bolter', 0)
                self.multimelta = self.variant('Multi-melta', 10)

        def __init__(self, parent):
            super(BikeSquad.AttackBike, self).__init__(
                parent=parent, name=self.model_name, points=self.model_points,
                gear=BikeSquad.model_gear + [Gear('Bolt pistol')])
            self.wep = self.Weapon(self)

    class OptUnits(OptionalSubUnit):
        def __init__(self, parent):
            super(BikeSquad.OptUnits, self).__init__(parent=parent, name='')
            self.attackbike = SubUnit(self, BikeSquad.AttackBike(parent=None))

    def __init__(self, parent, troops=False):
        super(BikeSquad, self).__init__(parent=parent)
        self.troops = troops
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(None))
        self.opt_units = self.OptUnits(self)

        self.bikers = Count(self, 'Space Marine Biker', 4 if self.troops else 2, 7, self.model_points, per_model=True,
                            gear=[])
        self.flame = Count(self, 'Flamer', 0, 2, 5, gear=[])
        self.melta = Count(self, 'Meltagun', 0, 2, 10, gear=[])
        self.grav = Count(self, 'Grav-gun', 0, 2, 15, gear=[])
        self.plasma = Count(self, 'Plasma gun', 0, 2, 15, gear=[])

    def check_rules(self):
        super(BikeSquad, self).check_rules()
        Count.norm_counts(0, 2, [self.flame, self.melta, self.grav, self.plasma])
        if self.troops:
            self.bikers.min = 4 - self.opt_units.count

    def get_count(self):
        return self.opt_units.count + self.bikers.cur + 1

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine Biker',
            options=BikeSquad.model_gear + [Gear('Bolt pistol')],
            points=BikeSquad.model_points
        )
        count = self.bikers.cur
        for o in [self.flame, self.melta, self.grav, self.plasma]:
            if o.cur:
                desc.add_dup(marine.clone().add_points(o.option_points).add(Gear(o.name)).set_count(o.cur))
                count -= o.cur
        desc.add(marine.set_count(count))
        desc.add(self.opt_units.description)
        return desc


class TroopsBikeSquad(BikeSquad):
    def __init__(self, parent):
        super(TroopsBikeSquad, self).__init__(parent=parent, troops=True)


class AttackBikeSquad(Unit):
    type_name = 'Attack Bike Squad'
    type_id = 'attackbike_v3'

    model_points = 40
    model_gear = Armour.power_armour_set + [Gear('Bolt pistol'), Gear('Space Marine bike'), Gear('Twin-linked boltgun')]
    model_name = 'Attack Bike'

    def __init__(self, parent):
        super(AttackBikeSquad, self).__init__(parent)
        self.bikers = Count(self, self.model_name, 1, 3, self.model_points, per_model=True)
        self.melta = Count(self, 'Multi-melta', 0, 1, 10, per_model=True)

    def check_rules(self):
        super(AttackBikeSquad, self).check_rules()
        self.melta.max = self.bikers.cur

    def get_count(self):
        return self.bikers.cur

    def build_description(self):
        desc = UnitDescription(name=self.type_name, points=self.points, count=self.get_count())
        marine = UnitDescription(name=self.model_name, options=self.model_gear, points=self.model_points)
        count = self.bikers.cur
        if self.melta.cur:
            desc.add(marine.clone()
                .add_points(self.melta.option_points)
                .add(Gear(self.melta.name))
                .set_count(self.melta.cur))
            count -= self.melta.cur
        if count:
            desc.add(marine.clone()
                .add(Gear('Heavy bolter'))
                .set_count(count))
        return desc


class LandSpeeder(SpaceMarinesBaseVehicle):
    class BaseWeapon(OneOf):
        def __init__(self, parent):
            super(LandSpeeder.BaseWeapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy Bolter', 0)
            self.heavyflamer = self.variant('Heavy Flamer', 0)
            self.multimelta = self.variant('Multi-melta', 10)

    class UpWeapon(OptionsList):
        def __init__(self, parent):
            super(LandSpeeder.UpWeapon, self).__init__(parent=parent, name='Upgrade', limit=1)
            self.heavybolter = self.variant('Heavy Bolter', 5)
            self.heavyflamer = self.variant('Heavy Flamer', 5)
            self.multimelta = self.variant('Multi-melta', 15)
            self.assaultcannon = self.variant('Assault Cannon', 20)
            self.typhoonmissilelauncher = self.variant('Typhoon Missile Launcher', 25)

    def __init__(self, parent):
        super(LandSpeeder, self).__init__(parent=parent, points=45, name="Land Speeder")
        self.wep = self.BaseWeapon(self)
        self.up = self.UpWeapon(self)


class LandSpeederSquadron(SpaceMarinesBaseSquadron):
    type_name = "Land Speeders"
    type_id = "land_speeder_squadron_v3"

    unit_class = LandSpeeder


class Darkwind(Unit):
    type_name = 'Land Speeder Darkwind'
    type_id = 'darkwind_v3'

    def __init__(self, parent):
        super(Darkwind, self).__init__(parent, points=75, static=True, unique=True, gear=[
                                       Gear('Assault cannon'), Gear('Heavy bolter')])


class Stormhawk(SpaceMarinesBaseVehicle):
    type_name = 'Stormhawk Interceptor'
    type_id = 'stormhawk_v3'

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Stormhawk.Weapon1, self).__init__(parent, 'Weapon')
            self.variant('Las-talon')
            self.variant('Icarus stormcannon')

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Stormhawk.Weapon2, self).__init__(parent, '')
            self.variant('Twin-linked heavy bolter')
            self.variant('Skyhammer missile launcher', 5)
            self.variant('Typhoon missile launcher', 20)

    def __init__(self, parent):
        super(Stormhawk, self).__init__(parent, points=125, gear=[
            Gear('Twin-linked assault cannon'),
            Gear('Ceramite plating'),
            Gear('Infernum halo-launcher')
        ])
        self.Weapon1(self)
        self.Weapon2(self)


class Stormhawks(SpaceMarinesBaseSquadron):
    type_name = 'Stormhawk Interceptors'
    type_id = 'stormhawk_wing_v3'
    unit_class = Stormhawk
    unit_max = 4


class Stormtalon(SpaceMarinesBaseVehicle):
    type_name = 'Stormtalon Gunship'
    type_id = 'stormtalongunship_v3'

    def __init__(self, parent):
        super(Stormtalon, self).__init__(parent=parent, points=110,
                                         gear=[Gear('Ceramite plating'), Gear('Twin-linked assault cannon')])
        self.weapon = self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Stormtalon.Weapon, self).__init__(parent=parent, name='Weapon')

            self.twinlinkedheavybolter = self.variant('Twin-linked heavy bolter', 0)
            self.skyhammermissilelauncher = self.variant('Skyhammer missile launcher', 5)
            self.twinlinkedlascannon = self.variant('Twin-linked lascannon', 15)
            self.typhoonmissilelauncher = self.variant('Typhoon missile launcher', 20)


class Stormtalons(SpaceMarinesBaseSquadron):
    type_name = 'Stormtalon gunships'
    type_id = 'stormtalon_wing_v3'
    unit_class = Stormtalon
    unit_max = 4


class LandSpeederStorm(SpaceMarinesBaseVehicle):
    type_name = 'Land Speeder Storm'
    type_id = 'land_speeder_storm_v3'
    model_points = 40

    def __init__(self, parent):
        super(LandSpeederStorm, self).__init__(
            parent=parent, points=self.model_points, transport=True,
            gear=[Gear('Cerberus launcher'), Gear('Jamming beacon')]
        )
        self.weapon = self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LandSpeederStorm.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant('Heavy bolter', 0)
            self.heavyflamer = self.variant('Heavy flamer', 0)
            self.multimelta = self.variant('Multi-melta', 10)
            self.assaultcannon = self.variant('Assault cannon', 15)
