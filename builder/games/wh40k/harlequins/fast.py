from builder.core2 import Gear, ListSubUnit, UnitList, OneOf
from builder.games.wh40k.roster import Unit
__author__ = 'Ivan Truskov'


class Starweaver(Unit):
    type_id = 'starweaver_v1'
    type_name = 'Starweaver'

    def __init__(self, parent):
        super(Starweaver, self).__init__(parent, self.type_name, 70,
                                         [Gear('Shuriken cannon', count=2),
                                          Gear('Holo-fields'),
                                          Gear('Mirage launchers')])


class Skyweavers(Unit):
    type_name = 'Skyweaver'
    type_id = 'skyweaver_v1'

    class Skyweaver(ListSubUnit):
        class Hand(OneOf):
            def __init__(self, parent):
                super(Skyweavers.Skyweaver.Hand, self).__init__(parent,
                                                                'Hand weapon')
                self.variant('Star bolas', 0)
                self.variant('Zephyrglaive', 10)

        class Bike(OneOf):
            def __init__(self, parent):
                super(Skyweavers.Skyweaver.Bike, self).__init__(parent,
                                                                'Bike weapon')
                self.variant('Shuriken cannon', 0)
                self.variant('Haywire cannon', 5)

        def __init__(self, parent):
            super(Skyweavers.Skyweaver, self).__init__(parent, 'Skyweaver', 50,
                                                       gear=[
                                                           Gear('Holo-suit'),
                                                           Gear('Mirage launchers'),
                                                           Gear('Skyweaver jetbike')
                                                       ])
            self.Hand(self)
            self.Bike(self)

    def __init__(self, parent):
        super(Skyweavers, self).__init__(parent, self.type_name)
        self.models = UnitList(self, self.Skyweaver, 2, 6)

    def get_count(self):
        return self.models.count
