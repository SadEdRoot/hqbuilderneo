from builder.core2 import Gear, OneOf
from builder.games.wh40k.roster import Unit
from .armory import YnnariEnigmas, Haywire
from builder.games.wh40k.ynnari.armory import YnnariBlade, YnnariPistol


class DeathJester(Unit):
    type_name = 'Death Jester'
    type_id = 'death_jester_v1'

    def __init__(self, parent):
        super(DeathJester, self).__init__(parent, self.type_name, 60,
                                          gear=[
                                              Gear('Holo-suit'),
                                              Gear('Shrieker cannon'),
                                              Gear('Flip belt')])
        Haywire(self)
        self.enigmas = YnnariEnigmas(self)

    def get_unique_gear(self):
        return self.enigmas.get_unique()


class Shadowseer(Unit):
    type_name = 'Shadowseer'
    type_id = 'seer_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Shadowseer.Weapon, self).__init__(parent, 'Ranged')
            self.variant('Shuriken pistol', 0)
            self.variant('Neuro distruptor', 10)
            self.crescendo = self.variant('Crescendo', 5)
            self.relic_options = [self.crescendo]

        def get_unique(self):
            if self.cur in self.relic_options:
                return self.description
            return []

    class ArtefactWeapon(YnnariPistol, Weapon):
        def __init__(self, parent):
            super(Shadowseer.ArtefactWeapon, self).__init__(parent)
            self.relic_options.append(self.ysong)

    class Mastery(OneOf):
        def __init__(self, parent):
            super(Shadowseer.Mastery, self).__init__(parent, 'Mastery level')
            self.lvl1 = self.variant('Mastery Level 1', 0)
            self.lvl2 = self.variant('Mastery Level 2', 25)

        def count_charges(self):
            if self.cur == self.lvl1:
                return 1
            else:
                return 2

    def __init__(self, parent):
        super(Shadowseer, self).__init__(parent, self.type_name, 60,
                                         gear=[
                                             Gear('Holo-suit'),
                                             Gear('Hallucinogen grenade launcher'),
                                             Gear('Miststave'),
                                             Gear('Flip belt')
                                         ])
        self.psy = self.Mastery(self)
        self.rng = self.ArtefactWeapon(self)
        Haywire(self)
        self.enigmas = YnnariEnigmas(self, seer=True)

    def check_rules(self):
        super(Shadowseer, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Can carry only one Enigma of the Black Library')

    def get_unique_gear(self):
        return self.rng.get_unique() + self.enigmas.get_unique()

    def count_charges(self):
        return self.psy.count_charges()

    def build_statistics(self):
        return self.note_charges(super(Shadowseer, self).build_statistics())


class Solitare(Unit):
    type_name = 'Solitaire'
    type_id = 'solitaire_v1'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Solitare.Weapon, self).__init__(parent, 'Melee')
            self.variant('Harlequin\'s kiss', 0)
            self.rose = self.variant('Cegorach\'s rose', 15)
            self.relic_options = [self.rose]

        def get_unique(self):
            if self.cur in self.relic_options:
                return self.description
            return []

    class RelicWeapon(YnnariBlade, Weapon):
        def __init__(self, person):
            super(Solitare.RelicWeapon, self).__init__(person)
            self.relic_options.append(self.hblade)

    def __init__(self, parent):
        super(Solitare, self).__init__(parent, self.type_name, 145,
                                       gear=[
                                           Gear('Holo-suit'),
                                           Gear('Harlequin\'s caress'),
                                           Gear('Flop belt')
                                       ])
        self.mle = self.RelicWeapon(self)
        Haywire(self)
        self.enigmas = YnnariEnigmas(self)

    def check_rules(self):
        super(Solitare, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Can carry only one Enigma of the Black Library')

    def get_unique_gear(self):
        return self.mle.get_unique() + self.enigmas.get_unique()
