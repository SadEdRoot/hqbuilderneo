__author__ = 'Ivan Truskov'

from builder.core2 import Gear, OptionalSubUnit,\
    SubUnit, Count, OptionsList, OneOf, UnitDescription
from .fast import Raider, Venom
from .armory import Melee, Arcane, Artefacts, NoWeapon,\
    WychWeapon, Torment, Torture, Pistol, YnnariArtefactBlade,\
    YnnariArtefactPistol, YnnariCruelArtefacts, ArtefactBlade,\
    ArtefactPistol

from builder.games.wh40k.roster import Unit


class Archon(Unit):
    type_id = 'archon'
    type_name = 'Archon'

    class WeaponMelee(Melee):
        def __init__(self, parent):
            super(Archon.WeaponMelee, self).__init__(parent,
                                                     extra=('Huskblade', 25))

    class ArtefactMelee(YnnariArtefactBlade, WeaponMelee):
        pass

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(Archon.WeaponRanged, self).\
                __init__(parent, name='Ranged Weapom')
            self.variant('Splinter pistol', 0)
            self.variant('Blast pistol', 15)
            self.variant('Blaster', 15)

    class ArtefactRanged(YnnariArtefactPistol, WeaponRanged):
        pass

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Archon.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Venom(parent=None))

    def __init__(self, parent):
        super(Archon, self).\
            __init__(parent, 'Archon', 60,
                     gear=[Gear('Plasma grenades'), Gear('Kabalite armour')])
        self.mle = self.ArtefactMelee(self)
        self.rng = self.ArtefactRanged(self)
        self.opt = Arcane(self)
        self.art = YnnariCruelArtefacts(self)
        self.transport = self.Transport(self)

    def get_unique_gear(self):
        return self.art.description + self.mle.get_unique()\
            + self.rng.get_unique()

    def check_rules(self):
        super(Archon, self).check_rules()
        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def build_statistics(self):
        return self.note_transport(super(Archon, self).build_statistics())


class Court(Unit):
    type_id = 'court'
    type_name = 'Court of Archon'

    class Transport(OptionalSubUnit):
        def __init__(self, parent):
            super(Court.Transport, self).__init__(parent, 'Transport')
            SubUnit(self, Raider(parent=None))
            SubUnit(self, Venom(parent=None))

    def __init__(self, parent):
        super(Court, self).__init__(parent, name=self.type_name, points=0)
        self.lhm = Count(self, 'Lhamaean', 1, 12, 10, True,
                         gear=UnitDescription('Lhamaean', 10,
                                              options=[
                                                  Gear('Kabalite armour'),
                                                  Gear('Splinter pistol'),
                                                  Gear('Shaimeshi blade')]))
        self.urgh = Count(self, 'Ur-Ghul', 1, 12, 15, True,
                          gear=UnitDescription('Ur-Ghul', 15))
        self.med = Count(self, 'Medusae', 1, 12, 25, True,
                         gear=UnitDescription('Medusae', 25, options=[Gear('Kabalite armour'), Gear('Eyeburst')]))
        self.ssl = Count(self, 'Sslyth', 1, 12, 25, True,
                         gear=UnitDescription('Sslyth', 25,
                                              options=[
                                                  Gear('Kabalite armour'),
                                                  Gear('Shardcarbine'),
                                                  Gear('Splinter pistol'),
                                                  Gear('Close combat weapon')]))
        self.transport = self.Transport(self)

    def check_rules(self):
        super(Court, self).check_rules()
        Count.norm_counts(1, 12, [self.lhm, self.urgh, self.med, self.ssl])

    def build_statistics(self):
        res = {'Models': self.lhm.cur + self.urgh.cur + self.med.cur + self.ssl.cur,
               'Units': 1}
        return self.note_transport(res)


class Succubus(Unit):
    type_id = 'succubus'
    type_name = 'Succubus'

    class Weapon(WychWeapon, Melee):
        def __init__(self, parent):
            super(Succubus.Weapon, self).__init__(parent, name="Melee weapon")
            self.variant('Archite glave', 20)

    class ArtefactMelee(YnnariArtefactBlade, Weapon):
        pass

    class Pistol(Pistol, NoWeapon):
        def __init__(self, parent):
            super(Succubus.Pistol, self).__init__(parent, name="Ranged weapon")
            self.variant('Blast pistol', 15)
            self.variant('Archite glave', 20)
            self.allow_empty(False)

    class ArtefactPistol(YnnariArtefactPistol, Pistol):
        pass

    class Options(OptionsList):
        def __init__(self, parent):
            super(Succubus.Options, self).__init__(parent, name='Options')
            self.variant('Haywire grenades', 5)
            self.variant('Webway portal', 35)

    def __init__(self, parent):
        super(Succubus, self).__init__(parent, self.type_name, 75,
                                       gear=[Gear('Plasma grenades'), Gear('Wychsuit')])
        self.mle = self.ArtefactMelee(self)
        self.rng = self.ArtefactPistol(self)
        self.opt = self.Options(self)
        self.art = YnnariCruelArtefacts(self)

    def get_unique_gear(self):
        return self.art.description + self.mle.get_unique() +\
            self.rng.get_unique()

    def count_weapons(self):
        cnt = 0
        if self.mle.is_wych():
            cnt += 2
        else:
            cnt += 1
        if self.rng.cur != self.rng.nothing and self.rng.used:
            cnt += 1
        return cnt

    def check_rules(self):
        self.rng.allow_empty(True)
        cnt = self.count_weapons()
        if cnt != 2:
            self.error('Succubus carries 2 weapons in hands; taken {}'.
                       format(cnt))
        # self.rng.used = self.rng.visible = not self.mle.is_wych()


class Lelith(Unit):
    type_name = 'Lelith Hesperax, Queen of knives'
    type_id = 'lelith'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Lelith.Weapon, self).__init__(parent, name='Weapon')
            self.variant('Close combat weapon', 0)
            self.variant('Impaler', 15)

    def __init__(self, parent):
        super(Lelith, self).__init__(parent, name=self.type_name, points=150, unique=True, gear=[
            Gear('Wychsuit'),
            Gear('Plasma grenades'),
            Gear('Close combat weapon')])
        self.Weapon(self)


class Haemunculus(Unit):
    type_name = 'Haemonculus'
    type_id = 'haem'

    class Melee(Torment, Torture):
        def __init__(self, parent):
            super(Haemunculus.Melee, self).__init__(parent)

    class ArtefactMelee(ArtefactBlade, Melee):
        def __init__(self, parent):
            super(Haemunculus.ArtefactMelee, self).__init__(parent)
            if parent.roster.is_coven():
                self.djin.used = self.djin.visible = False

    class Ranged(Torment, Pistol):
        def __init__(self, parent):
            super(Haemunculus.Ranged, self).__init__(parent)

    class ArtefactRanged(ArtefactPistol, Ranged):
        def __init__(self, parent):
            super(Haemunculus.ArtefactRanged, self).__init__(parent)
            if parent.roster.is_coven():
                self.par.used = self.par.visible = False

    class Options(OptionsList):
        def __init__(self, parent):
            super(Haemunculus.Options, self).__init__(parent, name='Options')
            self.variant('Crucible of malediction', 20)
            self.variant('Webway portal', 35)

    def __init__(self, parent):
        super(Haemunculus, self).__init__(parent, name=self.type_name,
                                          points=70, gear=[Gear('Gnarlskin')])
        self.mle1 = self.ArtefactMelee(self)
        self.mle2 = self.ArtefactMelee(self)
        self.rng = self.ArtefactRanged(self)
        self.Options(self)
        self.art = Artefacts(self)

    def check_rules(self):
        super(Haemunculus, self).check_rules()

        if len(self.get_unique_gear()) > 1:
            self.error('Model cannot carry more then one relic')

    def get_unique_gear(self):
        return self.art.description + self.mle1.get_unique() +\
            self.mle2.get_unique() + self.rng.get_unique()


class Rakarth(Unit):
    type_name = 'Urien Rakarth, sculptor of torments'
    type_id = 'rakarth'

    def __init__(self, parent):
        super(Rakarth, self).__init__(parent, name=self.type_name, points=140, unique=True, gear=[
            Gear('Close combat weapon'),
            Gear('Gnarlskin'),
            Gear('Casket of Flensing'),
            Gear('Clone field'),
            Gear('Ichor injector')])


class Drazhar(Unit):
    type_name = 'Drazhar, Master of blades'
    type_id = 'drazhar'

    def __init__(self, parent):
        super(Drazhar, self).__init__(parent, name=self.type_name, points=190, unique=True, gear=[Gear('The Executioner\'s Armour'), Gear('Demiclaives')])
