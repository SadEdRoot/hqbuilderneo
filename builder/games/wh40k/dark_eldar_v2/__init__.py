__author__ = 'Ivan Truskov'
__summary__ = ['Codex Dark Eldar 2014', 'The Haemunculus Covens 2014']

from .hq import *
from .elites import *
from .troops import *
from .fast import *
from .heavy import *
from builder.games.wh40k.ynnari import Yvraine, Visarch, Yncarne
from builder.games.wh40k.roster import Wh40kBase, Wh40k7ed, CombinedArmsDetachment,\
    AlliedDetachment, Roster, Wh40kKillTeam,\
    PrimaryDetachment, PlanetstrikeAttacker, PlanetstrikeDefender, SiegeAttacker, SiegeDefender, Wh40k7edMissions
from builder.games.wh40k.section import HQSection, ElitesSection, TroopsSection,\
    FastSection, HeavySection, UnitType, Detachment, Formation, FlyerSection,\
    CompositeFormation, LordsOfWarSection
from builder.games.wh40k.fortifications import Fort


class BaseHQ(HQSection):
    def __init__(self, parent):
        super(BaseHQ, self).__init__(parent)
        self.archon = UnitType(self, Archon)
        self.court = UnitType(self, Court, slot=0)
        UnitType(self, Succubus)
        UnitType(self, Lelith)
        UnitType(self, Haemunculus)
        UnitType(self, Rakarth)
        UnitType(self, Drazhar)

    def check_limits(self):
        full_count = len(self.units)
        full_count = (full_count - self.court.count) +\
                     max([0, self.court.count - self.archon.count])
        return self.min <= full_count <= self.max


class HQ(BaseHQ):
    def __init__(self, parent):
        super(HQ, self).__init__(parent)
        UnitType(self, Yvraine)
        UnitType(self, Visarch)


class BaseElites(ElitesSection):
    def __init__(self, parent):
        super(BaseElites, self).__init__(parent)
        UnitType(self, KabaliteTrueborn)
        UnitType(self, Bloodbrides)
        UnitType(self, Incubi)
        UnitType(self, Mandrakes)
        UnitType(self, Wracks)
        UnitType(self, Grotesques)


class Elites(BaseElites):
    pass


class BaseTroops(TroopsSection):
    def __init__(self, parent):
        super(BaseTroops, self).__init__(parent)
        UnitType(self, KabaliteWarriors)
        UnitType(self, Wyches)


class Troops(BaseTroops):
    pass


class BaseFastAttack(FastSection):
    def __init__(self, parent):
        super(BaseFastAttack, self).__init__(parent)
        UnitType(self, Raider)
        UnitType(self, Venom)
        UnitType(self, Beastmasters)
        UnitType(self, Reavers)
        UnitType(self, Hellions)
        razor = UnitType(self, Razorwing)
        razor.visible = False
        UnitType(self, Razorwings)
        UnitType(self, Scourges)


class FastAttack(BaseFastAttack):
    pass


class BaseHeavySupport(HeavySection):
    def __init__(self, parent):
        super(BaseHeavySupport, self).__init__(parent)
        UnitType(self, Talos)
        UnitType(self, Chronos)
        UnitType(self, Ravager)
        raven = UnitType(self, Voidraven)
        raven.visible = False
        UnitType(self, Voidravens)


class HeavySupport(BaseHeavySupport):
    pass


class Lords(LordsOfWarSection):
    def __init__(self, parent):
        super(Lords, self).__init__(parent)
        UnitType(self, Yncarne)


class Fliers(FlyerSection):
    def __init__(self, parent):
        super(Fliers, self).__init__(parent, [Razorwings, Voidravens])


class DarkEldarV2Common(Roster):
    def is_coven(self):
        return False


class RealspaceRaiders(Wh40kBase, DarkEldarV2Common):
    army_id = 'realspace_raiders_v2'
    army_name = 'Realspace Raiders'
    wikilink = 'https://sites.google.com/site/wh40000rules/codices/dark-eldar/structure#TOC-Realspace-Raiders-Detachment'  # AUTO_INSERTED

    def is_coven(self):
        return False

    def __init__(self):
        self.hq = HQ(parent=self)
        fast = FastAttack(self)
        fast.min, fast.max = (1, 6)
        super(RealspaceRaiders, self).__init__(
            sections=[self.hq, Elites(self), Troops(self), fast, HeavySupport(self)])


class DarkEldarV2Base(Wh40kBase, DarkEldarV2Common):
    def __init__(self):
        super(DarkEldarV2Base, self).__init__(
            hq=HQ(parent=self), elites=Elites(self), troops=Troops(self), fast=FastAttack(self),
            heavy=HeavySupport(self), fort=Fort(self), lords=Lords(self))


class DarkEldarV2CAD(DarkEldarV2Base, CombinedArmsDetachment):
    army_id = 'dark_eldar_v2_cad'
    army_name = 'Dark Eldar (Combined arms detachment)'


class DarkEldarV2AD(DarkEldarV2Base, AlliedDetachment):
    army_id = 'dark_eldar_v2_ad'
    army_name = 'Dark Eldar (Allied detachment)'


class DarkEldarV2PA(DarkEldarV2Base, PlanetstrikeAttacker):
    army_id = 'dark_eldar_v2_pa'
    army_name = 'Dark Eldar (Planetstrike attacker detachment)'


class DarkEldarV2PD(DarkEldarV2Base, PlanetstrikeDefender):
    army_id = 'dark_eldar_v2_pd'
    army_name = 'Dark Eldar (Planetstrike defender detachment)'


class DarkEldarV2SA(DarkEldarV2Base, SiegeAttacker):
    army_id = 'dark_eldar_v2_sa'
    army_name = 'Dark Eldar (Siege War attacker detachment)'


class DarkEldarV2SD(DarkEldarV2Base, SiegeDefender):
    army_id = 'dark_eldar_v2_sd'
    army_name = 'Dark Eldar (Siege War defender detachment)'


class FlierDetachment(Wh40kBase):
    army_id = 'dark_eldar_v2_asd'
    army_name = 'Dark Eldar (Air superiority detachment)'

    def __init__(self):
        super(FlierDetachment, self).__init__(wings=Fliers(self))


class DarkEldarV2KillTeam(Wh40kKillTeam):
    army_id = 'dark_eldar_v2_kt'
    army_name = 'Dark Eldar'

    class KTTroops(TroopsSection):
        def __init__(self, parent):
            super(DarkEldarV2KillTeam.KTTroops, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                KabaliteWarriors, Wyches])

    class KTElites(ElitesSection):
        def __init__(self, parent):
            super(DarkEldarV2KillTeam.KTElites, self).__init__(parent)
            Wh40kKillTeam.process_transported_units(self, [
                KabaliteTrueborn, Bloodbrides, Incubi, Wracks,
                Grotesques])
            UnitType(self, Mandrakes)

    class KTFastAttack(FastSection):
        def __init__(self, parent):
            super(DarkEldarV2KillTeam.KTFastAttack, self).__init__(parent)
            Wh40kKillTeam.process_vehicle_units(self, [
                Raider, Venom])
            UnitType(self, Beastmasters)
            UnitType(self, Reavers)
            UnitType(self, Hellions)
            UnitType(self, Scourges)

    def __init__(self):
        super(DarkEldarV2KillTeam, self).__init__(
            self.KTTroops(self), self.KTElites(self), self.KTFastAttack(self)
        )


class Coterie(Wh40kBase):
    army_id = 'dark_eldar_v2_coterie'
    army_name = 'Covenite Coterie Detachment'

    class HQ(HQSection):
        def __init__(self, parent):
            super(Coterie.HQ, self).__init__(parent, 2, 6)
            UnitType(self, Rakarth)
            UnitType(self, Haemunculus)

    class Elites(ElitesSection):
        def __init__(self, parent):
            super(Coterie.Elites, self).__init__(parent, 2, 8)
            UnitType(self, Wracks)
            UnitType(self, Grotesques)

    class Heavy(HeavySection):
        def __init__(self, parent):
            super(Coterie.Heavy, self).__init__(parent, 0, 4)
            UnitType(self, Talos)
            UnitType(self, Chronos)

    def is_coven(self):
        return True

    def __init__(self):
        super(Coterie, self).__init__(sections=[
            self.HQ(self), self.Elites(self), self.Heavy(self)])


class Talon(Formation):
    army_id = 'dark_eldar_v2_talon'
    army_name = 'Blackheart Talon'

    def __init__(self):
        super(Talon, self).__init__()

        class Wings(Razorwings):
            unit_min = 2
            unit_max = 2

        class Ravens(Voidravens):
            unit_min = 2
            unit_max = 2

        UnitType(self, Wings, min_limit=1, max_limit=1)
        UnitType(self, Ravens, min_limit=1, max_limit=1)


class CovenFormation(Formation):
    def is_coven(self):
        return True


class Grotesquerie(CovenFormation):
    army_id = 'dark_eldar_v2_grotesk'
    army_name = 'Grotesquerie'

    def __init__(self):
        super(Grotesquerie, self).__init__()
        urien = UnitType(self, Rakarth)
        haem = UnitType(self, Haemunculus)
        self.add_type_restriction([urien, haem], 1, 1)
        UnitType(self, Grotesques, min_limit=2, max_limit=2)


class Epicureans(CovenFormation):
    army_id = 'dark_eldar_v2_epic'
    army_name = 'Scarlet Epicureans'

    def __init__(self):
        super(Epicureans, self).__init__()
        UnitType(self, Haemunculus, min_limit=1, max_limit=1)
        UnitType(self, Chronos.SingleChronos, min_limit=1, max_limit=1)
        UnitType(self, Wracks, min_limit=2, max_limit=2)


class Scalpel(CovenFormation):
    army_id = 'dark_eldar_v2_scalpel'
    army_name = 'Scalpel Squadron'

    class Wracks(Wracks):
        take_own_transport = False

    def __init__(self):
        super(Scalpel, self).__init__()
        UnitType(self, self.Wracks, min_limit=2, max_limit=2)
        UnitType(self, Venom, min_limit=2, max_limit=2)


class Corpsethief(CovenFormation):
    army_id = 'dark_eldar_v2_corpsethief'
    army_name = 'Corpsethief Claw'

    class Talos(Talos):
        max_size = 5

    def __init__(self):
        super(Corpsethief, self).__init__()
        self.talos = UnitType(self, self.Talos, min_limit=1, max_limit=1)

    def check_rules(self):
        super(Corpsethief, self).check_rules()
        for u in self.talos.units:
            if u.count < 5:
                self.error('Talos unit must include 5 models')


class Artisan(CovenFormation):
    army_id = 'dark_eldar_v2_artisan'
    army_name = 'Dark Artisan'

    def __init__(self):
        super(Artisan, self).__init__()
        UnitType(self, Haemunculus, min_limit=1, max_limit=1)
        UnitType(self, Chronos.SingleChronos, min_limit=1, max_limit=1)
        UnitType(self, Talos.SingleTalos, min_limit=1, max_limit=1)


class Fleshcorps(CovenFormation):
    army_id = 'dark_eldar_v2_fleshcorps'
    army_name = 'Covenite Fleshcorps'

    def __init__(self):
        super(Fleshcorps, self).__init__()
        UnitType(self, Haemunculus, min_limit=1, max_limit=1)
        UnitType(self, Scalpel.Wracks, min_limit=3, max_limit=3)
        UnitType(self, Raider, min_limit=3, max_limit=3)


class Carnival(CovenFormation, CompositeFormation):
    army_id = 'dark_eldar_v2_carnival'
    army_name = 'Carnival of Pain'

    def __init__(self):
        super(Carnival, self).__init__()
        Detachment.build_detach(self, Grotesquerie, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Epicureans, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Scalpel, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Corpsethief, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Artisan, min_limit=1, max_limit=1)
        Detachment.build_detach(self, Fleshcorps, min_limit=1, max_limit=1)


class PurgeCoterie(Formation):
    army_id = 'dark_eldar_v2_purge_coterie'
    army_name = 'Purge Coterie'

    def __init__(self):
        super(PurgeCoterie, self).__init__()
        UnitType(self, Archon, min_limit=1, max_limit=1)
        UnitType(self, KabaliteWarriors, min_limit=1, max_limit=1)
        UnitType(self, Raider, min_limit=1, max_limit=1)
        UnitType(self, Reavers, min_limit=1, max_limit=1)

    def is_coven(self):
        return False


class RaidingParty(Formation):
    army_name = 'Kabalite Raiding Party'
    army_id = 'dark_eldar_v2_raiding_party'

    def is_coven(self):
        return False

    def __init__(self):
        super(RaidingParty, self).__init__()
        UnitType(self, Archon, min_limit=1, max_limit=1)
        self.transported = [
            UnitType(self, Court, min_limit=1, max_limit=1),
            UnitType(self, Incubi, min_limit=1, max_limit=1),
            UnitType(self, KabaliteWarriors, min_limit=6, max_limit=6)
        ]
        UnitType(self, Scourges, min_limit=1, max_limit=1)
        UnitType(self, Hellions, min_limit=1, max_limit=1)

    def check_rules(self):
        super(RaidingParty, self).check_rules()
        for ut in self.transported:
            for unit in ut.units:
                if not unit.transport.any:
                    self.error("All units in this formation except Archon must take a Raider or Venom as a dedicated transport if they ahve option to do so")
                    return


faction = 'Dark Eldar'


class DarkEldarV2(Wh40k7ed):
    army_id = 'dark_eldar_v2'
    army_name = 'Dark Eldar'
    faction = faction

    def __init__(self):
        super(DarkEldarV2, self).__init__([
            DarkEldarV2CAD, RealspaceRaiders, FlierDetachment,
            RaidingParty,
            Coterie, Grotesquerie, Epicureans,
            Scalpel, Corpsethief, Artisan,
            Fleshcorps, Carnival, Talon, PurgeCoterie])

class DarkEldarV2Missions(Wh40k7edMissions):
    army_id = 'dark_eldar_v2_mis'
    army_name = 'Dark Eldar'
    faction = faction

    def __init__(self):
        super(DarkEldarV2Missions, self).__init__([
            DarkEldarV2CAD, 
            DarkEldarV2PA, DarkEldarV2PD, DarkEldarV2SA, DarkEldarV2SD,
            RealspaceRaiders, FlierDetachment,
            RaidingParty,
            Coterie, Grotesquerie, Epicureans,
            Scalpel, Corpsethief, Artisan,
            Fleshcorps, Carnival, Talon, PurgeCoterie])


detachments = [
    DarkEldarV2CAD,
    DarkEldarV2AD,
    DarkEldarV2PA,
    DarkEldarV2PD,
    DarkEldarV2SA,
    DarkEldarV2SD,
    RealspaceRaiders,
    FlierDetachment,
    RaidingParty,
    Coterie,
    Grotesquerie,
    Epicureans,
    Scalpel,
    Corpsethief,
    Artisan,
    Fleshcorps,
    Carnival,
    Talon,
    PurgeCoterie
]


unit_types = [
    Archon, Court, Succubus, Lelith, Haemunculus, Rakarth,
    Drazhar, KabaliteTrueborn, Bloodbrides, Incubi, Mandrakes, Wracks,
    Grotesques, KabaliteWarriors, Wyches, Raider, Venom, Beastmasters,
    Reavers, Hellions, Razorwings, Scourges, Talos, Chronos, Ravager,
    Voidravens
]
