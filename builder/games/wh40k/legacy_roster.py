__author__ = 'Denis Romanov'

from builder.core.unit import Unit
from builder.core.section import Section
#from fortifications import *
from .old_roster import Wh40k
import builder.core2

#temporary


class Aegis(Unit):
    name = 'Aegis Defense Line'
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Comms relay', 20, 'crel'],
            ['Icarus lascannon', 25, 'icar'],
            ['Quad-gun', 50, 'qgun']
        ], 1)


class Skyshield(Unit):
    name = 'Skyshield Landing Pad'
    base_points = 75
    static = True

#    def __init__(self):
#        Unit.__init__(self)


class Bastion(Unit):
    name = 'Imperial Bastion'
    base_points = 75
    gear = ['4 Heavy bolters']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Options', [
            ['Comms relay', 20, 'crel'],
            ['Icarus lascannon', 25, 'icar'],
            ['Quad-gun', 50, 'qgun']
        ], 1)


class Fortress(Unit):
    name = 'Fortress of Redemtion'
    base_points = 220
    gear = ['Twin-linked Icarus lascannon', 'Fragstorm missiles']

    def __init__(self):
        Unit.__init__(self)
        self.opt_count('Emplaced heavy bolters', 0, 4, 10)
        self.opt_options_list('Options', [['Krakstorm missiles', 30, 'ksm']])


class LegacyWh40k(Wh40k):

    class ArmyOptions(builder.core2.OptionsList):
        def __init__(self, parent):
            super(LegacyWh40k.ArmyOptions, self).__init__(parent=parent, name='Options', visible=False)
            self.ia = self.variant('Imperial armour')
            self.apoc = self.variant('Apocalypse', visible=False)

    def build_detach_unit(self, roster_class):
        class Detach(Unit):
            name = roster_class.army_name
            unique = True
            parent = self

            def __init__(self):
                Unit.__init__(self, detach_unit=True)
                self.sub_roster = self.append_opt(SubRoster(roster_class))

        Detach.__name__ = roster_class.__name__ + 'Detach'
        return Detach

    def __init__(self, hq, elites, troops, fast, heavy, secondary=False, super_heavy=None, ally=None):
        hq = Section('HQ', 1, 2, hq, self)
        elites = Section('Elites', 0, 3, elites, self)
        troops = Section('Troops', 2, 6, troops, self)
        fast = Section('Fast attack', 0, 3, fast, self)
        heavy = Section('Heavy support', 0, 3, heavy, self)
        fort = None
        if not secondary:
            fort = Section('Fortification', 0, 1,
                           [Aegis, Skyshield,
                            Bastion, Fortress], roster=self)

        if super_heavy:
            super_heavy = Section('Super-heavy', 0, 0, super_heavy, self)

        super(LegacyWh40k, self).__init__(
            hq=hq, elites=elites, troops=troops, fast=fast, heavy=heavy, fort=fort, ally=ally, super_heavy=super_heavy,
            secondary=secondary
        )
        self.opt = self.ArmyOptions(self)

    @property
    def ia_enabled(self):
        return not self.secondary and self.opt.ia.value

    def load(self, data):
        super(LegacyWh40k, self).load(data)
        #legacy settings loading
        settings = data.get('settings', None)
        if not settings:
            return
        foc = settings.get('foc', 'Standard')
        if settings.get('foc_custom', False):
            foc = 'Custom'
        ia = self.opt.ia.value
        for o in settings.get('options', []):
            if o.get('id', None) == 'ia':
                ia = o.get('value', ia)
        self.opt.ia.value = ia
        foc_types = {
            'Standard': self.foc.std,
            'Double FOC': self.foc.double,
            'Planet Strike Attacker\'s forces': self.foc.ps_att,
            'Planet Strike Defender\'s forces': self.foc.ps_def,
            'Zone Mortalis Attacker\'s forces': self.foc.zm_att,
            'Zone Mortalis Defender\'s forces': self.foc.zm_def,
            'Zone Mortalis Combatant\'s forces': self.foc.zm_combat,
            'Unlimited': self.foc.unlimited,
            'Custom': self.foc.custom,
        }
        self.foc.cur = foc_types.get(foc, self.foc.std)
