__author__ = 'Ivan Truskov'

from builder.core2 import Count, UnitDescription, Gear
from builder.games.wh40k.roster import Unit


class Fulgurites(Unit):
    type_id = 'fulgurite_v1'
    type_name = 'Fulgurite Electro-Priests'
    model_points = 18

    def __init__(self, parent):
        super(Fulgurites, self).__init__(parent)
        self.models = Count(self, self.type_name, 5, 15, self.model_points, True,
                            gear=UnitDescription('Fulgurite Electro-Priest', self.model_points,
                                                 options=[
                                                     Gear('Electrostatic gauntlets'),
                                                     Gear('Voltagheist field')
                                                 ]))

    def get_count(self):
        return self.models.cur


class Corpuscarii(Unit):
    type_id = 'corpuscarii_v1'
    type_name = 'Corpuscarii Electro-Priests'
    model_points = 18

    def __init__(self, parent):
        super(Corpuscarii, self).__init__(parent)
        self.models = Count(self, self.type_name, 5, 15, self.model_points, True,
                            gear=UnitDescription('Corpuscarii Electro-Priest', self.model_points,
                                                 options=[
                                                     Gear('Electrostatic gauntlets'),
                                                     Gear('Voltagheist field')
                                                 ]))

    def get_count(self):
        return self.models.cur


class Servitors(Unit):
    type_name = "Servitors"
    type_id = 'cm_servitors_v1'
    model_points = 10

    def __init__(self, parent):
        super(Servitors, self).__init__(parent=parent)
        self.models = Count(self, self.name, points=self.model_points, min_limit=1, max_limit=5, per_model=True)
        self.hw = [
            Count(self, 'Heavy bolter', 0, 2, 10),
            Count(self, 'Multi-melta', 0, 2, 10),
            Count(self, 'Plasma cannon', 0, 2, 15)
        ]

    def check_rules(self):
        Count.norm_counts(0, min(2, self.get_count()), self.hw)

    def build_description(self):
        desc = UnitDescription(self.name, points=self.points, count=self.get_count())
        serv = UnitDescription('Servitor', points=10)
        arms_count = self.get_count()
        for wep in self.hw:
            hw_serv = serv.clone()
            hw_serv.add(Gear(wep.name))
            hw_serv.points += wep.option_points
            hw_serv.count = wep.cur
            desc.add(hw_serv)
            arms_count -= wep.cur
        serv.add(Gear('Servo-Arm'))
        if arms_count > 0:
            serv.count = arms_count
            desc.add(serv)
        return desc

    def get_count(self):
        return self.models.cur
