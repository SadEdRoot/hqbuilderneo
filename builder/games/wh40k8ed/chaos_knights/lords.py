__author__ = 'Ivan Truskov'

from . import melee, ranged, units, armory
from builder.games.wh40k8ed.unit import LordsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, \
    UnitDescription, OptionsList, UnitList, ListSubUnit
from builder.games.wh40k8ed.utils import *


class KnightDespoiler(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.KnightDespoiler)
    type_id = 'rknight_v2'
    power = 23

    class CaparaceWeapons(OptionsList):
        def __init__(self, parent):
            super(KnightDespoiler.CaparaceWeapons, self).__init__(parent, 'Caparace Weapon',
                                                                  limit=1)
            armory.add_caparace_weapons(self)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightDespoiler.Weapon1, self).__init__(parent, 'Weapon')
            self.thunderstrike = self.variant(*melee.ThunderstrikeGauntlet)
            armory.add_knight_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(KnightDespoiler.Weapon2, self).__init__(parent, '')
            self.chainsword = self.variant(*melee.ReaperChainsword)
            armory.add_knight_weapons(self)

    class SecondaryWeapon(OneOf):
        def __init__(self, parent):
            super(KnightDespoiler.SecondaryWeapon, self).__init__(parent, 'Secondary weapon')
            self.variant(*ranged.HeavyStubber)
            self.variant(*ranged.Meltagun)

    def __init__(self, parent):
        super(KnightDespoiler, self).__init__(parent, points=get_cost(units.KnightDespoiler),
                                              gear=[Gear('Titanic feet')])
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.SecondaryWeapon(self)
        self.CaparaceWeapons(self)

    def build_points(self):
        points = super(KnightDespoiler, self).build_points()
        if self.wep1.thunderstrike == self.wep1.cur or self.wep2.chainsword == self.wep2.cur:
            points += get_cost(units.KnightDespoilerGallant) - get_cost(units.KnightDespoiler)
        return points


class KnightTyrant(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.KnightTyrant)
    type_id = 'rknight_dominus_v2'

    power = 30

    class WeaponCaparace(OneOf):
        def __init__(self, parent):
            super(KnightTyrant.WeaponCaparace, self).__init__(parent, 'Caparace Weapon')
            self.variant(*ranged.TwinSiegebreakerCannon)
            self.variant('Two shieldbreaker missiles', 2 * get_cost(ranged.ShieldbreakerMissile),
                         gear=Gear('Shieldbreaker missile', count=2))

    class Weapons(OneOf):
        def __init__(self, parent):
            super(KnightTyrant.Weapons, self).__init__(parent, 'Weapons')
            gear1 = [ranged.PlasmaDecimator, ranged.VolcanoLance]
            self.variant(join_and(*gear1), get_costs(*gear1),
                         gear=create_gears(*gear1))
            gear2 = [ranged.ConflagrationCannon, ranged.ThundercoilHarpoon]
            self.variant(join_and(*gear2), get_costs(*gear2),
                         gear=create_gears(*gear2))

    def __init__(self, parent):
        gear = [ranged.TwinSiegebreakerCannon,
                ranged.TwinMeltagun, ranged.TwinMeltagun,
                ranged.ShieldbreakerMissile, ranged.ShieldbreakerMissile,
                melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightTyrant), *gear)
        super(KnightTyrant, self).__init__(parent, get_name(units.KnightTyrant), points=cost,
                                           gear=create_gears(*gear))
        self.Weapons(self)
        self.WeaponCaparace(self)


class WarDog(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.WarDog)
    type_id = 'rarmiger_v2'
    power = 9
    is_armiger = True
    keywords = ['Vehicle']

    class SingleWarDog(ListSubUnit):
        type_name = 'War Dog'

        class Weapons(OneOf):
            def __init__(self, parent):
                super(WarDog.SingleWarDog.Weapons, self).__init__(parent, 'Weapons')
                gear1 = [ranged.WarDogAutocannon, ranged.WarDogAutocannon]
                self.variant('Two War Dog autocannons', get_costs(*gear1),
                             gear=create_gears(*gear1))
                gear2 = [ranged.ThermalSpear, melee.ReaperChainCleaver]
                self.variant(join_and(*gear2), get_costs(*gear2),
                             gear=create_gears(*gear2))

        class Caparace(OneOf):
            def __init__(self, parent):
                super(WarDog.SingleWarDog.Caparace, self).__init__(parent, 'Caparace weapon')
                self.variant(*ranged.HeavyStubber)
                self.variant(*ranged.Meltagun)

        def __init__(self, parent):
            super(WarDog.SingleWarDog, self).__init__(parent, points=get_cost(units.WarDog))
            self.Weapons(self)
            self.Caparace(self)

    def __init__(self, parent):
        super(WarDog, self).__init__(parent)
        self.models = UnitList(self, self.SingleWarDog, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.models.count


class KnightDesecrator(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.KnightDesecrator)
    type_id = 'chkn_desecrator_v1'

    power = 23

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(KnightDesecrator.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*melee.ReaperChainsword)
            self.variant(*melee.ThunderstrikeGauntlet)

    def __init__(self, parent):
        gear = [ranged.LaserDestructor, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightDesecrator), *gear)
        super(KnightDesecrator, self).__init__(parent, get_name(units.KnightDesecrator), points=cost,
                                               gear=create_gears(*gear))
        self.Weapon1(self)
        KnightDespoiler.SecondaryWeapon(self)
        KnightDespoiler.CaparaceWeapons(self)


class KnightRampager(LordsUnit, armory.KnightUnit):
    type_name = get_name(units.KnightRampager)
    type_id = 'chkn_rampager_v1'

    power = 19

    def __init__(self, parent):
        gear = [ranged.HeavyStubber, melee.ReaperChainsword, melee.ThunderstrikeGauntlet, melee.TitanicFeet]
        cost = points_price(get_cost(units.KnightRampager), *gear)
        super(KnightRampager, self).__init__(parent, get_name(units.KnightRampager), points=cost,
                                             gear=create_gears(*gear))
