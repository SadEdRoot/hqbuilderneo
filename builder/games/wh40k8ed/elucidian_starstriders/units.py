from builder.games.wh40k8ed.unit import HqUnit, ElitesUnit, TroopsUnit, Unit
from builder.games.wh40k8ed.options import UnitDescription, Gear
from . import points
from builder.games.wh40k8ed.utils import *


class StarstriderUnit(Unit):
    faction = ['Imperium', 'Elucidian Starstriders']


class Elucia(HqUnit, StarstriderUnit):
    type_name = get_name(points.EluciaVhane)
    type_id = 'elucia_v1'
    faction = ['Astra Cartographica']
    keywords = ['Character', 'Infantry', 'Rogue Trader']
    power = 4

    def __init__(self, parent):
        super(Elucia, self).__init__(parent, points=get_cost(points.EluciaVhane),
                                     static=True, unique=True,
                                     gear=[Gear('Monomolecular cane-rapier'),
                                           Gear('Heirloom pistol'),
                                           Gear('Concussion grenades')])

    def build_statistics(self):
        res = super(Elucia, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 3
        return res


class Larsen(ElitesUnit, StarstriderUnit):
    type_name = get_name(points.Larsen)
    type_id = 'larsen_v1'
    keywords = ['Character', 'Infantry', 'Adeptus Mechanicus', 'Mars', 'Tech-Priest', 'Lectro-Maester']
    power = 4

    def __init__(self, parent):
        super(Larsen, self).__init__(parent, points=get_cost(points.Larsen),
                                     static=True, unique=True,
                                     gear=[Gear('Voltaic pistol'),
                                           Gear('Concussion grenades')])


class Sanistasia(ElitesUnit, StarstriderUnit):
    type_name = get_name(points.SanistasiaMinst)
    type_id = 'sanistasia_v1'
    keywords = ['Character', 'Infantry', 'Rejuvenat Adept']
    power = 4

    def __init__(self, parent):
        super(Sanistasia, self).__init__(parent, points=get_cost(points.SanistasiaMinst),
                                         static=True, unique=True,
                                         gear=[Gear('Scalpel claw'),
                                               Gear('Laspistol'),
                                               Gear('Concussion grenades')])


class KnossoPrond(ElitesUnit, StarstriderUnit):
    type_name = get_name(points.KnossoPrond)
    type_id = 'knosso_v1'
    keywords = ['Character', 'Infantry', 'Adeptus Ministorum', 'Death Cult executioner']
    power = 3

    def __init__(self, parent):
        super(KnossoPrond, self).__init__(parent, points=get_cost(points.KnossoPrond),
                                          static=True, unique=True,
                                          gear=[Gear('Death Cult power blade'),
                                                Gear('Dartmask'),
                                                Gear('Concussion grenades')])


class NitschSquad(TroopsUnit, StarstriderUnit):
    type_name = get_name(points.NitschSquad)
    type_id = 'nitsch_squad_v1'
    keywords = ['Infantry', 'Voidsmen-at-Arms']
    power = 3

    def __init__(self, parent):
        super(NitschSquad, self).__init__(
            parent, points=get_cost(points.NitschSquad) * 6,
            static=True, unique=True,
            gear=[
                UnitDescription('Voidmaster Nitsch', options=[Gear('Artificer shotgun'), Gear('Laspistol'),
                                                              Gear('Concussion grenades')]),
                UnitDescription('Voidsman', count=3, options=[Gear('Lasgun'), Gear('Laspistol'),
                                                                   Gear('Concussion grenades')]),
                UnitDescription('Voidsman', options=[Gear('Rotor cannon'), Gear('Laspistol'),
                                                     Gear('Concussion grenades')]),
                UnitDescription('Aximillion')
            ])

