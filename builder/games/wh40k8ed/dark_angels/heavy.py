from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, \
    ListSubUnit, UnitList, SubUnit
from . import armory
from . import melee
from . import ranged
from . import wargear
from . import units

from builder.games.wh40k8ed.utils import get_name, create_gears, points_price, get_costs, get_cost


class DACodexDevastators(HeavyUnit, armory.DAUnit):
    type_name = get_name(units.DevastatorSquad) + ' (Codex)'
    type_id = 'da_devastators_v2'
    keywords = ['Infantry']
    power = 8

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]

    class Sergeant(Unit):
        type_name = 'Space Marine Sergeant'

        def __init__(self, parent):
            super(DACodexDevastators.Sergeant, self).__init__(
                parent, gear=create_gears(*DACodexDevastators.model_gear),
                points=get_cost(units.DevastatorSquad)
            )
            self.wep1 = armory.SergeantWeapon(self, '')

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(DACodexDevastators.HeavyWeapon, self).__init__(parent, 'Heavy Weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_heavy_weapons(self)

    class Options(OptionsList):
        def __init__(self, parent):
            super(DACodexDevastators.Options, self).__init__(parent, 'Options')
            self.variant('Armorium Cherub', points=get_cost(units.ArmoriumCherub),
                         gear=[UnitDescription('Armorium Cherub')])

    def get_leader(self):
        return self.Sergeant

    def __init__(self, parent):
        super(DACodexDevastators, self).__init__(parent=parent, name=get_name(units.DevastatorSquad))
        self.sergeant = SubUnit(parent=self, unit=self.get_leader()(self))
        self.marines = Count(parent=self, name='Space Marine', min_limit=4, max_limit=9,
                             points=get_cost(units.DevastatorSquad),
                             per_model=True)
        self.heavy = [self.HeavyWeapon(self) for _ in range(0, 4)]
        self.opt = self.Options(self)

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points,
                               count=self.get_count(), options=self.opt.description)
        desc.add(self.sergeant.description)
        marine = UnitDescription(
            name='Space Marine',
            options=create_gears(*DACodexDevastators.model_gear),
        )
        for o in self.heavy:
            spec_marine = marine.clone()
            spec_marine.add(o.description)
            desc.add_dup(spec_marine)

        marine.add(Gear('Boltgun')).set_count(self.marines.cur - 4)
        desc.add_dup(marine)
        return desc

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return self.power + 3 * (self.marines.cur > 4)


class DAHellblasters(HeavyUnit, armory.DAUnit):
    type_name = get_name(units.HellblasterSquad)
    type_id = 'da_hellblasters_v1'
    keywords = ['Infantry', 'Primaris']
    power = 8

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]

    class Sergeant(OneOf):
        def __init__(self, parent):
            super(DAHellblasters.Sergeant, self).__init__(parent, 'Sergeant weapon')
            self.variant(*ranged.BoltPistol)
            self.variant(*ranged.PlasmaPistol)

        @property
        def description(self):
            return [UnitDescription('Hellblaster Sergeant', options=create_gears(*DAHellblasters.model_gear[0:2])).
                        add(super(DAHellblasters.Sergeant, self).description).
                        add(self.parent.wep.cur.gear)]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DAHellblasters.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*ranged.PlasmaIncinerator)
            self.variant(*ranged.AssaultPlasmaIncinerator)
            self.variant(*ranged.HeavyPlasmaIncinerator)

        @property
        def description(self):
            return []

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Hellblaster', options=create_gears(*DAHellblasters.model_gear)).
                        add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(DAHellblasters, self).__init__(parent, points=get_cost(units.HellblasterSquad))
        self.sarge = self.Sergeant(self)
        self.wep = self.Weapon(self)
        self.marines = self.Marines(self, 'Hellblasters', 4, 9,
                                    get_cost(units.HellblasterSquad), per_model=True)

    def build_points(self):
        return super(DAHellblasters, self).build_points() + self.wep.points * self.marines.cur

    def get_count(self):
        return self.marines.cur + 1

    def build_power(self):
        return self.power * (1 + (self.get_count() > 5))


class DACodexPredator(HeavyUnit, armory.DAUnit):
    type_name = get_name(units.Predator) + ' (Codex)'
    type_id = "da_predator_v2"
    keywords = ['Vehicle']
    power = 9

    class Turret(OneOf):
        def __init__(self, parent):
            super(DACodexPredator.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant(*ranged.PredatorAutocannon)
            self.twinlinkedlascannon = self.variant(*ranged.TwinLascannon)

    class Sponsons(OptionsList):
        def __init__(self, parent):
            super(DACodexPredator.Sponsons, self).__init__(parent=parent, name='Sponsons', limit=1)
            self.sponsonswithheavybolters = self.variant('Two heavy bolters', 2 * get_cost(ranged.HeavyBolter),
                                                         gear=create_gears(*[ranged.HeavyBolter] * 2))
            self.sponsonswithlascannons = self.variant('Two lascannons', 2 * get_cost(ranged.Lascannon),
                                                       gear=create_gears(*[ranged.Lascannon] * 2))

    class Options(OptionsList):
        def __init__(self, parent, blade=False):
            super(DACodexPredator.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.StormBolter)

    def __init__(self, parent):
        super(DACodexPredator, self).__init__(parent=parent, name=get_name(units.Predator),
                                              points=get_cost(units.Predator))
        self.turret = self.Turret(self)
        self.side = self.Sponsons(self)
        self.opt = self.Options(self)


class DACodexWhirlwind(HeavyUnit, armory.DAUnit):
    type_name = get_name(units.Whirlwind) + ' (Codex)'
    type_id = "da_whirlwind_v2"
    keywords = ['Vehicle']
    power = 5

    class Turret(OneOf):
        def __init__(self, parent):
            super(DACodexWhirlwind.Turret, self).__init__(parent=parent, name='Turret')
            self.autocannon = self.variant(*ranged.WhirlwindVengeanceLauncher)
            self.autocannon = self.variant(*ranged.WhirlwindCastellanLauncher)

    def __init__(self, parent):
        super(DACodexWhirlwind, self).__init__(parent=parent, name=get_name(units.Whirlwind),
                                               points=get_cost(units.Whirlwind))
        self.turret = self.Turret(self)
        self.opt = DACodexPredator.Options(self)


class DACodexVindicator(HeavyUnit, armory.DAUnit):
    type_name = get_name(units.Vindicator) + ' (Codex)'
    type_id = "da_vindicator_v2"
    keywords = ['Vehicle']
    power = 7

    def __init__(self, parent):
        gear = [ranged.DemolisherCannon]
        cost = points_price(get_cost(units.Vindicator), *gear)
        super(DACodexVindicator, self).__init__(parent=parent, name=get_name(units.Vindicator),
                                                points=cost,
                                                gear=create_gears(*gear))
        self.opt = DACodexPredator.Options(self)


class DAHunter(HeavyUnit, armory.DAUnit):
    type_name = get_name(units.Hunter)
    type_id = "da_hunter_v1"
    keywords = ['Vehicle']
    power = 5

    def __init__(self, parent):
        gear = [ranged.SkyspearMissileLauncher]
        cost = points_price(get_cost(units.Hunter), *gear)
        super(DAHunter, self).__init__(parent=parent, points=cost,
                                       gear=create_gears(*gear))
        self.opt = DACodexPredator.Options(self)


class DAStalker(HeavyUnit, armory.DAUnit):
    type_name = get_name(units.Stalker)
    type_id = "da_stalker_v1"
    keywords = ['Vehicle']
    power = 6

    def __init__(self, parent):
        gear = [ranged.IcarusStormcannon] * 2
        super(DAStalker, self).__init__(parent=parent,
                                        points=points_price(get_cost(units.Stalker), *gear),
                                        gear=create_gears(*gear))
        self.opt = DACodexPredator.Options(self)


class DALandRaider(HeavyUnit, armory.DAUnit):
    type_id = 'da_land_raider_v1'
    type_name = get_name(units.LandRaider)

    keywords = ['Vehicle', 'Transport']
    power = 19

    @classmethod
    def calc_faction(cls):
        return super(DALandRaider, cls).calc_faction() + ['DEATHWATCH']

    class Options(DACodexPredator.Options):
        def __init__(self, parent):
            super(DALandRaider.Options, self).__init__(parent)
            self.variant(*ranged.MultiMelta)

    def __init__(self, parent):
        gear = [ranged.TwinHeavyBolter, ranged.TwinLascannon, ranged.TwinLascannon]
        cost = points_price(get_cost(units.LandRaider), *gear)
        super(DALandRaider, self).__init__(parent=parent, points=cost,
                                           gear=create_gears(*gear))
        self.opt = self.Options(self)


class DALandRaiderCrusader(HeavyUnit, armory.DAUnit):
    type_id = 'da_land_raider_crusader_v1'
    type_name = get_name(units.LandRaiderCrusader)
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 16

    @classmethod
    def calc_faction(cls):
        return super(DALandRaiderCrusader, cls).calc_faction() + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [ranged.TwinAssaultCannon, ranged.HurricaneBolter, ranged.HurricaneBolter]
        cost = points_price(get_cost(units.LandRaiderCrusader), *gear)
        super(DALandRaiderCrusader, self).__init__(parent=parent, gear=create_gears(*gear),
                                                   points=cost)
        self.opt = DALandRaider.Options(self)


class DALandRaiderRedeemer(HeavyUnit, armory.DAUnit):
    type_id = 'da_land_raider_redeemer_v1'
    type_name = get_name(units.LandRaiderRedeemer)
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 18

    @classmethod
    def calc_faction(cls):
        return super(DALandRaiderRedeemer, cls).calc_faction() + ['DEATHWATCH']

    def __init__(self, parent):
        gear = [ranged.TwinAssaultCannon, ranged.FlamestormCannon, ranged.FlamestormCannon]
        cost = points_price(get_cost(units.LandRaiderRedeemer), *gear)
        super(DALandRaiderRedeemer, self).__init__(parent=parent, points=cost,
                                                   gear=create_gears(*gear))
        self.opt = DALandRaider.Options(self)


class DAEliminators(HeavyUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.EliminatorSquad)
    type_id = 'da_eliminators_v1'

    keywords = ['Infantry', 'Primaris', 'Phobos']

    model_cost = get_cost(units.EliminatorSquad)
    gearlist = [ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades, wargear.CamoCloak]
    power = 4

    class Sergeant(OneOf):
        def __init__(self, parent):
            super(DAEliminators.Sergeant, self).__init__(parent, 'Sergeant weapon')
            self.variant(*ranged.BoltSniperRifle)
            self.variant(*ranged.InstigatorBoltCarbine)
            self.variant(*ranged.LasFusil)

        @property
        def description(self):
            return [UnitDescription('Eliminator Sergeant', options=create_gears(*DAEliminators.gearlist))
                        .add(super(DAEliminators.Sergeant, self).description)]

    class SquadWeapons(OneOf):
        def __init__(self, parent):
            super(DAEliminators.SquadWeapons, self).__init__(parent, 'Squad weapons')
            self.variant(*ranged.BoltSniperRifle)
            self.variant(*ranged.LasFusil)

        @property
        def description(self):
            return [UnitDescription('Eliminator', count=2, options=create_gears(*DAEliminators.gearlist))
                        .add(super(DAEliminators.SquadWeapons, self).description)]

    def __init__(self, parent):
        cost = points_price(get_cost(units.EliminatorSquad), *self.gearlist)
        super(DAEliminators, self).__init__(parent, points=3 * cost)
        self.Sergeant(self)
        self.sqw = self.SquadWeapons(self)

    def build_points(self):
        return super(DAEliminators, self).build_points() + self.sqw.points

    def get_count(self):
        return 3
