from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList
from . import armory, units, ranged, wargear
from builder.games.wh40k8ed.utils import get_name, create_gears, points_price, get_costs


class DARhino(TransportUnit, armory.DAUnit):
    type_name = get_name(units.Rhino)
    type_id = 'da_rhino_aa_v1'

    keywords = ['Vehicle', 'Transport']
    power = 4

    class Options(OptionsList):
        def __init__(self, parent, blade=False):
            super(DARhino.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.StormBolter)

    def __init__(self, parent):
        gear = [ranged.StormBolter]
        super(DARhino, self).__init__(
            parent, gear=create_gears(*gear),
            points=points_price(armory.get_cost(units.Rhino), *gear))
        self.Options(self)



class DACodexRazorback(TransportUnit, armory.DAUnit):
    type_name = get_name(units.Razorback) + ' (Codex)' 
    type_id = 'da_razorback_v2'

    keywords = ['Vehicle', 'Transport']
    power = 5

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DACodexRazorback.Weapon, self).__init__(parent=parent, name='Weapon')

            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)
            self.variant(*ranged.TwinAssaultCannon)

    def __init__(self, parent):
        super(DACodexRazorback, self).__init__(parent=parent,
                                               points=armory.get_cost(units.Razorback))
        self.Weapon(self)
        DARhino.Options(self)


class DACodexDropPod(TransportUnit, armory.DAUnit):
    type_id = 'da_drop_pod_v2'
    type_name = get_name(units.DropPod) + ' (Codex)'

    keywords = ['Vehicle', 'Transport']
    power = 5

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DACodexDropPod.Weapon, self).__init__(parent, 'Weapon')
            self.sbgun = self.variant(*ranged.StormBolter)
            self.dwind = self.variant(*ranged.DeathwindLauncher)

    def __init__(self, parent):
        super(DACodexDropPod, self).__init__(parent=parent, name=get_name(units.DropPod),
                                             points=armory.get_cost(units.DropPod))
        self.Weapon(self)


class DALandSpeederStorm(TransportUnit, armory.DAUnit):
    type_name = get_name(units.LandSpeederStorm)
    model_points = armory.get_cost(units.LandSpeederStorm)
    type_id = 'da_land_speeder_storm_v1'
    keywords = ['Vehicle', 'Transport', 'Land Speeder', 'Scout']
    power = 5

    def __init__(self, parent):
        gear = [ranged.CerberusLauncher]
        super(DALandSpeederStorm, self).__init__(
            parent=parent, points=points_price(self.model_points, *gear),
            gear=create_gears(gear)
        )
        self.weapon = self.Weapon(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DALandSpeederStorm.Weapon, self).__init__(parent=parent, name='Weapon')
            self.heavybolter = self.variant(*ranged.HeavyBolter)
            self.multimelta = self.variant(*ranged.MultiMelta)
            self.heavyflamer = self.variant(*ranged.HeavyFlamer)
            self.assaultcannon = self.variant(*ranged.AssaultCannon)


class DARepulsor(TransportUnit, armory.DAUnit):
    type_name = get_name(units.Repulsor)
    type_id = 'da_repulsor_v1'
    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 16

    @classmethod
    def calc_faction(cls):
        return super(DARepulsor, cls).calc_faction() + ['DEATHWATCH']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(DARepulsor.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(DARepulsor.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavyOnslaughtGatlingCannon)
            self.variant(*ranged.LasTalon)

    class Weapon3(OneOf):
        def __init__(self, parent):
            super(DARepulsor.Weapon3, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)
            self.variant(*ranged.OnslaughtGatlingCannon)

    class Weapon4(OneOf):
        def __init__(self, parent):
            super(DARepulsor.Weapon4, self).__init__(parent, '')
            self.variant('Two stormbolters', points=get_costs(*[ranged.StormBolter] * 2),
                         gear=create_gears(*[ranged.StormBolter] * 2))
            self.variant('Two fragstorm grenade launchers', points=get_costs(*[ranged.FragstormGrenadeLauncher] * 2),
                         gear=create_gears(*[ranged.FragstormGrenadeLauncher] * 2))

    class Weapon5(OneOf):
        def __init__(self, parent):
            super(DARepulsor.Weapon5, self).__init__(parent, 'AA turret')
            self.variant(*ranged.IcarusIronhailHeavyStubber)
            self.variant(*ranged.IcarusRocketPod)
            self.variant(*ranged.StormBolter)
            self.variant(*ranged.FragstormGrenadeLauncher)

    class Weapon6(OneOf):
        def __init__(self, parent):
            super(DARepulsor.Weapon6, self).__init__(parent, 'Launchers')
            self.variant(*wargear.AutoLaunchers)
            self.variant('Two fragstorm grenade launchers', points=get_costs(*[ranged.FragstormGrenadeLauncher] * 2),
                         gear=create_gears(*[ranged.FragstormGrenadeLauncher] * 2))

    class Weapon7(OptionsList):
        def __init__(self, parent):
            super(DARepulsor.Weapon7, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)

    def __init__(self, parent):
        gear = [ranged.KrakstormGrenadeLauncher] * 2
        super(DARepulsor, self).__init__(parent, points=points_price(armory.get_cost(units.Repulsor), *gear),
                                         gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)
        self.Weapon7(self)
        self.Weapon4(self)
        self.Weapon5(self)
        self.Weapon6(self)
