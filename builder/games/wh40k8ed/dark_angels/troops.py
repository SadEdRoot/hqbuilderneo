from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, \
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
from . import armory, units, ranged, melee, wargear
from builder.games.wh40k8ed.utils import get_name, create_gears, get_cost, get_costs, points_price


class DATacticalSquad(TroopsUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.TacticalSquad)
    type_id = 'da_tactical_squad_v1'

    keywords = ['Infantry']

    common_gear = [ranged.FragGrenades, ranged.KrakGrenades]

    model = UnitDescription('Tactical Squad', options=create_gears(*(common_gear + [ranged.BoltPistol])))

    class Sergeant(armory.ClawUser):
        type_name = 'Space Marine Sergeant'

        class Bomb(OptionsList):
            def __init__(self, parent):
                super(DATacticalSquad.Sergeant.Bomb, self).__init__(parent, 'Melta Bomb')
                self.variant(*ranged.MeltaBombs)

        def __init__(self, parent):
            super(DATacticalSquad.Sergeant, self).__init__(parent, points=get_cost(units.TacticalSquad),
                                                           gear=create_gears(*DATacticalSquad.common_gear))
            self.wep1 = armory.SergeantWeapon(self, double=True)
            self.wep2 = armory.SergeantWeapon(self, double=False)
            DATacticalSquad.Sergeant.Bomb(self)

        def check_rules(self):
            super(DATacticalSquad.Sergeant, self).check_rules()
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Marine(Count):
        @property
        def description(self):
            return DATacticalSquad.model.clone().add(create_gears(ranged.Boltgun)). \
                set_count(self.cur - sum(c.used for c in [self.parent.special, self.parent.heavy]))

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(DATacticalSquad.SpecialWeapon, self).__init__(parent, 'Special weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_special_weapons(self)

        @property
        def description(self):
            return []

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(DATacticalSquad.HeavyWeapon, self).__init__(parent, 'Heavy weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_heavy_weapons(self)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(DATacticalSquad, self).__init__(parent, get_name(units.TacticalSquad))
        self.sup = SubUnit(self, self.Sergeant(parent=self))
        self.squad = self.Marine(self, 'Tactical Squad', 4, 9, per_model=True,
                                 points=get_cost(units.TacticalSquad))
        self.special = self.SpecialWeapon(self)
        self.heavy = self.HeavyWeapon(self)

    def build_description(self):
        res = super(DATacticalSquad, self).build_description()
        if self.special.used:
            res.add_dup(DATacticalSquad.model.clone().add(self.special.cur.gear))
        if self.heavy.used:
            res.add_dup(DATacticalSquad.model.clone().add(self.heavy.cur.gear))
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 5 + 4 * (self.get_count() > 5)

    def check_rules(self):
        super(DATacticalSquad, self).check_rules()

        self.special.visible = self.special.used = (not self.heavy.used or
                                                    self.heavy.cur == self.heavy.bolt or
                                                    self.get_count() == 10)
        self.heavy.visible = self.heavy.used = (not self.special.used or
                                                self.special.cur == self.special.bolt or
                                                self.get_count() == 10)


class DAScoutSquad(TroopsUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.ScoutSquad)
    type_id = 'da_scout_squad_v1'

    keywords = ['Infantry']
    model = UnitDescription('Scout', options=create_gears(ranged.FragGrenades,
                                                          ranged.KrakGrenades,
                                                          ranged.BoltPistol))

    class Sergeant(armory.ClawUser):
        type_name = 'Scout Sergeant'

        class Options(OptionsList):
            def __init__(self, parent):
                super(DAScoutSquad.Sergeant.Options, self).__init__(parent=parent, name='Options')
                self.variant(*wargear.CamoCloak)

        def __init__(self, parent):
            super(DAScoutSquad.Sergeant, self).__init__(parent, points=get_cost(units.ScoutSquad),
                                                        gear=create_gears(ranged.FragGrenades,
                                                                          ranged.KrakGrenades))

            class Weapon1(armory.SergeantWeapon):
                def add_single(self):
                    super(Weapon1, self).add_single()
                    self.variant(*ranged.SniperRifle)
                    self.variant(*ranged.AstartesShotgun)
                    self.variant(*melee.CombatKnife)

            self.wep1 = Weapon1(self)
            self.wep2 = armory.SergeantWeapon(self, double=True)
            self.opt = self.Options(self)

        def check_rules(self):
            super(DAScoutSquad.Sergeant, self).check_rules()
            armory.SergeantWeapon.ensure_pairs(self.wep1, self.wep2)

    class Scout(ListSubUnit):
        type_name = 'Scout'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(DAScoutSquad.Scout.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.Boltgun)
                self.variant(*ranged.SniperRifle)
                self.variant(*ranged.AstartesShotgun)
                self.variant(*melee.CombatKnife)
                self.heavy = [self.variant(*ranged.HeavyBolter),
                              self.variant(*ranged.MissileLauncher)]

        def __init__(self, parent):
            super(DAScoutSquad.Scout, self).__init__(parent, points=get_cost(units.ScoutSquad),
                                                     gear=create_gears(ranged.FragGrenades,
                                                                       ranged.KrakGrenades,
                                                                       ranged.BoltPistol))
            self.wep = self.Weapon(self)
            DAScoutSquad.Sergeant.Options(self)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.wep.cur in self.wep.heavy

    def __init__(self, parent):
        super(DAScoutSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(parent=self, unit=self.Sergeant(self))
        self.squad = UnitList(self, self.Scout, 4, 9)

    def get_count(self):
        return self.squad.count + 1

    def check_rules(self):
        super(DAScoutSquad, self).check_rules()
        hcnt = sum(u.has_heavy() for u in self.squad.units)
        if hcnt > 1:
            self.error('Only one Scout may carry heavy weapon; taken: {}'.format(hcnt))

    def build_power(self):
        return 6 + 4 * (self.squad.count > 4)


class DAIntercessors(TroopsUnit, armory.DAUnit):
    type_name = get_name(units.IntercessorSquad)
    type_id = 'da_intercessors_v1'

    keywords = ['Infantry', 'Primaris']
    gearlist = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]

    swords = [melee.PowerSword]

    power = 5

    class Sergeant(OptionsList):
        def __init__(self, parent):
            super(DAIntercessors.Sergeant, self).__init__(parent, 'Sergeant weapons')
            self.gl = self.variant(*wargear.AuxiliaryGrenadeLauncher)
            self.swords = [self.variant(*t) for t in self.parent.swords]

        def check_rules(self):
            super(DAIntercessors.Sergeant, self).check_rules()
            OptionsList.process_limit(self.swords, 1)

        @property
        def description(self):
            return [UnitDescription('Intercessor Sergeant', options=create_gears(*DAIntercessors.gearlist)).
                        add(self.parent.wep.cur.gear).add(super(DAIntercessors.Sergeant, self).description)]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(DAIntercessors.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*ranged.BoltRifle)
            self.variant(*ranged.AutoBoltRifle)
            self.variant(*ranged.StalkerBoltRifle)

        @property
        def description(self):
            return []

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Intercessor', options=create_gears(*DAIntercessors.gearlist)).
                        add(self.parent.wep.cur.gear).set_count(self.cur - self.parent.aux.cur)]

    class AuxGL(Count):
        @property
        def description(self):
            return [UnitDescription('Intercessor', options=create_gears(*DAIntercessors.gearlist)).
                        add(self.parent.wep.cur.gear).add(create_gears(wargear.AuxiliaryGrenadeLauncher)).
                        set_count(self.cur)]

    def __init__(self, parent):
        super(DAIntercessors, self).__init__(parent, points=get_cost(units.IntercessorSquad))
        self.sarge = self.Sergeant(self)
        self.wep = self.Weapon(self)
        self.marines = self.Marines(self, 'Intercessors', 4, 9, get_cost(units.IntercessorSquad),
                                    per_model=True)
        self.aux = self.AuxGL(self, 'Auxilary grenade launchers', 0, 1, get_costs(wargear.AuxiliaryGrenadeLauncher))

    def get_count(self):
        return 1 + self.marines.cur

    def check_rules(self):
        super(DAIntercessors, self).check_rules()
        self.aux.max = (self.get_count() / 5) - self.sarge.gl.value

    def build_points(self):
        return super(DAIntercessors, self).build_points() + self.wep.points * self.marines.cur

    def build_power(self):
        return self.power * (1 + (self.marines.cur > 4))


class DAInfiltratorSquad(TroopsUnit, armory.DAUnit):
    type_name = 'Dark Angels ' + get_name(units.InfiltratorSquad)
    type_id = 'da_infiltrator_squad_v1'
    keywords = ['Infantry', 'Phobos', 'Primaris']
    power = 5

    model_gear = [ranged.MarksmanBoltCarbine, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]

    class Doctor(OptionsList):
        def __init__(self, parent):
            super(DAInfiltratorSquad.Doctor, self).__init__(parent, 'Specialists', limit=1)
            doctor_cost = points_price(get_cost(units.HelixAdept), *DAInfiltratorSquad.model_gear)
            doc = UnitDescription(get_name(units.HelixAdept), options=create_gears(*parent.model_gear))
            self.variant(get_name(units.HelixAdept), doctor_cost,
                         gear=[doc])
            radist_price = points_price(get_cost(units.InfiltratorSquad), wargear.InfiltratorCommsArray, *DAInfiltratorSquad.model_gear)
            radist = UnitDescription('Infiltrator', options=create_gears(wargear.InfiltratorCommsArray, *DAInfiltratorSquad.model_gear))
            self.variant('Infiltrator with Comms array', radist_price, gear=[radist])

    def __init__(self, parent):
        model_cost = points_price(get_cost(units.InfiltratorSquad), *self.model_gear)
        sarge = UnitDescription('Infiltrator Sergeant', options=create_gears(*self.model_gear))
        trooper = UnitDescription('Infiltrator', options=create_gears(*self.model_gear))
        super(DAInfiltratorSquad, self).__init__(parent, points=model_cost,
                                               gear=[sarge.clone()])
        self.models = Count(self, 'Infiltrators', 4, 9, model_cost, per_model=True, gear=trooper.clone())
        self.doc = self.Doctor(self)

    def get_count(self):
        return 1 + self.models.cur + self.doc.any

    def build_power(self):
        return self.power * (1 + (self.get_count() > 5))

    def check_rules(self):
        super(DAInfiltratorSquad, self).check_rules()
        self.models.min = 4 - self.doc.any
        self.models.max = 9 - self.doc.any


class DAIncursorSquad(TroopsUnit, armory.DAUnit):
    type_name = get_name(units.IncursorSquad)
    type_id = 'da_incursor_squad_v1'
    keywords = ['Infantry', 'Phobos', 'Primaris']
    power = 5
    model_gear = [ranged.OcculusBoltCarbine, ranged.BoltPistol, melee.PairedCombatBlades, ranged.FragGrenades, ranged.KrakGrenades, wargear.SmokeGrenades]

    class Mine(OptionsList):
        def __init__(self, parent):
            super(DAIncursorSquad.Mine, self).__init__(parent, 'Options')
            self.variant(*wargear.HaywireMine)

    def __init__(self, parent):
        model_cost = points_price(get_cost(units.IncursorSquad), *self.model_gear)
        sarge = UnitDescription('Incursor Sergeant', options=create_gears(*self.model_gear))
        trooper = UnitDescription('Incursor', options=create_gears(*self.model_gear))
        super(DAIncursorSquad, self).__init__(parent, points=model_cost,
                                               gear=[sarge.clone()])
        self.models = Count(self, 'Incursors', 4, 9, model_cost, per_model=True, gear=trooper.clone())
        self.Mine(self)

    def get_count(self):
        return 1 + self.models.cur

    def build_power(self):
        return self.power * (1 + (self.get_count() > 5))
