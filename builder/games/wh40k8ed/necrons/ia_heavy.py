__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit
from builder.games.wh40k8ed.options import ListSubUnit, OneOf, UnitList, OptionsList
from builder.games.wh40k8ed.utils import *
from . import armory, ia_units, ia_ranged, ia_wargear


class SentryPylon(HeavyUnit, armory.DynastyUnit):
    type_name = get_name(ia_units.SentryPylon) + ' (Imperial Armour)'
    type_id = 'sentry_pylon_v1'

    keywords = ['Vehicle', 'Artillery']
    power = 8

    class Options(OptionsList):
        def __init__(self, parent):
            super(SentryPylon.Options, self).__init__(parent, 'Options')
            self.variant(*ia_wargear.TeleportationMatrix)

    class SinglePylon(ListSubUnit):
        type_name = 'Sentry Pylon'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(SentryPylon.SinglePylon.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ia_ranged.GaussExterminator)
                self.variant(*ia_ranged.HeatCannon)
                self.variant(*ia_ranged.FocussedDeathRay)

        def __init__(self, parent):
            super(SentryPylon.SinglePylon, self).__init__(parent, points=get_cost(ia_units.SentryPylon))
            self.Weapon(self)

    def __init__(self, parent):
        super(SentryPylon, self).__init__(parent, get_name(ia_units.SentryPylon))
        self.opt = self.Options(self)
        self.models = UnitList(self, self.SinglePylon, 1, 3)

    def get_count(self):
        return self.models.count

    def build_points(self):
        res = super(SentryPylon, self).build_points() + self.opt.points * (self.get_count() - 1)
        return res

    def build_power(self):
        return 1 + 7 * self.get_count()


class TesseractArk(HeavyUnit, armory.DynastyUnit):
    type_name = get_name(ia_units.TesseractArk) + ' (Imperial Armour)'
    type_id = 'tesserac_ark_v1'

    keywords = ['Vehicle', 'Fly']
    power = 13

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TesseractArk.Weapon, self).__init__(parent, 'Weapon')
            self.variant('Two tesla cannons', 2 * get_cost(ia_ranged.TeslaCannon),
                         gear=create_gears(ia_ranged.TeslaCannon, ia_ranged.TeslaCannon))
            self.variant('Two gauss cannons', 2 * get_cost(ia_ranged.GaussCannon),
                         gear=create_gears(ia_ranged.GaussCannon, ia_ranged.GaussCannon))
            self.variant('Two particle beamers', 2 * get_cost(ia_ranged.ParticleBeamer),
                         gear=create_gears(ia_ranged.ParticleBeamer, ia_ranged.ParticleBeamer))

    def __init__(self, parent):
        super(TesseractArk, self).__init__(parent, get_name(ia_units.TesseractArk),
                                           points=get_cost(ia_units.TesseractArk),
                                           gear=create_gears(ia_ranged.TesseractSinglarity))
        self.Weapon(self)
