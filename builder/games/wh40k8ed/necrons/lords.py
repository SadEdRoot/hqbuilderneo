__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, SubUnit, Count
from . import armory, units
from builder.games.wh40k8ed.utils import *


class TesseractVault(LordsUnit, armory.DynastyUnit):
    type_name = get_name(units.TesseractVault)
    type_id = 'vault_v1'
    faction = ["C'tan Shards"]

    keywords = ['Vehicle', 'Titanic', 'Fly']
    power = 25

    def __init__(self, parent):
        super(TesseractVault, self).__init__(parent, points=get_cost(units.TesseractVault),
                                             gear=[Gear('Tesla sphere', count=4)],
                                             static=True)


class Obelisk(LordsUnit, armory.DynastyUnit):
    type_name = get_name(units.Obelisk)
    type_id = 'obelisk_v1'

    keywords = ['Vehicle', 'Titanic', 'Fly']
    power = 22

    def __init__(self, parent):
        super(Obelisk, self).__init__(parent, points=get_cost(units.Obelisk),
                                      gear=[Gear('Tesla sphere', count=4)],
                                      static=True)
