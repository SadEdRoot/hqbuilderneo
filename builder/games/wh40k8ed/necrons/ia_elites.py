__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, ElitesUnit
from builder.games.wh40k8ed.options import OptionsList
from . import armory, ia_units, ia_wargear, ia_ranged, ia_melee
from builder.games.wh40k8ed.utils import *


class TombStalker(ElitesUnit, armory.DynastyUnit):
    type_name = get_name(ia_units.CanoptekTombStalker) + ' (Imperial Armour)'
    type_id = 'tomb_stalker_v1'
    faction = ['Canoptek']
    keywords = ['Monster']
    power = 8

    class Options(OptionsList):
        def __init__(self, parent):
            super(TombStalker.Options, self).__init__(parent, 'Options')
            self.variant(*ia_wargear.GloomPrism)

    def __init__(self, parent):
        gear = [ia_ranged.TwinGaussSlicers, ia_melee.AutomatonClaws]
        cost = points_price(get_cost(ia_units.CanoptekTombStalker), *gear)
        super(TombStalker, self).__init__(parent, get_name(ia_units.CanoptekTombStalker),
                                          cost, gear=create_gears(*gear))
        self.Options(self)
