__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, SubUnit, OptionsList
from . import armory, units, wargear, ranged
from builder.games.wh40k8ed.utils import *


class Imotekh(HqUnit, armory.NecronUnit):
    type_name = get_name(units.Imotekh)
    type_id = 'imotekh_v1'
    faction = ['Sautekh']
    keywords = ['Character', 'Infantry', 'Overlord']

    power = 10

    def __init__(self, parent):
        super(Imotekh, self).__init__(parent=parent, points=get_cost(units.Imotekh),
                                      unique=True, gear=[
                                          Gear('Gauntlet of Fire'),
                                          Gear('Staff of the Destroyer')
                                      ], static=True)

    def build_statistics(self):
        res = super(Imotekh, self).build_statistics()
        if self.star == 2:
            res['Command points'] = 1
        return res


class Overlord(HqUnit, armory.DynastyUnit):
    type_name = get_name(units.Overlord)
    type_id = 'overlord_v1'
    keywords = ['Character', 'Infantry']

    power = 6

    def __init__(self, parent, void=True):
        super(Overlord, self).__init__(parent=parent, points=get_cost(units.Overlord))
        armory.LordMelee(self, void=void)
        armory.Orb(self)


class Lord(HqUnit, armory.DynastyUnit):
    type_name = get_name(units.Lord)
    type_id = 'lord_v1'
    keywords = ['Character', 'Infantry']

    power = 5

    def __init__(self, parent):
        super(Lord, self).__init__(parent=parent, points=get_cost(units.Lord))
        armory.LordMelee(self)
        armory.Orb(self)


class DLord(HqUnit, armory.DynastyUnit):
    type_name = get_name(units.DestroyerLord)
    type_id = 'dlord_v1'
    keywords = ['Character', 'Infantry', 'Fly']

    power = 7

    class Options(armory.Orb):
        def __init__(self, parent):
            super(DLord.Options, self).__init__(parent)
            self.limit = 1
            self.variant(*wargear.Phylactery)

    def __init__(self, parent):
        super(DLord, self).__init__(parent=parent, points=get_cost(units.DestroyerLord))
        armory.LordMelee(self)
        self.Options(self)


class Cryptek(HqUnit, armory.DynastyUnit):
    type_name = get_name(units.Cryptek)
    type_id = 'cryptek_v1'

    keywords = ['Character', 'Infantry']

    power = 5

    class Options(OptionsList):
        def __init__(self, parent):
            super(Cryptek.Options, self).__init__(parent, 'Options', limit=1)
            self.variant(*wargear.Chronometron)
            self.variant(*wargear.CanoptekCloak)

    def __init__(self, parent):
        cost = points_price(get_cost(units.Cryptek), ranged.StaffOfLight)
        super(Cryptek, self).__init__(parent, points=cost, gear=create_gears(ranged.StaffOfLight))
        self.Options(self)


class Zahndrekh(HqUnit, armory.NecronUnit):
    type_name = get_name(units.Zahndrekh)
    type_id = 'zehndrekh_v1'
    faction = ['Sautekh']
    keywords = ['Character', 'Infantry', 'Overlord']

    power = 9

    def __init__(self, parent):
        super(Zahndrekh, self).__init__(parent=parent, points=get_cost(units.Zahndrekh),
                                        unique=True, gear=[
                                            Gear('Staff of Light')
                                        ], static=True)


class Obyron(HqUnit, armory.NecronUnit):
    type_name = get_name(units.Obyron)
    type_id = 'obyron_v1'
    faction = ['Sautekh']
    keywords = ['Character', 'Infantry', 'Lord']

    power = 7

    def __init__(self, parent):
        super(Obyron, self).__init__(parent=parent, points=get_cost(units.Obyron),
                                      unique=True, gear=[
                                          Gear('Warscythe')
                                      ], static=True)


class Szeras(HqUnit, armory.NecronUnit):
    type_name = get_name(units.Szeras)
    type_id = 'szeras_v1'

    keywords = ['Character', 'Infantry', 'Cryptek']

    power = 8

    def __init__(self, parent):
        super(Szeras, self).__init__(parent=parent, points=get_cost(units.Szeras),
                                      unique=True, gear=[
                                          Gear('Eldritch lance')
                                      ], static=True)


class Orikan(HqUnit, armory.NecronUnit):
    type_name = get_name(units.Orikan)
    type_id = 'orikan_v1'
    faction = ['Sautekh']
    keywords = ['Character', 'Infantry', 'Cryptek']

    power = 6

    def __init__(self, parent):
        super(Orikan, self).__init__(parent=parent, points=get_cost(units.Orikan),
                                      unique=True, gear=[
                                          Gear('Staff of Tomorrow')
                                      ], static=True)


class Anrakyr(HqUnit, armory.NecronUnit):
    type_name = get_name(units.Anrakyr)
    type_id = 'anrakyr_v1'

    keywords = ['Character', 'Infantry', 'Overlord']

    power = 9

    def __init__(self, parent):
        super(Anrakyr, self).__init__(parent=parent, points=get_cost(units.Anrakyr),
                                      unique=True, gear=[
                                          Gear('Warscythe'),
                                          Gear('Tachyon arrow')
                                      ], static=True)


class Trazyn(HqUnit, armory.NecronUnit):
    type_name = get_name(units.Trazyn)
    type_id = 'trazyn_v1'
    faction = ['Nihilakh']
    keywords = ['Character', 'Infantry', 'Overlord']

    power = 5

    def __init__(self, parent):
        super(Trazyn, self).__init__(parent=parent, points=get_cost(units.Trazyn),
                                      unique=True, gear=[
                                          Gear('Emppathic Obliterator')
                                      ], static=True)


class CommandBarge(HqUnit, armory.DynastyUnit):
    type_name = get_name(units.CommandBarge)
    type_id = 'command_barge_v1'
    keywords = ['Vehicle', 'Fly', 'Overlord', 'Character']

    power = 9

    class Weapon(OneOf):
        def __init__(self, parent):
            super(CommandBarge.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant(*ranged.TeslaCannon)
            self.variant(*ranged.GaussCannon)

    class Barge(Unit):
        def __init__(self, parent):
            super(CommandBarge.Barge, self).__init__(
                parent, CommandBarge.type_name, points=get_cost(units.CommandBarge) - get_cost(units.Overlord))
            CommandBarge.Weapon(self)

    def __init__(self, parent):
        super(CommandBarge, self).__init__(parent)
        SubUnit(self, Overlord(parent=self, void=False))
        SubUnit(self, self.Barge(parent=self))
