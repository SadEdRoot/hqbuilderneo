__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import LordsUnit
from builder.games.wh40k8ed.options import Gear
from builder.games.wh40k8ed.utils import *
from . import armory, ia_units


class GaussPylon(LordsUnit, armory.DynastyUnit):
    type_name = get_name(ia_units.GaussPylon) + ' (Imperial Armour)'
    type_id = 'gauss_pylon_v1'

    keywords = ['Vehicle', 'Artillery', 'Titanic']
    power = 24

    def __init__(self, parent):
        super(GaussPylon, self).__init__(parent, name=get_name(ia_units.GaussPylon),
                                         points=get_cost(ia_units.GaussPylon),
                                         gear=[Gear('Gauss annihilator'), Gear('Tesla arc')],
                                         static=True)
