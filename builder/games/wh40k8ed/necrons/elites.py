__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, ElitesUnit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, SubUnit, Count
from . import armory, units, wargear, ranged, melee
from builder.games.wh40k8ed.utils import *


class Deathmarks(ElitesUnit, armory.DynastyUnit):
    type_name = get_name(units.Deathmarks)
    type_id = 'deathmarks_v1'
    keywords = ['Infantry']
    power = 5

    def __init__(self, parent):
        super(Deathmarks, self).__init__(parent)
        self.models = Count(self, 'Deathmark', 5, 10, get_cost(units.Deathmarks), True,
                            gear=UnitDescription('Deathmark',
                                                 options=[Gear('Synaptic desintegrator')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 5))


class Lychguard(ElitesUnit, armory.DynastyUnit):
    type_name = get_name(units.Lychguard)
    type_id = 'lychguard_v1'
    keywords = ['Infantry']
    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Lychguard.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.Warscythe)
            self.variant(join_and(melee.HyperphaseSword, wargear.DispersionShield),
                         get_costs(melee.HyperphaseSword, wargear.DispersionShield),
                         gear=create_gears(melee.HyperphaseSword, wargear.DispersionShield))

        @property
        def points(self):
            return super(Lychguard.Weapon, self).points * self.parent.get_count()

    def __init__(self, parent):
        super(Lychguard, self).__init__(parent)
        self.models = Count(self, 'Lychguard', 5, 10, get_cost(units.Lychguard), True)
        self.wep = self.Weapon(self)

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Lychguard',
                                count=self.models.cur)
        model.add(self.wep.description)
        desc.add(model)
        return desc

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 5))


class FlayedOnes(ElitesUnit, armory.DynastyUnit):
    type_name = get_name(units.FlayedOnes)
    type_id = 'flayed_v1'
    keywords = ['Infantry']
    power = 4

    def __init__(self, parent):
        super(FlayedOnes, self).__init__(parent)
        self.models = Count(self, 'Flayed Ones', 5, 20, get_cost(units.FlayedOnes), True,
                            gear=UnitDescription('Flayed One',
                                                 options=[Gear('Flayer claws')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * ((self.models.cur + 4) / 5)


class Praetorians(ElitesUnit, armory.NecronUnit):
    type_name = get_name(units.TriarchPraetorians)
    type_id = 'praetorians_v1'
    keywords = ['Infantry', 'Fly']
    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Praetorians.Weapon, self).__init__(parent, 'Weapon')
            # costs are similar, so they are added to model cost
            self.variant(*ranged.RodOfCovenant)
            self.variant(join_and(melee.Voidblade, ranged.ParticleCaster),
                         points=get_costs(melee.Voidblade, ranged.ParticleCaster),
                         gear=create_gears(melee.Voidblade, ranged.ParticleCaster))

    def __init__(self, parent):
        super(Praetorians, self).__init__(parent)
        self.models = Count(self, 'Praetorians', 5, 10, get_cost(units.TriarchPraetorians), True)
        self.wep = self.Weapon(self)

    def build_points(self):
        return super(Praetorians, self).build_points() + (self.models.cur - 1) * self.wep.points

    def build_description(self):
        desc = UnitDescription(self.type_name, self.points, self.count)
        model = UnitDescription('Praetorians', 0,
                                self.models.cur,
                                self.wep.description)
        desc.add(model)
        return desc

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * (1 + (self.models.cur > 5))


class TriarchStalker(ElitesUnit, armory.NecronUnit):
    type_name = get_name(units.TriarchStalker)
    type_id = 'TriarchStalker_v1'
    keywords = ['Vehicle']

    power = 8

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TriarchStalker.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HeatRay)
            self.variant(*ranged.ParticleShredder)
            self.variant(*ranged.TwinHeavyGaussCannon)

    def __init__(self, parent):
        super(TriarchStalker, self).__init__(parent, points=get_cost(units.TriarchStalker),
                                      gear=[Gear('Massive forelimbs')])
        self.Weapon(self)


class Nightbringer(ElitesUnit, armory.NecronUnit):
    type_name = get_name(units.Nightbringer)
    type_id = 'nightbringer_v1'
    keywords = ['Character', 'Monster', 'Fly']
    faction = ["C'Tan Shards"]

    power = 12

    def __init__(self, parent):
        super(Nightbringer, self).__init__(parent, points=get_cost(units.Nightbringer),
                                           gear=[Gear('Scythe of the Nightbringer')],
                                           unique=True, static=True)


class Deciever(ElitesUnit, armory.NecronUnit):
    type_name = get_name(units.Deciever)
    type_id = 'deciever_v1'
    keywords = ['Character', 'Monster', 'Fly']
    faction = ["C'Tan Shards"]

    power = 12

    def __init__(self, parent):
        super(Deciever, self).__init__(parent, points=get_cost(units.Deciever),
                                       gear=[Gear('Star-god fists')],
                                       unique=True, static=True)
