__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
from . import armory
from . import ranged, melee, units
from builder.games.wh40k8ed.utils import *


class PlagueCrawler(HeavyUnit, armory.GuardUnit):
    type_name = get_name(units.PlagueburstCrawler)
    type_id = 'plaguecrawler_v1'
    keywords = ['Vehicle', 'Daemon', 'Daemon Engine']
    power = 8

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(PlagueCrawler.Weapon1, self).__init__(parent, 'Weapons')
            self.variant('Two plaguespitters', 2 * get_cost(ranged.Plaguespitter),
                         gear=create_gears(ranged.Plaguespitter, ranged.Plaguespitter))
            self.variant('Two entropy cannons', 2 * get_cost(ranged.EntropyCannon),
                         gear=create_gears(ranged.EntropyCannon, ranged.EntropyCannon))

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(PlagueCrawler.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavySlugger)
            self.variant(*ranged.RothailVolleyGun)

    def __init__(self, parent):
        super(PlagueCrawler, self).__init__(parent, points=points_price(get_cost(units.PlagueburstCrawler),
                                                                        ranged.PlagueburstMortar),
                                            gear=create_gears(ranged.PlagueburstMortar))
        self.Weapon1(self)
        self.Weapon2(self)
