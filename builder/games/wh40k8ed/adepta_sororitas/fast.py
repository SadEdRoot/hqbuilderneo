__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
         OptionsList
from . import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *


class Dominions(FastUnit, armory.SororitasUnit):
    type_name = get_name(units.DominionSquad)
    type_id = 'dominions_v1'
    kwname = 'Dominions'
    keywords = ['Infantry']

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    model = UnitDescription('Dominion', options=common_gear + [Gear('Bolt pistol')])

    class Superior(Unit):
        type_name = 'Dominion Superior'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Dominions.Superior.Weapon1, self).__init__(parent, 'Pistol')
                armory.add_pistol_options(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Dominions.Superior.Weapon2, self).__init__(parent, 'Weapon')
                armory.add_ranged_weapons(self)
                armory.add_melee_weapons(self)

        class MeleeWeapon(OptionsList):
            def __init__(self, parent):
                super(Dominions.Superior.MeleeWeapon, self).__init__(parent, 'Melee weapon', limit=1)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(Dominions.Superior, self).__init__(parent, points=get_cost(units.DominionSquad),
                                                     gear=Dominions.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.MeleeWeapon(self)

    class Dominion(Count):
        @property
        def description(self):
            return Dominions.model.clone().add([Gear('Boltgun')]).\
                set_count(self.cur - sum(c.cur for c in self.parent.wep.counts))

    class SpecialWeapons(object):
        def variant(self, name, point=None):
            self.counts.append(Count(
                self.parent, name, 0, 4, point, gear=Dominions.model.clone().add(Gear(name))))

        def __init__(self, parent):
            self.parent = parent
            self.counts = []
            armory.add_special_weapons(self)
    
    def __init__(self, parent):
        super(Dominions, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=self))
        self.squad = self.Dominion(self, 'Dominion', 4, 9, per_model=True,
                                   points=get_cost(units.DominionSquad))
        self.wep = self.SpecialWeapons(self)
        self.opt = armory.Options(self)

    def check_rules(self):
        super(Dominions, self).check_rules()
        Count.norm_counts(0, 4, self.wep.counts)

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 5 + 4 * (self.squad.cur > 4)


class Seraphims(FastUnit, armory.SororitasUnit):
    type_name = get_name(units.SeraphimSquad)
    type_id = 'seraphims_v1'
    kwname = 'Seraphim'
    keywords = ['Infantry', 'Jump Pack']

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    model = UnitDescription('Seraphim', options=common_gear)

    class Superior(Unit):
        type_name = 'Seraphim Superior'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Seraphims.Superior.Weapon1, self).__init__(parent, 'Pistol')
                self.variant(*ranged.BoltPistol)
                self.variant(*melee.Chainsword)
                self.variant(*melee.PowerSword)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Seraphims.Superior.Weapon2, self).__init__(parent, '')
                self.variant(*ranged.BoltPistol)
                self.variant(*ranged.PlasmaPistol)

        def __init__(self, parent):
            super(Seraphims.Superior, self).__init__(parent, points=get_cost(units.SeraphimSquad),
                                                     gear=Seraphims.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)

    class Seraphim(Count):
        @property
        def description(self):
            return Seraphims.model.clone().add([Gear('Bolt pistol', count=2)]).\
                set_count(self.cur - sum(c.cur for c in self.parent.wep.counts))

    class SpecialWeapons(object):
        def variant(self, name, point=None):
            self.counts.append(Count(
                self.parent, name, 0, 2, 2 * point, gear=Seraphims.model.clone().add(Gear(name, count=2))))

        def __init__(self, parent):
            self.parent = parent
            self.counts = []
            self.variant(*ranged.HandFlamer)
            self.variant(*ranged.InfernoPistol)
    
    def __init__(self, parent):
        super(Seraphims, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=self))
        self.squad = self.Seraphim(self, 'Seraphim', 4, 9,
                                   points=get_cost(units.SeraphimSquad), per_model=True)
        self.wep = self.SpecialWeapons(self)

    def check_rules(self):
        super(Seraphims, self).check_rules()
        Count.norm_counts(0, 2, self.wep.counts)

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 4 + 4 * (self.squad.cur > 4)
