__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import Unit, TroopsUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList,\
    Gear, SubUnit, UnitDescription, Count
from . import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *


class BattleSisters(TroopsUnit, armory.SororitasUnit):
    type_name = get_name(units.BattleSisterSquad)
    type_id = 'bsisters_v1'

    keywords = ['Infantry']

    common_gear = [Gear('Frag grenades'), Gear('Krak grenades')]

    model = UnitDescription('Battle Sister', options=common_gear + [Gear('Bolt pistol')])

    class Superior(Unit):
        type_name = 'Sister Superior'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(BattleSisters.Superior.Weapon1, self).__init__(parent, 'Pistol')
                armory.add_pistol_options(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(BattleSisters.Superior.Weapon2, self).__init__(parent, 'Weapon')
                armory.add_ranged_weapons(self)
                armory.add_melee_weapons(self)

        class MeleeWeapon(OptionsList):
            def __init__(self, parent):
                super(BattleSisters.Superior.MeleeWeapon, self).__init__(parent, 'Melee weapon', limit=1)
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(BattleSisters.Superior, self).__init__(parent, points=get_cost(units.BattleSisterSquad),
                                                         gear=BattleSisters.common_gear)
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.MeleeWeapon(self)

    class Sister(Count):
        @property
        def description(self):
            return BattleSisters.model.clone().add([Gear('Boltgun')]).\
                set_count(self.cur - sum(c.cur != c.bolt for c in [self.parent.wep1, self.parent.wep2]))

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(BattleSisters.SpecialWeapon, self).__init__(parent, 'Special weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_special_weapons(self)

        @property
        def description(self):
            return []

    class HeavyWeapon(OneOf):
        def __init__(self, parent):
            super(BattleSisters.HeavyWeapon, self).__init__(parent, 'Special or heavy weapon')
            self.bolt = self.variant(*ranged.Boltgun)
            armory.add_special_weapons(self)
            armory.add_heavy_weapons(self)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(BattleSisters, self).__init__(parent)
        self.sup = SubUnit(self, self.Superior(parent=self))
        self.squad = self.Sister(self, 'Battle Sisters', 4, 9, per_model=True,
                                 points=get_cost(units.BattleSisterSquad))
        self.wep1 = self.SpecialWeapon(self)
        self.wep2 = self.HeavyWeapon(self)
        self.opt = armory.Options(self)

    def build_description(self):
        res = super(BattleSisters, self).build_description()
        res.add_dup(BattleSisters.model.clone().add(self.wep1.cur.gear))
        res.add_dup(BattleSisters.model.clone().add(self.wep2.cur.gear))
        return res

    def get_count(self):
        return self.squad.cur + 1

    def build_power(self):
        return 4 * ((self.squad.cur + 5) / 5)
