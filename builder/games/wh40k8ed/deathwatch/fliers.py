from builder.games.wh40k8ed.unit import FlierUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList,\
    SubUnit, UnitList
from . import armory
from . import ranged
from . import units
from . import wargear
from builder.games.wh40k8ed.utils import get_cost, get_name, create_gears, points_price


class Corvus(FlierUnit, armory.DeathwatchUnit):
    type_name =get_name(units.CorvusBlackstar)
    type_id = 'corvus_v1'
    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 12

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Corvus.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.TwinAssaultCannon)
            self.variant(*ranged.TwinLascannon)

    class Payload(OneOf):
        def __init__(self, parent):
            super(Corvus.Payload, self).__init__(parent, 'Payload')
            self.variant('Two stormstrike missile launchers', get_cost(ranged.StormstrikeMissileLauncher) * 2,
                         gear=[Gear('Stormstrike missile launcher', count=2)])
            self.variant('Two Blackstar rocket launchers', get_cost(ranged.BlackstarRocketLauncher) * 2,
                         gear=[Gear('Blackstar rocket launcher', count=2)])

    class Options(OptionsList):
        def __init__(self, parent):
            super(Corvus.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HurricaneBolter)
            self.alt = [self.variant(*wargear.InfernumHaloLauncher),
                        self.variant(*wargear.AuspexArray)]

        def check_rules(self):
            super(Corvus.Options, self).check_rules()
            OptionsList.process_limit(self.alt, 1)

    def __init__(self, parent):
        gear = [wargear.BlackstarClusterLauncher]
        cost = points_price(get_cost(units.CorvusBlackstar), *gear)
        super(Corvus, self).__init__(parent, points=cost, gear=create_gears(*gear))
        self.Weapon(self)
        self.Payload(self)
        self.Options(self)
