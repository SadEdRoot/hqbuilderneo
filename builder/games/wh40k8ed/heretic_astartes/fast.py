__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
from . import armory
from . import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class ChaosBikers(FastUnit, armory.CultLegionUnit):
    type_name = get_name(units.ChaosBikers)
    type_id = 'chaos_bikers_v1'

    keywords = ['Biker']
    power = 5

    class Champion(Unit):
        type_name = 'Chaos Biker Champion'

        def __init__(self, parent):
            gear = [wargear.FragGrenades, wargear.KrakGrenades, ranged.CombiBolter]
            cost = points_price(get_cost(units.ChaosBikers), *gear)
            super(ChaosBikers.Champion, self).__init__(parent, gear=create_gears(*gear),
                                                       points=cost)
            self.faction = parent.faction
            armory.ChampionWeapon(self, False)
            self.icon = armory.IconOfChaos(self)

    class ChaosBiker(ListSubUnit):
        type_name = 'Chaos Biker'

        class Pistol(OneOf):
            def __init__(self, parent):
                super(ChaosBikers.ChaosBiker.Pistol, self).__init__(parent, 'Pistol')
                self.variant(*ranged.BoltPistol)
                self.variant(*melee.Chainsword)

        class Gun(OptionsList):
            def __init__(self, parent):
                super(ChaosBikers.ChaosBiker.Gun, self).__init__(parent, 'Special weapon')
                armory.add_special_weapons(self)

        class BikeWeapon(OneOf):
            def __init__(self, parent):
                super(ChaosBikers.ChaosBiker.BikeWeapon, self).__init__(parent, 'Bike weapon')
                self.bolt = self.variant(*ranged.CombiBolter)
                armory.add_special_weapons(self)

        def __init__(self, parent):
            super(ChaosBikers.ChaosBiker, self).__init__(parent, gear=[Gear('Frag grenades'),
                                                                       Gear('Krak grenades')],
                                                         points=get_cost(units.ChaosBikers))
            self.faction = self.root_unit.faction
            self.rng = self.Pistol(self)
            self.gun = self.Gun(self)
            self.bwep = self.BikeWeapon(self)
            self.icon = armory.IconOfChaos(self)

        def check_rules(self):
            super(ChaosBikers.ChaosBiker, self).check_rules()
            self.gun.used = self.gun.visible = self.bwep.cur == self.bwep.bolt

        @ListSubUnit.count_gear
        def has_spec(self):
            return (self.bwep.cur != self.bwep.bolt) or self.gun.any

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

    def __init__(self, parent):
        super(ChaosBikers, self).__init__(parent)
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.ChaosBiker, 2, 8)

    def check_rules(self):
        super(ChaosBikers, self).check_rules()
        icnt = sum(u.has_icon() for u in self.models.units) + self.ldr.unit.icon.any
        if icnt > 1:
            self.error('Only one Chaos Biker may carry Icon of Chaos')
        pcnt = sum(u.has_spec() for u in self.models.units)
        if pcnt > 2:
            self.error('No more then 2 Chaos Bikers may take special weapons; taken: {}'.format(pcnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + 4 * ((self.models.count - 1) / 3)


class Raptors(FastUnit, armory.CultLegionUnit):
    type_name = get_name(units.Raptors)
    type_id = 'raptor_v1'

    keywords = ['Infantry', 'Jump pack', 'Fly']
    power = 6

    class Champion(armory.ClawUser):
        type_name = 'Raptor Champion'

        class Chainsword(armory.ChampionWeapon):
            def add_double(self, sword, fist):
                sword = [self.variant(*melee.Chainsword)]
                super(Raptors.Champion.Chainsword, self).add_double(False, fist)
                self.double += sword

            def __init__(self, parent):
                super(Raptors.Champion.Chainsword, self).__init__(parent, False, False)

        def __init__(self, parent):
            super(Raptors.Champion, self).__init__(parent, gear=[Gear('Frag grenades'),
                                                                 Gear('Krak grenades')],
                                                   points=get_cost(units.Raptors))
            self.faction = parent.faction
            self.wep1 = self.Chainsword(self)
            self.wep2 = armory.ChampionWeapon(self)
            self.icon = armory.IconOfChaos(self)

        def check_rules(self):
            super(Raptors.Champion, self).check_rules()
            armory.ChampionWeapon.ensure_pairs(self.wep1, self.wep2)

    class Raptor(ListSubUnit):
        type_name = 'Raptor'

        class Pistol(OneOf):
            def __init__(self, parent):
                super(Raptors.Raptor.Pistol, self).__init__(parent, 'Weapon')
                self.bp = self.variant(join_and(ranged.BoltPistol, melee.Chainsword),
                                       gear=create_gears(ranged.BoltPistol, melee.Chainsword))
                self.variant(join_and(ranged.PlasmaPistol, melee.Chainsword),
                             get_costs(ranged.PlasmaPistol, melee.Chainsword),
                             gear=create_gears(ranged.PlasmaPistol, melee.Chainsword))
                armory.add_special_weapons(self)

        def __init__(self, parent):
            super(Raptors.Raptor, self).__init__(parent, gear=[Gear('Frag grenades'),
                                                               Gear('Krak grenades')],
                                                 points=get_cost(units.Raptors))
            self.faction = self.root_unit.faction
            self.rng = self.Pistol(self)
            self.icon = armory.IconOfChaos(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.rng.cur != self.rng.bp

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

    def __init__(self, parent):
        super(Raptors, self).__init__(parent)
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.Raptor, 4, 14)

    def check_rules(self):
        super(Raptors, self).check_rules()
        icnt = sum(u.has_icon() for u in self.models.units) + self.ldr.unit.icon.any
        if icnt > 1:
            self.error('Only one Raptor may carry Icon of Chaos')
        pcnt = sum(u.has_spec() for u in self.models.units)
        if pcnt > 2:
            self.error('No more then 2 Raptors may replace bolt pistols; taken: {}'.format(pcnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + 4 * ((self.get_count() - 1) / 5)


class WarpTalons(FastUnit, armory.CultLegionUnit):
    type_name = get_name(units.WarpTalons)
    type_id = 'warp_talons_v1'

    keywords = ['Infantry', 'Daemon', 'Jump Pack', 'Fly']
    power = 7
    member = UnitDescription('Warp Talon', options=[Gear('Lightning claw', count=2)])

    def __init__(self, parent):
        super(WarpTalons, self).__init__(parent, points=get_cost(units.WarpTalons) + 12, gear=[
            UnitDescription('Warp Talon Champion', options=[Gear('Lightning claw', count=2)])])
        self.models = Count(self, 'WarpTalons', 4, 9, points=get_cost(units.WarpTalons) + 12,
                            per_model=True, gear=self.member.clone())

    def get_count(self):
        return self.models.cur + 1

    def build_power(self):
        return 7 + 6 * (self.models.cur > 4)


class ChaosSpawn(FastUnit, armory.CultLegionUnit):
    type_name = get_name(units.ChaosSpawn)
    type_id = 'chaos_spawn_v1'

    keywords = ['Beast']

    member = UnitDescription('Chaos Spawn', options=[Gear('Hideous mutations')])

    @classmethod
    def calc_faction(cls):
        return super(ChaosSpawn, cls).calc_faction() + ['THOUSAND SONS', 'DEATH GUARD']

    def __init__(self, parent):
        super(ChaosSpawn, self).__init__(parent)
        self.models = Count(self, 'Chaos Spawn', 1, 5, points=get_cost(units.ChaosSpawn),
                            per_model=True, gear=self.member.clone())

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 2 * (self.models.cur)
