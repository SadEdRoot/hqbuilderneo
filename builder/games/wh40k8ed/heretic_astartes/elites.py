__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
from . import armory
from . import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class Fallen(ElitesUnit, Unit):
    type_name = get_name(units.Fallen)
    type_id = 'fallen_v1'
    faction = ['Chaos', 'Imperium', 'Fallen']
    keywords = ['Infantry']
    power = 6

    class FallenChampion(armory.ClawUser):
        type_name = 'Fallen Champion'

        def __init__(self, parent):
            super(Fallen.FallenChampion, self).__init__(parent, gear=[Gear('Frag grenades'),
                                                                      Gear('Krak grenades')],
                                                        points=get_cost(units.Fallen))
            self.wep1 = armory.ChampionWeapon(self, True)
            self.wep2 = armory.ChampionWeapon(self)

        def check_rules(self):
            super(Fallen.FallenChampion, self).check_rules()
            armory.ChampionWeapon.ensure_pairs(self.wep1, self.wep2)

    class SingleFallen(ListSubUnit):
        type_name = 'Fallen'

        class Gun(OneOf):
            def __init__(self, parent):
                super(Fallen.SingleFallen.Gun, self).__init__(parent, 'Weapon')
                self.default = [self.variant(*ranged.Boltgun),
                                self.variant(*melee.Chainsword)]
                armory.add_combi_weapons(self)
                armory.add_special_weapons(self)
                armory.add_heavy_weapons(self, fallen=True)

        class Pistol(OneOf):
            def __init__(self, parent):
                super(Fallen.SingleFallen.Pistol, self).__init__(parent, 'Pistol')
                self.variant(*ranged.BoltPistol)
                self.ppist = self.variant(*ranged.PlasmaPistol)

        class Melee(OptionsList):
            def __init__(self, parent):
                super(Fallen.SingleFallen.Melee, self).__init__(parent, 'Melee weapon', limit=1)
                self.claws = self.variant('Two lightning claws', 12, gear=[Gear('Lightning claw', count=2)])
                armory.add_melee_weapons(self)

        def __init__(self, parent):
            super(Fallen.SingleFallen, self).__init__(parent, points=get_cost(units.Fallen))
            self.rng1 = self.Gun(self)
            self.rng2 = self.Pistol(self)
            self.mle = self.Melee(self)

        def check_rules(self):
            super(Fallen.SingleFallen, self).check_rules()
            self.rng1.used = self.rng1.visible =\
                             self.rng2.used = self.rng2.visible =\
                                              not self.mle.claws.value
            self.mle.used = self.mle.visible = (self.rng1.cur in self.rng1.default)\
                            and (self.rng2.cur != self.rng2.ppist)
            self.rng2.ppist.active = (self.rng1.cur in self.rng1.default)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.rng1.used and self.rng1.cur in self.rng1.heavy

        @ListSubUnit.count_gear
        def has_special(self):
            return self.rng1.used and self.rng1.cur in self.rng1.special

        @ListSubUnit.count_gear
        def has_other(self):
            return (self.rng1.cur not in self.rng1.default) or\
                (self.rng2.cur == self.rng2.ppist) or\
                self.mle.any

    def __init__(self, parent):
        super(Fallen, self).__init__(parent)
        self.boss = SubUnit(self, self.FallenChampion(parent=self))
        self.models = UnitList(self, self.SingleFallen, 4, 9)

    def get_count(self):
        return self.models.count + 1

    def build_power(self):
        return self.power + 4 * (self.get_count() > 5)

    def check_rules(self):
        super(Fallen, self).check_rules()
        if sum(u.has_heavy() for u in self.models.units) > 1:
            self.error('No more then one Heavy Weapon may be taken')
        # flag is either o or 1
        sh_flag = any(u.has_heavy() for u in self.models.units) or\
                  any(u.has_special() for u in self.models.units)
        opt = sum(u.has_other() for u in self.models.units) - sh_flag
        if opt > 4:
            self.error('No more then 4 alternate loadouts may be taken in addition to single special or heavy weapon; taken: {}'.format(opt))


class ChaosTerminators(ElitesUnit, armory.CultLegionUnit):
    type_name = get_name(units.ChaosTerminators)
    type_id = 'ctermies_v1'
    keywords = ['Infantry', 'Terminator']
    power = 10

    class Champion(Unit):
        type_name = 'Terminator Champion'

        class Bolter(OneOf):
            def __init__(self, parent):
                super(ChaosTerminators.Champion.Bolter, self).__init__(parent, 'Ranged Weapon')
                armory.add_combi_weapons(self)

        class Melee(OneOf):
            def __init__(self, parent):
                super(ChaosTerminators.Champion.Melee, self).__init__(parent, 'Mele weapon')
                self.variant(*melee.Chainaxe)
                self.pair = self.variant(*melee.LightningClawsPair,
                                         gear=[Gear('Lightning claw', count=2)])
                armory.add_terminator_melee(self)

        def __init__(self, parent):
            super(ChaosTerminators.Champion, self).__init__(parent,
                                                            points=get_cost(units.ChaosTerminators))
            self.faction = parent.faction
            self.rng = self.Bolter(self)
            self.mle = self.Melee(self)
            self.icon = armory.IconOfChaos(self)

        def check_rules(self):
            super(ChaosTerminators.Champion, self).check_rules()
            self.rng.used = self.rng.visible = not self.mle.cur == self.mle.pair

    class Terminator(ListSubUnit):
        type_name = 'Chaos Terminator'

        class Bolter(OneOf):
            def __init__(self, parent):
                super(ChaosTerminators.Terminator.Bolter, self).__init__(parent, 'Ranged Weapon')
                armory.add_combi_weapons(self)
                self.heavy = [self.variant(*ranged.HeavyFlamer),
                              self.variant(*ranged.ReaperAutocannon)]

        class Melee(OneOf):
            def __init__(self, parent):
                super(ChaosTerminators.Terminator.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Chainaxe)
                self.pair = self.variant(*melee.LightningClawsPair,
                                         gear=[Gear('Lightning claw', count=2)])
                armory.add_terminator_melee(self)

        def __init__(self, parent):
            super(ChaosTerminators.Terminator, self).__init__(parent,
                                                              points=get_cost(units.ChaosTerminators))
            self.faction = self.root_unit.faction
            
            self.rng = self.Bolter(self)
            self.mle = self.Melee(self)
            self.icon = armory.IconOfChaos(self)

        def check_rules(self):
            super(ChaosTerminators.Terminator, self).check_rules()
            self.rng.used = self.rng.visible = not self.mle.cur == self.mle.pair

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.rng.cur in self.rng.heavy

    def __init__(self, parent):
        super(ChaosTerminators, self).__init__(parent)
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.Terminator, 4, 9)

    def check_rules(self):
        super(ChaosTerminators, self).check_rules()
        # check mark
        mark_cnt = self.ldr.unit.icon.any + sum(u.has_icon() for u in self.models.units)
        if mark_cnt > 1:
            self.error('Only one model may carry Icon of Chaos')
        heavy_max = 1 + (self.get_count() == 10)
        heavy_cnt = sum(u.has_heavy() for u in self.models.units)
        if heavy_cnt > heavy_max:
            self.error('No more then {} Chaos Terminators may take heavy weapons; taken: {}'.format(heavy_max, heavy_cnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + 14 * (self.models.count > 4)


class Berzerkers(ElitesUnit, armory.LegionUnit):
    type_name = get_name(units.KhorneBerzerkers)
    type_id = 'berz_v1'
    faction = ['Khorne']
    keywords = ['Infantry']
    power = 5

    class Champion(armory.ClawUser):
        type_name = 'Berzerker Champion'

        class Chainsword(armory.ChampionWeapon):
            def add_double(self, sword, fist):
                sword = [self.variant(*melee.Chainsword)]
                super(Berzerkers.Champion.Chainsword, self).add_double(False, fist)
                self.double += sword

            def __init__(self, parent):
                super(Berzerkers.Champion.Chainsword, self).__init__(parent, False, False)

        def __init__(self, parent):
            super(Berzerkers.Champion, self).__init__(parent, points=get_cost(units.KhorneBerzerkers),
                                                      gear=[Gear('Frag grenades'),
                                                            Gear('Krak grenades')])
            self.wep1 = self.Chainsword(self)
            self.wep2 = armory.ChampionWeapon(self)

        def check_rules(self):
            super(Berzerkers.Champion, self).check_rules()
            armory.ChampionWeapon.ensure_pairs(self.wep1, self.wep2)

    class Berzerker(ListSubUnit):
        type_name = 'Khorne Berzerker'

        class Pistol(OneOf):
            def __init__(self, parent):
                super(Berzerkers.Berzerker.Pistol, self).__init__(parent, 'Pistol')
                self.variant(*ranged.BoltPistol)
                self.ppist = self.variant(*ranged.PlasmaPistol)
                self.variant(*melee.Chainaxe)

        class Melee(OneOf):
            def __init__(self, parent):
                super(Berzerkers.Berzerker.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Chainsword)
                self.variant(*melee.Chainaxe)

        class Icon(OptionsList):
            def __init__(self, parent):
                super(Berzerkers.Berzerker.Icon, self).__init__(parent, 'Icon of Chaos')
                self.variant('Icon of Wrath', 10)

        def __init__(self, parent):
            super(Berzerkers.Berzerker, self).__init__(parent, points=get_cost(units.KhorneBerzerkers),
                                                       gear=[Gear('Frag grenades'),
                                                             Gear('Krak grenades')])
            self.rng = self.Pistol(self)
            self.Melee(self)
            self.icon = self.Icon(self)

        @ListSubUnit.count_gear
        def has_pistol(self):
            return self.rng.cur == self.rng.ppist

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

    def __init__(self, parent):
        super(Berzerkers, self).__init__(parent)
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.Berzerker, 4, 19)

    def check_rules(self):
        super(Berzerkers, self).check_rules()
        icnt = sum(u.has_icon() for u in self.models.units)
        if icnt > 1:
            self.error('Only one Khorne Berzerker may carry Icon of Wrath')
        pcnt = sum(u.has_pistol() for u in self.models.units)
        if pcnt > 2:
            self.error('No more then 2 Khorne Berzerkers may take plasma pistols; taken: {}'.format(pcnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + 4 * ((self.models.count - 1) / 5)


class RubricMarines(ElitesUnit, armory.LegionUnit):
    type_name = get_name(units.RubricMarines)
    type_id = 'rubr_v1'
    faction = ['Tzeentch']
    keywords = ['Infantry']
    power = 7
    sorc_points = get_cost(units.RubricMarines)

    class Champion(Unit):
        type_name = 'Aspiring Sorcerer'

        class Melee(OneOf):
            def __init__(self, parent):
                super(RubricMarines.Champion.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.ForceStave)
                self.variant(*melee.ForceAxe)
                self.variant(*melee.ForceSword)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(RubricMarines.Champion.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.InfernoBoltPistol)
                self.variant(*ranged.WarpflamePistol)

        def __init__(self, parent):
            super(RubricMarines.Champion, self).__init__(parent, points=parent.sorc_points)
            self.wep1 = self.Melee(self)
            self.wep2 = self.Ranged(self)

    class RubricMarine(ListSubUnit):
        type_name = 'Rubric Marine'

        class Ranged(OneOf):
            def __init__(self, parent):
                super(RubricMarines.RubricMarine.Ranged, self).__init__(parent, 'Weapon')
                self.variant(*ranged.InfernoBoltgun)
                self.variant(*ranged.Warpflamer)
                self.can = self.variant(*ranged.SoulreaperCannon)

        class Icon(OptionsList):
            def __init__(self, parent):
                super(RubricMarines.RubricMarine.Icon, self).__init__(parent, 'Icon of Chaos')
                self.variant(*wargear.IconOfFlame)

        def __init__(self, parent):
            super(RubricMarines.RubricMarine, self).__init__(parent, points=get_cost(units.RubricMarines))
            self.rng = self.Ranged(self)
            self.icon = self.Icon(self)

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.rng.cur == self.rng.can

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

    def __init__(self, parent):
        super(RubricMarines, self).__init__(parent)
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.RubricMarine, 4, 19)

    def check_rules(self):
        super(RubricMarines, self).check_rules()
        icnt = sum(u.has_icon() for u in self.models.units)
        if icnt > 1:
            self.error('Only one Rubric Marine may carry Icon of Flame')
        canmax = 1 + (self.get_count() == 20)
        cancnt = sum(u.has_heavy() for u in self.models.units)
        if cancnt > canmax:
            self.error('No more then 1 Rubric Marine may take soulreaper cannon unless the unit has 20 models')

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + (20 if self.get_count() > 15 else
                             (14 if self.get_count() > 10 else
                              (7 if self.get_count() > 5 else 0)))


class PlagueMarines(ElitesUnit, armory.LegionUnit):
    type_name = get_name(units.PlagueMarines) + ' (Index)'
    type_id = 'plag_v1'
    faction = ['Nurgle']
    keywords = ['Infantry']
    obsolete = True

    class Champion(Unit):
        type_name = 'Plague Champion'

        class Melee(OneOf):
            def __init__(self, parent):
                super(PlagueMarines.Champion.Melee, self).__init__(parent, 'Bootknife')
                self.variant('Plague knife', 0)
                self.variant('Plaguesword', 3)

        class SpecChamp(armory.ChampionWeapon):
            def __init__(self, parent, first):
                super(PlagueMarines.Champion.SpecChamp, self).__init__(parent, first, fist=first)
                if first:
                    self.pgun = self.variant('Plasma gun', 13)
                else:
                    self.pfist = self.variant('Power fist', 20)

        def __init__(self, parent):
            super(PlagueMarines.Champion, self).__init__(parent, points=21, gear=[Gear('Blight grenades'),
                                                                                  Gear('Krak grenades')])
            self.wep1 = self.SpecChamp(self, True)
            self.wep2 = self.SpecChamp(self, False)
            self.Melee(self)

        def check_rules(self):
            super(PlagueMarines.Champion, self).check_rules()
            armory.ChampionWeapon.ensure_pairs(self.wep1, self.wep2)
            if self.wep1.cur == self.wep1.pgun:
                self.wep2.cur = self.wep2.pfist

        def build_points(self):
            res = super(PlagueMarines.Champion, self).build_points()
            if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
                res -= 5
            return res

    class PlagueMarine(ListSubUnit):
        type_name = 'Plague Marine'

        class Pistol(OneOf):
            def __init__(self, parent):
                super(PlagueMarines.PlagueMarine.Pistol, self).__init__(parent, 'Pistol')
                self.variant('Bolt Pistol', 0)
                self.ppist = self.variant('Plasma pistol', 7)

        class Gun(OneOf):
            def __init__(self, parent):
                super(PlagueMarines.PlagueMarine.Gun, self).__init__(parent, 'Gun')
                self.bgun = self.variant('Boltgun', 0)
                self.lnch = self.variant('Blight launcher', 14)
                armory.add_special_weapons(self)

        class Icon(OptionsList):
            def __init__(self, parent):
                super(PlagueMarines.PlagueMarine.Icon, self).__init__(parent, 'Icon of Chaos')
                self.variant('Icon of Despair', 10)

        def __init__(self, parent):
            super(PlagueMarines.PlagueMarine, self).__init__(parent, points=21, gear=[Gear('Blight grenades'),
                                                                                      Gear('Krak grenades')])
            self.rng = self.Pistol(self)
            self.gun = self.Gun(self)
            self.icon = self.Icon(self)

        def check_rules(self):
            super(PlagueMarines.PlagueMarine, self).check_rules()
            self.rng.ppist.active = self.gun.cur == self.gun.bgun

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.rng.cur == self.rng.ppist or self.gun.cur != self.gun.bgun

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

    def __init__(self, parent):
        super(PlagueMarines, self).__init__(parent, get_name(units.PlagueMarines))
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.PlagueMarine, 4, 19)

    def check_rules(self):
        super(PlagueMarines, self).check_rules()
        icnt = sum(u.has_icon() for u in self.models.units)
        if icnt > 1:
            self.error('Only one Plague Marine may carry Icon of Despair')
        pcnt = sum(u.has_spec() for u in self.models.units)
        if pcnt > 2:
            self.error('No more then 2 Plague Marines may take plasma pistols or replace boltguns; taken: {}'.format(pcnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return 2 + 5 * ((5 + self.models.count) / 5)


class PlagueMarinesV2(ElitesUnit, armory.LegionUnit):
    type_name = get_name(units.PlagueMarines) + ' (Codex)'
    type_id = 'plag_v2'
    faction = ['Nurgle']
    keywords = ['Infantry']
    power = 7

    class Champion(Unit):
        type_name = 'Plague Champion'

        class Melee(OneOf):
            def __init__(self, parent):
                super(PlagueMarinesV2.Champion.Melee, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.PlagueKnife)
                self.variant(*melee.Plaguesword)

        class Ranged(OneOf):
            def __init__(self, parent):
                super(PlagueMarinesV2.Champion.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.Boltgun)
                self.variant(*ranged.BoltPistol)
                self.variant(*ranged.PlasmaPistol)
                self.variant(*ranged.PlasmaGun)

        class Fist(OptionsList):
            def __init__(self, parent):
                super(PlagueMarinesV2.Champion.Fist, self).__init__(parent, '')
                self.variant(*melee.PowerFist)

        def __init__(self, parent):
            super(PlagueMarinesV2.Champion, self).__init__(parent, points=get_cost(units.PlagueMarines),
                                                         gear=[Gear('Blight grenades'),
                                                               Gear('Krak grenades')])
            self.wep1 = self.Ranged(self)
            self.wep2 = self.Melee(self)
            self.Fist(self)

    class PlagueMarine(ListSubUnit):
        type_name = 'Plague Marine'

        class Gun(OneOf):
            def __init__(self, parent):
                super(PlagueMarinesV2.PlagueMarine.Gun, self).__init__(parent, 'Gun')
                self.bgun = self.variant(*ranged.Boltgun)
                self.spec = [self.variant(*ranged.PlagueSpewer),
                             self.variant(*ranged.PlagueBelcher),
                             self.variant(*ranged.BlightLauncher),
                             self.variant(*ranged.Meltagun),
                             self.variant(*ranged.PlasmaGun)]
                self.variant(*melee.BuboticAxe)
                self.pknife = self.variant(*melee.PlagueKnife)
                self.pair = self.variant(join_and(melee.MaceOfContagion, melee.BuboticAxe),
                                         get_costs(melee.MaceOfContagion, melee.BuboticAxe),
                                         gear=create_gears(melee.MaceOfContagion, melee.BuboticAxe))
                self.melee = [self.variant(*melee.GreatPlagueCleaver),
                              self.variant(*melee.FlailOfCorruption)]
                
        class Icon(OptionsList):
            def __init__(self, parent):
                super(PlagueMarinesV2.PlagueMarine.Icon, self).__init__(parent, 'Icon of Chaos')
                self.variant('Icon of Despair', 10)

        def __init__(self, parent):
            gear = [wargear.BlightGrenades, wargear.KrakGrenades, melee.PlagueKnife]
            cost = points_price(get_cost(units.PlagueMarines), *gear)
            super(PlagueMarinesV2.PlagueMarine, self).__init__(parent, points=cost,
                                                               gear=create_gears(*gear))
            self.gun = self.Gun(self)
            self.icon = self.Icon(self)

        def check_rules(self):
            super(PlagueMarinesV2.PlagueMarine, self).check_rules()
            self.icon.visible = self.icon.used = self.gun.cur in [self.gun.bgun, self.gun.pknife]

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.gun.cur in self.gun.spec

        @ListSubUnit.count_gear
        def has_pair(self):
            return self.gun.cur == self.gun.pair

        @ListSubUnit.count_gear
        def has_melee(self):
            return self.gun.cur in self.gun.melee

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.used and self.icon.any

    def __init__(self, parent):
        super(PlagueMarinesV2, self).__init__(parent, get_name(units.PlagueMarines))
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.PlagueMarine, 4, 19)

    def check_rules(self):
        super(PlagueMarinesV2, self).check_rules()
        icnt = sum(u.has_icon() for u in self.models.units)
        if icnt > 1:
            self.error('Only one Plague Marine may carry Icon of Despair')
        pcnt = sum(u.has_spec() for u in self.models.units)
        if pcnt > 2:
             self.error('No more then 2 Plague Marines may take special weapons; taken: {}'.format(pcnt))
        paircnt = sum(u.has_pair() for u in self.models.units)
        if paircnt > 2:
            self.error('No more then 2 Plague Marines may take mace of contagion and bubotic axe; taken: {}'.format(paircnt))
        mcnt = sum(u.has_melee() for u in self.models.units)
        if mcnt > 2:
            self.error('No more then 2 Plague Marines may take great plague cleaver or a flail of corruption; taken: {}'.format(mcnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + (16 if self.get_count() > 15 else
                             (11 if self.get_count() > 10 else
                              (6 if self.get_count() > 7 else
                               (3 if self.get_count() > 5 else 0))))


class NoiseMarines(ElitesUnit, armory.LegionUnit):
    type_name = get_name(units.NoiseMarines)
    type_id = 'noise_v1'
    faction = ['Slaanesh']
    keywords = ['Infantry']
    power = 6

    class Champion(armory.ClawUser):
        type_name = 'Noise Champion'

        class Syren(OptionsList):
            def __init__(self, parent):
                super(NoiseMarines.Champion.Syren, self).__init__(parent, 'Options')
                self.variant(*ranged.DoomSiren)

        class NoiseChampionWeapon(armory.ChampionWeapon):
            def add_single(self):
                self.single = [
                    self.variant(*ranged.Boltgun),
                    self.variant(*ranged.SonicBlaster),
                    self.variant(*ranged.CombiBolter),
                    self.variant(*ranged.CombiFlamer),
                    self.variant(*ranged.CombiMelta),
                    self.variant(*ranged.CombiPlasma)
                ]

        def __init__(self, parent):
            super(NoiseMarines.Champion, self).__init__(parent, points=get_cost(units.NoiseMarines),
                                                        gear=[Gear('Frag grenades'),
                                                              Gear('Krak grenades')])
            self.wep1 = self.NoiseChampionWeapon(self, True)
            self.wep2 = self.NoiseChampionWeapon(self)
            self.Syren(self)

        def check_rules(self):
            super(NoiseMarines.Champion, self).check_rules()
            armory.ChampionWeapon.ensure_pairs(self.wep1, self.wep2)

    class NoiseMarine(ListSubUnit):
        type_name = 'Noise Marine'

        class Gun(OneOf):
            def __init__(self, parent):
                super(NoiseMarines.NoiseMarine.Gun, self).__init__(parent, 'Gun')
                self.variant(*ranged.Boltgun)
                self.variant(*melee.Chainsword)
                self.variant(*ranged.SonicBlaster)
                self.bm = self.variant(*ranged.Blastmaster)

        class Icon(OptionsList):
            def __init__(self, parent):
                super(NoiseMarines.NoiseMarine.Icon, self).__init__(parent, 'Icon of Chaos')
                self.variant('Icon of Excess', 10)

        def __init__(self, parent):
            gear = [wargear.FragGrenades, wargear.KrakGrenades, ranged.BoltPistol]
            cost = points_price(get_cost(units.NoiseMarines), *gear)
            super(NoiseMarines.NoiseMarine, self).__init__(parent, points=cost,
                                                           gear=create_gears(*gear))
            self.rng = self.Gun(self)
            self.icon = self.Icon(self)

        @ListSubUnit.count_gear
        def has_blast(self):
            return self.rng.cur == self.rng.bm

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

    def __init__(self, parent):
        super(NoiseMarines, self).__init__(parent)
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.NoiseMarine, 4, 19)

    def check_rules(self):
        super(NoiseMarines, self).check_rules()
        icnt = sum(u.has_icon() for u in self.models.units)
        if icnt > 1:
            self.error('Only one Noise Marine may carry Icon of Excess')
        bm_max = 1 + (self.get_count() >= 10)
        bm_cnt = sum(u.has_blast() for u in self.models.units)
        if bm_cnt > bm_max:
            self.error('No more then {} Noise Marines may take blastmaster; taken: {}'.format(bm_max, bm_cnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power + (13 if self.models.count > 14 else
                             (9 if self.models.count > 9 else
                              (5 if self.models.count > 4 else 0)))


class Chosen(ElitesUnit, armory.CultLegionUnit):
    type_name = get_name(units.Chosen)
    type_id = 'chosen_v1'

    keywords = ['Infantry']
    power = 7

    class ChosenChampion(armory.ClawUser):
        type_name = 'Chosen Champion'

        def __init__(self, parent):
            super(Chosen.ChosenChampion, self).__init__(parent, gear=[Gear('Frag grenades'),
                                                                      Gear('Krak grenades')],
                                                        points=get_cost(units.Chosen))
            self.faction = parent.faction
            self.wep1 = armory.ChampionWeapon(self, True)
            self.wep2 = armory.ChampionWeapon(self)
            self.icon = armory.IconOfChaos(self)

        def check_rules(self):
            super(Chosen.ChosenChampion, self).check_rules()
            armory.ChampionWeapon.ensure_pairs(self.wep1, self.wep2)

    class SingleChosen(ListSubUnit):
        type_name = 'Chosen'

        class Gun(OneOf):
            def __init__(self, parent):
                super(Chosen.SingleChosen.Gun, self).__init__(parent, 'Weapon')
                self.default = [self.variant(*ranged.Boltgun),
                                self.variant(*melee.Chainsword)]
                armory.add_combi_weapons(self)
                armory.add_special_weapons(self)
                armory.add_heavy_weapons(self)
                armory.add_melee_weapons(self)

        class Pistol(OneOf):
            def __init__(self, parent):
                super(Chosen.SingleChosen.Pistol, self).__init__(parent, 'Pistol')
                self.bpist = self.variant(*ranged.BoltPistol)
                self.ppist = self.variant(*ranged.PlasmaPistol)
                self.pair = self.variant('Pair of lightning claws', 12,
                                         gear=[Gear('Lightning claw', count=2)])

        def __init__(self, parent):
            super(Chosen.SingleChosen, self).__init__(parent, gear=[Gear('Frag grenades'),
                                                                    Gear('Krak grenades')],
                                                      points=get_cost(units.Chosen))
            self.faction = self.root_unit.faction
            self.rng1 = self.Gun(self)
            self.rng2 = self.Pistol(self)
            self.icon = armory.IconOfChaos(self)

        def check_rules(self):
            super(Chosen.SingleChosen, self).check_rules()
            self.rng1.used = self.rng1.visible = not self.rng2.cur == self.rng2.pair
            self.rng2.ppist.active = (self.rng1.cur in self.rng1.default)

        @ListSubUnit.count_gear
        def has_icon(self):
            return self.icon.any

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.rng1.used and self.rng1.cur in self.rng1.heavy

        @ListSubUnit.count_gear
        def has_special(self):
            return self.rng1.used and self.rng1.cur in self.rng1.special

        @ListSubUnit.count_gear
        def has_other(self):
            return ((self.rng1.cur not in self.rng1.default) and (self.rng1.cur not in self.rng1.heavy)) or\
                (self.rng2.cur != self.rng2.bpist)

    def __init__(self, parent):
        super(Chosen, self).__init__(parent)
        self.boss = SubUnit(self, self.ChosenChampion(parent=self))
        self.models = UnitList(self, self.SingleChosen, 4, 9)

    def get_count(self):
        return self.models.count + 1

    def build_power(self):
        return self.power + 4 * (self.get_count() > 5)

    def check_rules(self):
        super(Chosen, self).check_rules()
        if sum(u.has_icon() for u in self.models.units) + self.boss.unit.icon.any > 1:
            self.error('Only one model may carry an Icon of Chaos')
        if sum(u.has_heavy() for u in self.models.units) > 1:
            self.error('No more then one Heavy Weapon may be taken')
        # flag is either o or 1
        sh_flag = any(u.has_heavy() for u in self.models.units) or\
                  any(u.has_special() for u in self.models.units)
        opt = sum(u.has_other() for u in self.models.units) - sh_flag
        if opt > 4:
            self.error('No more then 4 alternate loadouts may be taken in addition to single special or heavy weapon; taken: {}'.format(opt))


class Possessed(ElitesUnit, armory.CultLegionUnit):
    type_name = get_name(units.Possessed)
    type_id = 'possessed_v1'

    keywords = ['Infantry', 'Daemon']
    power = 6
    member = UnitDescription('Possessed', options=[Gear('Horrifying mutations')])

    @classmethod
    def calc_faction(cls):
        return super(Possessed, cls).calc_faction() + ['DEATH GUARD']

    def __init__(self, parent):
        super(Possessed, self).__init__(parent,)
        self.models = Count(self, 'Possessed', 5, 15, points=get_cost(units.Possessed),
                            per_model=True)
        self.icon = armory.IconOfChaos(self)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power + 5 * ((self.models.cur - 1) / 5)

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        if self.icon.any:
            res.add(self.member.clone().add(self.icon.description))
        res.add(self.member.clone().set_count(self.models.cur - self.icon.any))
        return res


class Helbrute(ElitesUnit, armory.CultLegionUnit):
    type_name = get_name(units.Helbrute)
    type_id = 'helbrute_v1'
    keywords = ['Vehicle']
    power = 7

    @classmethod
    def calc_faction(cls):
        return super(Helbrute, cls).calc_faction() + ['THOUSAND SONS', 'DEATH GUARD']

    def __init__(self, parent):
        super(Helbrute, self).__init__(parent=parent, points=get_cost(units.Helbrute), gear=[])
        self.wep1 = self.Weapon(self, 'Weapon')
        self.build1 = self.BuiltIn(self)
        self.wep2 = self.Fist(self, '')
        self.build2 = self.BuiltIn(self)

    def build_points(self):
        res = super(Helbrute, self).build_points()
        if self.wep1.has_fist() and self.wep2.has_fist():
            res -= 2 * get_cost(melee.HelbruteFist) - get_cost(melee.HelbruteFistPair)
        return res

    def check_rules(self):
        super(Helbrute, self).check_rules()
        self.build1.visible = self.build1.used = self.wep1.has_fist()
        self.build2.visible = self.build2.used = self.wep2.has_fist()
        self.wep1.activate_missile(not self.wep2.has_missile())
        self.wep2.activate_missile(not self.wep1.has_missile())

    class Fist(OneOf):
        def __init__(self, parent, name):
            super(Helbrute.Fist, self).__init__(parent=parent, name=name)

            self.fist = self.powerfist = self.variant(*melee.HelbruteFist)
            self.thunderhammer = self.variant(*melee.HelbruteHammer)
            self.powerscourge = self.variant(*melee.PowerScourge)
            self.missilelauncher = self.variant(*ranged.MissileLauncher)

        def has_fist(self):
            return self.cur == self.powerfist

        def has_missile(self):
            return self.cur == self.missilelauncher

        def activate_missile(self, val):
            self.missilelauncher.active = val

    class BuiltIn(OptionsList):
        def __init__(self, parent):
            super(Helbrute.BuiltIn, self).__init__(parent=parent, name='', limit=1)

            self.combibolter = self.variant(*ranged.CombiBolter)
            self.heavyflamer = self.variant(*ranged.HeavyFlamer)

    class Melta(OneOf):
        def __init__(self, parent, name):
            super(Helbrute.Melta, self).__init__(parent=parent, name=name)

            self.multimelta = self.variant(*ranged.MultiMelta)

    class Heavy(OneOf):
        def __init__(self, parent, name):
            super(Helbrute.Heavy, self).__init__(parent=parent, name=name)
            self.twinlinkedheavybolter = self.variant(*ranged.TwinHeavyBolter)
            self.twinlinkedlascannon = self.variant(*ranged.TwinLascannon)
            self.plasmacannon = self.variant(*ranged.HelbrutePlasmaCannon)
            self.reaperautocannon = self.variant(*ranged.ReaperAutocannon)

    class Weapon(Heavy, Fist, Melta):
        pass


class Mutilators(ElitesUnit, armory.CultLegionUnit):
    type_name = get_name(units.Mutilators)
    type_id = 'mutilators_v1'
    keywords = ['Infantry', 'Cult of Destruction', 'Daemon']
    power = 7

    def __init__(self, parent):
        super(Mutilators, self).__init__(parent, static=True, points=3 * get_cost(units.Mutilators),
                                         gear=[UnitDescription('Mutilator', count=3,
                                                               options=[Gear('Fleshmetal weapons')])])

    def get_count(self):
        return 3


class GreaterPossessed(ElitesUnit, armory.CultLegionUnit):
    type_name = get_name(units.GreaterPossessed)
    type_id = 'greater_possessed_v1'
    keywords = ['Infantry', 'Charater', 'Daemon']
    power = 4

    def __init__(self, parent):
        super(GreaterPossessed, self).__init__(parent)
        self.models = Count(self, get_name(units.GreaterPossessed), 1, 2,
                            get_cost(units.GreaterPossessed), per_model=True,
                            gear=UnitDescription(get_name(units.GreaterPossessed), options=create_gears(melee.DaemonicMutations)))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.models.cur


class DarkDisciples(ElitesUnit, armory.CultLegionUnit):
    type_name = get_name(units.DarkDisciples)
    type_id = 'dark_disciples_v1'
    keywords = ['Infantry']

    power = 1

    def __init__(self, parent):
        super(DarkDisciples, self).__init__(parent, points=get_cost(units.DarkDisciples), gear=create_gears(melee.CloseCombatWeapon), static=True)

    def get_count(self):
        return 2
