__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TransportUnit, Unit
from builder.games.wh40k8ed.options import OneOf, OptionsList,\
    Gear, SubUnit, UnitDescription, Count
from . import armory, melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class Raider(TransportUnit, armory.SharedUnit):
    type_name = 'Raider'
    type_id = 'raider_v1'

    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 5

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Raider.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.DarkLance)
            self.variant(*ranged.DisintegratorCannon)

    def __init__(self, parent):
        super(Raider, self).__init__(parent, points=get_cost(units.Raider), gear=[Gear('Bladevanes')])
        self.Weapon(self)
        armory.Equipment(self)


class Venom(TransportUnit, armory.SharedUnit):
    type_name = 'Venom'
    type_id = 'venom_v1'

    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 4

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Venom.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.TwinSplinterRifle)
            self.variant(*ranged.SplinterCannon)

    def __init__(self, parent):
        super(Venom, self).__init__(parent, points=points_price(get_cost(units.Venom), ranged.SplinterCannon),
                                    gear=[Gear('Splinter cannon'), Gear('Bladevanes')])
        self.Weapon(self)
        armory.Equipment(self, venom=True)
