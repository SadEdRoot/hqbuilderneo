from builder.games.wh40k8ed.unit import FortUnit
from builder.games.wh40k8ed.options import Count
from . import armory, units
from builder.games.wh40k8ed.utils import *


class FeculentGnarlmaws(FortUnit, armory.NurgleUnit):
    type_name = get_name(units.FeculentGnarlmaws)
    type_id = 'feculent_gnarlmaws_v1'
    power = 3

    def __init__(self, parent):
        super(FeculentGnarlmaws, self).__init__(parent)

        self.models = Count(self, 'Feculent Gnarlmaws', 1, 3, points=get_cost(units.FeculentGnarlmaws), per_model=True)

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * self.get_count()
