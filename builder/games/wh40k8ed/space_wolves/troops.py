from builder.games.wh40k8ed.space_marines.troops import Intercessors
from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList, OptionalSubUnit
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class WGPackLeader(armory.WolfClawUser, Unit):
    type_name = get_name(units.WolfGuardPackLeader)
    power = 2
    model_points = get_cost(units.WolfGuardPackLeader)

    class WeaponMelee(OneOf):
        def __init__(self, parent):
            super(WGPackLeader.WeaponMelee, self).__init__(parent, 'Melee weapon')
            self.variant('Chainsword')
            self.variant(*ranged.PlasmaPistol)
            self.variant(*wargear.StormShield)
            armory.add_melee_weapons(self, chain=False)

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(WGPackLeader.WeaponRanged, self).__init__(parent, 'Ranged weapon')
            self.variant('Bolt pistol')
            self.variant(*ranged.PlasmaPistol)
            armory.add_combi_weapons(self)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(WGPackLeader, self).__init__(
            parent, points=self.model_points, gear=[Gear('Frag grenades'), Gear('Krak grenades')])
        self.wep1 = self.WeaponRanged(self)
        self.wep2 = self.WeaponMelee(self)


class WGTermoPackLeader(armory.WolfClawUser, Unit):
    type_name = get_name(units.WolfGuardTerminatorPack)
    power = 3
    model_points = get_cost(units.WolfGuardTerminatorPack)

    class WeaponMelee(OneOf):
        def __init__(self, parent):
            super(WGTermoPackLeader.WeaponMelee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.PowerSword)
            self.variant(*wargear.StormShield)
            armory.add_term_melee_weapons(self, sword=False)

    class WeaponRanged(OneOf):
        def __init__(self, parent):
            super(WGTermoPackLeader.WeaponRanged, self).__init__(parent, 'Ranged weapon')
            armory.add_combi_weapons(self)
            armory.add_term_melee_weapons(self)

    def __init__(self, parent):
        super(WGTermoPackLeader, self).__init__(parent, points=self.model_points)
        self.wep1 = self.WeaponRanged(self)
        self.wep2 = self.WeaponMelee(self)


class BloodClaws(TroopsUnit, armory.SWUnit):
    type_name = get_name(units.BloodClaws)
    type_id = "blood_claws_v1"
    keywords = ['Infantry']
    model_points = get_cost(units.BloodClaws)

    class Marine(ListSubUnit):
        type_name = 'Blood Claw'

        class Ranged(OneOf):
            def __init__(self, parent):
                super(BloodClaws.Marine.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant('Bolt pistol')
                self.ppist = self.variant(*ranged.PlasmaPistol)

        class Melee(OneOf):
            def __init__(self, parent):
                super(BloodClaws.Marine.Melee, self).__init__(parent, 'Melee weapon')
                self.sword = self.variant('Chainsword')
                armory.add_special_weapons(self)

        def __init__(self, parent):
            super(BloodClaws.Marine, self).__init__(
                parent=parent, points=BloodClaws.model_points, gear=[
                    Gear('Frag Grenades'),
                    Gear('Krak Grenades')
                ]
            )
            self.wep1 = self.Melee(self)
            self.wep2 = self.Ranged(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep1.cur != self.wep1.sword

        @ListSubUnit.count_gear
        def has_pistol(self):
            return self.wep2.cur == self.wep2.ppist

    class Leader(Unit):
        type_name = 'Blood Claw Pack Leader'

        class Ranged(OneOf):
            def __init__(self, parent):
                super(BloodClaws.Leader.Ranged, self).__init__(parent, 'Ranged weapon')
                self.variant('Bolt pistol')
                self.ppist = self.variant(*ranged.PlasmaPistol)

        class Melee(OneOf):
            def __init__(self, parent):
                super(BloodClaws.Leader.Melee, self).__init__(parent, 'Melee weapon')
                self.sword = self.variant('Chainsword')
                self.variant(*melee.PowerSword)
                self.variant(*melee.PowerAxe)
                self.variant(*melee.PowerFist)

        def __init__(self, parent):
            super(BloodClaws.Leader, self).__init__(parent, gear=[
                Gear('Frag Grenades'),
                Gear('Krak Grenades')
            ], points=BloodClaws.model_points)
            self.Melee(self)
            self.Ranged(self)

    class Uncle(OptionalSubUnit):
        def __init__(self, parent):
            super(BloodClaws.Uncle, self).__init__(parent, 'Wolf Guard Pack Leader', limit=1)
            self.pwg = SubUnit(self, WGPackLeader(parent=parent))
            self.twg = SubUnit(self, WGTermoPackLeader(parent=parent))

    def __init__(self, parent):
        super(BloodClaws, self).__init__(parent)
        SubUnit(self, self.Leader(parent=self))
        self.marines = UnitList(self, self.Marine, 4, 14)
        self.wg = self.Uncle(self)

    def check_rules(self):
        super(BloodClaws, self).check_rules()

        if sum(u.has_pistol() for u in self.marines.units) > 1:
            self.error('Only one Blood Claw may upgrade his Bolt Pistol')
        spec_limit = 1 if self.get_count() < 15 else 2
        taken = sum(u.has_spec() for u in self.marines.units)
        if taken > spec_limit:
            self.error('Only {} Blood Claws may have special weapon; taken {}'.format(spec_limit, taken))

    def get_count(self):
        return self.marines.count + 1 + self.wg.count

    def build_power(self):
        return 1 + 3 * ((self.marines.count + 5) / 5) +\
            sum(sunit.unit.power for sunit in self.wg.units if sunit.used)


class GreyHunters(TroopsUnit, armory.SWUnit):
    type_name = "Grey Hunters"
    type_id = "grey_hunters_v1"
    keywords = ['Infantry']
    model_points = get_cost(units.GreyHunters)

    class Marine(ListSubUnit):
        type_name = 'Grey Hunter'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(GreyHunters.Marine.Weapon1, self).__init__(parent, 'Pistol')
                self.variant('Bolt pistol')
                self.ppist = self.variant(*ranged.PlasmaPistol)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(GreyHunters.Marine.Weapon2, self).__init__(parent, 'Gun')
                self.bgun = self.variant('Boltgun')
                armory.add_special_weapons(self)

        class Options(OptionsList):
            def __init__(self, parent):
                super(GreyHunters.Marine.Options, self).__init__(parent, 'Options')
                self.variant('Chainsword')
                self.wolfstandard = self.variant(*wargear.WolfStandard)

        def __init__(self, parent):
            super(GreyHunters.Marine, self).__init__(
                parent=parent, gear=[
                    Gear('Frag Grenades'),
                    Gear('Krak Grenades')
                ], points=GreyHunters.model_points
            )
            self.wep1 = self.Weapon1(self)
            self.wep2 = self.Weapon2(self)
            self.opt = self.Options(self)

        @ListSubUnit.count_gear
        def has_spec(self):
            return self.wep2.cur != self.wep2.bgun

        @ListSubUnit.count_gear
        def has_pistol(self):
            return self.wep1.cur == self.wep1.ppist

        @ListSubUnit.count_gear
        def has_banner(self):
            return self.opt.wolfstandard.value

    class Leader(Unit):
        type_name = 'Grey Hunters Pack leader'

        class Melee(OptionsList):
            def __init__(self, parent):
                super(GreyHunters.Leader.Melee, self).__init__(parent, 'Melee weapon', limit=1)
                self.variant('Chainsword')
                self.variant(*melee.PowerAxe)
                self.variant(*melee.PowerFist)
                self.variant(*melee.PowerSword)

        def __init__(self, parent):
            super(GreyHunters.Leader, self).__init__(parent, gear=[
                Gear('Frag grenades'), Gear('Krak grenades'),
                Gear('Bolt pistol'), Gear('Boltgun')
            ], points=GreyHunters.model_points)
            self.Melee(self)

    class Uncle(OptionalSubUnit):
        def __init__(self, parent):
            super(GreyHunters.Uncle, self).__init__(parent, 'Wolf Guard Pack Leader', limit=1)
            self.pwg = SubUnit(self, WGPackLeader(parent=self))
            self.twg = SubUnit(self, WGTermoPackLeader(parent=self))
    
    def __init__(self, parent):
        super(GreyHunters, self).__init__(parent)
        SubUnit(self, self.Leader(parent=self))
        self.marines = UnitList(self, self.Marine, 4, 9)
        self.wg = self.Uncle(self)

    def check_rules(self):
        super(GreyHunters, self).check_rules()

        if sum(u.has_pistol() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunter may upgrade his Bolt Pistol')
        spec_limit = 1 if self.get_count() < 10 else 2
        if sum(u.has_spec() for u in self.marines.units) > spec_limit:
            self.error('Only {} Grey Hunters may take a special weapon'.format(spec_limit))
        if sum(u.has_banner() for u in self.marines.units) > 1:
            self.error('Only one Grey Hunter may have Wolf Standard')

    def get_count(self):
        return self.marines.count + 1 + self.wg.count

    def build_power(self):
        return 4 + 4 * (self.marines.count > 4) +\
            sum(sunit.unit.power for sunit in self.wg.units if sunit.used)


class SWIntercessors(TroopsUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Intercessors)
    type_id = 'sw_intercessors_v1'
    keywords = ['Infantry', 'Primaris']
    gearlist = [ranged.FragGrenades, ranged.KrakGrenades, ranged.BoltPistol]

    swords = [melee.PowerSword]

    power = 5

    class Sergeant(OptionsList):
        def __init__(self, parent):
            super(SWIntercessors.Sergeant, self).__init__(parent, 'Sergeant weapons')
            self.gl = self.variant(*wargear.AuxilaryGrenadeLauncher)
            self.swords = self.variant(*melee.Chainsword)

        @property
        def description(self):
            return [UnitDescription('Intercessor Sergeant', options=create_gears(*SWIntercessors.gearlist)).
                    add(self.parent.wep.cur.gear).add(super(SWIntercessors.Sergeant, self).description)]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SWIntercessors.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*ranged.BoltRifle)
            self.variant(*ranged.AutoBoltRifle)
            self.variant(*ranged.StalkerBoltRifle)

        @property
        def description(self):
            return []

    class Marines(Count):
        @property
        def description(self):
            return [UnitDescription('Intercessor', options=create_gears(*SWIntercessors.gearlist)).
                    add(self.parent.wep.cur.gear).set_count(self.cur - self.parent.aux.cur)]

    class AuxGL(Count):
        @property
        def description(self):
            return [UnitDescription('Intercessor', options=create_gears(*SWIntercessors.gearlist)).
                    add(self.parent.wep.cur.gear).add(create_gears(wargear.AuxilaryGrenadeLauncher)).
                    set_count(self.cur)]
    
    def __init__(self, parent):
        super(SWIntercessors, self).__init__(parent, points=get_cost(units.Intercessors))
        self.sarge = self.Sergeant(self)
        self.wep = self.Weapon(self)
        self.marines = self.Marines(self, 'SWIntercessors', 4, 9, get_cost(units.Intercessors),
                                    per_model=True)
        self.aux = self.AuxGL(self, 'Auxilary grenade launchers', 0, 1, get_costs(wargear.AuxilaryGrenadeLauncher))

    def get_count(self):
        return 1 + self.marines.cur

    def check_rules(self):
        super(SWIntercessors, self).check_rules()
        self.aux.max = (self.get_count() / 5) - self.sarge.gl.value

    def build_points(self):
        return super(SWIntercessors, self).build_points() + self.wep.points * self.marines.cur

    def build_power(self):
        return self.power * (1 + (self.marines.cur > 4))


class SWInfiltratorSquad(TroopsUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.InfiltratorSquad)
    type_id = 'sw_infiltrator_squad_v1'
    keywords = ['Infantry', 'Phobos', 'Primaris']
    power = 5
    model_gear = [ranged.MarksmanBoltCarbine, ranged.BoltPistol, ranged.FragGrenades, ranged.KrakGrenades]


    class Doctor(OptionsList):
        def __init__(self, parent):
            super(SWInfiltratorSquad.Doctor, self).__init__(parent, 'Specialists', limit=1)
            doctor_cost = points_price(get_cost(units.HelixAdept), *InfiltratorSquad.model_gear)
            doc = UnitDescription(get_name(units.HelixAdept), options=create_gears(*parent.model_gear))
            self.variant(get_name(units.HelixAdept), doctor_cost,
                         gear=[doc])
            radist_price = points_price(get_cost(units.InfiltratorSquad), wargear.InfiltratorCommsArray, *InfiltratorSquad.model_gear)
            radist = UnitDescription('Infiltrator', options=create_gears(wargear.InfiltratorCommsArray, *InfiltratorSquad.model_gear))
            self.variant('Infiltrator with Comms array', radist_price, gear=[radist])

    def __init__(self, parent):
        model_cost = points_price(get_cost(units.InfiltratorSquad), *self.model_gear)
        sarge = UnitDescription('Infiltrator Sergeant', options=create_gears(*self.model_gear))
        trooper = UnitDescription('Infiltrator', options=create_gears(*self.model_gear))
        super(SWInfiltratorSquad, self).__init__(parent, name=get_name(units.InfiltratorSquad),
                                                 points=model_cost,
                                                 gear=[sarge.clone()])
        self.models = Count(self, 'Infiltrators', 4, 9, model_cost, per_model=True, gear=trooper.clone())
        self.doc = self.Doctor(self)

    def get_count(self):
        return 1 + self.models.cur + self.doc.any

    def build_power(self):
        return self.power + 6 * (self.get_count() > 5)
