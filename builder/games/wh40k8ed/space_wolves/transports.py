from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList
from builder.games.wh40k8ed.utils import *
from . import armory, units, ranged, wargear


class SWRhino(TransportUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Rhino)
    type_id = 'sw_rhino_v1'

    keywords = ['Vehicle', 'Transport']
    power = 4

    class Options(OptionsList):
        def __init__(self, parent):
            super(SWRhino.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.StormBolter)

    def __init__(self, parent):
        gear = [ranged.StormBolter]
        super(SWRhino, self).__init__(
            parent, get_name(units.Rhino), gear=create_gears(*gear),
            points=points_price(get_cost(units.Rhino), *gear))
        self.Options(self)


class SWRazorback(TransportUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Razorback)
    type_id = 'sw_razorback_v1'

    keywords = ['Vehicle', 'Transport']
    power = 5

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SWRazorback.Weapon, self).__init__(parent=parent, name='Weapon')
            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)
            self.variant(*ranged.TwinAssault)

    def __init__(self, parent):
        super(SWRazorback, self).__init__(parent=parent, name=get_name(units.Razorback),
                                          points=get_cost(units.Razorback))
        self.Weapon(self)
        SWRhino.Options(self)


class SWDropPod(TransportUnit, armory.SWUnit):
    type_id = 'sw_drop_pod_v1'
    type_name = 'Space Wolves ' + get_name(units.DropPod)

    keywords = ['Vehicle', 'Transport']
    power = 5

    class Weapon(OneOf):
        def __init__(self, parent):
            super(SWDropPod.Weapon, self).__init__(parent, 'Weapon')
            self.sbgun = self.variant(*ranged.StormBolter)
            self.dwind = self.variant(*ranged.DeathwindLauncher)

    def __init__(self, parent):
        super(SWDropPod, self).__init__(parent=parent, name=get_name(units.DropPod),
                                           points=get_cost(units.DropPod))
        self.Weapon(self)


class SWLandSpeederStorm(TransportUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.LandSpeederStorm)
    type_id = 'sw_land_speeder_storm_v1'
    keywords = ['Vehicle', 'Transport', 'Land Speeder', 'Scout', 'Fly']
    power = 5

    def __init__(self, parent):
        gear = [ranged.CerberusLauncher, ranged.HeavyBolter]
        super(SWLandSpeederStorm, self).__init__(
            parent=parent, name=get_name(units.LandSpeederStorm),
            points=points_price(get_cost(units.LandSpeederStorm), *gear),
            gear=create_gears(*gear), static=True
        )


class SWRepulsor(TransportUnit, armory.SWUnit):
    type_name = 'Space Wolves ' + get_name(units.Repulsor)
    type_id = 'sw_repulsor_v1'
    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 16

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(SWRepulsor.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ranged.TwinHeavyBolter)
            self.variant(*ranged.TwinLascannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(SWRepulsor.Weapon2, self).__init__(parent, '')
            self.variant(*ranged.HeavyOnslaughtGatlingCannon)
            self.variant(*ranged.LasTalon)

    class Weapon3(OneOf):
        def __init__(self, parent):
            super(SWRepulsor.Weapon3, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)
            self.variant(*ranged.OnslaughtGatlingCannon)

    class Weapon4(OneOf):
        def __init__(self, parent):
            super(SWRepulsor.Weapon4, self).__init__(parent, '')
            self.variant('Two stormbolters', points=get_costs(*[ranged.StormBolter] * 2),
                         gear=create_gears(*[ranged.StormBolter] * 2))
            self.variant('Two fragstorm grenade launchers', points=get_costs(*[ranged.FragstormGrenadeLauncher] * 2),
                         gear=create_gears(*[ranged.FragstormGrenadeLauncher] * 2))

    class Weapon5(OneOf):
        def __init__(self, parent):
            super(SWRepulsor.Weapon5, self).__init__(parent, 'AA turret')
            self.variant(*ranged.IcarusIronhailHeavyStubber)
            self.variant(*ranged.IcarusRocketPod)
            self.variant(*ranged.StormBolter)
            self.variant(*ranged.FragstormGrenadeLauncher)

    class Weapon6(OneOf):
        def __init__(self, parent):
            super(SWRepulsor.Weapon6, self).__init__(parent, 'Launchers')
            self.variant(*wargear.AutoLaunchers)
            self.variant('Two fragstorm grenade launchers', points=get_costs(*[ranged.FragstormGrenadeLauncher] * 2),
                         gear=create_gears(*[ranged.FragstormGrenadeLauncher] * 2))

    class Weapon7(OptionsList):
        def __init__(self, parent):
            super(SWRepulsor.Weapon7, self).__init__(parent, '')
            self.variant(*ranged.IronhailHeavyStubber)

    def __init__(self, parent):
        gear = [ranged.KrakstormGrenadeLauncher] * 2
        super(SWRepulsor, self).__init__(parent, points=points_price(get_cost(units.Repulsor), *gear),
                                         gear=create_gears(*gear))
        self.Weapon1(self)
        self.Weapon2(self)
        self.Weapon3(self)
        self.Weapon7(self)
        self.Weapon4(self)
        self.Weapon5(self)
        self.Weapon6(self)
