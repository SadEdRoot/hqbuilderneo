from builder.games.wh40k8ed.space_marines.armory import SMUnit, Unit
from builder.games.wh40k8ed.space_marines.old_armory import SergeantWeapon,\
    add_pistol_options, add_dred_heavy_weapons, add_term_heavy_weapons,\
    add_term_combi_weapons
from . import melee, ranged
from builder.games.wh40k8ed.utils import get_cost


def add_special_weapons(obj):
    obj.spec = [
        obj.variant(*ranged.Flamer),
        obj.variant(*ranged.Plasmagun),
        obj.variant(*ranged.Meltagun)]

def add_combi_weapons(obj):
    obj.combi = [
        obj.variant(*ranged.StormBolter),
        obj.variant(*ranged.CombiPlasma),
        obj.variant(*ranged.CombiFlamer),
        obj.variant(*ranged.CombiMelta)]

def add_heavy_weapons(obj):
    obj.heavy = [
        obj.variant(*ranged.HeavyBolter),
        obj.variant(*ranged.Lascannon),
        obj.variant(*ranged.MissileLauncher),
        obj.variant(*ranged.MultiMelta),
        obj.variant(*ranged.PlasmaCannon)]

def add_melee_weapons(obj, chain=True, sword=True):
    if chain:
        obj.variant(*melee.Chainsword)
    obj.variant(*melee.FrostAxe)
    obj.variant(*melee.FrostSword)
    obj.claw = obj.variant(*melee.LightningClaws)
    obj.variant(*melee.PowerAxe)
    obj.variant(*melee.PowerFist)
    obj.variant(*melee.PowerMaul)
    if sword:
        obj.variant(*melee.PowerSword)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant(*melee.ThunderHammerCharacter)
    else:
        obj.variant(*melee.ThunderHammer)
    obj.wclaw = obj.variant(*melee.WolfClaw)


def add_term_melee_weapons(obj, sword=True, fist=True):
    obj.variant(*melee.Chainfist)
    obj.variant(*melee.FrostAxe)
    obj.variant(*melee.FrostSword)
    obj.claw = obj.variant(*melee.LightningClaws)
    obj.variant(*melee.PowerAxe)
    if fist:
        obj.fist = obj.variant(*melee.PowerFist)
    obj.variant(*melee.PowerMaul)
    if sword:
        obj.variant(*melee.PowerSword)
    if 'CHARACTER' in obj.parent.keywords:
        obj.variant(*melee.ThunderHammerCharacter)
    else:
        obj.variant(*melee.ThunderHammer)
    obj.wclaw = obj.variant(*melee.WolfClaw)


def add_dred_heavy_weapons(obj):
    obj.variant(*ranged.HeavyPlasmaCannon)
    obj.variant(*ranged.HelfrostCannon)
    obj.variant(*ranged.MultiMelta)
    obj.variant(*ranged.TwinLascannon)

class SWUnit(SMUnit):
    faction_substitution = {'Space Wolves': '<Chapter>'}
    wiki_faction = 'Space Wolves'

    @classmethod
    def calc_faction(cls):
        return []


class WolfClawUser(Unit):
    def build_points(self):
        res = super(WolfClawUser, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= (get_cost(melee.LightningClaws) * 2 - get_cost(melee.LightningClawsPair))
        if self.wep1.cur == self.wep1.wclaw and self.wep2.cur == self.wep2.wclaw:
            res -= (get_cost(melee.WolfClaw) * 2 - get_cost(melee.WolfClawPair))
        return res


class ClawUser(Unit):
    def build_points(self):
        res = super(ClawUser, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= (get_cost(melee.LightningClaws) * 2 - get_cost(melee.LightningClawsPair))
        return res
