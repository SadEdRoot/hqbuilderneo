__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit
from builder.games.wh40k8ed.options import OneOf,\
    ListSubUnit, UnitList
from . import armory
from . import units
from . import wargear
from . import ranged
from . import melee
from builder.games.wh40k8ed.utils import *


class VertusPraetors(FastUnit, armory.CustodesUnit):
    type_name = get_name(units.VertusPraetors)
    type_id = 'vertus_v1'
    keywords = ['Biker', 'Fly']

    power = 15

    class Praetor(ListSubUnit):
        type_name = 'Vertus Praetor'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(VertusPraetors.Praetor.Weapon, self).__init__(parent, 'Bike weapon')
                self.variant(*ranged.HurricaneBolter)
                self.variant(*ranged.SalvoLauncher)

        def __init__(self, parent):
            super(VertusPraetors.Praetor, self).__init__(parent, points=points_price(get_cost(units.VertusPraetors), melee.InterceptorLance),
                                                         gear=create_gears(melee.InterceptorLance))
            self.Weapon(self)
            armory.Misericordia(self)

    def __init__(self, parent):
        super(VertusPraetors, self).__init__(parent)
        self.models = UnitList(self, self.Praetor, 3, 10)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power + 5 * (self.models.count - 3)
