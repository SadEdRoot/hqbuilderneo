__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, SubUnit,\
    OptionsList
from . import ranged, melee, armory, units, biomorphs
from builder.games.wh40k8ed.utils import *


class Broodlord(HqUnit, armory.FleetUnit):
    type_name = get_name(units.Broodlord)
    type_id = 'broodlord_v1'
    keywords = ['Character', 'Infantry', 'Genestealer', 'Psyker', 'Synapse']
    power = 8

    def __init__(self, parent):
        super(Broodlord, self).__init__(parent, points=get_cost(units.Broodlord),
                                        gear=create_gears(melee.MonstrousRendingClaws))


class HiveTyrant(HqUnit, armory.FleetUnit):
    type_name = get_name(units.HiveTyrant)
    type_id = 'hive_tyrant_v1'
    keywords = ['Character', 'Monster', 'Psyker', 'Synapse']
    power = 9

    class Options(OptionsList):
        def __init__(self, parent):
            super(HiveTyrant.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.ToxinSacsHTCWar)
            self.variant(*biomorphs.AdrenalGlandsMonsters)
            self.wings = self.variant('Wings', get_cost(units.HiveTyrantWithWings) -
                                      get_cost(units.HiveTyrant))

    class Weapon(OneOf):
        def __init__(self, parent):
            super(HiveTyrant.Weapon, self).__init__(parent, 'Weapon')
            self.talons = self.variant(*melee.MonstrousScythingTalonsHTyrant)
            armory.add_monster_weapon(self)
            armory.add_monster_cannon(self)

        def apply_rescriction(self, value):
            for v in self.restricted:
                v.active = value

    def __init__(self, parent):
        super(HiveTyrant, self).__init__(parent, points=get_cost(units.HiveTyrant),
                                         gear=[Gear('Prehencile pincer tail')])
        self.w1 = self.Weapon(self)
        self.w2 = self.Weapon(self)
        self.opt = self.Options(self)

    def check_rules(self):
        super(HiveTyrant, self).check_rules()
        self.w1.apply_rescriction(self.w2.cur not in self.w2.restricted)
        self.w2.apply_rescriction(self.w1.cur not in self.w1.restricted)

    def build_points(self):
        res = super(HiveTyrant, self).build_points()
        if self.w1.cur == self.w1.talons and self.w2.cur == self.w2.talons:
            res -= 2 * get_cost(melee.MonstrousScythingTalonsHTyrant) -\
                   get_cost(melee.MonstrousScythingTalonsHTyrant2)
        return res

    def build_power(self):
        return self.power + 2 * self.opt.wings.value


class TheSwarmlord(HqUnit, armory.FleetUnit):
    type_name = get_name(units.TheSwarmlord)
    type_id = 'the_swarmlord_v1'
    keywords = ['Character', 'Monster', 'Psyker', 'Hive Tyrant', 'Synapse']
    power = 15

    def __init__(self, parent):
        super(TheSwarmlord, self).__init__(parent, points=get_cost(units.TheSwarmlord),
                                           unique=True, static=True, gear=[
                                               Gear('Bone Sabres'),
                                               Gear('Prehencile pincer tail')])


class TyranidPrime(HqUnit, armory.FleetUnit):
    type_name = get_name(units.TyranidPrime)
    type_id = 'tyranid_prime_v1'
    keywords = ['Character', 'Infantry', 'Synapse']
    power = 6

    class Options(OptionsList):
        def __init__(self, parent):
            super(TyranidPrime.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.ToxinSacsHTCWar)
            self.variant(*biomorphs.AdrenalGlands)
            self.variant(*ranged.FleshHooks)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(TyranidPrime.Weapon1, self).__init__(parent, 'Ranged weapon')
            self.variant(*ranged.Devourer)
            armory.add_basic_weapons(self)
            armory.add_melee_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TyranidPrime.Weapon2, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.ScythingTalons)
            armory.add_melee_weapons(self)

    def __init__(self, parent):
        super(TyranidPrime, self).__init__(parent, points=get_cost(units.TyranidPrime))
        self.w1 = self.Weapon1(self)
        self.w2 = self.Weapon2(self)
        self.Options(self)


class Tervigon(HqUnit, armory.FleetUnit):
    type_name = get_name(units.Tervigon)
    type_id = 'tervigon_v1'
    keywords = ['Character', 'Monster', 'Psyker', 'Synapse']
    power = 13

    class TervigonWeapon(OneOf):
        def __init__(self, parent, name):
            super(Tervigon.TervigonWeapon, self).__init__(parent, name=name)
            self.variant(*melee.MassiveScythingTalonsTervigon)
            self.variant(*melee.MassiveCrushingClaws)

    class Options(OptionsList):
        def __init__(self, parent):
            super(Tervigon.Options, self).__init__(parent, name='Options')
            self.variant(*biomorphs.ToxinSacs)
            self.variant(*biomorphs.AdrenalGlandsMonsters)

    def __init__(self, parent):
        gear = [ranged.StingerSalvo]
        cost = points_price(get_cost(units.Tervigon), *gear)
        super(Tervigon, self).__init__(parent, points=cost,
                                       gear=create_gears(*gear))
        self.TervigonWeapon(self, 'Weapon')
        self.Options(self)


class Neurothrope(HqUnit, armory.FleetUnit):
    type_name = get_name(units.Neurothrope)
    type_id = 'neurothrope_v1'
    keywords = ['Character', 'Fly', 'Infantry', 'Zoanthrope', 'Psyker', 'Synapse']
    power = 4

    def __init__(self, parent):
        super(Neurothrope, self).__init__(
            parent, points=get_cost(units.Neurothrope),
            gear=create_gears(melee.ClawsAndTeeth), static=True)


class OldOneEye(HqUnit, armory.FleetUnit):
    type_name = get_name(units.OldOneEye)
    type_id = 'oldoneeye_v1'
    keywords = ['Character', 'Monster', 'Carnifex']
    power = 10

    def __init__(self, parent):
        super(OldOneEye, self).__init__(
            parent, points=get_cost(units.OldOneEye), unique=True,
            gear=[
                Gear('Monstrous crushing claws'),
                Gear('Monstrous scything talons'),
                Gear('Thresher scythe')
            ], static=True
        )
