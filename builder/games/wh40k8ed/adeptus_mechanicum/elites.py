__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionalSubUnit, SubUnit, OptionsList
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class Enginseer(ElitesUnit, armory.CultUnit):
    type_name = 'Tech-Priest Enginseer'
    type_id = 'enginseer_v1'
    kwname = 'Enginseer'

    faction = ['Astra Militarum']
    keywords = ['Character', 'Infantry', 'Tech-priest']
    power = 2

    @classmethod
    def check_faction(cls, fac):
        return super(Enginseer, cls).check_faction(fac) or fac in ['TYRANIDS', 'GENESTEALER CULTS']

    def __init__(self, parent):
        gear = [melee.OmnissianAxe, melee.ServoArm, ranged.Laspistol]
        # cost from Astra Militarum codex
        cost = points_price(30, *gear)
        super(Enginseer, self).__init__(parent, gear=create_gears(*gear),
                                        points=cost, static=True)


class Fulgurites(ElitesUnit, armory.CultUnit):
    type_id = 'fulgurite_v1'
    type_name = get_name(units.FulguriteElectroPriests)
    kwname = 'Fulgurite'
    keywords = ['Infantry', 'Electro-priests']
    power = 4

    def __init__(self, parent):
        super(Fulgurites, self).__init__(parent)
        self.models = Count(self, self.type_name, 5, 20, get_cost(units.FulguriteElectroPriests), True,
                            gear=UnitDescription('Fulgurite Electro-Priest',
                                                 options=[
                                                     Gear('Electroleech stave'),
                                                 ]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power * ((self.models.cur + 4) / 5)


class Corpuscarii(ElitesUnit, armory.CultUnit):
    type_id = 'corpuscarii_v1'
    type_name = get_name(units.CorpuscariiElectroPriests)
    kwname = 'Corpuscarii'
    keywords = ['Infantry', 'Electro-priests']
    power = 3

    def __init__(self, parent):
        super(Corpuscarii, self).__init__(parent)
        self.models = Count(self, self.type_name, 5, 20, get_cost(units.CorpuscariiElectroPriests), True,
                            gear=UnitDescription('Corpuscarii Electro-Priest',
                                                 options=[
                                                     Gear('Electrostatic gauntlets'),
                                                 ]))

    def build_power(self):
        return self.power * ((self.models.cur + 4) / 5)

    def get_count(self):
        return self.models.cur


class Datasmith(ElitesUnit, armory.CultUnit):
    type_name = get_name(units.CyberneticaDatasmith)
    type_id = 'datasmith_v1'
    keywords = ['Character', 'Infantry', 'Tech-priest']
    power = 3

    def __init__(self, parent):
        gear = [melee.PowerFist, ranged.GammaPistol]
        cost = points_price(get_cost(units.CyberneticaDatasmith), *gear)
        super(Datasmith, self).__init__(parent, points=cost, static=True,
                                        gear=create_gears(*gear))


class AMServitors(ElitesUnit, armory.ForgeworldUnit):
    type_name = get_name(units.Servitors)
    type_id = 'am_servitors_v1'
    keywords = ['Infantry']
    power = 4

    class Weapons(OneOf):
        def __init__(self, parent):
            super(AMServitors.Weapons, self).__init__(parent, 'Weapon')
            self.variant(*melee.ServoArm)
            self.variant(*ranged.HeavyBolter)
            self.variant(*ranged.PlasmaCannon)
            self.variant(*ranged.MultiMelta)

        @property
        def description(self):
            return []

    def __init__(self, parent):
        super(AMServitors, self).__init__(parent=parent, gear=[
            UnitDescription('Servitor', options=[Gear('Servo-arm')], count=2)
        ], points=4 * get_cost(units.Servitors) + 2 * get_cost(melee.ServoArm))
        self.weps = [self.Weapons(self), self.Weapons(self)]

    def build_description(self):
        desc = super(AMServitors, self).build_description()
        for w in self.weps:
            desc.add_dup(UnitDescription('Servitor').add(w.cur.gear))
        return desc

    def get_count(self):
        return 4


class Ruststalkers(ElitesUnit, armory.SkitariiUnit):
    type_name = get_name(units.SicarianRuststalkers)
    type_id = 'ruststalkers_v1'
    keywords = ['Infantry']
    power = 6

    class Princeps(Unit):
        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Ruststalkers.Princeps.Weapon1, self).__init__(parent, 'Melee weapon')
                self.razor = self.variant(*melee.TransonicRazor)
                self.blade = self.variant(*melee.TransonicBlades)

        def __init__(self, parent):
            super(Ruststalkers.Princeps, self).__init__(
                parent, 'Ruststalker princeps', get_costs(units.SicarianRuststalkers, melee.Chordclaw),
                gear=create_gears(melee.Chordclaw))
            self.Weapon1(self)

    class Stalkers(Count):
        @property
        def description(self):
            return [UnitDescription('Sicarian Ruststalker', options=[
                Gear('Transonic razor'), Gear('Chordclaw')
            ]).set_count(self.cur - self.parent.wep.cur)]
        
    def __init__(self, parent):
        super(Ruststalkers, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Princeps(parent=self))
        cost = points_price(get_cost(units.SicarianRuststalkers), melee.TransonicRazor,
                            melee.Chordclaw)
        self.wars = self.Stalkers(self, self.type_name, 4, 9, cost, True)
        self.wep = Count(self, 'Transonic blades', 0, 4,
                         get_cost(melee.TransonicBlades) - get_costs(melee.TransonicRazor, melee.Chordclaw),
                         gear=UnitDescription('Sicarian Ruststalker',
                                              options=[Gear('Transonic blades')]))

    def get_count(self):
        return self.wars.cur + 1  # leader always present

    def check_rules(self):
        super(Ruststalkers, self).check_rules()
        self.wep.max = self.wars.cur

    def build_power(self):
        return self.power * (1 + (self.wars.cur > 4))


class Infiltrators(ElitesUnit, armory.SkitariiUnit):
    type_name = get_name(units.SicarianInfiltrators)
    type_id = 'infiltrators_v1'
    keywords = ['Infantry']
    power = 6

    class Princeps(Unit):
        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Infiltrators.Princeps.Weapon1, self).__init__(parent, 'Melee weapon')
                self.variant(join_and(ranged.Stubcarbine, melee.PowerSword),
                             get_costs(ranged.Stubcarbine, melee.PowerSword),
                             gear=create_gears(ranged.Stubcarbine, melee.PowerSword))
                self.variant(join_and(ranged.FlechetteBlaster, melee.TaserGoad),
                             get_costs(ranged.FlechetteBlaster, melee.TaserGoad),
                             gear=create_gears(ranged.FlechetteBlaster, melee.TaserGoad))

        def __init__(self, parent):
            super(Infiltrators.Princeps, self).__init__(
                parent, 'Infiltrator princeps', get_cost(units.SicarianInfiltrators))
            self.Weapon1(self)

    class Infiltrators(Count):
        @property
        def description(self):
            return [UnitDescription('Sicarian Infiltrator', options=[
                Gear('Stubcarbine'), Gear('Power sword')
            ]).set_count(self.cur - self.parent.goad.cur)]
        
    def __init__(self, parent):
        super(Infiltrators, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Princeps(parent=self))
        cost = points_price(get_cost(units.SicarianInfiltrators),
                            ranged.Stubcarbine, melee.PowerSword)
        self.wars = self.Infiltrators(self, self.type_name, 4, 9, cost, True)
        diff_cost = get_costs(ranged.Stubcarbine, melee.PowerSword) -\
                    get_costs(ranged.FlechetteBlaster, melee.TaserGoad)
        self.goad = Count(self, 'Fletchette blaster & taser goad', 0, 4, diff_cost,
                          gear=UnitDescription('Sicarian Infiltrator',
                                               options=[Gear('Flechette blaster'),
                                                        Gear('Taser goad')]))

    def get_count(self):
        return self.wars.cur + 1  # leader always present

    def check_rules(self):
        super(Infiltrators, self).check_rules()
        self.goad.max = self.wars.cur

    def build_power(self):
        return self.power * (1 + (self.wars.cur > 4))
