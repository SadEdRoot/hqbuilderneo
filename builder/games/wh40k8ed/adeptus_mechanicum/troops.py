__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionalSubUnit, SubUnit, OptionsList
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *
__author__ = 'Ivan Trusov'


class Breachers(TroopsUnit, armory.CultUnit):
    type_id = 'breachers_v1'
    type_name = get_name(units.KataphronBreachers)
    keywords = ['Infantry']
    power = 8

    class Breacher(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(Breachers.Breacher.Weapon1, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.HeavyArcRifle)
                self.variant(*ranged.TorsionCannon)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(Breachers.Breacher.Weapon2, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.ArcClaw)
                self.variant(*melee.HydraulicClaw)

        def __init__(self, parent):
            super(Breachers.Breacher, self).__init__(parent, name='Kataphron Breacher',
                                                     points=get_cost(units.KataphronBreachers))
            self.Weapon1(self)
            self.Weapon2(self)

    def __init__(self, parent):
        super(Breachers, self).__init__(parent)
        self.models = UnitList(self, self.Breacher, 3, 12)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * ((self.get_count() + 2) / 3)


class KatDestroyers(TroopsUnit, armory.CultUnit):
    type_id = 'destroyers_v1'
    type_name = get_name(units.KataphronDestroyers)
    keywords = ['Infantry']
    power = 10

    class Destroyer(ListSubUnit):

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(KatDestroyers.Destroyer.Weapon1, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.PlasmaCulverin)
                self.variant(*ranged.HeavyGravCannon)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(KatDestroyers.Destroyer.Weapon2, self).__init__(parent, '')
                self.variant(*ranged.PhosphorBlaster)
                self.variant(*ranged.CognisFlamer)

        def __init__(self, parent):
            super(KatDestroyers.Destroyer, self).__init__(parent, name='Kataphron Destroyer',
                                                          points=get_cost(units.KataphronDestroyers))
            self.Weapon1(self)
            self.Weapon2(self)

    def __init__(self, parent):
        super(KatDestroyers, self).__init__(parent)
        self.models = UnitList(self, self.Destroyer, 3, 12)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * ((self.models.count + 2) / 3)


class TroopOptions(OptionsList):
    def __init__(self, parent):
        super(TroopOptions, self).__init__(parent, 'Options', limit=1)
        self.variant(*wargear.EnhancedDataTether)
        self.variant(*wargear.Omnispex)


class SkitariiRangers(TroopsUnit, armory.SkitariiUnit):
    type_name = get_name(units.SkitariiRangers)
    type_id = 'rangers_v1'
    keywords = ['Infantry']
    power = 4

    class Alpha(Unit):
        type_name = 'Ranger Alpha'

        class WeaponMelee(OptionsList):
            def __init__(self, parent):
                super(SkitariiRangers.Alpha.WeaponMelee, self).__init__(parent, 'Melee weapon', limit=1)
                armory.add_melee_weapons(self)

        class WeaponRanged(OneOf):
            def __init__(self, parent):
                super(SkitariiRangers.Alpha.WeaponRanged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.GalvanicRifle)
                armory.add_pistol_weapons(self)

        def __init__(self, parent):
            super(SkitariiRangers.Alpha, self).__init__(parent, points=get_cost(units.SkitariiRangers))
            self.rng = self.WeaponRanged(self)
            self.mle = self.WeaponMelee(self)

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(SkitariiRangers.SpecialWeapon, self).__init__(parent, 'Special weapon')
            self.rifle = self.variant(*ranged.GalvanicRifle)
            armory.add_special_weapons(self)

    def __init__(self, parent):
        super(SkitariiRangers, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Alpha(parent=self))
        self.wars = Count(self, 'Skitarii Ranger', 4, 9, get_cost(units.SkitariiRangers), True)
        self.opt = TroopOptions(self)
        self.spec = [self.SpecialWeapon(self) for i in range(0, 3)]

    def get_count(self):
        return self.wars.cur + 1  # leader always present

    def check_rules(self):
        super(SkitariiRangers, self).check_rules()
        self.spec[2].used = self.spec[2].visible = self.get_count() == 10

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        model = UnitDescription(name='Skitarii Ranger')
        spec_count = sum([(o.used and not o.cur == o.rifle) for o in self.spec]) + (1 if self.opt.any else 0)
        desc.add(model.clone().add(Gear('Galvanic rifle')).set_count(self.wars.cur - spec_count))
        if self.opt.any:
            desc.add(model.clone().add(Gear('Galvanic rifle')).add(self.opt.description))
        for g in self.spec:
            if g.used and not g.cur == g.rifle:
                desc.add_dup(model.clone().add(g.description))
        return desc

    def build_power(self):
        return self.power + 3 * (self.wars.cur > 4)


class Vanguard(TroopsUnit, armory.SkitariiUnit):
    type_name = get_name(units.SkitariiVanguard)
    type_id = 'vanguard_v1'
    power = 4

    class Alpha(Unit):
        type_name = 'Vanguard Alpha'

        class WeaponMelee(OptionsList):
            def __init__(self, parent):
                super(Vanguard.Alpha.WeaponMelee, self).__init__(parent, 'Melee weapon', limit=1)
                armory.add_melee_weapons(self)

        class WeaponRanged(OneOf):
            def __init__(self, parent):
                super(Vanguard.Alpha.WeaponRanged, self).__init__(parent, 'Ranged weapon')
                self.variant(*ranged.RadiumCarbine)
                armory.add_pistol_weapons(self)

        def __init__(self, parent):
            super(Vanguard.Alpha, self).__init__(parent, points=get_cost(units.SkitariiVanguard))
            self.rng = self.WeaponMelee(self)
            self.mle = self.WeaponRanged(self)

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(Vanguard.SpecialWeapon, self).__init__(parent, 'Special weapon')
            self.carbine = self.variant(*ranged.RadiumCarbine)
            armory.add_special_weapons(self)

    def __init__(self, parent):
        super(Vanguard, self).__init__(parent, self.type_name)
        self.ldr = SubUnit(self, self.Alpha(parent=self))
        self.wars = Count(self, self.type_name, 4, 9, get_cost(units.SkitariiVanguard), True)
        self.opt = TroopOptions(self)
        self.spec = [self.SpecialWeapon(self) for i in range(0, 3)]

    def get_count(self):
        return self.wars.cur + 1  # leader always present

    def check_rules(self):
        super(Vanguard, self).check_rules()
        self.spec[2].used = self.spec[2].visible = self.get_count() == 10

    def build_description(self):
        desc = UnitDescription(name=self.name, points=self.points, count=self.get_count())
        desc.add(self.ldr.description)
        model = UnitDescription(name=self.name)
        spec_count = sum([(o.used and not o.cur == o.carbine) for o in self.spec]) + (1 if self.opt.any else 0)
        desc.add(model.clone().add(Gear('Radium carbine')).set_count(self.wars.cur - spec_count))
        if self.opt.any:
            desc.add(model.clone().add(Gear('Radium carbine')).add(self.opt.description).add_points(self.opt.points))
        for g in self.spec:
            if g.used and not g.cur == g.carbine:
                desc.add_dup(model.clone().add(g.description))
        return desc

    def build_power(self):
        return self.power + 3 * (self.wars.cur > 4)
