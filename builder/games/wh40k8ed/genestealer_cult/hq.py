__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList, Count
from . import armory, units, melee, ranged
from builder.games.wh40k8ed.utils import *


class Patriarch(HqUnit, armory.CultUnit):
    type_name = get_name(units.Patriarch)
    type_id = 'gs_patriarch_v1'
    power = 7
    keywords = ['Infantry', 'Character', 'Genestealer', 'Psyker']

    def __init__(self, parent):
        super(Patriarch, self).__init__(parent, points=get_cost(units.Patriarch),
                                        gear=create_gears(melee.MonstrousRendingClaws))
        self.familliars = Count(self, 'Familiars', 0, 2, get_cost(units.Familiars),
                                gear=UnitDescription('Familiar'))

    def build_power(self):
        return self.power + (self.familiars.cur > 0)


class Magus(HqUnit, armory.CultUnit):
    type_name = get_name(units.Magus)
    type_id = 'gs_magus_v1'
    power = 4
    keywords = ['Infantry', 'Character', 'Psyker']

    def __init__(self, parent):
        gear = [ranged.Autopistol, melee.ForceStave, melee.CultistKnife]
        cost = points_price(get_cost(units.Magus), *gear)
        super(Magus, self).__init__(parent, points=cost,
                                    gear=create_gears(*gear))
        self.familiars = Count(self, 'Familiars', 0, 2, get_cost(units.Familiars),
                               gear=UnitDescription('Familiar'))

    def build_power(self):
        return self.power + (self.familiars.cur > 0)


class Primus(HqUnit, armory.CultUnit):
    type_name = get_name(units.Primus)
    type_id = 'gs_primus_v1'
    power = 4
    keywords = ['Infantry', 'Character']

    def __init__(self, parent):
        gear = [ranged.NeedlePistol, melee.Bonesword, melee.ToxinInjectorClaw, ranged.BlastingCharge]
        cost = points_price(get_cost(units.Primus), *gear)
        super(Primus, self).__init__(parent, points=cost, gear=create_gears(*gear),
                                     static=True)


class Iconward(HqUnit, armory.CultUnit):
    type_name = get_name(units.AcolyteIconward)
    type_id = 'gs_iconward_v1'
    power = 3
    keywords = ['Infantry', 'Character']

    def __init__(self, parent):
        gear = [ranged.Autopistol, ranged.BlastingCharge]
        super(Iconward, self).__init__(parent, points=get_cost(units.AcolyteIconward),
                                       gear=create_gears(*gear) +
                                       [Gear('Rending claw')], static=True)


class Abominant(HqUnit, armory.CultUnit):
    type_name = get_name(units.Abominant)
    type_id = 'gs_abominant_v1'
    power = 6
    keywords = ['Infantry', 'Character', 'Aberrant']

    def __init__(self, parent):
        gear = [melee.PowerSledgehammer]
        super(Abominant, self).__init__(parent, points=get_cost(units.Abominant),
                                        gear=create_gears(*gear) +
                                        [Gear('Rending claw'),
                                         UnitDescription('Mindwyrm Familiar', options=[Gear('Familiar claws')])],
                                        static=True)


class JackalAlphus(HqUnit, armory.CultUnit):
    type_name = get_name(units.JackalAlphus)
    type_id = 'gs_jackal_alphus_v1'
    power = 4
    keywords = ['Biker', 'Character']

    def __init__(self, parent):
        gear = [ranged.JackalSniperRifle, ranged.Autopistol, ranged.BlastingCharge]
        super(JackalAlphus, self).__init__(parent,
                                           points=points_price(get_cost(units.JackalAlphus), *gear),
                                           gear=create_gears(*gear), static=True)

