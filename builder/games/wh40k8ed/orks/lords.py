from . import armory
from builder.games.wh40k8ed.unit import Unit, LordsUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count, ListSubUnit, SubUnit, UnitList
from . import melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *


class Stompa(LordsUnit, armory.OrkClanUnit):
    type_name = get_name(units.Stompa)
    type_id = 'stompa_v1'
    power = 46

    def __init__(self, parent):
        gears = [ranged.Deffkannon, ranged.SupaGatler] + [ranged.BigShoota] * 3 +\
                [ranged.TwinBigShoota] + [ranged.SupaRokkit] * 3 + [ranged.Skorcha, melee.MegaChoppa]
        cost = points_price(get_costs(units.Stompa), *gears)
        super(Stompa, self).__init__(parent, points=cost,
                                     gear=create_gears(*gears))
        Count(self, 'Supa-rokkit', 0, 2, get_cost(ranged.SupaRokkit))
