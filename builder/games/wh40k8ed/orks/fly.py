from builder.games.wh40k8ed.unit import Unit, FlierUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, \
    Gear, UnitDescription, Count, ListSubUnit, SubUnit, UnitList
from . import melee, ranged, wargear, units
from builder.games.wh40k8ed.utils import *
from . import armory


class Dakkajet(FlierUnit, armory.OrkClanUnit):
    type_name = get_name(units.Dakkajet)
    type_id = 'dakkajet_v1'
    power = 7

    class Weapons(OptionsList):
        def __init__(self, parent):
            super(Dakkajet.Weapons, self).__init__(parent, 'Options')
            self.variant(*ranged.SupaShoota)
            self.variant(*ranged.SupaShoota)

    def __init__(self, parent):
        gear = [ranged.SupaShoota for el in range(0, 4)]
        super(Dakkajet, self).__init__(
            parent,
            points=points_price(get_costs(units.Dakkajet), *gear),
            gear=create_gears(*gear)
        )
        self.Weapons(self)


class BurnaBommer(FlierUnit, armory.OrkClanUnit):
    type_name = get_name(units.BurnaBommer)
    type_id = 'burna_bommer_v1'
    power = 7

    class Options(OptionsList):
        def __init__(self, parent):
            super(BurnaBommer.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.SkorchaMissiles)

    def __init__(self, parent):
        gear = [ranged.TwinBigShoota, ranged.SupaShoota, ranged.SupaShoota]
        super(BurnaBommer, self).__init__(
            parent,
            points=points_price(get_costs(units.BurnaBommer), *gear),
            gear=create_gears(*gear)
        )
        self.Options(self)


class BlitzaBommer(FlierUnit, armory.OrkClanUnit):
    type_name = get_name(units.BlitzaBommmer)
    type_id = 'blitza_bommer_v1'
    power = 6

    def __init__(self, parent):
        gear = [ranged.BigShoota, ranged.SupaShoota, ranged.SupaShoota]
        cost = points_price(get_costs(units.BlitzaBommmer), *gear)
        gear = create_gears(*gear)
        super(BlitzaBommer, self).__init__(parent, points=cost, gear=gear)


class WazzboomBlastjet(FlierUnit, armory.OrkClanUnit):
    type_name = get_name(units.WazbomBlastajet)
    type_id = 'wazzboom_blastjet_v1'
    power = 8

    class Wargear(OneOf):
        def __init__(self, parent):
            super(WazzboomBlastjet.Wargear, self).__init__(parent, 'Wargear')
            self.variant(*ranged.StikkbombFinga)
            self.variant(*wargear.KustomForceField)

    class Weapons(OptionsList):
        def __init__(self, parent):
            super(WazzboomBlastjet.Weapons, self).__init__(parent, 'Options')
            self.variant(*ranged.SupaShoota)
            self.variant(*ranged.SupaShoota)

    class Shoota(OneOf):
        def __init__(self, parent):
            super(WazzboomBlastjet.Shoota, self).__init__(parent, 'Shooting weapons')
            self.variant('Two wasbom mega-kannons', 2 * get_cost(ranged.WazbomMegaKannon),
                         gear=2 * create_gears(ranged.WazbomMegaKannon))
            self.variant('Two Tellyport mega-blastas', 2 * get_cost(ranged.TellyportMegaBlasta),
                         gear=2 * create_gears(ranged.TellyportMegaBlasta))

    def __init__(self, parent):
        gear = [ranged.SmashaGun]
        cost = points_price(get_cost(units.WazbomBlastajet), *gear)
        super(WazzboomBlastjet, self).__init__(parent, points=cost, gear=create_gears(*gear))
        self.Wargear(self)
        self.Weapons(self)

