from .hq import BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk,\
    GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike, Weirdboy,\
    ShokkBigMek, DeffkillaWartrike
from .elite import Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek,\
    NobWithBanner, Nobz, NobzOnWarbikes, Painboy,\
    PainboyOnWarbike, Runtherd, TankBustas, PainboyV2, MekV2
from .troops import Boyz, Gretchin
from .fast import Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, WarBuggies,\
    KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies,\
    ShokkjumpDragstas
from .transport import Trukk
from .heavy import BigGunz, MekGunz, Battlewagon, KillaKans, Morkanaut,\
    Gorkanaut, Lootas, FlashGitz, DeffDreads, Gunwagon, Bonebreaka
from .fly import Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet
from .lords import Stompa
from .fort import MekboyWorkshop

hq_units = [
    BigMek, BigMekInMegaArmour, BigMekOnWarbike, BossSnikrot, BossZagstruk,
    GhazkullThraka, KaptinBadrukk, Warboss, WarbossInMegArmour, WarbossOnWarBike,
    Weirdboy, ShokkBigMek, DeffkillaWartrike
]
troops_units = [
    Boyz, Gretchin
]

elite_units = [
    Kommandos, MegaNobz, BurnaBoyz, MadDockGrotsnik, Mek,
    NobWithBanner, Nobz, NobzOnWarbikes, Painboy,
    PainboyOnWarbike, Runtherd, TankBustas, PainboyV2, MekV2
]

fast_units = [
    Stormboyz, Deffkoptas, Warbikers, Wartracks, Skorchas, WarBuggies,
    KustomBoosta, BoomdakkaSnazzwagons, MegatrakkScrapjets, RukkatrukkSquigbuggies,
    ShokkjumpDragstas
]

transport_units = [
    Trukk
]

heavy_units = [
    BigGunz, MekGunz, Battlewagon, KillaKans, Morkanaut,
    Gorkanaut, Lootas, FlashGitz, DeffDreads, Gunwagon, Bonebreaka
]

fly_units = [
    Dakkajet, BurnaBommer, BlitzaBommer, WazzboomBlastjet
]

lords_units = [
    Stompa
]

fort_units = [
    MekboyWorkshop
]

unit_types = []
unit_types.extend(hq_units)
unit_types.extend(elite_units)
unit_types.extend(fast_units)
unit_types.extend(heavy_units)
unit_types.extend(fly_units)
unit_types.extend(transport_units)
unit_types.extend(troops_units)
unit_types.extend(lords_units)
unit_types.extend(fort_units)
