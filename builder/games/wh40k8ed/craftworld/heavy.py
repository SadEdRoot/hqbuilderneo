from builder.games.wh40k8ed.unit import HeavyUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, ListSubUnit, \
    Gear, UnitDescription, Count, UnitList
from builder.games.wh40k8ed.utils import get_name, create_gears, points_price, get_cost
from . import armory
from . import units
from . import melee
from . import ranged

__author__ = 'Ivan Truskov'


class Reapers(HeavyUnit, armory.CraftworldUnit):
    type_name = get_name(units.DarkReapers)
    type_id = 'reapers_v1'
    faction = ['Aspect Warrior']
    keywords = ['Infantry']

    model_gear = [ranged.ReaperLauncher]
    model = UnitDescription('Dark Reaper', options=create_gears(*model_gear))

    class ExarchWeapon(OneOf):
        def __init__(self, parent):
            super(Reapers.ExarchWeapon, self).__init__(parent, 'Exarch Weapon')
            self.variant(*ranged.ReaperLauncher)
            self.variant(*ranged.ShurikenCannon)
            self.variant(*ranged.AeldariMissileLauncher)
            self.variant(*ranged.TempestLauncher)

        @property
        def description(self):
            res = UnitDescription('Dark Reaper Exarch')
            res.add(self.cur.gear)
            return [res]

    class Boss(OptionsList):
        def __init__(self, parent):
            super(Reapers.Boss, self).__init__(parent, 'Exarch')
            self.variant('Include Dark Reaper Exarch', gear=[], points=get_cost(units.DarkReapers))

    def __init__(self, parent):
        super(Reapers, self).__init__(parent)
        self.boss = self.Boss(self)
        self.wep = self.ExarchWeapon(self)
        points = points_price(get_cost(units.DarkReapers), *Reapers.model_gear)
        self.models = Count(self, 'Dark Reapers', 3, 10, per_model=True, gear=Reapers.model.clone(), points=points)

    def check_rules(self):
        super(Reapers, self).check_rules()
        self.wep.used = self.wep.visible = self.boss.any
        self.models.min, self.models.max = (3 - self.boss.any, 10 - self.boss.any)

    def get_count(self):
        return self.models.cur + self.boss.any

    def build_power(self):
        return 4 + (9 if self.get_count() > 5 else
                    (3 if self.get_count() > 3 else 0))


class VaulWrath(HeavyUnit, armory.CraftworldUnit):
    type_name = get_name(units.SupportWeapons)
    type_id = 'vaul_v1'
    faction = ['Warhost']
    kwname = 'Support Weapon'
    keywords = ['Vehicle', 'Artillery']

    class Piece(ListSubUnit):
        type_name = 'Support Weapon'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(VaulWrath.Piece.Weapon, self).__init__(parent, 'Weapon')
                self.variant(*ranged.ShadowWeaver)
                self.variant(*ranged.VibroCannon)
                self.variant(*ranged.DCannon)

        def __init__(self, parent):
            points = points_price(get_cost(units.SupportWeapons), *[ranged.ShurikenCatapult])
            super(VaulWrath.Piece, self).__init__(parent, gear=[
                UnitDescription('Guardian', options=[Gear('Shuriken catapult')])], points=points)
            self.Weapon(self)

    def __init__(self, parent):
        super(VaulWrath, self).__init__(parent)
        self.models = UnitList(self, self.Piece, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return 3 * self.models.count


class Falcon(HeavyUnit, armory.CraftworldUnit):
    type_name = get_name(units.Falcon)
    type_id = 'falcon_v1'
    faction = ['Warhost']

    keywords = ['Vehicle', 'Transport', 'Fly']

    power = 9

    class MainWeapon(OneOf):
        def __init__(self, parent):
            super(Falcon.MainWeapon, self).__init__(parent, name='Main Weapon')
            armory.add_heavy_weapons(self)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Falcon.Weapon, self).__init__(parent, 'Secondary Weapon')
            self.variant(*ranged.TwinShurikenCatapult)
            self.variant(*ranged.ShurikenCannon)

    def __init__(self, parent):
        points = points_price(get_cost(units.Falcon), *[ranged.PulseLaser])
        super(Falcon, self).__init__(parent, gear=[Gear('Pulse laser')], points=points)
        self.MainWeapon(self)
        self.Weapon(self)
        armory.Vehicle(self)


class FirePrism(HeavyUnit, armory.CraftworldUnit):
    type_name = get_name(units.FirePrism)
    type_id = 'fire_prism_v1'
    faction = ['Warhost']

    keywords = ['Vehicle', 'Fly']

    power = 9

    def __init__(self, parent):
        points = points_price(get_cost(units.FirePrism), *[ranged.PrismCannon])
        super(FirePrism, self).__init__(parent, gear=[Gear('Prism cannon')], points=points)
        Falcon.Weapon(self)
        armory.Vehicle(self)


class NightSpinner(HeavyUnit, armory.CraftworldUnit):
    type_name = get_name(units.NightSpinner)
    type_id = 'night_spinner_v1'
    faction = ['Warhost']

    keywords = ['Vehicle', 'Fly']

    power = 8

    def __init__(self, parent):
        points = points_price(get_cost(units.NightSpinner), *[ranged.Doomweaver])
        super(NightSpinner, self).__init__(parent, gear=[Gear('Doomweaver')], points=points)
        Falcon.Weapon(self)
        armory.Vehicle(self)


class Walkers(HeavyUnit, armory.CraftworldUnit):
    type_name = get_name(units.WarWalkers)
    type_id = 'wwalkers_v1'
    faction = ['Warhost']

    keywords = ['Vehicle']

    class Walker(ListSubUnit):
        type_name = 'War Walker'

        class Weapon(OneOf):
            def __init__(self, parent):
                super(Walkers.Walker.Weapon, self).__init__(parent, 'Weapon')
                armory.add_heavy_weapons(self)

        def __init__(self, parent):
            super(Walkers.Walker, self).__init__(parent, points=get_cost(units.WarWalkers))
            self.Weapon(self)
            self.Weapon(self)

    def __init__(self, parent):
        super(Walkers, self).__init__(parent)
        self.models = UnitList(self, self.Walker, 1, 3)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return 4 * self.get_count()


class Wraithlord(HeavyUnit, armory.CraftworldUnit):
    type_name = 'Wraithlord'
    type_id = 'wraithlord_v1'
    faction = ['Spirit Host']
    keywords = ['Monster']

    power = 8

    class Underslung(OneOf):
        def __init__(self, parent):
            super(Wraithlord.Underslung, self).__init__(parent, 'Weapon')
            self.variant(*ranged.ShurikenCatapult)
            self.variant(*ranged.Flamer)

    class Melee(OptionsList):
        def __init__(self, parent):
            super(Wraithlord.Melee, self).__init__(parent, 'Melee weapon')
            self.variant(*melee.Ghostglaive)

    def variant(self, name, points=None):
        ap = None if self.use_power else points
        self.hw.append(Count(self, name, 0, 2, ap))

    def __init__(self, parent):
        points = points_price(get_cost(units.Wraithlord), *[melee.WraithboneFists])
        super(Wraithlord, self).__init__(parent, gear=[Gear('Wraithbone fists')], points=points)
        self.Underslung(self)
        self.Underslung(self)
        self.Melee(self)
        self.hw = []
        armory.add_heavy_weapons(self)

    def check_rules(self):
        super(Wraithlord, self).check_rules()
        Count.norm_counts(0, 2, self.hw)
