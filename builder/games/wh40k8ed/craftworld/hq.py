from builder.core2 import UnitList

__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import HqUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, \
    UnitDescription, SubUnit, OptionsList, Count, ListSubUnit
from builder.games.wh40k8ed.utils import get_name, get_cost, create_gears, points_price
from . import armory
from . import units
from . import melee
from . import ranged
from . import wargear


class Eldrad(HqUnit, armory.EldarUnit):
    type_name = get_name(units.EldradUlthran)
    type_id = 'eldrad_v1'
    faction = ['Warhost', 'Ulthwe']
    keywords = ['Character', 'Infantry', 'Psyker', 'Farseer']
    power = 8

    def __init__(self, parent):
        super(Eldrad, self).__init__(parent=parent, points=get_cost(units.EldradUlthran),
                                     unique=True, gear=[
                Gear('Staff of Ulthamar'),
                Gear('Shuriken pistol'),
                Gear('Witchblade'),
            ], static=True)


class Yriel(HqUnit, armory.EldarUnit):
    type_name = get_name(units.PrinceYriel)
    type_id = 'princeyriel_v1'
    faction = ['Warhost', 'Iyanden']
    keywords = ['Character', 'Infantry', 'Autarch']
    power = 5

    def __init__(self, parent):
        super(Yriel, self).__init__(parent=parent, points=get_cost(units.PrinceYriel),
                                    unique=True, gear=[
                Gear('The Eye of Wrath'),
                Gear('The Spear of Twillight'),
                Gear('Plasma grenades'),
            ], static=True)


class Illic(HqUnit, armory.EldarUnit):
    type_name = get_name(units.IllicNightspear)
    type_id = 'illicnightspear_v1'
    faction = ['Warhost', 'Alatoic']
    keywords = ['Character', 'Infantry', 'Psyker']
    power = 4

    def __init__(self, parent):
        super(Illic, self).__init__(parent=parent, points=get_cost(units.IllicNightspear),
                                    unique=True, gear=[
                Gear('Voidbringer'),
                Gear('Shuriken pistol'),
                Gear('Power sword'),
            ], static=True)


class Asurmen(HqUnit, armory.EldarUnit):
    type_name = get_name(units.Asurmen)
    type_id = 'asurmen_v1'
    faction = ['Aspect Warrior']
    keywords = ['Character', 'Infantry', 'Phoenix Lord']
    power = 9

    def __init__(self, parent):
        super(Asurmen, self).__init__(parent=parent, points=get_cost(units.Asurmen),
                                      unique=True, gear=[
                Gear('The Sword of Asur'),
                Gear('Avenger shuriken catapult', count=2),
            ], static=True)


class JainZar(HqUnit, armory.EldarUnit):
    type_name = get_name(units.JainZar)
    type_id = 'jainzar_v1'
    faction = ['Aspect Warrior']
    keywords = ['Character', 'Infantry', 'Phoenix Lord']
    power = 7

    def __init__(self, parent):
        super(JainZar, self).__init__(parent=parent, points=get_cost(units.JainZar),
                                      unique=True, gear=[
                Gear('Silent Death'),
                Gear('Blade of Destruction'),
            ], static=True)


class Karandras(HqUnit, armory.EldarUnit):
    type_name = get_name(units.Karandras)
    type_id = 'karandras_v1'
    faction = ['Aspect Warrior']
    keywords = ['Character', 'Infantry', 'Phoenix Lord']
    power = 9

    def __init__(self, parent):
        super(Karandras, self).__init__(parent=parent, points=get_cost(units.Karandras),
                                        unique=True, gear=[
                Gear('Scorpion chainsword'),
                Gear('Scorpion\'s claw'),
                Gear('Sunburst grenades'),
            ], static=True)


class Fuegan(HqUnit, armory.EldarUnit):
    type_name = get_name(units.Fuegan)
    type_id = 'fuegan_v1'
    faction = ['Aspect Warrior']
    keywords = ['Character', 'Infantry', 'Phoenix Lord']
    power = 8

    def __init__(self, parent):
        super(Fuegan, self).__init__(parent=parent, points=get_cost(units.Fuegan),
                                     unique=True, gear=[
                Gear('Fire Axe'),
                Gear('Firepike'),
                Gear('Melta bombs'),
            ], static=True)


class Baharroth(HqUnit, armory.EldarUnit):
    type_name =get_name(units.Baharroth)
    type_id = 'baharroth_v1'
    faction = ['Aspect Warrior']
    keywords = ['Character', 'Infantry', 'Phoenix Lord', 'Jump pack', 'Fly']
    power = 6

    def __init__(self, parent):
        super(Baharroth, self).__init__(parent=parent, points=get_cost(units.Baharroth),
                                        unique=True, gear=[
                Gear('The Shining Blade'),
                Gear('Hawk\'s talon'),
            ], static=True)


class Maugan(HqUnit, armory.EldarUnit):
    type_name = get_name(units.MauganRa)
    type_id = 'mauganra_v1'
    faction = ['Aspect Warrior']
    keywords = ['Character', 'Infantry', 'Phoenix Lord']
    power = 8

    def __init__(self, parent):
        super(Maugan, self).__init__(parent=parent, points=get_cost(units.MauganRa),
                                     unique=True, gear=[
                Gear('The Maugetar'),
            ], static=True)


class Autarch(HqUnit, armory.CraftworldUnit):
    type_name = get_name(units.Autarch)
    type_id = 'autarch_v1'
    faction = ['Warhost']
    keywords = ['Character', 'Infantry']
    power = 4
    model_gear = [melee.StarGlaive, ranged.PlasmaGrenade, wargear.Forceshield]

    def __init__(self, parent):
        cost = points_price(get_cost(units.Autarch), *Autarch.model_gear)
        super(Autarch, self).__init__(parent, gear=create_gears(*Autarch.model_gear), points=cost)


class SwoopingAutarch(HqUnit, armory.CraftworldUnit):
    type_name = get_name(units.AutarchWithSwoopingHawkWings)
    type_id = 'autarch_winged_v1'
    faction = ['Warhost']
    kwname = 'Autarch'
    keywords = ['Character', 'Infantry', 'Fly', 'Jump pack']
    power = 5
    model_gear = [melee.PowerSword, ranged.FusionPistol, ranged.PlasmaGrenade, wargear.Forceshield]

    def __init__(self, parent):
        cost = points_price(get_cost(units.AutarchWithSwoopingHawkWings), *SwoopingAutarch.model_gear)
        super(SwoopingAutarch, self).__init__(parent, gear=create_gears(*SwoopingAutarch.model_gear), points=cost)


class BikerAutarch(HqUnit, armory.CraftworldUnit):
    type_name = get_name(units.AutarchSkyrunner)
    type_id = 'autarch_biker_v1'
    faction = ['Warhost']
    kwname = 'Autarch'
    keywords = ['Character', 'Biker', 'Fly']
    power = 6
    model_gear = [ranged.TwinShurikenCatapult]

    class Weapon(OneOf):
        def __init__(self, parent):
            super(BikerAutarch.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.PowerSword)
            self.variant(*ranged.LaserLance)
            self.variant(*ranged.FusionGun)


    def __init__(self, parent):
        cost = points_price(get_cost(units.AutarchSkyrunner), *BikerAutarch.model_gear)
        super(BikerAutarch, self).__init__(parent, gear=create_gears(*BikerAutarch.model_gear), points=cost)
        BikerAutarch.Weapon(self)


class Avatar(HqUnit, Unit):
    """
    All other craftworld eldar are also Ynnari, so we duplicate faction list here
    """
    type_name = get_name(units.Avatar)
    type_id = 'avatar_v1'

    @classmethod
    def calc_faction(cls):
        return ['ULTHWE', 'IYANDEN', 'ALATOIC', 'BIEL-TAN', 'SAIM-HANN']

    faction = ['Aeldari', 'Asuryani', '<Craftworld>', 'Aspect Warrior']

    keywords = ['Character', 'Monster', 'Daemon']
    power = 13

    def __init__(self, parent):
        super(Avatar, self).__init__(parent=parent, points=get_cost(units.Avatar),
                                     unique=True, gear=[
                                         Gear('The Wailing Doom'),
                                     ], static=True)


class JumpingAutarch(HqUnit, armory.CraftworldUnit):
    type_name = 'Autarch with Warp Jump Generator'
    type_id = 'autarch_jumper_v1'
    faction = ['Warhost']
    kwname = 'Autarch'
    keywords = ['Character', 'Infantry', 'Jump pack']
    power = 5

    class Mask(OptionsList):
        def __init__(self, parent):
            super(JumpingAutarch.Mask, self).__init__(parent, 'Helmet', limit=1)
            self.variant('Banshee mask', 0)
            self.variant('Mandiblasters', 0)

    class Pistol(OneOf):
        def __init__(self, parent):
            super(JumpingAutarch.Pistol, self).__init__(parent, 'Pistol')
            self.variant('Shuriken pistol', 0)

    class AutarchWeapon(OptionsList):
        def __init__(self, parent):
            super(JumpingAutarch.AutarchWeapon, self).__init__(parent, 'Autarch Weapons')
            self.single = [
                self.variant(*ranged.FusionGun),
                self.variant(*ranged.Lasblaster),
                self.variant(*ranged.ReaperLauncher)
            ]
            self.double = [
                Count(parent, 'Avenger shuriken catapult', 0, 2,
                      get_cost(ranged.AvengerShurikenCatapult)),
                Count(parent, 'Death spinner', 0, 2,
                      get_cost(ranged.DeathSpinner)),
                Count(parent, 'Power sword', 0, 2,
                      get_cost(melee.PowerSword)),
                Count(parent, 'Scorpion chainsword', 0, 2,
                      get_cost(melee.ScorpionChainsword))
            ]

        def check_rules(self):
            super(JumpingAutarch.AutarchWeapon, self).check_rules()

    def __init__(self, parent):
        super(JumpingAutarch, self).__init__(parent, gear=[Gear('Sunburst grenades')], points=73)
        self.Mask(self)
        self.Pistol(self)
        self.AutarchWeapon(self)


class Farseer(HqUnit, armory.CraftworldUnit):
    type_name = get_name(units.Farseer)
    type_id = 'farseer_v1'
    faction = ['Warhost']
    keywords = ['Character', 'Infantry', 'Psyker']
    model_gear = [ranged.ShurikenPistol]
    power = 6

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Farseer.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*melee.Witchblade)
            self.variant(*ranged.SingingSpear)

    def __init__(self, parent):
        super(Farseer, self).__init__(parent, gear=create_gears(
                                              *Farseer.model_gear
                                          ), points=get_cost(units.Farseer))
        self.Weapon(self)


class BikerFarseer(HqUnit, armory.CraftworldUnit):
    type_name = get_name(units.FarseerSkyrunner)
    type_id = 'farseer_biker_v1'
    faction = ['Warhost']
    keywords = ['Character', 'Biker', 'Psyker', 'Fly', 'Farseer']
    model_gear = [ranged.ShurikenPistol, ranged.TwinShurikenCatapult]
    power = 7

    def __init__(self, parent):
        cost = points_price(get_cost(units.FarseerSkyrunner), *BikerFarseer.model_gear)
        super(BikerFarseer, self).__init__(parent, points=cost,
                                           gear=create_gears(*BikerFarseer.model_gear))
        Farseer.Weapon(self)


class WarlockConclave(HqUnit, armory.CraftworldUnit):
    type_name = get_name(units.WarlockConclave)
    type_id = 'warlocks_v1'
    faction = ['Warhost']
    keywords = ['Infantry', 'Psyker', 'Warlock']

    model_gear = [ranged.ShurikenPistol]
    model = UnitDescription('Warlock', options=create_gears(model_gear))

    class Warlock(Count):
        @property
        def description(self):
            return [WarlockConclave.model.clone().add(Gear(melee.Witchblade[0])).set_count(self.cur - self.parent.spear.cur)]

    def __init__(self, parent):
        super(WarlockConclave, self).__init__(parent)
        cost = points_price(get_cost(units.WarlockConclave), *WarlockConclave.model_gear)
        self.models = self.Warlock(self, 'Warlocks', 2, 10, cost, per_model=True)
        self.spear = Count(self, 'Singing spear', 0, 10, get_cost(ranged.SingingSpear),
                           gear=WarlockConclave.model.clone().add(Gear(ranged.SingingSpear[0])))

    def check_rules(self):
        super(WarlockConclave, self).check_rules()
        self.spear.max = self.models.cur
    
    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 3 + 2 * (self.models.cur - 2)


class Warlock(HqUnit, armory.CraftworldUnit):
    type_name = 'Warlock'
    type_id = 'warlock_v1'
    faction = ['Warhost']
    keywords = ['Character', 'Infantry', 'Psyker']
    power = 2

    def __init__(self, parent):
        super(Warlock, self).__init__(parent, gear=create_gears([ranged.ShurikenPistol]), points=get_cost(units.Warlock))
        Farseer.Weapon(self)


class BikerWarlockConclave(HqUnit, armory.CraftworldUnit):
    type_name = 'Warlock Skyrunner Conclave'
    type_id = 'warlocks_bikers_v1'
    faction = ['Warhost']
    keywords = ['Biker', 'Psyker', 'Warlock', 'Fly']

    model_gear = [ranged.ShurikenPistol, ranged.TwinShurikenCatapult]

    model = UnitDescription('Warlock Skyrunner', options=create_gears(*model_gear))

    class Warlock(Count):
        @property
        def description(self):
            return [BikerWarlockConclave.model.clone().add(Gear(melee.Witchblade[0])).set_count(self.cur - self.parent.spear.cur)]

    def __init__(self, parent):
        super(BikerWarlockConclave, self).__init__(parent)
        cost = points_price(get_cost(units.WarlockSkyrunnerConclave), *BikerWarlockConclave.model_gear)
        self.models = self.Warlock(self, 'Warlock Skyrunners', 2, 10, cost, per_model=True)
        self.spear = Count(self, 'Singing spear', 0, 10, get_cost(ranged.SingingSpear),
                           gear=BikerWarlockConclave.model.clone().add(Gear(ranged.SingingSpear[0])))

    def check_rules(self):
        super(BikerWarlockConclave, self).check_rules()
        self.spear.max = self.models.cur

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return 7 + 2 * (self.models.cur - 2)


class BikerWarlock(HqUnit, armory.CraftworldUnit):
    type_name = 'Warlock Skyrunner'
    type_id = 'warlock_biker_v1'
    faction = ['Warhost']
    keywords = ['Character', 'Infantry', 'Psyker', 'Fly', 'Warlock']
    power = 5

    model_gear = [ranged.ShurikenPistol, ranged.TwinShurikenCatapult]

    def __init__(self, parent):
        cost = points_price(get_cost(units.WarlockSkyrunner), *BikerWarlockConclave.model_gear)
        super(BikerWarlock, self).__init__(parent, gear=create_gears(*BikerWarlock.model_gear), points=cost)
        Farseer.Weapon(self)


class Spiritseer(HqUnit, armory.CraftworldUnit):
    type_name = 'Spiritseer'
    type_id = 'spiritseer_v1'
    faction = ['Spirit Host']
    keywords = ['Character', 'Infantry', 'Psyker']
    power = 3

    model_gear = [ranged.ShurikenPistol, melee.WitchStaff]

    def __init__(self, parent):
        cost = points_price(get_cost(units.Spiritseer), *Spiritseer.model_gear)
        super(Spiritseer, self).__init__(parent, gear=create_gears(Spiritseer.model_gear), points=cost)
