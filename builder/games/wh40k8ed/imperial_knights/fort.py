from . import armory
from . import units
from builder.games.wh40k8ed.unit import FortUnit
from builder.games.wh40k8ed.utils import *


class SacristanForgeshrine(FortUnit, armory.ImperialKnightUnit):
    type_name = get_name(units.SacristanForgeshrine)
    type_id = 'sacristan_forgeshrine_v1'
    keywords = ['Sector Mechanicus']
    power = 4

    def __init__(self, parent):
        super(SacristanForgeshrine, self).__init__(parent, points=get_cost(units.SacristanForgeshrine),
                                         static=True)
