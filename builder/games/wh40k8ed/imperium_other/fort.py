__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FortUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear,\
    UnitDescription, OptionsList, Count


class AegisDefenseLine(FortUnit, Unit):
    type_name = 'Aegis Defense Line'
    type_id = 'adl_v1'
    faction = ['Unaligned']

    keywords = ['Vehicle', 'Gun emplacement']
    power = 4

    class Emplacement(OneOf):
        def __init__(self, parent):
            super(AegisDefenseLine.Emplacement, self).__init__(parent, 'Gun emplacement') 
            self.variant('Icarus lascannon', 25)
            self.variant('Quad-gun', 30)

    def __init__(self, parent):
        super(AegisDefenseLine, self).__init__(parent, points=75)
        self.Emplacement(self)


class ImperialBastion(FortUnit, Unit):
    type_name = 'Imperial Bastion'
    type_id = 'ibastion_v1'
    faction = ['Unaligned']

    keywords = ['Building', 'Vehicle', 'Transport']

    power = 10

    class Emplacement(OptionsList):
        def __init__(self, parent):
            super(ImperialBastion.Emplacement, self).__init__(parent, 'Gun emplacement',
                                                           limit=1)
            self.variant('Icarus lascannon', 25)
            self.variant('Quad-gun', 30)

    def __init__(self, parent):
        super(ImperialBastion, self).__init__(parent, points=160 + 8 * 4,
                                              gear=Gear('Heavy bolter', count=4))
        self.Emplacement(self)


class ImperialDefenseLine(FortUnit, Unit):
    type_name = 'Imperial Defence Line'
    type_id = 'wom_line_v1'
    faction = ['Unaligned']

    keywords = ['Wall of Martyrs']
    power = 4

    def __init__(self, parent):
        super(ImperialDefenseLine, self).__init__(parent, points=85, static=True)


class ImperialBunker(FortUnit, Unit):
    type_name = 'Imperial Bunker'
    type_id = 'wom_bunker_v1'
    faction = ['Unaligned']

    keywords = ['Wall of Martyrs', 'Building', 'Vehicle', 'Transport']
    power = 4

    def __init__(self, parent):
        super(ImperialBunker, self).__init__(parent, points=100)
        ImperialBastion.Emplacement(self)


class VengeanceBatteries(FortUnit, Unit):
    type_name = 'Vengeance Weapon Batteries'
    type_id = 'wom_battery_v1'
    faction = ['Unaligned']

    keywords = ['Wall of Martyrs', 'Building', 'Vehicle']

    model = UnitDescription('Vengeance Weapon Battery')

    class Gun(OneOf):
        def __init__(self, parent, skippable=True):
            super(VengeanceBatteries.Gun, self).__init__(parent, 'Vengeance Weapon Battery')
            self.empty = None
            if skippable:
                self.empty = self.variant('<Not taken>', gear=[])
            self.variant('Punisher gatling cannon', points=20 + 80,
                         gear=parent.member.clone().add(Gear('Punisher gatling cannon')))
            self.variant('Battle cannon', points=30 + 80,
                         gear=parent.member.clone().add(Gear('Battle cannon')))
            self.variant('Quad Icarus lascannon', points=70 + 80,
                         gear=parent.member.clone().add(Gear('Quad Icarus lascannon')))

    def __init__(self, parent):
        super(VengeanceBatteries, self).__init__(parent)
        self.Gun(self)
        self.second = self.Gun(self, True)

    def get_count(self):
        return 1 + (self.second.cur != self.second.empty)

    def build_power(self):
        return 6 * self.get_count()


class FirestormRedoubt(FortUnit, Unit):
    type_name = 'Firestorm Redoubt'
    type_id = 'wom_firestorm_v1'
    faction = ['Unaligned']

    keywords = ['Wall of Martyrs', 'Transport', 'Building', 'Vehicle']
    power = 15

    class Gun(OneOf):
        def __init__(self, parent):
            super(FirestormRedoubt.Gun, self).__init__(parent, 'Weapon')
            self.variant('Quad Icarus lascannon', 70)
            self.variant('Punisher gatling cannon', 20)
            self.variant('Battle cannon', 30)

    def __init__(self, parent):
        super(FirestormRedoubt, self).__init__(parent, points=160)
        self.Gun(self)
        self.Gun(self)


class PlasmaObliterator(FortUnit, Unit):
    type_name = 'Plasma Obliterator'
    type_id = 'plasma_obliterator_v1'
    faction = ['Unaligned']

    keywords = ['Transport', 'Building', 'Vehicle']
    power = 9

    def __init__(self, parent):
        super(PlasmaObliterator, self).__init__(parent, points=150 + 40, static=True,
                                                gear=[Gear('Plasma obliterator')])


class CannonAquila(FortUnit, Unit):
    type_name = 'Macro-cannon Aquila Strongpoint'
    type_id = 'cannon_aquila_v1'
    kwname = 'Macro-cannon'
    faction = ['Unaligned']

    keywords = ['Transport', 'Building', 'Vehicle', 'Wall of Martyrs', 'Aquila strongpoint']
    power = 20

    def __init__(self, parent):
        super(CannonAquila, self).__init__(parent, points=330 + 80,
                                           gear=[Gear('Aquila macro-cannon')])
        Count(self, 'Heavy bolter', 0, 4, 8)


class MissileAquila(FortUnit, Unit):
    type_name = 'Vortex Missile Aquila Strongpoint'
    type_id = 'vortex_aquila_v1'
    kwname = 'Vortex missile'
    faction = ['Unaligned']

    keywords = ['Transport', 'Building', 'Vehicle', 'Wall of Martyrs', 'Aquila strongpoint']
    power = 21

    def __init__(self, parent):
        super(MissileAquila, self).__init__(parent, points=330 + 100,
                                           gear=[Gear('Vortex missile battery')])
        Count(self, 'Heavy bolter', 0, 4, 8)


class VoidShield(FortUnit, Unit):
    type_name = 'Void Shield Generator'
    type_id = 'vsg_v1'
    faction = ['Unaligned']

    keywords = ['Building', 'Vehicle']
    power = 9

    def __init__(self, parent):
        super(VoidShield, self).__init__(parent, points=190, static=True)


class LandingPad(FortUnit, Unit):
    type_name = 'Skyshield Landing Pad'
    type_id = 'slp_v1'
    faction = ['Unaligned']

    keywords = ['Building', 'Vehicle']
    power = 6

    def __init__(self, parent):
        super(LandingPad, self).__init__(parent, points=110, static=True)


class RedemptionFortress(FortUnit, Unit):
    type_name = 'Fortress of Redemption'
    type_id = 'red_fortress_v1'
    faction = ['Unaligned']

    keywords = ['Building', 'Vehicle', 'Transport']

    power = 20

    def __init__(self, parent):
        super(RedemptionFortress, self).__init__(parent,
                                                 gear=[Gear('Twin Icarus lascannon'),
                                                       Gear('Redemption missile silo')])
        Count(self, 'Heavy bolter', 0, 4, 8)
