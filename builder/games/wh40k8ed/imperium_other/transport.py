__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import Gear, UnitDescription, OptionsList
from . import armory


class NullRhino(TransportUnit, armory.SSisterUnit):
    type_name = 'Null-Maiden Rhino'
    type_id = 'null_rhino_v1'
    keywords = ['Vehicle', 'Transport', 'Rhino']
    power = 4

    class Options(OptionsList):
        def __init__(self, parent):
            super(NullRhino.Options, self).__init__(parent, 'Options')
            self.variant('Hunter-killer missile', 6)

    def __init__(self, parent):
        super(NullRhino, self).__init__(parent, points=60 + 2,
                                        gear=[Gear('Storm bolter')])
        self.Options(self)
