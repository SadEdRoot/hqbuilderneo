from .hq import Greyfax, Karamazov, Coteaz, Inquisitor, TermoMalleus, Eisenhorn,\
    JanusDraik, Taddeus
from .troops import CustodianSquad
# from lords import KnightCrusader, KnightErrant, KnightGallant,\
#     KnightPaladin, KnightWarden, ArmigerWarglaives
from .fort import AegisDefenseLine, ImperialBastion, ImperialDefenseLine,\
    ImperialBunker, VengeanceBatteries, FirestormRedoubt, PlasmaObliterator,\
    CannonAquila, MissileAquila, VoidShield, LandingPad
from .elites import VindicareAssasin, CallidusAssasin,\
    EversorAssasin, CulexusAssasin, Acolytes, Jokaero,\
    Daemonhost, Prosecutors, Vigilators, Witchseekers,\
    EspernLocarno, PiousVorne, UR25, ReinRaus
from .transport import NullRhino

from builder.games.wh40k8ed.sections import TroopsSection, UnitType

from builder.games.wh40k8ed.rosters import DetachVanguard


class SilenceVanguard(DetachVanguard):
    army_name = 'Sisters of silence (Vanguard detachment)'
    faction_base = 'SISTERS OF SILENCE'
    army_id = 'vanguard_sisters_of_silence'
    army_factions = ['IMPERIUM', 'ASTRA TELEPATHICA', 'SISTERS OF SILENCE']

    def __init__(self, parent=None):
        super(SilenceVanguard, self).__init__(parent=parent, hq=False, transports=True)
        self.elite.add_classes([Prosecutors, Vigilators, Witchseekers])
        self.transports.add_classes([NullRhino])
        self.command = 0


class AssassinVanguard(DetachVanguard):
    army_name = 'Officio Assassinorum (Vanguard detachment)'
    faction_base = 'OFFICIO ASSASSINORUM'
    army_id = 'vanguard_officio_assassinorum'
    army_factions = ['IMPERIUM', 'OFFICIO ASSASSINORUM']

    def __init__(self, parent=None):
        super(AssassinVanguard, self).__init__(parent=parent, hq=False)
        self.elite.add_classes([VindicareAssasin, CallidusAssasin, EversorAssasin, CulexusAssasin])

    def check_rules(self):
        super(AssassinVanguard, self).check_rules()
        # Execution force
        self.command = all(t.count == 1 for t in self.elite.types)


class CustTroops(TroopsSection):
    def __init__(self, *args, **kwargs):
        super(CustTroops, self).__init__(*args, **kwargs)
        self.cust_types = []

    def unit_type_add(self, ut):
        if ut == CustodianSquad:
            self.cust_types += [UnitType(self, CustodianSquad)]
        else:
            super(CustTroops, self).unit_type_add(ut)

    def count_vexilla(self):
        res = 0
        for ut in self.cust_types:
            for un in ut.units:
                res += un.count_vexilla()
        return res

    def check_rules(self):
        super(CustTroops, self).check_rules()
        vc = self.count_vexilla()
        if vc > 1:
            self.error('No more then one Custodes Vexilla can be taken per detachment; taken: {}'.format(vc))


unit_types = [VindicareAssasin, CallidusAssasin, EversorAssasin,
              CulexusAssasin, Greyfax, Karamazov, Coteaz, Inquisitor,
              TermoMalleus, Acolytes, Jokaero, Daemonhost,
              CustodianSquad, Prosecutors, Vigilators, Witchseekers,
              NullRhino, Eisenhorn, JanusDraik, PiousVorne, Taddeus,
              UR25, ReinRaus]

fortification_types = [AegisDefenseLine, ImperialBastion,
                       ImperialDefenseLine, ImperialBunker,
                       VengeanceBatteries, FirestormRedoubt,
                       PlasmaObliterator, CannonAquila, MissileAquila,
                       VoidShield, LandingPad]
