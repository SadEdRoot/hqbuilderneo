from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from builder.games.wh40k8ed.unit import HqUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription
from . import armory
from . import ia_ranged
from . import ia_units
from . import ia_wargear
from . import ranged, wargear


class ShasOrMyr(HqUnit, armory.TauUnit):
    type_name = get_name(ia_units.ShasORMyr) + ' (Imperial Armour)'
    type_id = 'shasormyr_v1'
    faction = ["DAL'YTH SEPT"]
    keywords = ['Battlesuit', 'Fly', 'Character', 'Commander', 'Jet Pack']
    power = 7

    def __init__(self, parent):
        gear = [ia_ranged.DoubleBarrelledPlasmaRifle, ia_ranged.MiniaturisedFletchettePod]
        super(ShasOrMyr, self).__init__(parent=parent, name=get_name(ia_units.ShasORMyr), unique=True,
                                        points=points_price(get_cost(ia_units.ShasORMyr), *gear),
                                        static=True, gear=create_gears(*gear))


class ShasOrAlai(HqUnit, armory.TauUnit):
    type_name = get_name(ia_units.ShasORAlai) + ' (Imperial Armour)'
    type_id = 'shasoralai_v1'
    faction = ["KE'LSHAN SEPT"]
    keywords = ['Battlesuit', 'Fly', 'Character', 'Commander', 'Jet Pack']
    power = 10

    def __init__(self, parent):
        gear = [ia_ranged.ExperimentalPulseSubmunitionsRifle, ia_wargear.BlacklightMarkerDrones,
                ia_wargear.BlacklightMarkerDrones, ia_ranged.Markerlight, ia_ranged.Markerlight]
        super(ShasOrAlai, self).__init__(parent=parent, unique=True, name=get_name(ia_units.ShasORAlai),
                                         points=points_price(get_cost(ia_units.ShasORAlai), *gear),
                                         static=True, gear=[
                                             Gear(*ia_ranged.ExperimentalPulseSubmunitionsRifle),
                                             UnitDescription(get_name(ia_wargear.BlacklightMarkerDrones),
                                                             options=[Gear(get_name(ia_ranged.Markerlight))],
                                                             count=2)])


class XV81Commander(HqUnit, armory.SeptUnit):
    type_name = get_name(ia_units.Xv81Commander) + ' (Imperial Armour)'
    type_id = 'xv81_commander_v1'
    kwname = 'Commander'
    keywords = ['Battlesuit', 'Character', 'XV81 Crisis', 'Jetpack', 'Fly']

    power = 6

    def variant(self, name, points=None):
        return Count(self, name, 0, 4, points)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(XV81Commander.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*ia_ranged.BurstCannon)
            armory.add_ranged_weapons(self, False)
            self.variant(*wargear.CounterfireDefenceSystem)
            self.variant(*wargear.DroneController)
            self.variant(*wargear.MultiTracker)
            self.variant(*wargear.CrisisAdvancedTargetingSystem)
            self.variant(*wargear.CrisisTargetLock)
            self.variant(*wargear.CrisisVelocityTracker)
            self.variant(*wargear.ShieldGenerator)

    class Weapon2(armory.SupportSystem):
        def __init__(self, parent):
            super(XV81Commander.Weapon2, self).__init__(parent)
            armory.add_ranged_weapons(self)

    def __init__(self, parent):
        gear = [ranged.SmartMissileSystem]
        super(XV81Commander, self).__init__(parent, name=get_name(ia_units.Xv81Commander),
                                            points=points_price(get_cost(ia_units.Xv81Commander), *gear),
                                            gear=create_gears(*gear), unique=True)
        self.Weapon1(self)
        self.Weapon2(self)
        armory.add_drones(self)

    def check_rules(self):
        super(XV81Commander, self).check_rules()
        Count.norm_counts(0, 2, self.drones)

    def build_statistics(self):
        res = super(XV81Commander, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return self.power + any(c.cur for c in self.drones)


class XV84Commander(HqUnit, armory.SeptUnit):
    type_name = get_name(ia_units.Xv84Commander) + ' (Imperial Armour)'
    type_id = 'xv84_commander_v1'
    kwname = 'Commander'
    keywords = ['Battlesuit', 'Character', 'XV84 Crisis', 'Jetpack', 'Fly']

    power = 6

    def variant(self, name, points=None):
        return Count(self, name, 0, 4, points)

    def __init__(self, parent):
        super(XV84Commander, self).__init__(parent, name=get_name(ia_units.Xv84Commander),
                                            points=get_cost(ia_units.Xv84Commander))
        XV81Commander.Weapon1(self)
        XV81Commander.Weapon2(self)
        armory.add_drones(self)

    def check_rules(self):
        super(XV84Commander, self).check_rules()
        Count.norm_counts(0, 2, self.drones)

    def build_statistics(self):
        res = super(XV84Commander, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones)
        return res

    def build_power(self):
        return self.power + any(c.cur for c in self.drones)
