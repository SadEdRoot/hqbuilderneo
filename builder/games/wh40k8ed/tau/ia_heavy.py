__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit,\
    OptionalSubUnit
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import ranged
from . import ia_units
from . import ia_ranged
from . import ia_wargear
from . import wargear
from . import units


class Rvarna(HeavyUnit, armory.SeptUnit):
    type_name = get_name(ia_units.RVarnaBattlesuit) + ' (Imperial Armour)'
    type_id = 'xv107rvarna_v1'
    keywords = ['Battlesuit', 'Monster', 'Jetpack', 'Fly']
    power = 19

    class Options(OptionsList):
        def __init__(self, parent):
            super(Rvarna.Options, self).__init__(parent, 'Options')
            self.variant(*wargear.AdvancedTargetingSystem)
            self.variant(*wargear.StimulantInjector)

    def __init__(self, parent):
        gear = [ia_ranged.PulseSubmunitionsCannon, ia_ranged.PulseSubmunitionsCannon]
        super(Rvarna, self).__init__(parent, name=get_name(ia_units.RVarnaBattlesuit),
                                     points=points_price(get_cost(ia_units.RVarnaBattlesuit), *gear),
                                     gear=create_gears(*gear))
        self.supp = self.Options(self)
        self.drones = Count(self, get_name(units.MV84ShieldedMissileDrone), min_limit=0, max_limit=2,
                            points=points_price(get_cost(units.MV84ShieldedMissileDrone), ranged.MissilePod),
                            per_model=True, gear=UnitDescription(get_name(units.MV84ShieldedMissileDrone)))

    def build_power(self):
        return self.power + 2 * (self.drones.cur > 0)


class HeavyGunDrones(HeavyUnit, armory.SeptUnit):
    type_name = 'Heavy Gun Drones Squadron' + ' (Imperial Armour)'
    type_id = 'heavy_drones_v1'
    keywords = ['Drone', 'Fly']
    power = 3

    base_model = UnitDescription(get_name(ia_wargear.HeavyGunDrone),
                                 options=create_gears(ia_ranged.BurstCannon))

    class Drones(Count):
        @property
        def description(self):
            cnt = self.cur - self.parent.marker.cur
            if cnt == 0:
                return []
            else:
                return [HeavyGunDrones.base_model.clone().add(Gear(get_name(ia_ranged.BurstCannon))).set_count(cnt)]

    def __init__(self, parent):
        super(HeavyGunDrones, self).__init__(parent, 'Heavy Gun Drones Squadron')
        self.drones = self.Drones(self, 'Heavy Gun Drones', 2, 6,
                                  points_price(get_cost(ia_wargear.HeavyGunDrone), ia_ranged.BurstCannon, ia_ranged.BurstCannon),
                                  per_model=True)
        self.marker = Count(self, 'Markerlights', 0, 2, get_cost(ia_ranged.Markerlight) - get_cost(ia_ranged.BurstCannon),
                            gear=HeavyGunDrones.base_model.clone().add(Gear(get_name(ia_ranged.Markerlight))))

        def get_count(self):
            return self.drones.cur

        def build_power(self):
            return self.power + (self.drones.cur - 2)


class HBHammerhead(HeavyUnit, armory.SeptUnit):
    type_name = get_name(ia_units.Tx7HeavyBombardmentHammerheadGunship) + ' (Imperial Armour)'
    type_id = 'hb_hammerhead_v1'
    keywords = ['Vehicle', 'Fly', 'Hammerhead']
    power = 10

    class Weapon(OneOf):
        def __init__(self, parent):
            super(HBHammerhead.Weapon, self).__init__(parent=parent, name='Weapon')
            self.twogundrones = self.variant('Two Gun Drones', get_cost(units.MV1GunDrone) * 2,
                                             gear=[UnitDescription('MV1 Gun Drone', count=2, options=[
                                                 Gear('Pulse carbine', count=2)
                                             ])])
            self.variant('Two burst cannons', get_cost(ia_ranged.BurstCannon) * 2, gear=[
                Gear('Burst cannon', count=2)
            ])
            self.variant('Two smart missile systems', get_cost(ia_ranged.SmartMissileSystem) * 2, gear=[
                Gear('Smart missile system', count=2)
            ])

    def __init__(self, parent):
        gear = [ia_ranged.HighYieldMissilePod, ia_ranged.HighYieldMissilePod]
        cost = points_price(get_cost(ia_units.Tx7HeavyBombardmentHammerheadGunship), *gear)
        super(HBHammerhead, self).__init__(parent=parent, name=get_name(ia_units.Tx7HeavyBombardmentHammerheadGunship),
                                           points=cost, gear=create_gears(*gear))
        self.weapon = self.Weapon(self)
        self.seekermissile = Count(self, 'Seeker missile', min_limit=0, max_limit=2,
                                   points=get_cost(ranged.SeekerMissile))

    def build_statistics(self):
        res = super(HBHammerhead, self).build_statistics()
        if self.weapon.cur == self.weapon.twogundrones:
            res['Models'] += 2
        return res


class FSHammerhead(HeavyUnit, armory.SeptUnit):
    type_name = get_name(ia_units.Tx7FireSupportHammerheadGunship) + ' (Imperial Armour)'
    type_id = 'fs_hammerhead_v1'
    keywords = ['Vehicle', 'Fly', 'Hammerhead']
    power = 10

    class MainWeapon(OneOf):
        def __init__(self, parent):
            super(FSHammerhead.MainWeapon, self).__init__(parent, 'Main weapon')
            self.variant(*ia_ranged.TwinTAuPlasmaCannon)
            self.variant(*ia_ranged.TwinHeavyBurstCannon)
            self.variant(*ia_ranged.TwinFusionCannon)

    class Weapon(OneOf):
        def __init__(self, parent):
            super(FSHammerhead.Weapon, self).__init__(parent=parent, name='Weapon')
            self.twogundrones = self.variant('Two Gun Drones', get_cost(units.MV1GunDrone) * 2,
                                             gear=[UnitDescription('MV1 Gun Drone', count=2, options=[
                                                 Gear('Pulse carbine', count=2)
                                             ])])
            self.variant('Two burst cannons', get_cost(ia_ranged.BurstCannon) * 2, gear=[
                Gear('Burst cannon', count=2)
            ])
            self.variant('Two smart missile systems', get_cost(ia_ranged.SmartMissileSystem) * 2, gear=[
                Gear('Smart missile system', count=2)
            ])

    def __init__(self, parent):
        super(FSHammerhead, self).__init__(parent=parent, name=get_name(ia_units.Tx7FireSupportHammerheadGunship),
                                           points=get_cost(ia_units.Tx7FireSupportHammerheadGunship))
        self.MainWeapon(self)
        self.weapon = self.Weapon(self)
        self.seekermissile = Count(self, 'Seeker missile', min_limit=0, max_limit=2,
                                   points=get_cost(ranged.SeekerMissile))

    def build_statistics(self):
        res = super(FSHammerhead, self).build_statistics()
        if self.weapon.cur == self.weapon.twogundrones:
            res['Models'] += 2
        return res
