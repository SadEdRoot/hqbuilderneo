__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import TransportUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, \
    UnitDescription
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import units
from . import ranged


class Devilfish(TransportUnit, armory.SeptUnit):
    type_name = get_name(units.Devilfish)
    type_id = 'devilfish_v1'
    keywords = ['Vehicle', 'Transport', 'Fly']
    power = 6

    def __init__(self, parent=None):
        gear = [ranged.BurstCannon]
        cost = points_price(get_cost(units.Devilfish), *gear)
        super(Devilfish, self).__init__(parent=parent, points=cost, gear=create_gears(*gear))
        self.wep = self.Weapon(self)
        Count(self, get_name(ranged.SeekerMissile), min_limit=0, max_limit=2, points=get_cost(ranged.SeekerMissile))

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Devilfish.Weapon, self).__init__(parent=parent, name='Weapon')
            gun_gear = [ranged.PulseCarbine, ranged.PulseCarbine]

            self.twogundrones = self.variant('Two Gun Drones', get_cost(units.MV1GunDrone) * 2,
                                             gear=[UnitDescription(get_name(units.MV1GunDrone),
                                                                   options=create_gears(*gun_gear), count=2)])
            self.twinlinkedsmartmissilesystem = self.variant('Two smart missile systems',
                                                             get_cost(ranged.SmartMissileSystem) * 2, gear=[
                    Gear('Smart missile system', count=2)
                ])

    def build_statistics(self):
        res = super(Devilfish, self).build_statistics()
        if self.wep.cur == self.wep.twogundrones:
            res['Models'] += 2
        return res
