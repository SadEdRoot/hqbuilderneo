__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FlierUnit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionsList
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import ia_units
from . import ia_ranged
from . import ia_wargear


class RemoraSquadron(FlierUnit, armory.SeptUnit):
    type_name = get_name(ia_wargear.Dx6RemoraStealthDrone) + ' Squadron' + ' (Imperial armour)'
    type_id = 'remoras_v1'
    keywords = ['Drone', 'Fly']
    power = 3

    class Remora(ListSubUnit):
        type_name = get_name(ia_wargear.Dx6RemoraStealthDrone)

        def __init__(self, parent):
            gear = [ia_ranged.LongBarrelledBurstCannon, ia_ranged.LongBarrelledBurstCannon]
            cost = points_price(get_cost(ia_wargear.Dx6RemoraStealthDrone), *gear)
            super(RemoraSquadron.Remora, self).__init__(parent, points=cost, gear=create_gears(*gear))
            self.seekermissile = Count(self, 'Seeker missile', min_limit=0, max_limit=2,
                                       points=get_cost(ia_ranged.SeekerMissile))

    def __init__(self, parent):
        super(RemoraSquadron, self).__init__(parent, name=get_name(ia_wargear.Dx6RemoraStealthDrone) + ' Squadron')
        self.models = UnitList(self, self.Remora, 1, 6)

    def get_count(self):
        return self.models.count

    def build_power(self):
        return self.power * self.get_count()


class TigerShark(FlierUnit, armory.SeptUnit):
    type_name = get_name(ia_units.TigerSharkFighterBomber) + ' (Imperial Armour)'
    type_id = 'tigershark_v1'
    keywords = ['Vehicle', 'Fly']
    power = 21

    def __init__(self, parent):
        gear = [ia_ranged.MissilePod, ia_ranged.BurstCannon] * 2
        cost = points_price(get_cost(ia_units.TigerSharkFighterBomber), *gear)
        super(TigerShark, self).__init__(
            parent=parent, points=cost, name=get_name(ia_units.TigerSharkFighterBomber),
            gear=create_gears(*gear)
        )
        self.Weapon2(self)
        self.Options(self)
        Count(self, 'Seeker missile', min_limit=0, max_limit=6,
              points=get_cost(ia_ranged.SeekerMissile))

    class Options(OptionsList):
        def __init__(self, parent):
            super(TigerShark.Options, self).__init__(parent=parent, name='Replace transport bay with')
            self.variant('Two skyspear missile racks', 2 * get_cost(ia_ranged.SkyspearMissileRack),
                         gear=create_gears(ia_ranged.SkyspearMissileRack) * 2)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(TigerShark.Weapon2, self).__init__(parent=parent, name='Weapons')
            self.variant('Two ion cannons', get_cost(ia_ranged.IonCannon) * 2, gear=[
                Gear('Ion cannon', count=2)
            ])
            self.variant('Two heavy burst cannons', get_cost(ia_ranged.HeavyBurstCannon) * 2, gear=[
                Gear('Heavy burst cannon', count=2)
            ])
            self.variant('Two swiftstrike railguns', get_cost(ia_ranged.SwiftstrikeRailgun) * 2, gear=[
                Gear('Swiftstrike railgun', count=2)
            ])


class Barracuda(FlierUnit, armory.SeptUnit):
    type_name = get_name(ia_units.BarracudaAx52) + ' (Imperial Armour)'
    type_id = 'barracuda_v1'
    keywords = ['Vehicle', 'Fly']
    power = 12

    def __init__(self, parent):
        gear = [ia_ranged.MissilePod, ia_ranged.MissilePod]
        cost = points_price(get_cost(ia_units.BarracudaAx52), *gear)
        super(Barracuda, self).__init__(
            parent=parent, points=cost, name=get_name(ia_units.BarracudaAx52),
            gear=create_gears(*gear)
        )
        self.Weapon(self)
        self.Weapon2(self)
        Count(self, 'Seeker missile', min_limit=0, max_limit=4,
              points=get_cost(ia_ranged.SeekerMissile))

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Barracuda.Weapon, self).__init__(parent=parent, name='Weapon')
            self.burstcannon = self.variant(*ia_ranged.HeavyBurstCannon)
            self.variant(*ia_ranged.IonCannon)
            self.variant(*ia_ranged.SwiftstrikeRailgun)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Barracuda.Weapon2, self).__init__(parent=parent, name='Wing weapons')
            self.variant('Two long-barreled burst cannons', get_cost(ia_ranged.LongBarrelledBurstCannon) * 2, gear=[
                Gear('Long-barreled burst cannon', count=2)
            ])
            self.variant('Two cyclic ion blasters', get_cost(ia_ranged.CyclicIonBlaster) * 2, gear=[
                Gear('Cyclic ion blaster', count=2)
            ])


class TigerSharkAX10(FlierUnit, armory.SeptUnit):
    type_name = get_name(ia_units.TigerSharkAx10) + ' (Imperial Armour)'
    type_id = 'tigershark_ax10_v1'
    keywords = ['Vehicle', 'Fly']
    power = 30

    def __init__(self, parent):
        gear = [ia_ranged.HeavyRailCannon, ia_ranged.MissilePod, ia_ranged.BurstCannon] * 2
        cost = points_price(get_cost(ia_units.TigerSharkAx10), *gear)
        super(TigerSharkAX10, self).__init__(
            parent=parent, points=cost, name=get_name(ia_units.TigerSharkAx10),
            gear=create_gears(*gear)
        )
        Count(self, 'Seeker missile', min_limit=0, max_limit=6,
              points=get_cost(ia_ranged.SeekerMissile))


class Orca(FlierUnit, armory.SeptUnit):
    type_name = get_name(ia_units.OrcaDropship) + ' (Imperial Armour)'
    type_id = 'orca_v1'
    keywords = ['Vehicle', 'Fly']
    power = 14

    def __init__(self, parent):
        gear = [ia_ranged.LongBarrelledBurstCannon, ia_ranged.MissilePod, ia_ranged.LongBarrelledBurstCannon]
        cost = points_price(get_cost(ia_units.OrcaDropship), *gear)
        super(Orca, self).__init__(
            parent=parent, points=cost, name=get_name(ia_units.OrcaDropship),
            gear=create_gears(*gear), static=True
        )
