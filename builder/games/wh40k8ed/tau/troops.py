__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, SubUnit, OptionalSubUnit
from builder.games.wh40k8ed.utils import get_cost, points_price, get_name, create_gears
from . import armory
from . import units
from . import ranged
from . import melee
from . import wargear


class StrikeTeam(TroopsUnit, armory.SeptUnit):
    type_name = get_name(units.StrikeTeam)
    type_id = 'strike_team_v1'
    keywords = ['Infantry']

    model_points = get_cost(units.StrikeTeam) + get_cost(ranged.PulseRifle) + get_cost(ranged.PhotonGrenades)
    model_gear = [Gear('Photon grenades')]
    model_min = 5
    model_max = 12
    model_name = 'Fire Warrior'
    model = UnitDescription(model_name, points=model_points, options=model_gear)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(StrikeTeam.Leader, self).__init__(parent=parent, name='')
            self.shasui = SubUnit(self, StrikeTeam.Shasui(parent=self))

    class ModelCount(Count):
        @property
        def description(self):
            return [StrikeTeam.model.clone().add(Gear('Pulse rifle')).set_count(self.cur - self.parent.carbine.cur)]

    class Shasui(Unit):
        type_name = 'Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent=None):
            super(StrikeTeam.Shasui, self).__init__(parent=parent, points=StrikeTeam.model_points,
                                                    gear=StrikeTeam.model_gear)
            self.Weapon(self)
            self.Options(self)

        class Weapon(OneOf):
            def __init__(self, parent):
                super(StrikeTeam.Shasui.Weapon, self).__init__(parent=parent, name='Weapon')
                self.pulserifle = self.variant(*ranged.PulseRifle)
                self.pulsecarbine = self.variant(*ranged.PulseCarbine)
                self.variant(*ranged.PulsePistol)

        class Options(OptionsList):
            def __init__(self, parent):
                super(StrikeTeam.Shasui.Options, self).__init__(parent=parent, name='Options', limit=None)
                self.variant(*ranged.Markerlight)
                self.variant(*ranged.PulsePistol)

    class Turret(OptionsList):
        def __init__(self, parent):
            super(StrikeTeam.Turret, self).__init__(parent, name='Tactical support turret', limit=1)
            self.variant('Tactical support turret with missile pod',
                         points=get_cost(units.DS8TacticalSupportTurret) + get_cost(ranged.MissilePod), gear=[
                UnitDescription('DS8 Tactical Support Turret', options=[Gear('Missile pod')])
            ])
            self.variant('Tactical support turret with smart missile system',
                         points=get_cost(units.DS8TacticalSupportTurret) + get_cost(ranged.SmartMissileSystem), gear=[
                UnitDescription('DS8 Tactical Support Turret', options=[Gear('Smart missile system')])
            ])

    def __init__(self, parent):
        super(StrikeTeam, self).__init__(parent=parent)
        self.leader = self.Leader(self)
        self.warrior = self.ModelCount(self, self.model_name, min_limit=self.model_min, max_limit=self.model_max,
                                       points=StrikeTeam.model_points)
        self.carbine = Count(self, 'Pulse carbine', min_limit=0, max_limit=self.model_min,
                             gear=self.model.clone().add(Gear('Pulse carbine')))
        self.Turret(self)
        armory.add_drones(self)
        self.guard = Count(self, 'Guardian Drone', 0, 1, points=get_cost(units.MV36GuardianDrone),
                           gear=UnitDescription('MV36 Guardian Drone'))

    def check_rules(self):
        super(StrikeTeam, self).check_rules()
        self.warrior.min = self.model_min - self.leader.count
        self.warrior.max = self.model_max - self.leader.count
        self.carbine.max = self.warrior.cur
        Count.norm_counts(0, 2 - self.guard.cur, self.drones)

    def get_count(self):
        return self.warrior.cur + self.leader.count

    def build_statistics(self):
        res = super(StrikeTeam, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones + [self.guard])
        return res

    def build_power(self):
        return 2 + (3 if self.get_count() > 10 else
                    (2 if self.count > 5 else 0)) + (sum(c.cur for c in (self.drones + [self.guard])) > 0)


class BreacherTeam(TroopsUnit, armory.SeptUnit):
    type_name =get_name(units.BreacherTeam)
    type_id = 'breacher_team_v1'
    keywords = ['Infantry']

    model_points = get_cost(units.BreacherTeam) + get_cost(ranged.PulseBlaster) + get_cost(ranged.PhotonGrenades)
    model_gear = [Gear('Photon grenades'), Gear('Pulse blaster')]
    model_min = 5
    model_max = 10
    model_name = 'Fire Warrior'
    model = UnitDescription(model_name, points=model_points, options=model_gear)

    class Leader(OptionalSubUnit):
        def __init__(self, parent):
            super(BreacherTeam.Leader, self).__init__(parent=parent, name='')
            self.shasui = SubUnit(self, BreacherTeam.Shasui(parent=self))

    class Shasui(Unit):
        type_name = 'Shas\'ui'
        type_id = 'shasui_v1'

        def __init__(self, parent=None):
            super(BreacherTeam.Shasui, self).__init__(parent=parent, points=BreacherTeam.model_points,
                                                      gear=BreacherTeam.model_gear)
            StrikeTeam.Shasui.Options(self)

    def __init__(self, parent):
        super(BreacherTeam, self).__init__(parent=parent)
        self.leader = self.Leader(self)
        self.warrior = Count(self, self.model_name, min_limit=self.model_min, max_limit=self.model_max,
                             points=BreacherTeam.model_points, per_model=True, gear=self.model.clone())
        StrikeTeam.Turret(self)
        armory.add_drones(self)
        self.guard = Count(self, 'Guardian Drone', 0, 1, points=get_cost(units.MV36GuardianDrone),
                           gear=UnitDescription('MV36 Guardian Drone'))

    def check_rules(self):
        super(BreacherTeam, self).check_rules()
        self.warrior.min = self.model_min - self.leader.count
        self.warrior.max = self.model_max - self.leader.count
        Count.norm_counts(0, 2 - self.guard.cur, self.drones)

    def get_count(self):
        return self.warrior.cur + self.leader.count

    def build_statistics(self):
        res = super(BreacherTeam, self).build_statistics()
        res['Models'] += sum(c.cur for c in self.drones + [self.guard])
        return res

    def build_power(self):
        return 2 + (3 if self.get_count() > 10 else
                    (2 if self.count > 5 else 0)) + (sum(c.cur for c in (self.drones + [self.guard])) > 0)


class KrootSquad(TroopsUnit, armory.TauUnit):
    type_name = get_name(units.KrootCarnivores)
    type_id = 'kroot_v1'
    faction = ['Kroot']
    keywords = ['Infantry']
    model_points = get_cost(units.KrootCarnivores) + get_cost(ranged.KrootRifle)

    def __init__(self, parent):
        super(KrootSquad, self).__init__(parent=parent)
        self.kroot = Count(
            self, 'Kroot', min_limit=10, max_limit=20, points=self.model_points,
            gear=UnitDescription('Kroot', options=[Gear('Kroot rifle')])
        )

    def get_count(self):
        return self.kroot.cur

    def build_power(self):
        return 3 * (1 + (self.kroot.cur > 10))
