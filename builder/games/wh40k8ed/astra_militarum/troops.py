__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import TroopsUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription,\
    ListSubUnit, UnitList, OptionalSubUnit, SubUnit, OptionsList
from . import armory, units, melee, ranged, wargear
from builder.games.wh40k8ed.utils import *


class WeaponTeam(Unit):
    type_name = 'Heavy Weapons Team'

    class Weapon(OneOf):
        def __init__(self, parent):
            super(WeaponTeam.Weapon, self).__init__(parent, 'Heavy weapon')
            armory.add_heavy_weapons(self)

    def __init__(self, parent):
        super(WeaponTeam, self).__init__(parent, points=2 * get_cost(units.InfantrySquad),
                                         gear=[Gear('Frag grenades'), Gear('Lasgun')])
        self.wep = self.Weapon(self)


class InfantrySquad(TroopsUnit, armory.RegimentUnit):
    type_name = get_name(units.InfantrySquad)
    type_id = 'infantry_squad_v1'

    keywords = ['Infantry']
    power = 3

    member = UnitDescription('Guardsman', options=[Gear('Frag grenades')])

    class Sergeant(Unit):
        type_name = 'Sergeant'

        class Weapon1(OptionsList):
            def __init__(self, parent):
                super(InfantrySquad.Sergeant.Weapon1, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Chainsword)
                self.variant(*melee.PowerSword)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(InfantrySquad.Sergeant.Weapon2, self).__init__(parent, 'Pistol')
                self.variant(*ranged.Laspistol)
                armory.add_ranged_weapons(self)

        def __init__(self, parent):
            super(InfantrySquad.Sergeant, self).__init__(parent,
                                                         gear=[Gear('Frag grenades')], points=get_cost(units.InfantrySquad))
            self.Weapon1(self)
            self.Weapon2(self)

    class SpecWeapon(OptionsList):
        def variant(self, name, points=None):
            return super(InfantrySquad.SpecWeapon, self).variant(name, points,
                                                                 gear=InfantrySquad.member.clone().add(Gear(name)))

        def __init__(self, parent, name):
            super(InfantrySquad.SpecWeapon, self).__init__(parent, name=name)
            super(InfantrySquad.SpecWeapon, self).variant(
                *wargear.VoxCaster,
                gear=InfantrySquad.member.clone().add(Gear('Vox-caster')).add(Gear('Lasgun'))
            )
            armory.add_special_weapons(self)

        def check_rules(self):
            super(InfantrySquad.SpecWeapon, self).check_rules()
            self.process_limit(self.special, 1)

        @property
        def description(self):
            desk = super(InfantrySquad.SpecWeapon, self).description
            count = 9 - self.count - 2 * self.parent.members.count
            return desk + [InfantrySquad.member.clone().add(Gear('Lasgun')).set_count(count)]

    class Members(OptionalSubUnit):
        def __init__(self, parent):
            super(InfantrySquad.Members, self).__init__(parent, name='')
            self.heavy = SubUnit(self, WeaponTeam(parent))

    def __init__(self, parent):
        super(InfantrySquad, self).__init__(parent)
        SubUnit(self, self.Sergeant(self))
        self.spec = self.SpecWeapon(self, name='Options')
        self.members = self.Members(self)

    def build_points(self):
        return super(InfantrySquad, self).build_points() +\
            get_cost(units.InfantrySquad) * (9 - 2* self.members.any)
    
    def get_count(self):
        return 10


class Conscripts(TroopsUnit, armory.RegimentUnit):
    type_name = get_name(units.Conscripts)
    type_id = 'conscripts_v1'

    keywords = ['Infantry']
    power = 3

    def __init__(self, parent):
        super(Conscripts, self).__init__(parent)
        self.models = Count(self, 'Conscripts', 20, 30, per_model=True,
                            points=get_cost(units.Conscripts),
                            gear=UnitDescription('Conscript', options=[Gear('Frag grenades'), Gear('Lasgun')]))

    def get_count(self):
        return self.models.cur

    def build_power(self):
        return self.power + (self.models.cur > 20)


class TempestusSquad(TroopsUnit, armory.IGUnit):
    type_name = get_name(units.MilitarumTempestusScions)
    type_id = 'tempestus_squad_v1'

    keywords = ['Infantry']
    faction = ['Militarum Tempestus']

    member = UnitDescription('Tempestus Scion',
                             options=[Gear('Frag grenades'), Gear('Krak grenades')])
    power = 3

    class Sergeant(Unit):
        type_name = 'Tempestor'

        class Weapon1(OneOf):
            def __init__(self, parent):
                super(TempestusSquad.Sergeant.Weapon1, self).__init__(parent, 'Melee weapon')
                self.variant(*melee.Chainsword)
                armory.add_melee_weapons(self)

        class Weapon2(OneOf):
            def __init__(self, parent):
                super(TempestusSquad.Sergeant.Weapon2, self).__init__(parent, 'Pistol')
                self.variant(*ranged.HotShotLaspistol)
                self.variant(*ranged.BoltPistol)
                self.variant(*ranged.PlasmaPistol)

        def __init__(self, parent):
            super(TempestusSquad.Sergeant, self).__init__(
                parent, gear=[
                    Gear('Frag grenades'), Gear('Krak grenades')],
                points=get_cost(units.MilitarumTempestusScions))
            self.Weapon1(self)
            self.Weapon2(self)

    class BaseWeapon(OneOf):
        def __init__(self, parent):
            super(TempestusSquad.BaseWeapon, self).__init__(parent, 'Weapon')
            self.variant(*ranged.HotShotLasgun)
            self.variant(*ranged.Flamer)
            self.variant(*ranged.MeltagunPrecise)
            self.variant(*ranged.PlasmaGunPrecise)
            self.variant(*ranged.GrenadeLauncher)
            self.variant(*ranged.HotShotVolleyGun)

    class Options(OptionsList):
        def __init__(self, parent):
            super(TempestusSquad.Options, self).__init__(parent, 'Options')
            self.vox = self.variant(
                join_and(wargear.VoxCaster, ranged.HotShotLaspistol),
                get_costs(wargear.VoxCaster, ranged.HotShotLaspistol),
                gear=create_gears(wargear.VoxCaster, ranged.HotShotLaspistol))

    def __init__(self, parent):
        super(TempestusSquad, self).__init__(parent)
        self.ldr = SubUnit(self, self.Sergeant(parent=self))
        self.models = Count(self, 'Tempestus Scions', 4, 9,
                            points=get_cost(units.MilitarumTempestusScions),
                            per_model=True)
        self.opt = self.Options(self)
        self.wep = [self.BaseWeapon(self) for i in range(0, 4)]

    def get_count(self):
        return 1 + self.models.cur

    def build_power(self):
        return self.power + 2 * (self.models.cur > 4)

    def build_points(self):
        # calc hot-shot lasgun points
        res = super(TempestusSquad, self).build_points()
        guns = self.models.cur - self.opt.any - sum(w.used for w in self.wep)
        return res + get_cost(ranged.HotShotLasgun) * guns

    def build_description(self):
        res = UnitDescription(self.name, self.points, self.get_count())
        res.add(self.ldr.description)
        other = 0
        if self.opt.any:
            other = other + 1
            res.add(TempestusSquad.member.clone().add(self.opt.description))
        for w in self.wep:
            if w.used:
                res.add_dup(TempestusSquad.member.clone().add(w.description))
                other = other + 1
        res.add_dup(TempestusSquad.member.clone().add(Gear('Hot-shot lasgun')).set_count(self.models.cur - other))
        return res

    def check_rules(self):
        super(TempestusSquad, self).check_rules()
        for w in self.wep[2:]:
            w.used = w.visible = self.get_count() == 10
