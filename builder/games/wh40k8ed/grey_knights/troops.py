from builder.core2 import SubUnit, UnitList
from builder.games.wh40k8ed.unit import TroopsUnit, Unit, ListSubUnit
from builder.games.wh40k8ed.options import OneOf
from builder.games.wh40k8ed.utils import *
from . import armory
from . import ranged
from . import units


class StrikeSquad(TroopsUnit, armory.GKUnit):
    type_name = get_name(units.StrikeSquad)
    type_id = 'strike_squad_v1'
    keywords = ['Infantry', 'Psyker']
    power = 7

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class MeleeWeapon(OneOf):
        def __init__(self, parent):
            super(StrikeSquad.MeleeWeapon, self).__init__(parent=parent, name='Melee Weapon')
            armory.add_melee_weapons(self)

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(StrikeSquad.SpecialWeapon, self).__init__(parent=parent, name='')
            armory.add_ranged_weapons(self)

    class GreyKnight(ListSubUnit):
        def __init__(self, parent):
            super(StrikeSquad.GreyKnight, self).__init__(parent=parent, name='Grey Knight',
                                                         points=get_cost(units.StrikeSquad),
                                                         gear=create_gears(*StrikeSquad.model_gear))
            self.melee = StrikeSquad.MeleeWeapon(self)
            self.ranged = StrikeSquad.SpecialWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.ranged.cur != self.ranged.sb

        def check_rules(self):
            super(StrikeSquad.GreyKnight, self).check_rules()
            self.melee.used = self.melee.visible = not self.count_special()

    class Sergeant(Unit):
        def __init__(self, parent):
            super(StrikeSquad.Sergeant, self).__init__(parent=parent, name='Justicar',
                                                       points=get_cost(units.StrikeSquad) +
                                                              get_cost(ranged.StormBolter),
                                                       gear=create_gears(
                                                           *(StrikeSquad.model_gear + [ranged.StormBolter])
                                                       ))
            self.melee = StrikeSquad.MeleeWeapon(self)

    def __init__(self, parent):
        super(StrikeSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=self))
        self.gk = UnitList(self, self.GreyKnight, min_limit=4, max_limit=9)

    def get_count(self):
        return self.gk.count + 1

    def build_power(self):
        return self.power * (1 + self.gk.count > 4)

    def check_rules(self):
        super(StrikeSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.gk.units)
        if 5 * spec_total > self.get_count():
            self.error('In Strike squad may be only 1 special weapon per 5 models (taken: {0})'.format(spec_total))


class GKTerminatorSquad(TroopsUnit, armory.GKUnit):
    type_name = 'Grey Knights' + get_name(units.TerminatorSquad)
    type_id = 'gk_terminator_squad_v1'
    keywords = ['Infantry', 'Terminator', 'Psyker']
    power = 13

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class MeleeWeapon(OneOf):
        def __init__(self, parent):
            super(GKTerminatorSquad.MeleeWeapon, self).__init__(parent=parent, name='Melee Weapon')
            armory.add_melee_weapons(self)

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(GKTerminatorSquad.SpecialWeapon, self).__init__(parent=parent, name='')
            armory.add_term_ranged_weapons(self)

    class GreyKnightTerminator(ListSubUnit):
        def __init__(self, parent):
            super(GKTerminatorSquad.GreyKnightTerminator, self).__init__(parent=parent, name='Grey Knight Terminator',
                                                                       points=get_cost(units.TerminatorSquad),
                                                                       gear=create_gears(
                                                                           *GKTerminatorSquad.model_gear))
            self.melee = GKTerminatorSquad.MeleeWeapon(self)
            self.ranged = GKTerminatorSquad.SpecialWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.ranged.cur != self.ranged.sb

    class Sergeant(Unit):
        def __init__(self, parent):
            super(GKTerminatorSquad.Sergeant, self).__init__(parent=parent, name='Terminator Justicar',
                                                           points=get_cost(units.TerminatorSquad) +
                                                                  get_cost(ranged.StormBolter),
                                                           gear=create_gears(
                                                               *(GKTerminatorSquad.model_gear + [ranged.StormBolter])
                                                           ))
            self.melee = GKTerminatorSquad.MeleeWeapon(self)

    def __init__(self, parent):
        super(GKTerminatorSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=self))
        self.gkt = UnitList(self, self.GreyKnightTerminator, min_limit=4, max_limit=9)

    def get_count(self):
        return self.gkt.count + 1

    def build_power(self):
        return self.power * (1 + self.gkt.count > 4)

    def check_rules(self):
        super(GKTerminatorSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.gkt.units)
        if 5 * spec_total > self.get_count():
            self.error('In Terminator squad may be only 1 special weapon per 5 models (taken: {0})'.format(spec_total))
