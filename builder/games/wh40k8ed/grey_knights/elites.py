from builder.core2 import SubUnit, UnitList, UnitDescription, Gear
from builder.games.wh40k8ed.unit import ElitesUnit, Unit, ListSubUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList, Count
from builder.games.wh40k8ed.utils import *
from . import armory
from . import ranged
from . import melee
from . import units


class PurifierSquad(ElitesUnit, armory.GKUnit):
    type_name = get_name(units.PurifierSquad)
    type_id = 'purifier_squad_v1'
    keywords = ['Infantry', 'Psyker']
    power = 9

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class MeleeWeapon(OneOf):
        def __init__(self, parent):
            super(PurifierSquad.MeleeWeapon, self).__init__(parent=parent, name='Melee Weapon')
            armory.add_melee_weapons(self)

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(PurifierSquad.SpecialWeapon, self).__init__(parent=parent, name='')
            armory.add_ranged_weapons(self)

    class Purifier(ListSubUnit):
        def __init__(self, parent):
            super(PurifierSquad.Purifier, self).__init__(parent=parent, name='Purifier',
                                                         points=get_cost(units.PurifierSquad),
                                                         gear=create_gears(*PurifierSquad.model_gear))
            self.melee = PurifierSquad.MeleeWeapon(self)
            self.ranged = PurifierSquad.SpecialWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.ranged.cur != self.ranged.sb

        def check_rules(self):
            super(PurifierSquad.Purifier, self).check_rules()
            self.melee.used = self.melee.visible = not self.count_special()

    class Sergeant(Unit):
        def __init__(self, parent):
            super(PurifierSquad.Sergeant, self).__init__(parent=parent, name='Knight of the Flame',
                                                         points=get_cost(units.PurifierSquad) +
                                                                get_cost(ranged.StormBolter),
                                                         gear=create_gears(
                                                             *(PurifierSquad.model_gear + [ranged.StormBolter])
                                                         ))
            self.melee = PurifierSquad.MeleeWeapon(self)

    def __init__(self, parent):
        super(PurifierSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=self))
        self.purifier = UnitList(self, self.Purifier, min_limit=4, max_limit=9)

    def get_count(self):
        return self.purifier.count + 1

    def build_power(self):
        return self.power * (1 + self.purifier.count > 4)

    def check_rules(self):
        super(PurifierSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.purifier.units)
        if 5 * spec_total > 2 * self.get_count():
            self.error('In Purifier squad may be only 2 special weapon per 5 models (taken: {0})'.format(spec_total))


class PaladinSquad(ElitesUnit, armory.GKUnit):
    type_name = get_name(units.PaladinSquad)
    type_id = 'gk_paladin_squad_v1'
    keywords = ['Infantry', 'Paladin', 'Terminator', 'Psyker']
    power = 10

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class MeleeWeapon(OneOf):
        def __init__(self, parent):
            super(PaladinSquad.MeleeWeapon, self).__init__(parent=parent, name='Melee Weapon')
            armory.add_melee_weapons(self)

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(PaladinSquad.SpecialWeapon, self).__init__(parent=parent, name='')
            armory.add_term_ranged_weapons(self)

    class Paladin(ListSubUnit):
        def __init__(self, parent):
            super(PaladinSquad.Paladin, self).__init__(parent=parent, name='Paladin',
                                                       points=get_cost(units.PaladinSquad),
                                                       gear=create_gears(
                                                           *PaladinSquad.model_gear))
            self.melee = PaladinSquad.MeleeWeapon(self)
            self.ranged = PaladinSquad.SpecialWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.ranged.cur != self.ranged.sb

    class Sergeant(Unit):
        def __init__(self, parent):
            super(PaladinSquad.Sergeant, self).__init__(parent=parent, name='Paragon',
                                                        points=get_cost(units.PaladinSquad) +
                                                               get_cost(ranged.StormBolter),
                                                        gear=create_gears(
                                                            *(PaladinSquad.model_gear + [ranged.StormBolter])
                                                        ))
            self.melee = PaladinSquad.MeleeWeapon(self)

    def __init__(self, parent):
        super(PaladinSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=self))
        self.paladin = UnitList(self, self.Paladin, min_limit=2, max_limit=9)

    def get_count(self):
        return self.paladin.count + 1

    def build_power(self):
        if self.get_count() == 3:
            return self.power
        elif self.get_count() <= 5:
            return self.power + 9
        else:
            return self.power + 22

    def check_rules(self):
        super(PaladinSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.paladin.units)
        if 5 * spec_total > 2 * self.get_count():
            self.error('In Paladin squad may be only 2 special weapon per 5 models (taken: {0})'.format(spec_total))


class GKDreadnought(ElitesUnit, armory.GKUnit):
    type_name = 'Grey Knights ' + get_name(units.Dreadnought)
    type_id = 'gk_dreadnought_v1'
    keywords = ['Vehicle', 'Psyker']
    power = 8

    model_points = get_cost(units.Dreadnought)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(GKDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.StormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(GKDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.assaultcannon = self.variant(*ranged.AssaultCannon)
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(GKDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtCombatWeapon)
            self.missilelauncher = self.variant(*ranged.Missilelauncher)

    def __init__(self, parent):
        super(GKDreadnought, self).__init__(parent=parent, name=get_name(units.Dreadnought),
                                            points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(GKDreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class GKVenDreadnought(ElitesUnit, armory.GKUnit):
    type_name = 'Grey Knights ' + get_name(units.VenerableDreadnought)
    type_id = 'gk_ven_dreadnought_v1'
    keywords = ['Vehicle', 'Psyker']
    power = 9

    model_points = get_cost(units.VenerableDreadnought)

    class BuildIn(OneOf):
        def __init__(self, parent):
            super(GKVenDreadnought.BuildIn, self).__init__(parent=parent, name='')
            self.builtinstormbolter = self.variant(*ranged.StormBolter)
            self.builtinheavyflamer = self.variant(*ranged.HeavyFlamer)

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(GKVenDreadnought.Weapon1, self).__init__(parent=parent, name='Weapon')
            self.assaultcannon = self.variant(*ranged.AssaultCannon)
            armory.add_dred_heavy_weapons(self)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(GKVenDreadnought.Weapon2, self).__init__(parent=parent, name='')
            self.powerfist = self.variant(*melee.DreadnoughtCombatWeapon)
            self.missilelauncher = self.variant(*ranged.Missilelauncher)

    def __init__(self, parent):
        super(GKVenDreadnought, self).__init__(parent=parent,
                                               name=get_name(units.VenerableDreadnought),
                                               points=self.model_points)
        self.wep1 = self.Weapon1(self)
        self.wep2 = self.Weapon2(self)
        self.build_in = self.BuildIn(self)

    def check_rules(self):
        super(GKVenDreadnought, self).check_rules()
        self.build_in.visible = self.build_in.used = self.wep2.cur == self.wep2.powerfist


class GKApothecary(ElitesUnit, armory.GKUnit):
    type_name = get_name(units.Apothecary)
    type_id = 'gk_apothecary_v1'

    keywords = ['Character', 'Infantry', 'Terminator', 'Psyker']
    power = 4
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(GKApothecary.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*melee.NemesisSword)
            self.variant(*melee.NemesisHalberd)
            self.variant(*melee.NemesisStave)
            self.variant(*melee.NemesisFalchion)
            self.variant(*melee.NemesisDaemonHammer)

    def __init__(self, parent):
        super(GKApothecary, self).__init__(parent, 'Apothecary',
                                           points=get_cost(units.Apothecary),
                                           gear=create_gears(*self.model_gear))
        self.Weapon1(self)


class BrotherhoodAncient(ElitesUnit, armory.GKUnit):
    type_name = get_name(units.BrotherhoodAncient)
    type_id = 'gk_brotherhood_ancient_v1'

    keywords = ['Character', 'Infantry', 'Ancient', 'Terminator', 'Psyker']
    power = 7
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class Weapon1(OptionsList):
        def __init__(self, parent):
            super(BrotherhoodAncient.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*melee.NemesisFalchion)

    def __init__(self, parent):
        super(BrotherhoodAncient, self).__init__(parent, 'Brotherhood Ancient',
                                                 points=get_costs(units.BrotherhoodAncient, ranged.StormBolter),
                                                 gear=create_gears(
                                                     *(BrotherhoodAncient.model_gear + [ranged.StormBolter])
                                                 ))
        self.Weapon1(self)


class PaladinAncient(ElitesUnit, armory.GKUnit):
    type_name = get_name(units.PaladinAncient)
    type_id = 'gk_paladin_ancient_v1'

    keywords = ['Character', 'Infantry', 'Paladin', 'Ancient', 'Terminator', 'Psyker']
    power = 7
    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class Melee(OptionsList):
        def __init__(self, parent):
            super(PaladinAncient.Melee, self).__init__(parent, 'Weapon')
            self.variant(*melee.NemesisFalchion)

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(PaladinAncient.SpecialWeapon, self).__init__(parent=parent, name='')
            armory.add_term_ranged_weapons(self)

    def __init__(self, parent):
        super(PaladinAncient, self).__init__(parent, 'Paladin Ancient',
                                                 points=get_cost(units.PaladinAncient),
                                                 gear=create_gears(
                                                     *(PaladinAncient.model_gear)
                                                 ))
        self.Melee(self)
        self.SpecialWeapon(self)


class GKServitors(ElitesUnit, armory.GKUnit):
    type_name = get_name(units.Servitors)
    type_id = 'gk_servitors_v1'
    keywords = ['Infantry']

    power = 3
    model = UnitDescription('Servitor')

    class Servitor(Count):
        @property
        def description(self):
            return GKServitors.model.clone().add(create_gears(melee.ServoArm)).\
                set_count(self.cur - sum(c.cur for c in self.parent.wep.counts))

    class SpecialWeapons(object):
        def variant(self, name, point=None):
            self.counts.append(Count(
                self.parent, name, 0, 4, point, gear=GKServitors.model.clone().add(Gear(name))))

        def __init__(self, parent):
            self.parent = parent
            self.counts = []
            self.variant(*ranged.HeavyBolter)
            self.variant(*ranged.PlasmaCannon)
            self.variant(*ranged.MultiMelta)

    def __init__(self, parent):
        super(GKServitors, self).__init__(parent)
        self.squad = self.Servitor(self, 'Servitor', 4, 4,
                                   per_model=True, points=get_cost(units.Servitors))
        self.squad.visible = False
        self.wep = self.SpecialWeapons(self)

    def check_rules(self):
        super(GKServitors, self).check_rules()
        Count.norm_counts(0, 2, self.wep.counts)

    def build_points(self):
        return super(GKServitors, self).build_points() +\
                                   get_cost(melee.ServoArm) * (4 - sum(c.cur for c in self.wep.counts))
