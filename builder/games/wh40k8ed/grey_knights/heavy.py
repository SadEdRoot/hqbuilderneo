from builder.core2 import SubUnit, Count, UnitDescription, Gear, UnitList
from builder.games.wh40k8ed.unit import HeavyUnit, Unit, ListSubUnit
from builder.games.wh40k8ed.options import OneOf, OptionsList
from builder.games.wh40k8ed.utils import *
from . import armory
from . import ranged
from . import units


class Dreadknight(HeavyUnit, armory.GKUnit):
    type_id = 'dreadknight_v1'
    type_name = get_name(units.NemesisDreadknight)

    keywords = ['Vehicle', 'Psyker']
    power = 11

    def __init__(self, parent):
        super(Dreadknight, self).__init__(parent, points=get_cost(units.NemesisDreadknight))
        armory.DreadknightMeleeWeapon(self)
        armory.DreadknightRangedWeapons(self)
        armory.DreadknightOptions(self)


class PurgationSquad(HeavyUnit, armory.GKUnit):
    type_name = get_name(units.PurgationSquad)
    type_id = 'purgation_squad_v1'
    keywords = ['Infantry', 'Psyker']
    power = 7

    model_gear = [ranged.FragGrenades, ranged.KrakGrenades, ranged.PsykOutGrenades]

    class MeleeWeapon(OneOf):
        def __init__(self, parent):
            super(PurgationSquad.MeleeWeapon, self).__init__(parent=parent, name='Melee Weapon')
            armory.add_melee_weapons(self)

    class SpecialWeapon(OneOf):
        def __init__(self, parent):
            super(PurgationSquad.SpecialWeapon, self).__init__(parent=parent, name='')
            armory.add_ranged_weapons(self)

    class Purgator(ListSubUnit):
        def __init__(self, parent):
            super(PurgationSquad.Purgator, self).__init__(parent=parent, name='Purgator',
                                                          points=get_cost(units.PurgationSquad),
                                                          gear=create_gears(*PurgationSquad.model_gear))
            self.melee = PurgationSquad.MeleeWeapon(self)
            self.ranged = PurgationSquad.SpecialWeapon(self)

        @ListSubUnit.count_gear
        def count_special(self):
            return self.ranged.cur != self.ranged.sb

        def check_rules(self):
            super(PurgationSquad.Purgator, self).check_rules()
            self.melee.used = self.melee.visible = not self.count_special()

    class Sergeant(Unit):
        def __init__(self, parent):
            super(PurgationSquad.Sergeant, self).__init__(parent=parent, name='Purgator Justicar',
                                                          points=get_cost(units.PurgationSquad) +
                                                                 get_cost(ranged.StormBolter),
                                                          gear=create_gears(
                                                              *(PurgationSquad.model_gear + [ranged.StormBolter])
                                                          ))
            self.melee = PurgationSquad.MeleeWeapon(self)

    def __init__(self, parent):
        super(PurgationSquad, self).__init__(parent=parent)
        self.sergeant = SubUnit(self, self.Sergeant(parent=self))
        self.purg = UnitList(self, self.Purgator, min_limit=4, max_limit=9)

    def get_count(self):
        return self.purg.count + 1

    def build_power(self):
        return self.power * (1 + self.purg.count > 4)

    def check_rules(self):
        super(PurgationSquad, self).check_rules()
        spec_total = sum(u.count_special() for u in self.purg.units)
        if spec_total > 4:
            self.error('Purgation squad may carry no more then 4 special weapons (taken: {0})'.format(spec_total))


class GKLandRaider(HeavyUnit, armory.GKUnit):
    type_id = 'gk_land_raider_v1'
    type_name = 'Grey Knights ' + get_name(units.LandRaider)

    keywords = ['Vehicle', 'Transport']
    power = 19

    class Options(OptionsList):
        def __init__(self, parent):
            super(GKLandRaider.Options, self).__init__(parent, 'Options')
            self.variant(*ranged.HunterKillerMissile)
            self.variant(*ranged.StormBolter)
            self.variant(*ranged.MultiMelta)

    def __init__(self, parent):
        gear = [ranged.TwinHeavyBolter, ranged.TwinLascannon, ranged.TwinLascannon]
        cost = points_price(get_cost(units.LandRaider), *gear)
        super(GKLandRaider, self).__init__(parent=parent, points=cost,
                                           gear=create_gears(*gear))
        self.opt = self.Options(self)


class GKLandRaiderCrusader(HeavyUnit, armory.GKUnit):
    type_id = 'gk_land_raider_crusader_v1'
    type_name = 'Grey Knights ' + get_name(units.LandRaiderCrusader)
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 16

    def __init__(self, parent):
        gear = [ranged.TwinAssaultCannon, ranged.HurricaneBolter, ranged.HurricaneBolter]
        cost = points_price(get_cost(units.LandRaiderCrusader), *gear)
        super(GKLandRaiderCrusader, self).__init__(parent=parent, gear=create_gears(*gear),
                                                   points=cost)
        self.opt = GKLandRaider.Options(self)


class GKLandRaiderRedeemer(HeavyUnit, armory.GKUnit):
    type_id = 'gk_land_raider_redeemer_v1'
    type_name = 'Grey Knights ' + get_name(units.LandRaiderRedeemer)
    keywords = ['Vehicle', 'Transport', 'Land Raider']
    power = 18

    def __init__(self, parent):
        gear = [ranged.TwinAssaultCannon, ranged.FlamestormCannon, ranged.FlamestormCannon]
        cost = points_price(get_cost(units.LandRaiderRedeemer), *gear)
        super(GKLandRaiderRedeemer, self).__init__(parent=parent, points=cost,
                                                   gear=create_gears(*gear))
        self.opt = GKLandRaider.Options(self)
