from builder.games.wh40k8ed.unit import HeavyUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count, UnitDescription, OptionsList, ListSubUnit,\
    SubUnit, UnitList
from builder.games.wh40k8ed.utils import *
from . import armory
from . import ia_units, ia_ranged, ia_melee, ia_wargear


class MortisDreadnought(HeavyUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.MortisDreadnought) + ' (Imperial Armor)'
    type_id = 'ia_mortis_dreadnought_v1'

    keywords = ['Vehicle', 'Dreadnought']
    power = 8

    @classmethod
    def calc_faction(cls):
        # magic slicing to leave off Space Wolves
        return super(MortisDreadnought, cls).calc_faction() + ['DEATHWATCH']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(MortisDreadnought.Weapon, self).__init__(parent, 'Pair of')
            self.variant(*ia_ranged.HeavyBolter)
            self.variant(*ia_ranged.TwinAutocannon)
            self.variant(*ia_ranged.TwinLascannon)
            self.variant(*ia_ranged.MissileLauncher)
            self.variant(*ia_ranged.AssaultCannon)
            self.variant(*ia_ranged.MultiMelta)
            self.variant(*ia_ranged.HeavyPlasmaCannon)

    def __init__(self, parent):
        super(MortisDreadnought, self).__init__(parent, get_name(ia_units.MortisDreadnought),
                                                points=get_cost(ia_units.MortisDreadnought))
        self.wep = self.Weapon(self)

    def build_points(self):
        return super(MortisDreadnought, self).build_points() + self.wep.points

    def build_description(self):
        res = super(MortisDreadnought, self).build_description()
        res.add(self.wep.description)
        return res


class SiegeDreadnought(HeavyUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.SiegeDreadnought) + ' (Imperial Armor)'
    type_id = 'ia_siege_dreadnought_v1'

    keywords = ['Vehicle', 'Dreadnought']
    power = 10

    @classmethod
    def calc_faction(cls):
        # magic slicing to leave off Space Wolves
        return super(SiegeDreadnought, cls).calc_faction() + ['DEATHWATCH']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(SiegeDreadnought.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(join_and(ia_melee.SeismicHammer, ia_ranged.Meltagun),
                         get_costs(ia_melee.SeismicHammer, ia_ranged.Meltagun),
                         gear=create_gears(ia_melee.SeismicHammer, ia_ranged.Meltagun))
            self.variant(*ia_ranged.DreadnoughtInfernoCannon)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(SiegeDreadnought.Weapon2, self).__init__(parent, '')
            self.variant(*ia_ranged.DreadnoughtInfernoCannon)
            self.variant(*ia_ranged.MultiMelta)    
            self.variant(*ia_ranged.TwinLascannon)
            self.variant(*ia_ranged.TwinAutocannon)

    def __init__(self, parent):
        super(SiegeDreadnought, self).__init__(parent, get_name(ia_units.SiegeDreadnought),
                                               points=get_cost(ia_units.SiegeDreadnought))
        self.Weapon1(self)
        self.Weapon2(self)


class ContemptorMortis(HeavyUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.ContemptorMortisDreadnought) + ' (Imperial Armor)'
    type_id = 'ia_contemptor_mortis_v1'

    keywords = ['Vehicle', 'Dreadnought']
    power = 9

    @classmethod
    def calc_faction(cls):
        # magic slicing to leave off Space Wolves
        return super(ContemptorMortis, cls).calc_faction() + ['DEATHWATCH']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ContemptorMortis.Weapon, self).__init__(parent, 'Pair of')
            self.variant(*ia_ranged.HeavyBolter)
            self.variant(*ia_ranged.TwinAutocannon)
            self.variant(*ia_ranged.TwinLascannon)
            self.variant(*ia_ranged.MissileLauncher)
            self.variant(*ia_ranged.AssaultCannon)
            self.variant(*ia_ranged.MultiMelta)
            self.variant(*ia_ranged.HeavyPlasmaCannon)

    def __init__(self, parent):
        super(ContemptorMortis, self).__init__(parent, get_name(ia_units.ContemptorMortisDreadnought),
                                                points=get_cost(ia_units.ContemptorMortisDreadnought))
        self.wep = self.Weapon(self)

    def build_points(self):
        return super(ContemptorMortis, self).build_points() + self.wep.points

    def build_description(self):
        res = super(ContemptorMortis, self).build_description()
        res.add(self.wep.description)
        return res


class LeviathanDreadnought(HeavyUnit, armory.CommonSMUnit):
    type_name = get_name(ia_units.LeviathanDreadnought) + ' (Imperial Armor)'
    type_id = 'ia_leviathan_v1'

    keywords = ['Vehicle', 'Dreadnought', 'Relic']
    power = 16

    @classmethod
    def calc_faction(cls):
        # magic slicing to leave off Space Wolves
        return super(LeviathanDreadnought, cls).calc_faction() + ['DEATHWATCH']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(LeviathanDreadnought.Weapon, self).__init__(parent, 'Weapon')
            wep = [ia_melee.LeviathanSiegeClaw, ia_ranged.Meltagun]
            self.claw = self.variant(join_and(*wep), get_costs(*wep), gear=create_gears(*wep))
            wep = [ia_melee.LeviathanSiegeDrill, ia_ranged.Meltagun]
            self.drill = self.variant(join_and(*wep), get_costs(*wep), gear=create_gears(*wep))
            self.variant(*ia_ranged.StormCannonArray)
            self.variant(*ia_ranged.CyclonicMeltaLance)
            self.variant(*ia_ranged.GravFluxBombard)

    def __init__(self, parent):
        gear = [ia_ranged.Flamer, ia_ranged.Flamer]
        cost = points_price(get_costs(ia_units.LeviathanDreadnought), *gear)
        super(LeviathanDreadnought, self).__init__(parent, get_name(ia_units.LeviathanDreadnought),
                                                   points=cost, gear=create_gears(*gear))
        self.wep1 = self.Weapon(self)
        self.wep2 = self.Weapon(self)

    def build_points(self):
        res = super(LeviathanDreadnought, self).build_points()
        if self.wep1.cur == self.wep1.claw and self.wep2.cur == self.wep2.claw:
            res -= 2 * get_cost(ia_melee.LeviathanSiegeClaw) - get_cost(ia_melee.LeviathanSiegeClawPair)
        if self.wep1.cur == self.wep1.drill and self.wep2.cur == self.wep2.drill:
            res -= 2 * get_cost(ia_melee.LeviathanSiegeDrill) - get_cost(ia_melee.LeviathanSiegeDrillPair)
        return res
