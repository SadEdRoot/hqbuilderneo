__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import FastUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
from . import armory
from . import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class Enlightened(FastUnit, armory.SonsUnit):
    type_name = get_name(units.TzaangorEnlightened)
    type_id = 'tz_enlightened_v1'
    kwname = 'Enlightened'
    keywords = ['Cavalry', 'Daemon', 'Tzaangor', 'Fly']
    gearlist = [UnitDescription('Disc of Tzeentch', options=[Gear('Blades')])]

    power = 3

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Enlightened.Weapon, self).__init__(parent, 'Weapons')
            self.variant(*melee.DiviningSpear)
            self.variant(join_and(melee.Chainsword, ranged.Autopistol),
                         get_costs(melee.Chainsword, ranged.Autopistol),
                         gear=create_gears(melee.Chainsword, ranged.Autopistol))
            self.variant(*ranged.FatecasterGreatbow)

        @property
        def description(self):
            return [UnitDescription('Aviarch', options=Enlightened.gearlist).add(self.cur.gear)]

    class Riders(Count):
        @property
        def description(self):
            return [UnitDescription('Enlightened', options=Enlightened.gearlist).
                    add(self.parent.wep.cur.gear).set_count(self.cur)]

    def __init__(self, parent):
        super(Enlightened, self).__init__(parent, points=get_cost(units.TzaangorEnlightened))
        self.wep = self.Weapon(self)
        self.goats = self.Riders(self, 'Enlightened', 2, 8, get_cost(units.TzaangorEnlightened),
                                 per_model=True)

    def get_count(self):
        return 1 + self.goats.cur

    def build_points(self):
        return super(Enlightened, self).build_points() + self.wep.points * self.goats.cur

    def build_power(self):
        return self.power + 2 * (self.goats.cur / 3)
