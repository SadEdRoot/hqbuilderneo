__author__ = 'Ivan Truskov'

from builder.games.wh40k8ed.unit import Unit


class SonsUnit(Unit):
    faction = ['Chaos', 'Tzeentch', 'Heretic Astartes', 'Thousand Sons']
    wiki_faction = 'Thousand Sons'
