__author__ = 'Ivan Truskov'


from builder.games.wh40k8ed.unit import ElitesUnit, Unit
from builder.games.wh40k8ed.options import OneOf, Gear, Count,\
    UnitDescription, OptionsList, ListSubUnit, UnitList, SubUnit
from . import armory
from . import wargear, ranged, melee, units
from builder.games.wh40k8ed.utils import *


class Shaman(ElitesUnit, armory.SonsUnit):
    type_name = get_name(units.TzaangorShaman)
    type_id = 'tz_shaman_v1'
    kwname = 'Shaman'
    keywords = ['Character', 'Cavalry', 'Daemon', 'Tzaangor', 'Fly', 'Psyker']
    power = 5

    def __init__(self, parent):
        super(Shaman, self).__init__(parent, points=points_price(get_cost(units.TzaangorShaman), melee.ForceStave),
                                     gear=create_gears(melee.ForceStave) + [UnitDescription('Disc of Tzeentch', options=[Gear('Blades')])],
                                     static=True)


class ScarabOccultTerminators(ElitesUnit, armory.SonsUnit):

    type_name = get_name(units.ScarabOccultTerminators)
    type_id = 'sotermies_v1'
    keywords = ['Infantry', 'Terminator', 'Psyker']
    power = 11

    class Champion(Unit):
        type_name = 'Scarab Occult Sorcerer'

        class Bolter(OneOf):
            def __init__(self, parent):
                super(ScarabOccultTerminators.Champion.Bolter, self).__init__(parent, 'Weapon')
                self.variant(*ranged.InfernoCombiBolter)
                self.variant(*melee.PowerSword)

        def __init__(self, parent):
            super(ScarabOccultTerminators.Champion, self).__init__(parent, points=points_price(get_cost(units.ScarabOccultTerminators), melee.ForceStave),
                                                                   gear=[Gear('Force stave')])
            self.Bolter(self)

    class Terminator(ListSubUnit):
        type_name = 'Scarab Occult Terminator'

        class Bolter(OneOf):
            def __init__(self, parent):
                super(ScarabOccultTerminators.Terminator.Bolter, self).__init__(parent, 'Ranged Weapon')
                self.variant(*ranged.InfernoCombiBolter)
                self.heavy = [self.variant(*ranged.HeavyWarpflamer),
                              self.variant(*ranged.SoulreaperCannon)]

        class Rack(OptionsList):
            def __init__(self, parent):
                super(ScarabOccultTerminators.Terminator.Rack, self).__init__(parent, '')
                self.variant(*ranged.HellfyreMissileRack)

        def __init__(self, parent):
            super(ScarabOccultTerminators.Terminator, self).__init__(parent, points=points_price(get_cost(units.ScarabOccultTerminators), melee.PowerSword),
                                                                     gear=[Gear('Power sword')])
            self.rng = self.Bolter(self)
            self.rng2 = self.Rack(self)

        @ListSubUnit.count_gear
        def has_rack(self):
            return self.rng2.any

        @ListSubUnit.count_gear
        def has_heavy(self):
            return self.rng.cur in self.rng.heavy

    def __init__(self, parent):
        super(ScarabOccultTerminators, self).__init__(parent)
        self.ldr = SubUnit(self, self.Champion(parent=self))
        self.models = UnitList(self, self.Terminator, 4, 9)

    def check_rules(self):
        super(ScarabOccultTerminators, self).check_rules()
        # check mark
        heavy_max = 1 + (self.get_count() == 10)
        heavy_cnt = sum(u.has_heavy() for u in self.models.units)
        if heavy_cnt > heavy_max:
            self.error('No more then {} Scarab Occult Terminators may take heavy weapons; taken: {}'.format(heavy_max, heavy_cnt))
        rack_cnt = sum(u.has_rack() for u in self.models.units)
        if rack_cnt > heavy_max:
            self.error('No more then {} Scarab Occult Terminators may take missile racks; taken: {}'.format(heavy_max, rack_cnt))

    def get_count(self):
        return 1 + self.models.count

    def build_power(self):
        return self.power * (1 + (self.models.count > 4))
