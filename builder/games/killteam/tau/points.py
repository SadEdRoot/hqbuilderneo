__author__ = 'Ivan Truskov'

# units
ShasLa = ('Shas\'la', 8)
ShasUi = ('Shas\'ui', 8)
Ds8TacticalSupportTurret = ('DS8 Tactical Support Turret', 0)
BreacherShasLa = ('Breacher Shas\'la', 8)
BreacherShasUi = ('Breacher Shas\'ui', 8)
Pathfinder = ('Pathfinder', 6)
PathfinderGunner = ('Pathfinder Gunner', 7)
PathfinderShasUi = ('Pathfinder Shas\'ui', 7)
StealthShasUi = ('Stealth Shas\'ui', 20)
StealthShasVre = ('Stealth Shas\'vre', 20)

Kroot = ('Kroot', 6)
KrootHound = ('Kroot Hound', 6)
KrootoxRider = ('Krootox Rider', 27)

DahyakGrekh = ('Dahiak Grekh', [25, 30, 40, 50])
Fireblade = ('Cadre Fireblade', [23, 28, 43, 63])
Ethereal = ('Ethereal', [18, 23, 38, 58])
# Ranged
BurstCannon = ('Burst cannon', 0)
FusionBlaster = ('Fusion blaster', 4)
IonRifle = ('Ion rifle', 3)
Markerlight = ('Markerlight', 0)
MissilePod = ('Missile pod', 7)
PhotonGrenade = ('Photon grenade', 0)
PulseBlaster = ('Pulse blaster', 0)
PulseCarbine = ('Pulse carbine', 0)
PulsePistol = ('Pulse pistol', 0)
PulseRifle = ('Pulse rifle', 0)
RailRifle = ('Rail rifle', 5)
SmartMissileSystem = ('Smart missile system', 5)

KrootRifle = ('Kroot Rifle', 0)
KrootPistol = ('Kroot Pistol', 0)
KrootGun = ('Kroot Gun', 0)

# Melee
RippingFangs = ('Ripping fangs', 0)
KrootoxFists = ('Krootox fists', 0)

Equalizers = ('Equalizers', 1)
HonourBlade = ('Honour blade', 0)

# Drones
Mv1GunDrone = ('MV1 Gun Drone', 7)
Mv4ShieldDrone = ('MV4 Shield Drone', 7)
Mv7MarkerDrone = ('MV7 Marker Drone', 7)
Mv36GuardianDrone = ('MV36 Guardian Drone', 7)
Mv33GravInhibitorDrone = ('MV33 Grav-inhibitor Drone', 7)
Mv31PulseAcceleratorDrone = ('MV31 Pulse Accelerator Drone', 7)
Mb3ReconDrone = ('MB3 Recon Drone', 7)

HoverDrone = ('Hover drone', 5)
# wargear
TargetLock = ('Target Lock', 1)
