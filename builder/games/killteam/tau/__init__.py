__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import FireShasui, FireShasla, Ds8Turret, Pathfinder,\
    PathfinderGunner, PathfinderShasui, BreacherShasla, BreacherShasui,\
    Ds8TurretBreacher, StealthShasui, StealthShasvre, Drone,\
    Kroot, KrootHound, KrootoxRider
from .commanders import DahyakGrekh, Fireblade, Ethereal


class TauKT(KTRoster):
    army_name = 'Tau Empire Kill Team'
    army_id = 'tau_kt_v1'
    unit_types = [FireShasui, FireShasla, Ds8Turret, Pathfinder,
                  PathfinderGunner, PathfinderShasui, BreacherShasla,
                  BreacherShasui, Ds8TurretBreacher, StealthShasui,
                  StealthShasvre, Drone, Kroot, KrootHound, KrootoxRider]


class ComTauKT(KTCommanderRoster, TauKT):
    army_name = 'Tau Empire Kill Team'
    army_id = 'tau_kt_v2_com'
    commander_types = [Fireblade, Ethereal, DahyakGrekh]


class KrootKT(KTRoster):
    army_id = 'kroot_kt_v1'
    army_name = 'Kroot Kill Team'
    unit_types = [Kroot, KrootHound, KrootoxRider]


class ComKrootKT(KTCommanderRoster, KrootKT):
    army_id = 'kroot_kt_v2_com'
    army_name = 'Kroot Kill Team'
    commander_types = [DahyakGrekh]
