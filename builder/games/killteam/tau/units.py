__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel, KTModelNoSpec
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from builder.core2 import UnitDescription
from . import points


class FireShasla(KTModel):
    desc = points.ShasLa
    datasheet = 'Fire Warrior'
    type_id = 'fire_warrior_v1'
    gears = [points.PhotonGrenade]
    specs = ['Comms', 'Medic', 'Scout', 'Sniper', 'Veteran']

    class Gun(OneOf):
        def __init__(self, parent):
            super(FireShasla.Gun, self).__init__(parent, 'Weapon')
            self.variant(*points.PulseRifle)
            self.variant(*points.PulseCarbine)

    class Pistol(OptionsList):
        def __init__(self, parent):
            super(FireShasla.Pistol, self).__init__(parent, '')
            self.variant(*points.PulsePistol)

    def __init__(self, parent):
        super(FireShasla, self).__init__(parent)
        self.Gun(self)
        self.Pistol(self)


class FireShasui(KTModel):
    desc = points.ShasUi
    datasheet = 'Fire Warrior'
    type_id = 'fire_warrior_leader_v1'
    gears = [points.PhotonGrenade]
    specs = ['Leader', 'Comms', 'Medic', 'Scout', 'Sniper', 'Veteran']
    limit = 1

    class Gun(OneOf):
        def __init__(self, parent):
            super(FireShasui.Gun, self).__init__(parent, 'Weapon')
            self.variant(*points.PulseRifle)
            self.variant(*points.PulseCarbine)
            self.pp = self.variant(*points.PulsePistol)

    class Pistol(OptionsList):
        def __init__(self, parent):
            super(FireShasui.Pistol, self).__init__(parent, '')
            self.pp = self.variant(*points.PulsePistol)
            self.variant(*points.Markerlight)

    def __init__(self, parent):
        super(FireShasui, self).__init__(parent)
        self.gun = self.Gun(self)
        self.pist = self.Pistol(self)

    def check_rules(self):
        super(FireShasui, self).check_rules()
        self.pist.pp.used = self.pist.pp.visible = self.gun.cur != self.gun.pp


class Ds8Turret(KTModelNoSpec):
    desc = points.Ds8TacticalSupportTurret
    type_id = 'ds8_fw_v1'
    datasheet = 'Fire Warrior'
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Ds8Turret.Weapon, self).__init__(parent, 'Heavy Weapon')
            self.variant(*points.MissilePod)
            self.variant(*points.SmartMissileSystem)

    def __init__(self, parent):
        super(Ds8Turret, self).__init__(parent)
        self.Weapon(self)


class Pathfinder(KTModel):
    desc = points.Pathfinder
    type_id = 'pathfinder_v1'
    gears = [points.PulseCarbine, points.Markerlight, points.PhotonGrenade]
    specs = ['Comms', 'Demolitions', 'Medic', 'Scout', 'Veteran']


class PathfinderGunner(KTModel):
    desc = points.PathfinderGunner
    datasheet = 'Pathfinder'
    type_id = 'pathfinder_gunner_v1'
    gears = [points.PhotonGrenade]
    specs = ['Sniper', 'Comms', 'Demolitions', 'Medic', 'Scout', 'Veteran']
    limit = 3

    class Weapon(OneOf):
        def __init__(self, parent):
            super(PathfinderGunner.Weapon, self).__init__(parent, 'Weapons')
            self.variant('Pulse carbine and markerlight',
                         points_price(0, points.PulseCarbine, points.Markerlight),
                         gear=create_gears(points.PulseCarbine, points.Markerlight))
            self.variant(*points.IonRifle)
            self.variant(*points.RailRifle)

    def __init__(self, parent):
        super(PathfinderGunner, self).__init__(parent)
        self.Weapon(self)


class PathfinderShasui(KTModel):
    desc = points.PathfinderShasUi
    datasheet = 'Pathfinder'
    type_id = 'pathfinder_leader_v1'
    gears = [points.PulseCarbine, points.Markerlight, points.PhotonGrenade]
    specs = ['Leader', 'Comms', 'Demolitions', 'Medic', 'Scout', 'Veteran']
    limit = 1

    def __init__(self, parent):
        super(PathfinderShasui, self).__init__(parent)
        FireShasla.Pistol(self)


class BreacherShasla(KTModel):
    desc = points.BreacherShasLa
    datasheet = 'Fire Warrior Breacher'
    type_id = 'breacher_v1'
    gears = [points.PulseBlaster, points.PhotonGrenade]
    specs = ['Comms', 'Demolitions', 'Medic', 'Scout', 'Veteran']

    def __init__(self, parent):
        super(BreacherShasla, self).__init__(parent)
        FireShasla.Pistol(self)


class BreacherShasui(KTModel):
    desc = points.BreacherShasUi
    datasheet = 'Fire Warrior Breacher'
    type_id = 'breacher_leader_v1'
    gears = [points.PhotonGrenade]
    specs = ['Leader', 'Demolitions', 'Medic', 'Scout', 'Veteran']
    limit = 1

    class Gun(OneOf):
        def __init__(self, parent):
            super(BreacherShasui.Gun, self).__init__(parent, 'Weapon')
            self.variant(*points.PulseBlaster)
            self.pp = self.variant(*points.PulsePistol)

    def __init__(self, parent):
        super(BreacherShasui, self).__init__(parent)
        self.gun = self.Gun(self)
        self.pist = FireShasui.Pistol(self)

    def check_rules(self):
        super(BreacherShasui, self).check_rules()
        self.pist.pp.used = self.pist.pp.visible = self.gun.cur != self.gun.pp


class Ds8TurretBreacher(KTModelNoSpec):
    desc = points.Ds8TacticalSupportTurret
    type_id = 'ds8_breacher_v1'
    datasheet = 'Fire Warrior Breacher'
    limit = 1

    def __init__(self, parent):
        super(Ds8TurretBreacher, self).__init__(parent)
        Ds8Turret.Weapon(self)


class StealthShasui(KTModel):
    desc = points.StealthShasUi
    datasheet = 'XV25 Stealth Battlesuit'
    type_id = 'stealth_shaui_v1'
    specs = ['Comms', 'Heavy', 'Scout', 'Veteran']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(StealthShasui.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.BurstCannon)
            self.blaster = self.variant(*points.FusionBlaster)

    def __init__(self, parent):
        super(StealthShasui, self).__init__(parent)
        self.gun = self.Weapon(self)

    def get_unique_gear(self):
        if self.gun.cur == self.gun.blaster:
            return ['Fusion blaster']
        else:
            return []


class StealthShasvre(KTModel):
    desc = points.StealthShasVre
    datasheet = 'XV25 Stealth Battlesuit'
    type_id = 'stealth_leader_v1'
    specs = ['Leader', 'Comms', 'Heavy', 'Scout', 'Veteran']

    class Gears(OptionsList):
        def __init__(self, parent):
            super(StealthShasvre.Gears, self).__init__(parent, 'Options')
            self.variant(*points.Markerlight)
            self.variant(*points.TargetLock)

    def __init__(self, parent):
        super(StealthShasvre, self).__init__(parent)
        self.gun = StealthShasui.Weapon(self)
        self.Gears(self)

    def get_unique_gear(self):
        if self.gun.cur == self.gun.blaster:
            return ['Fusion blaster']
        else:
            return []


class Drone(KTModelNoSpec):
    desc = ('Drone', 0)
    type_id = 'drone_v1'

    class DroneClass(OneOf):
        def make_var(self, data):
            return self.variant(
                data[0][0],
                points_price(data[0][1], *data[1]),
                gear=create_gears(*data[1])
            )

        def __init__(self, parent):
            types = [
                (points.Mv1GunDrone, [points.PulseCarbine] * 2),
                (points.Mv4ShieldDrone, [('Shield generator', 0)]),
                (points.Mv7MarkerDrone, [points.Markerlight]),
                (points.Mv36GuardianDrone, []),
                (points.Mv33GravInhibitorDrone, []),
                (points.Mv33GravInhibitorDrone, []),
                (points.Mb3ReconDrone, [points.BurstCannon])
            ]
            super(Drone.DroneClass, self).__init__(parent, 'Drone type')
            for t in types[0:3]:
                self.make_var(t)
            self.gdrone = self.make_var(types[3])
            self.pdrones = [self.make_var(t) for t in types[4:]]

    def __init__(self, parent):
        super(Drone, self).__init__(parent)
        self.drone_class = self.DroneClass(self)

    def build_description(self):
        return UnitDescription(self.drone_class.cur.title, self.build_points(), options=self.drone_class.cur.gear)

    def check_rules(self):
        super(Drone, self).check_rules()
        fw_flag = any(u.datasheet in ['Fire Warrior', 'Fire Warrior Breacher'] and not u.type_name == points.Ds8TacticalSupportTurret[0]
                      for u in self.roster.fire_teams.units)
        self.drone_class.gdrone.active = fw_flag
        path_flag = any(u.datasheet in ['Pathfinder']
                        for u in self.roster.fire_teams.units)
        for v in self.drone_class.pdrones:
            v.active = path_flag

    def get_unique(self):
        if self.drone_class.cur in [self.drone_class.gdrone] + self.drone_class.pdrones:
            return self.drone_class.cur.title


class Kroot(KTModel):
    desc = points.Kroot
    datasheet = 'Kroot Carnivore'
    type_id = 'kroot_v1'
    gears = [points.KrootRifle]
    specs = ['Leader', 'Combat', 'Scout', 'Veteran', 'Zealot']


class KrootHound(KTModel):
    desc = points.KrootHound
    type_id = 'kroot_hound_v1'
    gears = [points.RippingFangs]
    specs = ['Combat', 'Scout']


class KrootoxRider(KTModel):
    desc = points.KrootoxRider
    type_id = 'krootox_rider_v1'
    gears = [points.KrootGun, points.KrootoxFists]
    specs = ['Combat', 'Heavy']
