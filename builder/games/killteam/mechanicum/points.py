__author__ = 'Ivan Truskov'

# units
SicarianInfiltrator = ('Sicarian Infiltrator', 14)
InfiltratorPrinceps = ('Infiltrator Princeps', 15)
SicarianRuststalker = ('Sicarian Ruststalker', 14)
RuststalkerPrinceps = ('Ruststalker Princeps', 15)
SkitariiRanger = ('Skitarii Ranger', 9)
RangerGunner = ('Ranger Gunner', 10)
RangerAlpha = ('Ranger Alpha', 10)
SkitariiVanguard = ('Skitarii Vanguard', 9)
VanguardGunner = ('Vanguard Gunner', 10)
VanguardAlpha = ('Vanguard Alpha', 10)
UR025 = ('UR-025', 30)

Manipulus = ('Tech-Priest Manipulus', [102, 122, 142, 167])
Enginseer = ('Tech-Priest Enginseer', [28, 33, 48, 68])
Dominus = ('Tech-Priest Dominus', [130, 150, 170, 195])

# Ranged
ArcPistol = ('Arc pistol', 0)
ArcRifle = ('Arc rifle', 0)
FlechetteBlaster = ('Flechette blaster', 0)
GalvanicRifle = ('Galvanic rifle', 0)
PhosphorBlastPistol = ('Phosphor blast pistol', 0)
PlasmaCaliver = ('Plasma caliver', 3)
RadiumCarbine = ('Radium carbine', 0)
RadiumPistol = ('Radium pistol', 0)
Stubcarbine = ('Stubcarbine', 0)
TransuranicArquebus = ('Transuranic arquebus', 5)

MagnarailLance = ('Magnarail lance', 0)
TransonicCannon = ('Transonic cannon', 10)
Laspistol = ('Laspistol', 0)
EradicationRay = ('Eradication ray', 14)
Macrostubber = ('Macrostubber', 0)
PhosphorSerpentra = ('Phosphor serpentra', 4)
VolkiteBlaster = ('Volkite blaster', 0)

# Melee
ArcMaul = ('Arc maul', 0)
Chordclaw = ('Chordclaw', 1)
PowerSword = ('Power sword', 0)
TaserGoad = ('Taser goad', 1)
TransonicBlades = ('Transonic blades', 0)
TransonicRazor = ('Transonic razor', 0)

Mechadendrites = ('Mechadendrites', 0)
OmnissiahStaff = ('Omnissiah staff', 0)
OmnissiahAxe = ('Omnissiah axe', 0)
ServoArm = ('Servo-arm', 0)

# gear
EnhancedDataTether = ('Enhanced data-tether', 5)
Omnispex = ('Omnispex', 1)
