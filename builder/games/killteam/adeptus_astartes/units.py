from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class Scout(KTModel):
    desc = points.Scout
    type_id = 'scout_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Comms', 'Demolitions', 'Scout', 'Sniper']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Scout.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Boltgun)
            self.variant(*points.CombatKnife)
            self.variant(*points.AstartesShotgun)
            self.variant('Sniper rifle and camo cloac', get_cost(points.SniperRifle) + get_cost(points.CamoCloak),
                         gear=create_gears(points.SniperRifle, points.CamoCloak))

    def __init__(self, parent):
        super(Scout, self).__init__(parent)
        self.Weapon(self)


class ScoutSergeant(KTModel):
    desc = points.ScoutSergeant
    datasheet = 'Scout'
    type_id = 'scout_sergeant_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Leader', 'Comms', 'Demolitions', 'Scout', 'Sniper']
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScoutSergeant.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Boltgun)
            self.variant(*points.Chainsword)
            self.variant(*points.AstartesShotgun)
            self.variant('Sniper rifle and camo cloac', get_cost(points.SniperRifle) + get_cost(points.CamoCloak),
                         gear=create_gears(points.SniperRifle, points.CamoCloak))

    def __init__(self, parent):
        super(ScoutSergeant, self).__init__(parent)
        self.Weapon(self)


class ScoutGunner(KTModel):
    desc = points.ScoutGunner
    datasheet = 'Scout'
    type_id = 'scout_gunner_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Heavy', 'Comms', 'Demolitions', 'Scout', 'Sniper']
    limit = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(ScoutGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Boltgun)
            self.variant(*points.HeavyBolter)
            self.missle_launcher = self.variant(*points.MissileLauncher)
            self.variant('Sniper rifle and camo cloac', get_cost(points.SniperRifle) + get_cost(points.CamoCloak),
                         gear=create_gears(points.SniperRifle, points.CamoCloak))

    class Wargear(OptionsList):
        def __init__(self, parent):
            super(ScoutGunner.Wargear, self).__init__(parent, 'Wargear', limit=1)
            self.variant(*points.CamoCloak)

    def __init__(self, parent):
        super(ScoutGunner, self).__init__(parent)
        self.wep = self.Weapon(self)
        self.opt = self.Wargear(self)

    def check_rules(self):
        super(ScoutGunner, self).check_rules()
        self.opt.used = self.opt.visible = self.wep.cur != self.wep.missle_launcher


class TacticalMarine(KTModel):
    desc = points.TacticalMarine
    type_id = 'tactical_marine_v1'
    gears = [points.Boltgun, points.BoltPistol, points.FragGrenade, points.KrakGrenade]
    specs = ['Comms', 'Demolitions', 'Sniper', 'Veteran']

    def __init__(self, parent):
        super(TacticalMarine, self).__init__(parent)


class TacticalMarineSergeant(KTModel):
    desc = points.TacticalSergeant
    datasheet = 'Tactical Marine'
    type_id = 'tactical_marine_sergeant_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Leader', 'Comms', 'Demolitions', 'Sniper', 'Veteran']
    limit = 1

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TacticalMarineSergeant.Weapon, self).__init__(parent, 'Weapon')
            self.bolt = self.variant(*points.Boltgun)
            self.variant(*points.CombiFlamer)
            self.variant(*points.CombiGrav)
            self.variant(*points.CombiMelta)
            self.variant(*points.CombiPlasma)

    class Pistol(OneOf):
        def __init__(self, parent):
            super(TacticalMarineSergeant.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.BoltPistol)
            self.variant(*points.PlasmaPistol)
            self.variant(*points.GravPistol)

    class Other(OptionsList):
        def __init__(self, parent):
            super(TacticalMarineSergeant.Other, self).__init__(parent, '', limit=1)
            self.variant(*points.Chainsword)
            self.variant(*points.PowerFist)
            self.variant(*points.PowerSword)
            self.variant(*points.Auspex)

    def __init__(self, parent):
        super(TacticalMarineSergeant, self).__init__(parent)
        self.gun = self.Weapon(self)
        self.pist = self.Pistol(self)
        self.sword = self.Other(self)

    def check_rules(self):
        super(TacticalMarineSergeant, self).check_rules()
        self.pist.used = self.pist.visible = self.gun.cur == self.gun.bolt
        self.sword.used = self.sword.visible = self.gun.cur == self.gun.bolt


class TacticalMarineGunner(KTModel):
    desc = points.TacticalMarineGunner
    datasheet = 'Tactical Marine'
    type_id = 'tactical_marine_gunner_v1'
    gears = [points.FragGrenade, points.KrakGrenade]
    specs = ['Heavy', 'Comms', 'Demolitions', 'Sniper', 'Veteran']
    limit = 2

    class Weapon(OneOf):
        def __init__(self, parent):
            super(TacticalMarineGunner.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.Boltgun)
            self.group1 = [
                self.variant(*points.Flamer),
                self.variant(*points.Meltagun),
                self.variant(*points.PlasmaGun),
                self.variant(*points.GravGun),
            ]
            self.group2 = [
                self.variant(*points.MissileLauncher),
                self.variant(*points.HeavyBolter)
            ]

    def __init__(self, parent):
        super(TacticalMarineGunner, self).__init__(parent)
        self.gun = self.Weapon(self)

    def get_unique_gear(self):
        if self.gun.cur in self.gun.group1:
            return ['Flamer, meltagun, plasma gun or grav-gun']
        elif self.gun.cur in self.gun.group2:
            return ['Missile launcher or heavy bolter']
        pass


class Reiver(KTModel):
    desc = points.Reiver
    type_id = 'reiver_v1'
    gears = [points.HeavyBoltPistol, points.FragGrenade, points.KrakGrenade, points.ShockGrenade]
    specs = ['Combat', 'Comms', 'Demolitions', 'Scout', 'Veteran']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Reiver.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.BoltCarbine)
            self.variant(*points.CombatKnife)

    class Optionnal(OptionsList):
        def __init__(self, parent):
            super(Reiver.Optionnal, self).__init__(parent, 'Optionnal')
            self.variant(*points.GravChute)
            self.variant(*points.GrapnelLauncher)

    def __init__(self, parent):
        super(Reiver, self).__init__(parent)
        self.Weapon(self)
        self.Optionnal(self)


class ReiverSergeant(KTModel):
    desc = points.ReiverSergeant
    datasheet = 'Reiver'
    type_id = 'reiver_sergeant_v1'
    gears = [points.FragGrenade, points.KrakGrenade, points.ShockGrenade]
    specs = ['Leader', 'Combat', 'Comms', 'Demolitions', 'Scout', 'Veteran']
    limit = 1

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(ReiverSergeant.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*points.BoltCarbine)
            self.variant(*points.CombatKnife)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(ReiverSergeant.Weapon2, self).__init__(parent, 'Weapon')
            self.variant(*points.HeavyBoltPistol)
            self.variant(*points.CombatKnife)

    def __init__(self, parent):
        super(ReiverSergeant, self).__init__(parent)
        self.Weapon1(self)
        self.Weapon2(self)
        Reiver.Optionnal(self)


class Intercessor(KTModel):
    desc = points.Intercessor
    type_id = 'intercessor_v1'
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]
    specs = ['Combat', 'Comms', 'Sniper', 'Veteran']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(Intercessor.Weapon, self).__init__(parent, 'Weapon')
            self.variant(*points.BoltRifle)
            self.variant(*points.AutoBoltRifle)
            self.variant(*points.StalkerBoltRifle)

    def __init__(self, parent):
        super(Intercessor, self).__init__(parent)
        self.Weapon(self)


class IntercessorSergeant(KTModel):
    desc = points.IntercessorSergeant
    datasheet = 'Intercessor'
    type_id = 'intercessor_sergeant_v1'
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]
    specs = ['Leader', 'Combat', 'Comms', 'Sniper', 'Veteran']
    limit = 1

    class Weapon1(OptionsList):
        def __init__(self, parent):
            super(IntercessorSergeant.Weapon1, self).__init__(parent, 'Weapon')
            self.variant(*points.PowerSword)
            self.variant(*points.Chainsword)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(IntercessorSergeant.Weapon2, self).__init__(parent, 'Weapon')
            self.rifle = self.variant(*points.BoltRifle)
            self.variant(*points.AutoBoltRifle)
            self.variant(*points.StalkerBoltRifle)
            self.melee = [
                self.variant(*points.PowerSword),
                self.variant(*points.Chainsword)
            ]

    def __init__(self, parent):
        super(IntercessorSergeant, self).__init__(parent)
        self.gun = self.Weapon2(self)
        self.sword = self.Weapon1(self)

    def check_rules(self):
        super(IntercessorSergeant, self).check_rules()
        self.sword.used = self.sword.visible = self.gun.cur not in self.gun.melee


class IntercessorGunner(KTModel):
    desc = points.IntercessorGunner
    datasheet = 'Intercessor'

    type_id = 'intercessor_gunner_v1'
    gears = [points.BoltPistol, points.FragGrenade, points.KrakGrenade]
    specs = ['Demolitions', 'Combat', 'Comms', 'Sniper', 'Veteran']

    class Optional(OptionsList):
        def __init__(self, parent):
            super(IntercessorGunner.Optional, self).__init__(parent, 'Weapon')
            self.variant(*points.AuxiliaryGrenadeLauncher)

    def __init__(self, parent):
        super(IntercessorGunner, self).__init__(parent)
        self.Optional(self)
