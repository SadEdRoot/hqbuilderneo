__author__ = 'Ivan Truskov'

# units
InfantrySquadGuardsman = ('Guardsman', 5)
GuardsmanGunner = ('Guardsman Gunner', 5)
Sergeant = ('Sergeant', 5)
MilitarumTempestusScion = ('Scion', 9)
ScionGunner = ('Scion Gunner', 10)
Tempestor = ('Tempestor', 10)
SpecialWeaponsSquadGuardsman = ('Special Weapons Guardsman', 5)
SpecialWeaponsGunner = ('Special Weapons Gunner', 5)

Commissar = ('Commissar', [10, 15, 30, 50])
LordCommissar = ('Lord Commissar', [25, 30, 45, 65])
PlatoonCommander = ('Platoon Commander', [10, 15, 30, 50])
CompanyCommander = ('Company Commander', [15, 20, 35, 55])
TempestorPrime = ('Tempestor Prime', [25, 30, 45, 65])

EspernLocarno = ('Espern Locarno', [30, 40, 50, 60])
JanusDraik = ('Janus Draik', [50, 65, 80, 105])
PiousVorne = ('Pious Vorne', 15)
Raus = ('Raus', 10)
Rein = ('Rein', 8)
Taddeus = ('Taddeus the Purifier', [50, 55, 70, 85])
Severina = ('Severina Raine', [20])
Eisenhorn = ('Inquisitor Eqisenhorn', [70, 80, 90, 105])

# Ranged
BoltPistol = ('Bolt pistol', 0)
Flamer = ('Flamer', 3)
FragGrenade = ('Frag grenade', 0)
GrenadeLauncher = ('Grenade launcher', 2)
HotShotLasgun = ('Hot-shot lasgun', 0)
HotShotLaspistol = ('Hot-shot laspistol', 0)
HotShotVolleyGun = ('Hot-shot volley gun', 3)
KrakGrenade = ('Krak grenade', 0)
Lasgun = ('Lasgun', 0)
Laspistol = ('Laspistol', 0)
Meltagun = ('Meltagun', 3)
PlasmaGun = ('Plasma gun', 3)
PlasmaPistol = ('Plasma pistol', 1)
SniperRifle = ('Sniper rifle', 1)

ComPlasmaPistol = ('Plasma pistol', 3)
Boltgun = ('Boltgun', 2)

HeirloomPistol = ('Heirloom pistol', 0)
ArcheotechGrenade = ('Archeotech grenade', 0)
ServoStubber = ('Servo-stubber', 0)
StubPistol = ('Stub pistol', 0)
DemolitionCharge = ('Demolition charge', 0)
ArtificerBoltPistol = ('Artificer Bolt Pistol', 0)

# Melee
Chainsword = ('Chainsword', 0)
PowerFist = ('Power fist', 2)
PowerSword = ('Power sword', 1)

ComPowerFist = ('Power fist', 6)
PrimePowerFist = ('Power fist', 8)
ComPowerSword = ('Power sword', 2)
ComChainsword = ('Chainsword', 1)

MonomolecularRapier = ('Monomolecular rapier', 0)
PowerMaul = ('Power Maul', 0)
ForceOrbCane = ('Force-orb cane', 0)

# gear
VoxCaster = ('Vox-caster', 5)

CommandRod = ('Tempestus Command Rod', 2)
