__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class Archon(KTCommander):
    desc = points.Archon
    type_id = 'archon_v1'
    specs = ['Ferocity']

    class Weapon1(OneOf):
        def __init__(self, parent):
            super(Archon.Weapon1, self).__init__(parent, 'Blade')
            self.variant(*points.CHuskblade)
            self.variant(*points.CAgoniser)
            self.variant(*points.CPowerSword)
            self.variant(*points.CVenomBlade)

    class Weapon2(OneOf):
        def __init__(self, parent):
            super(Archon.Weapon2, self).__init__(parent, 'Gun')
            self.variant(*points.CSplinterPistol)
            self.variant(*points.CBlastPistol)

    def __init__(self, parent):
        super(Archon, self).__init__(parent)
        self.Weapon1(self)
        self.Weapon2(self)


class Succubus(KTCommander):
    desc = points.Succubus
    type_id = 'succubus_v1'
    gears = [points.CAgoniser, points.ArchiteGlaive]
    specs = ['Ferocity', 'Melee', 'Stealth']


class Haemonculus(KTCommander):
    desc = points.Haemonculus
    type_id = 'haemonculus_v1'
    specs = ['Ferocity', 'Fortitude', 'Logistics', 'Melee', 'Strength']
    gears = [points.StingerPistol, points.HaemonculusTools]

    class Option(OneOf):
        def __init__(self, parent):
            super(Haemonculus.Option, self).__init__(parent, 'Weapon')
            self.variant(*points.IchorInjector)

    def __init__(self, parent):
        super(Haemonculus, self).__init__(parent)
        self.Option(self)
