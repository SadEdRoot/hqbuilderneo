__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Kabalite, KabGunner, Sybarite,\
    Wych, WychFighter, Hekatrix
from .commanders import Archon, Succubus, Haemonculus


class DrukhariKT(KTRoster):
    army_name = 'Drukhari Kill Team'
    army_id = 'drukhari_kt_v1'
    unit_types = [Kabalite, KabGunner, Sybarite, Wych, WychFighter,
                  Hekatrix]


class ComDrukhariKT(KTCommanderRoster, DrukhariKT):
    army_name = 'Drukhari Kill Team'
    army_id = 'drukhari_kt_v2_com'
    commander_types = [Archon, Succubus, Haemonculus]
