__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import RubricMarine, RubricMarineGunner,\
    AspiringSorcerer, Tzaangor, Twistbray
from .commanders import ExaltedSorcerer, TzaangorShaman


class ThousandSonsKT(KTRoster):
    army_name = 'Thousand Sons Kill Team'
    army_id = 'thousand_sons_kt_v1'
    unit_types = [RubricMarine, RubricMarineGunner, AspiringSorcerer,
                  Tzaangor, Twistbray]


class ComThousandSonsKT(KTCommanderRoster, ThousandSonsKT):
    army_name = 'Thousand Sons Kill Team'
    army_id = 'thousand_sons_kt_v2_com'
    commander_types = [ExaltedSorcerer, TzaangorShaman]
