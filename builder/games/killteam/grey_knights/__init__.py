__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Justicar, GreyKnight, GKGunner
from .commanders import BrotherhoodChampion


class GreyKnightKT(KTRoster):
    army_name = 'Grey Knight Team'
    army_id = 'grey_knight_kt_v1'
    unit_types = [Justicar, GreyKnight, GKGunner]


class ComGreyKnightKT(KTCommanderRoster, GreyKnightKT):
    army_name = 'Grey Knight Team'
    army_id = 'grey_knight_kt_v2_com'
    commander_types = [BrotherhoodChampion]
