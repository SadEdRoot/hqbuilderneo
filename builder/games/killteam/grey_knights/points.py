__author__ = 'Ivan Truskov'

# units
GreyKnight = ('Grey Knight', 18)
GreyKnightGunner = ('Grey Knight Gunner', 19)
Justicar = ('Justicar', 19)

BrotherhoodChampion = ('Brotherhood Champion', [108, 128, 148, 173])

# ranged weapons
FragGrenade = ('Frag grenade', 0)
Incinerator = ('Incinerator', 3)
KrakGrenade = ('Krak grenade', 0)
Psilencer = ('Psilencer', 3)
Psycannon = ('Psycannon', 2)
PsykOutGrenade = ('Psyk-out grenade', 0)
StormBolter = ('Storm bolter', 0)

# melee weapons
NemesisDaemonHammer = ('Nemesis Daemon hammer', 2)
NemesisFalchion = ('Nemesis falchion', 1)
NemesisForceHalberd = ('Nemesis force halberd', 0)
NemesisForceSword = ('Nemesis force sword', 0)
NemesisWardingStave = ('Nemesis warding stave', 0)
