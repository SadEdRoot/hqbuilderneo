__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTModel
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class GreyKnight(KTModel):
    desc = points.GreyKnight
    type_id = 'grey_knight_v1'
    gears = [points.FragGrenade, points.KrakGrenade, points.PsykOutGrenade]
    specs = ['Combat', 'Comms', 'Demolitions', 'Veteran', 'Zealot']

    class Weapon(OneOf):
        def __init__(self, parent):
            super(GreyKnight.Weapon, self).__init__(parent, 'Melee weapon')
            self.variant(*points.NemesisForceSword)
            self.variant(*points.NemesisForceHalberd)
            self.variant(*points.NemesisDaemonHammer)
            self.variant(*points.NemesisWardingStave)
            self.variant('Two Nemesis falchions', 2 * get_cost(points.NemesisFalchion),
                         gear=2 * create_gears(points.NemesisFalchion))

        @property
        def description(self):
            if self.used:
                return super(GreyKnight.Weapon, self).description + create_gears(points.StormBolter)
            else:
                return []


    def __init__(self, parent):
        super(GreyKnight, self).__init__(parent)
        self.Weapon(self)


class Justicar(KTModel):
    desc = points.Justicar
    datasheet = 'Grey Knight'
    type_id = 'justicar_v1'
    gears = [points.FragGrenade, points.KrakGrenade, points.PsykOutGrenade]
    specs = ['Leader', 'Combat', 'Comms', 'Demolitions', 'Veteran', 'Zealot']
    limit = 1

    def __init__(self, parent):
        super(Justicar, self).__init__(parent)
        GreyKnight.Weapon(self)


class GKGunner(KTModel):
    desc = points.GreyKnightGunner
    datasheet = 'Grey Knight'
    type_id = 'gk_gunner_v1'
    gears = [points.FragGrenade, points.KrakGrenade, points.PsykOutGrenade]
    specs = ['Heavy', 'Combat', 'Comms', 'Demolitions', 'Veteran', 'Zealot']
    limit = 2

    class Heavy(OptionsList):
        def __init__(self, parent):
            super(GKGunner.Heavy, self).__init__(parent, 'Heavy weapon', limit=1)
            self.variant(*points.Incinerator)
            self.variant(*points.Psilencer)
            self.variant(*points.Psycannon)

    def __init__(self, parent):
        super(GKGunner, self).__init__(parent)
        self.mle = GreyKnight.Weapon(self)
        self.rng = self.Heavy(self)

    def check_rules(self):
        super(GKGunner, self).check_rules()
        self.mle.used = self.mle.visible = not self.rng.any
