__author__ = 'Ivan Truskov'

# units
Bloodletter = ('Bloodletter', 7)
BloodletterHornblower = ('Bloodletter Hornblower', 8)
BloodletterIconBearer = ('Bloodletter Icon Bearer', 8)
Bloodreaper = ('Bloodreaper', 8)
Daemonette = ('Daemonette', 7)
DaemonetteHornblower = ('Daemonette Hornblower', 8)
DaemonetteIconBearer = ('Daemonette Icon Bearer', 8)
Alluress = ('Alluress', 8)
Horror = ('Pink Horror', 12)
HorrorHornblower = ('Pink Horror Hornblower', 13)
HorrorIconBearer = ('Pink Horror Icon Bearer', 13)
IridescentHorror = ('Iridescent Horror', 13)
Plaguebearer = ('Plaguebearer', 7)
PlaguebearerHornblower = ('Plaguebearer Hornblower', 8)
PlaguebearerIconBearer = ('Plaguebearer Icon Bearer', 8)
Plagueridden = ('Plagueridden', 8)

# weapon
Hellblade = ('Hellblade', 0)
PiercingClaws = ('Piercing Claws', 8)
Plaguesword = ('Plaguesword', 0)

CoruscatingFlames = ('Coruscating Flames', 0)

# wargear

IconOfKhorne = ('Icon of Khorne', 3)
IconOfNurgle = ('Icon of Nurgle', 1)
IconOfSlaanesh = ('Icon of Slaanesh', 5)
IconOfTzeentch = ('Icon of Tzeentch', 3)
InstrumentOfKhorne = ('Instrument of Khorne', 2)
InstrumentOfNurgle = ('Instrument of Nurgle', 2)
InstrumentOfSlaanesh = ('Instrument of Slaanesh', 2)
InstrumentOfTzeentch = ('Instrument of Tzeentch', 2)
