__author__ = 'Truskov Ivan'

from builder.core2 import Unit
from .specialities import Speciality, EmptySpecialty
from builder.core2.options import OptionsList
from .utils import *


class MetaUnit(type):
    def __init__(cls, name, bases, cldict):
        # keywords are uppercase
        if 'desc' in cldict:
            cls.type_name = get_name(cls.desc)
            cls.model_cost = get_cost(cls.desc)
        if not 'datasheet' in cldict:
            cls.datasheet = cls.type_name
        super(MetaUnit, cls).__init__(name, bases, cldict)


class KTModel(Unit, metaclass=MetaUnit):
    specs = []
    model_cost = 0
    force_spec = False
    gears = []

    def __init__(self, parent, **kwargs):
        super(KTModel, self).__init__(parent, points=points_price(self.model_cost, *self.gears), gear=create_gears(*self.gears), **kwargs)
        self.spec = Speciality(self, self.specs, self.force_spec)

    def check_rules_chain(self):
        self.spec.check_rules()
        super(KTModel, self).check_rules_chain()

    def build_statistics(self):
        return {'Models': 1}


class KTModelNoSpec(Unit, metaclass=MetaUnit):
    model_cost = 0
    gears = []

    def __init__(self, parent, **kwargs):
        super(KTModelNoSpec, self).__init__(parent, points=points_price(self.model_cost, *self.gears), gear=create_gears(*self.gears), **kwargs)
        self.spec = EmptySpecialty(self)

    def build_statistics(self):
        return {'Models': 1}


class KTCommander(Unit, metaclass=MetaUnit):
    specs = []
    model_cost = [0, 0, 0, 0]
    gears = []

    class Traits(OptionsList):
        def __init__(self, parent):
            super(KTCommander.Traits, self).__init__(parent, 'Commander traits', order=17)
            self.variant('Iron will', 15)
            self.variant('Stoic hero', 10)
            self.variant('Destined fate', 10)
            self.variant('Tactical planner', 15)
            self.level4 = [
                self.variant('Generalist', 15),
                self.variant('Master specialist', 30)
            ]

        def check_rules(self):
            super(KTCommander.Traits, self).check_rules()
            for var in self.level4:
                if self.parent.spec.level == 4:
                    var.active = True
                else:
                    var.active = var.value = False

    def __init__(self, parent, **kwargs):
        super(KTCommander, self).__init__(parent, points=points_price(0, *self.gears), gear=create_gears(*self.gears), **kwargs)
        self.traits = self.Traits(self)
        self.spec = Speciality(self, self.specs, True, self.model_cost)

    def check_rules_chain(self):
        self.spec.check_rules()
        super(KTCommander, self).check_rules_chain()

    def check_rules(self):
        super(KTCommander, self).check_rules()
        if not self.spec.is_spec():
            self.error('Commasnder must have a specialism')

    def build_statistics(self):
        return {'Models': 1}
