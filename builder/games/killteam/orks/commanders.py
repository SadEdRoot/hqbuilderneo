__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.core2.options import OneOf, OptionsList
from . import points


class Painboy(KTCommander):
    desc = points.Painboy
    type_id = 'painboy_v1'
    specs = ['Ferocity', 'Fortitude', 'Logistics', 'Melee', 'Strength']
    gears = [points.PPowerKlaw, points.Syringe]


class Warboss(KTCommander):
    desc = points.Warboss
    type_id = 'warboss_v1'
    specs = ['Ferocity', 'Fortitude', 'Logistics', 'Melee', 'Strategist', 'Strength']
    gears = [points.Stikkbomb]

    class Gun(OneOf):
        def __init__(self, parent):
            super(Warboss.Gun, self).__init__(parent, 'Dakka')
            self.variant(*points.KustomShoota)
            self.variant(*points.WKombiRokkit)
            self.variant(*points.WKombiSkorcha)

    class Choppa(OneOf):
        def __init__(self, parent):
            super(Warboss.Choppa, self).__init__(parent, 'Choppa')
            self.variant(*points.WBigChoppa)
            self.variant(*points.WPowerKlaw)

    class Pet(OptionsList):
        def __init__(self, parent):
            super(Warboss.Pet, self).__init__(parent, 'Pet')
            self.variant(*points.Squig)

    def __init__(self, parent):
        super(Warboss, self).__init__(parent)
        self.Gun(self)
        self.Choppa(self)
        self.Pet(self)


class BigMek(KTCommander):
    desc = points.BigMek
    type_id = 'big_mek_v1'
    specs = ['Ferocity', 'Fortitude', 'Logistics', 'Melee', 'Strength']
    gears = [points.Choppa, points.Stikkbomb]

    class Gun(OneOf):
        def __init__(self, parent):
            super(BigMek.Gun, self).__init__(parent, 'Dakka')
            self.slugga = self.variant(*points.Slugga)
            self.variant(*points.KustomMegaSlugga)
            self.variant(*points.SAG)

    class Options(OptionsList):
        def __init__(self, parent):
            super(BigMek.Options, self).__init__(parent, 'Options')
            self.variant(*points.KFF)

    def __init__(self, parent):
        super(BigMek, self).__init__(parent)
        self.gun = self.Gun(self)
        self.opt = self.Options(self)

    def check_rules(self):
        super(BigMek, self).check_rules()
        self.opt.used = self.opt.visible = self.gun.cur == self.gun.slugga
