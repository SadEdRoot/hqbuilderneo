__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import OrkBoy, OrkBoyGunner, BossNob, Gretchin,\
    Kommando, KommandoNob, BurnaBoy, BurnaSpanner, Loota, LootaSpanner
from .commanders import Warboss, BigMek, Painboy

class OrksKT(KTRoster):
    army_name = 'Orks Kill Team'
    army_id = 'orks_kt_v1'
    unit_types = [OrkBoy, OrkBoyGunner, BossNob, Gretchin, Kommando,
                  KommandoNob, BurnaBoy, BurnaSpanner, Loota, LootaSpanner]


class ComOrksKT(KTCommanderRoster, OrksKT):
    army_name = 'Orks Kill Team'
    army_id = 'ork_kt_v2_com'
    commander_types = [Warboss, BigMek, Painboy]
