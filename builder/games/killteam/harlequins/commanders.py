__author__ = 'Ivan Truskov'


from builder.games.killteam.unit import KTCommander
from builder.games.killteam.utils import *
from builder.core2.options import OneOf, OptionsList
from . import points


class TroupeMaster(KTCommander):
    desc = points.TroupeMaster
    type_id = 'troupe_master_v1'
    gears = [points.PlasmaGrenade]
    specs = ['Ferocity', 'Leadership', 'Melee', 'Shooting', 'Stealth', 'Strategist']

    class Pistol(OneOf):
        def __init__(self, parent):
            super(TroupeMaster.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.ShurikenPistol)
            self.variant(*points.CNeuroDisruptor)
            self.variant(*points.CFusionPistol)

    class Sword(OneOf):
        def __init__(self, parent):
            super(TroupeMaster.Sword, self).__init__(parent, 'Close combat weapon')
            self.variant(*points.HarlequinBlade)
            self.variant(*points.PowerSword)
            self.variant(*points.CHarlequinEmbrace)
            self.variant(*points.CHarlequinCaress)
            self.variant(*points.CHarlequinKiss)

    def __init__(self, parent):
        super(TroupeMaster, self).__init__(parent)
        self.Pistol(self)
        self.Sword(self)


class Shadowseer(KTCommander):
    desc = points.Shadowseer
    type_id = 'shadowseer_v1'
    gears = [points.Hallicunogen, points.Miststave]
    specs = ['Ferocity', 'Melee', 'Psyker', 'Shooting', 'Stealth']

    class Pistol(OneOf):
        def __init__(self, parent):
            super(Shadowseer.Pistol, self).__init__(parent, 'Pistol')
            self.variant(*points.ShurikenPistol)
            self.variant(*points.CNeuroDisruptor)

    def __init__(self, parent):
        super(Shadowseer, self).__init__(parent)
        self.Pistol(self)


class DeathJester(KTCommander):
    desc = points.DeathJester
    type_id = 'djester_v1'
    gears = [points.ShriekerCannon]
    specs = ['Ferocity', 'Melee', 'Shooting', 'Stealth']
