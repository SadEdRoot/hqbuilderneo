__author__ = 'Ivan Truskov'


from builder.games.killteam.roster import KTRoster, KTCommanderRoster
from .units import Cultist, CultistGunner, CultistChampion,\
    ChaosMarine, ChaosMarineGunner, AspiringChampion
from .commanders import ExaltedChampion, Sorcerer

class HereticAstartesKT(KTRoster):
    army_name = 'Heretic Astartes Kill Team'
    army_id = 'heretic_astartes_kt_v1'
    unit_types = [Cultist, CultistGunner, CultistChampion,
                  ChaosMarine, ChaosMarineGunner, AspiringChampion]


class ComHereticAstartesKT(KTCommanderRoster, HereticAstartesKT):
    army_name = 'Heretic Astartes Kill Team'
    army_id = 'heretic_astartes_kt_v2_com'
    commander_types = [ExaltedChampion, Sorcerer]
