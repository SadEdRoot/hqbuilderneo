__author__ = 'Ivan Truskov'

# units
Genestealer = ('Genestealer', 11)
Hormagaunt = ('Hormagaunt', 4)
Lictor = ('Lictor', 25)
Termagant = ('Termagant', 4)
TyranidWarrior = ('Tyranid Warrior', 20)
TyranidWarriorGunner = ('Tyranid Warrior Gunner', 20)

TyranidPrime = ('Tyranid Prime', [50, 65, 80, 105])
Broodlord = ('Broodlord', [131, 151, 171, 196])

# Ranged
BarbedStrangler = ('Barbed strangler', 3)
Deathspitter = ('Deathspitter', 2)
DevourerTermagant = ('Devourer', 3)
DevourerWarrior = ('Devourer', 0)
FleshHooks = ('Flesh hooks', 0)
Fleshborer = ('Fleshborer', 0)
Spinefists = ('Spinefists', 0)
VenomCannon = ('Venom cannon', 4)

PDeathspitter = ('Deathspitter', 5)
PFleshHooks = ('Flesh hooks', 2)
# Melee
AcidMaw = ('Acid maw', 0)
Boneswords = ('Boneswords', 0)
GraspingTalons = ('Grasping talons', 0)
LashWhipAndBonesword = ('Lash whip and bonesword', 1)
RendingClaws = ('Rending claws', 0)
ScythingTalons = ('Scything talons', 0)

PBoneswords = ('Boneswords', 5)
PLashWhipAndBonesword = ('Lash whip and bonesword', 5)
MonstrousRendingClaws = ('Monstrous rending claws', 0)
# Wargear
AdrenalGlands = ('Adrenal glands', 1)
ExtendedCarapace = ('Extended carapace', 0)
ToxinSacs = ('Toxin sacs', 1)

PToxinSacs = ('Toxin sacs', 8)
