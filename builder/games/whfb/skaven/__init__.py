__author__ = 'Denis Romanov'

import math
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.skaven.heroes import *
from builder.games.whfb.skaven.lords import *
from builder.games.whfb.skaven.core import *
from builder.games.whfb.skaven.special import *
from builder.games.whfb.skaven.rare import *


class Skaven(LegacyFantasy):
    army_name = 'Skaven'
    army_id = '7815f0d1bb0844f094041740864be73e'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Skrolk, Thanquol, Ikik, Throt, Queek, VerminLord, Warlord, GreySeer],
            heroes=[Snikch, Tretch, Assassin, Warlock, Chieftain, Priest],
            core=[Clanrats, Stormvermin, Slaves, NightRunners, GiantRats, Swarm,
                  dict(unit=PlagueMonks, active=False), dict(unit=RatOgres, active=False)],
            special=[GutterRunners, RatOgres, PlagueMonks, PlagueCenser, Jezzails, Globadiers],
            rare=[Abomination, Doomwheel, WarpCannon, Catapult]
        )

    def has_queek(self):
        return self.lords.count_unit(Queek) > 0

    def check_rules(self):
        LegacyFantasy.check_rules(self)
        bodyguards = sum((1 for squad in self.core.get_units([Stormvermin]) if squad.is_bodyguards()))
        if bodyguards > 1:
            self.error("You may upgrade only one unit Stormvermin to Queek\'s Bodyguards "
                       "(upgraded: {})".format(bodyguards))

        skrolk = self.lords.count_units([Skrolk])
        self.core.set_active_types([PlagueMonks], skrolk > 0)
        self.special.set_active_types([PlagueMonks], skrolk == 0)
        if not skrolk and self.core.count_units([PlagueMonks]) > 0:
            self.error("You can't have Plague Monks as core choices without Lord Skrolk")

        throt = self.lords.count_units([Throt])
        self.core.set_active_types([RatOgres], throt > 0)
        if (not throt and self.core.count_units([RatOgres]) > 0) or self.core.count_units([RatOgres]) > 2:
            self.error("You can take up to two units Rat Ogres as core choice if Throt the Unclean in you army")

        if sum((1 for squad in self.core.get_units([NightRunners]) if squad.has_weapon_team())) > 1:
            self.error("Only one unit in Night Runners in army can take weapon team")

        if sum((1 for squad in self.special.get_units([GutterRunners]) if squad.has_weapon_team())) \
                > math.ceil(self.special.count_unit(GutterRunners)/2.):
            self.error("Only half Gutter Runners units can take weapon team")

        if sum((1 for squad in self.special.get_units([Globadiers]) if squad.has_weapon_team())) \
                > math.ceil(self.special.count_unit(Globadiers)/2.):
            self.error("Only half Poisoned Wind Globadiers units can take weapon team")
