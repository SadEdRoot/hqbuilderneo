__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OptionsList, OptionalSubUnit, OneOf
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.skaven.armory import *
from builder.games.whfb.skaven.core import GiantRats

unit_standards = filter_items(skaven_standard, 50)


class GutterRunners(FCGUnit):
    name = 'Gutter Runners'

    class WeaponTeam(Unit):
        name = 'Weapon Team'
        gear = ['Hand weapon', 'Heavy armour', 'Warp-grinder']
        base_points = 60
        static = True

    def __init__(self):
        self.team = OptionalSubUnit('team', '', [self.WeaponTeam()])
        FCGUnit.__init__(
            self, model_name='Gutter Runner', model_price=12, min_models=5, max_model=15,
            champion_name='Deathrunner', champion_price=12,
            base_gear=['Hand weapon', 'Throwing stars'], unit_options=[self.team],
            options=[
                OneOf('wep', 'Weapon', [
                    ['Hand weapon', 0],
                    ['Snare-net', 0],
                ]),
                OptionsList('arm', '', [
                    ['Slings', 1, 'sl'],
                    ['Poisoned Attacks', 5, 'pa'],
                ])
            ],
            champion_options=[OptionsList('fo', 'Deathrunner\'s options', [
                ['Smoke Bombs', 10, 'sb'],
                ['Weeping Blade', 30, 'wb'],
            ])]
        )

    def get_count(self):
        return FCGUnit.get_count(self) + self.team.get_count()

    def has_weapon_team(self):
        return self.team.get_count() > 0


class Globadiers(FCGUnit):
    name = 'Poisoned Wind Globadiers'

    class WeaponTeam(Unit):
        name = 'Weapon Team'
        gear = ['Hand weapon', 'Heavy armour', 'Poisoned Wind Mortar']
        base_points = 65
        static = True

    def __init__(self):
        self.team = OptionalSubUnit('team', '', [self.WeaponTeam()])
        FCGUnit.__init__(
            self, model_name='Globadier', model_price=10, min_models=5, max_model=15,
            champion_name='Bombardier', champion_price=5,
            base_gear=['Hand weapon', 'Heavy armour', 'Poisoned wind globes'],
            champion_options=[OptionsList('fo', 'Bombardier\'s options', [
                ['Death Globe', 25, 'dg'],
            ])], unit_options=[self.team]
        )

    def get_count(self):
        return FCGUnit.get_count(self) + self.team.get_count()

    def has_weapon_team(self):
        return self.team.get_count() > 0


class RatOgres(GiantRats):
    name = 'Rat Ogres'

    def __init__(self):
        GiantRats.__init__(self, beast_name='Rat Ogre', beast_points=40, beast_gear='Claws and savagery',
                           beast_limit=2)
        self.master_bred = self.opt_options_list('', [
            ['Master-bred Rat Ogre', 15, 'mb']
        ])

    def check_rules(self, opt=None):
        GiantRats.check_rules(self, self.master_bred)


class PlagueMonks(FCGUnit):
    name = 'Plague Monks'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Plague Monk', model_price=7, min_models=10,
            musician_price=5, standard_price=10, champion_name='Bringer-of-the-Word', champion_price=10,
            magic_banners=pest_standard + unit_standards,
            base_gear=['Hand weapon', 'Hand weapon'],
        )


class PlagueCenser(FCGUnit):
    name = 'Plague Censer Bearers'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Censer Bearer', model_price=16, min_models=5,
            champion_name='Plague Chanter', champion_price=13,
            base_gear=['Plague censer'],
        )


class Jezzails(FCGUnit):
    name = 'Warplock Jezzails'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Jezzail Team', model_price=20, min_models=3,
            champion_name='Sharpshooter Team', champion_price=10,
            base_gear=['Hand weapon', 'Jezzail', 'Pavise'],
        )
