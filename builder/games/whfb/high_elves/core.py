__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit, Unit
from builder.core.unit import OptionsList
from builder.core.model_descriptor import ModelDescriptor
from .armory import *

unit_standards = filter_items(he_standard, 25)


class Spearmen(FCGUnit):
    name = 'Spearmen'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Spearman', model_price=9, min_models=10,
            musician_price=10, standard_price=10, champion_name='Sentinel', champion_price=10,
            base_gear=['Shield', 'Spear', 'Light armour'],
            magic_banners=unit_standards
        )


class Archers(FCGUnit):
    name = 'Archers'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Archer', model_price=10, min_models=10,
            musician_price=10, standard_price=10, champion_name='Hawkeye', champion_price=10,
            base_gear=['Hand weapon', 'Long bow'],
            options=[
                OptionsList('arm', '', [['Light armour', 1, 'la']])
            ]
        )


class SeaGuard(FCGUnit):
    name = 'Lothern Sea Guard'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Sea Guard', model_price=11, min_models=10,
            musician_price=10, standard_price=10, champion_name='Sea Master', champion_price=10,
            base_gear=['Spear', 'Bow', 'Light armour'],
            options=[
                OptionsList('arm', '', [['Shield', 1, 'sh']])
            ]
        )


class SilverHelm(FCGUnit):
    name = 'Silver Helms'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Silver Helm', model_price=21, min_models=5,
            musician_price=10, standard_price=10, champion_name='High Helm', champion_price=10,
            base_gear=['Hand weapon', 'Lance', 'Heavy armour', 'Ithilmar barding'],
            options=[
                OptionsList('arm', '', [['Shield', 2, 'sh']])
            ]
        )


class Reavers(FCGUnit):
    name = 'Ellyrian Reavers'

    def __init__(self):
        self.wep = OptionsList('wep', 'Weapon', [
            ['Spear', 0, 'sp'],
            ['Bow', 1, 'bow1'],
            ['Bow', 3, 'bow3'],
        ])
        FCGUnit.__init__(
            self, model_name='Ellyrian Reaver', model_price=16, min_models=5,
            musician_price=10, standard_price=10, champion_name='Harbringer', champion_price=10,
            base_gear=['Hand weapon', 'Light armour'],
            options=[self.wep]
        )

    def check_rules(self):
        self.wep.set_visible_options(['bow3'], self.wep.get('sp'))
        self.wep.set_visible_options(['bow1'], not self.wep.get('sp'))
        FCGUnit.check_rules(self)
