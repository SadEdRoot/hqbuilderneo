from builder.games.whfb.roster import (
    LordsSection as BaseLordsSection,
    HeroesSection as BaseHeroesSection,
    CoreSection as BaseCoreSection,
    SpecialSection as BaseSpecialSection,
    RareSection as BaseRareSection,
    FantasyRoster
)

from .lords import *
from .heroes import *
from .core import *
from .special import *
from .rare import *

__author__ = 'Denis Romanov'


class LordsSection(BaseLordsSection):
    def __init__(self, parent):
        super(LordsSection, self).__init__(parent=parent)
        UnitType(self, Orion)
        UnitType(self, Durthu)
        UnitType(self, Araloth)
        UnitType(self, GladeLord)
        UnitType(self, SpellWeaver)
        UnitType(self, TreemanAncient)


class HeroesSection(BaseHeroesSection):
    def __init__(self, parent):
        super(HeroesSection, self).__init__(parent=parent)
        UnitType(self, Drycha)
        UnitType(self, NaestraArahan)
        UnitType(self, GladeCaptain)
        UnitType(self, SpellSigner)
        UnitType(self, ShadowDancer)
        UnitType(self, Waystalker)
        UnitType(self, BranchWraith)


class CoreSection(BaseCoreSection):
    def __init__(self, parent):
        super(CoreSection, self).__init__(parent=parent)
        UnitType(self, GladeGuard)
        UnitType(self, Dryads)
        UnitType(self, GladeRiders)
        UnitType(self, EternalGuard)


class SpecialSection(BaseSpecialSection):
    def __init__(self, parent):
        super(SpecialSection, self).__init__(parent=parent)
        UnitType(self, WildwoodRangers)
        UnitType(self, Wardancers)
        UnitType(self, Treekin)
        UnitType(self, DeepWoodScouts)
        UnitType(self, WarhawkRiders)
        UnitType(self, Sisters)
        UnitType(self, WildRiders)


class RareSection(BaseRareSection):
    def __init__(self, parent):
        super(RareSection, self).__init__(parent=parent)
        UnitType(self, Waywatchers)
        UnitType(self, Eagles)
        UnitType(self, Treeman)


class WoodElvesV2(FantasyRoster):
    army_name = 'Wood Elves'
    army_id = 'wood_elves_v2'
    development = True

    def __init__(self):
        super(WoodElvesV2, self).__init__(
            lords=LordsSection(parent=self),
            heroes=HeroesSection(parent=self),
            core=CoreSection(parent=self),
            special=SpecialSection(parent=self),
            rare=RareSection(parent=self)
        )
