__author__ = 'Denis Romanov'

from builder.core.unit import Unit, OptionsList
from builder.games.whfb.unit import FCGUnit
from builder.core.model_descriptor import ModelDescriptor
from .special import unit_standards


class Shaggoth(Unit):
    name = 'Dragon Ogre Shaggoth'
    base_points = 215
    gear = ['Hand weapon', 'Light armour']

    def __init__(self):
        Unit.__init__(self)
        self.opt = self.opt_options_list('Weapon', [
            ['Hand weapon', 5, 'hw'],
            ['Great weapon', 12, 'gw'],
        ], limit=1)


class Giant(Unit):
    name = 'Chaos Giant'
    base_points = 200
    gear = ['Hand weapon']

    def __init__(self):
        Unit.__init__(self)
        self.marks = self.opt_options_list('Options', [
            ['Mark of Khorne', 10, 'kh'],
            ['Mark of Tzeentch', 15, 'tz'],
            ['Mark of Nurgle', 15, 'ng'],
            ['Mark of Slaanesh', 5, 'sl'],
        ], limit=1)


class Spawn(Unit):
    name = 'Chaos Spawn'
    base_points = 50

    def __init__(self):
        Unit.__init__(self)
        self.marks = self.opt_options_list('Options', [
            ['Mark of Khorne', 5, 'kh'],
            ['Mark of Tzeentch', 20, 'tz'],
            ['Mark of Nurgle', 10, 'ng'],
            ['Mark of Slaanesh', 10, 'sl'],
        ], limit=1)


class Slaughterbrute(Unit):
    name = 'Slaughterbrute'
    base_points = 205

    def __init__(self):
        Unit.__init__(self)
        self.marks = self.opt_options_list('Options', [
            ['Extra claws', 20, 'cw'],
        ], limit=1)


class Mutalith(Unit):
    name = 'Mutalith Vortex Beast'
    base_points = 240
    static = True


class Hellcannon(Unit):
    name = 'Hellcannon'
    base_points = 210
    static = True

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Hellcannon', count=1).build())
        self.description.add(ModelDescriptor(name='Chaos Dwarf Handler', count=3, gear=['Hand weapon']).build())


class Skullcrushers(FCGUnit):
    name = 'Skullcrushers of Khorne'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Skullcrusher', model_price=75, min_models=3,
            musician_price=10, standard_price=10, champion_name='Skullhunter', champion_price=10,
            magic_banners=unit_standards,
            base_gear=['Hand weapon', 'Chaos armour', 'Shield'],
            options=[
                OptionsList('wep', 'Weapon', [
                    ['Lance', 2, 'ln'],
                    ['Ensorcelled weapon', 3, 'gw'],
                ], limit=1)
            ]
        )

    def check_rules(self):
        self.ban.set_visible_options(['woc_blas'], False)
        self.ban.set_visible_options(['woc_rage'], True)
        FCGUnit.check_rules(self)