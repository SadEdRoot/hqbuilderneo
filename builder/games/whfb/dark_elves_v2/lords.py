from .armory import *
from builder.games.whfb.roster import Character, Lore

__author__ = 'Denis Romanov'


class Mount(OptionsList):
    def __init__(self, parent):
        super(Mount, self).__init__(parent, name='Mounts')
        self.mounts = []

    def check_rules(self):
        super(Mount, self).check_rules()
        self.process_limit(self.mounts, 1)

    def has_mount(self):
        return any(o.value and o.used for o in self.mounts)

    def simple_mount(self, name, points):
        self.mounts.append(self.variant(name=name, points=points, gear=UnitDescription(name=name, points=points)))


class ColdOne(Mount):
    def __init__(self, parent):
        super(ColdOne, self).__init__(parent)
        self.simple_mount('Cold One', 25)


class DarkSteed(Mount):
    def __init__(self, parent):
        super(DarkSteed, self).__init__(parent)
        self.simple_mount('Dark Steed', 20)


class DarkPegasus(Mount):
    def __init__(self, parent):
        super(DarkPegasus, self).__init__(parent)
        self.simple_mount('Dark Pegasus', 50)


class BlackDragon(Mount):
    def __init__(self, parent):
        super(BlackDragon, self).__init__(parent)
        self.simple_mount('Black Dragon', 300)


class Seraphon(Mount):
    def __init__(self, parent):
        super(Seraphon, self).__init__(parent)
        self.simple_mount('Seraphon (Black Dragon)', 300)


class ColdOneChariot(Mount):
    def __init__(self, parent):
        super(ColdOneChariot, self).__init__(parent)
        name = 'Cold One Chariot'
        points = 120
        self.mounts.append(self.variant(
            name=name, points=points,
            gear=UnitDescription(name=name, points=points, options=[
                UnitDescription('Knight Charioteers', options=[Gear('Spear'), Gear('Hand weapon'),
                                                               Gear('Repeater crossbow')]),
                UnitDescription('Cold One', count=2),
                Gear('Scythes')
            ])
        ))


class Cauldron(Mount):
    def __init__(self, parent):
        super(Cauldron, self).__init__(parent)
        name = 'Cauldron of Blood'
        points = 190
        self.mounts.append(self.variant(
            name=name, points=points,
            gear=UnitDescription(name=name, points=points, options=[
                UnitDescription('Witch Elf Crew', options=[Gear('Hand weapon', count=2)]),
                Gear('Scythes')
            ])
        ))


class MalekithChariot(Mount):
    def __init__(self, parent):
        super(MalekithChariot, self).__init__(parent)
        name = 'Cold One Chariot'
        points = 110
        self.mounts.append(self.variant(
            name=name, points=points,
            gear=UnitDescription(name=name, points=points, options=[
                UnitDescription('Cold One', count=2),
                Gear('Scythes')
            ])
        ))


class Manticore(Mount):
    def __init__(self, parent):
        super(Manticore, self).__init__(parent)
        self.mant = self.variant(name='Manticore', points=150, gear=[])
        self.mounts.append(self.mant)
        self.mant_opt = [
            self.variant('Iron Hard Skin', points=20, gear=[]),
            self.variant('Blind Rage', points=25, gear=[]),
        ]

    def check_rules(self):
        super(Manticore, self).check_rules()
        for o in self.mant_opt:
            o.visible = o.used = self.mant.value and self.mant.used

    @property
    def description(self):
        desc = super(Manticore, self).description
        if self.mant.value and self.mant.used:
            model = UnitDescription('Manticore', points=self.mant.points)
            for o in self.mant_opt:
                if o.value and o.used:
                    model.add_points(o.points)
                    model.add(Gear(o.title))
            desc.append(model)
        return desc


class Malekith(Unit):
    type_name = 'Malekith'
    type_id = 'malekith_v1'

    class Mount(Seraphon, MalekithChariot, ColdOne):
        pass

    def __init__(self, parent):
        super(Malekith, self).__init__(
            parent, points=510, gear=[Gear('Level 4 Wizard'), Gear('Lore of Dark Magic'), Gear('Destroyer'),
                                      Gear('Armour of Midnight'), Gear('Supreme Spellshield'), Gear('Circlet of Iron')])
        self.Mount(self)


class Morathi(Unit):
    type_name = 'Morathi'
    type_id = 'morathi_v1'

    def __init__(self, parent):
        super(Morathi, self).__init__(
            parent, points=375, gear=[Gear('Level 4 Wizard'), Gear('Lore of Dark Magic'), Gear('Lore of Death'),
                                      Gear('Lore of Shadow'), Gear('Heartrender'), Gear('Darksword'),
                                      UnitDescription('Sulephet (Dark Pegasus)')])


class Malus(Unit):
    type_name = 'Malus Darkblade'
    type_id = 'malus_v1'

    def __init__(self, parent):
        super(Malus, self).__init__(
            parent, points=295, gear=[Gear('Heavy armour'), Gear('Sea Dragon cloak'), Gear('Warpsword of Khaine'),
                                      UnitDescription('Spite (Cold One)')])


class Crone(Unit):
    type_name = 'Hellebron'
    type_id = 'hellebron_v1'

    class Mount(Cauldron, Manticore):
        pass

    def __init__(self, parent):
        super(Crone, self).__init__(
            parent, points=310, gear=[Gear('Deathsword'), Gear('Cursed Blade'), Gear('Amulet of Fire'),
                                      Gear('Rune of Khaine'), Gear('Witchbrew'), Gear('Cry of War')])
        self.Mount(self)


class Dreadlord(Character):
    type_name = 'Dreadlord'
    type_id = 'dreadlord_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Dreadlord.Weapon, self).__init__(parent, name='Weapon')
            self.hw = self.variant('Additional hand weapon', 3, gear=Gear('Hand weapon'))
            self.halberd = self.variant('Halberd', 3)
            self.lance = self.variant('Lance', 7)
            self.gw = self.variant('Great weapon', 6)
            self.melee = [self.hw, self.halberd, self.lance, self.gw]
            self.ranged = [
                self.variant('Repeater crossbow', 5),
                self.variant('Repeater handbow', 5),
                self.variant('Brace of repeater handbows', 10, gear=Gear('Repeater handbow', count=2))
            ]

        def check_rules(self):
            has_mount = self.parent.mount.has_mount()
            if has_mount and self.hw.value:
                self.hw.value = False
            if not has_mount and self.lance.value:
                self.hw.value = False
            self.process_limit(self.melee, 1)
            self.process_limit(self.ranged, 1)
            self.hw.active = self.hw.used = not has_mount
            self.lance.active = self.lance.used = has_mount

    class ArmourSuit(OneOf):
        def __init__(self, parent):
            super(Dreadlord.ArmourSuit, self).__init__(parent, name='Armour')
            self.armour = self.variant('Light armour', 0)
            self.armour = self.variant('Heavy armour', 6)

        def check_rules(self):
            super(Dreadlord.ArmourSuit, self).check_rules()
            self.used = self.active = not self.parent.magic_armour.has_suit()

    class Armour(OptionsList):
        def __init__(self, parent):
            super(Dreadlord.Armour, self).__init__(parent, name='')
            self.shield = self.variant('Shield', 3)
            self.armour = self.variant('Sea Dragon cloak', 8)

        def check_rules(self):
            super(Dreadlord.Armour, self).check_rules()
            self.shield.used = self.shield.active = not self.parent.magic_armour.has_shield()

    class Mount(BlackDragon, Manticore, ColdOneChariot, DarkPegasus, ColdOne, DarkSteed):
        pass

    def __init__(self, parent):
        super(Dreadlord, self).__init__(parent, points=140, gear=[Gear('Hand weapon')], magic_limit=100)
        self.Weapon(self)
        self.ArmourSuit(self)
        self.Armour(self)
        self.mount = self.Mount(self)

        self.magic_weapon = Weapon(self)
        self.magic_armour = Armour(self)
        self.magic = [
            self.magic_weapon,
            self.magic_armour,
            Enchanted(self),
            Talismans(self),
        ]


class SupremeSorceress(Character):
    type_name = 'Supreme Sorceress'
    type_id = 'supreme_sorceress_v1'

    class Mount(BlackDragon, Manticore, DarkPegasus, ColdOne, DarkSteed):
        pass

    class MagicLevel(OneOf):
        def __init__(self, parent):
            super(SupremeSorceress.MagicLevel, self).__init__(parent, 'Wizard')
            self.variant('Level 3 Wizard')
            self.variant('Level 4 Wizard', 35)

    class Lore(Lore):
        def __init__(self, parent):
            super(SupremeSorceress.Lore, self).__init__(parent)
            self.variant('The Lore of Dark Magic')

    def __init__(self, parent):
        super(SupremeSorceress, self).__init__(parent, points=185, gear=[Gear('Hand weapon')], magic_limit=100)

        self.Mount(self)
        self.MagicLevel(self)
        self.Lore(self)

        self.magic = [
            Weapon(self),
            Enchanted(self),
            Arcane(self),
            Talismans(self),
        ]


class HighBeastmaster(Character):
    type_name = 'High Beastmaster'
    type_id = 'high_beastmaster_v1'

    class Mount(OneOf):

        class Manticore(Unit):
            def __init__(self):
                super(HighBeastmaster.Mount.Manticore, self).__init__(None, name='Manticore')
                self.Options(self)

            class Options(OptionsList):
                def __init__(self, parent):
                    super(HighBeastmaster.Mount.Manticore.Options, self).__init__(parent, 'Options')
                    self.variant('Iron Hard Skin', points=20),
                    self.variant('Blind Rage', points=25),

        def __init__(self, parent):
            super(HighBeastmaster.Mount, self).__init__(parent, name='Mount')
            self.variant('Scourge Runner Chariot', points=0, gear=UnitDescription(
                name='Scourge Runner Chariot', options=[
                    UnitDescription('Beastmaster Crew', options=[Gear('Spear'), Gear('Hand weapon'),
                                                                 Gear('Repeater crossbow')], count=1),
                    UnitDescription('Dark Steed', count=2),
                    Gear('Ravager harpoon')
                ])
            )
            self.mant = self.variant('Manticore', points=0, gear=[])
            self.mant_unit = SubUnit(parent, self.Manticore())

        def check_rules(self):
            self.mant_unit.visible = self.mant_unit.used = self.cur == self.mant

    def __init__(self, parent):
        super(HighBeastmaster, self).__init__(
            parent, points=300,
            gear=[Gear('Hand weapon'), Gear('Sea Dragon cloak')], magic_limit=100
        )
        self.Mount(self)
        self.magic = []
        self.magic.append(Weapon(self))
        self.armour = Armour(self)
        self.magic.append(self.armour)
        self.magic.extend([
            Enchanted(self),
            Talismans(self),
        ])

    def build_description(self):
        desc = super(HighBeastmaster, self).build_description()
        if not self.armour.has_suit():
            desc.add(Gear('Light armour'))
        return desc


class BlackArkFleetmaster(Character):
    type_name = 'Black Ark Fleetmaster'
    type_id = 'fleetmaster_v1'

    def __init__(self, parent):
        super(BlackArkFleetmaster, self).__init__(
            parent, points=155,
            gear=[Gear('Hand weapon', count=2), Gear('Sea Dragon cloak')], magic_limit=50
        )
        self.magic = []
        self.magic.append(Weapon(self, limit=50))
        self.armour = Armour(self, limit=50)
        self.magic.append(self.armour)
        self.magic.extend([
            Enchanted(self, limit=50),
            Talismans(self, limit=50),
        ])

    def build_description(self):
        desc = super(BlackArkFleetmaster, self).build_description()
        if not self.armour.has_suit():
            desc.add(Gear('Light armour'))
        return desc
