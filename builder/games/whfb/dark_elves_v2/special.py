from builder.core2 import *
from builder.games.whfb.roster import FCGUnit
from .armory import Standards, Weapon

__author__ = 'Denis Romanov'


class Shades(FCGUnit):
    type_name = 'Shades'
    type_id = 'shades_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Shades.Weapon, self).__init__(parent, 'Options', limit=1)
            self.variant('Additional hand weapon', points=2, gear=Gear('Hand weapon'))
            self.variant('Great weapon', points=2)

    class Armour(OptionsList):
        def __init__(self, parent):
            super(Shades.Armour, self).__init__(parent, 'Options')
            self.variant('Light armour', points=1)

    def __init__(self, parent):
        super(Shades, self).__init__(
            parent, model_name='Shade', model_price=16, min_models=5,
            musician_price=10,
            standard_price=10,
            champion_name='Bloodshade', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Repeater crossbow')]
        )
        self.model_options = [self.Weapon(self), self.Armour(self)]


class ColdOneChariot(Unit):
    type_name = 'Cold One Chariot'
    type_id = 'cold_one_chariot_v1'

    def __init__(self, parent):
        super(ColdOneChariot, self).__init__(parent, points=110, gear=[
            UnitDescription('Knight Charioteers', options=[Gear('Spear'), Gear('Hand weapon'),
                                                           Gear('Repeater crossbow')], count=2),
            UnitDescription('Cold One', count=2),
            Gear('Scythes')
        ])


class ScourgeRunnerChariot(Unit):
    type_name = 'Scourgerunner Chariot'
    type_id = 'scourgerunner_chariot_v1'

    def __init__(self, parent):
        super(ScourgeRunnerChariot, self).__init__(parent, points=150, gear=[
            UnitDescription('Beastmaster Crew', options=[Gear('Spear'), Gear('Hand weapon'),
                                                         Gear('Repeater crossbow')], count=2),
            UnitDescription('Dark Steed', count=2),
            Gear('Ravager harpoon')
        ])


class ReaperBoltThrower(Unit):
    type_name = 'Reaper Bolt Thrower'
    type_id = 'reaper_bolt_thrower_v1'

    def __init__(self, parent):
        super(ReaperBoltThrower, self).__init__(parent, points=70, gear=[
            UnitDescription('Dark Elf Crew', options=[Gear('Hand weapon'), Gear('Light armour')], count=2),
            UnitDescription('Reaper Bolt Thrower'),
        ])


class ColdOneKnights(FCGUnit):
    type_name = 'Cold One Knights'
    type_id = 'cold_one_knights_v1'

    def __init__(self, parent):
        super(ColdOneKnights, self).__init__(
            parent, model_name='Cold One Knight', model_price=30, min_models=5,
            musician_price=10,
            standard_price=10,
            champion_name='Dread Knight', champion_price=10,
            gear=[Gear('Lance'), Gear('Hand weapon'), Gear('Shield'), Gear('Heavy armour')]
        )
        self.magic_banners = [Standards(self, limit=50)]
        self.champion_magic_options = [Weapon(self, name='Dread Knight\'s weapon', limit=25)]


class BlackGuard(FCGUnit):
    type_name = 'Black Guard of Naggarond'
    type_id = 'black_guard_v1'

    def __init__(self, parent):
        super(BlackGuard, self).__init__(
            parent, model_name='Black Guard', model_price=15, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Tower Master', champion_price=10,
            gear=[Gear('Halberd'), Gear('Heavy armour')]
        )
        self.magic_banners = [Standards(self, limit=50)]
        self.champion_magic_options = [Weapon(self, name='Tower Master\'s weapon', limit=25)]


class Executioners(FCGUnit):
    type_name = 'Har Ganeth Executioners'
    type_id = 'executioners_v1'

    def __init__(self, parent):
        super(Executioners, self).__init__(
            parent, model_name='Executioner', model_price=12, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Draich Master', champion_price=10,
            gear=[Gear('Great weapon'), Gear('Heavy armour')]
        )
        self.magic_banners = [Standards(self, limit=50)]


class Harpies(Unit):
    type_name = 'Harpies'
    type_id = 'harpies_v1'

    def __init__(self, parent):
        super(Harpies, self).__init__(parent)
        self.models = Count(self, 'Harpies', 5, 100, points=15, per_model=True,
                            gear=UnitDescription('Harpies', points=15))

    def get_count(self):
        return self.models.cur


class WarHydra(Unit):
    type_id = 'warhydra_v1'
    type_name = 'War Hydra'

    class Options(OptionsList):
        def __init__(self, parent):
            super(WarHydra.Options, self).__init__(parent, 'Options')
            self.variant('Fiery Breath', points=20)
            self.variant('Spit Fire', points=20)

    def __init__(self, parent):
        super(WarHydra, self).__init__(parent, points=160)
        self.Options(self)
