from builder.core2 import *
from builder.games.whfb.roster import FCGUnit
from .armory import Standards

__author__ = 'Denis Romanov'


class Dreadspears(FCGUnit):
    type_name = 'Dreadspears'
    type_id = 'dreadspears_v1'

    def __init__(self, parent):
        super(Dreadspears, self).__init__(
            parent, model_name='Dark Elf Warrior', model_price=9, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Lording', champion_price=10,
            gear=[Gear('Spear'), Gear('Light armour'), Gear('Shield')]
        )
        self.magic_banners = [Standards(self, limit=25)]


class Blackswords(FCGUnit):
    type_name = 'Blackswords'
    type_id = 'blackswords_v1'

    def __init__(self, parent):
        super(Blackswords, self).__init__(
            parent, model_name='Dark Elf Warrior', model_price=9, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Lording', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Light armour'), Gear('Shield')]
        )
        self.magic_banners = [Standards(self, limit=25)]


class Darkshards(FCGUnit):
    type_name = 'Darkshards'
    type_id = 'darkshards_v1'

    class Opt(OptionsList):
        def __init__(self, parent):
            super(Darkshards.Opt, self).__init__(parent, 'Options')
            self.variant('Shield', points=1)

    def __init__(self, parent):
        super(Darkshards, self).__init__(
            parent, model_name='Dark Elf Warrior', model_price=12, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Guardmaster', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Repeater crossbow'), Gear('Light armour')]
        )
        self.magic_banners = [Standards(self, limit=25)]
        self.model_options = [self.Opt(self)]


class DarkRaiders(FCGUnit):
    type_name = 'Dark Raiders'
    type_id = 'darkraiders_v1'

    class Opt(OptionsList):
        def __init__(self, parent):
            super(DarkRaiders.Opt, self).__init__(parent, 'Options')
            self.variant('Shield', points=1)
            self.variant('Repeater crossbow', points=3)

    def __init__(self, parent):
        super(DarkRaiders, self).__init__(
            parent, model_name='Dark Rider', model_price=16, min_models=5,
            musician_price=10,
            standard_price=10,
            champion_name='Herald', champion_price=10,
            gear=[Gear('Spear'), Gear('Light armour')]
        )
        self.model_options = [self.Opt(self)]


class Corsairs(FCGUnit):
    type_name = 'Black Ark Corsairs'
    type_id = 'corsairs_v1'

    class Opt(OneOf):
        def __init__(self, parent):
            super(Corsairs.Opt, self).__init__(parent, 'Options')
            self.variant('Additional hand weapon', points=2, gear=Gear('Hand weapon'))
            self.variant('Repeater handbow', points=2)

    class Reaver(OptionsList):
        def __init__(self, parent):
            super(Corsairs.Reaver, self).__init__(parent, 'Reaver\'s')
            self.variant('Brace of repeater handbows', points=4, gear=Gear('Repeater handbow', count=2))

    def __init__(self, parent):
        super(Corsairs, self).__init__(
            parent, model_name='Black Ark Corsairs', model_price=9, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Reaver', champion_price=10,
            gear=[Gear('Hand weapon'), Gear('Sea Dragon cloak'), Gear('Light armour')]
        )
        self.magic_banners = [Standards(self, limit=25)]
        self.champion_options = [self.Reaver(self)]
        self.model_options = [self.Opt(self)]


class WitchElves(FCGUnit):
    type_name = 'Witch Elves'
    type_id = 'witch_elves_v1'

    def __init__(self, parent):
        super(WitchElves, self).__init__(
            parent, model_name='Witch Elf', model_price=11, min_models=10,
            musician_price=10,
            standard_price=10,
            champion_name='Hag', champion_price=10,
            gear=[Gear('Hand weapon', count=2)]
        )
        self.magic_banners = [Standards(self, limit=50)]
