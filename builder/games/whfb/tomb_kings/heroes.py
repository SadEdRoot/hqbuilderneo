__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.tomb_kings.mounts import *
from builder.games.whfb.tomb_kings.armory import *
from builder.core.options import norm_point_limits
from builder.games.whfb.tomb_kings.special import Warsphinx

heroes_weapon = filter_items(tk_weapon, 50)
heroes_armour = filter_items(tk_armour, 50)
heroes_talisman = filter_items(tk_talisman, 50)
heroes_arcane = filter_items(tk_arcane, 50)
heroes_enchanted = filter_items(tk_enchanted, 50)


class TombPrince(Unit):
    name = 'Tomb Prince'
    gear = ['Hand Weapon']
    base_points = 100

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
            ['Flail', 2, 'hw'],
            ['Spear', 2, 'sp'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 2, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [KingChariot(), Warsphinx(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(tk_shields_ids))
        self.gear = ['Hand weapon'] + (['Light armour'] if not self.magic_arm.is_selected(tk_armour_suits_ids) else [])
        self.magic_enc.set_visible_options(['tk_cloak'], self.steed.get_count() == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Necrotect(Unit):
    name = 'Necrotect'
    gear = ['Hand Weapon', 'Whip']
    base_points = 60

    def __init__(self):
        Unit.__init__(self)
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.gear = \
            ['Hand weapon', 'Whip'] + (['Light armour'] if not self.magic_arm.is_selected(tk_armour_suits_ids) else [])
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class TombHerald(Unit):
    name = 'Tomb Herald'
    gear = ['Hand Weapon']
    base_points = 60

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Great weapon', 4, 'gw'],
            ['Flail', 2, 'hw'],
            ['Halberd', 4, 'hb'],
            ['Spear', 2, 'sp'],
        ], limit=1)
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 2, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [KingChariot(), SkeletalSteed(10)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', tk_standard, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(tk_shields_ids))
        self.gear = ['Hand weapon'] + (['Light armour'] if not self.magic_arm.is_selected(tk_armour_suits_ids) else [])
        self.magic_enc.set_visible_options(['tk_cloak'], self.steed.get_count() == 0)
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])


class Priest(Unit):
    name = 'Liche Priest'
    gear = ['Hand Weapon']
    base_points = 70

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [SkeletalSteed(10)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Nehekhara', 0, 'neh'],
            ['The Lore of Light', 0, 'light'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.magic_enc.set_visible_options(['tk_cloak'], self.steed.get_count() == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Nekaph(Unit):
    name = 'The Herald Nekaph'
    base_points = 120
    gear = ['Flail of Skulls', 'Light armour']

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [KingChariot(), SkeletalSteed(10)])


class Apophas(StaticUnit):
    name = 'Prince Apophas'
    base_points = 130
    gear = ['Hand weapon', 'Light armour']


class Ramhotep(StaticUnit):
    name = 'Ramhotep the Visionary'
    base_points = 110
    gear = ['Hand weapon', 'Whip', 'Light armour']
