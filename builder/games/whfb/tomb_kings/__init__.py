__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from builder.games.whfb.tomb_kings.heroes import *
from builder.games.whfb.tomb_kings.lords import *
from builder.games.whfb.tomb_kings.core import *
from builder.games.whfb.tomb_kings.special import *
from builder.games.whfb.tomb_kings.rare import *


class TombKings(LegacyFantasy):
    army_name = 'Tomb Kings'
    army_id = '172091933f8448509cdee06c70b9e0fc'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Settra, Khalida, Arkhan, Khater, TombKing, HighPriest],
            heroes=[Nekaph, Apophas, Ramhotep, TombPrince, TombHerald, Priest, Necrotect],
            core=[Warriors, Archers, Horsemen, HorseArchers, Chariots],
            special=[TombGuard, NecropolisKnights, Scorpion, Ushabti, Swarm, Carrion, Warsphinx, Stalkers],
            rare=[Colossus, Hierotitan, Necrosphinx, Catapult, Casket]
        )
