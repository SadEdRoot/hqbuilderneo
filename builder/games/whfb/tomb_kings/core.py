__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit
from builder.core.unit import OptionsList
from .armory import tk_standard, filter_items

unit_standards = filter_items(tk_standard, 25)


class Warriors(FCGUnit):
    name = 'Skeleton Warriors'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Skeleton Warrior', model_price=4, min_models=10,
            musician_price=10, standard_price=10, champion_name='Master of Arms', champion_price=10,
            base_gear=['Hand weapon', 'Shield'],
            options=[
                OptionsList('arm', '', [
                    ['Spear', 1, 'sp'],
                    ['Light armour', 1, 'al'],
                ])
            ]
        )


class Archers(FCGUnit):
    name = 'Skeleton Archers'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Skeleton Archer', model_price=6, min_models=10,
            musician_price=10, standard_price=10, champion_name='Master of Arrows', champion_price=10,
            base_gear=['Hand weapon', 'Bow'],
            options=[
                OptionsList('arm', '', [
                    ['Light armour', 1, 'al'],
                ])
            ]
        )


class Horsemen(FCGUnit):
    name = 'Skeleton Horsemen'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Skeleton Horseman', model_price=12, min_models=5,
            musician_price=10, standard_price=10, champion_name='Master of Horse', champion_price=10,
            base_gear=['Hand weapon', 'Spear', 'Shield'],
            options=[
                OptionsList('arm', '', [
                    ['Light armour', 2, 'al'],
                ])
            ]
        )


class HorseArchers(FCGUnit):
    name = 'Skeleton Horse Archers'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Skeleton Horseman', model_price=14, min_models=5,
            musician_price=10, standard_price=10, champion_name='Master of Scouts', champion_price=10,
            base_gear=['Hand weapon', 'Bow'],
        )


class Chariots(FCGUnit):
    name = 'Skeleton Chariots'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Skeleton Chariot', model_price=55, min_models=3,
            musician_price=10, standard_price=10, champion_name='Master of Chariots', champion_price=10,
            base_gear=['Hand weapon', 'Bow', 'Spear'],
            magic_banners=unit_standards
        )
