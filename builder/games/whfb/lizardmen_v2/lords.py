from builder.games.whfb.roster import Character, BSB, Lore
from .special import Stegadon
from .armory import *

__author__ = 'Denis Romanov'


class Kroak(Unit):
    type_name = 'Lord Kroak'
    type_id = 'kroak_v1'

    def __init__(self, parent):
        super(Kroak, self).__init__(parent, points=400, unique=True, gear=[
            Gear('Golden Death Mask'),
            Gear('Level 4 Wizard'),
        ])


class Mazdamundi(Unit):
    type_name = 'Lord Mazdamundi'
    type_id = 'mazdamundi_v1'

    def __init__(self, parent):
        super(Mazdamundi, self).__init__(parent, points=780, unique=True, gear=[
            Gear('Cobra Mace of Mazdamundi'),
            Gear('Sunburst Standard of Hexoatl'),
            Gear('Level 4 Wizard'),
            UnitDescription('Zlaaq'),
        ])


class KroqGar(Unit):
    type_name = 'Kroq-Gar'
    type_id = 'kroq_v1'

    def __init__(self, parent):
        super(KroqGar, self).__init__(parent, points=520, unique=True, gear=[
            Gear('Light armour'),
            Gear('Hand of Gods'),
            Gear('Revered Spear of Tlanxia'),
            UnitDescription('Grymloq'),
        ])


class Disciplines(OptionsList):
    def __init__(self, parent):
        super(Disciplines, self).__init__(parent, 'Disciplines of the Old Ones', limit=4, points_limit=150)
        self.desc = [
            self.variant('Reservoir of Eldritch Energy', points=20),
            self.variant('Soul of Stone', points=25),
            self.variant('Becalming Cogitation', points=25),
            self.variant('Wandering deliberations', points=30),
            self.variant('Harmonic convergence', points=30),
            self.variant('The Harrowing Scrutiny', points=30),
            self.variant('The Transcendent Healing', points=30),
            self.variant('Unfathomable presence', points=30),
            self.variant('Focus of mystery', points=35),
            self.variant('Higher State of Consciousness', points=60),
        ]


class AncientStegadonMount(Unit):
    def __init__(self, parent):
        super(AncientStegadonMount, self).__init__(parent, 'Ancient Stegadon', points=280, gear=[
            Gear('Engine of the Gods'), Stegadon.crew.clone().set_count(4)
        ])
        Stegadon.Options(self)


class Tehenhauin(Unit):
    type_name = 'Tehenhauin'
    type_id = 'tehenhauin_v1'

    class Mount(OptionalSubUnit):

        def __init__(self, parent):
            super(Tehenhauin.Mount, self).__init__(parent, 'Mount')
            SubUnit(self, AncientStegadonMount(parent=None))

    def __init__(self, parent):
        super(Tehenhauin, self).__init__(parent, points=230, unique=True, gear=[
            Gear('Blade of the Serpent\'s Tongue'),
            Gear('Plaque of Sotek'),
            Gear('Level 3 Wizard'),
            Gear('The Lore of Beasts'),
        ])
        self.Mount(self)


class Slann(Character):
    type_name = 'Slann Mage-Priest'
    type_id = 'slaan_v1'

    class Lore(Lore):
        def __init__(self, parent):
            super(Slann.Lore, self).__init__(parent)
            self.variant('The Lore of High Magic')

    def __init__(self, parent):
        super(Slann, self).__init__(parent, points=300, gear=[Gear('Hand weapon'), Gear('Level 4 Wizard')],
                                    magic_limit=100)
        Disciplines(self)
        self.magic = [
            Weapon(self),
            Arcane(self),
            Enchanted(self),
            Talismans(self),
        ]
        self.slaan_bsb = BSB(self, Standards)
        self.Lore(self)

    def get_unique_gear(self):
        return super(Slann, self).get_unique_gear() + self.slaan_bsb.get_unique_gear()


class Mount(OptionsList):
    def __init__(self, parent):
        super(Mount, self).__init__(parent, name='Mounts')
        self.mounts = []

    def check_rules(self):
        super(Mount, self).check_rules()
        self.process_limit(self.mounts, 1)

    def has_mount(self):
        return any(o.value and o.used for o in self.mounts)


class ColdOne(Mount):
    def __init__(self, parent):
        super(ColdOne, self).__init__(parent)
        self.mounts.append(self.variant(name='Cold One', points=30, gear=UnitDescription(name='Cold One', points=30)))


class Carnosaur(Mount):
    def __init__(self, parent):
        super(Carnosaur, self).__init__(parent)
        self.carno = self.variant(name='Carnosaur', points=220, gear=[])
        self.mounts.append(self.carno)
        self.carno_opt = [
            self.variant('Loping Stride', points=15, gear=[]),
            self.variant('Bloodroar', points=25, gear=[]),
        ]

    def check_rules(self):
        super(Carnosaur, self).check_rules()
        for o in self.carno_opt:
            o.visible = o.used = self.carno.value and self.carno.used

    @property
    def description(self):
        desc = super(Carnosaur, self).description
        if self.carno.value and self.carno.used:
            model = UnitDescription('Carnosaur', points=self.carno.points)
            for o in self.carno_opt:
                if o.value and o.used:
                    model.add_points(o.points)
                    model.add(Gear(o.title))
            desc.append(model)
        return desc


class Oldblood(Character):
    type_name = 'Saurus Oldblood'
    type_id = 'oldblood_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Oldblood.Weapon, self).__init__(parent, name='Weapon', limit=1)
            self.hw = self.variant('Additional hand weapon', 3, gear=Gear('Hand weapon'))
            self.variant('Halberd', 3)
            self.variant('Spear', 3)
            self.variant('Great weapon', 6)

        def check_rules(self):
            has_mount = self.parent.mount.has_mount()
            if has_mount and self.hw.value:
                self.hw.value = False
            super(Oldblood.Weapon, self).check_rules()
            self.hw.active = self.hw.used = not has_mount

    class Armour(OptionsList):
        def __init__(self, parent):
            super(Oldblood.Armour, self).__init__(parent, name='Armour')
            self.armour = self.variant('Light armour', 9)
            self.shield = self.variant('Shield', 6)

        def check_rules(self):
            super(Oldblood.Armour, self).check_rules()
            self.armour.used = self.armour.active = not self.parent.magic_armour.has_suit()
            self.shield.used = self.shield.active = not self.parent.magic_armour.has_shield()

    class Mount(Carnosaur, ColdOne):
        pass

    def __init__(self, parent):
        super(Oldblood, self).__init__(parent, points=140, gear=[Gear('Hand weapon')], magic_limit=100)
        self.Weapon(self)
        self.Armour(self)
        self.mount = self.Mount(self)

        self.magic_weapon = Weapon(self)
        self.magic_armour = Armour(self)
        self.magic = [
            self.magic_weapon,
            self.magic_armour,
            Enchanted(self),
            Talismans(self),
        ]
