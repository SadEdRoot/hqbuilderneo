from .armory import *
from .lords import AncientStegadonMount, Stegadon, Carnosaur, Mount
from builder.games.whfb.roster import Character, BSB

__author__ = 'Denis Romanov'


class Chakax(Unit):
    type_name = 'Chakax'
    type_id = 'chakax_v1'

    def __init__(self, parent):
        super(Chakax, self).__init__(parent, points=270, unique=True, gear=[
            Gear('The Star-Stone Mace'),
            Gear('Helm of the Prime Guardian'),
            Gear('Key of the Eternity Chamber'),
        ])


class GorRok(Unit):
    type_name = 'Gor-Rok'
    type_id = 'gorrok_v1'

    def __init__(self, parent):
        super(GorRok, self).__init__(parent, points=185, unique=True, gear=[
            Gear('The Shield of Aeons'),
            Gear('The Mace of Ulamak'),
        ])


class TettoEko(Unit):
    type_name = 'Tetto\'eko'
    type_id = 'tetto_eko_v1'

    def __init__(self, parent):
        super(TettoEko, self).__init__(parent, points=185, unique=True, gear=[
            Gear('Level 2 Wizard'),
            Gear('The Lore of Heavens'),
            Gear('Hand weapon'),
            Gear('Eye of the Old Ones'),
            Gear('Stellar Staff'),
        ])


class Oxiotl(Unit):
    type_name = 'Oxyotl'
    type_id = 'oxiotl_v1'

    def __init__(self, parent):
        super(Oxiotl, self).__init__(parent, points=120, unique=True, gear=[
            Gear('Hand weapon'), Gear('The Golden Blowpipe of P\'Toohee')
        ])


class TictaqTo(Unit):
    type_name = 'Tiktaq\'to'
    type_id = 'tictaqto_v1'

    def __init__(self, parent):
        super(TictaqTo, self).__init__(parent, points=170, unique=True, gear=[
            Gear('Mask of Heavens'),
            Gear('Blade of the Ancient Skies'),
            UnitDescription('Zwup')
        ])


class ColdOne(Mount):
    def __init__(self, parent):
        super(ColdOne, self).__init__(parent)
        self.mounts.append(self.variant(name='Cold One', points=20, gear=UnitDescription(name='Cold One', points=20)))


class Scar(Character):
    type_name = 'Saurus Scar-Veteran'
    type_id = 'saurus_scar_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Scar.Weapon, self).__init__(parent, name='Weapon', limit=1)
            self.hw = self.variant('Additional hand weapon', 2, gear=Gear('Hand weapon'))
            self.variant('Halberd', 2)
            self.variant('Spear', 2)
            self.variant('Great weapon', 4)

        def check_rules(self):
            has_mount = self.parent.mount.has_mount()
            if has_mount and self.hw.value:
                self.hw.value = False
            super(Scar.Weapon, self).check_rules()
            self.hw.active = self.hw.used = not has_mount

    class Armour(OptionsList):
        def __init__(self, parent):
            super(Scar.Armour, self).__init__(parent, name='Armour')
            self.armour = self.variant('Light armour', 6)
            self.shield = self.variant('Shield', 4)

        def check_rules(self):
            super(Scar.Armour, self).check_rules()
            self.armour.used = self.armour.active = not self.parent.magic_armour.has_suit()
            self.shield.used = self.shield.active = not self.parent.magic_armour.has_shield()

    class Mount(Carnosaur, ColdOne):
        pass

    def __init__(self, parent):
        super(Scar, self).__init__(parent, points=80, gear=[Gear('Hand weapon')], magic_limit=50)
        self.Weapon(self)
        self.Armour(self)
        self.mount = self.Mount(self)

        self.bsb = BSB(self, Standards)

        self.magic_weapon = Weapon(self, limit=50)
        self.magic_armour = Armour(self, limit=50)
        self.magic = [
            self.magic_weapon,
            self.magic_armour,
            Enchanted(self, limit=50),
            Talismans(self, limit=50),
        ]


class Chief(Character):
    type_name = 'Skink Chief'
    type_id = 'skink_chief_v1'

    class Weapon(OptionsList):
        def __init__(self, parent):
            super(Chief.Weapon, self).__init__(parent, name='Weapon', limit=1)
            self.hw = self.variant('Additional hand weapon', 2, gear=Gear('Hand weapon'))
            self.variant('Blowpipe', 6)
            self.variant('Lustrian javelin', 2)
            self.variant('Spear', 2)

        def check_rules(self):
            has_mount = self.parent.mount.has_mount()
            if has_mount and self.hw.value:
                self.hw.value = False
            super(Chief.Weapon, self).check_rules()
            self.hw.active = self.hw.used = not has_mount

    class Armour(OptionsList):
        def __init__(self, parent):
            super(Chief.Armour, self).__init__(parent, name='Armour')
            self.armour = self.variant('Light armour', 2)
            self.shield = self.variant('Shield', 2)

        def check_rules(self):
            super(Chief.Armour, self).check_rules()
            self.armour.used = self.armour.active = not self.parent.magic_armour.has_suit()
            self.shield.used = self.shield.active = not self.parent.magic_armour.has_shield()

    class Mount(OptionsList):

        class Stegadon(Unit):
            type_name = 'Stegadon'
            model_points = 215

            def __init__(self, parent):
                super(Chief.Mount.Stegadon, self).__init__(parent, points=self.model_points, gear=[
                    Gear('Giant bow'), Stegadon.crew.clone().set_count(4)
                ])
                Stegadon.Options(self)

        class AncientStegadon(Unit):
            type_name = 'Ancient Stegadon'
            model_points = 230

            def __init__(self, parent):
                super(Chief.Mount.AncientStegadon, self).__init__(parent, points=self.model_points, gear=[
                    Gear('Giant Blowpipes'), Stegadon.crew.clone().set_count(4)
                ])
                Stegadon.Options(self)

        def __init__(self, parent):
            super(Chief.Mount, self).__init__(parent, name='Mount', limit=1)
            self.variant('Terradon', points=35, gear=[UnitDescription('Terradon', 35)])
            self.variant('Ripperdactyl', points=40, gear=[UnitDescription('Ripperdactyl', 40)])
            self.steg = self.variant(self.Stegadon.type_name, points=self.Stegadon.model_points)
            self.ant = self.variant(self.AncientStegadon.type_name, points=self.AncientStegadon.model_points)
            self.steg.used = self.ant.used = False

            self.steg_model = SubUnit(parent, self.Stegadon(None))
            self.ant_model = SubUnit(parent, self.AncientStegadon(None))

        def check_rules(self):
            super(Chief.Mount, self).check_rules()
            self.steg_model.visible = self.steg_model.used = self.steg.value
            self.ant_model.visible = self.ant_model.used = self.ant.value

        def has_mount(self):
            return self.count > 0

    def __init__(self, parent):
        super(Chief, self).__init__(parent, points=40, gear=[Gear('Hand weapon')], magic_limit=50)
        self.Weapon(self)
        self.Armour(self)
        self.mount = self.Mount(self)

        self.bsb = BSB(self, Standards)

        self.magic_weapon = Weapon(self, limit=50)
        self.magic_armour = Armour(self, limit=50)
        self.magic = [
            self.magic_weapon,
            self.magic_armour,
            Enchanted(self, limit=50),
            Talismans(self, limit=50),
        ]


class Priest(Character):
    type_name = 'Skink Priest'
    type_id = 'skink_priest_v1'

    class Mount(OptionalSubUnit):
        def __init__(self, parent):
            super(Priest.Mount, self).__init__(parent, 'Mount')
            SubUnit(self, AncientStegadonMount(parent=None))

    class MagicLevel(OneOf):
        def __init__(self, parent):
            super(Priest.MagicLevel, self).__init__(parent, 'Wizard')
            self.variant('Level 1 Wizard')
            self.variant('Level 2 Wizard', 35)

    class Lore(OptionsList):
        def __init__(self, parent):
            super(Priest.Lore, self).__init__(parent, 'Lores')
            self.variant('The Lore of Beasts')
            self.variant('The Lore of Heavens')

    def __init__(self, parent):
        super(Priest, self).__init__(parent, points=65, gear=[Gear('Hand weapon')], magic_limit=50)

        self.Mount(self)
        self.MagicLevel(self)
        self.Lore(self)

        self.magic = [
            Weapon(self, limit=50),
            Enchanted(self, limit=50),
            Arcane(self, limit=50),
            Talismans(self, limit=50),
        ]
