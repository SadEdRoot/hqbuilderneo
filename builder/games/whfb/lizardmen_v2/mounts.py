__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit


class ColdOne(Unit):
    name = 'Cold One'
    static = True

    def __init__(self, price=30):
        Unit.__init__(self)
        self.base_points = price


class Carnosaur(Unit):
    name = 'Carnosaur'
    static = True

    def __init__(self, price=210):
        Unit.__init__(self)
        self.base_points = price


class Terradon(Unit):
    name = 'Terradon'
    static = True

    def __init__(self, price=30):
        Unit.__init__(self)
        self.base_points = price
#
#
# class GiantWolf(Unit):
#     name = 'Giant Wolf'
#     static = True
#
#     def __init__(self, price=18):
#         Unit.__init__(self)
#         self.base_points = price
#
#
# class GiantSpider(Unit):
#     name = 'Giant Spider'
#     static = True
#
#     def __init__(self, price=22):
#         Unit.__init__(self)
#         self.base_points = price
#
#
# class GiganticSpider(Unit):
#     name = 'Gigantic Spider'
#     static = True
#
#     def __init__(self, price=50):
#         Unit.__init__(self)
#         self.base_points = price
#
#
# class GreatCaveSquig(Unit):
#     name = 'Great Cave Squig'
#     static = True
#
#     def __init__(self, price=40):
#         Unit.__init__(self)
#         self.base_points = price
#
#
# class Wyvern(StaticUnit):
#     name = 'Wyvern'
#     base_points = 160
