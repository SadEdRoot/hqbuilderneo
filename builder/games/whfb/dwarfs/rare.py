__author__ = 'Denis Romanov'


from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.dwarfs.special import WarMachine


class OrganGun(Unit):
    name = 'Organ Gun'
    base_points = 120
    static = True

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name=self.name, count=1).build())
        self.description.add(ModelDescriptor(name='Crew', count=3, gear=['Hand weapon', 'Light armour']).build())


class FlameCannon(OrganGun):
    name = 'Flame Cannon'
    base_points = 140


class Gyrocopter(Unit):
    name = 'Gyrocopter'
    base_points = 140
    static = True

    def __init__(self):
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name=self.name, count=1).build())
        self.description.add(ModelDescriptor(name='Pilot', count=1, gear=['Hand weapon']).build())
