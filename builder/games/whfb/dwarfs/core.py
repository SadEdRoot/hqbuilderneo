__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit
from builder.core.unit import OptionsList, OneOf
from .armory import *


class Warriors(FCGUnit):
    name = 'Warriors'
    single_name = 'Warrior'
    model_price = 8

    def __init__(self):
        self.rangers = OptionsList('rang', 'Options', [
            ['Rangers', 1, 'rng'],
            ['Throwing axes', 1, 'ta']
        ])
        self.gw = OptionsList('wep', 'Weapon', [['Great weapon', 2, 'gw']])

        FCGUnit.__init__(
            self, model_name=self.single_name, model_price=self.model_price, min_models=10,
            musician_price=5, standard_price=10, champion_name='Veteran', champion_price=10,
            base_gear=['Hand weapon', 'Heavy armour'],
            options=[
                self.gw,
                OptionsList('sh', 'Armour', [['Shield', 1, 'sh']]),
                self.rangers
            ]
        )

    def check_rules(self):
        self.rangers.set_visible(self.gw.get('gw'))
        FCGUnit.check_rules(self)

    def is_rangers(self):
        return self.rangers.get('rng')


class Longbeards(Warriors):
    name = 'Longbeards'
    single_name = 'Longbeard'
    model_price = 11

    def __init__(self):
        self.rangers = OptionsList('rang', 'Options', [
            ['Rangers', 1, 'rng'],
            ['Throwing axes', 1, 'ta']
        ])
        self.gw = OptionsList('wep', 'Weapon', [['Great weapon', 2, 'gw']])
        self.runic = OptionsList('opt', 'Runic standard', lesser_runic_standards, points_limit=50)

        FCGUnit.__init__(
            self, model_name=self.single_name, model_price=self.model_price, min_models=10,
            musician_price=5, standard_price=10, champion_name='Veteran', champion_price=10,
            base_gear=['Hand weapon', 'Heavy armour'],
            options=[
                self.gw,
                OptionsList('sh', 'Armour', [['Shield', 1, 'sh']]),
                self.rangers
            ],
            unit_options=[self.runic]
        )

    def check_rules(self):
        self.rangers.set_visible(self.gw.get('gw'))
        self.runic.set_visible(self.has_flag())
        process_base_rune_list(self.runic, lesser_runic_standards_ids)
        FCGUnit.check_rules(self)

    def is_rangers(self):
        return self.rangers.get('rng')


class Crossbowmen(FCGUnit):
    name = 'Quarrellers'

    def __init__(self):
        self.rangers = OptionsList('rang', 'Options', [
            ['Rangers', 1, 'rng'],
        ])
        self.gw = OptionsList('wep', 'Weapon', [['Great weapon', 2, 'gw']])
        FCGUnit.__init__(
            self, model_name='Quarreller', model_price=11, min_models=10,
            musician_price=5, standard_price=10, champion_name='Veteran', champion_price=10,
            base_gear=['Crossbow', 'Hand weapon', 'Light armor'],
            options=[
                self.gw,
                OptionsList('sh', 'Armour', [['Shield', 1, 'sh']]),
                self.rangers
            ],
        )

    def check_rules(self):
        self.rangers.set_visible(self.gw.get('gw'))
        FCGUnit.check_rules(self)

    def is_rangers(self):
        return self.rangers.get('rng')


class Thunderers(FCGUnit):
    name = 'Thunderers'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Thunderer', model_price=14, min_models=10,
            musician_price=5, standard_price=10, champion_name='Veteran', champion_price=10,
            base_gear=['Hand weapon', 'Light armor'], rank_gear=['Dwarf handgun'],
            options=[OptionsList('sh', 'Armour', [['Shield', 1, 'sh']])],
            champion_options=[OneOf('arm', 'Veteran\'s weapon', [
                    ['Dwarf handgun', 0, 'hg'],
                    ['Brace of Pistols', 2, 'bp']
            ])]
        )
