__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, OneOf, OptionsList
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.vampire_counts.armory import vampire_magic_standards, filter_items


class CorpseCart(Unit):
    name = 'Corpse Cart'
    base_points = 90

    class Corpsemaster(Unit):
        name = 'Corpsemaster'

        def __init__(self):
            Unit.__init__(self)
            self.wep = self.opt_options_list('Weapon', [['Spear', 1, 'sp']])

    def __init__(self, has_passenger=False):
        Unit.__init__(self)
        if not has_passenger:
            self.driver = self.opt_sub_unit(self.Corpsemaster())
        self.opt = self.opt_options_list('Options', [
            ['Balefire', 15, 'bf'],
            ['Unholy Lodestone', 30, 'ul']
        ])

    def check_rules(self):
        self.points.set(self.build_points())
        self.build_description()
        self.description.add(ModelDescriptor(name='The Restless Dead', count=1).build())


class GraveGuard(FCGUnit):
    name = 'Grave Guard'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Grave guard', model_price=11, min_models=10,
            musician_price=10, standard_price=10, champion_name='Seneschal', champion_price=10,
            magic_banners=filter_items(vampire_magic_standards, 50),
            base_gear=['Heavy armour', 'Hand weapon'],
            options=[OneOf('wep', 'Weapon', [
                ['Shield', 0, 'sh'],
                ['Great weapon', 1, 'gw']
            ])]
        )


class BlackKnights(FCGUnit):
    name = 'Black Knights'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Black Knight', model_price=21, min_models=5,
            musician_price=10, standard_price=10, champion_name='Hell Knight', champion_price=10,
            magic_banners=filter_items(vampire_magic_standards, 50),
            base_gear=['Skeletal Steed', 'Heavy armour', 'Hand weapon', 'Shield'],
            options=[OptionsList('wep', 'Weapon', [
                ['Barding', 3, 'brf'],
                ['Lance', 2, 'lnc']
            ])]
        )


class CryptHorrors(FCGUnit):
    name = 'Crypt Horrors'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Crypt Horror', model_price=38, min_models=3,
            champion_name='Crypt Haunter', champion_price=10,
        )


class FellBats(FCGUnit):
    name = 'Fell Bats'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Fell Bat', model_price=16, min_models=2)


class BatSwarms(FCGUnit):
    name = 'Bat Swarms'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Bat Swarm', model_price=35, min_models=2)


class SpiritHost(FCGUnit):
    name = 'Spirit Host'

    def __init__(self):
        FCGUnit.__init__(self, model_name='Spirit Host', model_price=45, min_models=1, base_gear=['Hand weapons'])


class Hexwraiths(FCGUnit):
    name = 'Hexwraiths'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Hexwraith', model_price=30, min_models=5, max_model=10,
            champion_name='Hellwraith', champion_price=10,
            base_gear=['Skeletal steed', 'Great weapon']
        )


class Vargheists(FCGUnit):
    name = 'Vargheists'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Vargheist', model_price=46, min_models=3, max_model=10,
            champion_name='Vargoyle', champion_price=10
        )
