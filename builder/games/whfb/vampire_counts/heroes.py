__author__ = 'Ivan Truskov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.vampire_counts.mounts import *
from builder.games.whfb.legacy_armory import *
from builder.games.whfb.vampire_counts.armory import *
from builder.games.whfb.vampire_counts.special import CorpseCart
from builder.core.options import norm_point_limits


class MannfredA(Unit):
    name = 'Mannfred the Acolyte'
    base_points = 200
    gear = ['Heavy armour', 'Level 2 Wizard', 'The Lore of the Vampires', 'Sword of Unholy Power', 'Dark Acolyte']

    def get_unique(self):
        return 'Mannfred'

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [Nightmare(8), AbyssalTerror()])


class Krell(StaticUnit):
    name = 'Krell, Lord of Undeath'
    base_points = 205
    gear = ['The Black Axe of Krell', 'Armour of the Barrows']


class Konrad(StaticUnit):
    name = 'Konrad von Carstein'
    base_points = 160
    gear = ['Hand weapon', 'Heavy armour', 'Sword of Waldenhof', 'Red Fury']


class Izabella(StaticUnit):
    name = 'Izabella von Carstein'
    base_points = 175
    gear = ['Hand weapon', 'Heavy armour', 'Blood Chalice of Bathori', 'Level 1 Wizard', 'The Lore of the Vampires',
            'Beguile']


vampire_armours_heroes = filter_items(vampire_armours, 50)
vampire_magic_weapons_heroes = filter_items(vampire_magic_weapons, 50)
talismans_heroes = filter_items(talismans, 50)
vampire_arcane_items_heroes = filter_items(vampire_arcane_items, 50)
vampire_enchanted_items_heroes = filter_items(vampire_enchanted_items, 50)
vampire_powers_heroes = filter_items(vampire_powers, 50)


class Necromancer(Unit):
    name = 'Necromancer'
    gear = ['Hand weapon']
    base_points = 65

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))

        self.steed = self.opt_optional_sub_unit('Steed', [Nightmare(9), CorpseCart(True)])
        self.opt = self.opt_options_list('Upgrade', [['Master of the Dead', 20, 'motd']])
        self.magic = [
            self.opt_options_list('Magic weapon', vampire_magic_weapons_heroes, 1),
            self.opt_options_list('Magic armour', necromancer_armours, 1),
            self.opt_options_list('Talisman', talismans_heroes, 1),
            self.opt_options_list('Arcane item', vampire_arcane_items_heroes, 1),
            self.opt_options_list('Enchanted item', vampire_enchanted_items_heroes, 1),
        ]
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Death', 0, 'death'],
            ['The Lore of the Vampires', 0, 'vampires'],
        ], limit=1)


    def check_rules(self):
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])

    def is_master(self):
        return self.opt.get('motd')


class Vampire(Unit):
    name = 'Vampire'
    gear = ['Hand weapon']
    base_points = 105

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 3, 'hw'],
            ['Great weapon', 8, 'gw'],
            ['Lance', 8, 'lnc']
        ], limit=1)
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 2, 'sh'],
            ['Heavy armour', 4, 'harm']
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Nightmare(8), Hellsteed(), CovenThrone()])

        self.powers = self.opt_options_list('Vampiric powers', vampire_powers_heroes, points_limit=50)
        self.magwep = self.opt_options_list('Magic weapon', vampire_magic_weapons_heroes, 1)
        self.magarm = self.opt_options_list('Magic armour', vampire_armours_heroes, 1)
        self.tals = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.arcane = self.opt_options_list('Arcane item', vampire_arcane_items_heroes, 1)
        self.ench = self.opt_options_list('Enchanted item', vampire_enchanted_items_heroes, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', vampire_magic_standards, 1)
        self.magic = [self.magwep, self.magarm, self.tals, self.arcane, self.ench]
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Death', 0, 'death'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of the Vampires', 0, 'vampires'],
        ], limit=1)

    def check_rules(self):
        self.wep.set_active_options(['lnc'], self.steed.get_count() > 0)
        self.magban.set_active(self.banner.get('bsb'))
        self.eq.set_active_options(['sh'], not self.magarm.is_selected(magic_shields_ids))
        self.eq.set_active_options(['harm'], not self.magarm.is_selected(magic_armour_suits_ids))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class WightKing(Unit):
    name = 'Wight King'
    base_points = 85

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 3, 'hw'],
            ['Great weapon', 8, 'gw'],
            ['Lance', 8, 'lnc']
        ])
        self.eq = self.opt_options_list('Equipment', [
            ['Shield', 2, 'sh'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [SceletalSteed()])
        self.magwep = self.opt_options_list('Magic weapon', vampire_magic_weapons_heroes, 1)
        self.magarm = self.opt_options_list('Magic armour', vampire_armours_heroes, 1)
        self.tals = self.opt_options_list('Talisman', talismans_heroes, 1)
        self.ench = self.opt_options_list('Enchanted item', vampire_enchanted_items_heroes, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', vampire_magic_standards, 1)
        self.magic = [self.magwep, self.magarm, self.tals, self.ench]

    def check_rules(self):
        self.wep.set_active_options(['lnc'], self.steed.get_count() > 0)
        self.magban.set_active(self.banner.get('bsb'))
        self.eq.set_active_options(['sh'], not self.magarm.is_selected(magic_shields_ids))
        self.gear = ['Hand weapon']
        if not self.magarm.is_selected(magic_armour_suits_ids):
            self.gear = self.gear + ['Heavy armour']
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])

    def is_bearer(self):
        return self.banner.get('bs')


class CairnWraith(Unit):
    name = 'Cairn Wraith'
    base_points = 60
    static = True
    gear = ['Great weapon']


class TombBanshee(Unit):
    name = 'Tomb Banshee'
    static = True
    base_points = 95
    gear = ['Hand weapon']
