__author__ = 'Denis Romanov'
from builder.core.unit import Unit, StaticUnit
from builder.games.whfb.bretonnia.mounts import *
from builder.games.whfb.bretonnia.armory import *
from builder.games.whfb.bretonnia.armory import *
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.bretonnia.special import TiranocChariot, Skycutter
from builder.games.whfb.bretonnia.rare import FrostPhoenix


heroes_weapon = filter_items(bret_weapon, 50)
heroes_armour = filter_items(bret_armour, 50)
heroes_talisman = filter_items(bret_talisman, 50)
heroes_arcane = filter_items(bret_arcane, 50)
heroes_enchanted = filter_items(bret_enchanted, 50)


class Noble(Unit):
    name = 'Noble'
    gear = ['Hand Weapon']
    base_points = 70

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Halberd', 2, 'hb'],
            ['Spear', 2, 'sp'],
            ['Great weapon', 4, 'gw'],
            ['Hand weapon', 2, 'hw'],
            ['Lance', 6, 'ln'],
        ], limit=1)
        self.wep_1 = self.opt_options_list('', [
            ['Long Bow', 5, 'lb'],
        ])
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 2, 'sh'],
            ['Lion cloak', 4, 'lc'],
        ])
        self.arm = self.opt_one_of('', [
            ['Light armour', 0, 'la'],
            ['Heavy armour', 4, 'ha'],
            ['Dragon armour', 10, 'da'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [ElvenSteed(10, 5), GreatEagle(), Griffon(),
                                                          TiranocChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', [['Banner of Avelorn', 40, 'avelorn']] + bret_standard, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.magban.set_active_options(['avelorn'], self.get_roster().has_alarielle())
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(bret_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(bret_armour_suits_ids))
        self.wep.set_visible_options(['hw'], self.steed.get_count() == 0)
        self.wep.set_visible_options(['ln'], self.steed.get_count() > 0)
        self.magic_wep.set_visible_options(['bret_star'], self.steed.get_count() > 0)
        self.magic_enc.set_visible_options(['bret_mor'], self.steed.get_count() == 0)
        self.eq.set_active_options(['lc'], self.arm.get_cur() != 'da')
        self.arm.set_active_options(['da'], not self.eq.get('lc'))
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])


class SeaHelm(Unit):
    name = 'Lothern Sea Helm'
    base_gear = ['Spear']
    base_points = 100

    def __init__(self):
        Unit.__init__(self)
        self.wep_1 = self.opt_options_list('', [
            ['Bow', 4, 'lb'],
        ])
        self.steed = self.opt_optional_sub_unit('Steed', [Skycutter(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]
        self.banner = self.opt_options_list('', [['Battle Standard Bearer', 25, 'bsb']])
        self.magban = self.opt_options_list('Magic Standard', [['Banner of Avelorn', 40, 'avelorn']] + bret_standard, 1)

    def check_rules(self):
        self.magban.set_active_options(['avelorn'], self.get_roster().has_alarielle())
        self.gear = self.base_gear
        if not self.magic_arm.is_selected(bret_shields_ids):
            self.gear = self.gear + ['Shield']
        if not self.magic_arm.is_selected(bret_armour_suits_ids):
            self.gear = self.gear + ['Light armour']
        self.magic_wep.set_visible_options(['bret_star'], self.steed.get_count() > 0)
        self.magic_enc.set_visible_options(['bret_mor'], self.steed.get_count() == 0)
        self.magban.set_active(self.banner.get('bsb'))
        for opt in self.magic:
            opt.set_visible(len(self.magban.get_all()) == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic + [self.magban] if m.get_selected()), [])


class Handmaiden(Unit):
    name = 'Handmaiden of the Everqueen'
    base_gear = ['Spear', 'Bow of Avelorn']
    base_points = 95

    def __init__(self):
        Unit.__init__(self)
        self.horn = self.opt_options_list('', [
            ['Horn of Isha', 50, 'horn'],
        ])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.horn.set_active_options(['horn'], self.get_roster().has_alarielle())
        self.gear = self.base_gear
        if not self.magic_arm.is_selected(bret_armour_suits_ids):
            self.gear = self.gear + ['Light armour']
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Mage(Unit):
    name = 'Mage'
    gear = ['Hand Weapon']
    base_points = 85

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [ElvenSteed(10, 5), TiranocChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of High', 0, 'high'],
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Beasts', 0, 'beast'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Light', 0, 'light'],
            ['The Lore of Life', 0, 'life'],
            ['The Lore of Heavens', 0, 'heavens'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.magic_wep.set_visible_options(['bret_star'], self.steed.get_count() > 0)
        self.magic_enc.set_visible_options(['bret_mor'], self.steed.get_count() == 0)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class DragonMage(Unit):
    name = 'Dragon Mage of Caledor'
    gear = ['Hand Weapon']
    base_points = 350

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(1, 2, 35))
        self.arm = self.opt_options_list('', [
            ['Dragon armour', 10, 'da'],
        ])
        self.magic_wep = self.opt_options_list('Magic weapon', heroes_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', heroes_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', heroes_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', heroes_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', heroes_arcane, 1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc, self.magic_arm]

    def check_rules(self):
        self.arm.set_visible(not self.magic_arm.is_selected(bret_armour_suits_ids))
        self.magic_wep.set_visible_options(['bret_star'], True)
        self.magic_enc.set_visible_options(['bret_mor'], False)
        norm_point_limits(50, self.magic)
        Unit.check_rules(self)
        self.description.add(ModelDescriptor('Sun Dragon').build(1))

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class Korhil(Unit):
    base_points = 150
    name = 'Korhil'
    gear = ['Hand weapon', 'Heavy armour', 'Chayal', 'Pelt of Charandis']

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [TiranocChariot(boss=True)])


class Caradryan(Unit):
    base_points = 170
    name = 'Caradryan'
    gear = ['Heavy armour', 'Phoenix blade']

    class Ashtari(FrostPhoenix):
        name = 'Ashtari'

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [self.Ashtari()])
