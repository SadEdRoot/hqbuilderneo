__author__ = 'Denis Romanov'

from builder.games.whfb.obsolete.dark_elves.mounts import *
from builder.games.whfb.obsolete.dark_elves.armory import *
from builder.games.whfb.obsolete.dark_elves.special import ColdOneChariot
from builder.core.options import norm_point_limits
from builder.core.model_descriptor import ModelDescriptor


class Malekith(Unit):
    name = 'Malekith, the Witch King'
    base_points = 600
    gear = ['Level 4 Wizard', 'Lore of Dark Magic', 'Destroyer', 'Armour of Midnight', 'Spellshield', 'Circlet of Iron']
    unique = True

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [ColdOne(), BlackDragon(), ColdOneChariot(malekith=True)])


class Morathi(Unit):
    name = 'Morathi, the Hag Sorceress'
    base_points = 455
    gear = ['Level 4 Wizard', 'Lore of Dark Magic', 'Hand weapon']
    unique = True

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_one_of('Weapon', [
            ['Heartrender', 45, 'hr'],
            ['Darksword', 25, 'hr'],
        ])
        self.magic_enc = self.opt_options_list('Enchanted item', de_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', de_arcane, 1)

    def check_rules(self):
        Unit.check_rules(self)
        self.description.add(ModelDescriptor(name='Sulephet', count=1).build())


class Crone(Unit):
    name = 'Crone Hellebron, the Hag Queen'
    base_points = 350
    gear = ['Deathsword', 'Parrying Blade', 'Amulet of Fire', 'Rune of Khaine', 'Witchbrew', 'Cry of War']
    unique = True

    def __init__(self):
        Unit.__init__(self)
        self.steed = self.opt_optional_sub_unit('Steed', [Manticore()])


class Dreadlord(Unit):
    name = 'Dreadlord'
    gear = ['Hand Weapon']
    base_points = 140

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Halberd', 6, 'hb'],
            ['Great weapon', 6, 'gw'],
            ['Hand weapon', 6, 'hw'],
            ['Lance', 6, 'ln'],
        ], limit=1)
        self.wep_1 = self.opt_options_list('', [
            ['Repeater crossbow', 10, 'rc'],
            ['Repeater handbow', 8, 'rh'],
            ['Pair of repeater handbows', 16, 'prh'],
        ])
        self.eq = self.opt_options_list('Armour', [
            ['Shield', 3, 'sh'],
            ['Sea Dragon cloak', 6, 'lc'],
        ])
        self.arm = self.opt_options_list('', [
            ['Light armour', 3, 'la'],
            ['Heavy armour', 6, 'ha'],
        ], limit=1)
        self.steed = self.opt_optional_sub_unit('Steed', [Steed(), ColdOne(), DarkPegasus(), Manticore(), BlackDragon(),
                                                          ColdOneChariot(boss=True)])
        self.magic_wep = self.opt_options_list('Magic weapon', de_weapon, 1)
        self.magic_arm = self.opt_options_list('Magic armour', de_armour, 1)
        self.magic_tal = self.opt_options_list('Talisman', de_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', de_enchanted, 1)
        self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

    def check_rules(self):
        self.eq.set_active_options(['sh'], not self.magic_arm.is_selected(de_shields_ids))
        self.arm.set_visible(not self.magic_arm.is_selected(de_armour_suits_ids))
        self.magic_wep.set_visible_options(['de_ex'], self.steed.get_count() == 0)
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])


class SupremeSorceress(Unit):
    name = 'Supreme Sorceress'
    gear = ['Hand Weapon']
    base_points = 225

    def __init__(self):
        Unit.__init__(self)
        self.mage = self.opt_one_of('Magic level', build_magic_levels(3, 2, 35))
        self.steed = self.opt_optional_sub_unit('Steed', [Steed(), ColdOne(), DarkPegasus(), Manticore(),
                                                          BlackDragon()])
        self.magic_wep = self.opt_options_list('Magic weapon', de_weapon, 1)
        self.magic_tal = self.opt_options_list('Talisman', de_talisman, 1)
        self.magic_enc = self.opt_options_list('Enchanted item', de_enchanted, 1)
        self.magic_arc = self.opt_options_list('Arcane item', de_arcane, 1)
        self.lores = self.opt_options_list('Lores', [
            ['The Lore of Dark Magic', 0, 'dark'],
            ['The Lore of Fire', 0, 'fire'],
            ['The Lore of Metal', 0, 'metal'],
            ['The Lore of Shadow', 0, 'shadow'],
            ['The Lore of Death', 0, 'death'],
        ], limit=1)
        self.magic = [self.magic_wep, self.magic_arc, self.magic_tal, self.magic_enc]

    def check_rules(self):
        norm_point_limits(100, self.magic)
        Unit.check_rules(self)

    def get_unique_gear(self):
        return sum((m.get_selected() for m in self.magic), [])
