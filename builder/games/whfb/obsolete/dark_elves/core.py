__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit, Unit
from builder.core.unit import OptionsList, OneOf, OptionalSubUnit
from builder.games.whfb.obsolete.dark_elves.armory import de_standard, assassin_gifts, filter_items

unit_standards = filter_items(de_standard, 25)


class Assassin(Unit):
    name = 'Assassin'
    base_points = 90
    gear = ['Hand weapon']

    def __init__(self):
        Unit.__init__(self)
        self.wep = self.opt_options_list('Weapon', [
            ['Hand weapon', 6, 'hw'],
            ['Repeater crossbow', 10, 'rc'],
        ])
        self.gifts = self.opt_options_list('Gifts', assassin_gifts, points_limit=75)

    def get_unique_gear(self):
        if self.gifts.get('de_ven'):
            return ['Venom sword']
        if self.gifts.get('de_ct'):
            return ['Cloak of Twilight']
        return []


class AssassinUnit(FCGUnit):

    def __init__(self, *args, **kwargs):
        self.hidden = OptionalSubUnit('as', 'Hidden', [Assassin()])
        if 'unit_options' in kwargs:
            kwargs['unit_options'].append(self.hidden)
        else:
            kwargs['unit_options'] = [self.hidden]
        FCGUnit.__init__(self, *args, **kwargs)

    def get_unique_gear(self):
        gear = FCGUnit.get_unique_gear(self)
        gear = [] if gear is None else gear
        su = self.hidden.get_unit()
        if su:
            gear += su.get_unit().get_unique_gear()
        return gear

    def get_count(self):
        return FCGUnit.get_count(self) + self.hidden.get_count()


class Warriors(AssassinUnit):
    name = 'Dark Elf Warriors'

    def __init__(self):
        AssassinUnit.__init__(
            self, model_name='Dark Elf Warrior', model_price=6, min_models=10,
            musician_price=3, standard_price=6, champion_name='Lording', champion_price=6,
            base_gear=['Hand weapon', 'Spear', 'Light armour'],
            magic_banners=unit_standards,
            options=[
                OptionsList('arm', 'Options', [['Shield', 1, 'sh']])
            ],
        )


class Crossbowmen(AssassinUnit):
    name = 'Dark Elf Repeater Crossbowmen'

    def __init__(self):
        AssassinUnit.__init__(
            self, model_name='Dark Elf Crossbowman', model_price=10, min_models=10,
            musician_price=5, standard_price=10, champion_name='Guardmaster', champion_price=5,
            base_gear=['Hand weapon', 'Repeater Crossbow', 'Light armour'],
            options=[
                OptionsList('arm', 'Options', [['Shield', 1, 'sh']])
            ]
        )


class Corsairs(AssassinUnit):
    name = 'Black Ark Corsairs'

    def __init__(self):
        AssassinUnit.__init__(
            self, model_name='Corsair', model_price=10, min_models=10,
            musician_price=5, standard_price=10, champion_name='Reaver', champion_price=10,
            base_gear=['Hand weapon', 'Sea Dragon Cloak', 'Light armour'],
            options=[
                OneOf('arm', 'Weapon', [
                    ['Hand weapon', 0, 'hw'],
                    ['Handbow', 0, 'hb'],
                ])
            ],
            champion_options=[
                OptionsList('ch_opt', 'Reaver\'s weapon', [['Pair of handbows', 3, 'hb']])
            ],
            magic_banners=[['Sea Serpent Standard', 25, 'de_sea']] + unit_standards,
        )


class DarkRaiders(FCGUnit):
    name = 'Dark Raiders'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Dark Rider', model_price=17, min_models=5,
            musician_price=7, standard_price=14, champion_name='Reaver', champion_price=14,
            base_gear=['Hand weapon', 'Spear', 'Light armour'],
            options=[
                OptionsList('arm', 'Armour', [['Shield', 1, 'sh']]),
                OptionsList('wep', 'Weapon', [['Repeater crossbow', 5, 'cb']])
            ]
        )


class Harpies(FCGUnit):
    name = 'Harpies'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Harpy', model_price=11, min_models=5, max_model=10,
            base_gear=['Vicious claws'],
        )


#
#
# class Archers(FCGUnit):
#     name = 'Archers'
#
#     def __init__(self):
#         FCGUnit.__init__(
#             self, model_name='Archer', model_price=10, min_models=10,
#             musician_price=10, standard_price=10, champion_name='Hawkeye', champion_price=10,
#             base_gear=['Hand weapon', 'Long bow'],
#             options=[
#                 OptionsList('arm', '', [['Light armour', 1, 'la']])
#             ]
#         )
#
#
# class SeaGuard(FCGUnit):
#     name = 'Lothern Sea Guard'
#
#     def __init__(self):
#         FCGUnit.__init__(
#             self, model_name='Sea Guard', model_price=11, min_models=10,
#             musician_price=10, standard_price=10, champion_name='Sea Master', champion_price=10,
#             base_gear=['Spear', 'Bow', 'Light armour'],
#             options=[
#                 OptionsList('arm', '', [['Shield', 1, 'sh']])
#             ]
#         )
#
#
# class SilverHelm(FCGUnit):
#     name = 'Silver Helms'
#
#     def __init__(self):
#         FCGUnit.__init__(
#             self, model_name='Silver Helm', model_price=21, min_models=10,
#             musician_price=10, standard_price=10, champion_name='High Helm', champion_price=10,
#             base_gear=['Hand weapon', 'Lance', 'Heavy armour', 'Ithilmar barding'],
#             options=[
#                 OptionsList('arm', '', [['Shield', 2, 'sh']])
#             ]
#         )
#
#
# class Reavers(FCGUnit):
#     name = 'Ellyrian Reavers'
#
#     def __init__(self):
#         self.wep = OptionsList('wep', 'Weapon', [
#             ['Spear', 0, 'sp'],
#             ['Bow', 1, 'bow1'],
#             ['Bow', 3, 'bow3'],
#         ])
#         FCGUnit.__init__(
#             self, model_name='Ellyrian Reaver', model_price=16, min_models=10,
#             musician_price=10, standard_price=10, champion_name='Harbringer', champion_price=10,
#             base_gear=['Hand weapon', 'Light armour'],
#             options=[self.wep]
#         )
#
#     def check_rules(self):
#         self.wep.set_visible_options(['bow3'], self.wep.get('sp'))
#         self.wep.set_visible_options(['bow1'], not self.wep.get('sp'))
#         FCGUnit.check_rules(self)
