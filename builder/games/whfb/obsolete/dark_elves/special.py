__author__ = 'Denis Romanov'
from builder.core.unit import Unit, OptionsList, OptionalSubUnit
from builder.core.model_descriptor import ModelDescriptor
from builder.games.whfb.unit import FCGUnit
from builder.games.whfb.obsolete.dark_elves.armory import *
from builder.core.options import norm_point_limits
from builder.games.whfb.obsolete.dark_elves.core import AssassinUnit
from builder.games.whfb.obsolete.dark_elves.armory import de_standard, filter_items

lesser_unit_standards = filter_items(de_standard, 25)
unit_standards = filter_items(de_standard, 50)

champion_weapon = filter_items(de_weapon, 25)
champion_armour = filter_items(de_armour, 25)
champion_talisman = filter_items(de_talisman, 25)
champion_arcane = filter_items(de_arcane, 25)
champion_enchanted = filter_items(de_enchanted, 25)
hag_gear = filter_items(hag_gifts, 25)


class WitchElves(AssassinUnit):
    name = 'Witch Elves'

    def __init__(self):
        AssassinUnit.__init__(
            self, model_name='Witch Elf', model_price=10, min_models=5,
            musician_price=5, standard_price=10, champion_price=10, champion_name='Hag',
            base_gear=['Hand weapon' for _ in range(2)],
            magic_banners=lesser_unit_standards,
            champion_gear=hag_gear
        )


class Shades(AssassinUnit):
    name = 'Shades'

    def __init__(self):
        AssassinUnit.__init__(
            self, model_name='Shade', model_price=16, min_models=5,
            champion_price=18, champion_name='Bloodshade',
            base_gear=['Hand weapon', 'Repeater crossbow'],
            options=[
                OptionsList('arm', 'Options', [['Light armour', 1, 'la']]),
                OptionsList('wep', '', [
                    ['Great weapon', 2, 'gw'],
                    ['Hand weapon', 1, 'hw'],
                ], limit=1)
            ]
        )


class Executioners(AssassinUnit):
    name = 'Har Ganeth Executioners'

    class Master(Unit):
        name = 'Driach-master'
        base_points = 24
        gear = ['Hand weapon', 'Great weapon', 'Heavy armour']
        static = True

    class Tullaris(Unit):
        name = 'Tullaris of Har Ganeth'
        base_points = 95
        gear = ['The Blade of Har Ganeth', 'Heavy armour']
        static = True

    def __init__(self):
        self.master = OptionalSubUnit('camp', 'Champion', [self.Master(), self.Tullaris()])
        AssassinUnit.__init__(
            self, model_name='Executioner', model_price=12, min_models=5,
            musician_price=6, standard_price=12,
            base_gear=['Hand weapon', 'Great weapon', 'Heavy armour'],
            magic_banners=lesser_unit_standards,
            champion_units=self.master
        )


class ColdOneKnights(FCGUnit):
    name = 'Cold One Knights'

    class Master(Unit):
        name = 'Dread Knight'
        base_points = 27 + 16
        gear = ['Hand weapon', 'Lance']

        def __init__(self):
            Unit.__init__(self)
            self.magic_wep = self.opt_options_list('Magic weapon', champion_weapon, 1)
            self.magic_arm = self.opt_options_list('Magic armour', champion_armour, 1)
            self.magic_tal = self.opt_options_list('Talisman', champion_talisman, 1)
            self.magic_enc = self.opt_options_list('Enchanted item', champion_enchanted, 1)
            self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

        def check_rules(self):
            if not self.magic_arm.is_selected(de_shields_ids):
                self.gear = self.gear + ['Shield']
            if not self.magic_arm.is_selected(de_armour_suits_ids):
                self.gear = self.gear + ['Heavy armour']
            norm_point_limits(25, self.magic)
            Unit.check_rules(self)

    def __init__(self):
        self.master = OptionalSubUnit('camp', 'Champion', [self.Master()])
        FCGUnit.__init__(
            self, model_name='Cold One Knight', model_price=27, min_models=5,
            musician_price=8, standard_price=16,
            base_gear=['Hand weapon', 'Lance', 'Heavy armour', 'Shield'],
            magic_banners=unit_standards,
            champion_units=self.master
        )


class BlackGuard(AssassinUnit):
    name = 'Black Guard of Naggarond'

    class Master(Unit):
        name = 'Tower master'
        base_points = 27
        gear = ['Hand weapon', 'Halberd']

        def __init__(self):
            Unit.__init__(self)
            self.magic_wep = self.opt_options_list('Magic weapon', champion_weapon, 1)
            self.magic_arm = self.opt_options_list('Magic armour', champion_armour, 1)
            self.magic_tal = self.opt_options_list('Talisman', champion_talisman, 1)
            self.magic_enc = self.opt_options_list('Enchanted item', champion_enchanted, 1)
            self.magic = [self.magic_wep, self.magic_arm, self.magic_tal, self.magic_enc]

        def check_rules(self):
            if not self.magic_arm.is_selected(de_armour_suits_ids):
                self.gear = self.gear + ['Heavy armour']
            norm_point_limits(25, self.magic)
            Unit.check_rules(self)

    class Kouran(Unit):
        name = 'Kouran of the Black Guard'
        base_points = 75
        gear = ['The Crimson Death', 'The Armour of Grief']
        static = True

    def __init__(self):
        self.master = OptionalSubUnit('camp', 'Champion', [self.Master(), self.Kouran()])
        AssassinUnit.__init__(
            self, model_name='Black Guard', model_price=13, min_models=5, max_model=20,
            musician_price=7, standard_price=14,
            base_gear=['Hand weapon', 'Halberd', 'Heavy armour'],
            magic_banners=unit_standards,
            champion_units=self.master
        )


class ColdOneChariot(Unit):
    name = 'Cold One Chariot'
    base_points = 100
    static = True

    def __init__(self, boss=False, malekith=False):
        self.boss = boss or malekith
        if boss:
            self.base_points = 90
        elif malekith:
            self.base_points = 110
            self.name = 'The Black Chariot'
        Unit.__init__(self)

    def check_rules(self):
        self.set_points(self.build_points())
        self.build_description(options=[])
        self.description.add(ModelDescriptor(name='Charioteer', count=1 if self.boss else 2,
                                             gear=['Spear', 'Hand weapon', 'Repeater crossbow']).build())
        self.description.add(ModelDescriptor(name='Cold One', count=2).build())
