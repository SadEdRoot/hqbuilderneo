__author__ = 'Denis Romanov'


from builder.core.unit import Unit
from builder.core.model_descriptor import ModelDescriptor
from .special import Stegadon


class AncientStegadon(Stegadon):
    name = 'Ancient Stegadon'
    gear = ['Giant Blowpipe', 'Giant Blowpipe']
    base_points = 275

    def __init__(self, boss=False, priest=False):
        Stegadon.__init__(self, boss=boss)
        if priest:
            self.gear = ['Engine of the Gods']
            self.base_points = 290


class SalamanderPack(Unit):
    name = 'Salamander Hunting Pack'

    def __init__(self, model_name='Salamander'):
        Unit.__init__(self)
        self.model_name = model_name
        self.salamander = self.opt_count(self.model_name, 1, 3, 75)
        self.handler = self.opt_count('Skink Handler', 0, 1, 5)

    def check_rules(self):
        self.handler.update_range(0, self.salamander.get())
        self.set_points(self.build_points(count=1))
        self.build_description(options=[])
        self.description.add(ModelDescriptor(self.model_name, points=60, count=self.salamander.get()).build())
        self.description.add(ModelDescriptor('Skink Handler', points=5, gear=['Hand weapon'],
                                             count=self.salamander.get() * 3 + self.handler.get()).build())


class RazordonPack(SalamanderPack):
    name = 'Barbed Razordon Pack'

    def __init__(self):
        SalamanderPack.__init__(self, 'Barbed Razordon')
