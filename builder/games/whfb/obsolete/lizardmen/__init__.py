__author__ = 'Denis Romanov'
from builder.games.whfb.legacy_roster import LegacyFantasy
from .heroes import *
from .lords import *
from .core import *
from .special import *
from .rare import *


class Lizardmen(LegacyFantasy):
    obsolete = True
    army_name = 'Lizardmen (2009)'
    army_id = '391ba48cd8ac476b8e554846cf2698ff'

    def __init__(self):
        LegacyFantasy.__init__(
            self,
            lords=[Kroak, Mazdamundi, KroqGar, Tehenhauin, Slann, Oldblood],
            heroes=[Chakax, GorRok, TettoEko, TictaqTo, Oxiotl, Scar, Chief, Priest],
            core=[SaurusWarriors, Skinks, SkinksSkirmishers, Swarm],
            special=[ChameleonSkins, TerradonRaiders, TempleGuard, ColdOnes, Kroxigor, Stegadon],
            rare=[AncientStegadon, SalamanderPack, RazordonPack]
        )
