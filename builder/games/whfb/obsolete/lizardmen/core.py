__author__ = 'Denis Romanov'

from builder.games.whfb.unit import FCGUnit, Unit
from builder.core.unit import OptionsList
from builder.core.model_descriptor import ModelDescriptor


class SaurusWarriors(FCGUnit):
    name = 'Saurus Warriors'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Saurus Warrior', model_price=11, min_models=10,
            musician_price=6, standard_price=12, champion_name='Saurus Champion', champion_price=12,
            base_gear=['Shield', 'Hand weapon'],
            options=[OptionsList('wep', 'Weapon', [
                ['Spear', 1, 'sp'],
            ], limit=1)]
        )


class Skinks(FCGUnit):
    name = 'Skinks'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Skink', model_price=5, min_models=10,
            musician_price=6, standard_price=8, champion_name='Skink Brave', champion_price=8,
            base_gear=['Shield', 'Hand weapon', 'Javelin']
        )
        self.krox = self.opt_count('Kroxigor', 0, 1, 55)

    def check_rules(self):
        self.krox.update_range(0, int(self.get_count() / 8))
        FCGUnit.check_rules(self)
        self.description.add(ModelDescriptor('Kroxigor', points=55, gear=['Great weapon']).build(self.krox.get()))


class SkinksSkirmishers(Unit):
    name = 'Skinks Skirmishers'

    def __init__(self):
        Unit.__init__(self)
        self.count = self.opt_count('Skinks Skirmisher', 10, 20, 7)
        self.wep = self.opt_one_of('Weapon', [
            ['Blowpipe', 0, 'bp'],
            ['Javelin and shield', 1, 'js'],
        ])
        self.brave = self.opt_options_list('Skink Brave', [['Skink Brave', 6, 'sb']])

    def check_rules(self):
        self.set_points(self.build_points(exclude=[self.wep.id], count=1) + self.wep.points() * self.get_count())
        self.build_description(options=[])
        sk = ModelDescriptor('Skink', points=7, gear=['Hand weapon'])
        if self.wep.get_cur() == 'bp':
            sk.add_gear('Blowpipe')
        else:
            sk.add_gear('Javelin').add_gear('Shield', 1)

        count = self.count.get()
        if self.brave.get('sb'):
            self.description.add(sk.clone().set_name('Skink Brave').add_points(6).build(1))
            count -= 1
        self.description.add(sk.build(count))


class Swarm(FCGUnit):
    name = 'Jungle Swarm'

    def __init__(self):
        FCGUnit.__init__(
            self, model_name='Jungle Swarm', model_price=45, min_models=1, max_model=6,
            base_gear=['Teeth and benom!']
        )
