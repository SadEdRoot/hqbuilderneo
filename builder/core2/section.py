from .node import Node, Observable, IdGenerator
from itertools import chain
from functools import reduce

__author__ = 'Denis Romanov'


class UnitType(Node):

    def __init__(self, parent, unit_class, slot=1, min_limit=-1,
                 max_limit=-1, active=True, visible=True, group=None,
                 order=-1):
        super(UnitType, self).__init__(parent=parent, node_id=unit_class.type_id, active=active, visible=visible)
        self.unit_class = unit_class
        self.min_limit = min_limit
        self.max_limit = max_limit
        self.name = self.unit_class.type_name
        self.slot = slot
        self.group = group
        self.order = order
        self.parent.append_unit_type(self)

    def dump(self):
        res = super(UnitType, self).dump()
        res.update({
            'name': self.unit_class.type_name,
            'min_limit': self.min_limit,
            'max_limit': self.max_limit,
            'tag': self.group
        })
        return res

    @property
    def units(self):
        return self.parent.get_units(self.unit_class.type_id)

    @property
    def count(self):
        return self.parent.count_units(self.unit_class.type_id)

    @property
    def error(self):
        if self.max_limit >= 0 and self.count > self.max_limit:
            return ["Number of {0} must be at most {1}".format(self.name, self.max_limit)]
        if self.min_limit >= 0 and self.count < self.min_limit:
            return ["Number of {0} must be at least {1}".format(self.name, self.min_limit)]
        return []


class Section(Node):
    def __init__(self, parent, sec_id, name, min_limit, max_limit):
        super(Section, self).__init__(parent=parent, node_id=sec_id)
        self.name = name
        self._min = Observable(min_limit)
        self._max = Observable(max_limit)
        self._types = Observable([])
        self._type_lookup = dict()
        self._units = Observable([])
        self._added_units = Observable([])
        self._deleted_units = Observable([])
        self.errors = []

    @property
    def types(self):
        return self._types.value

    @property
    def min(self):
        return self._min.value

    @min.setter
    def min(self, value):
        self._min.value = value

    @property
    def max(self):
        return self._max.value

    @max.setter
    def max(self, value):
        self._max.value = value

    @property
    def units(self):
        return self._units.value

    @units.setter
    def units(self, value):
        self._units.value = value

    @property
    def deleted_units(self):
        return self._deleted_units.value

    @deleted_units.setter
    def deleted_units(self, value):
        self._deleted_units.value = value

    @property
    def added_units(self):
        return self._added_units.value

    @added_units.setter
    def added_units(self, value):
        self._added_units.value = value

    def append_unit_type(self, unit_type):
        if unit_type.order < 0:
            unit_type.order = len(self.types)
        self.types.append(unit_type)
        self._type_lookup[unit_type.id] = unit_type

    def remove_unit_class(self, unit_class):
        cand = [uc for uc in self.types if issubclass(uc.unit_class, unit_class)]
        if cand:
            del self._type_lookup[cand[0].id]
            self.types.remove(cand[0])

    def get_insert_position(self, order, units):
        if not units:
            return 0
        if order < 0:
            return 1 + len(units)
        for idx, unit in enumerate(units):
            # we should not have units without unit types in lookup table!
            if self._type_lookup[unit.type_id].order > order:
                return idx
        else:
            return 1 + idx

    def add_units(self, data):
        added = []
        for add_data in data:
            unit_type = self._type_lookup.get(add_data['id'], None)
            if unit_type:
                unit = unit_type.unit_class(parent=self)
                unit.check_rules_chain()
                idx = self.get_insert_position(unit_type.order, self.units)
                self.units.insert(idx, unit)
                added.append(dict(unit.dump(), **{'pos': idx}))
        self.added_units = added

    def delete_units(self, data):
        to_del = []
        for del_data in data:
            for unit in self.units:
                if unit.id == del_data['id']:
                    to_del.append(unit)
        for unit in to_del:
            self.units.remove(unit)
        self.deleted_units = [{'id': unit.id} for unit in to_del]

    def clone_unit(self, data):
        for unit in self.units:
            if unit.id != data['id']:
                continue
            new_unit = unit.clone()
            new_unit.check_rules_chain()
            idx = self.get_insert_position(self._type_lookup[unit.type_id].order, self.units)
            self.units.insert(idx, new_unit)
            self.added_units = [dict(new_unit.dump(), **{'pos': idx})]
            return

    def cut_unit(self, unit_id):
        for unit in self.units:
            if unit.id == unit_id:
                self.units.remove(unit)
                self.deleted_units = self.deleted_units + [{'id': unit.id}]
                return unit

    def paste_unit(self, unit):
        idx = self.get_insert_position(self._type_lookup[unit.type_id].order, self.units)
        self.units.insert(idx, unit)
        self.added_units = [dict(unit.dump(), **{'pos': idx})]

    def move_units(self, units):
        added = self.added_units
        for unit in units:
            if unit:
                self.move_unit(unit)
                added += self.added_units
        self.added_units = added

    def move_unit(self, unit):
        self.paste_unit(unit.parent.cut_unit(unit.id))

    def clear_sections(self):
        self.deleted_units = [{'id': unit.id} for unit in self.units]
        del self.units[:]

    def update_units(self, data):
        for unit_data in data:
            for unit in self.units:
                if unit.id == unit_data['id']:
                    unit.update(unit_data)

    @property
    def points(self):
        return reduce(lambda val, u: val + u.points, self.units, 0)

    def update(self, data):
        min_old = self.min
        max_old = self.max
        self.min = data.get('min', min_old)
        self.max = data.get('max', max_old)
        if self.min != min_old or self.max != max_old and self.roster:
            self.roster.limits_updated(self)

        if 'added_units' in list(data.keys()):
            self.add_units(data['added_units'])
        if 'deleted_units' in list(data.keys()):
            self.delete_units(data['deleted_units'])
        if 'units' in list(data.keys()):
            self.update_units(data['units'])
        if 'clone_unit' in list(data.keys()):
            self.clone_unit(data['clone_unit'])
        if 'clear' in list(data.keys()):
            self.clear_sections()

    def dump(self):
        res = super(Section, self).dump()
        res.update({
            'name': self.name,
            'min': self.min,
            'max': self.max,
            'types': [t.dump() for t in self.types],
            'units': [u.dump() for u in self.units]
        })
        return res

    def save(self):
        res = super(Section, self).save()
        res.update({
            'min': self.min,
            'max': self.max,
            'units': [u.save() for u in self.units]
        })
        return res

    def load(self, data):
        super(Section, self).load(data)
        # clear default
        units = []
        self.min = data['min']
        self.max = data['max']
        for unit_data in data.get('units', []):
            ut = self._type_lookup.get(unit_data['type'], None)
            if not ut:
                return
            unit_class = ut.unit_class
            unit = unit_class(parent=self)
            unit.load(unit_data)
            idx = self.get_insert_position(ut.order, units)
            units.insert(idx, unit)
        self.units = units
        # if len(self.units) == 0:
        #     for ut in self.types:
        #         if ut.min_limit > 0:
        #             self.units.extend([ut.unit_class(parent=self)] * (ut.min_limit - self.count_units(ut)))

    def count_slots(self):
        return sum(float(t.count) * float(t.slot) for t in self.types)

    def check_limits(self):
        res = True
        if self.min is not None:
            res = res and float(self.min) <=  self.count_slots()
        if self.max is not None:
            res = res and self.count_slots() <= float(self.max)
        return res

    def set_limits(self, limit_min, limit_max):
        self.min = limit_min
        self.max = limit_max

    def get_limits(self):
        return self.min, self.max

    def get_limits_error(self):
        if self.min is None and self.max is None:
            return
        if not self.check_limits():
            return 'Units number in section {0} must be from {1} to {2}'.format(self.name, self.min, self.max if self.max is not None else 'infinity')

    def get_errors(self):
        return list(chain(
            (unit.errors for unit in self.units),
            [self.get_limits_error()],
            self.errors))

    def error(self, message):
        if not isinstance(message, (list, tuple)):
            message = [message]
        self.errors = self.errors + [e for e in message if e]

    def get_units(self, unit_type):
        return [unit for unit in self.units if unit.type_id == unit_type]

    def count_units(self, unit_type):
        return sum(1 for unit in self.units if unit.type_id == unit_type)

    def check_rules_chain(self):
        self.errors = []
        super(Section, self).check_rules_chain()
        self.check_rules()

    def check_rules(self):
        pass


class Formation(Node):
    army_id = None
    army_name = None

    class Restriction:
        def __init__(self, unit_types, total_min, total_max):
            self.unit_types = unit_types
            self.total_min = total_min
            self.total_max = total_max

        def get_errors(self):
            current = sum([ut.count for ut in self.unit_types])
            if current < self.total_min and self.total_min > 0:
                return ['Must include no less than {} of {}. Taken: {}'.format(
                    self.total_min,
                    ' or '.join([ut.name for ut in self.unit_types]),
                    current)]
            if current > self.total_max and self.total_max > 0:
                return ['Must include no more than {} of {}. Taken: {}'.format(
                    self.total_max,
                    ' or '.join([ut.name for ut in self.unit_types]),
                    current)]
            return []

    def __init__(self, parent=None):
        super(Formation, self).__init__(parent=parent)
        self._name = Observable(self.army_name)
        self._errors = Observable([])
        self._options = Observable([])
        self._types = Observable([])
        self._type_lookup = dict()
        self._units = Observable([])
        self._added_units = Observable([])
        self._deleted_units = Observable([])
        self._restrictions = []
        self.id_generator = IdGenerator()

    def _build_unique_map(self, names_getter):
        unique_map = {}
        for unit in self.units:
            un = names_getter(unit)
            if un is None:
                continue
            if not isinstance(un, (list, type)):
                un = [un]
            for name in un:
                if name in list(unique_map.keys()):
                    unique_map[name] += 1
                else:
                    unique_map[name] = 1
        return unique_map

    def add_type_restriction(self, unit_types, total_min, total_max):
        self._restrictions.append(self.Restriction(unit_types, total_min, total_max))

    @property
    def name(self):
        return self._name.value

    @name.setter
    def name(self, val):
        self._name.value = val

    @property
    def options(self):
        return self._options.value

    @options.setter
    def options(self, value):
        self._options.value = value


    def append_option(self, option):
        if option.id is None:
            option.id = next(self.id_generator)
        self.options.append(option)
        return option

    @property
    def types(self):
        return self._types.value

    @property
    def units(self):
        return self._units.value

    @property
    def errors(self):
        return self._errors.value

    @errors.setter
    def errors(self, val):
        self._errors.value = val

    @units.setter
    def units(self, value):
        self._units.value = value

    @property
    def deleted_units(self):
        return self._deleted_units.value

    @deleted_units.setter
    def deleted_units(self, value):
        self._deleted_units.value = value

    @property
    def added_units(self):
        return self._added_units.value

    @added_units.setter
    def added_units(self, value):
        self._added_units.value = value

    def append_unit_type(self, unit_type):
        if unit_type.order < 0:
            unit_type.order = len(self.types)
        self.types.append(unit_type)
        self._type_lookup[unit_type.id] = unit_type
        #added for formations support
        if unit_type.min_limit > 0:
            self.units.extend([unit_type.unit_class(parent=self) for i in range(unit_type.min_limit)])

    def get_insert_position(self, order, units):
        if not units:
            return 0
        if order < 0:
            return 1 + len(units)
        for idx, unit in enumerate(units):
            # we should not have units without unit types in lookup table!
            if self._type_lookup[unit.type_id].order > order:
                return idx
        else:
            return 1 + idx

    def add_units(self, data):
        added = []
        for add_data in data:
            unit_type = self._type_lookup.get(add_data['id'], None)
            if unit_type:
                unit = unit_type.unit_class(parent=self)
                unit.check_rules_chain()
                idx = self.get_insert_position(unit_type.order, self.units)
                self.units.insert(idx, unit)
                added.append(dict(unit.dump(), **{'pos': idx}))
        self.added_units = added

    def delete_units(self, data):
        to_del = []
        for del_data in data:
            for unit in self.units:
                if unit.id == del_data['id']:
                    to_del.append(unit)
        for unit in to_del:
            self.units.remove(unit)
        self.deleted_units = [{'id': unit.id} for unit in to_del]

    def clone_unit(self, data):
        for unit in self.units:
            if unit.id != data['id']:
                continue
            new_unit = unit.clone()
            new_unit.check_rules_chain()
            idx = self.get_insert_position(self._type_lookup[unit.type_id].order, self.units)
            self.units.insert(idx, new_unit)
            self.added_units = [dict(new_unit.dump(), **{'pos': idx})]
            return

    def cut_unit(self, unit_id):
        for unit in self.units:
            if unit.id == unit_id:
                self.units.remove(unit)
                self.deleted_units = self.deleted_units + [{'id': unit.id}]
                return unit

    def paste_unit(self, unit):
        idx = self.get_insert_position(self._type_lookup[unit.type_id].order, self.units)
        self.units.insert(idx, unit)
        self.added_units = [dict(unit.dump(), **{'pos': idx})]

    def move_units(self, units):
        for unit in units:
            self.move_unit(unit)

    def move_unit(self, unit):
        self.paste_unit(unit.parent.cut_unit(unit.id))

    def clear_sections(self):
        self.deleted_units = [{'id': unit.id} for unit in self.units]
        del self.units[:]

    def update_units(self, data):
        for unit_data in data:
            for unit in self.units:
                if unit.id == unit_data['id']:
                    unit.update(unit_data)

    @property
    def points(self):
        return reduce(lambda val, u: val + u.points, self.units, 0)

    def update(self, data):
        # import pdb; pdb.set_trace()
        for option_data in data.get('options', []):
            for option in self.options:
                if option.id == option_data['id']:
                    option.update(option_data)
        if 'added_units' in list(data.keys()):
            self.add_units(data['added_units'])
        if 'deleted_units' in list(data.keys()):
            self.delete_units(data['deleted_units'])
        if 'units' in list(data.keys()):
            self.update_units(data['units'])
        if 'clone_unit' in list(data.keys()):
            self.clone_unit(data['clone_unit'])
        if 'clear' in list(data.keys()):
            self.clear_sections()

    def get_options_description_dump(self):
        result = []
        for o in self.options:
            if o.used:
                result.extend(g.dump() for g in o.description)
        return result

    @property
    def delta(self):
        res = super(Formation, self).delta
        if 'options' in res:
            res.update({
                'options_description': self.get_options_description_dump(),
            })
        return res

    def dump(self):
        res = super(Formation, self).dump()
        # import pdb; pdb.set_trace()
        res.update({
            'name': self.name,
            'types': [t.dump() for t in self.types],
            'units': [u.dump() for u in self.units],
            'options': [o.dump() for o in self.options],
            'options_description': self.get_options_description_dump(),
            'errors': self.errors
        })
        return res

    def save(self):
        res = super(Formation, self).save()
        res.update({
            'units': [u.save() for u in self.units],
            'options': [o.save() for o in self.options]
        })
        return res

    def load(self, data):
        super(Formation, self).load(data)
        # clear default
        units = []
        for unit_data in data.get('units', []):
            ut = self._type_lookup.get(unit_data['type'], None)
            if not ut:
                return
            unit_class = ut.unit_class
            unit = unit_class(parent=self)
            unit.load(unit_data)
            idx = self.get_insert_position(ut.order, units)
            units.insert(idx, unit)
        self.units = units
        for option_data in data.get('options', []):
            try:
                next(o for o in self.options if o.id == option_data['id']).load(option_data)
            except StopIteration:
                print('StopIteration for', option_data['id'])

    def get_errors(self):
        return [aggr for aggr in chain(*[inner for inner in chain(
            (unit.errors for unit in self.units),
            (utype.error for utype in self.types),
            (restriction.get_errors() for restriction in self._restrictions))])]

    def get_units(self, unit_type):
        return [unit for unit in self.units if unit.type_id == unit_type]

    def count_units(self, unit_type):
        return sum(1 for unit in self.units if unit.type_id == unit_type)

    def check_rules_chain(self):
        super(Formation, self).check_rules_chain()
        self.check_rules()

    def check_rules(self):
        self.errors = self.get_errors()

    def error(self, message):
        self.errors.append(message)

    @property
    def roster(self):
        return self

    def build_statistics(self):
        from collections import defaultdict
        res = defaultdict(int)
        for unit in self.units:
            for k, v in list(unit.build_statistics().items()):
                res[k] += v
        return dict(res)


class PercentSection(Section):
    def __init__(self, parent, sec_id, name, min_limit=0, max_limit=100):
        super(PercentSection, self).__init__(parent, sec_id, name, min_limit, max_limit)

    def check_limits(self):
        pass

    def get_limits_error(self):
        if not self.roster.limit:
            return
        current_percent = 100.0 * self.points / self.roster.limit
        if self.max < current_percent:
            return 'Section {0} is more then {1}% of total points'.format(self.name, self.max)
        elif self.min > current_percent:
            return 'Section {0} is less then {1}% of total points'.format(self.name, self.min)
