__author__ = 'dromanow'


class Observable(object):
    def __init__(self, value):
        super(Observable, self).__init__()
        self.id = None
        self._value = value
        self._update_count = 0

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, val):
        if self._value == val:
            return
        self._update_count += 1
        self._value = val

    def _common_delta(self):
        if self._update_count is 0:
            return {}
        #self._update_count = 0
        return {self.id: self.value}

    def _object_delta(self):
        delta = self.value.delta
        if not delta:
            return {}
        return {self.id: delta}

    def _array_delta(self):
        if not len(self.value):
            return self._common_delta()
        res = []
        for opt in self.value:
            delta = opt.delta
            if delta == {}:
                continue
            res.append(delta)
        if not res:
            return {}
        return {self.id: res}

    @property
    def delta(self):
        try:
            if isinstance(self.value, (tuple, list)):
                return self._array_delta()
            else:
                return self._object_delta()
        except AttributeError:
            return self._common_delta()

    def flush(self):
        try:
            if isinstance(self.value, (tuple, list)) and len(self.value):
                for opt in self.value:
                    opt.flush()
            else:
                self.value.flush()
        except AttributeError:
            self._update_count = 0


class Node(object):
    def __init__(self, parent, node_id=None, active=True, visible=True):
        super(Node, self).__init__()
        self.parent = parent
        self.id = node_id
        self._observables = None
        self._active = Observable(active)
        self._visible = Observable(visible)

    @staticmethod
    def norm_name(name):
        return name[1:] if name[0] == '_' else name

    @property
    def roster(self):
        return self.parent.roster if self.parent else self

    @property
    def root(self):
        return self.parent.root if self.parent else self

    @property
    def observable(self):
        if self._observables is None:
            self._observables = []  # we need avoid init in next call
            observables = []
            for n in dir(self):
                obj = getattr(self, n)
                if isinstance(obj, Observable):
                    obj.id = self.norm_name(n)
                    observables.append(obj)
            self._observables = observables
        return self._observables

    @property
    def active(self):
        return self._active.value

    @active.setter
    def active(self, value):
        self._active.value = value

    @property
    def visible(self):
        return self._visible.value

    @visible.setter
    def visible(self, value):
        self._visible.value = value

    @property
    def delta(self):  # Get and clean current delta
        delta = {}
        for o in self.observable:
            delta.update(o.delta)
        if delta == {}:
            return {}
        delta['id'] = self.id
        return delta

    def flush(self):
        for o in self.observable:
            o.flush()

    def check_rules_chain(self):
        for o in self.observable:
            if isinstance(o.value, (tuple, list)):
                for opt in o.value:
                    if hasattr(opt, 'check_rules_chain'):
                        opt.check_rules_chain()
            else:
                if hasattr(o.value, 'check_rules_chain'):
                    o.value.check_rules_chain()

    def dump(self):  # Dump currents attributes state
        return {
            'id': self.id,
            'active': self.active,
            'visible': self.visible,
        }

    def save(self):
        return {
            'id': self.id,
        }

    def load(self, data):
        self.id = data.get('id', self.id)


class IdGenerator(object):
    def __init__(self):
        super(IdGenerator, self).__init__()
        self.idx = 0

    def __next__(self):
        self.idx += 1
        return '_id_' + str(self.idx)
