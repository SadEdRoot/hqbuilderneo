from builder.core2.node import IdGenerator

__author__ = 'dromanow'

import unittest


class IdGeneratorTest(unittest.TestCase):
    def test_generator(self):
        gen = IdGenerator()
        self.assertTrue(next(gen) == '_id_1')
        self.assertTrue(next(gen) == '_id_2')
        self.assertTrue(next(gen) == '_id_3')
