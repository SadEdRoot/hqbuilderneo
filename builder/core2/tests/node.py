__author__ = 'dromanow'

import unittest

from builder.core2.node import Observable, Node


class ObservableTest(unittest.TestCase):

    def test_common_usage(self):
        obs = Observable(value=1)
        self.assertTrue(obs.value == 1)
        self.assertDictEqual(obs.delta, {})

        obs.id = 'test_id'
        obs.value = 5
        self.assertTrue(obs.value == 5)
        self.assertDictEqual(obs.delta, {'test_id': 5})
        obs.flush()
        self.assertDictEqual(obs.delta, {})

        obs.value = 3
        obs.flush()
        self.assertDictEqual(obs.delta, {})

        obs.value = 3
        self.assertDictEqual(obs.delta, {})

        obs.value = 4
        self.assertDictEqual(obs.delta, {'test_id': 4})

    class TestClass(object):
        def __init__(self, obj_id='test_id', value=10):
            super(ObservableTest.TestClass, self).__init__()
            self.delta = {obj_id: value}

        def flush(self):
            self.delta = {}

    def test_object_usage(self):
        test_class = self.TestClass()

        obs = Observable(test_class)
        obs.id = 'obs_id'
        self.assertDictEqual(obs.delta, {'obs_id': {'test_id': 10}})

        obs.flush()
        self.assertDictEqual(obs.delta, {})

        self.assertTrue(test_class == obs.value)

    def test_array_usage(self):
        classes = [
            self.TestClass('id1', value=1),
            self.TestClass('id2', value=2),
            self.TestClass('id3', value=3),
        ]
        obs = Observable(0)
        obs.id = 'test_id'
        obs.value = [1, 2, 3]

        self.assertDictEqual(obs.delta, {'test_id': [1, 2, 3]})
        obs.flush()
        self.assertDictEqual(obs.delta, {})

        obs.value = classes
        self.assertDictEqual(obs.delta, {'test_id': [
            classes[0].delta,
            classes[1].delta,
            classes[2].delta,
        ]})

        # TestClass has not auto flush delta
        obs.flush()
        self.assertDictEqual(obs.delta, {})

        obs = Observable([1, 2, 3])
        obs.id = 'test_id'
        obs.value = []
        self.assertDictEqual(obs.delta, {'test_id': []})
        obs.flush()
        self.assertDictEqual(obs.delta, {})


class NodeTest(unittest.TestCase):

    def test_create(self):
        node = Node(parent=self)
        self.assertEqual(node.parent, self)
        self.assertTrue(node.id is None)
        self.assertTrue(node.active is True)
        self.assertTrue(node.visible is True)

        node = Node(parent=self, node_id='test_id')
        self.assertTrue(node.id == 'test_id')
        self.assertTrue(node.active is True)
        self.assertTrue(node.visible is True)

        node = Node(parent=self, active=False)
        self.assertTrue(node.id is None)
        self.assertTrue(node.active is False)
        self.assertTrue(node.visible is True)

        node = Node(parent=self, visible=False)
        self.assertTrue(node.id is None)
        self.assertTrue(node.active is True)
        self.assertTrue(node.visible is False)

        node = Node(parent=None)
        node.active = False
        self.assertTrue(node.active is False)
        node.visible = False
        self.assertTrue(node.visible is False)
        self.assertEqual(node.parent, None)

    def test_dump(self):
        node = Node(parent=None, node_id='test_id')
        self.assertDictEqual(node.dump(), {
            'id': 'test_id',
            'visible': True,
            'active': True
        })

        node.active = False
        self.assertDictEqual(node.dump(), {
            'id': 'test_id',
            'visible': True,
            'active': False
        })

        node.visible = False
        self.assertDictEqual(node.dump(), {
            'id': 'test_id',
            'visible': False,
            'active': False
        })

    def test_load(self):
        node = Node(parent=None)
        data = {'id': 'test_id'}
        node.load(data)
        self.assertDictEqual(node.dump(), {
            'id': 'test_id',
            'visible': True,
            'active': True
        })
        self.assertDictEqual(node.save(), data)

        node = Node(parent=None)
        node.load({})

        self.assertDictEqual(node.dump(), {
            'id': None,
            'visible': True,
            'active': True
        })

    def test_delta(self):
        class TestNode(Node):
            def __init__(self):
                super(TestNode, self).__init__(parent=None, node_id='test_id')
                self.obs1 = Observable(1)
                self._obs2 = Observable(2)

                self.active = False
                self.visible = False
                self.obs1.value = 3
                self._obs2.value = 4

        node = TestNode()
        self.assertDictEqual(node.delta, {
            'id': 'test_id',
            'active': False,
            'visible': False,
            'obs1': 3,
            'obs2': 4,
        })

        node = TestNode()
        node.flush()
        self.assertDictEqual(node.delta, {})

    def test_check_rules(self):
        class Checker(object):
            def __init__(self):
                super(Checker, self).__init__()
                self.checked = False

            def check_rules_chain(self):
                self.checked = True

        class TestNode(Node):
            def __init__(self):
                super(TestNode, self).__init__(parent=None, node_id='test_id')
                self.obs1 = Observable(Checker())
                self.obs2 = Observable([Checker(), Checker(), Checker()])

        node = TestNode()
        node.check_rules_chain()

        self.assertTrue(node.obs1.value.checked == node.obs2.value[0].checked == node.obs2.value[1].checked ==
                     node.obs2.value[2].checked and node.obs1.value.checked is True)
#
#
##class Armour(OneOf):
##    def __init__(self):
##        super(Armour, self).__init__('Armour')
##        self.pwr = self.Variant('Power armour', 0)
##        self.art = self.Variant('Artificer armour', 20)
##        self.tda = self.Variant('Terminator armour', 40)
##
##
##class OneOfTest(unittest.TestCase):
##
##    def test_init(self):
##        opt1 = Armour()
##        print opt1.dump()
##
##        data = {
##            'used': True,
##            'name': 'Armour',
##            'visible': True,
##            'active': True,
##            'type': 'one_of',
##            'options': [
##                {
##                    'used': True,
##                    'name': 'Power armour (free)',
##                    'visible': True,
##                    'points': 0,
##                    'active': True,
##                    'id': 'pwr'
##                }, {
##                    'used': True,
##                    'name': 'Terminator armour (+40pt.)',
##                    'visible': True,
##                    'points': 40,
##                    'active': True,
##                    'id': 'tda'
##                }, {
##                    'used': True,
##                    'name': 'Artificer armour (+20pt.)',
##                    'visible': True,
##                    'points': 20,
##                    'active': True,
##                    'id': 'art'
##                }
##            ]
##        }
#
#
#        #opt = oldOneOf('test_id', 'Armour', [
#        #    ['Power armour', 0, 'pwr'],
#        #    ['Artificer armour', 20, 'art'],
#        #    ['Terminator armour', 40, 'tda']
#        #])
#        #self.assertDictEqual(opt.dump(), {})
#        {'active': True,
#-  'id': 'test_id',
#-  'name': 'Armour',
#-  'options': [{'active': True,
#-               'id': 'pwr',
#-               'name': 'Power armour (free)',
#-               'visible': True},
#-              {'active': True,
#-               'id': 'art',
#-               'name': 'Artificer armour (+20pt.)',
#-               'visible': True},
#-              {'active': True,
#-               'id': 'tda',
#-               'name': 'Terminator armour (+40pt.)',
#-               'visible': True}],
#-  'selected': 'pwr',
#-  'type': 'one_of',
#-  'visible': True}
