__author__ = 'Ivan Truskov'

from flask import Blueprint, request
from werkzeug.contrib.atom import AtomFeed
from operator import attrgetter
from . import feed


class FeedBlueprint(Blueprint):
    def __init__(self, app):
        super(FeedBlueprint, self).__init__('feed', __name__)
        # regenerate feed
        feed.regenerate_feed(self.root_path)
        feed.articles_feed = []
        feed.fetch_feed('{}/tags'.format(self.root_path))
        feed.articles_feed.sort(key=attrgetter('tag_date'), reverse=True)

        def view_feed():
            atom_feed = AtomFeed('Last Updates', feed_url=request.url, url=request.url_root)
            for article in feed.articles_feed:
                atom_feed.add('Update', str(article.content),
                              content_type='html', author=article.author,
                              id=article.commit_id,
                              updated=article.tag_date, published=article.commit_date)
            return atom_feed.get_response()

        self.add_url_rule('/updates.xml', endpoint='feed', view_func=view_feed)

        app.register_blueprint(self)
