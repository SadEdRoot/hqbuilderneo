__author__ = 'dante'

from waaagh.multiblog.models import Post, Tag, PrivateMessage, User, UserTag
from waaagh.multiblog.consts import most_commented_limit, most_rated_limit
from waaagh.multiblog.controllers.online import get_online_users, add_online_user
from waaagh.multiblog.controllers.post_list import get_bookmarked_posts_headers

# Stable data structure:
#   stable
#       current_tag
#       auth: #TODO
#       current_post
#           id
#           can_edit
#       current_user
#           username
#           my_user True if current user is auth user
#       current_blog
#           author
#           title
#           description
#           my_blog: True if it's auth blog
#       blog_ref: "/blog/<current_blog.author>" or ""
#       side_bar
#           bookmarks
#           most_rated
#           most_commented
#           tags
#           online

def get_side_tags(user):
    if user is None:
        return Tag.objects.exclude(post_count=0).order_by('-post_count')
    else:
        return UserTag.objects.filter(user = user).exclude(post_count=0).order_by('-post_count')

def side_bar_data(request, current_blog_author):
    data = {
        'most_rated': Post.objects.order_by('-rate')[0:most_rated_limit],
        'most_commented': Post.objects.order_by('-comments')[0:most_commented_limit],
        'tags': get_side_tags(current_blog_author),
        'online': get_online_users(),
    }
    if request.user.is_authenticated():
        data['bookmarks'] = get_bookmarked_posts_headers(request.user)
    return data

def get_auth_info(request):
    if not request.user.is_authenticated():
        return None
    add_online_user(request.user.username)
    return {
        'username': request.user.username,
        'unread_mail': PrivateMessage.objects.filter(to_user=request.user, read_time=None).count(),
        'avatar': request.user.get_profile().avatar,
        }

def get_current_blog(request, author):
    if not author:
        return None
    return {
        'author': author.username,
        'title': author.get_profile().blog_title,
        'description': author.get_profile().blog_description,
        'my_blog': request.user.username == author.username if request.user.is_authenticated() else False,
    }

def get_blog_ref_prefix(author):
    if not author:
        return ''
    return '/blog/' + author.username

def get_current_user(request, user):
    if not user:
        return None
    return {
        'username': user.username,
        'my_user': user.username == request.user.username if request.user.is_authenticated() else False,
    }

def get_stable_data(request, tag = None, current_user = None, current_blog_author = None):
    current_user = User.objects.get(username=current_user) if current_user else None
    current_blog_author = User.objects.get(username=current_blog_author) if current_blog_author else None
    return {
        'auth': get_auth_info(request),
        'side_bar': side_bar_data(request, current_blog_author),
        'current_tag': tag,
        'current_user': get_current_user(request, current_user),
        'current_blog': get_current_blog(request, current_blog_author),
        'blog_ref': get_blog_ref_prefix(current_blog_author),
    }