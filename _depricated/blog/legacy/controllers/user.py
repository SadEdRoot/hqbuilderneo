__author__ = 'dante'

from django.contrib import auth
from datetime import date
from waaagh.multiblog.tools.misc import get_random_key
from waaagh.multiblog.models import UserProfile, FilterTag, FilterUser, User, Tag

def block_user(user):
    user.is_active = False
    user.save()

def unblock_user(user):
    user.is_active = True
    user.save()

def get_user_settings(user):
    filters = {
        UserProfile.filter_all : 'all',
        UserProfile.filter_include : 'include',
        UserProfile.filter_exclude : 'exclude',
        }

    profile = user.get_profile()

    user_data = {
        'e_mail': user.email,
        'city': user.get_profile().city,
        'country' : user.get_profile().country,
        'about': user.get_profile().about,

        'blog_title': profile.blog_title,
        'blog_description': profile.blog_description,
        'new_messages_notify': profile.new_messages_notify,
        'new_bookmarked_comments_notify': profile.new_bookmarked_comments_notify,
        'new_my_post_comments_notify': profile.new_my_post_comments_notify,
        'always_keep_bookmarks': profile.always_keep_bookmarks,
        'keep_bookmarks': profile.keep_bookmarks,

        'filter_tags': filters[profile.filter_tags],
        'filter_tags_list': [ filter_tag.filtered_tag.title for filter_tag in user.filtertag_set.all() ],
        'filter_users': filters[profile.filter_users],
        'filter_users_list' : [ filter_user.filtered_user.username for filter_user in user.stream_owner.all() ],
    }
    if user.get_profile().birth_date is not None:
        user_data.update({
            'empty_birth': 'false',
            'birth_day' : user.get_profile().birth_date.day,
            'birth_month' : user.get_profile().birth_date.month,
            'birth_year' : user.get_profile().birth_date.year
        })
    else:
        user_data.update({ 'empty_birth': 'true', 'birth_day' : '', 'birth_month' : '', 'birth_year' : '' })
    return user_data

def update_user_secure(user, password, new_password, email):
    auth_user = auth.authenticate(username=user.username,
        password=password)
    if auth_user is None or not auth_user.is_active:
        return 'denied'
    user.email=email
    if new_password != '':
        user.set_password(new_password)
    user.save()
    return 'success'

def update_user_profile(user, country, city, date_value, about):
    profile = user.get_profile()
    profile.country = country
    profile.city = city
    profile.about = about
    if date_value:
        profile.birth_date = date(date_value['year'], date_value['month'], date_value['day'])
    else:
        profile.birth_date = None
    profile.save()
    return 'success'

def update_user_settings(user, blog_title, blog_desc, message_notify, bookmarked_notify, post_notify, keep):
    profile = user.get_profile()
    profile.blog_title = blog_title
    profile.blog_description = blog_desc
    profile.new_messages_notify = message_notify
    profile.new_bookmarked_comments_notify = bookmarked_notify
    profile.new_my_post_comments_notify = post_notify
    if keep is True:
        profile.always_keep_bookmarks = True
    else:
        profile.always_keep_bookmarks = False
        profile.keep_bookmarks = keep
    profile.save()
    return 'success'

def update_user_avatar(user, data):
    if not data['avatar']:
        return
    import os
    from waaagh.settings import AVATARS_DIR

    profile = user.get_profile()
    file_name, file_extension = os.path.splitext(data['avatar'].name)
    avatar = get_random_key() + file_extension
    with open(os.path.join(AVATARS_DIR, avatar), 'wb+') as destination:
        for chunk in data['avatar'].chunks():
            destination.write(chunk)
    try:
        os.remove(os.path.join(AVATARS_DIR, profile.avatar))
    except Exception:
        pass
    profile.avatar = avatar
    profile.save()

def update_stream(user, tag_filter, tags, user_filter, users):
    filters = {
        'all': UserProfile.filter_all,
        'include': UserProfile.filter_include,
        'exclude': UserProfile.filter_exclude,
        }

    profile = user.get_profile()
    profile.filter_users = filters[user_filter]
    profile.filter_tags = filters[tag_filter]

    failed_users = []
    users_obj = []
    if filters[user_filter] is not UserProfile.filter_all:
        for username in users:
            try:
                users_obj.append(User.objects.get(username = username))
            except User.DoesNotExist:
                failed_users.append(username)

    failed_tags = []
    tag_obj = []
    if filters[tag_filter] is not UserProfile.filter_all:
        for tag in tags:
            try:
                tag_obj.append(Tag.objects.get(title = tag))
            except Tag.DoesNotExist:
                failed_tags.append(tag)

    if len(failed_users) is not 0 or len(failed_tags) is not 0:
        return {'fail' : {'tags': failed_tags, 'users': failed_users }}

    FilterTag.objects.filter(stream_owner = user).delete()
    for tag in tag_obj:
        FilterTag(filtered_tag = tag, stream_owner = user).save()

    FilterUser.objects.filter(stream_owner = user).delete()
    for filtered_user  in users_obj:
        FilterUser(filtered_user = filtered_user, stream_owner = user).save()

    profile.save()
    return 'success'

