from django.contrib import admin
from _depricated.blog.legacy.models import *

admin.site.register(UserProfile)
admin.site.register(Tag)
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Vote)
admin.site.register(Bookmark)
admin.site.register(PrivateMessage)
admin.site.register(PostHit)

admin.site.register(Roster)
admin.site.register(Unit)
admin.site.register(Option)

