import pymongo
from .blog.records import Post, Comment, get_comment_from_record, get_post_from_record

__author__ = 'dante'


def can_modify_post(user, owner):
    return user.admin or user.id == owner.id


def build_new_post(data, user):
    return Post(text=data['text'], author=user, tags=data['tags'], title=data['title'])


def process_post(data, user):
    if 'preview' in list(data.keys()):
        post = build_new_post(data['preview'], user)
        return post.dump()

    if 'send' in list(data.keys()):
        post = build_new_post(data['send'], user)
        post.save()
        return post.dump()

    if 'comment' in list(data.keys()):
        data = data['comment']

        parent_id = data.get('parent', None)
        parent_id = ObjectId(parent_id) if parent_id else None  # it's works if parent not set, or set to None

        post_id = ObjectId(data['post_id'])

        comment = Comment(text=data['text'], author=user, post_id=post_id, parent_id=parent_id)
        comment.save()
        return get_comments(post_id=post_id, skip=data.get('count', 0))

    if 'delete_post' in list(data.keys()):
        post_id = ObjectId(data['delete_post']['post_id'])
        post = get_post_from_record(posts.find_one({'_id': post_id}))
        if can_modify_post(user, post.author):
            post.deleted = True
            post.save()
            return dict(result='ok')
        else:
            return dict(error='Access denied!')

    if 'get_raw_post' in list(data.keys()):
        post_id = data.get('get_raw_post')
        return dict(text=get_post_from_record(posts.find_one({'_id': ObjectId(post_id)})).text)

    if 'edit_post' in list(data.keys()):
        post_data = data['edit_post']
        post = get_post_from_record(posts.find_one({'_id': ObjectId(post_data['post_id'])}))
        if not can_modify_post(user, post.author):
            return dict(error='Access denied!')
        post.tags = post_data['tags']
        post.text = post_data['text']
        post.title = post_data['title']
        post.save()
        return post.dump()

    if 'delete_comment' in list(data.keys()):
        comment_id = ObjectId(data['delete_comment']['comment_id'])
        comment = get_comment_from_record(comments.find_one({'_id': ObjectId(comment_id)}))
        if not can_modify_post(user, comment.author):
            return dict(error='Access denied!')
        comment.deleted = True
        comment.save()
        return comment.dump()

    if 'get_raw_comment' in list(data.keys()):
        comment_id = data.get('get_raw_comment')
        return dict(text=get_comment_from_record(comments.find_one({'_id': ObjectId(comment_id)})).text)

    if 'edit_comment' in list(data.keys()):
        comment_data = data['edit_comment']
        comment_id = ObjectId(comment_data['comment_id'])
        comment = get_comment_from_record(comments.find_one({'_id': ObjectId(comment_id)}))
        if not can_modify_post(user, comment.author):
            return dict(error='Access denied!')
        comment.text = comment_data['text']
        comment.save()
        return comment.dump()


def get_data(data):
    if 'get_comments' in list(data.keys()):
        req = data['get_comments']
        return get_comments(post_id=ObjectId(req.get('post_id', None)), skip=req.get('count', 0))

    if 'get_more_posts' in list(data.keys()):
        data = data['get_more_posts']
        last_post = get_post_from_record(posts.find_one({'_id': ObjectId(data['last'])}))
        return dict(posts=get_posts(tag=data.get('tag'), user=data.get('user'), time=last_post.time))


def get_posts(user=None, tag=None, post=None, time=None):
    limit = 20
    req = {'$or': [{'deleted': {'$exists': False}}, {'deleted': False}]}
    if post:
        req.update(_id=ObjectId(post))
    if user:
        req.update(author_id=ObjectId(user))
    if tag:
        req.update(tags=tag)
    if time:
        req.update(time={'$lt': time})
    return [get_post_from_record(post).dump() for post in posts.find(req).sort('time', pymongo.DESCENDING).limit(limit)]


def get_comments(post_id, skip):
    if not post_id:
        return []
    for c in comments.find({'post_id': post_id}).skip(skip):
        print(c)
    return dict(comments=[get_comment_from_record(c).dump()
                          for c in comments.find({'post_id': post_id}).sort('time', pymongo.ASCENDING).skip(skip)])
