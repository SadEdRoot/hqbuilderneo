((nil . ((eval . (progn
                   ;; require projectile; the following code will fail
                   ;; without it.
                   (require 'projectile)
                   ;; provide a fake "recent" compilation cmd
                   ;; which will be returned by the function
                   ;; `projectile-compilation-command`
                   (puthash (projectile-project-root)
                            "python hq.py --debug"
                            projectile-compilation-cmd-map)
                   (puthash (projectile-project-root)
                            "nosetests"
                            projectile-test-cmd-map))))))
