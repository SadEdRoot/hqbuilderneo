ARG base_docker_file_hash
ARG requirements_hash

FROM hqbuilder/base-${base_docker_file_hash}-${requirements_hash}:latest

# Bundle app source
COPY . /src/hqbuilderneo/
WORKDIR /src/hqbuilderneo/

EXPOSE  5000
ENTRYPOINT python /src/hqbuilderneo/hq.py --tornado