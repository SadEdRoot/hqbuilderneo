// RosterSettings

var RosterSettings = function(block) {
    this.block = block;
    this.active = ko.observable(false);
};

RosterSettings.prototype.activate = function() {
    this.active(!this.active());
};

RosterSettings.prototype.addOption = function(opt) {
    this.block.options.unshift(opt);
};


// helper function
function mapDictionaryToStatisticsArray(dictionary) {
    var result = [];
    for (var key in dictionary) {
        if (dictionary.hasOwnProperty(key)) {
            result.push({ item: "<p><b>" + key + "</b>: " + dictionary[key] + "</p>" }); 
        }  
    }
    return result;
}

// RosterData

var RosterData = function(parent, data) {
    var self = this;
    this.id = data.id;
    this.parent = parent;
    this.errors = ko.observableArray([]);
    this.errorsCount = ko.computed(function() {return self.errors().length; });

    this.sections = ko.observableArray(data.sections.map(function(section) {
        return new Section({parent: self, data: section});
    }));
    this.options_description = ko.observable(data.options_description);
    this.settings = new RosterSettings(new OptionsBlock(self, data.options));
    this.properties = new RosterSettings(new OptionsBlock(self, data.properties));
    // always visible if any present
    this.properties.active(true);
    this.statistics = ko.observableArray(mapDictionaryToStatisticsArray(data.statistics));
    this.updateErrors(data);
};

RosterData.prototype.update = function(obj) {
    this.parent.update({id: this.id, options: [obj]})
};

RosterData.prototype.updateErrors = function(data) {
    this.errors(data.errors ? data.errors : []);
};

RosterData.prototype.updateSection = function(obj) {
    this.parent.update({id: this.id, sections: [obj]});
};

RosterData.prototype.pushChanges = function(data) {
    var self = this;
    if (data.sections)
        $.each(data.sections, function(n, sec_data) {
            $.each(self.sections(), function(n, section) {
                if (section.id == sec_data.id)
                    section.pushChanges(sec_data);
            });
        });
    if (data.options)
    {
        this.settings.block.pushChanges(data.options);
    }
    if (data.options_description)
    {
        this.options_description(data.options_description);
    }
    if(data.properties)
    {
        this.properties.block.pushChanges(data.properties);
    }
    if (data.statistics)
    {
        this.statistics.removeAll();
        mapDictionaryToStatisticsArray(data.statistics).forEach(function(element)
                                                                {
                                                                    self.statistics.push(element);
                                                                });
    }
    this.updateErrors(data);
};

// Roster

var Roster = function(roster) {
    var self = this;
    this.setupClick = function() {
        this.data.settings.active(true);
        scrollToElement($("#settings"));
    };
    this.loaded = ko.observable(false);
    this.name = new EditField({
        data: {value: roster.data.name, name: "Name", visible: true, active: true},
        update: function(value) { self.update({id: self.data.id, name: value}); }
    });
    this.points = ko.observable(roster.data.points);
    this.limit = new EditField({
        data: {value: roster.data.limit, name: "Limit", visible: true, active: true},
        update: function(value) { self.update({id: self.data.id, limit: parseInt(value)}); },
        key: digitFilter,
        type: 'roster_limit'
    });
    this.description = new EditField({
        data: {value: roster.data.description, name: "Description", visible: true, active: true},
        update: function(value) { self.update({id: self.data.id, description: value}); }
    });
    this.data = new RosterData(this, roster.data);
    this.data.settings.addOption(this.limit);
    this.data.settings.addOption(this.description);
    this.data.settings.addOption(this.name);
    this.deleteRequest = new AjaxTransport(roster.deleteUrl);
    this.updateRequest = new AjaxTransport(roster.updateUrl);

    this.deleteRoster = function() {
        document.modalInfo.warning("Delete roster? Are you sure?", function() {
            self.deleteRequest.send({'delete': self.data.id});
        });
    };

    this.updateTitle = function() {
        document.title = '{0} ({1}{2}pt.) {3}'.format(
            self.name.value(),
            self.points(),
            self.limit.value() ? '/{0}'.format(self.limit.value()) : '',
            self.description.value()
        );
    };

    this.data.updateErrors(roster.data);
    this.updateTitle();

    this.pushChanges = function(data) {
        if (data.name !== undefined)
            self.name.value(data.name);
        if (data.points !== undefined)
            self.points(data.points);
        if (data.limit !== undefined)
            self.limit.value(data.limit);
        if (data.description !== undefined)
            self.description.value(data.description);
        self.data.pushChanges(data);
        self.updateTitle();
    };

    this.update = function(obj) {
        self.updateRequest.send({roster: obj}, function (data) { // TODO need avoid parallel update call due pushing changes
            if (!data.roster || data.roster.id != self.data.id) {
                if (!data.reload)
                    return;
                location = data.reload;
            }
            else
                self.pushChanges(data.roster);
        });
    };
    this.loaded(true);
};
