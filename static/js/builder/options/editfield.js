/**
 * Created by dromanow on 4/30/14.
 */


var EditField = function(opt) {
    this.setupNode(opt.parent, opt.data);
    this.type = opt.type || 'edit_field';
    this.section = opt.data.name;
    this.value = ko.observable(opt.data.value);
    this.newValue = ko.observable(opt.data.value);
    this.newValue.key = opt.key || function digitFilter(obj, event) { return true; };
    this.editable = ko.observable(false);
    this.focused = ko.observable(false);
    this.update = opt.update;
};

EditField.prototype = new Node();

EditField.prototype.onClick = function() {
    this.editable(true);
    this.focused(true);
};

EditField.prototype.onOk = function() {
    this.value(this.newValue());
    if (this.update)
        this.update(this.value());
    else
        this.parent.update({id: this.id, value: this.value()});
    this.editable(false);
};

EditField.prototype.onCancel = function() {
    this.newValue(this.value());
    this.editable(false);
};

EditField.prototype.toggle = function() {
    this.visible(!this.visible());
    this.active(!this.active());
    if(!this.visible()) {
        this.editable(false);
    }
};
