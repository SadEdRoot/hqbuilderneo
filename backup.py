#!/usr/bin/python

import os
import datetime
import time

#folder_name = "/home/web/backup"
#base_file_name = folder_name + '.tar.bz2'
count = 0
back_up_num = 10

while True:
    print('Backup from {}'.format(datetime.datetime.utcnow()))
    file_name = os.path.join('home', 'web', 'backup', 'hq_backup_{}'.format(datetime.datetime.utcnow()))
    os.system('pg_dump bunker -h 127.0.0.1 -U hqbuilder | bzip2 -c > {path}'.format(file_name))
    os.system('mv {0} /mnt/yandex.disk/hq_backup'.format(file_name))
    print('... finished')
    time.sleep(60 * 60 * 12)
