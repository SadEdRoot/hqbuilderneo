"""battles added

Revision ID: 2824fef8e347
Revises: 42b1fd8c3e07
Create Date: 2014-05-16 13:12:04.155480

"""

# revision identifiers, used by Alembic.
revision = '2824fef8e347'
down_revision = '42b1fd8c3e07'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


def upgrade():
    op.create_table(
        'battles',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('owner_id', sa.Integer(), nullable=False),
        sa.Column('state', sa.Integer(), nullable=False),
        sa.Column('created', sa.DateTime(), nullable=False),
        sa.Column('result', sa.Integer(), nullable=False),
        sa.Column('played', sa.Date(), nullable=False),
        sa.Column('description', sa.Text(), nullable=True),
        sa.Column('data', postgresql.JSON(), nullable=True),
        sa.ForeignKeyConstraint(['owner_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index('ix_battles_owner_id', 'battles', ['owner_id'], unique=False)
    op.create_index('ix_battles_result', 'battles', ['result'], unique=False)
    op.create_index('ix_battles_state', 'battles', ['state'], unique=False)


def downgrade():
    op.drop_index('ix_battles_state', 'battles')
    op.drop_index('ix_battles_result', 'battles')
    op.drop_index('ix_battles_owner_id', 'battles')
    op.drop_table('battles')
