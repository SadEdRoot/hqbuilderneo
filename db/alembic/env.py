
import os
import sys
sys.path.append(os.getcwd())
from alembic import context
from sqlalchemy import engine_from_config, pool
from logging.config import fileConfig

config = context.config
fileConfig(config.config_file_name)

debug_mode = not context.get_x_argument(as_dictionary=True).get('mode', 'debug') == 'release'

if debug_mode:
    from config import DebugConfig as Config
    print('DebugConfig.SQLALCHEMY_DATABASE_URI ', Config.SQLALCHEMY_DATABASE_URI)
else:
    from config import ReleaseConfig as Config
    print('ReleaseConfig.SQLALCHEMY_DATABASE_URI ', Config.SQLALCHEMY_DATABASE_URI)


config.set_main_option('sqlalchemy.url', Config.SQLALCHEMY_DATABASE_URI)

from db.models import *
target_metadata = db.metadata


def run_migrations_offline():
    url = config.get_main_option("sqlalchemy.url")
    context.configure(url=url)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    engine = engine_from_config(
                config.get_section(config.config_ini_section),
                prefix='sqlalchemy.',
                poolclass=pool.NullPool)

    connection = engine.connect()
    context.configure(
                connection=connection,
                target_metadata=target_metadata
                )

    try:
        with context.begin_transaction():
            context.run_migrations()
    finally:
        connection.close()

if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()

