from flask import url_for
from flask.ext.mail import Mail, Message


class Mailer(Mail):
    def __init__(self, app):
        super(Mailer, self).__init__(app)
        self.tpl = app.jinja_env.get_template('mail/mail.html')
        self.bcc = [app.config['MAIL_BCC']]

    def send_mail(self, to, subj, text):
        msg = Message(
            subject=subj,
            html=self.tpl.render(text=text),
            recipients=[to],
            bcc=self.bcc
        )
        self.send(msg)

    def send_recovery_key(self, to, key):
        self.send_mail(to, 'Password recovery', 'For recovery password for {0} please follow next link: '
                                                '<a href="{1}">{1}</a>'.format(to, url_for('auth.recovery', key=key,
                                                                                           _external=True)))

    def send_new_password(self, to, password):
        self.send_mail(to, 'New password', 'New password for {}: {}'.format(to, password))

    def send_new_status(self, to, status):
        self.send_mail(to, 'Account changed', 'Your account type changed to {}'.format(status))
